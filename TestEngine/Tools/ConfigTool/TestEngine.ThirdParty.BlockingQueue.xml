<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.ThirdParty.BlockingQueue</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.ThirdParty.BlockingQueue">
            <summary>
            Author: William Stacey, MVP (staceyw@mvps.org)
            Modified Date: 08/03/2004
            One Lock Bounded Blocking Queue (e.g. Bounded Buffer).
            This queue is internally synchronized (thread-safe) and designed for one-many producers and one-many
            consumer threads.  This is ideal for pipelining or other consumer/producer needs.
            Fast and thread safe on single or multiple cpu machines.
            
            Consumer thread(s) will block on Dequeue operations until another thread performs a Enqueue
            operation, at which point the first scheduled consumer thread will be unblocked and get the
            current object.  Producer thread(s) will block on Enqueue operations until another
            consumer thread calls Dequeue to free a queue slot, at which point the first scheduled producer
            thread will be unblocked to finish its Enqueue operation.  No user code is needed to
            handle this "ping-pong" between locking/unlocking consumers and producers. 
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.#ctor(System.Int32)">
            <summary>
            Create instance of Queue with Bounded number of elements.  After that
            many elements are used, another Enqueue operation will "block" or wait
            until a Consumer calls Dequeue to free a slot.  Likewise, if the queue
            is empty, a call to Dequeue will block until another thread calls
            Enqueue.
            </summary>
            <param name="size"></param>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Enqueue(System.Object)">
            <summary>
            Adds an object to the end of the queue. If queue is full, this method will
            block until another thread calls one of the Dequeue methods.  This method will wait
            "Timeout.Infinite" until queue has a free slot.
            </summary>
            <param name="value"></param>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Enqueue(System.Object,System.Int32)">
            <summary>
            Adds an object to the end of the queue. If queue is full, this method will
            block until another thread calls one of the Dequeue methods or millisecondsTimeout
            expires.  If timeout, method will throw QueueTimeoutException.
            </summary>
            <param name="value"></param>
            <param name="millisecondsTimeout">Missing comment</param>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.TryEnqueue(System.Object)">
            <summary>
            Non-blocking version of Enqueue().  If Enqueue is successfull, this will
            return true; otherwise false if queue is full.
            </summary>
            <param name="value"></param>
            <returns>true if successfull, otherwise false.</returns>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Dequeue">
            <summary>
            Removes and returns the object at the beginning of the Queue.
            If queue is empty, method will block until another thread calls one of
            the Enqueue methods.   This method will wait "Timeout.Infinite" until another
            thread Enqueues and object.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Dequeue(System.Int32)">
            <summary>
            Removes and returns the object at the beginning of the Queue.
            If queue is empty, method will block until another thread calls one of
            the Enqueue methods or millisecondsTimeout expires.
            If timeout, method will throw QueueTimeoutException.
            </summary>
            <returns>The object that is removed from the beginning of the Queue.</returns>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.TryDequeue(System.Object@)">
            <summary>
            Non-blocking version of Dequeue.  Will return false if queue is empty and set
            value to null, otherwise will return true and set value to the dequeued object.
            </summary>
            <param name="value">The object that is removed from the beginning of the Queue or null if empty.</param>
            <returns>true if successfull, otherwise false.</returns>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Peek">
            <summary>
            Returns the object at the beginning of the queue without removing it.
            </summary>
            <returns>The object at the beginning of the queue.</returns>
            <remarks>
            This method is similar to the Dequeue method, but Peek does not modify the queue. 
            A null reference can be added to the Queue as a value. 
            To distinguish between a null value and the end of the queue, check the Count property or
            catch the InvalidOperationException, which is thrown when the Queue is empty.
            </remarks>
            <exception cref="T:System.InvalidOperationException">The queue is empty.</exception>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.TryPeek(System.Object@)">
            <summary>
            Returns the object at the beginning of the Queue without removing it.
            Similar to the Peek method, however this method will not throw exception if
            queue is empty, but instead will return false.
            </summary>
            <param name="value">The object at the beginning of the Queue or null if empty.</param>
            <returns>The object at the beginning of the Queue.</returns>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.Clear">
            <summary>
            Removes all objects from the Queue.
            </summary>
            <remarks>
            Count is set to zero. Size does not change.
            </remarks>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.CopyTo(System.Array,System.Int32)">
            <summary>
            Copies the Queue elements to an existing one-dimensional Array,
            starting at the specified array index.
            </summary>
            <param name="array">The one-dimensional Array that is the destination of the elements copied from Queue. The Array must have zero-based indexing.</param>
            <param name="index">The zero-based index in array at which copying begins. </param>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.BlockingQueue.GetEnumerator">
            <summary>
            GetEnumerator not implemented.  You can't enumerate the active queue
            as you would an array as it is dynamic with active gets and puts.  You could
            if you locked it first and unlocked after enumeration, but that does not
            work well for GetEnumerator.  The recommended method is to Get Values
            and enumerate the returned array copy.  That way the queue is locked for
            only a short time and a copy returned so that can be safely enumerated using
            the array's enumerator.  You could also create a custom enumerator that would
            dequeue the objects until empty queue, but that is a custom need. 
            </summary>
            <returns></returns>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.Values">
            <summary>
            Gets the object values currently in the queue.  If queue is empty, this
            will return a zero length array.  The returned array length can be
            0 to Size.  This method does not modify the queue, but returns a shallow copy
            of the queue buffer containing the objects contained in the queue.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.Hwm">
            <summary>
            Gets the highest queue utilisation so far.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.IsSynchronized">
            <summary>
            Gets a value indicating whether access to the Queue is synchronized (thread-safe).
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.Size">
            <summary>
            Returns the max elements allowed in the queue before blocking Enqueue
            operations.  This is the size set in the constructor.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.Count">
            <summary>
            Gets the number of elements contained in the Queue.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.ThirdParty.BlockingQueue.SyncRoot">
            <summary>
            Gets an object that can be used to synchronize access to the Queue.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.ThirdParty.QueueTimeoutException">
            <summary>
            Missing comment
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.ThirdParty.QueueTimeoutException.#ctor">
            <summary>
            Construct exception.
            </summary>
        </member>
    </members>
</doc>

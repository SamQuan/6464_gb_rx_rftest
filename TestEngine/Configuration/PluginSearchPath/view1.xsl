<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mstns="http://tempuri.org/PluginPathSchema.xsd" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:xdExtension="http://schemas.microsoft.com/office/infopath/2003/xslt/extension" xmlns:xdXDocument="http://schemas.microsoft.com/office/infopath/2003/xslt/xDocument" xmlns:xdSolution="http://schemas.microsoft.com/office/infopath/2003/xslt/solution" xmlns:xdFormatting="http://schemas.microsoft.com/office/infopath/2003/xslt/formatting" xmlns:xdImage="http://schemas.microsoft.com/office/infopath/2003/xslt/xImage" xmlns:xdUtil="http://schemas.microsoft.com/office/infopath/2003/xslt/Util" xmlns:xdMath="http://schemas.microsoft.com/office/infopath/2003/xslt/Math" xmlns:xdDate="http://schemas.microsoft.com/office/infopath/2003/xslt/Date" xmlns:sig="http://www.w3.org/2000/09/xmldsig#" xmlns:xdSignatureProperties="http://schemas.microsoft.com/office/infopath/2003/SignatureProperties">
	<xsl:output method="html" indent="no"/>
	<xsl:template match="mstns:PluginSearchPathConfig">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html"></meta>
				<style controlStyle="controlStyle">@media screen 			{ 			BODY{margin-left:21px;background-position:21px 0px;} 			} 		BODY{color:windowtext;background-color:window;layout-grid:none;} 		.xdListItem {display:inline-block;width:100%;vertical-align:text-top;} 		.xdListBox,.xdComboBox{margin:1px;} 		.xdInlinePicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) } 		.xdLinkedPicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) url(#default#urn::controls/Binder) } 		.xdSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdRepeatingSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdBehavior_Formatting {BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting);} 	 .xdBehavior_FormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting);} 	.xdExpressionBox{margin: 1px;padding:1px;word-wrap: break-word;text-overflow: ellipsis;overflow-x:hidden;}.xdBehavior_GhostedText,.xdBehavior_GhostedTextNoBUI{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#TextField) url(#default#GhostedText);}	.xdBehavior_GTFormatting{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_GTFormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_Boolean{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#BooleanHelper);}	.xdBehavior_Select{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#SelectHelper);}	.xdRepeatingTable{BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word;}.xdScrollableRegion{BEHAVIOR: url(#default#ScrollableRegion);} 		.xdMaster{BEHAVIOR: url(#default#MasterHelper);} 		.xdActiveX{margin:1px; BEHAVIOR: url(#default#ActiveX);} 		.xdFileAttachment{display:inline-block;margin:1px;BEHAVIOR:url(#default#urn::xdFileAttachment);} 		.xdPageBreak{display: none;}BODY{margin-right:21px;} 		.xdTextBoxRTL{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:right;} 		.xdRichTextBoxRTL{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:right;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTTextRTL{height:100%;width:100%;margin-left:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButtonRTL{margin-right:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);}.xdTextBox{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:left;} 		.xdRichTextBox{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:left;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTPicker{;display:inline;margin:1px;margin-bottom: 2px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;} 		.xdDTText{height:100%;width:100%;margin-right:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButton{margin-left:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);} 		.xdRepeatingTable TD {VERTICAL-ALIGN: top;}</style>
				<style tableEditor="TableStyleRulesID">TABLE.xdLayout TD {
	BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none
}
TABLE.msoUcTable TD {
	BORDER-RIGHT: 1pt solid; BORDER-TOP: 1pt solid; BORDER-LEFT: 1pt solid; BORDER-BOTTOM: 1pt solid
}
TABLE {
	BEHAVIOR: url (#default#urn::tables/NDTable)
}
</style>
				<style languageStyle="languageStyle">BODY {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
TABLE {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
SELECT {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-LEFT: 20px; FONT-WEIGHT: normal; FONT-SIZE: xx-small; BEHAVIOR: url(#default#xOptional); COLOR: #333333; FONT-STYLE: normal; FONT-FAMILY: Verdana; TEXT-DECORATION: none
}
.langFont {
	FONT-FAMILY: Verdana
}
.defaultInDocUI {
	FONT-SIZE: xx-small; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-RIGHT: 20px
}
</style>
			</head>
			<body>
				<div>
					<strong>
						<font color="#3366ff" size="4">Test Engine Plug-in Search Path Configuration:</font>
					</strong>
				</div>
				<div><xsl:apply-templates select="." mode="_1"/>
				</div>
				<div> </div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="mstns:PluginSearchPathConfig" mode="_1">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 2.25pt ridge; BORDER-TOP: #000000 2.25pt ridge; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 2.25pt ridge; WIDTH: 629px; BORDER-BOTTOM: #000000 2.25pt ridge; BACKGROUND-COLOR: #33cccc" align="left" xd:xctname="Section" xd:CtrlId="CTRL1" tabIndex="-1">
			<div><input class="xdBehavior_Boolean" title="" type="checkbox" tabIndex="0" xd:xctname="CheckBox" xd:CtrlId="CTRL2" xd:onValue="true" xd:offValue="false" xd:boundProp="xd:value" xd:binding="mstns:ConsideringFileDateForPluginLatestVersion">
					<xsl:attribute name="xd:value">
						<xsl:value-of select="mstns:ConsideringFileDateForPluginLatestVersion"/>
					</xsl:attribute>
					<xsl:if test="mstns:ConsideringFileDateForPluginLatestVersion=&quot;true&quot;">
						<xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
					</xsl:if>
				</input> Considering File Date For Plug-in Latest Version</div>
			<div><xsl:apply-templates select="mstns:ChassisSearchPathCollection" mode="_2"/>
			</div>
			<div><xsl:apply-templates select="mstns:InstrumentSearchPathCollection" mode="_3"/>
			</div>
			<div><xsl:apply-templates select="mstns:LimitReaderSearchPathCollection" mode="_4"/>
			</div>
			<div><xsl:apply-templates select="mstns:DataReaderSearchPathCollection" mode="_5"/>
			</div>
			<div><xsl:apply-templates select="mstns:DataWriterSearchPathCollection" mode="_6"/>
			</div>
			<div><xsl:apply-templates select="mstns:TestModuleSearchPathCollection" mode="_7"/>
			</div>
			<div><xsl:apply-templates select="mstns:TestProgramSearchPathCollection" mode="_8"/>
			</div>
			<div><xsl:apply-templates select="mstns:SecuritySearchPathCollection" mode="_9"/>
			</div>
			<div><xsl:apply-templates select="mstns:TestJigSearchPathCollection" mode="_10"/>
			</div>
			<div><xsl:apply-templates select="mstns:MESSearchPathCollection" mode="_11"/>
			</div>
			<div><xsl:apply-templates select="mstns:TestControlSearchPathCollection" mode="_12"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:ChassisSearchPathCollection" mode="_2">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL3" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL4">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td style="BORDER-RIGHT: #000000 1pt; BORDER-TOP: #000000 1pt; BORDER-LEFT: #000000 1pt">
								<div>
									<strong>Chassis Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:ChassisSearchPath">
							<tr style="MIN-HEIGHT: 26px">
								<td style="BORDER-RIGHT: #000000 1pt; BORDER-LEFT: #000000 1pt; BORDER-BOTTOM: #000000 1pt"><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL5" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="ChassisSearchPath_1" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:InstrumentSearchPathCollection" mode="_3">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL6" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL7">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Instrument Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:InstrumentSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL8" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="InstrumentSearchPath_2" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:LimitReaderSearchPathCollection" mode="_4">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL9" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL10">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Limit Reader Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:LimitReaderSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL11" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="LimitReaderSearchPath_3" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:DataReaderSearchPathCollection" mode="_5">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL12" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL13">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Data Reader Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:DataReaderSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL14" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="DataReaderSearchPath_4" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:DataWriterSearchPathCollection" mode="_6">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL15" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL16">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Data Writer Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:DataWriterSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL17" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="DataWriterSearchPath_5" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TestModuleSearchPathCollection" mode="_7">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL18" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL19">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Test Module Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:TestModuleSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL20" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="TestModuleSearchPath_6" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TestProgramSearchPathCollection" mode="_8">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL21" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL22">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Test Program Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:TestProgramSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL23" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="TestProgramSearchPath_7" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:SecuritySearchPathCollection" mode="_9">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL24" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL25">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Security Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:SecuritySearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL26" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="SecuritySearchPath_8" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TestJigSearchPathCollection" mode="_10">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL27" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL28">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Test Jig Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:TestJigSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL29" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="TestJigSearchPath_9" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:MESSearchPathCollection" mode="_11">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL30" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL31">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>MES Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:MESSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL32" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="MESSearchPath_10" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="mstns:TestControlSearchPathCollection" mode="_12">
		<div class="xdSection xdRepeating" title="" style="BORDER-RIGHT: #000000 1pt solid; BORDER-TOP: #000000 1pt solid; MARGIN-BOTTOM: 6px; BORDER-LEFT: #000000 1pt solid; WIDTH: 100%; BORDER-BOTTOM: #000000 1pt solid" align="left" xd:xctname="Section" xd:CtrlId="CTRL33" tabIndex="-1">
			<div>
				<table class="xdRepeatingTable msoUcTable" title="" style="TABLE-LAYOUT: fixed; WIDTH: 610px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word; BORDER-BOTTOM-STYLE: none" border="1" xd:CtrlId="CTRL34">
					<colgroup>
						<col style="WIDTH: 610px"></col>
					</colgroup>
					<tbody class="xdTableHeader">
						<tr>
							<td>
								<div>
									<strong>Test Control Search Path:</strong>
								</div>
							</td>
						</tr>
					</tbody><tbody xd:xctname="RepeatingTable">
						<xsl:for-each select="mstns:TestControlSearchPath">
							<tr>
								<td><span class="xdTextBox" hideFocus="1" title="" tabIndex="0" xd:xctname="PlainText" xd:CtrlId="CTRL35" xd:binding="mstns:SearchPathElement" style="WIDTH: 100%">
										<xsl:value-of select="mstns:SearchPathElement"/>
									</span>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
				<div class="optionalPlaceholder" xd:xmlToEdit="TestControlSearchPath_11" tabIndex="0" xd:action="xCollection::insert" style="WIDTH: 610px">Insert item</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>

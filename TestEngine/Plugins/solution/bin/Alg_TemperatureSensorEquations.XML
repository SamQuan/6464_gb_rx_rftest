<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Alg_TemperatureSensorEquations</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.Algorithms.SteinhartHart">
            <summary>
            This class contains static methods for implementing conversion 
            of Thermistor Resistance to Temperature and Vice versa. 
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.SteinhartHart.TemperatureCelsiusToResistanceOhms(System.Double,System.Double,System.Double,System.Double)">
            <summary>
            Convert a temperature in Degrees Celsius to a Thermistor Resistance (in ohms)
            using the Steinhart-Hart equation, stated in terms of resistance:
            R = e^(((-X/2 + Z)^1/3 + ((-X/2 - Z)^1/3) 
            
            where X =  (A - 1/T)/C, Z = Sqrt((X^2)/4 + (Y^3)/27)), and Y = B/C.
            
            Note that T in the above equation is in Kelvins.
            </summary>
            <param name="temperature">The Temperature in Celsius.</param>
            <param name="a">Steinhart-Hart Constant A</param>
            <param name="b">Steinhart-Hart Constant B</param>
            <param name="c">Steinhart-Hart Constant C</param>
            <returns>Calculated Resistance (ohms)</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.SteinhartHart.ResistanceOhmsToTemperatureCelsius(System.Double,System.Double,System.Double,System.Double)">
            <summary>
            Convert a thermistor resistnce Value in ohms into a Temperature in 
            Degrees Celsius, using the Steinhart-Hart equation: 1/T = A + B.lnR + C.(lnR)^3
            
            Note that T in the above equation is in Kelvins.
            </summary>
            <param name="resistance">Resistance (ohms) to be converted.</param>
            <param name="a">Steinhart-Hart Constant A</param>
            <param name="b">Steinhart-Hart Constant B</param>
            <param name="c">Steinhart-Hart Constant C</param>
            <returns>The Temperature in Celsius.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.SteinhartHart.CalcCubeRoot(System.Double)">
            <summary>
            Calculates the Cube root according to the sign of the input.
            </summary>
            <param name="x">Number to take the Cube root of</param>
            <returns>The result.</returns>
        </member>
        <member name="T:Bookham.TestLibrary.Algorithms.CallendarVanDusen">
            <summary>
            This class contains static methods for implementing conversion 
            of Platinum RTD Sensor Resistance to Temperature and Vice versa. 
            
            See http://content.honeywell.com/sensing/prodinfo/temperature/technical/c15_136.pdf 
            for details of the Callendar-Van Dusen equation used in this class.
            
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.CallendarVanDusen.TemperatureCelsiusToResistanceOhms(System.Double,System.Double,System.Double,System.Double,System.Double)">
            <summary>
            Convert a temperature in Degrees Celsius to a RTD Sensor Resistance (in ohms)
            using the Callendar-Van Dusen  equation.
            
            
            This is stated in terms of resistance:
            R = R0(1 + AT + BT^2 - 100CT^3 + CT^4)
            
            where R0 = Resistance at 0 degrees Celsius (ohms)
                  A = alpha + (alpha * delta)/100
                  B = (-alpha * delta)/100^2
                  C (if  T less than 0)  = (-alpha * beta)/100^4, else C = 0
            
                  and R0, alpha, beta and delta are Constants.
            
            </summary>
            <param name="temperature">The Temperature in Celsius.</param>
            <param name="r0">Callendar-Van Dusen Constant R0</param>
            <param name="alpha">Callendar-Van Dusen Constant alpha</param>
            <param name="beta">Callendar-Van Dusen Constant beta. Only used when Temperature less than 0.</param>
            <param name="delta">Callendar-Van Dusen Constant delta</param>
            <returns>Calculated Resistance (ohms)</returns>        
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.CallendarVanDusen.ResistanceOhmsToTemperatureCelsius(System.Double,System.Double,System.Double,System.Double,System.Double)">
            <summary>
            WARNING!!! this algorithm can only be used where Temperature > 0.
            
            Convert a RTD resistance Value in ohms into a Temperature in 
            Degrees Celsius, using the Callendar-Van Dusen equation: 
            T = ((-R0 * A) + sqrt ((R0^2 * A^2) - (4*R0*B *(R0 - RT ))))/(2* R0 * B)
            
            where RT = resistance at required temperature (ohms)
                  R0 = Resistance of sensor at 0 degrees Celsius (ohms)
                  A = alpha + (alpha * delta)/100
                  B = (-alpha * delta)/100^2
            
                  and alpha, beta and delta are Constants.
            
            </summary>
            <param name="resistance">Resistance (ohms) to be converted.</param>
            <param name="r0">Callendar-Van Dusen Constant R0</param>
            <param name="alpha">Callendar-Van Dusen Constant alpha</param>
            <param name="beta">Callendar-Van Dusen Constant beta</param>
            <param name="delta">Callendar-Van Dusen Constant delta</param>
            <returns>The Temperature in Celsius.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.CallendarVanDusen.calculateConstantA(System.Double,System.Double)">
            <summary>
            Calculates the "A" constant for the callendar-Van Dusen Equation using the 
            following formula:
            A = alpha + (alpha * delta)/100
            </summary>
            <param name="alpha">alpha</param>
            <param name="delta">delta</param>
            <returns>A -  the constant</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Algorithms.CallendarVanDusen.calculateConstantB(System.Double,System.Double)">
            <summary>
            Calculates the "B" constant for the callendar-Van Dusen Equation using the 
            following formula:
            B = (-alpha * delta)/100^2
            </summary>
            <param name="alpha">alpha</param>
            <param name="delta">delta</param>
            <returns>B -  the constant</returns>
        </member>
    </members>
</doc>

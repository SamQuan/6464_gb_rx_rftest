using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.ConherenRx;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_Product_GroupB_CoherentRxInfo
    {

        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        internal Specification MainSpec;

        internal CoheRxGBFinalTestConds TestConditions;

        internal Prog_Product_GroupB_CoherentRxInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        internal TestSelection WavelengthTestSelect;

        internal TestSelection ParamTestSelect;
    }
}

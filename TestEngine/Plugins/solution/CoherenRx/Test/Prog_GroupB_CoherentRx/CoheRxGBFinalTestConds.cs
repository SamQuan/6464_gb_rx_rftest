using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestSolution.TestPrograms
{

    internal class CoheRxGBFinalTestConds
    {

        internal CoheRxGBFinalTestConds(Specification spec)
        {
            // get test conditions
            string[] testCondNames = new string[]
            {
                "TC_BW_SET",
                "TC_DLP_SET",
                "TC_GD_SET",
                "TC_INPUT_POWER_LOCAL",
                "TC_INPUT_POWER_SIGNAL",
                "TC_INPUT_POWER_TOL",
                "TC_PD_REVERSE_BIAS",
                "TC_S22_SET",
                "TC_SPRR_SET",
                "TC_TEMPERATURE",
                "TC_TIA_VCC",
                "TC_TIA_VGC_XI",
                "TC_TIA_VGC_YI",
                "TC_TIA_VGC_XQ",
                "TC_TIA_VGC_YQ",
                "TC_TIA_VMC_X",
                "TC_TIA_VMC_Y",
                "TC_WAVELENGTH_1",
                //"TC_WAVELENGTH_2",
                "TC_WAVELENGTH_2",
                "TC_TIA_VGC_1",
                "TC_TIA_VGC_2",
                "TC_TIA_VGC_3",
                "TC_TIA_VGC_4",
                "TC_TIA_VGC_5",
                "TC_BW_SET_M4",
                "TC_BW_SET_P4",
                //"TC_VGC_CH_1",
                //"TC_VGC_CH_2",
                //"TC_BW_CH_1",
                //"TC_BW_CH_2",
        };
            DatumList testConds = SpecValuesToDatumList.GetTestConditions(spec, testCondNames);

            foreach (Datum d in testConds)
            {
                string datumName = d.Name;
                switch (datumName)
                {
                    case "TC_BW_SET":
                        this.BandwidthSetting = ((DatumDouble)d).Value;
                        break;
                    case "TC_DLP_SET":
                        this.DlpSetting_deg = ((DatumDouble)d).Value;
                        break;
                    case "TC_GD_SET":
                        this.GroupDelaySetting_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_INPUT_POWER_LOCAL":
                        this.zeroDBRefPowerLoc_dBm = ((DatumDouble)d).Value;
                        break;
                    case "TC_INPUT_POWER_SIGNAL":
                        this.zeroDBRefPowerSig_dBm = ((DatumDouble)d).Value;
                        break;
                    case "TC_INPUT_POWER_TOL":
                        this.InputPowerTolerance_dB = ((DatumDouble)d).Value;
                        break;
                    case "TC_PD_REVERSE_BIAS":
                        this.PdReverseBias_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_S22_SET":
                        this.S22Setting_dB = ((DatumDouble)d).Value;
                        break;
                    case "TC_SPRR_SET":
                        this.SprrSetting_GHz = ((DatumDouble)d).Value;
                        break;
                    case "TC_TEMPERATURE":
                        this.MidTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_TEMPERATURE_HIGH":
                        this.HighTemp = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VCC":
                        this.VccTia_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_XI":
                        this.VgcTiaXI_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_XQ":
                        this.VgcTiaXQ_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_YI":
                        this.VgcTiaYI_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_YQ":
                        this.VgcTiaYQ_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VMC_X":
                        this.VmcTiaX_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VMC_Y":
                        this.VmcTiaY_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_WAVELENGTH_1":
                        this.LaserWavelength1_nm = ((DatumDouble)d).Value;
                        break;
                    //case "TC_WAVELENGTH_2":
                    //    this.LaserWavelength2_nm = ((DatumDouble)d).Value;
                    //    break;
                    case "TC_WAVELENGTH_2":
                        this.LaserWavelength2_nm = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_1":
                        this.VgcSweepVolt1_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_2":
                        this.VgcSweepVolt2_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_3":
                        this.VgcSweepVolt3_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_4":
                        this.VgcSweepVolt4_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_TIA_VGC_5":
                        this.VgcSweepVolt5_V = ((DatumDouble)d).Value;
                        break;
                    case "TC_BW_SET_M4":
                        this.bwSettingM4 = int.Parse(d.ValueToString());
                        break;
                    case "TC_BW_SET_P4":
                        this.bwSwttingP4 = int.Parse(d.ValueToString());
                        break;
                }

            }
        }

        internal double BandwidthSetting;
        internal double DlpSetting_deg;
        internal double GroupDelaySetting_GHz;
        internal double zeroDBRefPowerLoc_dBm;
        internal double zeroDBRefPowerSig_dBm;
        internal double InputPowerTolerance_dB;
        internal double PdReverseBias_V;
        internal double S22Setting_dB;
        internal double SprrSetting_GHz;
        internal double MidTemp;
        internal double HighTemp;
        internal double VccTia_V;
        internal double VgcTiaXI_V;
        internal double VgcTiaXQ_V;
        internal double VgcTiaYI_V;
        internal double VgcTiaYQ_V;
        internal double VmcTiaX_V;
        internal double VmcTiaY_V;
        internal double LaserWavelength1_nm;
        //internal double LaserWavelength2_nm;
        internal double LaserWavelength2_nm;
        internal double VgcSweepVolt1_V;
        internal double VgcSweepVolt2_V;
        internal double VgcSweepVolt3_V;
        internal double VgcSweepVolt4_V;
        internal double VgcSweepVolt5_V;
        internal int bwSettingM4;
        internal int bwSwttingP4;



        

    }
}

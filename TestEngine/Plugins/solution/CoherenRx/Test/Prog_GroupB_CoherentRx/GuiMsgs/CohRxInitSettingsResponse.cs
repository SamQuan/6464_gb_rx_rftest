using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsgs
{
    internal class CohRxInitSettingsResponse
    {
        internal CohRxInitSettingsResponse(string filename)
        {
            this.Filename = filename;
        }

        internal readonly string Filename;
    }
}

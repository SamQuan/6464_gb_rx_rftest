// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// MzDcControl.Gui/LoadSnDialogueCtl
// 
// Author: tommy.yu
// Design: TODO

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// 
    /// </summary>
    partial class LoadSnDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadSnDialogueCtl));
            this.SerialNumberFrame = new System.Windows.Forms.GroupBox();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.StartTestButton = new System.Windows.Forms.Button();
            this.DeviceTypeFrame = new System.Windows.Forms.GroupBox();
            this.DeviceTypeCombo = new System.Windows.Forms.ComboBox();
            this.TestStageFrame = new System.Windows.Forms.GroupBox();
            this.TestStageCombo = new System.Windows.Forms.ComboBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.abortButton = new System.Windows.Forms.Button();
            this.retryAbortLabel = new System.Windows.Forms.Label();
            this.SerialNumberFrame.SuspendLayout();
            this.DeviceTypeFrame.SuspendLayout();
            this.TestStageFrame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SerialNumberFrame
            // 
            this.SerialNumberFrame.Controls.Add(this.loadBatchTextBox);
            this.SerialNumberFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialNumberFrame.Location = new System.Drawing.Point(9, 63);
            this.SerialNumberFrame.Name = "SerialNumberFrame";
            this.SerialNumberFrame.Size = new System.Drawing.Size(480, 58);
            this.SerialNumberFrame.TabIndex = 0;
            this.SerialNumberFrame.TabStop = false;
            this.SerialNumberFrame.Text = "Serial Number";
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(11, 24);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(459, 26);
            this.loadBatchTextBox.TabIndex = 0;
            this.loadBatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loadBatchTextBox_KeyPress);
            // 
            // StartTestButton
            // 
            this.StartTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartTestButton.Location = new System.Drawing.Point(516, 82);
            this.StartTestButton.Name = "StartTestButton";
            this.StartTestButton.Size = new System.Drawing.Size(104, 36);
            this.StartTestButton.TabIndex = 3;
            this.StartTestButton.Text = "Start";
            this.StartTestButton.Click += new System.EventHandler(this.StartTestButton_Click);
            // 
            // DeviceTypeFrame
            // 
            this.DeviceTypeFrame.Controls.Add(this.DeviceTypeCombo);
            this.DeviceTypeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceTypeFrame.Location = new System.Drawing.Point(9, 136);
            this.DeviceTypeFrame.Name = "DeviceTypeFrame";
            this.DeviceTypeFrame.Size = new System.Drawing.Size(214, 54);
            this.DeviceTypeFrame.TabIndex = 1;
            this.DeviceTypeFrame.TabStop = false;
            this.DeviceTypeFrame.Text = "Device Type";
            // 
            // DeviceTypeCombo
            // 
            this.DeviceTypeCombo.FormattingEnabled = true;
            this.DeviceTypeCombo.Location = new System.Drawing.Point(11, 22);
            this.DeviceTypeCombo.Name = "DeviceTypeCombo";
            this.DeviceTypeCombo.Size = new System.Drawing.Size(189, 24);
            this.DeviceTypeCombo.TabIndex = 0;
            this.DeviceTypeCombo.Leave += new System.EventHandler(this.DeviceTypeCombo_Leave);
            this.DeviceTypeCombo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DeviceTypeCombo_KeyUp);
            // 
            // TestStageFrame
            // 
            this.TestStageFrame.Controls.Add(this.TestStageCombo);
            this.TestStageFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestStageFrame.Location = new System.Drawing.Point(275, 136);
            this.TestStageFrame.Name = "TestStageFrame";
            this.TestStageFrame.Size = new System.Drawing.Size(214, 54);
            this.TestStageFrame.TabIndex = 2;
            this.TestStageFrame.TabStop = false;
            this.TestStageFrame.Text = "Test Stage";
            // 
            // TestStageCombo
            // 
            this.TestStageCombo.FormattingEnabled = true;
            this.TestStageCombo.Location = new System.Drawing.Point(12, 22);
            this.TestStageCombo.Name = "TestStageCombo";
            this.TestStageCombo.Size = new System.Drawing.Size(189, 24);
            this.TestStageCombo.TabIndex = 0;
            this.TestStageCombo.Leave += new System.EventHandler(this.TestStageCombo_Leave);
            this.TestStageCombo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TestStageCombo_KeyUp);
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(20, 237);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(448, 23);
            this.messageLabel.TabIndex = 5;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.AutoSize = true;
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(145, 19);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(556, 20);
            this.loadBatchScreenLabel.TabIndex = 6;
            this.loadBatchScreenLabel.Text = "Test Engine TestContol  - CohRx Goldbox: Offline MES Screen";
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(9, 7);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bookhamLogoPictureBox.TabIndex = 12;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // abortButton
            // 
            this.abortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abortButton.Location = new System.Drawing.Point(516, 146);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(104, 36);
            this.abortButton.TabIndex = 4;
            this.abortButton.Text = "Abort";
            this.abortButton.Click += new System.EventHandler(this.abortButton_Click);
            // 
            // retryAbortLabel
            // 
            this.retryAbortLabel.AutoSize = true;
            this.retryAbortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryAbortLabel.ForeColor = System.Drawing.Color.Red;
            this.retryAbortLabel.Location = new System.Drawing.Point(178, 46);
            this.retryAbortLabel.Name = "retryAbortLabel";
            this.retryAbortLabel.Size = new System.Drawing.Size(491, 20);
            this.retryAbortLabel.TabIndex = 7;
            this.retryAbortLabel.Text = "Load Batch operation failed. Please decide what to do next: ";
            // 
            // LoadSnDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SerialNumberFrame);
            this.Controls.Add(this.StartTestButton);
            this.Controls.Add(this.DeviceTypeFrame);
            this.Controls.Add(this.TestStageFrame);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.retryAbortLabel);
            this.Name = "LoadSnDialogueCtl";
            this.Size = new System.Drawing.Size(717, 280);
            this.Load += new System.EventHandler(this.LoadSnDialogueCtl_Load);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadSnDialogueCtl_MsgReceived);
            this.SerialNumberFrame.ResumeLayout(false);
            this.SerialNumberFrame.PerformLayout();
            this.DeviceTypeFrame.ResumeLayout(false);
            this.TestStageFrame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox SerialNumberFrame;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.Button StartTestButton;
        private System.Windows.Forms.GroupBox DeviceTypeFrame;
        private System.Windows.Forms.ComboBox DeviceTypeCombo;
        private System.Windows.Forms.GroupBox TestStageFrame;
        private System.Windows.Forms.ComboBox TestStageCombo;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Button abortButton;
        private System.Windows.Forms.Label retryAbortLabel;

    }
}

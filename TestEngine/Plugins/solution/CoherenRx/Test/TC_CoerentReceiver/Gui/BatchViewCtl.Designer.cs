// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// BatchViewCtl.Designer.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    partial class BatchViewCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchViewCtl));
            this.markForRetestButton = new System.Windows.Forms.Button();
            this.legendPictureBox = new System.Windows.Forms.PictureBox();
            this.testDeviceButton = new System.Windows.Forms.Button();
            this.endBatchButton = new System.Windows.Forms.Button();
            this.screenTitleLabel = new System.Windows.Forms.Label();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.devicesListBox = new System.Windows.Forms.ListBox();
            this.messagesLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.legendPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // markForRetestButton
            // 
            this.markForRetestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markForRetestButton.Location = new System.Drawing.Point(566, 136);
            this.markForRetestButton.Name = "markForRetestButton";
            this.markForRetestButton.Size = new System.Drawing.Size(144, 56);
            this.markForRetestButton.TabIndex = 2;
            this.markForRetestButton.Text = "Mark Device For Retest";
            this.markForRetestButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.markForRetestButton.Click += new System.EventHandler(this.markForRetestButton_Click);
            // 
            // legendPictureBox
            // 
            this.legendPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("legendPictureBox.Image")));
            this.legendPictureBox.Location = new System.Drawing.Point(726, 52);
            this.legendPictureBox.Name = "legendPictureBox";
            this.legendPictureBox.Size = new System.Drawing.Size(115, 112);
            this.legendPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.legendPictureBox.TabIndex = 16;
            this.legendPictureBox.TabStop = false;
            // 
            // testDeviceButton
            // 
            this.testDeviceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testDeviceButton.Location = new System.Drawing.Point(566, 52);
            this.testDeviceButton.Name = "testDeviceButton";
            this.testDeviceButton.Size = new System.Drawing.Size(144, 56);
            this.testDeviceButton.TabIndex = 1;
            this.testDeviceButton.Text = "Test Device";
            this.testDeviceButton.Click += new System.EventHandler(this.testDeviceButton_Click);
            // 
            // endBatchButton
            // 
            this.endBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endBatchButton.Location = new System.Drawing.Point(566, 220);
            this.endBatchButton.Name = "endBatchButton";
            this.endBatchButton.Size = new System.Drawing.Size(144, 56);
            this.endBatchButton.TabIndex = 3;
            this.endBatchButton.Text = "End Batch";
            this.endBatchButton.Click += new System.EventHandler(this.endBatchButton_Click);
            // 
            // screenTitleLabel
            // 
            this.screenTitleLabel.AutoSize = true;
            this.screenTitleLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.screenTitleLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.screenTitleLabel.Location = new System.Drawing.Point(150, 15);
            this.screenTitleLabel.Name = "screenTitleLabel";
            this.screenTitleLabel.Size = new System.Drawing.Size(323, 22);
            this.screenTitleLabel.TabIndex = 5;
            this.screenTitleLabel.Text = "Test Engine Batch View  - coherRx";
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.Location = new System.Drawing.Point(6, 4);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.logoPictureBox.TabIndex = 12;
            this.logoPictureBox.TabStop = false;
            // 
            // devicesListBox
            // 
            this.devicesListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devicesListBox.ItemHeight = 20;
            this.devicesListBox.Location = new System.Drawing.Point(14, 52);
            this.devicesListBox.Name = "devicesListBox";
            this.devicesListBox.Size = new System.Drawing.Size(544, 224);
            this.devicesListBox.TabIndex = 0;
            this.devicesListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.devicesListBox_DrawItem);
            // 
            // messagesLabel
            // 
            this.messagesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagesLabel.Location = new System.Drawing.Point(14, 244);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Size = new System.Drawing.Size(544, 16);
            this.messagesLabel.TabIndex = 4;
            // 
            // BatchViewCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.markForRetestButton);
            this.Controls.Add(this.legendPictureBox);
            this.Controls.Add(this.testDeviceButton);
            this.Controls.Add(this.endBatchButton);
            this.Controls.Add(this.screenTitleLabel);
            this.Controls.Add(this.logoPictureBox);
            this.Controls.Add(this.devicesListBox);
            this.Controls.Add(this.messagesLabel);
            this.Name = "BatchViewCtl";
            this.Size = new System.Drawing.Size(856, 307);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.BatchViewCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.legendPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button markForRetestButton;
        private System.Windows.Forms.PictureBox legendPictureBox;
        private System.Windows.Forms.Button testDeviceButton;
        private System.Windows.Forms.Button endBatchButton;
        private System.Windows.Forms.Label screenTitleLabel;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.ListBox devicesListBox;
        private System.Windows.Forms.Label messagesLabel;


    }
}

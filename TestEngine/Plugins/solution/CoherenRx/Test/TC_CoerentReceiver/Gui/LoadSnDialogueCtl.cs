// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// MzDcControl.Gui/LoadSnDialogueCtl.cs
// 
// Author: tommy.yu
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
//using Bookham.TestSolution.TestModules;


namespace Bookham.TestSolution.TestControl
{
    public partial class LoadSnDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// 
        /// </summary>
        public LoadSnDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
            //Hide the retry/abort dialogue
            hideRetryAbortControls();

            this.loadBatchTextBox.CharacterCasing = CharacterCasing.Upper;
        }

        private void LoadSnDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType().Equals(typeof(DatumStringArray)))
            {
                DatumStringArray sa = (DatumStringArray)payload;
                switch (sa.Name)
                {
                    case "stageList":
                        TestStageCombo.DataSource = sa.ValueCopy();
                        loadBatchTextBox.Focus();
                        break;
                    case "typeList":
                        DeviceTypeCombo.DataSource = sa.ValueCopy();
                        break;
                }
            }
            else if (payload.GetType().Equals(typeof(DatumString)))
            {
                DatumString ds = (DatumString)payload;
                switch (ds.Name)
                {
                    case "RetryAbortRequest":
                        // Disable the Load Batch Controls
                        disableLoadBatchControls();

                        //Show the Abort/Retry Controls
                        showRetryAbortControls();

                        // Display the error message.
                        this.messageLabel.Text = ds.Value;
                        break;
                    default:
                        break;
                }
            }
            else if (payload is string)
            {
                messageLabel.Text = payload.ToString();
            }
            else
            {
                //Error - invalid message.
                System.Diagnostics.Trace.Write("Warning unknown messaage type " + payload.GetType());
            }

        }

        /// <summary>
        /// The User clicked the load batch button. Send a message to the worker thread, with the 
        /// user's desired Batch ID.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void StartTestButton_Click(object sender, System.EventArgs e)
        {
            // Do not allow empty BatchIDs.
            loadBatchTextBox.Text = loadBatchTextBox.Text.Trim();
            if (this.loadBatchTextBox.TextLength != 0)
            {
                string serinalNumber = loadBatchTextBox.Text.Trim();
                string filePath = @"\Configuration\CanTestSerialNumber.csv";

                filePath = System.IO.Directory.GetCurrentDirectory() + filePath;

                List<string[]> listSNCanBeTest = new List<string[]>();
                using (CsvReader cr=new CsvReader ())
                {
                    listSNCanBeTest = cr.ReadFile(filePath);
                }

                bool SnCanBeTest = false;
                foreach (string[] item in listSNCanBeTest)
                {
                    if (loadBatchTextBox.Text==item[0].Trim())
                    {
                        SnCanBeTest = true;
                        break;
                    }
                }

                //if (!SnCanBeTest)
                //{
                //    MessageBox.Show("这是实验程序，这个序列号的器件不允许测试！");

                //    return;
                //}


                // Disable the button
                this.StartTestButton.Enabled = false;
                // mfx this.buttonManualFixIt.Enabled = false; // and the manual-fixit button 

                DatumList details = new DatumList();
                Datum serialNumber = new DatumString("SerialNumber", loadBatchTextBox.Text);
                Datum deviceType = new DatumString("DeviceType", DeviceTypeCombo.Text);
                Datum testStage = new DatumString("TestStage", TestStageCombo.Text);
                details.Add(serialNumber);
                details.Add(deviceType);
                details.Add(testStage);

                this.sendToWorker(details);
                //MessageClass.SendToWorker(details);
            }
        }

        /// <summary>
        /// Dispay normal load batch screen controls.
        /// </summary>
        private void displayControls()
        {
            this.loadBatchScreenLabel.Show();
            this.bookhamLogoPictureBox.Show();
            showLoadBatchControls();
            hideRetryAbortControls();
        }

        /// <summary>
        /// Hide All  Controls
        /// </summary>
        private void hideControls()
        {
            this.loadBatchScreenLabel.Hide();
            this.bookhamLogoPictureBox.Hide();
            hideLoadBatchControls();
            hideRetryAbortControls();

        }

        /// <summary>
        /// Hide Load Batch operation associated controls.
        /// </summary>
        private void hideLoadBatchControls()
        {
            this.loadBatchTextBox.Hide();
            this.StartTestButton.Hide();
            this.messageLabel.Hide();
        }

        /// <summary>
        /// Show Load Batch operation associated controls.
        /// </summary>
        private void showLoadBatchControls()
        {
            this.loadBatchTextBox.Show();
            this.StartTestButton.Show();
            this.messageLabel.Show();
        }

        /// <summary>
        /// Disable Load Batch operation associated controls.
        /// </summary>
        private void disableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = false;
            this.StartTestButton.Enabled = false;
        }

        /// <summary>
        /// Enable Load Batch operation associated controls.
        /// </summary>
        private void enableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = true;
            this.StartTestButton.Enabled = true;
        }

        /// <summary>
        /// Hide controls associated with abort/retry query.
        /// </summary>
        private void hideRetryAbortControls()
        {
            this.abortButton.Hide();
            this.retryAbortLabel.Hide();
        }

        /// <summary>
        /// Show controls associated with abort/retry query.
        /// </summary>
        private void showRetryAbortControls()
        {
            this.abortButton.Show();
            this.retryAbortLabel.Show();
        }

        /// <summary>
        /// User clicks the retry button. Send a retry message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void retryButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            this.sendToWorker(QueryRetryAbort.Retry);
            //MessageClass.SendToWorker(QueryRetryAbort.Retry);

            //Hide the Retry/Abort Controls.
            hideRetryAbortControls();

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";
        }

        /// <summary>
        /// User clicks the abort button. Send a abort message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void abortButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            this.sendToWorker(QueryRetryAbort.Abort);
            //MessageClass.SendToWorker(QueryRetryAbort.Abort);

            //Hide the Retry/Abort Controls.
            hideRetryAbortControls();

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";

            // Prompt the user
            this.loadBatchTextBox.Text = "";

            this.StartTestButton.Enabled = true;
        }

        private void loadBatchTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!string.IsNullOrEmpty(loadBatchTextBox.Text.Trim()) && e.KeyChar=='\r')
                if (!StartTestButton.Focused) StartTestButton.Focus();
        }

        private void LoadSnDialogueCtl_Load(object sender, EventArgs e)
        {
            DeviceTypeCombo.DropDownStyle = ComboBoxStyle.DropDown;
            TestStageCombo.DropDownStyle = ComboBoxStyle.DropDown;
        }

        private void DeviceTypeCombo_KeyUp(object sender, KeyEventArgs e)
        {
            Util_AutoSearch.AutoSearchKeyUp((ComboBox)sender, e);
        }

        private void DeviceTypeCombo_Leave(object sender, EventArgs e)
        {
            Util_AutoSearch.AutoSearchLeave((ComboBox)sender);
        }

        private void TestStageCombo_KeyUp(object sender, KeyEventArgs e)
        {
            Util_AutoSearch.AutoSearchKeyUp((ComboBox)sender, e);
        }

        private void TestStageCombo_Leave(object sender, EventArgs e)
        {
            Util_AutoSearch.AutoSearchLeave((ComboBox)sender);
        }
   
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// BatchViewCtl.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestControl
{
    public partial class BatchViewCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        // Internal store of Test Statuses for the current batch.
        TestStatus[] testStatuses;

        /// <summary>
        /// Constructor
        /// </summary>
        public BatchViewCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
            // Set the DrawMode property to draw fixed sized items.
            devicesListBox.DrawMode = DrawMode.OwnerDrawFixed;

            // Disable all the buttons.
            disableButtons();
        }

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="payload">Message payload</param>
        /// <param name="inMsgSeq">Input message sequence number.</param>
        /// <param name="respSeq">Outgoing message sequence number.</param>
        private void BatchViewCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload is string)
            {
                string message = (string)payload;
                // Just print a message to screen.
                this.messagesLabel.Text = message;
            }
            else if (payload.GetType().Equals(typeof(GuiMsgEnum)))
            {
                GuiMsgEnum gm=(GuiMsgEnum)payload;
                // Ensure that the buttons are enabled, Test Control Plug-in wants another DUT.
                if (gm == GuiMsgEnum.SelectNextDUTRequest)
                    enableButtons();
            }
            else if (payload is DUTObject)
            {
                // The Test Control Plug-in is adding a new device to the batch view display.
                DUTObject dut = (DUTObject)payload;
                this.devicesListBox.Items.Add(dut.SerialNumber);
            }
            else if (payload is TestStatus[])
            {
                // Refresh the Batch View Test Statuses.
                TestStatus[] updateMsg = (TestStatus[])payload;
                this.testStatuses = new TestStatus[updateMsg.Length];
                Array.Copy(updateMsg, 0, this.testStatuses, 0, updateMsg.Length);

                // Enable the GUI buttons. Ready to carry out next operation at this point.
                enableButtons();
            }
            else
            {
                //Invalid message.
            }

        }

        /// <summary>
        /// Update the Devices listbox display colours.
        /// </summary>
        /// <param name="sender">The system</param>
        /// <param name="e">event parameters.</param>
        private void devicesListBox_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            // Set the DrawMode property to draw fixed sized items.
            devicesListBox.DrawMode = DrawMode.OwnerDrawFixed;


            if (this.devicesListBox.Items.Count != 0)
            {
                int index = e.Index;
                TestStatus status = this.testStatuses[index];

                int penWidth = 1;

                // Create a new Brush and initialize to a Black colored brush by default.
                Brush foreBrush = Brushes.Black;
                Brush backBrush = new SolidBrush(e.BackColor);
                Pen highlightPen = new Pen(SystemColors.Highlight, penWidth);

                // Determine the color of the brush to draw each item based on the index and Test Status of the item to draw.
                if (status == TestStatus.Failed)
                {
                    // Display red if failed.
                    foreBrush = Brushes.White;
                    backBrush = Brushes.Red;
                    highlightPen = new Pen(Color.Yellow, penWidth);
                }
                else if (status == TestStatus.Passed)
                {
                    // Display green if failed.
                    foreBrush = Brushes.White;
                    backBrush = Brushes.Green;
                    highlightPen = new Pen(Color.Yellow, penWidth);
                }
                else if (status == TestStatus.MarkForRetest)
                {
                    // Display Purple if marked for retest.
                    foreBrush = Brushes.Purple;
                    backBrush = Brushes.Yellow;
                    // highlightPen as before
                }

                // Draw the background of the ListBox control for each item.
                e.Graphics.FillRectangle(backBrush, e.Bounds);

                // Draw the current item text based on the current Font and the custom brush settings.
                e.Graphics.DrawString(devicesListBox.Items[index].ToString(), e.Font, foreBrush, e.Bounds, StringFormat.GenericDefault);

                if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                {
                    e.Graphics.DrawRectangle(highlightPen, e.Bounds.Left + penWidth,
                        e.Bounds.Top + penWidth,
                        e.Bounds.Width - 2 - penWidth,
                        e.Bounds.Height - 2 - penWidth);
                }

                //				// If the ListBox has focus, draw a focus rectangle around the selected item.
                //				e.DrawFocusRectangle();
            }

        }

        /// <summary>
        /// The user has clicked the Test Device Button. Send a message with the serial number of the 
        ///  device highlighted in the devices list box to the Test Control plug-in.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters</param>
        private void testDeviceButton_Click(object sender, System.EventArgs e)
        {
            if (this.devicesListBox.SelectedItem != null)
            {
                // Send the message.
                DatumString response = new DatumString("NextSN",devicesListBox.SelectedItem.ToString());
                this.sendToWorker(response);
                //MessageClass.SendToWorker(response);

                // Disable the GUI buttons. They will be re-enabled when 
                // it is time to choose the next device to test.
                disableButtons();
            }
        }

        /// <summary>
        /// The user has clicked the End Batch button. Send a message to the Test Control Plug-in.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void endBatchButton_Click(object sender, System.EventArgs e)
        {
            // Send the message.
            this.sendToWorker(GuiMsgEnum.EndOfBatchNotification);
            //MessageClass.SendToWorker(GuiMsgEnum.EndOfBatchNotification);

            //Disable the GUI Buttons. The will be re-enabled when it is time to select another device to test.
            disableButtons();
        }


        /// <summary>
        /// The User has decided to mark a device for retest. Update the Display and 
        /// send a message to the Test Control Plug-in to inform it of the device marked.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters.</param>
        private void markForRetestButton_Click(object sender, System.EventArgs e)
        {
            if (this.devicesListBox.SelectedItem != null)
            {
                // Only mark the device if it hasn't already passed it's tests or is untested.
                if ((testStatuses[this.devicesListBox.SelectedIndex] != TestStatus.Untested) &&
                    (testStatuses[this.devicesListBox.SelectedIndex] != TestStatus.MarkForRetest))
                {
                    // send the message.
                    DatumString message = new DatumString("MarkSN", devicesListBox.SelectedItem.ToString());
                    this.sendToWorker(message);
                    //MessageClass.SendToWorker(message);

                    // update the internal Test statuses array.
                    testStatuses[this.devicesListBox.SelectedIndex] = TestStatus.MarkForRetest;

                    // Refresh the display.
                    this.devicesListBox.Refresh();
                }
            }
        }

        /// <summary>
        /// Enable GUI Buttons.
        /// </summary>
        private void enableButtons()
        {
            this.testDeviceButton.Enabled = true;
            this.endBatchButton.Enabled = true;
            this.markForRetestButton.Enabled = true;
            
        }

        /// <summary>
        /// Disable GUI Buttons.
        /// </summary>
        private void disableButtons()
        {
            this.testDeviceButton.Enabled = false;
            this.endBatchButton.Enabled = false;
            this.markForRetestButton.Enabled = false;
        }
    }
}

// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// MzDcControl.Gui/LoadDeviceTypeDialogueCtl.cs
// 
// Author: tommy.yu
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;
//using Bookham.TestSolution.TestModules;


namespace Bookham.TestSolution.TestControl
{
    public partial class LoadDeviceTypeDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// 
        /// </summary>
        public LoadDeviceTypeDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

        }

        private void LoadDeviceTypeDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType().Equals(typeof(DatumStringArray)))
            {
                DatumStringArray sa = (DatumStringArray)payload;
                DeviceTypeCombo.DataSource = sa.ValueCopy();

            }
            else
            {
                //Error - invalid message.
                System.Diagnostics.Trace.Write("Warning unknown messaage type " + payload.GetType());
            }

        }

        private void StartTestButton_Click(object sender, EventArgs e)
        {
            this.sendToWorker(this.DeviceTypeCombo.Text);
        }
   
    }
}

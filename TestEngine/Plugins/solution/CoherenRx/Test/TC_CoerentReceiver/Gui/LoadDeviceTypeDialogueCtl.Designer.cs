// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// MzDcControl.Gui/LoadDeviceTypeDialogueCtl
// 
// Author: tommy.yu
// Design: TODO

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// 
    /// </summary>
    partial class LoadDeviceTypeDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartTestButton = new System.Windows.Forms.Button();
            this.DeviceTypeFrame = new System.Windows.Forms.GroupBox();
            this.DeviceTypeCombo = new System.Windows.Forms.ComboBox();
            this.messageLabel = new System.Windows.Forms.Label();
            this.DeviceTypeFrame.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartTestButton
            // 
            this.StartTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartTestButton.Location = new System.Drawing.Point(299, 96);
            this.StartTestButton.Name = "StartTestButton";
            this.StartTestButton.Size = new System.Drawing.Size(104, 36);
            this.StartTestButton.TabIndex = 3;
            this.StartTestButton.Text = "Start";
            this.StartTestButton.Click += new System.EventHandler(this.StartTestButton_Click);
            // 
            // DeviceTypeFrame
            // 
            this.DeviceTypeFrame.Controls.Add(this.DeviceTypeCombo);
            this.DeviceTypeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceTypeFrame.Location = new System.Drawing.Point(36, 82);
            this.DeviceTypeFrame.Name = "DeviceTypeFrame";
            this.DeviceTypeFrame.Size = new System.Drawing.Size(214, 54);
            this.DeviceTypeFrame.TabIndex = 1;
            this.DeviceTypeFrame.TabStop = false;
            this.DeviceTypeFrame.Text = "Device Type";
            // 
            // DeviceTypeCombo
            // 
            this.DeviceTypeCombo.FormattingEnabled = true;
            this.DeviceTypeCombo.Location = new System.Drawing.Point(11, 22);
            this.DeviceTypeCombo.Name = "DeviceTypeCombo";
            this.DeviceTypeCombo.Size = new System.Drawing.Size(189, 24);
            this.DeviceTypeCombo.TabIndex = 0;
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(20, 237);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(448, 23);
            this.messageLabel.TabIndex = 5;
            // 
            // LoadDeviceTypeDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.StartTestButton);
            this.Controls.Add(this.DeviceTypeFrame);
            this.Controls.Add(this.messageLabel);
            this.Name = "LoadDeviceTypeDialogueCtl";
            this.Size = new System.Drawing.Size(717, 280);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadDeviceTypeDialogueCtl_MsgReceived);
            this.DeviceTypeFrame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartTestButton;
        private System.Windows.Forms.GroupBox DeviceTypeFrame;
        private System.Windows.Forms.ComboBox DeviceTypeCombo;
        private System.Windows.Forms.Label messageLabel;

    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.Framework.InternalData;


namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
            //Hide the retry/abort dialogue
            hideRetryAbortControls();
            this.loadBatchTextBox.CharacterCasing = CharacterCasing.Upper;
            
        }

        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload.GetType().Equals(typeof(GuiMsgEnum)))
            {
                GuiMsgEnum gm = (GuiMsgEnum)payload;
                switch (gm)
                {
                    case GuiMsgEnum.LoadBatchRequest:
                        // The Test Control Plug-in wants to load a batch. 
                        // Display the GUI controls and prompt the user.
                        this.messageLabel.Text = "Please input a batch to load";

                        //Enable the Load Batch dialogue.
                        displayControls();
                        this.loadBatchButton.Enabled = true;
                        break;
                    case GuiMsgEnum.LoadBatchComplete:
                        // The batch has been loaded. Hide the controls and prepare for the 
                        // next Load Batch operation.
                        this.loadBatchTextBox.Text = "";
                        this.messageLabel.Text = "";
                        hideControls();
                        break;

                }
            }
            else if (payload.GetType().Equals(typeof(DatumString)))
            {
                DatumString ds = (DatumString)payload;
                switch (ds.Name)
                {
                    case "RetryAbortRequest":
                        // Disable the Load Batch Controls
                        disableLoadBatchControls();

                        //Show the Abort/Retry Controls
                        showRetryAbortControls();

                        // Display the error message.
                        this.messageLabel.Text = ds.Value;
                        break;
                    default:
                        break;
                }
            }
            else if (payload is string)
            {
                messageLabel.Text = payload.ToString();
            }
            else
            {
                //Error - invalid message.
                System.Diagnostics.Trace.Write("Warning unknown messaage type " + payload.GetType());
            }


        }

        /// <summary>
        /// The User clicked the load batch button. Send a message to the worker thread, with the 
        /// user's desired Batch ID.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event parameters.</param>
        private void loadBatchButton_Click(object sender, System.EventArgs e)
        {
            // Do not allow empty BatchIDs.
            if (this.loadBatchTextBox.Text.Trim() == "")
            {
                this.messageLabel.Text = "Batch ID could not be empty.";
                return;
            }

            //disableLoadBatchControls();
            string batchID = this.loadBatchTextBox.Text.Trim();
            if (batchID.Contains(".001"))
            {
                batchID = batchID.Replace(".001", ".1");
                this.messageLabel.Text = "Loading......";
            }
            //else
            //{
            //    this.messageLabel.Text = "错误的SN号，格式应该像 xxxxxx.001 \r\nWrong SN format,SN should be like xxxxxx.001";
            //    this.loadBatchTextBox.Focus();
            //    return;
            //}

            DatumString retData = new DatumString("batchID", batchID);

            this.sendToWorker(retData);
        }

        /// <summary>
        /// Dispay normal load batch screen controls.
        /// </summary>
        private void displayControls()
        {
            this.loadBatchScreenLabel.Show();
            this.bookhamLogoPictureBox.Show();
            showLoadBatchControls();
            hideRetryAbortControls();
        }

        /// <summary>
        /// Hide All  Controls
        /// </summary>
        private void hideControls()
        {
            this.loadBatchScreenLabel.Hide();
            this.bookhamLogoPictureBox.Hide();
            hideLoadBatchControls();
            hideRetryAbortControls();
        }

        /// <summary>
        /// Hide Load Batch operation associated controls.
        /// </summary>
        private void hideLoadBatchControls()
        {
            this.loadBatchTextBox.Hide();
            this.loadBatchButton.Hide();
            this.messageLabel.Hide();
        }

        /// <summary>
        /// Show Load Batch operation associated controls.
        /// </summary>
        private void showLoadBatchControls()
        {
            this.loadBatchTextBox.Show();
            this.loadBatchButton.Show();
            this.messageLabel.Show();
        }

        /// <summary>
        /// Disable Load Batch operation associated controls.
        /// </summary>
        private void disableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = false;
            this.loadBatchButton.Enabled = false;
            this.comboBoxTestStage.Enabled = false;
        }

        /// <summary>
        /// Enable Load Batch operation associated controls.
        /// </summary>
        private void enableLoadBatchControls()
        {
            this.loadBatchTextBox.Enabled = true;
            this.loadBatchButton.Enabled = true;
            this.comboBoxTestStage.Enabled = true;
        }

        /// <summary>
        /// Hide controls associated with abort/retry query.
        /// </summary>
        private void hideRetryAbortControls()
        {
            this.abortButton.Hide();
            this.retryButton.Hide();
            this.retryAbortLabel.Hide();
        }


        /// <summary>
        /// Show controls associated with abort/retry query.
        /// </summary>
        private void showRetryAbortControls()
        {
            this.abortButton.Show();
            this.retryButton.Show();
            this.retryAbortLabel.Show();
        }


        /// <summary>
        /// User clicks the retry button. Send a retry message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void retryButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            this.sendToWorker(QueryRetryAbort.Retry);
            //MessageClass.SendToWorker(QueryRetryAbort.Retry);

            //Hide the Retry/Abort Controls.
            hideRetryAbortControls();

            //Enable Load Batch Controls
            //enableLoadBatchControls();
            disableLoadBatchControls();
            // Remove the error message.
            this.messageLabel.Text = "";
        }


        /// <summary>
        /// User clicks the abort button. Send a abort message to the worker thread and resume 
        /// displaying the normal load batch screen.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event parameters</param>
        private void abortButton_Click(object sender, System.EventArgs e)
        {
            // Message the worker thread.
            this.sendToWorker(QueryRetryAbort.Abort);
            //MessageClass.SendToWorker(QueryRetryAbort.Abort);

            //Hide the Retry/Abort Controls.
            hideRetryAbortControls();

            //Enable Load Batch Controls
            enableLoadBatchControls();

            // Remove the error message.
            this.messageLabel.Text = "";

            // Prompt the user
            this.loadBatchTextBox.Text = "";

            this.loadBatchButton.Enabled = true;
        }

        private void loadBatchTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!string.IsNullOrEmpty(loadBatchTextBox.Text.Trim()) && e.KeyChar == '\r')
                if (!loadBatchButton.Focused) loadBatchButton.Focus();

        }

        private void LoadBatchDialogueCtl_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && !loadBatchTextBox.Focused)
            {
                loadBatchTextBox.Focus();
            }
        }

        private void loadBatchTextBox_Enter(object sender, EventArgs e)
        {
            loadBatchTextBox.SelectAll();
        }

    }
}
      

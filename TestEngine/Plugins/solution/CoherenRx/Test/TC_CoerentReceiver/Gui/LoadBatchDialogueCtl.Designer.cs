// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.Designer.cs
//
// Author: tommy.yu, 2007
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadBatchDialogueCtl));
            this.retryAbortLabel = new System.Windows.Forms.Label();
            this.retryButton = new System.Windows.Forms.Button();
            this.abortButton = new System.Windows.Forms.Button();
            this.messageLabel = new System.Windows.Forms.Label();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.comboBoxTestStage = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // retryAbortLabel
            // 
            this.retryAbortLabel.AutoSize = true;
            this.retryAbortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryAbortLabel.ForeColor = System.Drawing.Color.Red;
            this.retryAbortLabel.Location = new System.Drawing.Point(228, 193);
            this.retryAbortLabel.Name = "retryAbortLabel";
            this.retryAbortLabel.Size = new System.Drawing.Size(491, 20);
            this.retryAbortLabel.TabIndex = 6;
            this.retryAbortLabel.Text = "Load Batch operation failed. Please decide what to do next: ";
            // 
            // retryButton
            // 
            this.retryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryButton.Location = new System.Drawing.Point(523, 78);
            this.retryButton.Name = "retryButton";
            this.retryButton.Size = new System.Drawing.Size(83, 31);
            this.retryButton.TabIndex = 2;
            this.retryButton.Text = "Retry";
            this.retryButton.Click += new System.EventHandler(this.retryButton_Click);
            // 
            // abortButton
            // 
            this.abortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abortButton.Location = new System.Drawing.Point(622, 78);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(83, 31);
            this.abortButton.TabIndex = 3;
            this.abortButton.Text = "Abort";
            this.abortButton.Click += new System.EventHandler(this.abortButton_Click);
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(7, 54);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(689, 23);
            this.messageLabel.TabIndex = 5;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.AutoSize = true;
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(143, 19);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(512, 20);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine TestControl - CohRx Goldbox : Load Batch Screen";
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(7, 7);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(129, 44);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bookhamLogoPictureBox.TabIndex = 10;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(7, 180);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(192, 45);
            this.loadBatchButton.TabIndex = 1;
            this.loadBatchButton.Text = "Load Batch";
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(7, 79);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(510, 29);
            this.loadBatchTextBox.TabIndex = 0;
            this.loadBatchTextBox.Enter += new System.EventHandler(this.loadBatchTextBox_Enter);
            this.loadBatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loadBatchTextBox_KeyPress);
            // 
            // comboBoxTestStage
            // 
            this.comboBoxTestStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTestStage.FormattingEnabled = true;
            this.comboBoxTestStage.Items.AddRange(new object[] {
            "rf_groupa",
            "rf_groupb_h",
            "rf_groupb_l",
            "rf SPC"});
            this.comboBoxTestStage.Location = new System.Drawing.Point(200, 114);
            this.comboBoxTestStage.Name = "comboBoxTestStage";
            this.comboBoxTestStage.Size = new System.Drawing.Size(317, 33);
            this.comboBoxTestStage.TabIndex = 11;
            this.comboBoxTestStage.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "Select Test Stage:";
            this.label1.Visible = false;
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxTestStage);
            this.Controls.Add(this.retryAbortLabel);
            this.Controls.Add(this.retryButton);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Name = "LoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(792, 268);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            this.VisibleChanged += new System.EventHandler(this.LoadBatchDialogueCtl_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label retryAbortLabel;
        private System.Windows.Forms.Button retryButton;
        private System.Windows.Forms.Button abortButton;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label loadBatchScreenLabel;
        private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
        private System.Windows.Forms.Button loadBatchButton;
        private System.Windows.Forms.TextBox loadBatchTextBox;
        private System.Windows.Forms.ComboBox comboBoxTestStage;
        private System.Windows.Forms.Label label1;



    }
}

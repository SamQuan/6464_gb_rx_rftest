﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TestControl;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Config;


namespace Bookham.TestSolution.TestControl
{
    public class MesMapping
    {
        public string PcasStage { get; private set; }
        public string MesStage { get; private set; }
        public string FwStep { get; private set; }
        public string PartCode { get; private set; }

        public MesMapping(string partcode, string pcasstage, string messtage)
        {
            this.PartCode = partcode;
            this.PcasStage = pcasstage;
            this.MesStage = messtage;
            this.FwStep = "Missing";
        }
        public MesMapping(string partcode, string pcasstage, string messtage, string fwstep)
        {
            this.PartCode = partcode;
            this.PcasStage = pcasstage;
            this.MesStage = messtage;
            this.FwStep = fwstep;
        }
    }
    public class MesMapCollection : List<MesMapping>
    {
        public MesMapCollection()
        {
            string mapFile = "Configuration\\ConherentRx\\FWStageListMapping.csv";
            using (StreamReader sr = new StreamReader(mapFile))
            {
                string head = sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    string[] lineStages = line.Split(new char[] { ',' });
                    if (lineStages.Length == 3)
                        this.Add(new MesMapping(lineStages[0], lineStages[1], lineStages[2]));
                    else if (lineStages.Length == 4)
                        this.Add(new MesMapping(lineStages[0], lineStages[1], lineStages[2], lineStages[3]));
                }
            }
        }
    }

    public static class EquitpmentReader
    {
        private static string ReadEquipID(ITestEngine engine, string filePath, string nodeID, string jigID)
        {
            ConfigDataAccessor config = new ConfigDataAccessor(filePath, "Table_EquipID");

            try
            {
                StringDictionary configKeys = new StringDictionary();
                configKeys["NodeID"] = nodeID;
                configKeys["JigID"] = jigID;

                return config.GetData(configKeys, true).ReadString("EquipID");
            }
            catch
            {
                engine.ErrorRaise(string.Format("Couldn't find EquipID by [NodeID: {0}, JigID: {1}] from file {2}", nodeID, jigID, filePath));
            }

            return null;
        }

        private static string ReadEquipID(ITestEngine engine, string filePath, string nodeID)
        {
            return ReadEquipID(engine, filePath, nodeID, "N/A");
        }

        /// <summary>
        /// Read EquipmentID method
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        public static string ReadEquipment(ITestEngine engine, DUTObject dutObject)
        {
            string equipID = ReadEquipID(engine, @"Configuration\EQUIP_ID_Config.xml", dutObject.NodeID.ToString());
            return equipID;
        }
    }
}

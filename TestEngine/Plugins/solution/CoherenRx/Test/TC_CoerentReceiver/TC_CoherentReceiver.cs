// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_CoherentRx_Control.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using System.Collections.Specialized;
using System.Collections.Generic;
using Bookham.TestEngine.Config;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;
using Bookham.Solution.Config;
using System.Data;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.MES;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
//using Bookham.TestSolution.TestPrograms;

namespace Bookham.TestSolution.TestControl
{

    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_CoherentReceiver : ITestControl
    {
        #region private members

        const string SettingsPath = @"Configuration\ConherentRx\TestSettings.xml";
        string updateSoftwareConfig = "";

        // The current BatchID
        string batchID = "";

        // Reference to Object passed by the Test Control Server that implements the 
        // IMES interface.
        FactoryWorks mes;
        int maxTestCount;
        string userId;

        //True if the Config file has been read
        bool SettingsHasRead;

        //Temporary simulation flag.
        bool inSimulationMode;

        // When True, the Plugin is retrying loading a Batch from the MES.
        bool retryingMesLoadBatch;

        //When True, we have a valid BatchId. 
        bool gotValidBatchId;

        //True if Trackout is to be inhibited. Read from the internal configuration file.
        bool trackoutInhibit;

        //Node ID as read from the configuration file.
        int nodeID;

        //Equipment ID as read from the configuration file.
        string equipmentId;

        // Object to hold the loaded batch.
        MESbatch loadedBatch;

        // Internal store of Test Statuses for the current batch.
        TestStatus[] testStatuses;
        //// Flag for the program ran (so we can abort the batch)
        //bool programRanOnce = false;

        /// <summary>
        /// Flag for use in GetNextDUT(). State of 'false' on first call, 'true' thereafter.
        /// </summary>
        bool subsequentCallOfGetNextDUT; // mfx

        // The Index of the current DUT within the batch & Test Statuses array.
        int currentDutIndex;

        // The Program status, as stored in the Post Device operation.
        ProgramStatus programStatus;

        // Test results for the current DUT.
        //TestResults testResults;

        //device test result status
        PassFail deviceTestStatus;

        // Batch View tab controls
        Type[] batchCtlList;

        // Test Control tab controls
        Type[] testCtlList;
        Location Location;
        string MainTestStage;
        // MES / Results database (PCAS) Node
        //int node;
        // Serial number variable
        //string serialNum;
        /// <summary>
        /// DUT object
        /// </summary>
        DUTObject DeviceUnderTest;
        /// <summary>
        /// Access DqpskTestParams.xml.
        /// </summary>
        TestParamConfigAccessor testParamsConfig;

        string testStage = "";
        GroupBtest gb = null;

        TestResults testResult;

        string PcasStage = "";

        private AutoTestStage CurrentStage;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_CoherentReceiver()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[3];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);
            // TODO: Add any new controls here
            this.testCtlList[1] = typeof(LoadSnDialogueCtl);
            this.testCtlList[2] = typeof(LoadDeviceTypeDialogueCtl);
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType)
        {
            gb = new GroupBtest(@"Configuration\ConherentRx\groupBdatabase.accdb");
            batchID = "";
            this.mes = mes as FactoryWorks;
            this.userId = operatorName;
            this.nodeID = this.mes.Node;
            bool onlySpcStageSelect = false;
            //if (operatorName=="EngDebug")
            //{
            //    onlySpcStageSelect = false;
            //}

            if (!SettingsHasRead)
                ReadTestControlSettings(testEngine);

            #region xiaojiang  auto update SW

            if (!this.UpdateSoftwareFunc(testEngine, this.mes.Node.ToString()))
            {
                testEngine.ErrorRaise("Software is out of date!!!");
            }
            #endregion

            //xiaojiang spc
            TestEngine.PluginInterfaces.TestControl.ButtonId btnResponse= testEngine.ShowYesNoUserQuery(TestControlTab.BatchView, "Do SPC Test?");
            if ((btnResponse == TestEngine.PluginInterfaces.TestControl.ButtonId.Yes) || this.inSimulationMode)
            {
                if (btnResponse == TestEngine.PluginInterfaces.TestControl.ButtonId.Yes)
                {
                    onlySpcStageSelect = true;
                    this.inSimulationMode = true;
                }
                else
                    this.inSimulationMode = true;
            }
            else
                this.inSimulationMode = false;



            // Bring the Test Control Tab to the front of the GUI, and attract the user's attention.
            testEngine.PageToFront(TestControlCtl.BatchView);
            testEngine.PageToFront(TestControlCtl.TestControl);
            testEngine.SendStatusMsg("Load a batch");
            testEngine.GetUserAttention(TestControlCtl.TestControl);

            if (!inSimulationMode)
            {
                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);
            }
            else
            {
                testEngine.SelectTestControlCtlToDisplay(typeof(LoadSnDialogueCtl));
                List<string> stageList;
                List<string> typeList;
                readOfflineStagesAndTypes(testEngine,onlySpcStageSelect, out stageList, out typeList);

                DatumStringArray sa = new DatumStringArray("stageList", stageList.ToArray());
                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), sa);
                //raise event
                //UpdateUI.Instance.SendToGui(sa);

                sa = new DatumStringArray("typeList", typeList.ToArray());
                testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadSnDialogueCtl), sa);

            }

            this.gotValidBatchId = false;
            this.retryingMesLoadBatch = false;
            while (true)
            {
                if (!retryingMesLoadBatch)
                {
                    // Wait for a message from the Test Control GUI.
                    CtlMsg msg = testEngine.WaitForCtlMsg();
                    object obj = msg.Payload;    
                   // object obj= MessageClass.WaitForGuiMsg();  

                    if (obj is Dictionary<string, string>)
                    {
                        Dictionary<string, string> ds = obj as Dictionary<string, string>;

                        batchID = ds["batchID"];
                        testStage = ds["TestStage"];

                        if (testStage == "rf_groupa")
                        {
                            testStage = "RFTEST";
                        }
                        else
                        {
                            inSimulationMode = true;
                            testEngine.SelectTestControlCtlToDisplay(typeof(LoadDeviceTypeDialogueCtl));
                            List<string> stageList;
                            List<string> typeList;
                            readOfflineStagesAndTypes(testEngine, onlySpcStageSelect, out stageList, out typeList);

                            DatumStringArray sa = new DatumStringArray("typeList", typeList.ToArray());
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadDeviceTypeDialogueCtl), sa);

                            CtlMsg message = testEngine.WaitForCtlMsg();
                            object obj2 = message.Payload;

                            DatumList dl = obj as DatumList;
                            DUTObject dut = new DUTObject();

                            dut.PartCode = (string)obj2;
                            dut.TestStage = testStage;
                            dut.SerialNumber = batchID;

                            dut.OverrideSimulationMode = false;
                            dut.IsSimulation = false;
                            dut.ContinueOnFail = false;
                            dut.BatchID = "BogusBatch";
                            dut.FirstInBatch = true;
                            dut.LastInBatch = true;


                            dut.Attributes.AddOrUpdateBool("RETER_DEVICE", false);    //add by Dong.Chen 

                            loadedBatch = new MESbatch();
                            loadedBatch.Add(dut);
                            loadedBatch.PartCode = dut.PartCode;
                            loadedBatch.Stage = dut.TestStage;
                            batchID = "BogusBatch";
                            //Stop attracting the user's attention.
                            testEngine.CancelUserAttention(TestControlCtl.TestControl);

                            this.gotValidBatchId = true;


                        }


                        //Stop attracting the user's attention.
                        testEngine.CancelUserAttention(TestControlCtl.TestControl);

                        //Set flag to indicate that there is a valid batchId.
                        this.gotValidBatchId = true;
                    }
                    else if (obj is DatumList)
                    {
                        DatumList dl = obj as DatumList;
                        DUTObject dut = new DUTObject();

                        dut.PartCode = dl.ReadString("DeviceType");
                       this.testStage=  this.MainTestStage = dut.TestStage = dl.ReadString("TestStage"); // Add by XiaoJiang.Wang_20160406_for RF_SPC
                        if (dl.ReadString("TestStage") == "auto")
                        {

                            gb.autotest = true;

                        }
                        else
                        {
                            gb.autotest = false;
                        }


                        dut.SerialNumber = dl.ReadString("SerialNumber");

                        dut.OverrideSimulationMode = false;
                        dut.IsSimulation = false;
                        dut.ContinueOnFail = false;
                        dut.BatchID = "BogusBatch";
                        dut.FirstInBatch = true;
                        dut.LastInBatch = true;
                        dut.Attributes.AddOrUpdateString("lot_type", "Standard");

                        loadedBatch = new MESbatch();
                        loadedBatch.Add(dut);
                        loadedBatch.PartCode = dut.PartCode;
                        loadedBatch.Stage = dut.TestStage;
                        batchID = "BogusBatch";
                        //Stop attracting the user's attention.
                        testEngine.CancelUserAttention(TestControlCtl.TestControl);

                        this.gotValidBatchId = true;

                    }
                    //xiaojiang spc
                    else if (obj is DatumString)
                    {
                        DatumString ds = obj as DatumString;

                        switch (ds.Name)
                        {
                            case "batchID":

                                batchID = ds.Value;
                                break;

                            default:
                                break;
                        }

                        //Stop attracting the user's attention.
                        testEngine.CancelUserAttention(TestControlCtl.TestControl);

                        //Set flag to indicate that there is a valid batchId.
                        this.gotValidBatchId = true;
                    }
                    //
                    //

                    else
                    {
                        //Invalid message recieved.
                        testEngine.ErrorRaise("Invalid message recieved: " + obj.ToString());
                    }
                }
                else
                {
                    //Retrying Load Batch operation
                    this.gotValidBatchId = true;
                }

                if (gotValidBatchId)
                {

                    try
                    {
                        if (!inSimulationMode)
                        {

                            // Load the Batch from the MES.
                            //loadedBatch = mes.LoadBatch(batchID, true);
                            this.loadedBatch = this.mes.LotQueryFullNew(this.batchID, this.userId);

                            #region xiaojiang fw 20151210
                            MesMapCollection mesCollection = new MesMapCollection();
                            MesMapping SuitableMes = mesCollection.Find(delegate(MesMapping map)
                            {
                                return map.PartCode == loadedBatch.PartCode && map.MesStage == loadedBatch.Stage;
                            });
                            this.testStage = this.MainTestStage = SuitableMes.PcasStage;

                            this.CurrentStage = (AutoTestStage)Enum.Parse(typeof(AutoTestStage), this.testStage);


                            #endregion

                            // Lookup node based on configuration
                            this.mes.Node = this.nodeID;
                            this.PcasStage = this.testStage;
                            //Do spc check
                            CheckSPCStatus(testEngine);

                            // Load the batch, but don't bother with component level attributes
                            if (this.loadedBatch.Attributes.ReadString("current_rule").ToLower() == "trackin")
                                this.mes.LotStateNew(batchID, this.userId);
                        }
                        // Initialise a new Test Statuses array.
                        this.testStatuses = new TestStatus[loadedBatch.Count];

                        // Initialise each device to untested status.
                        int index = 0;
                        foreach (DUTObject dut in loadedBatch)
                        {
                            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
                            testStatuses[index++] = TestStatus.Untested;

                            dut.NodeID = this.nodeID;
                            dut.EquipmentID = this.equipmentId;
                            dut.Attributes.AddString("Location", Location.ToString());
                            //if (string.IsNullOrEmpty(dut.TestStage))
                            dut.TestStage = MainTestStage;
                            //Do lot type check
                            CheckLotType(testEngine, dut);
                        }
                        // Send a load batch complete message to the Test Control GUI, so that it can hide 
                        // the load batch dialogue.
                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchComplete);
                        testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
                        //Batch has now loaded so we can now exit the while loop.
                        retryingMesLoadBatch = false;
                        break;
                    }
                    catch (MESTimeoutException)
                    {
                        // Log the fact that there was an MES Timeout exception.
                        testEngine.SendStatusMsg("MES Load Batch Operation timedout.");

                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (MESInvalidOperationException e)
                    {
                        // Log the fact that there was an MES Invalid operation exception.
                        testEngine.SendStatusMsg("MES Invalid operation exception." + e.Message + e.StackTrace);

                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (MESCommsException e)
                    {
                        testEngine.SendStatusMsg("MES Communications exception." + e.Message + e.StackTrace);
                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (Exception ex)
                    {
                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl, ex.Message);
                        testEngine.SendStatusMsg(ex.Message + ex.StackTrace);
                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                }
                else  //if (gotBatchId == true)
                {
                    //Invalid message recieved or no valid BatchId
                    testEngine.ErrorRaise("Invalid response to Batch ID Request.");
                }
            }// while( true )
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            //// Reset one-time run flag
            //programRanOnce = false;
            // Return serial number or batch id to Test Engine

            if (gb.autotest == true)
            {

                gb.device_type = loadedBatch[0].PartCode;
                gb.querystatus(gb.device_type, loadedBatch[0].SerialNumber);
            }

            return batchID;

        }

        private void readOfflineStagesAndTypes(ITestEngine testEngine,bool onlySpcStageSelect, out List<string> stageList, out List<string> typeList)
        {
            ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            DataTable progLookup = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");

            stageList = new List<string>();
            typeList = new List<string>();
            if (!onlySpcStageSelect)
            {
                stageList.Add("auto");
            }
            foreach (DataRow row in progLookup.Rows)
            {
                // Get Test Stages
                string str = (string)row["PacsTestStage"];
                if (string.IsNullOrEmpty(str)) throw new Exception("No TestStage node!");
                if (!stageList.Contains(str))
                    if (!onlySpcStageSelect)
                    {
                        if (!str.ToLower().Contains("spc"))
                        {
                            stageList.Add(str);
                        }
                    }
                    else
                    {
                        if (str.ToLower().Contains("spc"))
                        {
                            stageList.Add(str);
                        }
                    }
                    
                // Get Part Codes
                str = (string)row["PacsDeviceType"];
                if (string.IsNullOrEmpty(str)) throw new Exception("No DeviceType node!");
                if (!typeList.Contains(str)) typeList.Add(str);
                   
            }

        }

        private void dealWithLoadBatchFromMesError(ITestEngine testEngine)
        {
            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), new DatumString("RetryAbortRequest", "MES Load Batch operation Timed Out."));
            // wait for response.
            CtlMsg msg = testEngine.WaitForCtlMsg();
            object obj = msg.Payload;
            //object obj = MessageClass.WaitForGuiMsg();

            if (obj.GetType().Equals(typeof(QueryRetryAbort)))
            {
                QueryRetryAbort qr = (QueryRetryAbort)obj;
                switch (qr)
                {
                    case QueryRetryAbort.Abort:
                        // the User elected to abort. Request that the user select another batch for loading.
                        testEngine.SendStatusMsg("User aborted load batch operation");
                        //Instruct the Test Control User Control to get user input to load a batch.
                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchRequest);
                        retryingMesLoadBatch = false;
                        break;
                    default:
                        retryingMesLoadBatch = true;
                        break;
                }
            }
            else
            {
                // Something very wrong - Log an error.
                testEngine.ErrorRaise("Invalid message recieved");
            }
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, string operatorName, TestControlPrivilegeLevel operatorType)
        {

            bool found = false;
            string receivedSerialNumber = " ";
            bool autoTestStage = false;


            DUTObject currentDut = this.loadedBatch[currentDutIndex];

            DatumList spcList = new DatumList();
            if (currentDut.Attributes.IsPresent("SPC_RESULTS"))
            {
                spcList = currentDut.Attributes.ReadReference("SPC_RESULTS") as DatumList;
                spcList.AddOrUpdateString("TIME_DATE", DateTime.Now.ToString("yyyyMMddHHmmss"));
                SpcConfigReader.Initialise(currentDut.NodeID.ToString(), currentDut.TestStage);
                SpcHelper.SPC_Post_Checks(testEngine, currentDut.NodeID.ToString(), spcList);
                    //testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "SPC Rule Failed! Please retest！\n(标准件超出SPC limit 范围！请联系技术员分析！)");
            }

            // mfxV

            if (this.loadedBatch.Stage == "manual_fxit" && // special Manual mode (no BatchView)
                this.subsequentCallOfGetNextDUT == false)
            {
                // this.subsequentCallOfGetNextDUT will be set 'true' for next time - to allow user to re-test

                bool foundTheFirstOne = false;
                this.currentDutIndex = 0; // only the one DUT.

                foreach (DUTObject dutObject in loadedBatch)
                {
                    foundTheFirstOne = true; // Found the DUT Object 
                    break;    // Break out of the For loop.
                }

                if (foundTheFirstOne == false)
                {
                    // The required DUT is not in the batch. This is a fatal error.
                    testEngine.ErrorRaise("Get Next DUT: No DUT Specified");
                }

            }
            else // normal 'prompt-user-with-batchView' path ...
            {
                this.subsequentCallOfGetNextDUT = true;
                // mfxV
                if (!inSimulationMode && this.CurrentStage!= AutoTestStage.rf_end && testStatuses[currentDutIndex] == TestStatus.Passed)
                {
                    autoTestStage = true;
                    this.mes.TRACKOUT(this.batchID, this.loadedBatch.Stage, this.loadedBatch.PartCode, this.userId);
                    System.Threading.Thread.Sleep(5000);

                    try
                    {
                        if (!inSimulationMode)
                        {
                            // Load the Batch from the MES.
                            //loadedBatch = mes.LoadBatch(batchID, true);
                            //do
                            //{
                            this.loadedBatch = this.mes.LotQueryFullNew(this.batchID, this.userId);
                             
                            //} while (true);

                            #region xiaojiang fw 20151210
                            MesMapCollection mesCollection = new MesMapCollection();
                            MesMapping SuitableMes = mesCollection.Find(delegate(MesMapping map)
                            {
                                return map.PartCode == loadedBatch.PartCode && map.MesStage == loadedBatch.Stage;
                            });
                            this.testStage = this.MainTestStage = SuitableMes.PcasStage;

                            this.CurrentStage = (AutoTestStage)Enum.Parse(typeof(AutoTestStage), this.testStage);

                            #endregion

                            // Lookup node based on configuration
                            this.mes.Node = this.nodeID;

                            // Load the batch, but don't bother with component level attributes
                            if (this.loadedBatch.Attributes.ReadString("current_rule").ToLower() == "trackin")
                                this.mes.LotStateNew(batchID, this.userId);
                        }
                        // Initialise a new Test Statuses array.
                        this.testStatuses = new TestStatus[loadedBatch.Count];

                        // Initialise each device to untested status.
                        int index = 0;
                        foreach (DUTObject dut in loadedBatch)
                        {
                            if (!autoTestStage)
                            {
                                testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);
                            }
                            testStatuses[index++] = TestStatus.Untested;

                            dut.NodeID = this.nodeID;
                            dut.EquipmentID = this.equipmentId;
                            dut.Attributes.AddString("Location", Location.ToString());
                            //if (string.IsNullOrEmpty(dut.TestStage))
                            dut.TestStage = MainTestStage;

                            //Do lot type check
                            CheckLotType(testEngine, dut);
                        }
                        // Send a load batch complete message to the Test Control GUI, so that it can hide 
                        // the load batch dialogue.
                        if (!autoTestStage)
                        {
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl), GuiMsgEnum.LoadBatchComplete);
                            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
                        }
                        //Batch has now loaded so we can now exit the while loop.
                        retryingMesLoadBatch = false;

                    }
                    catch (MESTimeoutException)
                    {
                        // Log the fact that there was an MES Timeout exception.
                        testEngine.SendStatusMsg("MES Load Batch Operation timedout.");

                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (MESInvalidOperationException e)
                    {
                        // Log the fact that there was an MES Invalid operation exception.
                        testEngine.SendStatusMsg("MES Invalid operation exception." + e.Message + e.StackTrace);

                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (MESCommsException e)
                    {
                        testEngine.SendStatusMsg("MES Communications exception." + e.Message + e.StackTrace);
                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                    catch (Exception ex)
                    {
                        testEngine.SendStatusMsg(ex.Message + ex.StackTrace);
                        // Ask the user what to do about the error, then act accordingly.
                        dealWithLoadBatchFromMesError(testEngine);
                    }
                }

                //Grab the user's attention, and instruct them to select a DUT.
                if (!autoTestStage)
                {
                    testEngine.PageToFront(TestControlCtl.BatchView);
                    testEngine.SendStatusMsg("Select a DUT");
                    testEngine.GetUserAttention(TestControlCtl.BatchView);
                    testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), GuiMsgEnum.SelectNextDUTRequest);
                }
                // Select DUT - wait for message from BatchDisplay user ctl

                while (gb.serial_entry_flag && !autoTestStage)
                {
                    CtlMsg msg = testEngine.WaitForCtlMsg();
                    Type messageType = msg.Payload.GetType();
                    object obj = msg.Payload;
                    //object obj = MessageClass.WaitForGuiMsg();
                    //Type messageType = obj.GetType();

                    if (messageType == typeof(GuiMsgEnum))
                    {
                        // Exit here with a Null DUT object if we have reached the end of the batch.
                        GuiMsgEnum gm = (GuiMsgEnum)obj;
                        if (gm == GuiMsgEnum.EndOfBatchNotification)
                        {
                            testEngine.CancelUserAttention(TestControlCtl.BatchView);
                            return null;
                        }
                    }
                    else if (messageType == typeof(DatumString))
                    {
                        // The user marked a device for retest. Update the internal test status for the device.
                        DatumString response = (DatumString)obj;
                        if (response.Name == "NextSN")
                        {
                            receivedSerialNumber = response.Value;
                            //Find the index of the DUT Object in the loaded batch. Store this in the 
                            // currentDutIndex class private variable.
                            int index = 0;
                            foreach (DUTObject dutObject in loadedBatch)
                            {
                                if (dutObject.SerialNumber == receivedSerialNumber)
                                {
                                    currentDutIndex = index;
                                    found = true;

                                    // Found the DUT Object for the next device to be tested. Break out of the For loop.
                                    break;
                                }
                                index++;
                            }
                            // Break out of the message recieve while (true) loop.
                            break;
                        }
                        else if (response.Name == "MarkSN")
                        {
                            for (int i = 0; i < testStatuses.Length; i++)
                            {
                                if (loadedBatch[i].SerialNumber == response.Value)
                                {
                                    testStatuses[i] = TestStatus.MarkForRetest;

                                    // Break out of for loop, as we've found the deivce and updated it's status.
                                    break;
                                }
                            }
                        }
                        else if (response.Name == "NextQualTestSN")
                        {
                            receivedSerialNumber = response.Value;
                            //Find the index of the DUT Object in the loaded batch. Store this in the 
                            // currentDutIndex class private variable.
                            int index = 0;
                            foreach (DUTObject dutObject in loadedBatch)
                            {
                                if (dutObject.SerialNumber == receivedSerialNumber)
                                {
                                    currentDutIndex = index;
                                    found = true;
                                    dutObject.TestStage = "qualification";

                                    // Found the DUT Object for the next device to be tested. Break out of the For loop.
                                    break;
                                }
                                index++;
                            }
                            // Break out of the message recieve while (true) loop.
                            break;
                        }
                    }
                    else
                    {
                        testEngine.ErrorRaise("Test Control Plug-in Get Next DUT Operation - Invalid Message received: " + messageType.ToString());
                    }
                } // While (true)

                if (!found)
                {
                    //The required DUT is not in the bacth. This is a fatal error.
                    //testEngine.ErrorRaise("Get Next DUT: Invalid DUT Specified " + receivedSerialNumber);
                }

                // Stop attracting user attention
                testEngine.CancelUserAttention(TestControlCtl.BatchView);

                // mfxV
            } // end of normal 'prompt-user-with-batchView'


            this.subsequentCallOfGetNextDUT = true; // for next time
            // mfx^ 

            // Check if DUT is testable on current jig
            string jigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);

            if (jigID != "DefaultTestJigID")
            {
                testEngine.ErrorRaise("Invalid Test Jig for device!!");
            }

            // "Lookup" which test program to run
            //readTestProgramList(testEngine);
            DeviceUnderTest = new DUTObject();
            //if(testStatuses[this.currentDutIndex]!=TestStatus.Untested)
            //{
            DeviceUnderTest = this.loadedBatch[this.currentDutIndex];
            //}
            //else
            //{
            if (gb.autotest == true)
            {
                loadedBatch[this.currentDutIndex].TestStage = gb.getcurrenttestmodules();
            }
            DeviceUnderTest = SetTestProgram(testEngine);

            try
            {
                //add max continous test count
                if (DeviceUnderTest.Attributes.IsPresent("lot_type"))
                {
                    string lotType = DeviceUnderTest.Attributes.ReadString("lot_type");
                    if (lotType.ToLower()=="production")
                    {
                        if (RetestOutofRange(testEngine, DeviceUnderTest))
                        {
                            testEngine.ShowContinueUserQuery(TestControlTab.TestControl,
                                string.Format("Retest count out of spec, testing denied! MaxTestCount[{0}]", this.maxTestCount));

                            return null;
                        } 
                    }
                }
            }
            catch { }

            if (this.CurrentStage== AutoTestStage.rf_groupb_ll)
            {
                this.CurrentStage = AutoTestStage.rf_end;
            }

            // }

            // How do we signal 'wrong test stage' without the engine shutting down ungracefully?

            // Return the DUT updated with which test prpgram to run
            // return loadedBatch[currentDutIndex];
            if (gb.serial_entry_flag && !autoTestStage)
            {
                DeviceUnderTest.SerialNumber = receivedSerialNumber;
            }
            // Equipment ID is based on network computer name
            DeviceUnderTest.NodeID = this.nodeID;
            this.equipmentId = EquitpmentReader.ReadEquipment(testEngine, DeviceUnderTest);
            DeviceUnderTest.EquipmentID = this.equipmentId;


            DeviceUnderTest.TestJigID = jigID;
            DeviceUnderTest.BatchID = this.batchID;

            this.loadedBatch[0] = DeviceUnderTest;

            return DeviceUnderTest;
        }
        private DUTObject SetTestProgram(ITestEngine testEngine)
        {
            DUTObject dut = this.loadedBatch[this.currentDutIndex];
            ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            DataTable progLookup = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");


            string TestProgramName = null;
            string TestProgramVersion = null;
            string PacsDeviceType = null;
            string PacsTestStage = null;
            foreach (DataRow row in progLookup.Rows)
            {
                if (inSimulationMode)
                {
                    if ((string)row["PacsDeviceType"] == dut.PartCode && (string)row["PacsTestStage"] == dut.TestStage)
                    {
                        TestProgramName = (string)row["TestProgramName"];
                        TestProgramVersion = (string)row["TestProgramVersion"];
                        PacsDeviceType = (string)row["PacsDeviceType"];
                        PacsTestStage = (string)row["PacsTestStage"];
                        break;
                    }
                }
                else
                {
                    if ((string)row["DeviceType"] == dut.PartCode && (string)row["TestStage"] == dut.TestStage)
                    {
                        TestProgramName = (string)row["TestProgramName"];
                        TestProgramVersion = (string)row["TestProgramVersion"];
                        PacsDeviceType = (string)row["PacsDeviceType"];
                        PacsTestStage = (string)row["PacsTestStage"];
                        break;
                    }
                }
            }

            // First check if there is a matching data row
            if (string.IsNullOrEmpty(TestProgramName))
            {
                //Invalid configuration file contents.
                testEngine.ErrorRaise("Cannot find matching row in " + SettingsPath + " for DeviceType=" + dut.PartCode + " TestStage=" + dut.TestStage);
                dut.ProgramPluginName = "UNKNOWN";
                dut.ProgramPluginVersion = string.Empty;
                dut.PartCode = string.Empty;
                dut.TestStage = string.Empty;

            }
            else
            {
                dut.ProgramPluginName = TestProgramName;
                dut.ProgramPluginVersion = TestProgramVersion;
                //dut.PartCode = PacsDeviceType;
                dut.TestStage = PacsTestStage;
            }
            return dut;
        }

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            //TODO: Modify DUTOutcome object, 
            //If required: add details based on analysis of the test results, specify 
            //what specifications the test results are to be stored against plus any other supporting data.
            gb.updatedatabase();
            this.programStatus = programStatus;
            this.testResult = testResults;
            // Store the results for use in the Post Device commit Phase.
            testEngine.SendStatusMsg("Post Device");
            int numberOfSupermodes = int.MaxValue;
            string smSpecNameStub = "";

            if (DeviceUnderTest.Attributes.IsPresent("NumberOfSupermodes"))
            {
                numberOfSupermodes = DeviceUnderTest.Attributes.ReadSint32("NumberOfSupermodes");
            }
            if (DeviceUnderTest.Attributes.IsPresent("SupermodeSpecNameStub"))
            {
                smSpecNameStub = DeviceUnderTest.Attributes.ReadString("SupermodeSpecNameStub");
            }

            List<String> specsToUse = new List<String>();
            foreach (SpecResults specRes in testResults.SpecResults)
            {
                string specName = specRes.Name;
                //if (smSpecNameStub.Length > 0 && specName.Contains(smSpecNameStub) && specName.Contains("_"))
                //{
                //    // It's a supermode spec. Should we add it ?
                //    int lastUnderscore = specName.LastIndexOf("_");
                //    int smNumber = int.Parse(specName.Substring(lastUnderscore + 1, specName.Length - lastUnderscore - 1));
                //    // If the spec is less than the supermode count we can add it
                //    if (smNumber < numberOfSupermodes)
                //        specsToUse.Add(specName);
                //}
                //else
                //{
                
                    specsToUse.Add(specName);

                    if (testResults.ProgramStatus.Status == MultiSpecPassFail.AllPass
                        && testResults.OverallPassFail == PassFail.Pass)
                    {
                        if (testResults.ProgramStatus.IsComplete == MultiSpecComplete.NoParams)
                        {
                            deviceTestStatus = PassFail.Fail;
                        }
                        else
                        {
                            deviceTestStatus = specRes.Status.Status;
                        }
                    }
                    else
                    {
                        deviceTestStatus = PassFail.Fail;
                    }

                    
                //}
            }

            DUTOutcome dutOutcome = new DUTOutcome();
            dutOutcome.OutputSpecificationNames = specsToUse.ToArray();
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            archiveFiles(DeviceUnderTest);

            testEngine.SendStatusMsg("Post Device Commit");

            //Mark the Device as Tested. Also record it's pass fail status.
            if ((this.programStatus == ProgramStatus.Success) && (deviceTestStatus == PassFail.Pass))
            {
                //The Test Passed. Mark the device as completed.
                testStatuses[currentDutIndex] = TestStatus.Passed;
            }
            else if ((this.programStatus == ProgramStatus.Success) && (deviceTestStatus == PassFail.Fail))
            {
                //Problem writing Test Data. Mark for retest.
                testStatuses[currentDutIndex] = TestStatus.Failed;
            }
            else if (this.programStatus == ProgramStatus.Failed ||
                     this.programStatus == ProgramStatus.NonParametricFailure)
            // We only want to condemn a device which is definitely duff, so assume all
            // other statuses leave the device as "untested".
            {
                testStatuses[currentDutIndex] = TestStatus.Failed;

                //Update the Component level MES Data for the device if not in Simulation mode.
                //if (!inSimulationMode)
                //{
                //    mes.SetComponentAttribute(batchID, loadedBatch[currentDutIndex].SerialNumber, "PartId", "FAILED");
                //}
            }
            else
            {
                testStatuses[currentDutIndex] = TestStatus.Untested;
            }


            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);


            try
            {
                DUTObject dut = this.loadedBatch[0];
                string[] dropFiles = Directory.GetFiles(@"pcasc\temp\" + dut.NodeID, "*.pcasDrop");

                int filesCounts = dropFiles.Length;

                if (dut.Attributes.IsPresent("PinCheckStatus"))
                {
                    string PinCheckStatus = dut.Attributes.ReadString("PinCheckStatus");
                    if (PinCheckStatus.ToLower() == "fail")
                    {
                        for (int i = 0; i < filesCounts; i++)
                        {
                            string dropFile = dropFiles[i];
                            string testStage = "";
                            using (StreamReader sr = new StreamReader(dropFile))
                            {
                                do
                                {
                                    if (sr.ReadLine().Contains("~STAGE"))
                                    {
                                        testStage = sr.ReadLine();
                                        break;
                                    }

                                } while (true);
                            }

                            if (!testStage.ToLower().Contains("rf_pincheck"))
                            {
                                File.Delete(dropFile);
                            }
                            else
                            {
                                string strName = System.IO.Path.GetFileName(dropFile); //?? e.jpg 
                                File.Move(dropFile, @"pcasc\" + dut.NodeID + "\\" + strName);
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < filesCounts; i++)
                    {
                        string dropFile = dropFiles[i];
                        string strName = System.IO.Path.GetFileName(dropFile); //?? e.jpg 
                        File.Move(dropFile, @"pcasc\" + dut.NodeID + "\\" + strName);
                    }
                }
            }
            catch (Exception)
            {

                int aa = 0;
            }
        
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
            try
            {
                //Trackout the Batch if we are not in Simulation Mode.
                if ((!this.inSimulationMode) && (!trackoutInhibit))
                {
                    if (testStatuses[currentDutIndex] == TestStatus.Passed)
                    {
                        this.mes.TRACKOUT(this.batchID, this.loadedBatch.Stage, this.loadedBatch.PartCode, this.userId);
                        //mes.TrackOutBatchPass(this.batchID, loadedBatch.Stage, "Passed");
                    }
                    else
                    {
                        if (GetTestCycle(testEngine) >= this.maxTestCount - 1)
                            this.mes.REWORK(this.batchID, "", this.loadedBatch.PartCode, this.loadedBatch.Stage, this.userId, "Failed");
                    }
                    //else if (testStatuses[currentDutIndex] == TestStatus.Failed)
                    //{
                    //    //Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId respond =
                    //    //    testEngine.ShowYesNoUserQuery(TestControlTab.TestControl, "Please select whether track out device to Rework!");

                    //    //if (respond == Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId.Yes)
                    //    //{
                    //    //    string newBathID = mes.TrackOutComponentRework(this.batchID, loadedBatch.Stage, loadedBatch[currentDutIndex].SerialNumber, "Failed");
                    //    //}
                    //}
                    //else
                    //{ }
                }
            }
            catch (Exception ex)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.BatchView, ex.Message);
            }
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {
            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendStatusMsg("Restart Batch");

            for (int i = 0; i < testStatuses.Length; i++)
            {
                if (testStatuses[i] != TestStatus.Untested)
                {
                    testStatuses[i] = TestStatus.MarkForRetest;
                }

                testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), testStatuses);
            }
        }
        # endregion

        #region Private Methods

        #region add by xiaojiang
        private int GetTestCycle(ITestEngine engine)
        {
            StringDictionary mapkeys = new StringDictionary();
            mapkeys.Add("SCHEMA", "hiberdb");
            mapkeys.Add("SERIAL_NO", this.loadedBatch[0].SerialNumber);
            mapkeys.Add("DEVICE_TYPE", this.loadedBatch.PartCode);//this.pcasDeviceType);
            mapkeys.Add("TEST_STAGE", this.loadedBatch[0].TestStage);//this.MainTestStage);

            DatumList[] ret = engine.GetDataReader().GetResults(mapkeys, true, "");
            if (ret == null) return 0;
            return ret.Length;
        }
        #endregion
        private void ReadTestControlSettings(ITestEngine testEngine)
        {
            SettingsHasRead = false;
            ConfigurationManager cfg = new ConfigurationManager(SettingsPath);
            NameValueCollection Settings = (NameValueCollection)cfg.GetSection("TestControl/ControlSettings");
            this.inSimulationMode = bool.Parse(Settings["SimulateMes"]);
            this.trackoutInhibit = bool.Parse(Settings["InhibitTrackout"]);
            //this.nodeID = int.Parse(Settings["NodeId"]);
            this.maxTestCount = int.Parse(Settings["MaxTestCount"]);
            //this.equipmentId = Settings["EquipmentId"]; // Skip By Dongcheng_20160104
            this.updateSoftwareConfig = Settings["UpdateSoftwareConfig"];

            this.Location = (Location)Enum.Parse(typeof(Location), Settings["Location"]);
            DataTable TestStages = (DataTable)cfg.GetSection("TestControl/" + Location.ToString() + "_TestStages/MainTestStages");
            MainTestStage = (string)TestStages.Rows[0]["TestStage"];

            // Set flag to indicate that configuration has been read.
            SettingsHasRead = true;

        }
        private string archiveFiles(DUTObject dutObject)
        {
            testParamsConfig = new TestParamConfigAccessor(dutObject,
                @"Configuration\ConherentRx\CoherentReceiverTestParams.xml", "", "CoheRxTestParams");
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                testParamsConfig.GetStringParam("ResultsArchiveDirectory"),
                "CoherentRxFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(testParamsConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(testParamsConfig.GetStringParam("CoheRxFileDirectory")))
            {
                string[] filesToAdd = Directory.GetFiles(
                    testParamsConfig.GetStringParam("CoheRxFileDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);

                if (filesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    using (zipFile)
                    {
                        foreach (string fileToAdd in filesToAdd)
                        {
                            try
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                            catch (Exception)
                            {
                                // Ignore errors caused by engineers viewing plot data in XL !
                            }
                        }
                    }
                    cleanUpFiles();
                    return zipFileName;
                }
            }
            cleanUpFiles();
            return null;
        }
        /// <summary>
        /// Delete any old CSV data.
        /// </summary>
        private void cleanUpFiles()
        {
            if (!Directory.Exists(testParamsConfig.GetStringParam("CoheRxFileDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    testParamsConfig.GetStringParam("CoheRxFileDirectory"), "*.csv");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        public void CheckLotType(ITestEngine testEngine, DUTObject loadedBatch)
        {
            if (loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "ENGINEERING" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "PRODUCTION" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "CUSTOMERRETURN" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "REWORK" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "STANDARD")
            {
                //DO NOTHING
            }
            else
            {
                testEngine.ErrorRaise("This batch's current lot_type is " + loadedBatch.Attributes.ReadString("lot_type") + " , it must be engineering,production or customerreturn !" +
                    "\r\n当前器件lot_type 是 " + loadedBatch.Attributes.ReadString("lot_type") + " , 必须是 engineering,production 或者 customerreturn !");
            }
        } 

        private ActionResult CheckSPCStatus(ITestEngine testEngine)
        {
            #region xiaojiang 20160902
            ActionResult result = ActionResult.Empty;
            try
            {
                if (this.PcasStage == "")
                {
                    this.PcasStage = "ALL";
                }
                result = SpcHelper.MustDoSpcTest(testEngine, this.mes.Node, this.PcasStage);
            }
            catch
            {
                result = ActionResult.Empty;
            }

            if (result == ActionResult.Continue)
            {
                // do nothing
            }
            else if (result == ActionResult.Information)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "请尽快做标准件测试！");
            }
            else if (result == ActionResult.Stop)
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "请先做标准件测试");
                throw new Exception("请先做标准件测试才允许做产品测试！");
            }
            else if (result == ActionResult.ForceStop)
            {
                //testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "SPC Rule Failed! Please retest！\n(标准件超出SPC limit 范围！请重新测试标准件！)");
                throw new Exception("SPC Rule Failed! Please retest！\n(标准件超出SPC limit 范围！请重新测试标准件！)");
            }
            else if (result == ActionResult.Empty)
            {
                testEngine.SendStatusMsg("Error to judge daily spc status: Action is Empty!!!");
            }
            #endregion


            return result;
        }


        private string UpdateFileString(string updatefile)
        {
            string updateContent = string.Empty;
            if (File.Exists(updatefile))
            {
                using (StreamReader sr = new StreamReader(updatefile))
                {
                    updateContent = sr.ReadLine().Trim();
                }
            }

            return updateContent;
        }
        private bool ResetUpdateFileEmpty(string node, string updatefile)
        {
            try
            {
                INI.WriteValue(this.updateSoftwareConfig, "Node", node, "True");

                FileInfo info = new FileInfo(updatefile);
                info.IsReadOnly = false;

                using (StreamWriter sw = new StreamWriter(updatefile))
                {
                    sw.WriteLine("Empty!");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool UpdateSoftwareFinished(string node)
        {
            if (string.IsNullOrEmpty(this.updateSoftwareConfig)) return true;

            bool result = true;
            string value = "true";
            try
            {
                value = INI.GetStringValue(this.updateSoftwareConfig, "Node", node, "True");
            }
            catch { }

            if (bool.TryParse(value, out result))
                return result;

            return true;
        }
        private bool UpdateSoftwareFunc(ITestEngine engine, string node)
        {
            string updatefile = "Configuration\\UpdateFile";
            string updateContent = string.Empty;
            updateContent = UpdateFileString(updatefile);

            if (this.UpdateSoftwareFinished(node))
            {
                if (updateContent.ToLower() == "latest")
                    ResetUpdateFileEmpty(node, updatefile);

                return true;
            }

            updateContent = UpdateFileString(updatefile);

            if (updateContent.ToLower() == "latest")
            {
                ResetUpdateFileEmpty(node, updatefile);
                return true;
            }


            engine.ShowContinueUserQuery(TestControlTab.TestControl, "Software has been already updated, please download the latest version.\r\n软件版本已更新，请关闭软件重新下载最新版本进行测试！");
            return false;
        }

        private bool RetestOutofRange(ITestEngine engine, DUTObject dut)
        {
            StringDictionary mapKeys = new StringDictionary();

            mapKeys.Add("SCHEMA", "hiberdb");
            mapKeys.Add("SERIAL_NO", dut.SerialNumber);
            mapKeys.Add("TEST_STAGE", dut.TestStage);
            mapKeys.Add("DEVICE_TYPE", dut.PartCode);//这个要加上，因为测试次数达到最大时，可以转Code再测试。

            DatumList list = engine.GetDataReader().GetLatestResults(mapKeys, false);

            if (list.IsPresent("RETEST"))
            {
                string retest = list["RETEST"].ValueToString();

                engine.SendStatusMsg("RETEST:" + retest);

                int CL = this.ReadRetest(dut.TestStage);
                int RC = 999;
                int.TryParse(retest.Trim(), out RC);

                if (RC == CL - 1)
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                else if (RC == CL)
                {
                    return true;
                }
            }
            else
            {
                DatumList[] lists = engine.GetDataReader().GetResults(mapKeys, false, "");

                int CL = this.ReadRetest(dut.TestStage);
                if (lists.Length == CL)
                {
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                }
                else if (lists.Length == CL + 1)
                {
                    return true;
                }
            }
            return false;
        }

        private int ReadRetest(string stage)
        {
            string file = "Configuration\\Retest2Scrap.csv";
            if (!File.Exists(file)) return 999;

            int count = 999;

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string[] line = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length != 2) continue;

                    if (stage.ToLower() == line[0].Trim().ToLower())
                    {
                        int.TryParse(line[1].Trim(), out count);

                        break;
                    }
                }
            }
            return count;
        }


        #endregion

    }

    internal enum Location
    {
        CoherentRx,
        ConherentTx

    }

    internal enum GuiMsgEnum
    {
        LoadBatchRequest,
        LoadBatchComplete,
        EndOfBatchNotification,
        SelectNextDUTRequest,

    }
    /// <summary>
    /// User query result enum: Retry or abort current operation.
    /// </summary>
    internal enum QueryRetryAbort
    {
        /// <summary>
        /// Retry
        /// </summary>
        Retry,

        /// <summary>
        /// Abort.
        /// </summary>
        Abort
    }
    /// <summary>
    /// Test Status of devices within a batch
    /// </summary>
    internal enum TestStatus
    {
        /// <summary>
        /// Untested.
        /// </summary>
        Untested,

        /// <summary>
        /// Marked for restest.
        /// </summary>
        MarkForRetest,

        /// <summary>
        /// Passed.
        /// </summary>
        Passed,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed
    }

    internal enum AutoTestStage
    {
        Inlegal=0,
        rf_groupa = 1,
        rf_groupb_hs = 2,
        rf_groupb_hl = 4,
        rf_groupb_ls = 8,
        rf_groupb_ll = 16,
        rf_end = 32
    }

}

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace Bookham.TestSolution.TestControl
{
    public class GroupBtest
    {

        /// <summary>
        /// write the database
        /// </summary>
        /// <param name="executeSql">the command for update the database</param>
        /// <returns>return 0 stand of execute fail,return 1 stand of execute pass</returns>
        public int ExecuteSql(string executeSql)
        {
            int result = 0;
            OleDbConnection theConn = new OleDbConnection(connStr);
            theConn.Open();
            OleDbCommand theComm = new OleDbCommand(executeSql, theConn);
            result = theComm.ExecuteNonQuery();
            theConn.Close();

            return result;
        }
        public GroupBtest(string datafile)
        {
            connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + datafile;
            sampletest = false;
            serial_entry_flag = true;
            currenttestmodules = "";
            testmodules = new string[3];
            testmodules[0] = "rf_groupa";
            testmodules[1] = "rf_groupb_l";
            testmodules[2] = "rf_groupb_h";
        }

        public string getcurrenttestmodules()
        {
            if (currenttestmodules == "")
            {
                currenttestmodules = testmodules[0];
            }
            else
            {
                if (currenttestmodules == testmodules[0])
                {
                    currenttestmodules = testmodules[1];
                }
                else
                {
                    if (currenttestmodules == testmodules[1])
                    {
                        currenttestmodules = testmodules[2];
                    }
                }
            }
            return  currenttestmodules ;
        }
        public string connStr = "" ;
        // query to see the sample test
      
        //query current status  
        public void querystatus(string  devicetype , string serial_number)
            
        {
            if ( currenttestmodules ==  "")
            {
                //query table configure table : devicetype , sample rate ,lotnumber ,recordnumber
                device_type = devicetype;
                serialnumber = serial_number;
                DataSet ds = new DataSet();
                string sql = "select * from configuretable where devicetype='" + devicetype + "'";
                ds = GetDataSet(sql);
                DataTable currentDataTable = ds.Tables["DefaultTable"];
                int count = currentDataTable.Rows.Count;
                DataRow dr = null;
                for (int i = 0; i < count; i++)
                {
                    dr = currentDataTable.Rows[i];
                    samplerate = Convert.ToInt32(dr["samplerate"]);
                    lotnumber = Convert.ToInt32(dr["lotnumber"]);
                    recordnumber = Convert.ToInt32(dr["recordnumber"]);

                    if (samplerate > recordnumber + 1)
                    {
                        sampletest = false;
                        recordnumber = recordnumber + 1;
                    }
                    else
                    {
                        if (samplerate == recordnumber + 1)
                        {
                            sampletest = true;
                            recordnumber = recordnumber + 1;
                        }
                        else
                        {
                            lotnumber = lotnumber + 1;
                            recordnumber = 1;
                            sampletest = false;
                        }
                    }
                }
            }

        }

        public void updatetestprogress()
        {
            if (sampletest == true)
            {
                if (currenttestmodules == testmodules[2])
                {
                    serial_entry_flag = true;
                }
                else
                {
                    serial_entry_flag = false;
                }
            }
            else
            {
                serial_entry_flag = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void updatedatabase()
        {
            if (autotest)
            {
                updatetestprogress();

                if ((sampletest == true && currenttestmodules == testmodules[2]) || (sampletest == false))
                {
                    string sql = "Insert into deviceinformation(serialnumber ,devicetype, testdate, sampletest,lotnumber,recordnumber) values('" + serialnumber + "','" + device_type + "','" + DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + sampletest.ToString() + "','" + lotnumber.ToString() + "','" + recordnumber.ToString() + "' )";
                    //"Insert into RecordDeviceTest(SerialNo,TestTime,TestSetName,TestMode,GroupAPassFailFlag,GroupBPassFailFlag,GroupATestFlag,GroupBTestFlag) Values('" + tc_State.UnitSerialNbr + "','" + dateTime + "','" + tc_State.EquipmentId + "','" + testMode + "',false,false,false,false)";
                    try
                    {
                        ExecuteSql(sql);
                    }
                    catch (Exception e)
                    {
                    }
                    //update configuretable
                    sql = "update configuretable set lotnumber='" + lotnumber.ToString() + "' , recordnumber='" + recordnumber.ToString() + "'  where devicetype='" + device_type + "'";
                    try
                    {
                        ExecuteSql(sql);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
        /// <summary>
        /// Read the database
        /// </summary>
        /// <param name="selectSql">the command for select sentence</param>
        /// <returns>DataSet for store the result</returns>
        public DataSet GetDataSet(string selectSql)
        {
           
            DataSet ds = new DataSet();
            try
            {
                OleDbConnection theConn = new OleDbConnection(connStr);
                OleDbDataAdapter theAd = new OleDbDataAdapter(selectSql, theConn);
                theAd.Fill(ds, "DefaultTable");
                theConn.Close();
            }
            catch (Exception e)
            {
            }
            
            return ds;
        }
        public int lotnumber  ;
        public int recordnumber;
        public bool sampletest;
        public string device_type;
        public int samplerate;
        public string serialnumber;
                
        public string[]  testmodules;
        
        // define if test auto or hand set 
        public bool autotest;
        public bool serial_entry_flag;
        public string currenttestmodules ;


    }
}

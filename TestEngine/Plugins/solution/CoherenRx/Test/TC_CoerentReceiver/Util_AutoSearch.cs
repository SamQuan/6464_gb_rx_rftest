using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// 
    /// </summary>
    public static class Util_AutoSearch
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="e"></param>
        public static void AutoSearchKeyUp(ComboBox combo, KeyEventArgs e)
        {
            //cancel special key
            switch (e.KeyCode)
            {
                case Keys.Back:
                case Keys.Left:
                case Keys.Right:
                case Keys.Up:
                case Keys.Down:
                case Keys.Delete:
                case Keys.CapsLock:
                case Keys.ShiftKey:
                case Keys.ControlKey:
                case Keys.Alt:
                    return;
            }
            //find item in items
            string strInput = combo.Text;
            int intFoundIndex = combo.FindString(strInput);
            if (intFoundIndex > -1)
            {
                object objFoundItem = combo.Items[intFoundIndex];
                string strFoundText = combo.GetItemText(objFoundItem);
                string strAppend = strFoundText.Substring(strInput.Length);
                combo.Text = strInput + strAppend;
                combo.SelectionStart = strInput.Length;
                combo.SelectionLength = strAppend.Length;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="combo"></param>
        public static void AutoSearchLeave(ComboBox combo)
        {
            int intFoundIndex = combo.FindStringExact(combo.Text);
            combo.SelectedIndex = -1;
            combo.SelectedIndex = intFoundIndex;
        }

    }
}

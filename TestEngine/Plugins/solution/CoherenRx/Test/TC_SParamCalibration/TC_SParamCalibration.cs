// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// TC_DeliveryData.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using System.Collections.Specialized;
using Bookham.TestEngine.PluginInterfaces.Security;

//using Bookham.TestEngine.Security;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template.
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_SParamCalibration : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_SParamCalibration()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[1];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);
            // TODO: Add any new controls here
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType)
        {
            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.node = mes.Node;

            // show the user control
            testEngine.SelectTestControlCtlToDisplay(typeof(LoadBatchDialogueCtl));
            testEngine.GetUserAttention(TestControlCtl.TestControl);

            //// wait for the message to say "OK" has been clicked...
            //CtlMsg msg = testEngine.WaitForCtlMsg();
            //// ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            //// Get the serial number
            //object msgContents = msg.Payload;
            //this.serialNum = (string)msgContents;
            this.serialNum = "SparamCalibration";
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine
            return serialNum;
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg, string operatorName, TestControlPrivilegeLevel operatorType)
        {
            if (this.programRanOnce) return null; // end of batch after one run!

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            DUTObject dut = new DUTObject();
            dut.BatchID = this.serialNum;  //from Load Batch
            dut.SerialNumber = this.serialNum;
            // Equipment ID is based on network computer name
            dut.EquipmentID = System.Net.Dns.GetHostName();
            // Set results node
            dut.NodeID = this.node;

            // TODO: CHANGE THE FOLLOWING LINES FOR REQUIRED PROGRAM AND DEVICE
            dut.PartCode = "VNA";
            dut.TestStage = "S Param Cal";
            dut.ProgramPluginName = "Prog_SParamCalibration";
            dut.ProgramPluginVersion = "";
            dut.Attributes.AddString("OperatorType",operatorType.ToString());

            // only allowed though here once!
            this.programRanOnce = true;
            return dut;
        }


        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            //TODO: Modify DUTOutcome object, 
            //If required: add details based on analysis of the test results, specify 
            //what specifications the test results are to be stored against plus any other supporting data.

            DUTOutcome dutOutcome = new DUTOutcome();
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
        }
        # endregion


        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // MES / Results database (PCAS) Node
        int node;
        // Serial number variable
        string serialNum;
        #endregion

    }
}

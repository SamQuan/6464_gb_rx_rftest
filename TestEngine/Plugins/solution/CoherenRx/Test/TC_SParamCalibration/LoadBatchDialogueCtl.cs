// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    public partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }


        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
        }


        private void button1_Click(object sender, EventArgs e)
        {
            string serialNum = txtSerialNo.Text;

            this.sendToWorker(serialNum);
        }
    }
}

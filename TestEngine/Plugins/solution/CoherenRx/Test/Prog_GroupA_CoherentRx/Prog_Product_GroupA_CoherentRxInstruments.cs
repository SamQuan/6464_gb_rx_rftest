using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
//using Bookham.TestSolution.TestScriptLanguage;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_Product_GroupA_CoherentRxInstruments
    {

        internal IInstType_TecController TecCase;

        internal Inst_iTLATunableLaserSource laserSource;
        internal PdBiasInstrument PdSource;
        internal TiaInstrument Tia_X;
        internal TiaInstrument Tia_Y;

        internal Switch_LCA_RF RfSwitch;

        internal Instr_Ag33120A WaveformGenerator;

        internal InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        internal InstType_OpticalPowerMeter OPMMon_Sig;

        internal InstType_OpticalPowerMeter OPMRef_Sig;

        internal InstType_OpticalPowerMeter OPMMon_Loc;

        internal InstType_OpticalPowerMeter OPMRef_Loc;

        internal Inst_SR830 LockInAmplifier;

        internal Inst_OzDd100mc_Attenuator VOA_Loc;

        internal Inst_OzDd100mc_Attenuator VOA_Sig;

        internal Inst_Ke24xx SwitchForCutOffPwrInput_Loc;

        internal Inst_Ke24xx SwitchForCutOffPwrInput_Sig;

        internal Inst_OzDd100mc_Attenuator VOAForCutOffPwrInput_Loc;

        internal Inst_OzDd100mc_Attenuator VOAForCutOffPwrInput_Sig;

        internal PolarizeController PolController_Sig;

        internal PolarizeController PolController_Loc;

        internal InstType_ODL DelayLine_Sig;

        internal InstType_ODL DelayLine_Loc;

        internal Inst_Sub20ToAsic pol_sig;

        internal Inst_Sub20ToAsic pol_loc;

        internal Instr_SPI inSPI ;

    }

}

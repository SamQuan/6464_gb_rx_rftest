// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// Prog_GroupB_CoherentRx/ProgramGui.cs
// 
// Author: 
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class Prog_Product_GroupA_CoherentRxGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_Product_GroupA_CoherentRxGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Message Received Event Handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ProgramGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            Type t = payload.GetType();
            if (t == typeof(System.String))
            {
                this.label1.Text = payload.ToString();
            }
        }

        /// <summary>
        /// Expose the protected send to worker to allow other controls to use it!
        /// </summary>
        /// <param name="payload"></param>
        internal void SendToWorker(object payload)
        {
            this.sendToWorker(payload);
        }

        /// <summary>
        /// Indicate that the control has finished
        /// </summary>
        internal void CtrlFinished()
        {
            panel1.Controls.Clear();
        }
    }
}

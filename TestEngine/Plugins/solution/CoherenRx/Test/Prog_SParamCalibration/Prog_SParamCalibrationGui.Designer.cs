// [Copyright]
//
// Bookham Test Engine
// Prog_TCMZ_Final
//
// Prog_TCMZ_Final/ProgramGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestPrograms
{
    partial class Prog_SParamCalibrationGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groubBox_VNASetting = new System.Windows.Forms.GroupBox();
            this.label_IFBW = new System.Windows.Forms.Label();
            this.textBox_IFBW = new System.Windows.Forms.TextBox();
            this.label_SweepPoints = new System.Windows.Forms.Label();
            this.textBox_SweepPoints = new System.Windows.Forms.TextBox();
            this.label_StopFreq = new System.Windows.Forms.Label();
            this.textBox_StopFreq = new System.Windows.Forms.TextBox();
            this.label_StartFreq = new System.Windows.Forms.Label();
            this.textBox_StartFreq = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.but_OK = new System.Windows.Forms.Button();
            this.checkBox_S21ChipY = new System.Windows.Forms.CheckBox();
            this.checkBox_S21ChipX = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_S22ChipY = new System.Windows.Forms.CheckBox();
            this.checkBox_S22ChipX = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.groubBox_VNASetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groubBox_VNASetting);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.but_OK);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1047, 677);
            this.panel1.TabIndex = 0;
            // 
            // groubBox_VNASetting
            // 
            this.groubBox_VNASetting.Controls.Add(this.label_IFBW);
            this.groubBox_VNASetting.Controls.Add(this.textBox_IFBW);
            this.groubBox_VNASetting.Controls.Add(this.label_SweepPoints);
            this.groubBox_VNASetting.Controls.Add(this.textBox_SweepPoints);
            this.groubBox_VNASetting.Controls.Add(this.label_StopFreq);
            this.groubBox_VNASetting.Controls.Add(this.textBox_StopFreq);
            this.groubBox_VNASetting.Controls.Add(this.label_StartFreq);
            this.groubBox_VNASetting.Controls.Add(this.textBox_StartFreq);
            this.groubBox_VNASetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groubBox_VNASetting.Location = new System.Drawing.Point(385, 65);
            this.groubBox_VNASetting.Name = "groubBox_VNASetting";
            this.groubBox_VNASetting.Size = new System.Drawing.Size(315, 247);
            this.groubBox_VNASetting.TabIndex = 4;
            this.groubBox_VNASetting.TabStop = false;
            this.groubBox_VNASetting.Text = "VNA Setting";
            // 
            // label_IFBW
            // 
            this.label_IFBW.AutoSize = true;
            this.label_IFBW.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_IFBW.Location = new System.Drawing.Point(6, 204);
            this.label_IFBW.Name = "label_IFBW";
            this.label_IFBW.Size = new System.Drawing.Size(135, 29);
            this.label_IFBW.TabIndex = 7;
            this.label_IFBW.Text = "IFBW_KHz:";
            // 
            // textBox_IFBW
            // 
            this.textBox_IFBW.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_IFBW.Location = new System.Drawing.Point(190, 190);
            this.textBox_IFBW.Multiline = true;
            this.textBox_IFBW.Name = "textBox_IFBW";
            this.textBox_IFBW.Size = new System.Drawing.Size(119, 43);
            this.textBox_IFBW.TabIndex = 6;
            this.textBox_IFBW.Text = "10";
            // 
            // label_SweepPoints
            // 
            this.label_SweepPoints.AutoSize = true;
            this.label_SweepPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SweepPoints.Location = new System.Drawing.Point(6, 153);
            this.label_SweepPoints.Name = "label_SweepPoints";
            this.label_SweepPoints.Size = new System.Drawing.Size(162, 29);
            this.label_SweepPoints.TabIndex = 5;
            this.label_SweepPoints.Text = "SweepPoints:";
            // 
            // textBox_SweepPoints
            // 
            this.textBox_SweepPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_SweepPoints.Location = new System.Drawing.Point(190, 130);
            this.textBox_SweepPoints.Multiline = true;
            this.textBox_SweepPoints.Name = "textBox_SweepPoints";
            this.textBox_SweepPoints.Size = new System.Drawing.Size(119, 43);
            this.textBox_SweepPoints.TabIndex = 4;
            this.textBox_SweepPoints.Text = "801";
            // 
            // label_StopFreq
            // 
            this.label_StopFreq.AutoSize = true;
            this.label_StopFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StopFreq.Location = new System.Drawing.Point(6, 97);
            this.label_StopFreq.Name = "label_StopFreq";
            this.label_StopFreq.Size = new System.Drawing.Size(179, 29);
            this.label_StopFreq.TabIndex = 3;
            this.label_StopFreq.Text = "StopFreq_GHz:";
            // 
            // textBox_StopFreq
            // 
            this.textBox_StopFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_StopFreq.Location = new System.Drawing.Point(191, 75);
            this.textBox_StopFreq.Multiline = true;
            this.textBox_StopFreq.Name = "textBox_StopFreq";
            this.textBox_StopFreq.Size = new System.Drawing.Size(119, 43);
            this.textBox_StopFreq.TabIndex = 2;
            this.textBox_StopFreq.Text = "40";
            // 
            // label_StartFreq
            // 
            this.label_StartFreq.AutoSize = true;
            this.label_StartFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StartFreq.Location = new System.Drawing.Point(6, 33);
            this.label_StartFreq.Name = "label_StartFreq";
            this.label_StartFreq.Size = new System.Drawing.Size(178, 29);
            this.label_StartFreq.TabIndex = 1;
            this.label_StartFreq.Text = "StartFreq_GHz:";
            // 
            // textBox_StartFreq
            // 
            this.textBox_StartFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_StartFreq.Location = new System.Drawing.Point(190, 19);
            this.textBox_StartFreq.Multiline = true;
            this.textBox_StartFreq.Name = "textBox_StartFreq";
            this.textBox_StartFreq.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox_StartFreq.Size = new System.Drawing.Size(119, 43);
            this.textBox_StartFreq.TabIndex = 0;
            this.textBox_StartFreq.Text = "0.1399";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_S22ChipY);
            this.groupBox1.Controls.Add(this.checkBox_S22ChipX);
            this.groupBox1.Controls.Add(this.checkBox_S21ChipY);
            this.groupBox1.Controls.Add(this.checkBox_S21ChipX);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(25, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 173);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Calibarte fibre Path";
            // 
            // but_OK
            // 
            this.but_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.but_OK.Location = new System.Drawing.Point(25, 244);
            this.but_OK.Name = "but_OK";
            this.but_OK.Size = new System.Drawing.Size(354, 68);
            this.but_OK.TabIndex = 5;
            this.but_OK.Text = "OK";
            this.but_OK.UseVisualStyleBackColor = true;
            this.but_OK.Click += new System.EventHandler(this.but_OK_Click);
            // 
            // checkBox_S21ChipY
            // 
            this.checkBox_S21ChipY.AutoSize = true;
            this.checkBox_S21ChipY.Checked = true;
            this.checkBox_S21ChipY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_S21ChipY.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_S21ChipY.Location = new System.Drawing.Point(199, 31);
            this.checkBox_S21ChipY.Name = "checkBox_S21ChipY";
            this.checkBox_S21ChipY.Size = new System.Drawing.Size(139, 43);
            this.checkBox_S21ChipY.TabIndex = 4;
            this.checkBox_S21ChipY.Text = "S21_Y";
            this.checkBox_S21ChipY.UseVisualStyleBackColor = true;
            // 
            // checkBox_S21ChipX
            // 
            this.checkBox_S21ChipX.AutoSize = true;
            this.checkBox_S21ChipX.Checked = true;
            this.checkBox_S21ChipX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_S21ChipX.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_S21ChipX.Location = new System.Drawing.Point(25, 31);
            this.checkBox_S21ChipX.Name = "checkBox_S21ChipX";
            this.checkBox_S21ChipX.Size = new System.Drawing.Size(139, 43);
            this.checkBox_S21ChipX.TabIndex = 3;
            this.checkBox_S21ChipX.Text = "S21_X";
            this.checkBox_S21ChipX.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(837, 23);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox_S22ChipY
            // 
            this.checkBox_S22ChipY.AutoSize = true;
            this.checkBox_S22ChipY.Checked = true;
            this.checkBox_S22ChipY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_S22ChipY.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_S22ChipY.Location = new System.Drawing.Point(199, 103);
            this.checkBox_S22ChipY.Name = "checkBox_S22ChipY";
            this.checkBox_S22ChipY.Size = new System.Drawing.Size(139, 43);
            this.checkBox_S22ChipY.TabIndex = 7;
            this.checkBox_S22ChipY.Text = "S22_Y";
            this.checkBox_S22ChipY.UseVisualStyleBackColor = true;
            // 
            // checkBox_S22ChipX
            // 
            this.checkBox_S22ChipX.AutoSize = true;
            this.checkBox_S22ChipX.Checked = true;
            this.checkBox_S22ChipX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_S22ChipX.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_S22ChipX.Location = new System.Drawing.Point(25, 103);
            this.checkBox_S22ChipX.Name = "checkBox_S22ChipX";
            this.checkBox_S22ChipX.Size = new System.Drawing.Size(139, 43);
            this.checkBox_S22ChipX.TabIndex = 6;
            this.checkBox_S22ChipX.Text = "S22_X";
            this.checkBox_S22ChipX.UseVisualStyleBackColor = true;
            // 
            // Prog_SParamCalibrationGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "Prog_SParamCalibrationGui";
            this.Size = new System.Drawing.Size(1047, 677);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ProgramGui_MsgReceived);
            this.panel1.ResumeLayout(false);
            this.groubBox_VNASetting.ResumeLayout(false);
            this.groubBox_VNASetting.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button but_OK;
        private System.Windows.Forms.CheckBox checkBox_S21ChipY;
        private System.Windows.Forms.CheckBox checkBox_S21ChipX;
        private System.Windows.Forms.GroupBox groubBox_VNASetting;
        private System.Windows.Forms.Label label_StartFreq;
        private System.Windows.Forms.TextBox textBox_StartFreq;
        private System.Windows.Forms.Label label_IFBW;
        private System.Windows.Forms.TextBox textBox_IFBW;
        private System.Windows.Forms.Label label_SweepPoints;
        private System.Windows.Forms.TextBox textBox_SweepPoints;
        private System.Windows.Forms.Label label_StopFreq;
        private System.Windows.Forms.TextBox textBox_StopFreq;
        private System.Windows.Forms.CheckBox checkBox_S22ChipY;
        private System.Windows.Forms.CheckBox checkBox_S22ChipX;

    }
}

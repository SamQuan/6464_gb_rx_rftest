// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// Prog_GroupB_CoherentRx/ProgramGui.cs
// 
// Author: 
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class Prog_SParamCalibrationGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_SParamCalibrationGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Message Received Event Handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ProgramGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            Type t = payload.GetType();
            if (t == typeof(System.String))
            {
                operatorType = payload.ToString();
                this.label1.Text = operatorType;

                if (operatorType == "Technician")
                {
                    this.groubBox_VNASetting.Visible = true;
                }
                else
                {
                    this.groubBox_VNASetting.Visible = false;
                }

            }
        }

        /// <summary>
        /// Expose the protected send to worker to allow other controls to use it!
        /// </summary>
        /// <param name="payload"></param>
        internal void SendToWorker(object payload)
        {
            this.sendToWorker(payload);
        }

        /// <summary>
        /// Indicate that the control has finished
        /// </summary>
        internal void CtrlFinished()
        {
            panel1.Controls.Clear();
        }

        private void but_OK_Click(object sender, EventArgs e)
        {
            DatumList returnData = new DatumList();
            if (operatorType == "Technician")
            {
                returnData.AddDouble("vnaStartFreq_GHz", double.Parse(textBox_StartFreq.Text));
                returnData.AddDouble("vnaStopFreq_GHz", double.Parse(textBox_StopFreq.Text));
                returnData.AddDouble("vnaSweepPoints", int.Parse(textBox_SweepPoints.Text));
                returnData.AddDouble("vnaIFBW_KHz", double.Parse(textBox_IFBW.Text));
            }

            returnData.AddBool("S21XchipSelected", checkBox_S21ChipX.Checked);
            returnData.AddBool("S21YchipSelected", checkBox_S21ChipY.Checked);
            returnData.AddBool("S22XchipSelected", checkBox_S22ChipX.Checked);
            returnData.AddBool("S22YchipSelected", checkBox_S22ChipY.Checked);

            SendToWorker(returnData);
        }

        string operatorType;
    }
}

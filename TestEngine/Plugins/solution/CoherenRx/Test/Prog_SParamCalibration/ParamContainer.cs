using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestPrograms
{
    public class ParamContainer
    {
        public List<double> listS21Data = new List<double>();
        public List<double> listSigSprrData = new List<double>();
        public List<double> listLocSprrData = new List<double>();
        public List<double> listPdCurrentRatio = new List<double>();
        public List<bool> listS21MaskPassFail = new List<bool>();
        public List<bool> listSigSprrMaskPassFail = new List<bool>();
        public List<bool> listLocSprrMaskPassFail = new List<bool>();
        public DatumList listGroupDelayData = new DatumList();
    }
}

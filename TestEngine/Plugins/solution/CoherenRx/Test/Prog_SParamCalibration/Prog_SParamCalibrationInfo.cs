using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.ConherenRx;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_SPC_CoherentRxInfo
    {

        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        internal Specification MainSpec;

        internal CoheRxGAFinalTestConds TestConditions;

        internal Prog_SPC_CoherentRxInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        internal TestSelection TestSelect;
    }
}

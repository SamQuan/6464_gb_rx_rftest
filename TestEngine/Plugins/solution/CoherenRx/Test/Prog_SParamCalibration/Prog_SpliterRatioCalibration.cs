// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_SParamCalibration.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.Algorithms;
using System.Reflection;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
//using Bookham.TestSolution.TestScriptLanguage;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using System.Threading;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_SpliterRatioCal : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_SpliterRatioCal_CoherentRxInfo progInfo;
       // private DatumList postStabResults;
        //private DatumList traceDataList;
        private DateTime testTime_Start;
        private DateTime testTime_End;
        //private double labourTime = 0;
        //private string errorInformation;
        //private bool qualTest;
        string pcasDeviceType;

        //private bool SelectTestFlag;
       // private MultiSpecStatus allTestDataStatus;

        private MeasureSetupManage sigChainSetupManage = new MeasureSetupManage();
        private MeasureSetupManage locChainSetupManage = new MeasureSetupManage();

        //private DatumList listEoSweepData = new DatumList();
        //private DatumList listGroupDelayData = new DatumList();
        //private DatumList listGDelayDeviationData = new DatumList();
        //private List<double> listS21Data = new List<double>();
        //private List<bool> listS21MaskPassFail = new List<bool>();
        //private List<bool> listS22MaskPassFail = new List<bool>();
        //private List<bool> listSigSprrMaskPassFail = new List<bool>();
        //private List<bool> listLocSprrMaskPassFail = new List<bool>();
        //private DatumList listDlpData = new DatumList();
        //private List<double> listSigSprrData = new List<double>();
        //private List<double> listLocSprrData = new List<double>();
        //private List<double> listPdCurrentRatio = new List<double>();
        private DatumList fcuNoiseCurrentList = new DatumList();
        //private DatumList listPwrAndPdCurrentSprrTest = new DatumList();
        ////private double[] s21SweepFreqArray;
        //private List<ParamContainer> ListParamContainer = new List<ParamContainer>();


        private List<double> listTestPower_dB = new List<double>();
        List<double> CalWavelengList = new List<double>();


        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_SpliterRatioCal()
        {
            this.progInfo = new Prog_SpliterRatioCal_CoherentRxInfo();
        }

        public Type UserControl
        {
            get { return typeof(Prog_SpliterRatioCalGui); }
        }

        #region Program Initialisation
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs,
            ChassisCollection chassis)
        {
            foreach (Chassis aChassis in chassis.Values)
            {
                aChassis.EnableLogging = false;
            }
            foreach (Instrument var in instrs.Values)
            {
                var.EnableLogging = false;
            }

            testTime_Start = DateTime.Now;

            string[] DeviceType = dutObject.PartCode.Split('-');
            pcasDeviceType = DeviceType[0];


            // initialise config
            this.initConfig(dutObject, engine);

            // initialise instruments
            this.initInstrs(engine, instrs, chassis);

            //initialise Optical Chain Setup Manage
            InitializeOpticalChainSetupManage();

            // load specification
            this.loadSpecs(engine, dutObject);

            // init modules
            this.SpliterRatioCalibration_InitModule(engine, instrs, opticalPathEnum.SIG);
            this.SpliterRatioCalibration_InitModule(engine, instrs, opticalPathEnum.LOC);
        }

        /// <summary>
        /// Initialise optical chain settup manage class
        /// </summary>
        private void InitializeOpticalChainSetupManage()
        {
            RxSplitterRatioCalData splitterRatioCalData_Sig;
            RxSplitterRatioCalData splitterRatioCalData_Loc;
            string splitterName_Sig;
            string configFilePath_Sig;
            string dataTableName_Sig;
            string splitterName_Loc;
            string configFilePath_Loc;
            string dataTableName_Loc;

            splitterName_Sig = "SigPathSplitter";
            configFilePath_Sig = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\SplitterRatioCalSettings.xml";
            dataTableName_Sig = "DataBaseSettings";
            splitterName_Loc = "LocPathSplitter";
            configFilePath_Loc = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\SplitterRatioCalSettings.xml";
            dataTableName_Loc = "DataBaseSettings";
            splitterRatioCalData_Sig = new RxSplitterRatioCalData(splitterName_Sig, configFilePath_Sig, dataTableName_Sig);
            splitterRatioCalData_Loc = new RxSplitterRatioCalData(splitterName_Loc, configFilePath_Loc, dataTableName_Loc);

            //sigChainSetupManage.AuxPd = progInfo.Instrs.AuxPd;
            sigChainSetupManage.LightwaveComponentAnalyzer = progInfo.Instrs.LightwaveComponentAnalyzer;
            sigChainSetupManage.OPM_Mon = progInfo.Instrs.OPMMon_Sig;
            sigChainSetupManage.OPM_Ref = progInfo.Instrs.OPMRef_Sig;
            sigChainSetupManage.PdBias = progInfo.Instrs.PdSource;
            sigChainSetupManage.PolCtrl = progInfo.Instrs.PolController_Sig;
            sigChainSetupManage.VOA = progInfo.Instrs.VOA_Sig;
            sigChainSetupManage.DelayLine = progInfo.Instrs.DelayLine_Sig;
            sigChainSetupManage.LockInAmplifier = progInfo.Instrs.LockInAmplifier;
            if (progInfo.Instrs.laserSource != null)
            {
                sigChainSetupManage.LaserSource = progInfo.Instrs.laserSource;
            }
            sigChainSetupManage.SplitterRatiosCalData = splitterRatioCalData_Sig;
            sigChainSetupManage.SplitterRatiosCalData.FrequencyTolerance_Ghz = 0.05;
            sigChainSetupManage.FcuNoiseCurrentList = fcuNoiseCurrentList;

            //locChainSetupManage.AuxPd = progInfo.Instrs.AuxPd;
            locChainSetupManage.LightwaveComponentAnalyzer = progInfo.Instrs.LightwaveComponentAnalyzer;
            locChainSetupManage.OPM_Mon = progInfo.Instrs.OPMMon_Loc;
            locChainSetupManage.OPM_Ref = progInfo.Instrs.OPMRef_Loc;
            locChainSetupManage.PdBias = progInfo.Instrs.PdSource;
            locChainSetupManage.PolCtrl = progInfo.Instrs.PolController_Loc;
            locChainSetupManage.VOA = progInfo.Instrs.VOA_Loc;
            locChainSetupManage.DelayLine = progInfo.Instrs.DelayLine_Loc;
            locChainSetupManage.LockInAmplifier = progInfo.Instrs.LockInAmplifier;
            if (progInfo.Instrs.laserSource != null)
            {
                locChainSetupManage.LaserSource = progInfo.Instrs.laserSource;
            }
            locChainSetupManage.SplitterRatiosCalData = splitterRatioCalData_Loc;
            locChainSetupManage.SplitterRatiosCalData.FrequencyTolerance_Ghz = 0.05;
            locChainSetupManage.FcuNoiseCurrentList = fcuNoiseCurrentList;
        }

        /// <summary>
        /// initialise configuration
        /// </summary>
        /// <param name="dutObj"></param>
        /// <param name="engine"></param>
        private void initConfig(DUTObject dutObj, ITestEngineInit engine)
        {
            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObj,
                @"Configuration\ConherentRx\CohRxSpliterRatioCal.xml", "", "CoheRxTestParams");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObj, 1,
                @"Configuration\ConherentRx\TempTable.xml");

            progInfo.TestSelect = new TestSelection(@"Configuration\ConherentRx\TestSelect.xml");
        }

        /// <summary>
        /// load Specification
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string mainSpecNameFile = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileName");
                string mainSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + mainSpecNameFile;
                mainSpecKeys.Add("Filename", mainSpecFullFilename);

            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();
                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);//"final"
                mainSpecKeys.Add("DEVICE_TYPE", pcasDeviceType);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);

            // Get our specification object (so we can initialise modules with appropriate limits)
            progInfo.MainSpec = tempSpecList[0];

            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);

            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new CoheRxGAFinalTestConds(progInfo.MainSpec);

            listTestPower_dB.Add(progInfo.TestConditions.zeroDBRefPower_Sig_dBm);
        }

        /// <summary>
        /// initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        private void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {

            Prog_SpliterRatioCal_CoherentRxInstruments progInstrs = new Prog_SpliterRatioCal_CoherentRxInstruments();

            if (instrs.Contains("LaserSource"))
            {
                progInstrs.laserSource = (Inst_iTLATunableLaserSource)instrs["LaserSource"];
            }

            //TIA
            progInstrs.Tia_XI = new TiaInstrument();
            progInstrs.Tia_XQ = new TiaInstrument();
            progInstrs.Tia_YI = new TiaInstrument();
            progInstrs.Tia_YQ = new TiaInstrument();
            //TIA_XI
            progInstrs.Tia_XI.VccSupply = (Inst_SMUTI_TriggeredSMU)instrs["VccSupply_TiaXI"];
            if (instrs.Contains("OutputCtrl_TiaXI"))
            {
                progInstrs.Tia_XI.OutputCtrl = (Inst_SMUTI_TriggeredSMU)instrs["OutputCtrl_TiaXI"];
            }
            
            progInstrs.Tia_XI.OutputAmplitudeCtrlInAGC = (Inst_SMUTI_TriggeredSMU)instrs["OutputAmpCtrlInAGC_TiaXI"];
            progInstrs.Tia_XI.Vgc = (Inst_SMUTI_TriggeredSMU)instrs["VGC_TIAXI"];
            if (instrs.Contains("ModeCtrlOfGC_TiaXI"))
            {
                progInstrs.Tia_XI.ModeCtrlOfGC = (Inst_SMUTI_TriggeredSMU)instrs["ModeCtrlOfGC_TiaXI"];
            }
            if (instrs.Contains("PeakVoltDetector_TiaXI"))
            {
                progInstrs.Tia_XI.PeakVoltDetector = (Inst_SMUTI_TriggeredSMU)instrs["PeakVoltDetector_TiaXI"];
            }
            if (instrs.Contains("BandWidth_H_TiaX"))
            {
                progInstrs.Tia_XI.BandWidth_H = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_H_TiaX"];
                progInstrs.Tia_XI.BandWidth_L = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_L_TiaX"];
            }

            //TIA_XQ
            progInstrs.Tia_XQ.VccSupply = (Inst_SMUTI_TriggeredSMU)instrs["VccSupply_TiaXQ"];
            if (instrs.Contains("OutputCtrl_TiaXQ"))
            {
                progInstrs.Tia_XQ.OutputCtrl = (Inst_SMUTI_TriggeredSMU)instrs["OutputCtrl_TiaXQ"];
            }
            progInstrs.Tia_XQ.OutputAmplitudeCtrlInAGC = (Inst_SMUTI_TriggeredSMU)instrs["OutputAmpCtrlInAGC_TiaXQ"];
            progInstrs.Tia_XQ.Vgc = (Inst_SMUTI_TriggeredSMU)instrs["VGC_TIAXQ"];
            if (instrs.Contains("ModeCtrlOfGC_TiaXI"))
            {
                progInstrs.Tia_XQ.ModeCtrlOfGC = (Inst_SMUTI_TriggeredSMU)instrs["ModeCtrlOfGC_TiaXI"];
            }
            if (instrs.Contains("PeakVoltDetector_TiaXQ"))
            {
                progInstrs.Tia_XQ.PeakVoltDetector = (Inst_SMUTI_TriggeredSMU)instrs["PeakVoltDetector_TiaXQ"];
            }
            if (instrs.Contains("BandWidth_H_TiaX"))
            {
                progInstrs.Tia_XQ.BandWidth_H = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_H_TiaX"];
                progInstrs.Tia_XQ.BandWidth_L = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_L_TiaX"];
            }

            //TIA_YI
            progInstrs.Tia_YI.VccSupply = (Inst_SMUTI_TriggeredSMU)instrs["VccSupply_TiaYI"];
            if (instrs.Contains("OutputCtrl_TiaYI"))
            {
                progInstrs.Tia_YI.OutputCtrl = (Inst_SMUTI_TriggeredSMU)instrs["OutputCtrl_TiaYI"];
            }
            progInstrs.Tia_YI.OutputAmplitudeCtrlInAGC = (Inst_SMUTI_TriggeredSMU)instrs["OutputAmpCtrlInAGC_TiaXI"];
            progInstrs.Tia_YI.Vgc = (Inst_SMUTI_TriggeredSMU)instrs["VGC_TiaYI"];
            if (instrs.Contains("ModeCtrlOfGC_TiaYI"))
            {
                progInstrs.Tia_YI.ModeCtrlOfGC = (Inst_SMUTI_TriggeredSMU)instrs["ModeCtrlOfGC_TiaYI"];
            }
            if (instrs.Contains("PeakVoltDetector_TiaYI"))
            {
                progInstrs.Tia_YI.PeakVoltDetector = (Inst_SMUTI_TriggeredSMU)instrs["PeakVoltDetector_TiaYI"];
            }
            if (instrs.Contains("BandWidth_H_TiaY"))
            {
                progInstrs.Tia_YI.BandWidth_H = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_H_TiaY"];
                progInstrs.Tia_YI.BandWidth_L = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_L_TiaY"];
            }

            //TIA_YQ
            progInstrs.Tia_YQ.VccSupply = (Inst_SMUTI_TriggeredSMU)instrs["VccSupply_TiaYQ"];
            if (instrs.Contains("OutputCtrl_TiaYQ"))
            {
                progInstrs.Tia_YQ.OutputCtrl = (Inst_SMUTI_TriggeredSMU)instrs["OutputCtrl_TiaYQ"];
            }
            progInstrs.Tia_YQ.OutputAmplitudeCtrlInAGC = (Inst_SMUTI_TriggeredSMU)instrs["OutputAmpCtrlInAGC_TiaXQ"];
            progInstrs.Tia_YQ.Vgc = (Inst_SMUTI_TriggeredSMU)instrs["VGC_TiaYQ"];
            if (instrs.Contains("ModeCtrlOfGC_TiaYQ"))
            {
                progInstrs.Tia_YQ.ModeCtrlOfGC = (Inst_SMUTI_TriggeredSMU)instrs["ModeCtrlOfGC_TiaYQ"];
            }
            if (instrs.Contains("PeakVoltDetector_TiaYQ"))
            {
                progInstrs.Tia_YQ.PeakVoltDetector = (Inst_SMUTI_TriggeredSMU)instrs["PeakVoltDetector_TiaYQ"];
            }
            if (instrs.Contains("BandWidth_H_TiaY"))
            {
                progInstrs.Tia_YQ.BandWidth_H = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_H_TiaY"];
                progInstrs.Tia_YQ.BandWidth_L = (Inst_SMUTI_TriggeredSMU)instrs["BandWidth_L_TiaY"];
            }

            //pd bias
            progInstrs.PdSource = new PdBiasInstrument();
            progInstrs.PdSource.PdSource_XIpos = (InstType_ElectricalSource)instrs["XIpos"];
            progInstrs.PdSource.PdSource_XIneg = (InstType_ElectricalSource)instrs["XIneg"];
            progInstrs.PdSource.PdSource_XQpos = (InstType_ElectricalSource)instrs["XQpos"];
            progInstrs.PdSource.PdSource_XQneg = (InstType_ElectricalSource)instrs["XQneg"];
            progInstrs.PdSource.PdSource_YIpos = (InstType_ElectricalSource)instrs["YIpos"];
            progInstrs.PdSource.PdSource_YIneg = (InstType_ElectricalSource)instrs["YIneg"];
            progInstrs.PdSource.PdSource_YQpos = (InstType_ElectricalSource)instrs["YQpos"];
            progInstrs.PdSource.PdSource_YQneg = (InstType_ElectricalSource)instrs["YQneg"];

            //MZM power supply
            //if (instrs.Contains("MzmPwrSupply"))
            //{
            //    progInstrs.MzmSupply = (Inst_SMUTI_TriggeredSMU)instrs["MzmPwrSupply"];
            //}

            // TECs
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // OPMs
            progInstrs.OPMMon_Sig = (InstType_OpticalPowerMeter)instrs["OpmMon_Sig"];
            progInstrs.OPMMon_Loc = (InstType_OpticalPowerMeter)instrs["OpmMon_Loc"];
            progInstrs.OPMRef_Sig = (InstType_OpticalPowerMeter)instrs["OpmRef_Sig"];
            progInstrs.OPMRef_Loc = (InstType_OpticalPowerMeter)instrs["OpmRef_Loc"];

            //LightwaveComponentAnalyzer
            progInstrs.LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)instrs["LightwaveAnalyzer"];

            //WaveGenerator
            progInstrs.WaveformGenerator = (Instr_Ag33120A)instrs["WaveGenerator"];

            //locker in amplifier
            progInstrs.LockInAmplifier = (Inst_SR830)instrs["LockInAmplifier"];

            //VOA
            progInstrs.VOA_Sig = (Inst_OzDd100mc_Attenuator)instrs["VOA_Sig"];
            progInstrs.VOA_Loc = (Inst_OzDd100mc_Attenuator)instrs["VOA_Loc"];

            if (instrs.Contains("VOAForCutOffInput_Sig"))
            {
                progInstrs.VOAForCutOffPwrInput_Sig = (Inst_OzDd100mc_Attenuator)instrs["VOAForCutOffInput_Sig"];
            }
            if (instrs.Contains("VOAForCutOffInput_Loc"))
            {
                progInstrs.VOAForCutOffPwrInput_Loc = (Inst_OzDd100mc_Attenuator)instrs["VOAForCutOffInput_Loc"];
            }

            //delay line
            progInstrs.DelayLine_Sig = (InstType_ODL)instrs["DelayLine_Sig"];
            progInstrs.DelayLine_Loc = (InstType_ODL)instrs["DelayLine_Loc"];

            //sig pol controller
            progInstrs.PolController_Sig = new PolarizeController();
            progInstrs.PolController_Sig.Channel1 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel1"];
            progInstrs.PolController_Sig.Channel2 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel2"];
            progInstrs.PolController_Sig.Channel3 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel3"];
            progInstrs.PolController_Sig.Channel4 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel4"];
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel1);
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel2);
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel3);
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel4);

            //loc pol controller
            progInstrs.PolController_Loc = new PolarizeController();
            progInstrs.PolController_Loc.Channel1 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel1"];
            progInstrs.PolController_Loc.Channel2 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel2"];
            progInstrs.PolController_Loc.Channel3 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel3"];
            progInstrs.PolController_Loc.Channel4 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel4"];
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel1);
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel2);
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel3);
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel4);


            // RFswitch path management
            Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
                (@"Configuration\ConherentRx\RfSwitch.xml", instrs);
            Switch_LCA_RF switchRF = new Switch_LCA_RF(switchPathManager);
            progInstrs.RfSwitch = switchRF;

            progInfo.Instrs = progInstrs;

            if (!engine.IsSimulation)
            {
                // Configure TEC controllers
                //ConfigureTecController(progInstrs.TecCase, "TecCase");

                //configure lightWave analyser
                ConfigureLightwave(progInstrs.LightwaveComponentAnalyzer);

                //configure power meter
                progInstrs.OPMMon_Sig.SetDefaultState();
                progInstrs.OPMMon_Loc.SetDefaultState();
                progInstrs.OPMRef_Sig.SetDefaultState();
                progInstrs.OPMRef_Loc.SetDefaultState();

                //configure Wave generator
                progInstrs.WaveformGenerator.SetDefaultState();


                //configure mzm supply
                //progInstrs.MzmSupply.SetDefaultState();
                //progInstrs.MzmSupply.OutputEnabled = false;

                //configure locker in amplifier
                progInstrs.LockInAmplifier.SetDefaultState();

                //configure pol,disable pol ctrl
                ConfigurePolController(progInstrs.PolController_Sig);
                ConfigurePolController(progInstrs.PolController_Loc);

                //configure laser source supply
                if (progInfo.Instrs.laserSource != null)
                {
                    progInstrs.laserSource.SetDefaultState();
                }
                //configure TIA_XI controllers
                ConfigureTiaController(progInstrs.Tia_XI.Vgc);
                ConfigureTiaController(progInstrs.Tia_XI.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_XI.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_XI.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_XI.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_XI.VccSupply);

                //configure TIA_XQ controllers
                ConfigureTiaController(progInstrs.Tia_XQ.Vgc);
                ConfigureTiaController(progInstrs.Tia_XQ.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_XQ.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_XQ.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_XQ.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_XQ.VccSupply);

                //configure TIA_YI controllers
                ConfigureTiaController(progInstrs.Tia_YI.Vgc);
                ConfigureTiaController(progInstrs.Tia_YI.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_YI.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_YI.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_YI.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_YI.VccSupply);

                //configure TIA_YQ controllers
                ConfigureTiaController(progInstrs.Tia_YQ.Vgc);
                ConfigureTiaController(progInstrs.Tia_YQ.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_YQ.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_YQ.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_YQ.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_YQ.VccSupply);

                //configure dc bias pd
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XIpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XIneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XQpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XQneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YIpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YIneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YQpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YQneg);

                //configure TIA band width ctrl
                ConfigureTiaBandWidthController(progInstrs.Tia_XI);
                ConfigureTiaBandWidthController(progInstrs.Tia_XQ);
                ConfigureTiaBandWidthController(progInstrs.Tia_YI);
                ConfigureTiaBandWidthController(progInstrs.Tia_YQ);

                //off all rf connet to vna
                progInstrs.RfSwitch.SetState(Switch_LCA_RF.State.OffConnent);

                //init voa to max attenuation
                progInstrs.VOA_Sig.SetDefaultState();
                progInstrs.VOA_Loc.SetDefaultState();
                if (progInstrs.VOAForCutOffPwrInput_Sig != null)
                {
                    progInstrs.VOAForCutOffPwrInput_Sig.SetDefaultState();
                }
                if (progInstrs.VOAForCutOffPwrInput_Loc != null)
                {
                    progInstrs.VOAForCutOffPwrInput_Loc.SetDefaultState();
                }

                //init delay lines
               // progInstrs.DelayLine_Sig.SetDefaultState();
               // progInstrs.DelayLine_Loc.SetDefaultState();

                //MeasureFcuNoiseCurrent(engine, progInstrs);
            }

            engine.SendStatusMsg("Instruments initialised");
        }

        #endregion

        #region Program Running

        public void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //ModuleRun modRun;

            //ModuleRunReturn moduleRunReturn;

            ButtonId respond = ButtonId.Cancel;
            progInfo.Instrs.VOAForCutOffPwrInput_Sig.OutputEnabled = true;
            progInfo.Instrs.VOAForCutOffPwrInput_Sig.Attenuation_dB = 0;
            progInfo.Instrs.VOAForCutOffPwrInput_Loc.OutputEnabled = true;
            progInfo.Instrs.VOAForCutOffPwrInput_Loc.Attenuation_dB = 0;
            sigChainSetupManage.LaserSource.BeamEnable = true;

            do
            {
                locChainSetupManage.VOA.OutputEnabled = false;
                ModuleRun moduleRun = engine.GetModuleRun("Mod_SplitterRatioCal" + opticalPathEnum.SIG);
                moduleRun.ConfigData.AddOrUpdateString("opticalPath", opticalPathEnum.SIG.ToString());
                engine.RunModule("Mod_SplitterRatioCal" + opticalPathEnum.SIG);

                locChainSetupManage.VOA.OutputEnabled = true;
                sigChainSetupManage.VOA.OutputEnabled = false;
                Thread.Sleep(5000);
                moduleRun = engine.GetModuleRun("Mod_SplitterRatioCal" + opticalPathEnum.LOC);
                moduleRun.ConfigData.AddOrUpdateString("opticalPath", opticalPathEnum.LOC.ToString());
                engine.RunModule("Mod_SplitterRatioCal" + opticalPathEnum.LOC);


                InitializeOpticalChainSetupManage();

                bool calibratePassFail = true;
                for (int i = 0; i < CalWavelengList.Count; i++)
                {
                    double spliterRatio_Sig = sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(CalWavelengList[i]);
                    double spliterRatio_Loc = locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(CalWavelengList[i]);
                    if (!(spliterRatio_Sig > 8 && spliterRatio_Sig < 10 &&
                        spliterRatio_Loc > 8 && spliterRatio_Loc < 10))
                    {
                        calibratePassFail = false;
                        break;
                    }
                }

                respond = ButtonId.Cancel;
                if (!calibratePassFail)
                {
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("Redo calibration!", ButtonId.Continue),
                                            new ButtonInfo("Cancel calibration,but the calibration data was modify!",ButtonId.Cancel),
                                       };
                    respond = (ButtonId)engine.ShowUserQuery("Calibration fail��", buttonsInfo);
                }
            } while (respond != ButtonId.Cancel);
        }


        #endregion

        #region End of Program

        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                // Display temperature progress
                engine.GuiShow();
                engine.GuiToFront();

                engine.SendStatusMsg("close all input!");

                this.progInfo.Instrs.VOA_Sig.OutputEnabled = false;
                this.progInfo.Instrs.VOA_Loc.OutputEnabled = false;

                if (this.progInfo.Instrs.laserSource != null)
                {
                    this.progInfo.Instrs.laserSource.BeamEnable = false;
                }

                foreach (Inst_SMUTI_TriggeredSMU chan in this.progInfo.Instrs.PolController_Sig.Allchans)
                {
                    chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                }
                foreach (Inst_SMUTI_TriggeredSMU chan in this.progInfo.Instrs.PolController_Loc.Allchans)
                {
                    chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                }

                this.progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.OffConnent);
                //step1: power off Vgc and other control inputs
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_XI);
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_XQ);
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_YI);
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_YQ);
                //step2: power off Vcc
                this.progInfo.Instrs.Tia_XI.VccSupply.OutputEnable =
                     Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                this.progInfo.Instrs.Tia_XQ.VccSupply.OutputEnable =
                    Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                this.progInfo.Instrs.Tia_YI.VccSupply.OutputEnable =
                     Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                this.progInfo.Instrs.Tia_YQ.VccSupply.OutputEnable =
                    Inst_SMUTI_TriggeredSMU.OutputState.OFF;

                //step3: power off Vpd
                SetPdBiasOFF();

                //SetCaseTempSafe(engine, 25);
                this.progInfo.Instrs.TecCase.OutputEnabled = false;
            }
        }

        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {

            // Write keys required for external data (example below for PCAS)    
            DatumList tcTraceData = new DatumList();

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            StringDictionary keys = new StringDictionary();

            // TODO: MUST Add real values below!

            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("DEVICE_TYPE", pcasDeviceType);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", progInfo.MainSpec.Name);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);

            // Tell Test Engine about it...

            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)

            // !(beware, all these parameters MUST exist in the specification)!...

            //add testTime
            testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(testTime_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            dutObject.ProgramPluginVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            tcTraceData.AddString("SOFTWARE_IDENT", dutObject.ProgramPluginName + dutObject.ProgramPluginVersion);
            tcTraceData.AddString("OPERATOR_ID", userList.UserListString);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PART_CODE", dutObject.PartCode);
            tcTraceData.AddString("SPEC_ID", progInfo.MainSpec.Name);
            tcTraceData.AddSint32("NODE", dutObject.NodeID);
            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);

            string timeDate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            tcTraceData.AddString("TIME_DATE", timeDate);


            //if (File.Exists(rawDataFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_S21_WL"+, rawDataFile);
            //}
            //if (File.Exists(dlpPlotFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_DLP", dlpPlotFile);
            //}
            //if (File.Exists(gDelayPlotFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_GD", gDelayPlotFile);
            //}
            //if (File.Exists(sprrChipXCurrentFile))
            //{
            //    tcTraceData.AddFileLink("SPRR_POWER_IP_X_FILE", sprrChipXCurrentFile);
            //}
            //if (File.Exists(sprrChipYCurrentFile))
            //{
            //    tcTraceData.AddFileLink("SPRR_POWER_IP_Y_FILE", sprrChipYCurrentFile);
            //}
            //if (File.Exists(s21StatisticsFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_S21_STAT", s21StatisticsFile);
            //}
            //if (File.Exists(sprrStatisticsFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_SPRR_STAT", sprrStatisticsFile);
            //}
            //if (File.Exists(s21MaskPassFailFile))
            //{
            //    tcTraceData.AddFileLink("S21_MASK_FILE", s21MaskPassFailFile);
            //}
            //if (File.Exists(sprrMaskPassFailFile))
            //{
            //    tcTraceData.AddFileLink("SPRR_MASK_FILE", sprrMaskPassFailFile);
            //}
            //if (File.Exists(pdCurrentRatioFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_CURRENT_RATIO_STAT", pdCurrentRatioFile);
            //}

            //if (File.Exists(s21CurrentChipXFile))
            //{
            //    tcTraceData.AddFileLink("S21_POWER_IP_X_FILE", s21CurrentChipXFile);
            //}
            //if (File.Exists(s21CurrentChipYFile))
            //{
            //    tcTraceData.AddFileLink("S21_POWER_IP_Y_FILE", s21CurrentChipYFile);
            //}
            if (engine.GetProgramRunComments() != ProgramStatus.Success.ToString())
            {
                tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunComments());
            }
            else
            {
                tcTraceData.AddString("TEST_STATUS", progInfo.MainSpec.Status.Status.ToString());
            }

            // pick the specification to add this data to...

            engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
        }

        #endregion

        #region Instrument Initialise configure

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            // Keithley 2510 specific commands (must be done before 'SetDefaultState')
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");
            }
            // Operating modes

            tecCtlr.SetDefaultState();
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
                Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
                    Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));
            }


            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
            //tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
            tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
            tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            //tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
            tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
        }
        /// <summary>
        /// Initialisation method for the 8703A Lightwave.
        /// Test Module assumes that the Test Program does this work.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrumentSet"></param>
        /// <param name="progConfigData"></param>
        private void ConfigureLightwave(InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer)
        {
            if (LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                LightwaveComponentAnalyzer.SetDefaultState();
            }
            else
            {
                string vnaSetupFile = "'c:\\testset\\cal\\XChipSetup_S22.chx'";

                Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
                MS4046A.RecalSetup(vnaSetupFile);
                Thread.Sleep(3000);
                MS4046A.ActiveMeausurementChannel = 0;
                
            }
        }
        /// <summary>
        /// confiugre pol controller
        /// </summary>
        /// <param name="polCtrl"></param>
        private static void ConfigurePolController(PolarizeController polCtrl)
        {
            foreach (Inst_SMUTI_TriggeredSMU chan in polCtrl.Allchans)
            {
                chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                chan.VoltageSetPoint_Volt = 0.0;
                chan.CurrentSetPoint_amp = 0.0;
            }
        }
        /// <summary>
        /// configure tia bandwidth controller
        /// </summary>
        /// <param name="TiaCtrl"></param>
        private static void ConfigureTiaBandWidthController(TiaInstrument TiaCtrl)
        {
            if (TiaCtrl.BandWidth_H != null)
            {
                TiaCtrl.BandWidth_H.CurrentComplianceSetPoint_Amp = 0.0;
                TiaCtrl.BandWidth_H.VoltageSetPoint_Volt = 0.0;
                TiaCtrl.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                TiaCtrl.BandWidth_L.CurrentComplianceSetPoint_Amp = 0.0;
                TiaCtrl.BandWidth_L.VoltageSetPoint_Volt = 0.0;
                TiaCtrl.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tiaCtrl"></param>
        /// <param name="TiaCtlId"></param>
        private void ConfigureTiaController(Inst_SMUTI_TriggeredSMU tiaCtrl)
        {
            if (tiaCtrl != null)
            {
                tiaCtrl.CurrentSetPoint_amp = 0.0;
                tiaCtrl.VoltageSetPoint_Volt = 0.0;
                tiaCtrl.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="auxCtrl"></param>
        /// <param name="auxCtrlId"></param>
        private void ConfigureAuxBaisController(Inst_SMUTI_TriggeredSMU auxCtrl)
        {
            // auxCtrl.SetDefaultState();
            auxCtrl.CurrentComplianceSetPoint_Amp = 0.0;
            auxCtrl.VoltageSetPoint_Volt = 0.0;
            auxCtrl.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="smuX"></param>
        /// <param name="p"></param>
        private void ConfigurePdBiasController(InstType_ElectricalSource pdBias)
        {
            // pdBias.SetDefaultState();
            pdBias.CurrentComplianceSetPoint_Amp = 0.002;
            pdBias.VoltageSetPoint_Volt = 0.0;
            pdBias.OutputEnabled = false;
        }
        #endregion

        #region Test module initialise Functions

        private void SpliterRatioCalibration_InitModule(ITestEngineInit engine, InstrumentCollection instrs,opticalPathEnum path)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_SplitterRatioCal" + path.ToString(), "Mod_SplitterRatioCal", "");
            modRun.Instrs.Add("OPMeter", progInfo.Instrs.OPMMon_Sig);

            if (path == opticalPathEnum.SIG)
            {
                modRun.ConfigData.AddReference("ChainSetupManage", sigChainSetupManage);
            }
            else
            {
                modRun.ConfigData.AddReference("ChainSetupManage", locChainSetupManage);
            }
            
            modRun.ConfigData.AddDouble("SR_Max", progInfo.TestParamsConfig.GetDoubleParam("SR_Max"));
            modRun.ConfigData.AddDouble("SR_Min", progInfo.TestParamsConfig.GetDoubleParam("SR_Min"));
            modRun.ConfigData.AddDouble("VoaInitAtt", progInfo.TestParamsConfig.GetDoubleParam("VoaInitAtt"));
            modRun.ConfigData.AddDouble("LaserSourcePower_dBm", progInfo.TestParamsConfig.GetDoubleParam("LaserSourcePower_dBm"));
            modRun.ConfigData.AddString("GuiTitle", progInfo.TestParamsConfig.GetStringParam("GuiTitle"));

            if (CalWavelengList.Count==0)
            {
                CalWavelengList.Add(progInfo.TestParamsConfig.GetDoubleParam("Wavelen1_nm"));
                CalWavelengList.Add(progInfo.TestParamsConfig.GetDoubleParam("Wavelen2_nm"));
                CalWavelengList.Add(progInfo.TestParamsConfig.GetDoubleParam("wavelen3_nm"));
            }
            modRun.ConfigData.AddDoubleArray("CalWavelengList", CalWavelengList.ToArray());
            
        }

        #endregion

        #region Private Helper Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="progInstrs"></param>
        private void MeasureFcuNoiseCurrent(ITestEngineInit engine, Prog_SpliterRatioCal_CoherentRxInstruments progInstrs)
        {


            double currentXIP_mA = 0.0;
            double currentXIN_mA = 0.0;
            double currentXQP_mA = 0.0;
            double currentXQN_mA = 0.0;
            double currentYIP_mA = 0.0;
            double currentYIN_mA = 0.0;
            double currentYQP_mA = 0.0;
            double currentYQN_mA = 0.0;

            SetPdBias(engine, 5, 2.0);

            System.Threading.Thread.Sleep(1000);

            //measure Xchip pd dark current 10 times
            if (progInstrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                UsedFrontOrBackTerminals(useFrontTerminals);
            }
            Thread.Sleep(1000);
            for (int i = 0; i < 10; i++)
            {
                currentXIP_mA += progInstrs.PdSource.PdSource_XIpos.CurrentActual_amp * 1000;
                currentXIN_mA += progInstrs.PdSource.PdSource_XIneg.CurrentActual_amp * 1000;
                currentXQP_mA += progInstrs.PdSource.PdSource_XQpos.CurrentActual_amp * 1000;
                currentXQN_mA += progInstrs.PdSource.PdSource_XQneg.CurrentActual_amp * 1000;
            }
           
            //measure Ychip pd dark current 10 times
            if (progInstrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                UsedFrontOrBackTerminals(!useFrontTerminals);
            }
            Thread.Sleep(1000);
            for (int i = 0; i < 10; i++)
            {
                currentYIP_mA += progInstrs.PdSource.PdSource_YIpos.CurrentActual_amp * 1000;
                currentYIN_mA += progInstrs.PdSource.PdSource_YIneg.CurrentActual_amp * 1000;
                currentYQP_mA += progInstrs.PdSource.PdSource_YQpos.CurrentActual_amp * 1000;
                currentYQN_mA += progInstrs.PdSource.PdSource_YQneg.CurrentActual_amp * 1000;
            }

            currentXIP_mA /= 10;
            currentXIN_mA /= 10;
            currentXQP_mA /= 10;
            currentXQN_mA /= 10;
            currentYIP_mA /= 10;
            currentYIN_mA /= 10;
            currentYQP_mA /= 10;
            currentYQN_mA /= 10;

            fcuNoiseCurrentList.AddDouble("XIP", currentXIP_mA);
            fcuNoiseCurrentList.AddDouble("XIN", currentXIN_mA);
            fcuNoiseCurrentList.AddDouble("XQP", currentXQP_mA);
            fcuNoiseCurrentList.AddDouble("XQN", currentXQN_mA);
            fcuNoiseCurrentList.AddDouble("YIP", currentYIP_mA);
            fcuNoiseCurrentList.AddDouble("YIN", currentYIN_mA);
            fcuNoiseCurrentList.AddDouble("YQP", currentYQP_mA);
            fcuNoiseCurrentList.AddDouble("YQN", currentYQN_mA);

            SetPdBiasOFF();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetPdBiasOFF()
        {
            progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tia"></param>
        private void SetTiaCtrlVoltageOff(TiaInstrument Tia)
        {
            if (Tia.BandWidth_H != null)
            {
                Tia.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                Tia.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
            if (Tia.ModeCtrlOfGC != null)
            {
                Tia.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
            if (Tia.OutputAmplitudeCtrlInAGC != null)
            {
                Tia.OutputAmplitudeCtrlInAGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
            if (Tia.OutputCtrl != null)
            {
                Tia.OutputCtrl.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
            if (Tia.Vgc != null)
            {
                Tia.Vgc.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
            }
        }

        /// <summary>
 /// <summary>
        /// 
        /// </summary>
        /// <param name="useFrontTerminals"></param>
        private void UsedFrontOrBackTerminals(bool useFrontTerminals)
        {
            Inst_Ke24xx pdBias;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XIpos;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XIneg;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XQpos;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XQneg;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
        }

        /// <summary>
        /// set pd bias volt
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="voltSet_V">pd bias volt</param>
        /// <param name="currentCompliance_mA">pd bias compliance current</param>
        private void SetPdBias(ITestEngineInit engine, double voltSet_V, double currentCompliance_mA)
        {

            progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = true;

            progInfo.Instrs.PdSource.PdSource_XIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;

            if (!progInfo.Instrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                int counts = (int)(voltSet_V / 0.2);
                for (int i = 1; i <= counts; i++)
                {
                    double voltageSet_V = 0.2 * i;
                    progInfo.Instrs.PdSource.PdSource_XIpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XIneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XQpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XQneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YIpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YIneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YQpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YQneg.VoltageSetPoint_Volt = voltageSet_V;

                    if (i == 1)
                    {
                        List<double> voltSensList_V = new List<double>();
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XIpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XIneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XQpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XQneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YIpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YIneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YQpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YQneg.VoltageActual_Volt);
                        foreach (double volt in voltSensList_V)
                        {
                            if (Math.Abs(volt - voltageSet_V) > 0.1)
                            {
                                progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = false;

                                engine.ErrorInProgram("pd bias error,please check FCU2 pd bias whether demang");
                            }
                        }
                    }

                    Thread.Sleep(50);
                }
            }
            progInfo.Instrs.PdSource.PdSource_XIpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XIneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XQpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XQneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YIpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YIneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YQpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YQneg.VoltageSetPoint_Volt = voltSet_V;

        }

        /// <summary>
        /// set fci noise current to trace
        /// </summary>
        private void SetFcuNoiseTraceData()
        {
            DatumList fcuNoiseTraceDataList = new DatumList();
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XIP", fcuNoiseCurrentList.ReadDouble("XIP"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XIN", fcuNoiseCurrentList.ReadDouble("XIN"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XQP", fcuNoiseCurrentList.ReadDouble("XQP"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XQN", fcuNoiseCurrentList.ReadDouble("XQN"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YIP", fcuNoiseCurrentList.ReadDouble("YIP"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YIN", fcuNoiseCurrentList.ReadDouble("YIN"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YQP", fcuNoiseCurrentList.ReadDouble("YQP"));
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YQN", fcuNoiseCurrentList.ReadDouble("YQN"));
            this.progInfo.MainSpec.SetTraceData(fcuNoiseTraceDataList);
        }

        /// <summary>
        /// Switch rf to NVA
        /// </summary>
        /// <param name="rfPort"></param>
        private void SwitchRfPortToVNA(RfOutputEnum rfPort)
        {
            switch (rfPort)
            {
                case RfOutputEnum.XIP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XIpos);
                    break;
                case RfOutputEnum.XIN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XIneg);
                    break;
                case RfOutputEnum.XQP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XQpos);
                    break;
                case RfOutputEnum.XQN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XQneg);
                    break;
                case RfOutputEnum.YIP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YIpos);
                    break;
                case RfOutputEnum.YIN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YIneg);
                    break;
                case RfOutputEnum.YQP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YQpos);
                    break;
                case RfOutputEnum.YQN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YQneg);
                    break;
                default:
                    break;
            }

            Thread.Sleep(1000);
        }

        #endregion

        enum RfOutputEnum
        {
            XIP,
            XIN,
            XQP,
            XQN,
            YIP,
            YIN,
            YQP,
            YQN
        }

        enum TiaEnum
        {
            XI,
            XQ,
            YI,
            YQ
        }

        enum HybirdChipEnum
        {
            X,
            Y
        }

        enum opticalPathEnum
        { 
            SIG,
            LOC
        }


    }
}

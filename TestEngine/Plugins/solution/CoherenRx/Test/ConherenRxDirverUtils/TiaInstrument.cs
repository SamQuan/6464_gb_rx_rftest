using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// all tia control source
    /// </summary>
    public class TiaInstrument
    {
        /// <summary>
        /// Electrical Source for tia output control
        /// </summary>
        public Inst_SMUTI_TriggeredSMU OutputCtrl;
        /// <summary>
        /// Electrical Source for tia peak voltage detector
        /// </summary>
        public Inst_SMUTI_TriggeredSMU PeakVoltDetector;
        /// <summary>
        /// Electrical Source for tia gain control
        /// </summary>
        public Inst_Ke24xx Vgc;
        /// <summary>
        /// Electrical Source for tia output amplitude control in AGC mode
        /// </summary>
        public Inst_SMUTI_TriggeredSMU OutputAmplitudeCtrlInAGC;
        /// <summary>
        /// Electrical Source for tia mode control when gain control 
        /// </summary>
        public Inst_SMUTI_TriggeredSMU ModeCtrlOfGC;
        /// <summary>
        /// Electrical Source for tia vcc
        /// </summary>
        public Inst_Ke24xx VccSupply;
        /// <summary>
        /// Electrical Source for tia bandwidth high control 
        /// </summary>
        public Inst_Ke24xx BandWidth_H;
        /// <summary>
        /// Electrical Source for tia bandwidth low control 
        /// </summary>
        public Inst_Ke24xx BandWidth_L;
    }
}

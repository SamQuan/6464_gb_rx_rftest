using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// class for aux phase align instruments
    /// </summary>
    public class AuxPhaseAlignInstrument
    {
        /// <summary>
        /// instrument for SigX aux phase align instrument
        /// </summary>
        public Inst_SMUTI_TriggeredSMU SIG_X;
        /// <summary>
        /// instrument for SigY aux phase align instrument
        /// </summary>
        public Inst_SMUTI_TriggeredSMU SIG_Y;
        /// <summary>
        /// instrument for LocalX aux phase align instrument
        /// </summary>
        public Inst_SMUTI_TriggeredSMU LOC_X;
        /// <summary>
        /// instrument for LocalY aux phase align instrument
        /// </summary>
        public Inst_SMUTI_TriggeredSMU LOC_Y;
    }
}

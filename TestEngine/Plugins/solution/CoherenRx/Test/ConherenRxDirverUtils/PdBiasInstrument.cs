using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// class for pd bias instruments
    /// </summary>
    public class PdBiasInstrument
    {
        /// <summary>
        /// instrument variable for pd XIpos bias 
        /// </summary>
        public Inst_Ke24xx PdSource_XIpos;
        /// <summary>
        /// instrument variable for pd XIneg bias 
        /// </summary>
        public Inst_Ke24xx PdSource_XIneg;
        /// <summary>
        /// instrument variable for pd XQpos bias 
        /// </summary>
        public Inst_Ke24xx PdSource_XQpos;
        /// <summary>
        /// instrument variable for pd XQneg bias 
        /// </summary>
        public Inst_Ke24xx PdSource_XQneg;
        /// <summary>
        /// instrument variable for pd YIpos bias 
        /// </summary>
        public Inst_Ke24xx PdSource_YIpos;
        /// <summary>
        /// instrument variable for pd YIneg bias 
        /// </summary>
        public Inst_Ke24xx PdSource_YIneg;
        /// <summary>
        /// instrument variable for pd YQpos bias 
        /// </summary>
        public Inst_Ke24xx PdSource_YQpos;
        /// <summary>
        /// instrument variable for pd YQneg bias 
        /// </summary>
        public Inst_Ke24xx PdSource_YQneg;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// all tia control source
    /// </summary>
    public class SPICont
    {
        /// <summary>
        /// SPI Write command
        /// </summary>
        public SPICont WriteDIO_SPI;
  
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TcmzCommonUtils
//
// TestSelection.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// Test Selection class 
    /// </summary>
    public class TestSelection
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configFile">Configuration file</param>
        public TestSelection(string configFile)
        {
            loadTsData(configFile);
        }
        
        /// <summary>
        /// Test selected status
        /// </summary>
        /// <param name="ItuFreq_GHz">Channel frequency</param>
        /// <param name="TestName">Test to be checked</param>
        /// <returns>True if test is selected</returns>
        public bool IsTestSelected(double ItuFreq_GHz, string TestName)
        {
            // Search all rows of data
            foreach (DataRow row in tsDataTable.Rows)
            {
                // Search for matching ITU frequency
                if (Convert.ToDouble(row[tsDataTable.Columns["ITU Frequency"]].ToString()) == ItuFreq_GHz)
                {
                    // Return status
                    return Convert.ToBoolean(row[tsDataTable.Columns[TestName]].ToString());
                }
            }
            return false;
        }

        /// <summary>
        /// Test selected status
        /// </summary>
        /// <param name="PartCode">Product PartCode</param>
        /// <returns>Sweep1 if Partcode is not exist</returns>
        public string SweepModeSelected(string PartCode)
        {
            // Search all rows of data
            foreach (DataRow row in tsDataTable.Rows)
            {
                // Search for matching ITU frequency
                if (row[tsDataTable.Columns["PartCode"]].ToString() == PartCode)
                {
                    // Return status
                    return row[tsDataTable.Columns["SweepMode"]].ToString();
                }
            }
            return "Sweep1";
        }
        /// <summary>
        /// Expose table data
        /// </summary>
        public DataTable TestSelectTable
        {
            get { return tsDataTable; }
        }

        // Load data table from config file
        private void loadTsData(string configFile)
        {
            // Load the XML file into a DataSet object
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(configFile);
            // Confirm we have the correct switch configuration table
            if (!dataSet.Tables.Contains("TestSelect"))
            {
                throw new Exception("Invalid configuration file - missing 'TestSelect' table");
            }
            
            // Initialise local table
            tsDataTable = dataSet.Tables["TestSelect"];
        }

        // Local table
        private DataTable tsDataTable;

    }
}

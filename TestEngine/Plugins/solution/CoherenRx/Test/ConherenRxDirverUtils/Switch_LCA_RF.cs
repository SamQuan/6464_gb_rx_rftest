using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// LCA switch wrapper utility for coherentRx
    /// </summary>
    public class Switch_LCA_RF
    {
        /// <summary>
        /// Switch position
        /// </summary>
        public enum State
        {
            /// <summary>XIpos</summary>
            XIpos,
            /// <summary>XIneg</summary>
            XIneg,
            /// <summary>XQpos</summary>
            XQpos,
            /// <summary>XQneg</summary>
            XQneg,
            /// <summary>YIpos</summary>
            YIpos,
            /// <summary>YIneg</summary>
            YIneg,
            /// <summary>YQpos</summary>
            YQpos,
            /// <summary>YQneg</summary>
            YQneg,
            /// <summary>OFF Connent RF</summary>
            OffConnent,
            /// <summary>Disconnect voltages from switches</summary>
            VoltagesOff
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="swMan"></param>
        public Switch_LCA_RF(Util_SwitchPathManager swMan)
        {
            this.swMan = swMan;
        }

        /// <summary>
        /// Underlying Switch Path Manager utility
        /// </summary>
        private Util_SwitchPathManager swMan;

        /// <summary>
        /// Switch state
        /// </summary>
        /// <param name="state"></param>
        public void SetState(State state)
        {
            switch (state)
            {
                case State.XIpos:
                    swMan.SetToPosn("LCA_TO_XIpos");
                    break;
                case State.XIneg:
                    swMan.SetToPosn("LCA_TO_XIneg");
                    break;
                case State.XQpos:
                    swMan.SetToPosn("LCA_TO_XQpos");
                    break;
                case State.XQneg:
                    swMan.SetToPosn("LCA_TO_XQneg");
                    break;
                case State.YIpos:
                    swMan.SetToPosn("LCA_TO_YIpos");
                    break;
                case State.YIneg:
                    swMan.SetToPosn("LCA_TO_YIneg");
                    break;
                case State.YQpos:
                    swMan.SetToPosn("LCA_TO_YQpos");
                    break;
                case State.YQneg:
                    swMan.SetToPosn("LCA_TO_YQneg");
                    break;
                case State.OffConnent:
                    swMan.SetToPosn("OffConnent");
                    break;
                case State.VoltagesOff:
                    swMan.SetToPosn("VoltagesOff");
                    break;
                default:
                    throw new ArgumentException("Invalid state: " + state.ToString());
            }
            
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestSolution.Instruments;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// class for S parameter measurement
    /// </summary>
    public static class SparamMeasurement
    {
          
       /// <summary>
        /// Get s21 sweep data,include real imaginary and sweep freq
       /// </summary>
       /// <param name="engine">reference of engine</param>
        /// <param name="componentAnalyzer">LightwaveComponentAnalyzer</param>
        /// <param name="s21CalRegister">s21 calibration register</param>
        /// <returns>s21 sweep result trace</returns>
        static public Trace Get_S21(ITestEngine engine, InstType_LightwaveComponentAnalyzer componentAnalyzer, int s21CalRegister)
        {
            Trace s21Trace = new Trace();
            int delayTime = 40;

            if (componentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                componentAnalyzer.AutoScale();
                Thread.Sleep(delayTime * 10);
                componentAnalyzer.WaitForCleanSweep();
            }
           
            
            
            Thread.Sleep(delayTime*10);

            try
            {
                if (componentAnalyzer.HardwareIdentity.Contains("MS46"))
                {
                    componentAnalyzer.SparameterMeasurementMode = SparameterMeasurementMode.S21;
                    Thread.Sleep(300);
               //     componentAnalyzer.AutoScale();
                    Thread.Sleep(300);
                 //   componentAnalyzer.WaitForCleanSweep();
                    //yubo add 1 second delay to get data   after calibrate to 40G 
                    Thread.Sleep(1000);
                    //componentAnalyzer.SweepMode = componentAnalyzer.SweepModeEnum.HOLD; 
                    
                    //componentAnalyzer.MeasurementRestart();
                   
                    //Thread.Sleep(2000);
                }
                //to deal with some time get no data from VNA , if fail at second time ,then throw error
                try
                {
                    s21Trace = componentAnalyzer.GetTraceData();
                }
                catch (Exception e)

                {
                    Thread.Sleep(4000);
                    engine.SendStatusMsg(" record: fail to get data from VNA ");
                    s21Trace = componentAnalyzer.GetTraceData();
                }

            }
            catch (Exception ex)
            {
                if (componentAnalyzer.HardwareIdentity.Contains("8703"))
                {
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("  manual press botton LOCAL and PRESET to fix  and contine ", ButtonId.Continue),
                                            new ButtonInfo("abort",ButtonId.No),
                                       };

                    ButtonId respond = ButtonId.Continue;
                    respond = (ButtonId)engine.ShowUserQuery("8703 error ", buttonsInfo);

                    if (respond == ButtonId.Continue)
                    {
                        componentAnalyzer.RecallRegister(s21CalRegister);
                        Thread.Sleep(delayTime * 100);
                        componentAnalyzer.AutoScale();
                        Thread.Sleep(delayTime * 10);

                        componentAnalyzer.WaitForCleanSweep();

                        Thread.Sleep(delayTime * 10);

                        s21Trace = componentAnalyzer.GetTraceData();
                    }
                    else
                    {
                        engine.ErrorInModule(ex.Message);
                    }
                }
                else
                {
                    engine.SendStatusMsg(ex.Message.ToString() );
                    engine.ErrorInModule(ex.Message);
                }
            }

            return s21Trace;
        }
       

        static public Trace Get_S21_Fast(ITestEngine engine, InstType_LightwaveComponentAnalyzer componentAnalyzer, int s21CalRegister)
        {
            Trace s21Trace = new Trace();
            int delayTime = 40;

            if (componentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                componentAnalyzer.AutoScale();
                Thread.Sleep(delayTime * 10);
                componentAnalyzer.WaitForCleanSweep();
                Thread.Sleep(delayTime * 10);
            }
           
            
            
            

            try
            {
                if (componentAnalyzer.HardwareIdentity.Contains("MS46"))
                {
                    componentAnalyzer.SparameterMeasurementMode = SparameterMeasurementMode.S21;
                    //Thread.Sleep(300);
                    componentAnalyzer.AutoScale();
                    //Thread.Sleep(300);
                    componentAnalyzer.WaitForCleanSweepFast();
                    //yubo add 1 second delay to get data   after calibrate to 40G 
                    //Thread.Sleep(2000);
                    //componentAnalyzer.MeasurementRestart();
                   
                    //Thread.Sleep(2000);
                }
                //to deal with some time get no data from VNA , if fail at second time ,then throw error
                try
                {
                    s21Trace = componentAnalyzer.GetTraceDataFAST(PointType.Three_Dimensional);
                }
                catch (Exception e)

                {
                    Thread.Sleep(4000);
                    engine.SendStatusMsg(" record: fail to get data from VNA ");
                    s21Trace = componentAnalyzer.GetTraceData();
                }

            }
            catch (Exception ex)
            {
                if (componentAnalyzer.HardwareIdentity.Contains("8703"))
                {
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("  manual press botton LOCAL and PRESET to fix  and contine ", ButtonId.Continue),
                                            new ButtonInfo("abort",ButtonId.No),
                                       };

                    ButtonId respond = ButtonId.Continue;
                    respond = (ButtonId)engine.ShowUserQuery("8703 error ", buttonsInfo);

                    if (respond == ButtonId.Continue)
                    {
                        componentAnalyzer.RecallRegister(s21CalRegister);
                        Thread.Sleep(delayTime * 100);
                        componentAnalyzer.AutoScale();
                        Thread.Sleep(delayTime * 10);

                        componentAnalyzer.WaitForCleanSweep();

                        Thread.Sleep(delayTime * 10);

                        s21Trace = componentAnalyzer.GetTraceData();
                    }
                    else
                    {
                        engine.ErrorInModule(ex.Message);
                    }
                }
                else
                {
                    engine.SendStatusMsg(ex.Message.ToString() );
                    engine.ErrorInModule(ex.Message);
                }
            }

            return s21Trace;
        }
        /// <summary>
        /// get s22 sweep data include freq_GHz and mag
        /// </summary>
        /// <param name="engine">reference of engine</param>
        /// <param name="componentAnalyzer">reference LightwaveComponentAnalyzer</param>
        /// <param name="s22CalRegister">s22 calibration register</param>
        /// <returns>s22 sweep result trace</returns>
        static public Trace Get_S22(ITestEngine engine, InstType_LightwaveComponentAnalyzer componentAnalyzer, int s22CalRegister)
        {
            Trace s22Trace = new Trace();
            Trace s22RealAndImaginaryTrace = new Trace();
            int delayTime = 10;
            if (componentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                componentAnalyzer.RecallRegister(s22CalRegister);
                Thread.Sleep(2000);
            }
            
            try
            {
                s22Trace.Clear();
                s22RealAndImaginaryTrace.Clear();
                //Thread.Sleep(2000);
                componentAnalyzer.SparameterMeasurementMode = SparameterMeasurementMode.S22;
                Thread.Sleep(300);
                componentAnalyzer.WaitForCleanSweep();
                //yubo add 1 second delay to get data   after calibrate to 40G 
                Thread.Sleep(1000);
              
                if (componentAnalyzer.HardwareIdentity.Contains("MS46"))
                {
                    s22RealAndImaginaryTrace = componentAnalyzer.GetTraceData();
                    double[] freqArray = s22RealAndImaginaryTrace.GetXArray();
                    double[] realArray = s22RealAndImaginaryTrace.GetYArray();
                    double[] imaginaryArray = s22RealAndImaginaryTrace.GetZArray();

                    for (int i = 0; i < realArray.Length; i++)
                    {
                        double temp = Math.Sqrt(Math.Pow(realArray[i], 2) + Math.Pow(imaginaryArray[i], 2));
                        double magintude_dB = 20 * Math.Log10(temp);
                        s22Trace.Add(freqArray[i], magintude_dB);
                    }
                }
                else
                {
                    s22Trace = componentAnalyzer.GetTraceData();
                }
            }
            catch (Exception ex)
            {
                if (componentAnalyzer.HardwareIdentity.Contains("8703"))
                {
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("  manual press botton LOCAL and PRESET to fix  and contine ", ButtonId.Continue),
                                            new ButtonInfo("abort",ButtonId.No),
                                       };

                    ButtonId respond = ButtonId.Continue;

                    respond = (ButtonId)engine.ShowUserQuery("8703 error ", buttonsInfo);

                    if (respond == ButtonId.Continue)
                    {
                        componentAnalyzer.Preset();
                        System.Threading.Thread.Sleep(delayTime * 400);

                        componentAnalyzer.RecallRegister(s22CalRegister);
                        Thread.Sleep(2000);

                        componentAnalyzer.WaitForCleanSweep();
                        Thread.Sleep(2000);
                        s22Trace = componentAnalyzer.GetTraceData();
                    }
                    else
                    {
                        engine.ErrorInModule(ex.Message);
                    }
                }
                else
                {
                    engine.ErrorInModule(ex.Message);
                }
            }
            return s22Trace;
        }
    }

    
    //public struct AnalyzerSetup
    //{  
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double activeChannelAveragingFactor;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double startFrequency_GHz;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double stopFrequency_GHz;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double smoothingAperturePercent;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double IFBandWidth;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double referencePosition;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double s21Scale;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public double s22Scale;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public uint sweepPoints;
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public int CalRegister;
    //}
}

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestEngine.PluginInterfaces.Module;
using System.Threading;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.InternalData;


namespace Bookham.ToolKit.ConherenRx
{
    internal class polaritycontrol
    {

        internal PolarizeController PolController_Sig;

        internal PolarizeController PolController_Loc;

        internal Inst_Sub20ToAsic pol_sig;

        internal Inst_Sub20ToAsic pol_loc;

    }
    /// <summary>
    /// A common class that set power,set polarization,measure splitter ratio
    /// </summary>
    public class MeasureSetupManage
    {
        #region public variable

        /// <summary>
        /// optical attenuator object
        /// </summary>
        public InstType_OpticalAttenuator VOA;
        /// <summary>
        /// optical power meter that measure real optical power input  device
        /// </summary>
        public InstType_OpticalAttenuator VOA2;
        /// <summary>
        /// Optical switch controlled by kethley 24xx
        /// </summary>
        public Inst_Ke24xx OptSwitch;
        /// <summary>
        /// optical power meter that measure real optical power input  device
        /// </summary>
        public InstType_OpticalPowerMeter OPM_Ref;
        /// <summary>
        /// optical power meter that measure monitor optical power that splite from spliter
        /// </summary>
        public InstType_OpticalPowerMeter OPM_Mon;
        /// <summary>
        /// delay line object
        /// </summary>
        public InstType_ODL DelayLine;
        /// <summary>
        /// Electrical Source class object for tia control
        /// </summary>
        public PdBiasInstrument PdBias;
        /// <summary>
        /// ag8703 object
        /// </summary>
        public InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;
        /// <summary>
        /// Electrical Source for polarisation control
        /// </summary>
        public PolarizeController PolCtrl;
        /// <summary>
        /// itla laser source
        /// </summary>
        public Inst_iTLATunableLaserSource LaserSource;
        /// <summary>
        /// locker in amplifier object
        /// </summary>
        public Inst_SR830 LockInAmplifier;
        /// <summary>
        /// splitter ratio data for splitter in the specified optical path 
        /// </summary>
        public RxSplitterRatioCalData SplitterRatiosCalData;
        /// <summary>
        /// bool variable represent splitter ratio calculate ok
        /// </summary>
        /// 
        public Inst_Sub20ToAsic sub20_Pol;



        public bool IsSplitterRatioCalOK;



        #endregion


        /// <summary>
        /// set optical input power
        /// </summary>
        /// <param name="engine">reference of engine</param>
        /// <param name="targetPower_dBm">target power </param>
        /// <param name="splitterRatio">splitter ratio</param>
        /// <param name="tolerance_dB">tolerance of power</param>
        public void SetOpticalInputPower(ITestEngine engine, double targetPower_dBm,
            double splitterRatio, double tolerance_dB)
        {
            ButtonId respond = ButtonId.Continue;
            bool setPwrOk = false;
            do
            {

                setPwrOk = SetOpticalPower(engine, targetPower_dBm, splitterRatio, tolerance_dB);
               
                //if can not set to target power
                if (!setPwrOk)
                {
                    
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("Power is too low, retry!(功率过低,Retry!）", ButtonId.Cancel),
                                            new ButtonInfo("Skip and continues test(跳过继续测试)",ButtonId.Continue),
                                       };
                    respond = (ButtonId)engine.ShowUserQuery("Power is too low!", buttonsInfo);
                }
                else
                {
                    respond = ButtonId.Continue;
                }
            } while (respond != ButtonId.Continue);

            if (!setPwrOk)
            {
                engine.ErrorInModule("Power is too low! please check and retry or end test!");
            }
        }

        /// <summary>
        /// set the optical path input power to target power,
        /// if targetPower_dBm==double.NegativeInfinity,output is null power
        /// </summary>
        /// <param name="engine">reference of ITestEngine</param>
        /// <param name="targetPower_dBm">targetPower_dBm</param>
        /// <param name="splitterRatio">splitterRatio</param>
        /// <param name="tolerance_dB">power tolerance_dB</param>
        /// <returns>return true as set power ok,return false as set power fail</returns>
        private bool SetOpticalPower(ITestEngine engine, double targetPower_dBm,
            double splitterRatio, double tolerance_dB)
        {
            //variable record current attenuation
            double currAttenua;
            //variable record current real output power in dBm
            double realInputPower_dBm;
            //variable record difference between real output power and target power
            double diffPower_dB;
            //variable record power has been set correctly or not
            bool setOpticalOk = false;

            int maxLoopCount = 10;

            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            //OPM_Ref.ReferencePower = 0;
            //OPM_Ref.Wavelength_nm = 1550.1;

            //try-catch for VOA communication error
            try
            {   

                if (double.IsNegativeInfinity(targetPower_dBm))
                {

                    
                    if (VOA2 != null && VOA2.IsOnline)
                    {
                        VOA2.Attenuation_dB = 50;
                        VOA.OutputEnabled = false;
                    }
                    else if (OptSwitch!= null && OptSwitch.IsOnline){

                        OptSwitch.DigitalOutputState_SlotID = false;
                        Thread.Sleep(10);
                    }

                    setOpticalOk = true;
                }
                else
                {
                    //read back current attenuation
                    currAttenua = VOA.Attenuation_dB;
                    if (VOA2 != null && VOA2.IsOnline)
                    {
                        VOA2.Attenuation_dB = 1.5;
                    }
                    else if (OptSwitch != null && OptSwitch.IsOnline) {

                        OptSwitch.DigitalOutputState_SlotID = true;
                        Thread.Sleep(10);
                    }

                    do
                    {
                        //read back current output power
                        realInputPower_dBm = OPM_Ref.ReadPower()  + splitterRatio;

                        //calculate the difference between real output power and target power
                        diffPower_dB = targetPower_dBm - realInputPower_dBm;

                        if (Math.Abs(diffPower_dB) < tolerance_dB)
                        {
                            setOpticalOk = true;
                            break;
                        }

                        currAttenua = currAttenua - diffPower_dB;

                        if (currAttenua < 0)
                        {
                            throw new Exception("current attenua is negative attenuation,please check the hardware!");
                        }
                        VOA.Attenuation_dB = currAttenua;

                        //wait for output power stabile
                        Thread.Sleep(200);
                        maxLoopCount--;
                    } while (maxLoopCount > 0);
                }
            }
            catch (Exception ex)
            {
                engine.ErrorInModule(ex.Message);
            }

            return setOpticalOk;
        }

        /// <summary>
        ///  Set optical polarization
        /// Method: Adjust Polarization Controller to let the aux pd current maximised
        /// </summary>
        /// <param name="engine">reference of engine</param>
        /// <param name="path">optical path sig or loc</param>
        /// <param name="hybirdChip">hybird chipX or chipY</param>
        /// <param name="sopcSweepConfig">pol control sweep configuration</param>
        /// <param name="sopcAdjustTraceFile">file save sopc adjust trace data</param>
        /// <returns></returns>
        public double[] SetOpticalPolarization(ITestEngine engine, OpticalPathEnum path, HybirdChipEnum hybirdChip,
            SOPCSweepConfig sopcSweepConfig,  string sopcAdjustTraceFile, MeasureSetupManage measSetupLoc)
        {
            DatumList infoListToGui = new DatumList();
            double peakPointCtrlVoltage_V = 0.0;
            List<double> maxPeakOfPdList = new List<double>();
            List<double[]> listSopcCtrlVolt = new List<double[]>();
            List<double> listSopcFinalCtrlVolt = new List<double>();
            List<double> listSopcFinalPeakCurrent = new List<double>();

            Util_SavePlotData saveSopcAdjustTrace = new Util_SavePlotData();
            int maxLoopCounts = 2;
            double currentFinalSopcSet_mA = 0.0;
            double maxPeakCurrent_mA = 0.0;
            //int indexOfMaxPeak = 0;
            double[] arr_IfinalSopc_mA;  //arr_IfinalSopc_mA
            double sweepCounts = (sopcSweepConfig.sweepStop_V - sopcSweepConfig.sweepStart_V) / sopcSweepConfig.sweepStep_V + 1;


 //           Inst_Sub20ToAsic sub20sig = (Inst_Sub20ToAsic)measSetupSig.sub20_sig;
            Inst_Sub20ToAsic sub20Pol = (Inst_Sub20ToAsic)measSetupLoc.sub20_Pol;
 //           sub20loc.SetDefaultState();
 //           sub20loc.pol_write(1009, 0);
          //  polaritycontrol progInstrs = new polaritycontrol();
          //  progInstrs.pol_sig = (Inst_Sub20ToAsic)instr["Pol_sig"];

          //  Inst_Sub20ToAsic pol_controller = PolCtrl.sub20;
            
 
            //set each channal of sopc control to 0.1V,this just because the sopc can't not set to zero voltage
            foreach (Inst_SMUTI_TriggeredSMU sopcSource in PolCtrl.Allchans)
            {
                //this is the FCU MARK2 setting sequence,OutputEnabled first
             //   sopcSource.OutputEnabled = true;
             //   sopcSource.CurrentComplianceSetPoint_Amp = sopcSweepConfig.IcomplianceSOPC_A;
             //   sopcSource.VoltageSetPoint_Volt = sopcSweepConfig.sweepStart_V;
                sub20_Pol.SetDefaultState();
              }

            do
            {
                //if optimising one time can not reach best polarization status,then redo again
                maxPeakOfPdList.Clear();
                listSopcCtrlVolt.Clear();
                listSopcFinalCtrlVolt.Clear();
                listSopcFinalPeakCurrent.Clear();
                currentFinalSopcSet_mA = 0.0;
                maxPeakCurrent_mA = 0.0;
                //indexOfMaxPeak = 0;

                //optimising the polarisation voltage for each channel in turn(sweep ctrl volt,meas pd current sum,find peak point)
                for (uint iCount = 0; iCount < PolCtrl.Allchans.Count; iCount++)
                //for (int iCount = 0; iCount < 2; iCount++)
                {
                    int chanNum = (int)iCount + 1;

                    engine.SendStatusMsg("Adjust channel " + chanNum.ToString());
                     Inst_SMUTI_TriggeredSMU sopcSource = PolCtrl.Allchans[chanNum - 1];
                     
                    
                    //init sopc channels default sweep start volt setting
                    double[] sopcCtrlVoltArray_V = new double[4];
                    for (int i = 0; i < sopcCtrlVoltArray_V.Length; i++)
                    {
                        if (chanNum == 1)
                        {   //because can't set the control voltage to zero volt,so would set to sweepStart_V
                            sopcCtrlVoltArray_V[i] = sopcSweepConfig.sweepStart_V;
                        }
                        else
                        {
                            sopcCtrlVoltArray_V[i] = listSopcCtrlVolt[chanNum - 2][i];
                        }
                    }

                    //variable save polarization sweep data
                    List<double> listPdCurrentSum = new List<double>();
                    List<double[]> listEachPdCurrent = new List<double[]>();
                    List<double> listSweepVoltage = new List<double>();
                    double[] pdCurrentArray_mA = new double[PolCtrl.Allchans.Count];

                    //do sopc control voltage sweep,read pd current
                    for (uint i = 0; i < sweepCounts; i++)
                    {
                      //  double voltSet_V = sopcSweepConfig.sweepStart_V + sopcSweepConfig.sweepStep_V * i;
                        uint voltSet_V = 41*i;

                        //set polarization control voltage
                        //sopcSource.VoltageSetPoint_Volt = voltSet_V;
                        sub20_Pol.pol_write(voltSet_V, iCount);


                        Thread.Sleep((int)sopcSweepConfig.sweepDelayTime_ms);

                        //measure pd current
                        double pdCurrentSum_mA = MeasurePdCurrent(hybirdChip, PdBias,  out pdCurrentArray_mA);

                        //save pd current to list
                        listSweepVoltage.Add(voltSet_V);
                        listEachPdCurrent.Add(pdCurrentArray_mA);
                        listPdCurrentSum.Add(pdCurrentSum_mA);
                    }

                    //find max peak of pd current,save sopc ctrl voltage and max pd current
                    //will not get the maximum current if directly setting to peak point ctrl volt,so not interpolating the peak
                    int index = Alg_FindFeature.FindIndexForMaxPeak(listPdCurrentSum.ToArray());
                    peakPointCtrlVoltage_V = sopcSweepConfig.sweepStart_V + sopcSweepConfig.sweepStep_V * index;
                    peakPointCtrlVoltage_V = listSweepVoltage[index];
                    sopcCtrlVoltArray_V[chanNum - 1] = peakPointCtrlVoltage_V;

                    listSopcCtrlVolt.Add(sopcCtrlVoltArray_V);
                    maxPeakOfPdList.Add(listPdCurrentSum[index]);

                    //set sopc control voltage to pd current peak point,
                    //will not get the maximum current if directly setting to peak point ctrl volt
                    sub20_Pol.pol_write((uint)peakPointCtrlVoltage_V, iCount);
 
  //                 for (int i = 0; i < index; i++)
  //                  {
  //                      sopcSource.VoltageSetPoint_Volt = sopcSweepConfig.sweepStart_V + i * sopcSweepConfig.sweepStep_V;
  //                      Thread.Sleep(1);
  //                  }

                    //measure pd current
                    double currentSum_mA = MeasurePdCurrent(hybirdChip, PdBias,  out pdCurrentArray_mA);

                    listSopcFinalCtrlVolt.Add(peakPointCtrlVoltage_V);
                    listSopcFinalPeakCurrent.Add(currentSum_mA);

                    //save sopc voltage sweep data
                    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "Chan" + chanNum.ToString() + "_SweepVolt_V", listSweepVoltage.ToArray());

                    //save each pd sweep current to .csv
                    for (int i = 0; i < pdCurrentArray_mA.Length; i++)
                    {
                        List<double> tempList_arrPdCurrent = new List<double>();
                        foreach (double[] arrPdCurrent in listEachPdCurrent)
                        {
                            tempList_arrPdCurrent.Add(arrPdCurrent[i]);
                        }
                        double pdNumber = i + 1;
                        if (hybirdChip == HybirdChipEnum.ChipY)
                        {
                            //Ychip pd number is pd5/pd6/pd7/pd8,so here pdNumber = i + 4
                            pdNumber = i + 4;
                        }
                        saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile,
                            "Chan" + chanNum.ToString() + "_PD" + pdNumber.ToString() + "_mA", tempList_arrPdCurrent.ToArray());
                    }

                    //save pd current sum and peak point control voltage to .csv
                    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, sopcSource.Name + "Sum_mA", listPdCurrentSum.ToArray());
                    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "Chan" + chanNum.ToString() + "_PeakCtrlVolt_V", sopcCtrlVoltArray_V);

                    //updata pdSum curve to GUI
                    infoListToGui = new DatumList();
                    infoListToGui.AddDoubleArray("sweepVoltage", listSweepVoltage.ToArray());
                    infoListToGui.AddDoubleArray("pdCurrentSum", listPdCurrentSum.ToArray());
                    engine.SendToGui("channel " + chanNum.ToString() + " sweep");
                    engine.SendToGui(infoListToGui);
                }

                maxPeakCurrent_mA = maxPeakOfPdList[0];
                for (int i = 0; i < maxPeakOfPdList.Count; i++)
                {
                    if (maxPeakCurrent_mA < maxPeakOfPdList[i])
                    {
                        maxPeakCurrent_mA = maxPeakOfPdList[i];
                    }
                }

                Thread.Sleep(1000);
                currentFinalSopcSet_mA = MeasurePdCurrent(hybirdChip, PdBias,  out arr_IfinalSopc_mA);

                maxLoopCounts--;
                engine.SendStatusMsg("do " + maxLoopCounts.ToString() + " times SOPC sweep!");

            } while (Math.Abs(currentFinalSopcSet_mA - maxPeakCurrent_mA) > maxPeakCurrent_mA * 0.07 && maxLoopCounts >= 1);

            //save sopc final setting voltage
            saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "real control volt", listSopcFinalCtrlVolt.ToArray());
            saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "pdCurrent_mA", listSopcFinalPeakCurrent.ToArray());

            return listSopcFinalCtrlVolt.ToArray();
        }

        /// <summary>
        ///  Measure pd current according hybird chip type,xchip just measure X PD current,Y chip just measure Y chip
        /// </summary>
        /// <param name="hybirdChip">Enum of hybird chip type</param>
        /// <param name="PdBias">reference of pd</param>
        /// <param name="pdcurrentArray_mA">out each pd current in mA,the sequence is IP/IN/QP/QN</param>
        /// <returns>return 4 pd current sum</returns>
        private double MeasurePdCurrent(HybirdChipEnum hybirdChip, PdBiasInstrument PdBias,
            out double[] pdcurrentArray_mA)
        {
            double TempCurrent = 0;

            List<double> pdCurrentList_mA = new List<double>();

            switch (hybirdChip)
            {
                //x chip need to measure pd current of XIp XIn XQp XQn 
                case HybirdChipEnum.ChipX:

                    // Measure other pds first to allow auto-scale to work
                    TempCurrent = PdBias.PdSource_XQpos.CurrentActual_amp;
                    TempCurrent = PdBias.PdSource_YIpos.CurrentActual_amp;
                    TempCurrent = PdBias.PdSource_YQpos.CurrentActual_amp;

                    pdCurrentList_mA.Add(PdBias.PdSource_XIpos.CurrentActual_amp * 1000);
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_XIneg.CurrentActual_amp * 1000 );
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_XQpos.CurrentActual_amp * 1000 );
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_XQneg.CurrentActual_amp * 1000 );

                    break;

                //y chip need to measure pd current of YIp YIn YQp YQn 
                case HybirdChipEnum.ChipY:

                    // Measure other pds first to allow auto-scale to work
                    TempCurrent = PdBias.PdSource_XIpos.CurrentActual_amp;
                    TempCurrent = PdBias.PdSource_XQpos.CurrentActual_amp;
                    TempCurrent = PdBias.PdSource_YQpos.CurrentActual_amp;

                    pdCurrentList_mA.Add(PdBias.PdSource_YIpos.CurrentActual_amp * 1000);
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_YIneg.CurrentActual_amp * 1000 );
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_YQpos.CurrentActual_amp * 1000 );
                    //                    pdCurrentList_mA.Add(PdBias.PdSource_YQneg.CurrentActual_amp * 1000 );

                    break;
            }

            pdcurrentArray_mA = pdCurrentList_mA.ToArray();

            //calculate the sum of pd current
            double pdCurrentSum_mA = 0.0;
            for (int i = 0; i < pdCurrentList_mA.Count; i++)
            {
                pdCurrentSum_mA += pdCurrentList_mA[i];
            }

            return pdCurrentSum_mA;
        }

        /// <summary>
        /// set tia band width ctrol status
        /// </summary>
        /// <param name="Tia">tia object</param>
        /// <param name="tiaBandwidth">tia band width setting status</param>
        /// <param name="currentCompliance_mA">Compliance current of bandwith control</param>
        /// <param name="bwCtrlVolt_V">bandwidth control voltage</param>
        public void SetTiaBandWidth(TiaInstrument Tia, TiaBandwidth tiaBandwidth,
                                            double currentCompliance_mA, double bwCtrlVolt_V)
        {
            if (Tia.BandWidth_H != null && Tia.BandWidth_L != null)
            {
                Tia.BandWidth_H.OutputEnabled = true;
                Tia.BandWidth_L.OutputEnabled = true;

                Tia.BandWidth_H.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
                Tia.BandWidth_L.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;

                switch (tiaBandwidth)
                {
                    case TiaBandwidth._10point0_GHz: // Low & Low 
                        Tia.BandWidth_H.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_L.OutputEnabled =true;
                        break;
                    case TiaBandwidth._10point5_GHz: // low & off
                        Tia.BandWidth_H.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.OutputEnabled = false;
                        break;
                    case TiaBandwidth._10point9_GHz: // Low & high 
                        Tia.BandWidth_H.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_L.OutputEnabled = true;
                        break;
                    case TiaBandwidth._11point1_GHz: // off & low
                        Tia.BandWidth_H.OutputEnabled = false;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_L.OutputEnabled = true;
                        break;
                    case TiaBandwidth._11point5_GHz: // off & off
                        Tia.BandWidth_H.OutputEnabled = false;
                        Tia.BandWidth_L.OutputEnabled = false;
                        break;
                    case TiaBandwidth._12point8_GHz: // off & high
                        Tia.BandWidth_H.OutputEnabled =false;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_L.OutputEnabled = true;
                        break;
                    case TiaBandwidth._14point0_GHz: // high & low
                        Tia.BandWidth_H.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = 0;
                        Tia.BandWidth_L.OutputEnabled = true;
                        break;
                    case TiaBandwidth._14point8_GHz: // high & off
                        Tia.BandWidth_H.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.OutputEnabled = false;
                        break;
                    case TiaBandwidth._15point4_GHz: // high & high
                        Tia.BandWidth_H.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_H.OutputEnabled = true;
                        Tia.BandWidth_L.VoltageSetPoint_Volt = bwCtrlVolt_V;
                        Tia.BandWidth_L.OutputEnabled = true;
                        break;
                    default:     // off & off
                        Tia.BandWidth_H.OutputEnabled = false;
                        Tia.BandWidth_L.OutputEnabled = false;
                        break;
                }
            }
        }




        /// <summary>
        /// Measure pd current
        /// </summary>
        /// <returns>return pd current array,unit is mA</returns>
        public double[] MeasurePdCurrentSum()
        {
            List<double> pdCurrentList = new List<double>();

            pdCurrentList.Add(PdBias.PdSource_XIpos.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_XIneg.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_XQpos.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_XQneg.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_YIpos.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_YIneg.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_YQpos.CurrentActual_amp * 1000);
            pdCurrentList.Add(PdBias.PdSource_YQneg.CurrentActual_amp * 1000);

            return pdCurrentList.ToArray();
        }



        #region splitterRatio function
        /// <summary>
        /// Read splitter ratio value at all frequency from database,
        /// the database information contains in the .xml configuration file       
        /// </summary>
        /// <param name="splitterName">splitter name</param>
        /// <param name="configFilePath"> database configuration file's full path, it should be XML file</param>
        /// <param name="dataTableName"> data table name in XML file</param>        
        public void SetupSplitterRatioData(string splitterName,
            string configFilePath, string dataTableName)
        {
            SplitterRatiosCalData = new RxSplitterRatioCalData(splitterName, configFilePath, dataTableName);
            if (SplitterRatiosCalData == null)
                IsSplitterRatioCalOK = false;
            else
                IsSplitterRatioCalOK = true;
        }

       

        /// <summary>
        /// Measure optical path's splitter ratio with series wavelenth
        /// if each splitter ratio at each wavelen are within max and min limit, return true
        /// else return false
        /// </summary>
        /// <param name="wavelenArray"></param>
        /// <param name="voa_Att"></param>
        /// <param name="sr_Max"> Maximun Splliter ratio allowed </param>
        /// <param name="sr_Min"> Minimun Splliter ratio allowed </param>
        public bool MeasSplitterRatioByWavelen(double[] wavelenArray, double voa_Att, double sr_Max, double sr_Min)
        {
            InstType_OpticalPowerMeter.MeterMode mode_Mon = OPM_Mon.Mode;
            InstType_OpticalPowerMeter.MeterMode mode_Ref = OPM_Ref.Mode;
            OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;

            bool isDataOk = true;
            for (int i = 0; i < wavelenArray.Length; i++)
            {
                ChainWaveLen_nm = wavelenArray[i];
                Thread.Sleep(1000);
                SplitterRatioCalDataUnit newData = MeasSplitterRatio(voa_Att,wavelenArray[i]);
                SplitterRatiosCalData.AddCalData(newData);
                if (newData.Ratio > sr_Max || newData.Ratio < sr_Min) isDataOk = false;
            }

            OPM_Mon.Mode = mode_Mon;
            OPM_Ref.Mode = mode_Ref;
            return isDataOk;
        }

        /// <summary>
        /// Get/Set wavelen apply to the whole instruments in optical chain that might involves wavelenth setting
        /// </summary>
        public double ChainWaveLen_nm
        {
            set
            {
                if (LaserSource != null)
                {
                    LaserSource.WavelengthINnm = value;
                }
                setPathWaveLen(value);
            }
            get
            {
                if (LaserSource != null)
                {
                    return LaserSource.WavelengthINnm;
                }
                else
                {
                    //when there no separate laser,we will used laser source from 9703,it work at 1550.1
                    double WavelengthINnm = 1550.1;
                    return WavelengthINnm;
                }
            }
        }

        /// <summary>
        /// Measure splitter ratio at optical instruments chain's wavelenth in effect
        /// </summary>
        /// <param name="voa_Att">attenuation should be set to VOA,unit is dBm</param>
        /// <param name="wavelength_nm">wavelength in nm unit</param>
        /// <returns></returns>
        public SplitterRatioCalDataUnit MeasSplitterRatio(double voa_Att,double wavelength_nm)
        {
            double freq_GHz;
            double wavelen_nm;


            freq_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelength_nm);
            wavelen_nm = wavelength_nm;


            OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            VOA.Attenuation_dB = voa_Att;

            double pReading_Mon = OPM_Mon.ReadPower();
            double pReading_Ref = OPM_Ref.ReadPower();

            double ratio = Math.Abs(pReading_Mon - pReading_Ref);

            SplitterRatioCalDataUnit dataUnit = new SplitterRatioCalDataUnit();
            dataUnit.Ratio = Math.Round(ratio,2);
            dataUnit.Frequency_Ghz = freq_GHz;
            dataUnit.Wavelen_nM = wavelen_nm;

            return dataUnit;
        }

        /// <summary>
        /// Set wavelenth to VOA, OPM, etc. such instruments might involve frequency in measurement
        /// </summary>
        void setPathWaveLen(double wavelen_nM)
        {
            //VOA.Wavelength_nm = wavelen_nM;
            OPM_Mon.Wavelength_nm = wavelen_nM;
            OPM_Ref.Wavelength_nm = wavelen_nM;
        }

        #endregion

        #region public Enum

        /// <summary>
        /// optical path Enum
        /// </summary>
        public enum OpticalPathEnum
        {
            /// <summary>
            /// signal path
            /// </summary>
            SIG,
            /// <summary>
            /// local path
            /// </summary>
            LOC
        }

        /// <summary>
        /// Hybird Chip Enum
        /// </summary>
        public enum HybirdChipEnum
        {
            /// <summary>
            /// hybird chip x
            /// </summary>
            ChipX,
            /// <summary>
            /// hybird chip y
            /// </summary>
            ChipY
        }

        /// <summary>
        /// Struct of SOPC Sweep Config parameters
        /// </summary>
        public struct SOPCSweepConfig
        {
            /// <summary>
            /// sopc sweep start voltage
            /// </summary>
            public double sweepStart_V;
            /// <summary>
            /// sopc sweep stop voltage
            /// </summary>
            public double sweepStop_V;
            /// <summary>
            /// sopc sweep step voltage
            /// </summary>
            public double sweepStep_V;
            /// <summary>
            /// compliance current for sopc control source
            /// </summary>
            public double IcomplianceSOPC_A;
            /// <summary>
            /// sweep delay time between two sweep point
            /// </summary>
            public double sweepDelayTime_ms;

        }

        /// <summary>
        /// Enum for frequency set mode
        /// </summary>
        public enum EnumFrequencySetMode
        {
            /// <summary>
            /// 
            /// </summary>
            ByChan,
            /// <summary>
            /// 
            /// </summary>
            ByFrequency,
            /// <summary>
            /// 
            /// </summary>
            ByWavelenth,
        }

        public enum TiaBandwidth
        {
            _10point0_GHz = -4,
            _10point5_GHz = -3,
            _10point9_GHz = -2,
            _11point1_GHz = -1,
            _11point5_GHz = 0,
            _12point8_GHz = 1,
            _14point0_GHz = 2,
            _14point8_GHz = 3,
            _15point4_GHz = 4,

        }

        #endregion
    }
}

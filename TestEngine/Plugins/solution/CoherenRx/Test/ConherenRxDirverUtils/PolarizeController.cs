using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.ToolKit.ConherenRx
{
    /// <summary>
    /// class polarize controller instrument
    /// </summary>
    public class PolarizeController
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PolarizeController()
        {
            Allchans.Clear();
        }
        /// <summary>
        /// instrument for polarizer channel 1 controller
        /// </summary>
        public  Inst_SMUTI_TriggeredSMU Channel1;
        /// <summary>
        /// instrument for polarizer channel 2 controller
        /// </summary>
        public  Inst_SMUTI_TriggeredSMU Channel2;
        /// <summary>
        /// instrument for polarizer channel 3 controller
        /// </summary>
        public  Inst_SMUTI_TriggeredSMU Channel3;
        /// <summary>
        /// instrument for polarizer channel 4 controller
        /// </summary>
        public  Inst_SMUTI_TriggeredSMU Channel4;

        public Inst_Sub20ToAsic sub20;

        /// <summary>
        /// list contain instrument for polarizer all channel controller
        /// </summary>
        public List<Inst_SMUTI_TriggeredSMU> Allchans = new List<Inst_SMUTI_TriggeredSMU>();
    }
}

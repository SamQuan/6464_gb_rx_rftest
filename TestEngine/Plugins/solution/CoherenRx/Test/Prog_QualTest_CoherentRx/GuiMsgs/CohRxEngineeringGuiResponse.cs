using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsgs
{
    internal class CohRxEngineeringGuiResponse
    {
        internal CohRxEngineeringGuiResponse(double fiddleFactor)
        {
            this.FiddleFactor = fiddleFactor;
        }

        internal readonly double FiddleFactor;
    }
}
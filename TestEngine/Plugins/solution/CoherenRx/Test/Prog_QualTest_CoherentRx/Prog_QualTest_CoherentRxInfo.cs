using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TcmzCommonUtils;

namespace Bookham.TestSolution.TestPrograms
{
    internal class Prog_QualTest_CoherentRxInfo
    {
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        internal Specification MainSpec;

        internal CoheRxFinalTestConds TestConditions;
      
        internal Prog_QualTest_CoherentRxInstruments Instrs;

        internal TestParamConfigAccessor TestParamsConfig;

        internal TempTableConfigAccessor TempConfig;

        //internal TestSelection TestSelect;
    }
}

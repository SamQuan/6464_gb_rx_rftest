using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class EngineeringPowerConsession : UserControl
    {
        public EngineeringPowerConsession(Prog_QualTest_CoherentRxGui parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private Prog_QualTest_CoherentRxGui parent;

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string[] powerTarget = comboPower.Text.Split(' ');
            double parsedValue;
            if ( !double.TryParse(powerTarget[0], out parsedValue) ) return;

            GuiMsgs.CohRxEngineeringGuiResponse resp = new GuiMsgs.CohRxEngineeringGuiResponse(Convert.ToDouble(powerTarget[0]));
            parent.SendToWorker(resp);
            parent.CtrlFinished();
        }  
    }
}

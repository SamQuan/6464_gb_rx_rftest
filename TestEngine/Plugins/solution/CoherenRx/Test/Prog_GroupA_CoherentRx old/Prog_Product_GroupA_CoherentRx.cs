// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// Prog_CoherentReceiver.cs
//
// Author: sam.quan 2011.7
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestEngine.Config;
using System.Collections;
using Bookham.TestLibrary.Algorithms;
using System.Reflection;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
//using Bookham.TestSolution.TestScriptLanguage;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using System.Threading;

namespace Bookham.TestSolution.TestPrograms
{
    public class Prog_Product_GroupA_CoherentRx : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private Prog_Product_GroupA_CoherentRxInfo progInfo;
        private DatumList postStabResults;
        //private DatumList traceDataList;
        private DateTime testTime_Start;
        private DateTime testTime_End;
        //private double labourTime = 0;
        //private string errorInformation;
        //private bool qualTest;
        string pcasDeviceType;

        //private bool SelectTestFlag;
        private MultiSpecStatus allTestDataStatus;

        private MeasureSetupManage sigChainSetupManage = new MeasureSetupManage();
        private MeasureSetupManage locChainSetupManage = new MeasureSetupManage();

        private DatumList listEoSweepData = new DatumList();
        private DatumList listGroupDelayData = new DatumList();
        private DatumList listGDelayDeviationData = new DatumList();
        private List<double> listS21Data = new List<double>();
        private List<bool> listS21MaskPassFail = new List<bool>();
        private List<bool> listS22MaskPassFail = new List<bool>();
        private List<bool> listSigSprrMaskPassFail = new List<bool>();
        private List<bool> listLocSprrMaskPassFail = new List<bool>();
        private DatumList listDlpData = new DatumList();
        private List<double> listSigSprrData = new List<double>();
        private List<double> listLocSprrData = new List<double>();
        private List<double> listPdCurrentRatio = new List<double>();
        private DatumList fcuNoiseCurrentList = new DatumList();
        private DatumList listPwrAndPdCurrentSprrTest = new DatumList();
        private double[] s21SweepFreqArray;
        private List<ParamContainer> ListParamContainer = new List<ParamContainer>();

        private double powerSetDualInput_Sig_dB;
        private double powerSetDualInput_Loc_dB;
        private double powerSetOnlySigInput_Sig_dB;
        private double powerSetOnlyLocInput_Loc_dB;
        private double powerZeroDBRefSig_dBm;
        private double powerZeroDBRefLoc_dBm;
        

        //private double delayXchip_Sig;
        //private double delayXChip_Loc;
        //private double delayYChip_Sig;
        //private double delayYChip_Loc;

        private List<double> listTestPower_dB = new List<double>();

        string s22PlotFile;
        string s21StatisticsFile;
        string sprrStatisticsFile;
        string s22MaskPassFailFile;
        string s21MaskPassFailFile;
        string sprrMaskPassFailFile;
        string pdCurrentRatioFile;
        string dlpAndGDDivationFile;
        DatumList listRawDataFile;
        DatumList listRawDataFile1;
        DatumList listRawDataFile2;
        DatumList listProcessingSprrFile;
        DatumList listS21PlotFile;
        DatumList listSprrPlotFile;
        //DatumList listSprrChipXCurrentFile;
        //DatumList listSprrChipYCurrentFile;

        string s21CurrentChipXFile;
        string s21CurrentChipYFile;

        DatumList listSprrCurrentFile;

        //string sprrChipXCurrentFile;
        //string sprrChipYCurrentFile;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public Prog_Product_GroupA_CoherentRx()
        {
            this.progInfo = new Prog_Product_GroupA_CoherentRxInfo();
        }

        public Type UserControl
        {
            get { return typeof(Prog_Product_GroupA_CoherentRxGui); }
        }

        #region Program Initialisation
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs,
            ChassisCollection chassis)
        {
            foreach (Chassis aChassis in chassis.Values)
            {
                aChassis.EnableLogging = false;
            }
            foreach (Instrument var in instrs.Values)
            {
                var.EnableLogging = false;
            }

            testTime_Start = DateTime.Now;

           //string[] DeviceType = dutObject.PartCode.Split('-');
           // pcasDeviceType = DeviceType[0];
            pcasDeviceType = dutObject.PartCode;

            // initialise config
            this.initConfig(dutObject, engine);

            // load specification
            this.loadSpecs(engine, dutObject);

            // initialise instruments
            this.initInstrs(engine, instrs, chassis);

            //initialise Optical Chain Setup Manage
            InitializeOpticalChainSetupManage();

            // load specification
 //           this.loadSpecs(engine, dutObject);
            // init modules
            this.initModules(engine, instrs, dutObject);
        }

        /// <summary>
        /// Initialise optical chain settup manage class
        /// </summary>
        private void InitializeOpticalChainSetupManage()
        {
            RxSplitterRatioCalData splitterRatioCalData_Sig;
            RxSplitterRatioCalData splitterRatioCalData_Loc;
            string splitterName_Sig;
            string configFilePath_Sig;
            string dataTableName_Sig;
            string splitterName_Loc;
            string configFilePath_Loc;
            string dataTableName_Loc;

            splitterName_Sig = "SigPathSplitter";
            configFilePath_Sig = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\SplitterRatioCalSettings.xml";
            dataTableName_Sig = "DataBaseSettings";
            splitterName_Loc = "LocPathSplitter";
            configFilePath_Loc = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\SplitterRatioCalSettings.xml";
            dataTableName_Loc = "DataBaseSettings";
            splitterRatioCalData_Sig = new RxSplitterRatioCalData(splitterName_Sig, configFilePath_Sig, dataTableName_Sig);
            splitterRatioCalData_Loc = new RxSplitterRatioCalData(splitterName_Loc, configFilePath_Loc, dataTableName_Loc);

            //sigChainSetupManage.AuxPd = progInfo.Instrs.AuxPd;
            sigChainSetupManage.LightwaveComponentAnalyzer = progInfo.Instrs.LightwaveComponentAnalyzer;
            sigChainSetupManage.OPM_Mon = progInfo.Instrs.OPMMon_Sig;
            sigChainSetupManage.OPM_Ref = progInfo.Instrs.OPMRef_Sig;
            sigChainSetupManage.PdBias = progInfo.Instrs.PdSource;
            sigChainSetupManage.PolCtrl = progInfo.Instrs.PolController_Sig;
            sigChainSetupManage.VOA = progInfo.Instrs.VOA_Sig;
            sigChainSetupManage.VOA2 = progInfo.Instrs.VOAForCutOffPwrInput_Sig;
            sigChainSetupManage.DelayLine = progInfo.Instrs.DelayLine_Sig;
            sigChainSetupManage.LockInAmplifier = progInfo.Instrs.LockInAmplifier;
            sigChainSetupManage.sub20_Pol = progInfo.Instrs.pol_sig;
            if (progInfo.Instrs.laserSource != null)
            {
                sigChainSetupManage.LaserSource = progInfo.Instrs.laserSource;
            }
            sigChainSetupManage.SplitterRatiosCalData = splitterRatioCalData_Sig;
            sigChainSetupManage.SplitterRatiosCalData.FrequencyTolerance_Ghz = 0.05;
            sigChainSetupManage.FcuNoiseCurrentList = fcuNoiseCurrentList;

            //locChainSetupManage.AuxPd = progInfo.Instrs.AuxPd;
            locChainSetupManage.LightwaveComponentAnalyzer = progInfo.Instrs.LightwaveComponentAnalyzer;
            locChainSetupManage.OPM_Mon = progInfo.Instrs.OPMMon_Loc;
            locChainSetupManage.OPM_Ref = progInfo.Instrs.OPMRef_Loc;
            locChainSetupManage.PdBias = progInfo.Instrs.PdSource;
            locChainSetupManage.PolCtrl = progInfo.Instrs.PolController_Loc;
            locChainSetupManage.VOA = progInfo.Instrs.VOA_Loc;
            locChainSetupManage.VOA2 = progInfo.Instrs.VOAForCutOffPwrInput_Loc;
            locChainSetupManage.DelayLine = progInfo.Instrs.DelayLine_Loc;
            locChainSetupManage.LockInAmplifier = progInfo.Instrs.LockInAmplifier;
            locChainSetupManage.sub20_Pol = progInfo.Instrs.pol_loc;
            if (progInfo.Instrs.laserSource != null)
            {
                locChainSetupManage.LaserSource = progInfo.Instrs.laserSource;
            }
            locChainSetupManage.SplitterRatiosCalData = splitterRatioCalData_Loc;
            locChainSetupManage.SplitterRatiosCalData.FrequencyTolerance_Ghz = 0.05;
            locChainSetupManage.FcuNoiseCurrentList = fcuNoiseCurrentList;
        
        }

        /// <summary>
        /// initialise configuration
        /// </summary>
        /// <param name="dutObj"></param>
        /// <param name="engine"></param>
        private void initConfig(DUTObject dutObj, ITestEngineInit engine)
        {
            progInfo.TestParamsConfig = new TestParamConfigAccessor(dutObj,
                @"Configuration\ConherentRx\CoherentReceiverTestParams.xml", "", "CoheRxTestParams");

            progInfo.TempConfig = new TempTableConfigAccessor(dutObj, 1,
                @"Configuration\ConherentRx\TempTable.xml");

            progInfo.WaveLengthTestSelect = new TestSelection(@"Configuration\ConherentRx\TestSelect.xml");

            progInfo.ParamTestSelect = new TestSelection(@"Configuration\ConherentRx\ParamTestSelect.xml");
        }

        /// <summary>
        /// load Specification
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        private void loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (progInfo.TestParamsConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                string mainSpecNameFile = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileName");
                string mainSpecFullFilename = progInfo.TestParamsConfig.GetStringParam("PcasLimitFileDirectory")
                    + @"\" + mainSpecNameFile;
                mainSpecKeys.Add("Filename", mainSpecFullFilename);

            }
            else
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();
                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);//"final"
                mainSpecKeys.Add("DEVICE_TYPE", pcasDeviceType);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);

            // Get our specification object (so we can initialise modules with appropriate limits)
            progInfo.MainSpec = tempSpecList[0];

            // declare spec list to Test Engine
            SpecList specList = new SpecList();
            specList.Add(progInfo.MainSpec);

            engine.SetSpecificationList(specList);

            progInfo.TestConditions = new CoheRxGAFinalTestConds(progInfo.MainSpec);

            listTestPower_dB.Add(progInfo.TestConditions.zeroDBRefPower_1_Sig_dBm);
            listTestPower_dB.Add(progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);

        }

        
 //           #region Step2: measure S22

//            DoS22Measure(engine, moduleRunReturn);

//            #endregion
        
        
        
        /// <summary>
        /// initialise modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        private void initModules(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject)
        {
            //this.Initialise_InitModule(engine, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Mid", 25, progInfo.TestConditions.MidTemp);
            //this.Mod_CheckSpliterRatio_InitModule(engine, instrs, dutObject);
            this.CaseTempControl_InitModule(engine, "CaseTemp_Safe", progInfo.TestConditions.MidTemp, 25);
            this.MeasureSetupOpticalInputPower_InitModule(engine, instrs, dutObject);
            this.Pincheck_InitModule(engine, instrs, dutObject);

            foreach (string portName in Enum.GetNames(typeof(RfOutputEnum)))
            {
                if (portName.Contains("OFFALL"))
                {
                    continue;
                }
                RfOutputEnum rfOutput = (RfOutputEnum)Enum.Parse(typeof(RfOutputEnum), portName);
                this.MeasureS22_InitModule(engine, instrs, dutObject, rfOutput);
                this.MeasurementOE_InitModule(engine, instrs, dutObject, rfOutput);
                for (int i = 0; i < listTestPower_dB.Count; i++)
                {
                    if (double.IsNaN(listTestPower_dB[i]))
                    {
                        continue;
                    }
                    this.MeasureS21_InitModule(engine, instrs, dutObject, rfOutput, i + 1);
                }
                
                this.MeasureDLP_InitModule(engine, instrs, dutObject, rfOutput);
                this.MeasureGroupDelay_InitModule(engine, instrs, dutObject, rfOutput);
            }

            foreach (string tiaName in Enum.GetNames(typeof(TiaEnum)))
            {
                TiaEnum tia = (TiaEnum)Enum.Parse(typeof(TiaEnum), tiaName);
                for (int i = 0; i < listTestPower_dB.Count; i++)
                {
                    if (double.IsNaN(listTestPower_dB[i]))
                    {
                        continue;
                    }
                    this.MeasureSPRR_InitModule(engine, instrs, dutObject, tia, i + 1);
                }
            }

            foreach (string hybirdChipName in Enum.GetNames(typeof(HybirdChipEnum)))
            {
                HybirdChipEnum hybirdChip = (HybirdChipEnum)Enum.Parse(typeof(HybirdChipEnum), hybirdChipName);
                for (int i = 0; i < listTestPower_dB.Count; i++)
                {
                    if (double.IsNaN(listTestPower_dB[i]))
                    {
                        continue;
                    }
                    this.MeasureSetupOpticalPathDelay_InitModule(engine, instrs, dutObject, hybirdChip, i + 1);
                }
                this.MeasureSetupOpticalPolarisation_InitModule(engine, instrs, dutObject, hybirdChip);
                
                this.Mod_TiaVgcAndBandwidthSweep_InitModule(engine, instrs, dutObject, hybirdChip);
                
            }

            this.MeasureGroupDelayDivation_InitModule(engine, instrs, dutObject);

        }

        /// <summary>
        /// initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        private void initInstrs(ITestEngineInit engine, InstrumentCollection instrs, ChassisCollection chassis)
        {

            Prog_Product_GroupA_CoherentRxInstruments progInstrs = new Prog_Product_GroupA_CoherentRxInstruments();
            progInstrs.pol_sig  = (Inst_Sub20ToAsic)instrs["Pol_sig"];
            progInstrs.pol_loc = (Inst_Sub20ToAsic)instrs["Pol_loc"];
            progInstrs.pol_loc.SetDefaultState();
            progInstrs.pol_loc.pol_write(1009,1);

            //progInstrs.pol_sig.SetDefaultState();
            //progInstrs.pol_sig.pol_write(900, 0);

   //         progInstrs.pol_sig.gpio_config(0xffffffff, 0xffffffff);
   //         progInstrs.pol_sig.gpio_write(0x0fffffff, 0xffffffff);
   //         progInstrs.pol_sig.gpio_write(0x0000, 0xffffffff);
            

    //      
          //  progInstrs.DelayLine_Sig = (InstType_ODL)instrs["DelayLine_Sig"];
            //progInstrs.DelayLine_Sig.SetDefaultState();
            //progInstrs.DelayLine_Sig.Delay_ps = 150;
            //progInstrs.DelayLine_Sig.Delay_ps = 250;
    //        if (progInstrs.DelayLine_Sig.DriverName.Contains  ( "MDL002"))
    //        {

    //        }
    //       progInstrs.sub20

            if (instrs.Contains("LaserSource"))
            {
                progInstrs.laserSource = (Inst_iTLATunableLaserSource)instrs["LaserSource"];
            }

            //TIA
            progInstrs.Tia_X = new TiaInstrument();
            progInstrs.Tia_Y = new TiaInstrument();
            
            //TIA_XI
            progInstrs.Tia_X.VccSupply = (Inst_Ke24xx)instrs["VccSupply_TiaX"];

            progInstrs.Tia_X.Vgc = (Inst_Ke24xx)instrs["VGC_TiaX"];


            if (instrs.Contains("BandWidth_H_TiaX"))
            {
                progInstrs.Tia_X.BandWidth_H = (Inst_Ke24xx)instrs["BandWidth_H_TiaX"];
                progInstrs.Tia_X.BandWidth_L = (Inst_Ke24xx)instrs["BandWidth_L_TiaX"];
            }


            //TIA_Y
            progInstrs.Tia_Y.VccSupply = (Inst_Ke24xx)instrs["VccSupply_TiaY"];
            progInstrs.Tia_Y.Vgc = (Inst_Ke24xx)instrs["VGC_TiaY"];

            if (instrs.Contains("BandWidth_H_TiaY"))
            {
                progInstrs.Tia_Y.BandWidth_H = (Inst_Ke24xx)instrs["BandWidth_H_TiaY"];
                progInstrs.Tia_Y.BandWidth_L = (Inst_Ke24xx)instrs["BandWidth_L_TiaY"];
            }

            //pd bias
            progInstrs.PdSource = new PdBiasInstrument();
            progInstrs.PdSource.PdSource_XIpos = (Inst_Ke24xx)instrs["XIpos"];
            progInstrs.PdSource.PdSource_XIneg = (Inst_Ke24xx)instrs["XIneg"];
            progInstrs.PdSource.PdSource_XQpos = (Inst_Ke24xx)instrs["XQpos"];
            progInstrs.PdSource.PdSource_XQneg = (Inst_Ke24xx)instrs["XQneg"];
            progInstrs.PdSource.PdSource_YIpos = (Inst_Ke24xx)instrs["YIpos"];
            progInstrs.PdSource.PdSource_YIneg = (Inst_Ke24xx)instrs["YIneg"];
            progInstrs.PdSource.PdSource_YQpos = (Inst_Ke24xx)instrs["YQpos"];
            progInstrs.PdSource.PdSource_YQneg = (Inst_Ke24xx)instrs["YQneg"];


            // TECs
            progInstrs.TecCase = (IInstType_TecController)instrs["TecCase"];

            // OPMs
            progInstrs.OPMMon_Sig = (InstType_OpticalPowerMeter)instrs["OpmMon_Sig"];
            progInstrs.OPMMon_Loc = (InstType_OpticalPowerMeter)instrs["OpmMon_Loc"];
            progInstrs.OPMRef_Sig = (InstType_OpticalPowerMeter)instrs["OpmRef_Sig"];
            progInstrs.OPMRef_Loc = (InstType_OpticalPowerMeter)instrs["OpmRef_Loc"];

            //LightwaveComponentAnalyzer
            progInstrs.LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)instrs["LightwaveAnalyzer"];

            //WaveGenerator
            progInstrs.WaveformGenerator = (Instr_Ag33120A)instrs["WaveGenerator"];

            //locker in amplifier
            progInstrs.LockInAmplifier = (Inst_SR830)instrs["LockInAmplifier"];

            //VOA
            progInstrs.VOA_Sig = (Inst_OzDd100mc_Attenuator)instrs["VOA_Sig"];
            progInstrs.VOA_Loc = (Inst_OzDd100mc_Attenuator)instrs["VOA_Loc"];

            if (instrs.Contains("VOAForCutOffInput_Sig"))
            {
                progInstrs.VOAForCutOffPwrInput_Sig = (Inst_OzDd100mc_Attenuator)instrs["VOAForCutOffInput_Sig"];
            }
            if (instrs.Contains("VOAForCutOffInput_Loc"))
            {
                progInstrs.VOAForCutOffPwrInput_Loc = (Inst_OzDd100mc_Attenuator)instrs["VOAForCutOffInput_Loc"];
            }

            //delay line
            progInstrs.DelayLine_Sig = (InstType_ODL)instrs["DelayLine_Sig"];
            progInstrs.DelayLine_Loc = (InstType_ODL)instrs["DelayLine_Loc"];

            //sig pol controller
            progInstrs.PolController_Sig = new PolarizeController();
          
            progInstrs.PolController_Sig.Channel1 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel1"];
            progInstrs.PolController_Sig.Channel2 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel2"];
            //progInstrs.PolController_Sig.Channel3 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel3"];
            //progInstrs.PolController_Sig.Channel4 = (Inst_SMUTI_TriggeredSMU)instrs["SigSOPC_Channel4"];
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel1);
            progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel2);
            //progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel3);
            //progInstrs.PolController_Sig.Allchans.Add(progInstrs.PolController_Sig.Channel4);

            //loc pol controller
            progInstrs.PolController_Loc = new PolarizeController();
            progInstrs.PolController_Loc.Channel1 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel1"];
            progInstrs.PolController_Loc.Channel2 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel2"];
            //progInstrs.PolController_Loc.Channel3 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel3"];
            //progInstrs.PolController_Loc.Channel4 = (Inst_SMUTI_TriggeredSMU)instrs["LocSOPC_Channel4"];
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel1);
            progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel2);
            //progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel3);
            //progInstrs.PolController_Loc.Allchans.Add(progInstrs.PolController_Loc.Channel4);


            // RFswitch path management
            Util_SwitchPathManager switchPathManager = new Util_SwitchPathManager
                (@"Configuration\ConherentRx\RfSwitch.xml", instrs);
            Switch_LCA_RF switchRF = new Switch_LCA_RF(switchPathManager);
            progInstrs.RfSwitch = switchRF;

            progInfo.Instrs = progInstrs;

            if (!engine.IsSimulation)
            {
                // Configure TEC controllers
                ConfigureTecController(progInstrs.TecCase, "TecCase");

                //configure lightWave analyser
               ConfigureLightwave(progInstrs.LightwaveComponentAnalyzer);

                //configure power meter
                progInstrs.OPMMon_Sig.SetDefaultState();
                progInstrs.OPMMon_Loc.SetDefaultState();
                progInstrs.OPMRef_Sig.SetDefaultState();
                progInstrs.OPMRef_Loc.SetDefaultState();

                //configure Wave generator
                progInstrs.WaveformGenerator.SetDefaultState();


                //configure mzm supply
                //progInstrs.MzmSupply.SetDefaultState();
                //progInstrs.MzmSupply.OutputEnabled = false;

                //configure locker in amplifier
                progInstrs.LockInAmplifier.SetDefaultState();

                //configure pol,disable pol ctrl
      //          ConfigurePolController(progInstrs.PolController_Sig);
      //          ConfigurePolController(progInstrs.PolController_Loc);

                //configure laser source supply
                if (progInfo.Instrs.laserSource != null)
                {
                    progInstrs.laserSource.SetDefaultState();
                }
                //configure TIA_X controllers
                ConfigureTiaController(progInstrs.Tia_X.Vgc);
                ConfigureTiaController(progInstrs.Tia_X.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_X.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_X.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_X.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_X.VccSupply);

                //configure TIA_XQ controllers
                //need this jdConfigureTiaController(progInstrs.Tia_XQ.Vgc);
                //need this jd ConfigureTiaController(progInstrs.Tia_XQ.ModeCtrlOfGC);
                //need this jdConfigureTiaController(progInstrs.Tia_XQ.OutputAmplitudeCtrlInAGC);
                //need this jdConfigureTiaController(progInstrs.Tia_XQ.OutputCtrl);
                //need this jdConfigureTiaController(progInstrs.Tia_XQ.PeakVoltDetector);
                //need this jdConfigureTiaController(progInstrs.Tia_XQ.VccSupply);

                //configure TIA_Y controllers
                ConfigureTiaController(progInstrs.Tia_Y.Vgc);
                ConfigureTiaController(progInstrs.Tia_Y.ModeCtrlOfGC);
                ConfigureTiaController(progInstrs.Tia_Y.OutputAmplitudeCtrlInAGC);
                ConfigureTiaController(progInstrs.Tia_Y.OutputCtrl);
                ConfigureTiaController(progInstrs.Tia_Y.PeakVoltDetector);
                ConfigureTiaController(progInstrs.Tia_Y.VccSupply);

                //configure TIA_YQ controllers
                //ConfigureTiaController(progInstrs.Tia_Y.Vgc);
                //need this jdConfigureTiaController(progInstrs.Tia_YQ.ModeCtrlOfGC);
                //need this jdConfigureTiaController(progInstrs.Tia_YQ.OutputAmplitudeCtrlInAGC);
                //need this jdConfigureTiaController(progInstrs.Tia_YQ.OutputCtrl);
                //need this jdConfigureTiaController(progInstrs.Tia_YQ.PeakVoltDetector);
                //need this jdConfigureTiaController(progInstrs.Tia_YQ.VccSupply);

                //configure dc bias pd
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XIpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XIneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XQpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_XQneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YIpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YIneg);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YQpos);
                ConfigurePdBiasController(progInstrs.PdSource.PdSource_YQneg);

                //configure TIA band width ctrl
                ConfigureTiaBandWidthController(progInstrs.Tia_X);
                ConfigureTiaBandWidthController(progInstrs.Tia_Y);

                //off all rf connet to vna
                progInstrs.RfSwitch.SetState(Switch_LCA_RF.State.OffConnent);

  
                engine.SendStatusMsg("initialise VOA");
                //init voa to max attenuation
                progInstrs.VOA_Sig.SetDefaultState();
                progInstrs.VOA_Loc.SetDefaultState();


                if (progInstrs.VOAForCutOffPwrInput_Sig.IsOnline)
                {
                    progInstrs.VOAForCutOffPwrInput_Sig.SetDefaultState();
                }
                if (progInstrs.VOAForCutOffPwrInput_Loc.IsOnline)
                {
                    progInstrs.VOAForCutOffPwrInput_Loc.SetDefaultState();
                }



                //init delay lines
                engine.SendStatusMsg("initialise delay line");
                progInstrs.DelayLine_Sig.SetDefaultState();
                progInstrs.DelayLine_Loc.SetDefaultState();

                //need this jd MeasureFcuNoiseCurrent(engine, progInstrs);
            }

            engine.SendStatusMsg("Instruments initialised");
        }
        
        #endregion

        #region Program Running

        public void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            powerZeroDBRefSig_dBm = progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm;
            powerZeroDBRefLoc_dBm = progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm;
            powerSetDualInput_Sig_dB = progInfo.TestParamsConfig.GetDoubleParam("powerSetDualInput_Sig_dB");
            powerSetDualInput_Loc_dB = progInfo.TestParamsConfig.GetDoubleParam("powerSetDualInput_Loc_dB");
            powerSetOnlySigInput_Sig_dB = progInfo.TestParamsConfig.GetDoubleParam("powerSetOnlySigInput_Sig_dB");
            powerSetOnlyLocInput_Loc_dB = progInfo.TestParamsConfig.GetDoubleParam("powerSetOnlyLocInput_Loc_dB");

            // get feedforward data
//            if (progInfo.TestParamsConfig.GetBoolParam("GetChipInformationFromPcas"))
//            {
//                GetChipInformationFromPcas(engine, dutObject);
//            }
            //set Fcu noise to trace data
            if (!engine.IsSimulation)
            {
                if (dutObject.TestStage.Contains("rf qual") || dutObject.TestStage.Contains("rf test")||dutObject.TestStage.Contains("rf_groupa"))
                {
                    SetFcuNoiseTraceData();
                }
            }


            #region Creat file name
            listRawDataFile = new DatumList();
            listRawDataFile1 = new DatumList();
            listRawDataFile2 = new DatumList();
            listProcessingSprrFile = new DatumList();
            listS21PlotFile = new DatumList();
            listSprrPlotFile = new DatumList();
            listSprrCurrentFile = new DatumList();

            string coheRxFileDirectory = progInfo.TestParamsConfig.GetStringParam("CoheRxFileDirectory");

            s22PlotFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "S22Plot_", dutObject.SerialNumber, "csv");
            s22PlotFile = Directory.GetCurrentDirectory() + "\\" + s22PlotFile;

            s22MaskPassFailFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "S22MaskPassFail_", dutObject.SerialNumber, "csv");
            s22MaskPassFailFile = Directory.GetCurrentDirectory() + "\\" + s22MaskPassFailFile;

            s21StatisticsFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "S21StatPlot_", dutObject.SerialNumber, "csv");
            s21StatisticsFile = Directory.GetCurrentDirectory() + "\\" + s21StatisticsFile;

            sprrStatisticsFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "SprrStatPlot_", dutObject.SerialNumber, "csv");
            sprrStatisticsFile = Directory.GetCurrentDirectory() + "\\" + sprrStatisticsFile;

            s21MaskPassFailFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "S21MaskPassFail_", dutObject.SerialNumber, "csv");
            s21MaskPassFailFile = Directory.GetCurrentDirectory() + "\\" + s21MaskPassFailFile;

            sprrMaskPassFailFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "SprrMaskPassFail_", dutObject.SerialNumber, "csv");
            sprrMaskPassFailFile = Directory.GetCurrentDirectory() + "\\" + sprrMaskPassFailFile;

            pdCurrentRatioFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "PdCurrentRatio_", dutObject.SerialNumber, "csv");
            pdCurrentRatioFile = Directory.GetCurrentDirectory() + "\\" + pdCurrentRatioFile;

            dlpAndGDDivationFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "dlpAndGDDivation_", dutObject.SerialNumber, "csv");
            dlpAndGDDivationFile = Directory.GetCurrentDirectory() + "\\" + dlpAndGDDivationFile;


            foreach (string rfPortName in Enum.GetNames(typeof(RfOutputEnum)))
            {
                if (rfPortName.Contains("OFFALL"))
                {
                    continue;
                }
                string rawDataFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "rawData_" + rfPortName, dutObject.SerialNumber, "csv");
                rawDataFile = Directory.GetCurrentDirectory() + "\\" + rawDataFile;
                listRawDataFile.AddString(rfPortName, rawDataFile);

                string rawDataFile1 = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "rawData1_" + rfPortName, dutObject.SerialNumber, "csv");
                rawDataFile1 = Directory.GetCurrentDirectory() + "\\" + rawDataFile1;
                listRawDataFile1.AddString(rfPortName, rawDataFile1);


                string rawDataFile2 = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "rawData2_" + rfPortName, dutObject.SerialNumber, "csv");
                rawDataFile2 = Directory.GetCurrentDirectory() + "\\" + rawDataFile2;
                listRawDataFile2.AddString(rfPortName, rawDataFile2);

                string processingSprrFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "processingSprr_" + rfPortName, dutObject.SerialNumber, "csv");
                processingSprrFile = Directory.GetCurrentDirectory() + "\\" + processingSprrFile;
                listProcessingSprrFile.AddString(rfPortName, processingSprrFile);
            }

            for (int i = 0; i < listTestPower_dB.Count; i++)
            {
                if (double.IsNaN(listTestPower_dB[i]))
                {
                    continue;
                }
                string s21PlotFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "s21Plot_P" +
                    listTestPower_dB[i].ToString(), dutObject.SerialNumber, "csv");
                s21PlotFile = Directory.GetCurrentDirectory() + "\\" + s21PlotFile;
                listS21PlotFile.AddString("s21Plot_P" + listTestPower_dB[i].ToString(), s21PlotFile);

                string sprrPlotFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "sprrPlot_P" +
                    listTestPower_dB[i].ToString(), dutObject.SerialNumber, "csv");
                sprrPlotFile = Directory.GetCurrentDirectory() + "\\" + sprrPlotFile;
                listSprrPlotFile.AddString("sprrPlot_P" + listTestPower_dB[i].ToString(), sprrPlotFile);

                string sprrCurrentFile = Util_GenerateFileName.GenWithTimestamp(coheRxFileDirectory, "sprrCurrent_P" +
                     listTestPower_dB[i].ToString(), dutObject.SerialNumber, "csv");
                sprrCurrentFile = Directory.GetCurrentDirectory() + "\\" + sprrCurrentFile;
                listSprrCurrentFile.AddString("sprrCurrent_P" + listTestPower_dB[i].ToString(), sprrCurrentFile);
            }
            #endregion

            ModuleRun modRun;

            ModuleRunReturn moduleRunReturn;

            ModuleRunReturn dualInputEoMeasModuleRunReturn;

            #region Step1: Set case temperature to mid temperature
            // Set and wait for temperatures to stabilise

            moduleRunReturn = engine.RunModule("CaseTemp_Mid");

            #endregion


            if (progInfo.Instrs.VOAForCutOffPwrInput_Sig.IsOnline )
            {
                progInfo.Instrs.VOAForCutOffPwrInput_Sig.Attenuation_dB = 0.0;
                progInfo.Instrs.VOAForCutOffPwrInput_Loc.Attenuation_dB = 0.0;
            }

            modRun = engine.GetModuleRun("Mod_Pincheck");
            modRun.ConfigData.AddOrUpdateString("partcode", "0000");
            moduleRunReturn = engine.RunModule("Mod_Pincheck");

            #region Step2: measure S22

            DoS22Measure(engine, moduleRunReturn);

            #endregion

            //yubo:  change to FCU----kethley
            if (progInfo.Instrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                UsedFrontOrBackTerminals(useFrontTerminals);
            }

            int unTestPowerCounts = 0;
            foreach (string hybirdChipName in Enum.GetNames(typeof(HybirdChipEnum)))
            {

            #region Step3: Power up device,tia control and pd bias .ect

            //set the laser 
            progInfo.Instrs.laserSource.WavelengthINnm = progInfo.TestConditions.LaserWavelength_nm;
            progInfo.Instrs.laserSource.BeamEnable = true;
            sigChainSetupManage.OPM_Mon.Wavelength_nm = progInfo.TestConditions.LaserWavelength_nm;
            sigChainSetupManage.OPM_Ref.Wavelength_nm = progInfo.TestConditions.LaserWavelength_nm;
            locChainSetupManage.OPM_Mon.Wavelength_nm = progInfo.TestConditions.LaserWavelength_nm;
            locChainSetupManage.OPM_Ref.Wavelength_nm = progInfo.TestConditions.LaserWavelength_nm;

            modRun = engine.GetModuleRun("Mod_MeasureSetupOpticalInputPower");
            modRun.ConfigData.AddOrUpdateString("partcode", dutObject.PartCode);
            moduleRunReturn = engine.RunModule("Mod_MeasureSetupOpticalInputPower");

            #endregion

            //control for current test is tiaX or tiaY
//            int unTestPowerCounts = 0;
//            foreach (string hybirdChipName in Enum.GetNames(typeof(HybirdChipEnum)))
//            {
                #region switch corresponding rf port to vna

                //switch corresponding rf port to vna
                if (!engine.IsSimulation)
                {
                    string vnaSetupFile = "";
                    string vnaEmbedingSettingFile = "";
                    if (hybirdChipName.Contains("X"))
                    {
                        //yubo
                        if (progInfo.Instrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
                        {
                            bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                            UsedFrontOrBackTerminals(useFrontTerminals);
                        }
                        SwitchRfPortToVNA(RfOutputEnum.XIP);

                        if (progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
                        {
                            int registerNumber = progInfo.TestParamsConfig.GetIntParam("calXS21ChipRegister");
                            progInfo.Instrs.LightwaveComponentAnalyzer.RecallRegister(registerNumber);
                        }
                        else if (progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("MS464"))
                        {

                            vnaSetupFile = "'c:\\testset\\cal\\XChipSetup_S21.chx'";

                            vnaEmbedingSettingFile = "'c:\\testset\\cal\\MZEMBEDDING.edl'";

                            Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
                            MS4046A.RecalSetup(vnaSetupFile);
                            Thread.Sleep(7000);
                            MS4046A.RecalEmbedingSetting(vnaEmbedingSettingFile);
 //                           Thread.Sleep(2000);
                        }
                        else
                        {
                            engine.ErrorInProgram("can't not recognized equipment " + progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity);
                        }
                    }
                    else
                    {
                        //yubo
                        if (progInfo.Instrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
                        {
                            bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                            UsedFrontOrBackTerminals(!useFrontTerminals);
                        }

                        SwitchRfPortToVNA(RfOutputEnum.YIP);

                        if (progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
                        {
                            int registerNumber = progInfo.TestParamsConfig.GetIntParam("calYS21ChipRegister");
                            progInfo.Instrs.LightwaveComponentAnalyzer.RecallRegister(registerNumber);
                        }
                        else if (progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("MS464"))
                        {
                            vnaSetupFile = "'c:\\testset\\cal\\YChipSetup_S21.chx'"; 
                            
                            vnaEmbedingSettingFile = "'c:\\testset\\cal\\MZEMBEDDING.edl'";

                            Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
                            MS4046A.RecalSetup(vnaSetupFile);
                            Thread.Sleep(7000);
                            MS4046A.RecalEmbedingSetting(vnaEmbedingSettingFile);
//                         Thread.Sleep(2000);
                        }
                        else
                        {
                            engine.ErrorInProgram("can't not recognized equipment " + progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity);
                        }
                    }
                }

                #endregion

                #region Step4: set optical ploarisation

                SwitchRfPortToVNA(RfOutputEnum.OFFALL);

                modRun = engine.GetModuleRun("Mod_MeasureSetupOpticalPolarisation" + hybirdChipName);
                modRun.ConfigData.AddOrUpdateString("hybirdChip", hybirdChipName);
                modRun.ConfigData.AddOrUpdateDouble("zeroDBRefPowerSig_dBm", powerZeroDBRefSig_dBm);
                modRun.ConfigData.AddOrUpdateDouble("zeroDBRefPowerLoc_dBm", powerZeroDBRefLoc_dBm);
                moduleRunReturn = engine.RunModule("Mod_MeasureSetupOpticalPolarisation" + hybirdChipName);

                if (hybirdChipName.Contains("X"))
                {
                    SwitchRfPortToVNA(RfOutputEnum.XIP);
                }
                else
                {
                    SwitchRfPortToVNA(RfOutputEnum.YIP);
                }
                #endregion


                #region step5:set optical path delayline

                modRun = engine.GetModuleRun("Mod_MeasureSetupOpticalPathDelay" + hybirdChipName + "_P2");
                modRun.ConfigData.AddOrUpdateString("hybirdChip", hybirdChipName);
                modRun.ConfigData.AddOrUpdateDouble("wavelength_nm", progInfo.TestConditions.LaserWavelength_nm);
                modRun.ConfigData.AddOrUpdateDouble("zeroDBRefPowerSig_dBm", powerZeroDBRefSig_dBm);
                modRun.ConfigData.AddOrUpdateDouble("zeroDBRefPowerLoc_dBm", powerZeroDBRefLoc_dBm);
                moduleRunReturn = engine.RunModule("Mod_MeasureSetupOpticalPathDelay" + hybirdChipName + "_P2");

                #endregion

                unTestPowerCounts = 0;
                for (int iCountsRefPwr = 0; iCountsRefPwr < listTestPower_dB.Count; iCountsRefPwr++)
                {
                    //if (!progInfo.TestSelect.IsTestSelected(listTestPower_dB[iCountsWL], "PowerSelect"))
                    //{
                    //    continue;
                    //}
                    if (double.IsNaN(listTestPower_dB[iCountsRefPwr]))
                    {
                        unTestPowerCounts++;
                        continue;
                    }

                    #region initialize any list variable

                    int zeroDBrefPowerNum = iCountsRefPwr + 1;
                    ParamContainer paramContainer = new ParamContainer();
                    if (hybirdChipName.Contains("X"))
                    {
                        listS21Data = new List<double>();
                        listLocSprrData = new List<double>();
                        listSigSprrData = new List<double>();
                        listS21MaskPassFail = new List<bool>();
                        listLocSprrMaskPassFail = new List<bool>();
                        listSigSprrMaskPassFail = new List<bool>();
                        listPdCurrentRatio = new List<double>();
                        listGroupDelayData = new DatumList();

                        paramContainer.listS21Data = listS21Data;
                        paramContainer.listLocSprrData = listLocSprrData;
                        paramContainer.listSigSprrData = listSigSprrData;
                        paramContainer.listLocSprrMaskPassFail = listLocSprrMaskPassFail;
                        paramContainer.listSigSprrMaskPassFail = listSigSprrMaskPassFail;
                        paramContainer.listGroupDelayData = listGroupDelayData;
                        paramContainer.listPdCurrentRatio = listPdCurrentRatio;
                        paramContainer.listS21MaskPassFail = listS21MaskPassFail;

                        ListParamContainer.Add(paramContainer);

                    }
                    else
                    {
                        listS21Data = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listS21Data;
                        listS21MaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listS21MaskPassFail;
                        listLocSprrData = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listLocSprrData;
                        listSigSprrData = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listSigSprrData;
                        listLocSprrMaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listLocSprrMaskPassFail;
                        listSigSprrMaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listSigSprrMaskPassFail;
                        listGroupDelayData = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listGroupDelayData;
                        listPdCurrentRatio = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listPdCurrentRatio;

                        paramContainer.listS21Data = listS21Data;
                        paramContainer.listS21MaskPassFail = listS21MaskPassFail;
                        paramContainer.listLocSprrData = listLocSprrData;
                        paramContainer.listSigSprrData = listSigSprrData;
                        paramContainer.listLocSprrMaskPassFail = listLocSprrMaskPassFail;
                        paramContainer.listSigSprrMaskPassFail = listSigSprrMaskPassFail;
                        paramContainer.listGroupDelayData = listGroupDelayData;
                        paramContainer.listPdCurrentRatio = listPdCurrentRatio;

                        ListParamContainer[ListParamContainer.Count - 1] = paramContainer;
                    }

                    #endregion

                    #region updata zero dB point power
                    
                    //change zero dB ref point power
                    if (!engine.IsSimulation)
                    {
                        if (iCountsRefPwr == 0)
                        {
                            powerZeroDBRefSig_dBm = progInfo.TestConditions.zeroDBRefPower_1_Sig_dBm;
                            powerZeroDBRefLoc_dBm = progInfo.TestConditions.zeroDBRefPower_1_Loc_dBm;
                        }
                        else
                        {
                            powerZeroDBRefSig_dBm = progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm;
                            powerZeroDBRefLoc_dBm = progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm;
                        }
                    }

                    #endregion

                    //control for current test is XI XQ or YI YQ
                    foreach (string tiaName in Enum.GetNames(typeof(TiaEnum)))
                    {

                        listEoSweepData = new DatumList();

                        //judge current test is tia_X OR tia_Y
                        if (tiaName.Contains(hybirdChipName))
                        {
                            engine.SendStatusMsg("******************************************************************* ");
                            engine.SendStatusMsg("Test TIA: " + tiaName.ToString() + " refe power " + listTestPower_dB[iCountsRefPwr] + "_dBm");

                            //control for current test is XIP,XIN,XQP,XQN,YIP,YIN,YQP or YQN 
                            foreach (string rfPortName in Enum.GetNames(typeof(RfOutputEnum)))
                            {
                                if (rfPortName.Contains("OFFALL"))
                                {
                                    continue;
                                }
                                if (rfPortName.Contains(tiaName))
                                {
                                    engine.SendStatusMsg("***************************************************************");
                                    engine.SendStatusMsg("Test RF port: " + rfPortName.ToString() + " refer power " + listTestPower_dB[iCountsRefPwr] + "_dBm");

                                    string rawDataFile = listRawDataFile.ReadString(rfPortName);
                                    string rawDataFile1 = listRawDataFile1.ReadString(rfPortName);
                                    string rawDataFile2 = listRawDataFile2.ReadString(rfPortName);
                                    string processingSprrFile = listProcessingSprrFile.ReadString(rfPortName);

                                    #region Step6: switch corresponding RF port to vna
                                    //switch corresponding rf port to vna
                                    RfOutputEnum rfPortEnum = (RfOutputEnum)Enum.Parse(typeof(RfOutputEnum), rfPortName);
                                     
                                    if (!engine.IsSimulation)
                                    {
                                        SwitchRfPortToVNA(rfPortEnum);
                                    }
                                    listPwrAndPdCurrentSprrTest = new DatumList();
                                    #endregion

                                    #region Step7: Sparameter sweep, Capture x&y data

                                    //Set power for signal(-3dB) and local(0dB) measrue x&y data
                                    engine.SendStatusMsg("Signal path power: " + powerSetDualInput_Sig_dB.ToString() + " dB");
                                    engine.SendStatusMsg("Local path power: " + powerSetDualInput_Loc_dB.ToString() + " dB");

                                    engine.SendStatusMsg(" ");
                                    engine.SendStatusMsg("Optical input: Dual");
                                    modRun = engine.GetModuleRun("Mod_MeasureEO" + rfPortName);

                                    double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;
                                    modRun.ConfigData.AddOrUpdateDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                                    modRun.ConfigData.AddOrUpdateDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                                    modRun.ConfigData.AddOrUpdateDouble("targetPwrSig_dBm", powerZeroDBRefSig_dBm + powerSetDualInput_Sig_dB);
                                    modRun.ConfigData.AddOrUpdateDouble("targetPwrLoc_dBm", powerZeroDBRefLoc_dBm + powerSetDualInput_Loc_dB);
                                    string SweepMode = progInfo.ParamTestSelect.SweepModeSelected(dutObject.PartCode);
                                    modRun.ConfigData.AddString("SweepMode", SweepMode);


                                    //run module
                                    moduleRunReturn = engine.RunModule("Mod_MeasureEO" + rfPortName, true);

                                    dualInputEoMeasModuleRunReturn = moduleRunReturn;

                                    Trace realAndImaginaryTrace =
                                        (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_EO");
                                    if (SweepMode == "Sweep2")
                                    {
                                        Trace realAndImaginaryTrace1 =
                                            (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_1");
                                        Trace realAndImaginaryTrace2 =
                                            (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_2");
                                        SaveRealAndImaginaryData(rawDataFile1, realAndImaginaryTrace1, "dual", listTestPower_dB[iCountsRefPwr]);
                                        SaveRealAndImaginaryData(rawDataFile2, realAndImaginaryTrace2, "dual", listTestPower_dB[iCountsRefPwr]);
                                    }

                                    s21SweepFreqArray = realAndImaginaryTrace.GetXArray();
                                    listPdCurrentRatio.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadDouble("PdCurrentRatio"));
                                    if (SweepMode == "Sweep2")
                                    {
                                        listPdCurrentRatio.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadDouble("PdCurrentRatio_180"));
                                    }

                                    SaveRealAndImaginaryData(rawDataFile, realAndImaginaryTrace, "dual", listTestPower_dB[iCountsRefPwr]);

                                    SaveSprrPowerAndPdCurrent(moduleRunReturn, hybirdChipName, rfPortName, false, "dual", listTestPower_dB[iCountsRefPwr]);

                                    //SaveS21PowerAndPdCurrent(moduleRunReturn, hybirdChipName, rfPortName);

                                    #endregion

                                    #region Step8: extract magintude data,and calculate -3dB bandwidth
                                    //measure s21,and extract magintude data,and calculate -3dB bandwidth
                                    modRun = engine.GetModuleRun("Mod_MeasureS21_" + rfPortName.ToString() + "_P" + zeroDBrefPowerNum.ToString());
                                    modRun.ConfigData.AddOrUpdateReference("xyRawData", realAndImaginaryTrace);

                                    moduleRunReturn = engine.RunModule("Mod_MeasureS21_" + rfPortName.ToString() + "_P" + zeroDBrefPowerNum.ToString(), true, true);

                                    listS21Data.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadDouble("BandWidth_S21_GHz"));
                                    listS21MaskPassFail.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadBool("S21MaskPassFail"));

                                    //save s21 magintude data
                                    ArrayList s21PlotData = (ArrayList)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("Plot_S21");
                                    SaveS21MagintudeData(listS21PlotFile.ReadString("s21Plot_P" + listTestPower_dB[iCountsRefPwr].ToString()),
                                        rfPortName, s21PlotData, s21SweepFreqArray);

                                    SaveSprrProcessingData(listProcessingSprrFile.ReadString(rfPortName), s21PlotData, listTestPower_dB[iCountsRefPwr]);

                                    #endregion

                                    #region Step9: Calculate dlp
                                    //Calculate dlp
                                    modRun = engine.GetModuleRun("Mod_MeasureDLP" + rfPortName);
                                    modRun.ConfigData.AddOrUpdateReference("xyRawData", realAndImaginaryTrace);
                                    moduleRunReturn = engine.RunModule("Mod_MeasureDLP" + rfPortName, true, true);

                                    Trace s21PhaseTrace = (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("s21PhaseTrace");
                                    Trace s21UnwrapPhaseTrace = (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("s21UnwrapPhaseTrace");
                                    double[] phaseRawDataArray = s21PhaseTrace.GetYArray();
                                    double[] phaseUnwrapDataArray = s21UnwrapPhaseTrace.GetYArray();
                                    double[] phaseUnwrapLinearArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("unWrapPhaseLinearArray");
                                    double[] dlpRawArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("dlpRawArray");
                                    double[] dlpSmoothArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("dlpSmoothArray");

                                    if (iCountsRefPwr == 1)
                                    {
                                        listDlpData.AddDoubleArray(rfPortName, dlpSmoothArray);
                                    }

                                    //SavePhaseAndDlpData(rawDataFile, dlpPlotFile, rfPortName, phaseRawDataArray, phaseUnwrapDataArray,
                                    //    phaseUnwrapLinearArray, dlpRawArray, dlpSmoothArray, s21SweepFreqArray);
                                    SaveSprrProcessingData(listProcessingSprrFile.ReadString(rfPortName), dlpSmoothArray, "dlp", listTestPower_dB[iCountsRefPwr]);


                                    #endregion

                                    #region Step10: Calculate group delay
                                    //Calculate group delay

                                    modRun = engine.GetModuleRun("Mod_MeasureGroupDelay" + rfPortName);
                                    modRun.ConfigData.AddOrUpdateReference("xyRawData", realAndImaginaryTrace);
                                    moduleRunReturn = engine.RunModule("Mod_MeasureGroupDelay" + rfPortName, true, true);
                                    double[] gDRawDataArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("GDRawDataArray");
                                    double[] sweepFreqArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("SweepFreqArray");
                                    double[] firstSmoothGDArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("firstSmoothGDArray");
                                    double[] secondSmoothGDArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("secondSmoothGDArray");
                                    double[] groupDelayDiscardBelow1GHzArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("GroupDelayDiscardBelow1GHzArray");
                                    DatumList listGDArray = new DatumList();
                                    listGDArray.AddOrUpdateDoubleArray("Slope", gDRawDataArray);
                                    listGDArray.AddOrUpdateDoubleArray("First smooth", firstSmoothGDArray);
                                    listGDArray.AddOrUpdateDoubleArray("Second smooth", secondSmoothGDArray);
                                    listGDArray.AddOrUpdateDoubleArray("GroupDelay", groupDelayDiscardBelow1GHzArray);

                                    if (modRun.ConfigData.IsPresent("ALU"))
                                    {
                                        double[] thirdSmoothGDArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("thirdSmoothGDArray");
                                        double[] freqForALUArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("freqArrayForALU");
                                        double[] gdArrayForALU = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("gdArrayForALU");
                                        listGDArray.AddOrUpdateDoubleArray("Third smooth", thirdSmoothGDArray);
                                        listGDArray.AddOrUpdateDoubleArray("Freq for ALU", freqForALUArray);
                                        listGDArray.AddOrUpdateDoubleArray("GD for ALU", gdArrayForALU);
                                        listGroupDelayData.AddOrUpdateDoubleArray("GDAarray_" + rfPortName, gdArrayForALU);
                                    }
                                    else
                                    {
                                        listGroupDelayData.AddOrUpdateDoubleArray("GDAarray_" + rfPortName, secondSmoothGDArray);
                                    }

                                    listGroupDelayData.AddOrUpdateDoubleArray("sweepFreqArray_GHz", sweepFreqArray);

                                    //save group delay data
                                    //SaveGroupDelayData(rawDataFile, gDelayPlotFile, rfPortName, listGDArray, s21SweepFreqArray);
                                    SaveSprrProcessingData(listProcessingSprrFile.ReadString(rfPortName), secondSmoothGDArray, "gd", listTestPower_dB[iCountsRefPwr]);

                                    #endregion

                                    #region Step11: change sig/loc to different inpupt power,do sweep,Capture xyData

                                    //listPwrAndPdCurrentSprrTest = new DatumList();
                                    bool bWriteFileFlag = false;
                                    for (int i = 0; i < 3; i++)
                                    {
                                        modRun = engine.GetModuleRun("Mod_MeasureEO" + rfPortName);
                                        double targetPwrSig_dBm = double.NegativeInfinity;
                                        double targetPwrLoc_dBm = double.NegativeInfinity;
                                        string eoSweepDataName = "";
                                        string inPutPowerPortName = "";
                                        if (i == 2)
                                        {
                                            bWriteFileFlag = true;
                                        }
                                        switch (i)
                                        {
                                            //sigInputPwr=-9dB,locInputPwr=-6dB
                                            case 0:
                                                //    targetPwrSig_dBm = powerZeroDBRefSig_dBm + powerSetDualInput_Sig_dB;
                                                //    targetPwrLoc_dBm = powerZeroDBRefLoc_dBm + powerSetDualInput_Loc_dB;
                                                eoSweepDataName = rfPortName.Substring(rfPortName.Length - 1, 1) + "_S21";
                                                listEoSweepData.AddOrUpdateReference(eoSweepDataName,
                                                    (Trace)dualInputEoMeasModuleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_EO"));
                                                continue;
                                            //    inPutPowerPortName = "Dual";
                                            //    break;
                                            //sigInputPwr=-3dB,locInputPwr=null
                                            case 1:
                                                targetPwrSig_dBm = powerZeroDBRefSig_dBm + powerSetOnlySigInput_Sig_dB;
                                                targetPwrLoc_dBm = double.NegativeInfinity;
                                                eoSweepDataName = rfPortName.Substring(rfPortName.Length - 1, 1) + "_S21_SIG";
                                                inPutPowerPortName = "Sig";

                                                if (progInfo.Instrs.VOAForCutOffPwrInput_Sig != null && (progInfo.Instrs.VOAForCutOffPwrInput_Sig.IsOnline == true))
                                                {
                                                    progInfo.Instrs.VOAForCutOffPwrInput_Sig.OutputEnabled = true;
                                                    progInfo.Instrs.VOAForCutOffPwrInput_Loc.OutputEnabled = false;
                                                }
                                                break;
                                            //sigInputPwr=null,locInputPwr=0dB
                                            case 2:
                                                targetPwrSig_dBm = double.NegativeInfinity;
                                                targetPwrLoc_dBm = powerZeroDBRefLoc_dBm + powerSetOnlyLocInput_Loc_dB;
                                                eoSweepDataName = rfPortName.Substring(rfPortName.Length - 1, 1) + "_S21_LOC";
                                                inPutPowerPortName = "Loc";
                                                if (progInfo.Instrs.VOAForCutOffPwrInput_Sig != null && (progInfo.Instrs.VOAForCutOffPwrInput_Sig.IsOnline == true))
                                                {
                                                    progInfo.Instrs.VOAForCutOffPwrInput_Loc.OutputEnabled = true;
                                                    progInfo.Instrs.VOAForCutOffPwrInput_Sig.OutputEnabled = false;
                                                }
                                                break;
                                            default:
                                                break;
                                        }

                                        engine.SendStatusMsg("Signal path power: " + targetPwrSig_dBm.ToString() + "dB");
                                        engine.SendStatusMsg("Local path power: " + targetPwrLoc_dBm.ToString() + "dB");

                                        modRun.ConfigData.AddOrUpdateDouble("targetPwrSig_dBm", targetPwrSig_dBm);
                                        modRun.ConfigData.AddOrUpdateDouble("targetPwrLoc_dBm", targetPwrLoc_dBm);
                                        modRun.ConfigData.AddOrUpdateBool("sprrTest", true);
                                        engine.SendStatusMsg("      ");
                                        engine.SendStatusMsg("Optical input: " + inPutPowerPortName);
                                        engine.SendStatusMsg("Run mod_MeasureEO" + rfPortName);
                                        moduleRunReturn = engine.RunModule("Mod_MeasureEO" + rfPortName, true, true);
                                        listEoSweepData.AddOrUpdateReference(eoSweepDataName, (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_EO"));

                                        realAndImaginaryTrace =
                                        (Trace)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("XYRawData_EO");
                                        SaveRealAndImaginaryData(rawDataFile, realAndImaginaryTrace, inPutPowerPortName, listTestPower_dB[iCountsRefPwr]);

                                        SaveSprrPowerAndPdCurrent(moduleRunReturn, hybirdChipName, rfPortName, bWriteFileFlag, inPutPowerPortName, listTestPower_dB[iCountsRefPwr]);
                                    }

                                    //set this two voa to no attnuation
                                    if (progInfo.Instrs.VOAForCutOffPwrInput_Sig != null && (progInfo.Instrs.VOAForCutOffPwrInput_Sig.IsOnline == true))
                                    {
                                        progInfo.Instrs.VOAForCutOffPwrInput_Sig.OutputEnabled = true;
                                        progInfo.Instrs.VOAForCutOffPwrInput_Loc.OutputEnabled = true;
                                        Thread.Sleep(2000);
                                    }
                                    #endregion

                                    #region Set traceList
                                    //Set tracelist
                                    if ((iCountsRefPwr - unTestPowerCounts) == 0)
                                    {
                                        DatumList rawDataTraceDataList = new DatumList();
                                        rawDataTraceDataList.AddFileLink("RAW_" + rfPortName + "_FILE", listRawDataFile.ReadString(rfPortName));
                                        rawDataTraceDataList.AddFileLink("PRO_" + rfPortName + "_FILE", listProcessingSprrFile.ReadString(rfPortName));
                                        this.progInfo.MainSpec.SetTraceData(rawDataTraceDataList);
                                    }

                                    if (rfPortName.Contains("XIP"))
                                    {
                                        DatumList plotFileTraceDataList = new DatumList();
                                        plotFileTraceDataList.AddFileLink("PLOT_S21_P" + zeroDBrefPowerNum,
                                            listS21PlotFile.ReadString("s21Plot_P" + listTestPower_dB[iCountsRefPwr].ToString()));
                                        plotFileTraceDataList.AddFileLink("PLOT_SPRR_P" + zeroDBrefPowerNum,
                                            listSprrPlotFile.ReadString("sprrPlot_P" + listTestPower_dB[iCountsRefPwr].ToString()));

                                        this.progInfo.MainSpec.SetTraceData(plotFileTraceDataList);
                                    }

                                    if (rfPortName.Contains("IP"))
                                    {
                                        DatumList sprrCurrentTraceDataList = new DatumList();
                                        if (rfPortName.Contains("XIP"))
                                        {
                                            sprrCurrentTraceDataList.AddFileLink("SPRR_POWER_IP_P" + zeroDBrefPowerNum + "_FILE",
                                                listSprrCurrentFile.ReadString("sprrCurrent_P" + listTestPower_dB[iCountsRefPwr].ToString()));
                                        }
                                        //else
                                        //{
                                        //    sprrCurrentTraceDataList.AddFileLink("SPRR_POWER_IP_Y_WL" + wavelengthNum + "_FILE",
                                        //            listSprrChipYCurrentFile.ReadString("sprrCurrent_ChipY_WL" + listTestWavelength_nm[iCountsWL].ToString()));
                                        //}
                                        this.progInfo.MainSpec.SetTraceData(sprrCurrentTraceDataList);
                                    }

                                    #endregion
                                }
                            }

                            #region Step12: Calculate SPRR

                            modRun = engine.GetModuleRun("Mod_MeasureSPRR_" + tiaName + "_P" + zeroDBrefPowerNum.ToString());
                            modRun.ConfigData.AddOrUpdateListDatum("listEoSweepData", listEoSweepData);
                            modRun.ConfigData.AddOrUpdateDouble("wavelengthNum", listTestPower_dB[iCountsRefPwr]);
                            moduleRunReturn = engine.RunModule("Mod_MeasureSPRR_" + tiaName + "_P" + zeroDBrefPowerNum.ToString(), true, true);

                            listSigSprrData.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadDouble("sprr_Sig_GHz"));
                            listLocSprrData.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadDouble("sprr_Loc_GHz"));
                            listSigSprrMaskPassFail.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadBool("sprrSigMaskCheck"));
                            listLocSprrMaskPassFail.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadBool("sprrLocMaskCheck"));

                            double[] sprrArray_Sig = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_Sig");
                            double[] sprrArray_Loc = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_Loc");
                            double[] sprrArray_P_Sig = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_P_Sig");
                            double[] sprrArray_P_Loc = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_P_Loc");
                            double[] sprrArray_N_Sig = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_N_Sig");
                            double[] sprrArray_N_Loc = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("sprrArray_N_Loc");

                            SaveSprrPlotData(listSprrPlotFile.ReadString("sprrPlot_P" + listTestPower_dB[iCountsRefPwr].ToString()),
                                s21SweepFreqArray, sprrArray_Sig, tiaName, "Sig");
                            SaveSprrPlotData(listSprrPlotFile.ReadString("sprrPlot_P" + listTestPower_dB[iCountsRefPwr].ToString()),
                                s21SweepFreqArray, sprrArray_Loc, tiaName, "Loc");

                            SaveSprrProcessingData(listProcessingSprrFile.ReadString(tiaName + "P"), sprrArray_P_Sig,
                                "SPRR_Sig", listTestPower_dB[iCountsRefPwr]);
                            SaveSprrProcessingData(listProcessingSprrFile.ReadString(tiaName + "P"), sprrArray_P_Loc,
                                "SPRR_LOC", listTestPower_dB[iCountsRefPwr]);
                            SaveSprrProcessingData(listProcessingSprrFile.ReadString(tiaName + "N"), sprrArray_N_Sig,
                                "SPRR_Sig", listTestPower_dB[iCountsRefPwr]);
                            SaveSprrProcessingData(listProcessingSprrFile.ReadString(tiaName + "N"), sprrArray_N_Loc,
                                "SPRR_LOC", listTestPower_dB[iCountsRefPwr]);



                            #endregion
                        }
                    }

                    #region Step13: Tia vgc and bandwidth sweep
                    if (iCountsRefPwr == 1)
                    {
                        modRun = engine.GetModuleRun("Mod_TiaVgcAndBandwidthSweep" + hybirdChipName);
                        modRun.ConfigData.AddDouble("targetPwrSig_dBm", (powerZeroDBRefSig_dBm + powerSetDualInput_Sig_dB));
                        modRun.ConfigData.AddDouble("targetPwrLoc_dBm", (powerZeroDBRefLoc_dBm + powerSetDualInput_Loc_dB));
                        modRun.ConfigData.AddOrUpdateString("partcode", dutObject.PartCode);
                        //ADD BW setting for VGC sweep
                        modRun.ConfigData.AddOrUpdateSint32("BW_OVER_VGC", progInfo.TestConditions.BW_OVER_VGC);
                        engine.RunModule("Mod_TiaVgcAndBandwidthSweep" + hybirdChipName, true, true);
                    }
                    #endregion
                }

            }   //jump to here

            #region Step14: Calculate group delay deviation

            modRun = engine.GetModuleRun("Mod_MeasureGroupDelayDeviation");
            modRun.ConfigData.AddListDatum("listGroupDelayData", ListParamContainer[ListParamContainer.Count - 1].listGroupDelayData);
            modRun.ConfigData.AddListDatum("dlpArrayData", listDlpData);
            modRun.ConfigData.AddString("testStage", dutObject.TestStage);
            engine.RunModule("Mod_MeasureGroupDelayDeviation", true, true);

            #endregion

            #region Step15: Save 21 and sprr statistics data

            unTestPowerCounts = 0;
            for (int iCountsRefPwr = 0; iCountsRefPwr < listTestPower_dB.Count; iCountsRefPwr++)
            {
                if (double .IsNaN(listTestPower_dB[iCountsRefPwr]))
                {
                    unTestPowerCounts++;
                    continue;
                }
                int zeroDBrefPowerNum = iCountsRefPwr + 1;

                listS21Data = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listS21Data;
                listLocSprrData = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listLocSprrData;
                listSigSprrData = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listSigSprrData;
                listS21MaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listS21MaskPassFail;
                listSigSprrMaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listSigSprrMaskPassFail;
                listLocSprrMaskPassFail = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listLocSprrMaskPassFail;
                listPdCurrentRatio = ListParamContainer[iCountsRefPwr - unTestPowerCounts].listPdCurrentRatio;

                //save s21 and sprr statistics data 
                SaveS21StatisticsPlotData(s21StatisticsFile, listTestPower_dB[iCountsRefPwr].ToString(), listS21Data);
                SaveSprrStatisticsPlotData(engine, sprrStatisticsFile, listTestPower_dB[iCountsRefPwr].ToString(),
                    listSigSprrData, listLocSprrData);
                SaveS21MaskPassFailData(s21MaskPassFailFile, listTestPower_dB[iCountsRefPwr].ToString(), listS21MaskPassFail);
                SaveSprrMaskPassFailData(engine, sprrMaskPassFailFile, listTestPower_dB[iCountsRefPwr].ToString(),
                    listSigSprrMaskPassFail, listLocSprrMaskPassFail);
                SavePdCurrentRatioData(pdCurrentRatioFile, listTestPower_dB[iCountsRefPwr].ToString(), listPdCurrentRatio);
            
                //calculate s21 deviation
                CalculateS21Deviation(zeroDBrefPowerNum, listS21Data);

                listS21Data.Sort();
                listSigSprrData.Sort();
                listLocSprrData.Sort();
                listPdCurrentRatio.Sort();
                DatumList traceDataList = new DatumList();
                int s21MaskCheck = 1;
                int sprrMaskCheck_Sig = 1;
                int sprrMaskCheck_Loc = 1;
                for (int i = 0; i < listS21MaskPassFail.Count; i++)
                {
                    if (!listS21MaskPassFail[i])
                    {
                        s21MaskCheck = 0;
                        break;
                    }
                }

                for (int i = 0; i < listSigSprrMaskPassFail.Count; i++)
                {
                    if (!listSigSprrMaskPassFail[i])
                    {
                        sprrMaskCheck_Sig = 0;
                        break;
                    }
                }
                for (int i = 0; i < listLocSprrMaskPassFail.Count; i++)
                {
                    if (!listLocSprrMaskPassFail[i])
                    {
                        sprrMaskCheck_Loc = 0;
                        break;
                    }
                }

                traceDataList.AddSint32("S21_MASK_CHECK_P" + zeroDBrefPowerNum.ToString(), s21MaskCheck);
                traceDataList.AddSint32("SPRR_SIG_MASK_CHECK_P" + zeroDBrefPowerNum.ToString(), sprrMaskCheck_Sig);
                traceDataList.AddSint32("SPRR_LOC_MASK_CHECK_P" + zeroDBrefPowerNum.ToString(), sprrMaskCheck_Loc);
                traceDataList.AddDouble("MAX_S21_P" + zeroDBrefPowerNum.ToString(), listS21Data[listS21Data.Count - 1]);
                traceDataList.AddDouble("MIN_S21_P" + zeroDBrefPowerNum.ToString(), listS21Data[0]);
                traceDataList.AddDouble("MAX_SPRR_SIG_P" + zeroDBrefPowerNum.ToString(), listSigSprrData[listSigSprrData.Count - 1]);
                traceDataList.AddDouble("MAX_SPRR_LOC_P" + zeroDBrefPowerNum.ToString(), listLocSprrData[listLocSprrData.Count - 1]);
                traceDataList.AddDouble("MIN_CURRENT_RATIO_P" + zeroDBrefPowerNum.ToString(), listPdCurrentRatio[0]);
                this.progInfo.MainSpec.SetTraceData(traceDataList);
            }

            #endregion

            #region Step16: cool case temperature

            if (!engine.IsSimulation)
            {
                // Set device to safe case temperature
                //engine.RunModule("CaseTemp_Safe");
            }

            #endregion

            //set VNA to  s22 measure ment  yubo 2012-02-27

            string vnaSetup_File = "'c:\\testset\\cal\\YChipSetup_S22.chx'";
            engine.SendStatusMsg(vnaSetup_File);
            Instr_MS4640A_VNA MS4046A_VNA = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
            MS4046A_VNA.RecalSetup(vnaSetup_File);

            allTestDataStatus = engine.GetProgramDataStatus();
        }

        private void DoS22Measure(ITestEngineRun engine, ModuleRunReturn moduleRunReturn)
        {
            ButtonId respond = ButtonId.Cancel;

            if (!progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("MS464"))
            {
                int registerNumberS22 = progInfo.TestParamsConfig.GetIntParam("calXS22ChipRegister");
                progInfo.Instrs.LightwaveComponentAnalyzer.RecallRegister(registerNumberS22);
            }

            bool s22MaskCheckPassFail = true;
            do
            {
                s22MaskCheckPassFail = true;
                respond = ButtonId.Cancel;
                DatumList s22PassFailList = new DatumList();
                listS22MaskPassFail.Clear();

                foreach (Datum file in listRawDataFile)
                {
                    if (File.Exists(file.ValueToString()))
                    {
                        File.Delete(file.ValueToString());
                    }
                }
                if (File.Exists(s22PlotFile))
                {
                    File.Delete(s22PlotFile);
                }

                foreach (string rfPortName in Enum.GetNames(typeof(RfOutputEnum)))
                {
                    if (rfPortName.Contains("OFFALL"))
                    {
                        continue;
                    }
                    string rawDataFile = listRawDataFile.ReadString(rfPortName);
                    string sprrProcessingFile = listProcessingSprrFile.ReadString(rfPortName);
                    RfOutputEnum rfPortEnum = (RfOutputEnum)Enum.Parse(typeof(RfOutputEnum), rfPortName);
                    SwitchRfPortToVNA(rfPortEnum);

                    if (progInfo.Instrs.LightwaveComponentAnalyzer.HardwareIdentity.Contains("MS464"))
                    {
                        string vnaSetupFile = "";
                        //string vnaEmbedingSettingFile = "";
                        if (rfPortName.Contains("XIP"))
                        {
                            vnaSetupFile = "'c:\\testset\\cal\\XChipSetup_S22.chx'";
                        }
                        else if (rfPortName.Contains("YIP"))
                        {
                            vnaSetupFile = "'c:\\testset\\cal\\YChipSetup_S22.chx'";
                        }
                        else
                        { }
                        Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
                        MS4046A.RecalSetup(vnaSetupFile);
                        Thread.Sleep(3000);
                    }

                    MeasureS22(engine, moduleRunReturn, rfPortName, rawDataFile, sprrProcessingFile, s22PlotFile, ref s22PassFailList);
                }

                string PassFailContent = "s22 test fail,please check these RF port!\r\n";
                bool s22PassFail = true;
                foreach (Datum var in s22PassFailList)
                {
                    bool passFail = bool.Parse(var.ValueToString());
                    if (!passFail)
                    {
                        PassFailContent += " ";
                        PassFailContent += var.Name;
                        s22PassFail = false;
                    }
                }

                foreach (bool var in listS22MaskPassFail)
                {
                    if (!var)
                    {
                        s22MaskCheckPassFail = false;
                        break;
                    }
                }

                if (!s22PassFail || !s22MaskCheckPassFail)
                {
                    ButtonInfo[] buttonsInfo ={ 
                                            new ButtonInfo("Retest S22 after check RF port!(检查RF端口连接后重测 S22）", ButtonId.Continue),
                                            new ButtonInfo("Skip S22 and Continue test(跳过检查继续测试)",ButtonId.Cancel),
                                       };
                    respond = (ButtonId)engine.ShowUserQuery(PassFailContent, buttonsInfo);

                }

                else
                {

                }
            } while (respond != ButtonId.Cancel);

            SaveS22MaskPassFailData(s22MaskPassFailFile, "s22MaskResult", listS22MaskPassFail);
            
            //Set tracelist
            DatumList s22TraceDataList = new DatumList();
            s22TraceDataList.AddOrUpdateFileLink("PLOT_S22", s22PlotFile);
            s22TraceDataList.AddOrUpdateFileLink("S22_MASK_FILE", s22MaskPassFailFile);
            if (this.progInfo.MainSpec.ParamLimitExists("S22_MASK_CHECK"))
            {
                if (s22MaskCheckPassFail)
                {
                    s22TraceDataList.AddOrUpdateSint32("S22_MASK_CHECK", 1);
                }
                else
                {
                    s22TraceDataList.AddOrUpdateSint32("S22_MASK_CHECK", 0);
                }
            }
            this.progInfo.MainSpec.SetTraceData(s22TraceDataList);
        }

        #endregion

        #region End of Program

        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // Cleanup & power off
            if (!engine.IsSimulation)
            {
                // Display temperature progress
                engine.GuiShow();
                engine.GuiToFront();

                engine.SendStatusMsg("close all input!");

                this.progInfo.Instrs.VOA_Sig.OutputEnabled = false;
                this.progInfo.Instrs.VOA_Loc.OutputEnabled = false;

                if (this.progInfo.Instrs.laserSource != null)
                {
                    this.progInfo.Instrs.laserSource.BeamEnable = false;
                }

                //foreach (Inst_SMUTI_TriggeredSMU chan in this.progInfo.Instrs.PolController_Sig.Allchans)
                //{
                //    chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                //}
                //foreach (Inst_SMUTI_TriggeredSMU chan in this.progInfo.Instrs.PolController_Loc.Allchans)
                //{
                //    chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                //}

                //this.progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.OffConnent);
                //step1: power off Vgc and other control inputs
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_X);
                SetTiaCtrlVoltageOff(this.progInfo.Instrs.Tia_Y);
                
                //step2: power off Vcc
                this.progInfo.Instrs.Tia_X.VccSupply.OutputEnabled = false;
                this.progInfo.Instrs.Tia_Y.VccSupply.OutputEnabled = false;
                    
                //step3: power off Vpd
                SetPdBiasOFF();

                //SetCaseTempSafe(engine, 25);
                this.progInfo.Instrs.TecCase.OutputEnabled = false;
            }
        }

        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {

            // Write keys required for external data (example below for PCAS)    
            DatumList tcTraceData = new DatumList();

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in progInfo.MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("CH_"))
                {
                    if (!tcTraceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            tcTraceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            StringDictionary keys = new StringDictionary();

            // TODO: MUST Add real values below!

            keys.Add("SCHEMA", "HIBERDB");
            keys.Add("DEVICE_TYPE", pcasDeviceType);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", progInfo.MainSpec.Name);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);

            // Tell Test Engine about it...

            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)

            // !(beware, all these parameters MUST exist in the specification)!...

            //add testTime
            testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(testTime_Start);
            tcTraceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            dutObject.ProgramPluginVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            tcTraceData.AddString("SOFTWARE_IDENT", dutObject.ProgramPluginName + dutObject.ProgramPluginVersion);
            tcTraceData.AddString("OPERATOR_ID", userList.UserListString);
            tcTraceData.AddString("COMMENTS", engine.GetProgramRunComments());
            tcTraceData.AddString("PART_CODE", dutObject.PartCode);
            tcTraceData.AddString("SPEC_ID", progInfo.MainSpec.Name);
            tcTraceData.AddSint32("NODE", dutObject.NodeID);
            tcTraceData.AddString("EQUIP_ID", dutObject.EquipmentID);

            string timeDate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            tcTraceData.AddString("TIME_DATE", timeDate);


            //if (File.Exists(rawDataFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_S21_WL"+, rawDataFile);
            //}
            //if (File.Exists(dlpPlotFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_DLP", dlpPlotFile);
            //}
            //if (File.Exists(gDelayPlotFile))
            //{
            //    tcTraceData.AddFileLink("PLOT_GD", gDelayPlotFile);
            //}
            //if (File.Exists(sprrChipXCurrentFile))
            //{
            //    tcTraceData.AddFileLink("SPRR_POWER_IP_X_FILE", sprrChipXCurrentFile);
            //}
            //if (File.Exists(sprrChipYCurrentFile))
            //{
            //    tcTraceData.AddFileLink("SPRR_POWER_IP_Y_FILE", sprrChipYCurrentFile);
            //}
            if (File.Exists(s21StatisticsFile))
            {
                tcTraceData.AddFileLink("PLOT_S21_STAT", s21StatisticsFile);
            }
            if (File.Exists(sprrStatisticsFile))
            {
                tcTraceData.AddFileLink("PLOT_SPRR_STAT", sprrStatisticsFile);
            }
            if (File.Exists(s21MaskPassFailFile))
            {
                tcTraceData.AddFileLink("S21_MASK_FILE", s21MaskPassFailFile);
            }
            if (File.Exists(sprrMaskPassFailFile))
            {
                tcTraceData.AddFileLink("SPRR_MASK_FILE", sprrMaskPassFailFile);
            }
            if (File.Exists(pdCurrentRatioFile))
            {
                tcTraceData.AddFileLink("PLOT_CURRENT_RATIO_STAT", pdCurrentRatioFile);
            }

            if (File.Exists(s21CurrentChipXFile))
            {
                tcTraceData.AddFileLink("S21_POWER_IP_X_FILE", s21CurrentChipXFile);
            }
            if (File.Exists(s21CurrentChipYFile))
            {
                tcTraceData.AddFileLink("S21_POWER_IP_Y_FILE", s21CurrentChipYFile);
            }
            if (engine.GetProgramRunComments() != ProgramStatus.Success.ToString())
            {
                tcTraceData.AddString("TEST_STATUS", engine.GetProgramRunComments());
            }
            else
            {
                tcTraceData.AddString("TEST_STATUS", progInfo.MainSpec.Status.Status.ToString());
            }

            // pick the specification to add this data to...

            engine.SetTraceData(progInfo.MainSpec.Name, tcTraceData);
        }

        #endregion

        #region Instrument Initialise configure

        /// <summary>
        /// Configure a TEC controller using config data
        /// </summary>
        /// <param name="tecCtlr">Instrument reference</param>
        /// <param name="tecCtlId">Config table data prefix</param>
        private void ConfigureTecController(IInstType_TecController tecCtlr, string tecCtlId)
        {
            SteinhartHartCoefficients stCoeffs = new SteinhartHartCoefficients();

            // Keithley 2510 specific commands (must be done before 'SetDefaultState')
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.HardwareData["MinTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MinTemp_C");
                tecCtlr.HardwareData["MaxTemperatureLimit_C"] = progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_MaxTemp_C");
            }
            // Operating modes

            tecCtlr.SetDefaultState();
            tecCtlr.OperatingMode = (InstType_TecController.ControlMode)
                Enum.Parse(typeof(InstType_TecController.ControlMode), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_OperatingMode"));
            if (tecCtlr.DriverName.Contains("Ke2510"))
            {
                tecCtlr.Sensor_Type = (InstType_TecController.SensorType)
                    Enum.Parse(typeof(InstType_TecController.SensorType), progInfo.TestParamsConfig.GetStringParam(tecCtlId + "_Sensor_Type"));
            }


            // Thermistor characteristics
            if (tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire ||
                tecCtlr.Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire)
            {
                stCoeffs.A = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_A");
                stCoeffs.B = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_B");
                stCoeffs.C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_stCoeffs_C");
                tecCtlr.SteinhartHartConstants = stCoeffs;
            }

            // Additional parameters
            tecCtlr.TecCurrentCompliance_amp = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecCurrentCompliance_amp");
            //tecCtlr.TecVoltageCompliance_volt = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_TecVoltageCompliance_volt");
            tecCtlr.ProportionalGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_ProportionalGain");
            tecCtlr.IntegralGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_IntegralGain");
            //tecCtlr.DerivativeGain = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_DerivativeGain");
            tecCtlr.SensorTemperatureSetPoint_C = progInfo.TestParamsConfig.GetDoubleParam(tecCtlId + "_SensorTemperatureSetPoint_C");
        }
        /// <summary>
        /// Initialisation method for the 8703A Lightwave.
        /// Test Module assumes that the Test Program does this work.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrumentSet"></param>
        /// <param name="progConfigData"></param>
        private void ConfigureLightwave(InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer)
        {
            if (LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                LightwaveComponentAnalyzer.SetDefaultState();
            }
            else
            {
                string vnaSetupFile = "'c:\\testset\\cal\\XChipSetup_S22.chx'";

                Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)progInfo.Instrs.LightwaveComponentAnalyzer;
                MS4046A.RecalSetup(vnaSetupFile);
                Thread.Sleep(3000);
                MS4046A.ActiveMeausurementChannel = 0;
                
            }
        }
        /// <summary>
        /// confiugre pol controller
        /// </summary>
        /// <param name="polCtrl"></param>
        private static void ConfigurePolController(PolarizeController polCtrl)
        {
            foreach (Inst_SMUTI_TriggeredSMU chan in polCtrl.Allchans)
            {
                chan.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                chan.VoltageSetPoint_Volt = 0.0;
                chan.CurrentSetPoint_amp = 0.0;
            }
        }
        /// <summary>
        /// configure tia bandwidth controller
        /// </summary>
        /// <param name="TiaCtrl"></param>
        private static void ConfigureTiaBandWidthController(TiaInstrument TiaCtrl)
        {
            if (TiaCtrl.BandWidth_H != null)
            {
                TiaCtrl.BandWidth_H.CurrentComplianceSetPoint_Amp = 0.002;
                TiaCtrl.BandWidth_H.VoltageSetPoint_Volt = 3.3;
                TiaCtrl.BandWidth_H.OutputEnabled = false;
                TiaCtrl.BandWidth_L.CurrentComplianceSetPoint_Amp = 0.002;
                TiaCtrl.BandWidth_L.VoltageSetPoint_Volt = 3.3;
                TiaCtrl.BandWidth_L.OutputEnabled = false;
                 
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tiaCtrl"></param>
        /// <param name="TiaCtlId"></param>
        private void ConfigureTiaController(InstType_ElectricalSource tiaCtrl)
        {
            if (tiaCtrl != null)
            {
                tiaCtrl.CurrentSetPoint_amp = 0.0;
                tiaCtrl.VoltageSetPoint_Volt = 0.0;
                tiaCtrl.OutputEnabled = false;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="auxCtrl"></param>
        /// <param name="auxCtrlId"></param>
        private void ConfigureAuxBaisController(InstType_ElectricalSource auxCtrl)
        {
            // auxCtrl.SetDefaultState();
            auxCtrl.CurrentComplianceSetPoint_Amp = 0.0;
            auxCtrl.VoltageSetPoint_Volt = 0.0;
            auxCtrl.OutputEnabled = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="smuX"></param>
        /// <param name="p"></param>
        private void ConfigurePdBiasController(InstType_ElectricalSource pdBias)
        {
            // pdBias.SetDefaultState();
            pdBias.CurrentComplianceSetPoint_Amp = 0.002;
            pdBias.VoltageSetPoint_Volt = 0.0;
            pdBias.OutputEnabled = false;
        }
        #endregion

        #region Test module initialise Functions

       
        /// <summary>
        /// Get chip information from pcas
        /// </summary>
        /// <param name="engine">reference of engine</param>
        /// <param name="dutObject">dut object</param>
        private void GetChipInformationFromPcas(ITestEngineRun engine, DUTObject dutObject)
        {
            IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");   // always from SZN

            // Read Chip Information
            StringDictionary postStabKeys = new StringDictionary();
            postStabKeys.Add("SCHEMA", "hiberdb");
            postStabKeys.Add("TEST_STAGE", "package build");
            postStabKeys.Add("SERIAL_NO", dutObject.SerialNumber);

            this.postStabResults = dataRead.GetLatestResults(postStabKeys, false);

            if (this.postStabResults == null)
            {
                engine.ErrorInProgram("Can not get chip information of  DUT " + dutObject.SerialNumber.ToString() + " from PCAS!");
            }

            //Filling traceDataList...
            DatumList chipInfoTraceDataList = new DatumList();
            chipInfoTraceDataList.AddString("X_CHIP_BIN", postStabResults.ReadString("X_CHIP_BIN"));
            chipInfoTraceDataList.AddString("X_CHIP_ID", postStabResults.ReadString("X_CHIP_ID"));
            chipInfoTraceDataList.AddString("X_COC_SN", postStabResults.ReadString("X_COC_SN"));
            chipInfoTraceDataList.AddString("X_WAFER_ID", postStabResults.ReadString("X_WAFER_ID"));
            chipInfoTraceDataList.AddString("Y_CHIP_BIN", postStabResults.ReadString("Y_CHIP_BIN"));
            chipInfoTraceDataList.AddString("Y_CHIP_ID", postStabResults.ReadString("Y_CHIP_ID"));
            chipInfoTraceDataList.AddString("Y_COC_SN", postStabResults.ReadString("Y_COC_SN"));
            chipInfoTraceDataList.AddString("Y_WAFER_ID", postStabResults.ReadString("Y_WAFER_ID"));

            progInfo.MainSpec.SetTraceData(chipInfoTraceDataList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="moduleName"></param>
        /// <param name="currentTemp"></param>
        /// <param name="tempSetPoint"></param>
        private void CaseTempControl_InitModule(ITestEngineInit engine, string moduleName, Double currentTemp, Double tempSetPoint)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun(moduleName, "SimpleTempControl", "0.0.0.1");

            // Add instrument
            modRun.Instrs.Add("Controller", (Instrument)progInfo.Instrs.TecCase);

            // Add config data
            TempTablePoint tempCal = progInfo.TempConfig.GetTemperatureCal(currentTemp, tempSetPoint)[0];
            DatumList tempConfig = new DatumList();
            // Timeout = 2 * calibrated time, minimum of 60secs
            double timeout_s = Math.Max(tempCal.DelayTime_s * 15, 60);
            tempConfig.AddDouble("datumMaxExpectedTimeForOperation_s", timeout_s);
            // Stabilisation time = 0.5 * calibrated time
            tempConfig.AddDouble("RqdStabilisationTime_s", tempCal.DelayTime_s);
            tempConfig.AddSint32("TempTimeBtwReadings_ms", progInfo.TestParamsConfig.GetIntParam("CaseTempGui_UpdateTime_mS"));
            // Actual temperature to set
            tempConfig.AddDouble("SetPointTemperature_C", tempSetPoint + tempCal.OffsetC);
            tempConfig.AddDouble("TemperatureTolerance_C", tempCal.Tolerance_degC);
            modRun.ConfigData.AddListItems(tempConfig);
        }

        /// <summary>
        /// InitModule measure setup optical input power
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        private void MeasureSetupOpticalInputPower_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureSetupOpticalInputPower", "Mod_MeasureSetupOpticalInputPower", "");

            // Add instruments
            modRun.ConfigData.AddReference("Tia_X", progInfo.Instrs.Tia_X);
            modRun.ConfigData.AddReference("Tia_Y", progInfo.Instrs.Tia_Y);
            modRun.ConfigData.AddReference("PdBias", progInfo.Instrs.PdSource);
            //modRun.ConfigData.AddReference("AuxPd", progInfo.Instrs.AuxPd);

            modRun.ConfigData.AddReference("WaveformGenerator", progInfo.Instrs.WaveformGenerator);
            modRun.ConfigData.AddReference("LockInAmplifier", progInfo.Instrs.LockInAmplifier);
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);

            // Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));

            modRun.ConfigData.AddDouble("vmcOfTiaX_V", progInfo.TestConditions.VmcTiaX_V);
            modRun.ConfigData.AddDouble("vmcOfTiaY_V", progInfo.TestConditions.VmcTiaY_V);

            modRun.ConfigData.AddDouble("vccOfTia_V", progInfo.TestConditions.VccTia_V);

            modRun.ConfigData.AddDouble("vgcOfTiaX_V", progInfo.TestConditions.VgcTiaXI_V);
     //       modRun.ConfigData.AddDouble("vgcOfTiaXQ_V", progInfo.TestConditions.VgcTiaXQ_V);
    //        modRun.ConfigData.AddDouble("vgcOfTiaYI_V", progInfo.TestConditions.VgcTiaYI_V);
            modRun.ConfigData.AddDouble("vgcOfTiaY_V", progInfo.TestConditions.VgcTiaYI_V);

            modRun.ConfigData.AddDouble("pdBais_V", progInfo.TestConditions.PdReverseBias_V);
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);
            modRun.ConfigData.AddDouble("targetInputPower_Sig_dBm", progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);
            modRun.ConfigData.AddDouble("targetInputPower_Loc_dBm", progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm);
            //modRun.ConfigData.AddDouble("waveLength_nm", progInfo.TestConditions.LaserWavelength_nm);
            modRun.ConfigData.AddDouble("BandwidthSetting", progInfo.TestConditions.BandwidthSetting);
            modRun.ConfigData.AddDouble("pdComplanceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("pdComplanceCurrent_mA"));
            modRun.ConfigData.AddDouble("vccComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vccComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("vmcComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vmcComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("vgcComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vgcComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("bandWidthTiaComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("bandWidthTiaComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("waveformFreq_Khz", progInfo.TestParamsConfig.GetDoubleParam("waveformFreq_Khz"));
            modRun.ConfigData.AddDouble("waveformAmplitude_mV", progInfo.TestParamsConfig.GetDoubleParam("waveformAmplitude_mV"));
            modRun.ConfigData.AddDouble("waveformDcOffset_V", progInfo.TestParamsConfig.GetDoubleParam("waveformDcOffset_V"));
            modRun.ConfigData.AddBool("doMCControl", progInfo.TestParamsConfig.GetBoolParam("doMCControl"));
            modRun.ConfigData.AddBool("doBWControl", progInfo.TestParamsConfig.GetBoolParam("doBWControl"));

            // Tie up limits
            //modRun.Limits.AddParameter(progInfo.MainSpec, "INPUT_POWER_SIGNAL", "actualInputPower_Sig");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "INPUT_POWER_LOCAL", "actualInputPower_Loc");
            modRun.Limits.AddParameter(progInfo.MainSpec, "ICC_TIA_X", "iccTia_X");
            modRun.Limits.AddParameter(progInfo.MainSpec, "ICC_TIA_Y", "iccTia_Y");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_GC_YI", "igcTia_YI");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_GC_YQ", "igcTia_YQ");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_GC_XI", "igcTia_XI");
            modRun.Limits.AddParameter(progInfo.MainSpec, "I_GC_XQ", "igcTia_XQ");
            if (progInfo.TestParamsConfig.GetBoolParam("doMCControl"))
            {
                modRun.Limits.AddParameter(progInfo.MainSpec, "I_MC_X", "imcTia_X");
                modRun.Limits.AddParameter(progInfo.MainSpec, "I_MC_Y", "imcTia_Y");
            }
            modRun.Limits.AddParameter(progInfo.MainSpec, "TIME_SENSITIVITY_LIA_X", "TIME_SENSITIVITY_LIA_X");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TIME_SENSITIVITY_LIA_Y", "TIME_SENSITIVITY_LIA_Y");
            modRun.Limits.AddParameter(progInfo.MainSpec, "VOLTAGE_SENSITIVITY_LIA_X", "VOLTAGE_SENSITIVITY_LIA_X");
            modRun.Limits.AddParameter(progInfo.MainSpec, "VOLTAGE_SENSITIVITY_LIA_Y", "VOLTAGE_SENSITIVITY_LIA_Y");

        }


        private void Pincheck_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_Pincheck", "Mod_Pincheck", "");

            // Add instruments
            modRun.ConfigData.AddReference("Tia_X", progInfo.Instrs.Tia_X);
            modRun.ConfigData.AddReference("Tia_Y", progInfo.Instrs.Tia_Y);
            modRun.ConfigData.AddReference("PdBias", progInfo.Instrs.PdSource);
            //modRun.ConfigData.AddReference("AuxPd", progInfo.Instrs.AuxPd);

            modRun.ConfigData.AddReference("WaveformGenerator", progInfo.Instrs.WaveformGenerator);
            modRun.ConfigData.AddReference("LockInAmplifier", progInfo.Instrs.LockInAmplifier);
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);

            // Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));

            modRun.ConfigData.AddDouble("vmcOfTiaX_V", progInfo.TestConditions.VmcTiaX_V);
            modRun.ConfigData.AddDouble("vmcOfTiaY_V", progInfo.TestConditions.VmcTiaY_V);

            modRun.ConfigData.AddDouble("vccOfTia_V", progInfo.TestConditions.VccTia_V);

            modRun.ConfigData.AddDouble("vgcOfTiaX_V", progInfo.TestConditions.VgcTiaXI_V);
            //       modRun.ConfigData.AddDouble("vgcOfTiaXQ_V", progInfo.TestConditions.VgcTiaXQ_V);
            //        modRun.ConfigData.AddDouble("vgcOfTiaYI_V", progInfo.TestConditions.VgcTiaYI_V);
            modRun.ConfigData.AddDouble("vgcOfTiaY_V", progInfo.TestConditions.VgcTiaYI_V);

            modRun.ConfigData.AddDouble("pdBais_V", progInfo.TestConditions.PdReverseBias_V);
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);
            modRun.ConfigData.AddDouble("targetInputPower_Sig_dBm", progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);
            modRun.ConfigData.AddDouble("targetInputPower_Loc_dBm", progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm);
            //modRun.ConfigData.AddDouble("waveLength_nm", progInfo.TestConditions.LaserWavelength_nm);
            modRun.ConfigData.AddDouble("BandwidthSetting", progInfo.TestConditions.BandwidthSetting);
            modRun.ConfigData.AddDouble("pdComplanceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("pdComplanceCurrent_mA"));
            modRun.ConfigData.AddDouble("vccComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vccComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("vmcComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vmcComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("vgcComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("vgcComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("bandWidthTiaComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("bandWidthTiaComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("waveformFreq_Khz", progInfo.TestParamsConfig.GetDoubleParam("waveformFreq_Khz"));
            modRun.ConfigData.AddDouble("waveformAmplitude_mV", progInfo.TestParamsConfig.GetDoubleParam("waveformAmplitude_mV"));
            modRun.ConfigData.AddDouble("waveformDcOffset_V", progInfo.TestParamsConfig.GetDoubleParam("waveformDcOffset_V"));
            modRun.ConfigData.AddBool("doMCControl", progInfo.TestParamsConfig.GetBoolParam("doMCControl"));
            modRun.ConfigData.AddBool("doBWControl", progInfo.TestParamsConfig.GetBoolParam("doBWControl"));

  
            if (progInfo.TestParamsConfig.GetBoolParam("doMCControl"))
            {
                modRun.Limits.AddParameter(progInfo.MainSpec, "I_MC_X", "imcTia_X");
                modRun.Limits.AddParameter(progInfo.MainSpec, "I_MC_Y", "imcTia_Y");
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        /// <param name="rfOutput"></param>
        private void MeasureSetupOpticalPolarisation_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject,
            HybirdChipEnum hybirdChip)
        {
            // Initialise module Polarisation controller
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureSetupOpticalPolarisation" + hybirdChip.ToString(), "Mod_MeasureSetupOpticalPolarisation", "");

            // Add instruments
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);
            //need this jd  modRun.ConfigData.AddReference("polsig", progInfo.Instrs.pol_sig);
            //need this jd  modRun.ConfigData.AddReference("polloc", progInfo.Instrs.pol_loc);
            modRun.ConfigData.AddReference("Tia_X", progInfo.Instrs.Tia_X);
            modRun.ConfigData.AddReference("Tia_Y", progInfo.Instrs.Tia_Y);
            // Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));

            modRun.ConfigData.AddDouble("powerSetSOPC_Sig_dB", progInfo.TestParamsConfig.GetDoubleParam("powerSetSOPC_Sig_dB"));
            modRun.ConfigData.AddDouble("powerSetSOPC_Loc_dB", progInfo.TestParamsConfig.GetDoubleParam("powerSetSOPC_Loc_dB"));

            modRun.ConfigData.AddDouble("sopcSweeStartVoltage_V", progInfo.TestParamsConfig.GetDoubleParam("sopcSweeStartVoltage_V"));
            modRun.ConfigData.AddDouble("sopcSweeStopVoltage_V", progInfo.TestParamsConfig.GetDoubleParam("sopcSweeStopVoltage_V"));
            modRun.ConfigData.AddDouble("sopcSweeStepVoltage_V", progInfo.TestParamsConfig.GetDoubleParam("sopcSweeStepVoltage_V"));
            modRun.ConfigData.AddDouble("sopcSweeDelayTime_ms", progInfo.TestParamsConfig.GetDoubleParam("sopcSweeDelayTime_ms"));
            modRun.ConfigData.AddDouble("sopcComplianceCurrent_mA", progInfo.TestParamsConfig.GetDoubleParam("sopcComplianceCurrent_mA"));
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);
            modRun.ConfigData.AddDouble("zeroDBRefPowerSig_dBm", progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);
            modRun.ConfigData.AddDouble("zeroDBRefPowerLoc_dBm", progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm);
            modRun.ConfigData.AddListDatum("fcuNoiseCurrentList", fcuNoiseCurrentList);
            modRun.ConfigData.AddString("dutSerialNum", dutObject.SerialNumber);

            // Tie up limits
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_SIG_CH_1", "polCtrlVoltSigChan1");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_SIG_CH_2", "polCtrlVoltSigChan2");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_SIG_CH_3", "polCtrlVoltSigChan3");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_SIG_CH_4", "polCtrlVoltSigChan4");

            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_LOC_CH_1", "polCtrlVoltLocChan1");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_LOC_CH_2", "polCtrlVoltLocChan2");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_LOC_CH_3", "polCtrlVoltLocChan3");
            //modRun.Limits.AddParameter(progInfo.MainSpec, "POL_VOL_" + hybirdChip.ToString() + "_LOC_CH_4", "polCtrlVoltLocChan4");

            modRun.Limits.AddParameter(progInfo.MainSpec, "POL_SUM_IP_" + hybirdChip.ToString() + "_SIG", "pdCurrentSum_Sig");
            modRun.Limits.AddParameter(progInfo.MainSpec, "POL_SUM_IP_" + hybirdChip.ToString() + "_LOC", "pdCurrentSum_Loc");

            modRun.Limits.AddParameter(progInfo.MainSpec, "POL_" + hybirdChip.ToString() + "_SIG_FILE", "polSigSweepFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "POL_" + hybirdChip.ToString() + "_LOC_FILE", "polLocSweepFile");
        }

        /// <summary>
        /// measure setup optical path delay line initialise module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        /// <param name="hybirdChip"></param>
        /// <param name="wavelengthNumber"></param>
        private void MeasureSetupOpticalPathDelay_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject,
            HybirdChipEnum hybirdChip, int powerNumber)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureSetupOpticalPathDelay" + hybirdChip.ToString() + "_P" + powerNumber.ToString(),
                "Mod_MeasureSetupOpticalPathDelay", "");

            // Add instruments
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);

            // Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));

            modRun.ConfigData.AddDouble("OsciStartFreq_GHz", progInfo.TestParamsConfig.GetDoubleParam("OsciStartFreq_GHz"));
            modRun.ConfigData.AddDouble("OsciStopFreq_GHz", progInfo.TestParamsConfig.GetDoubleParam("OsciStopFreq_GHz"));
            modRun.ConfigData.AddDouble("s21bandWidthPoint_dB", progInfo.TestParamsConfig.GetDoubleParam("s21bandWidthPoint_dB"));
            modRun.ConfigData.AddDouble("s21smoothingPoints", progInfo.TestParamsConfig.GetDoubleParam("s21smoothingPoints"));
            modRun.ConfigData.AddDouble("s21NormalisationFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21NormalisationFrequency_GHz"));
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            modRun.ConfigData.AddListDatum("fcuNoiseCurrentList", fcuNoiseCurrentList);
            modRun.ConfigData.AddString("serialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("zeroDBRefPowerSig_dBm", progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);
            modRun.ConfigData.AddDouble("zeroDBRefPowerLoc_dBm", progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm);
            modRun.ConfigData.AddDouble("maxDelayLineTime", progInfo.TestParamsConfig.GetDoubleParam("maxDelayLineTime"));
            modRun.ConfigData.AddDouble("minDelayLineTime", progInfo.TestParamsConfig.GetDoubleParam("minDelayLineTime"));
            modRun.ConfigData.AddDouble("defaultDelayTimeSig_ps", progInfo.TestParamsConfig.GetDoubleParam("defaultDelayTimeSig_ps"));
            modRun.ConfigData.AddDouble("defaultDelayTimeLoc_ps", progInfo.TestParamsConfig.GetDoubleParam("defaultDelayTimeLoc_ps"));

            string calRegister;
            if (hybirdChip.ToString().Contains("X"))
            {
                calRegister = "calXS21ChipRegister";
            }
            else
            {
                calRegister = "calYS21ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));



            modRun.ConfigData.AddDouble("pdBais_V", progInfo.TestConditions.PdReverseBias_V);
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);
            modRun.ConfigData.AddDouble("targetInputPower_Sig_dBm", progInfo.TestConditions.zeroDBRefPower_2_Sig_dBm);
            modRun.ConfigData.AddDouble("targetInputPower_Loc_dBm", progInfo.TestConditions.zeroDBRefPower_2_Loc_dBm);

            // Tie up limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "DELAY_LINE_" + hybirdChip.ToString() + "_FILE", "delayLineAdjustFile");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DELAY_LINE_" + hybirdChip.ToString() + "_SIG", "delaySettingSig_ps");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DELAY_LINE_" + hybirdChip.ToString() + "_LOC", "delaySettingLoc_ps");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        /// <param name="tiaOutputPort"></param>
        private void MeasureS22_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject, RfOutputEnum rfOutput)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureS22_" + rfOutput.ToString(), "Mod_MeasureS22", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);

            // Add config data
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            string calRegister;
            if (rfOutput.ToString().Contains("X"))
            {
                calRegister = "calXS22ChipRegister";
            }
            else
            {
                calRegister = "calYS22ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));

            modRun.ConfigData.AddDouble("s22smoothingPoints", progInfo.TestParamsConfig.GetDoubleParam("s22smoothingPoints"));
            modRun.ConfigData.AddDouble("s22normalisationFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s22normalisationFrequency_GHz"));

            string maskFitFile = progInfo.TestParamsConfig.GetStringParam("s22MaskFitFile");
            maskFitFile = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\" + maskFitFile;
            modRun.ConfigData.AddString("s22MaskFitFile", maskFitFile);
            modRun.ConfigData.AddDouble("specialPowerOfS22_dB", progInfo.TestConditions.S22Setting_dB);
            modRun.ConfigData.AddBool("doS22MaskTest", progInfo.TestParamsConfig.GetBoolParam("doMaskS22"));
            // Tie up limits

            //modRun.Limits.AddParameter(progInfo.MainSpec, "S22_" + rfOutput.ToString() + "_MASK_CHECK", "bandWidth_S22_GHz");
        }

        /// <summary>
        /// MeasureS21 module initialise
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        /// <param name="tiaOutputPort">enum TiaOutputPort</param>
        private void MeasureS21_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject,
            RfOutputEnum rfOutput,int refPowerNumber)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureS21_" + rfOutput.ToString() + "_P" + refPowerNumber.ToString(), "Mod_MeasureS21", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);

            // Add config data
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            modRun.ConfigData.AddBool("doMask", progInfo.TestParamsConfig.GetBoolParam("doMaskS21"));

            string maskFitFile = progInfo.TestParamsConfig.GetStringParam("s21MaskFitFile");
            maskFitFile = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\" + maskFitFile;
            modRun.ConfigData.AddString("maskFitFile", maskFitFile);

            modRun.ConfigData.AddSint32("s21smoothingPoints", progInfo.TestParamsConfig.GetIntParam("s21smoothingPoints"));
            modRun.ConfigData.AddDouble("s21normalisationFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21normalisationFrequency_GHz"));
            modRun.ConfigData.AddString("s21MaskFitFile", progInfo.TestParamsConfig.GetStringParam("s21MaskFitFile"));
            modRun.ConfigData.AddDouble("s21bandWidthPoint_dB", progInfo.TestParamsConfig.GetDoubleParam("s21bandWidthPoint_dB"));
            string calRegister;
            if (rfOutput.ToString().Contains("X"))
            {
                calRegister = "calXS21ChipRegister";
            }
            else
            {
                calRegister = "calYS21ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));

            // Tie up limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_" + rfOutput.ToString() + "_P" + refPowerNumber.ToString(), "BandWidth_S21_GHz");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        /// <param name="tiaOutputPort"></param>
        private void MeasurementOE_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject, RfOutputEnum rfOutput)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureEO" + rfOutput.ToString(), "Mod_MeasureEO", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);

            // Add config data
            modRun.ConfigData.AddString("rfOutputPort", rfOutput.ToString());
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);
            modRun.ConfigData.AddListDatum("fcuNoiseCurrentList", fcuNoiseCurrentList);
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));

            string calRegister;
            if (rfOutput.ToString().Contains("X"))
            {
                calRegister = "calXS21ChipRegister";
            }
            else
            {
                calRegister = "calYS21ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));


            // Tie up limits
            //no nedd limits,this module just get s21 sweep data
        }

        /// <summary>
        /// MeasureS21 module initialise
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        /// <param name="tiaOutputPort">enum TiaOutputPort</param>
        private void MeasureDLP_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject, RfOutputEnum rfOutput)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureDLP" + rfOutput.ToString(), "Mod_MeasureDLP", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);

            // Add config data
            //modRun.ConfigData.AddBool("qualTest", true);

            //modRun.ConfigData.AddDouble("s21StartFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21StartFrequency_GHz"));
            //modRun.ConfigData.AddDouble("s21StopFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21StopFrequency_GHz"));
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            //modRun.ConfigData.AddDouble("s21smoothingAperturePercent", progInfo.TestParamsConfig.GetDoubleParam("s21smoothingAperturePercent"));
            //modRun.ConfigData.AddDouble("s21referencePosition", progInfo.TestParamsConfig.GetDoubleParam("s21referencePosition"));
            //modRun.ConfigData.AddDouble("s21IFBandWidth", progInfo.TestParamsConfig.GetDoubleParam("s21IFBandWidth"));
            //modRun.ConfigData.AddDouble("s21ActiveChannelAveragingFactor", progInfo.TestParamsConfig.GetDoubleParam("s21ActiveChannelAveragingFactor"));
            //modRun.ConfigData.AddDouble("s21Scale", progInfo.TestParamsConfig.GetDoubleParam("s21Scale"));
            modRun.ConfigData.AddSint32("s21smoothingPoints", progInfo.TestParamsConfig.GetIntParam("s21smoothingPoints"));
            modRun.ConfigData.AddSint32("dlpSmoothPoints", progInfo.TestParamsConfig.GetIntParam("dlpSmoothPoints"));
            modRun.ConfigData.AddDouble("phaseAnalysisStopFreq_GHz",progInfo.TestParamsConfig.GetDoubleParam("phaseAnalysisStopFreq_GHz"));

            string calRegister;
            if (rfOutput.ToString().Contains("X"))
            {
                calRegister = "calXS21ChipRegister";
            }
            else
            {
                calRegister = "calYS21ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));

            modRun.ConfigData.AddDouble("targetDLP", progInfo.TestConditions.DlpSetting_deg);

            // Tie up limits
           // modRun.Limits.AddParameter(progInfo.MainSpec, "DLP_" + rfOutput.ToString(), "targetDLPFreq_GHz");
        }

        /// <summary>
        /// MeasureGroupDelay module initialise
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        /// <param name="tiaOutputPort">enum TiaOutputPort</param>
        private void MeasureGroupDelay_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject, 
            RfOutputEnum rfOutput)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_MeasureGroupDelay" + rfOutput.ToString(), "Mod_MeasureGroupDelay", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);

            // Add config data
            //modRun.ConfigData.AddBool("qualTest", true);

            //modRun.ConfigData.AddDouble("s21StartFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21StartFrequency_GHz"));
            //modRun.ConfigData.AddDouble("s21StopFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21StopFrequency_GHz"));
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            //modRun.ConfigData.AddDouble("s21smoothingAperturePercent", progInfo.TestParamsConfig.GetDoubleParam("s21smoothingAperturePercent"));
            //modRun.ConfigData.AddDouble("s21referencePosition", progInfo.TestParamsConfig.GetDoubleParam("s21referencePosition"));
           // modRun.ConfigData.AddDouble("s21IFBandWidth", progInfo.TestParamsConfig.GetDoubleParam("s21IFBandWidth"));
            //modRun.ConfigData.AddDouble("s21ActiveChannelAveragingFactor", progInfo.TestParamsConfig.GetDoubleParam("s21ActiveChannelAveragingFactor"));
            //modRun.ConfigData.AddDouble("s21Scale", progInfo.TestParamsConfig.GetDoubleParam("s21Scale"));
            modRun.ConfigData.AddSint32("s21smoothingPoints", progInfo.TestParamsConfig.GetIntParam("s21smoothingPoints"));
            modRun.ConfigData.AddDouble("phaseAnalysisStopFreq_GHz", progInfo.TestParamsConfig.GetDoubleParam("phaseAnalysisStopFreq_GHz"));

            string calRegister;
            if (rfOutput.ToString().Contains("X"))
            {
                calRegister = "calXS21ChipRegister";
            }
            else
            {
                calRegister = "calYS21ChipRegister";
            }
            modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam(calRegister));

            modRun.ConfigData.AddDouble("targetGroupDelayFreq_GHz", progInfo.TestConditions.GroupDelaySetting_GHz);
            modRun.ConfigData.AddSint32("fitstSmoothingPoints", progInfo.TestParamsConfig.GetIntParam("gdFitstSmoothPoints"));
            modRun.ConfigData.AddSint32("secondSmoothingPoints", progInfo.TestParamsConfig.GetIntParam("gdSecondSmoothPoints"));
            modRun.ConfigData.AddSint32("thirdSmoothingPoints", progInfo.TestParamsConfig.GetIntParam("gdThirdSmoothPoints"));

            // Tie up limits
            //modRun.Limits.AddParameter(progInfo.MainSpec, "GD_" + rfOutput.ToString(), "groupDelay_ps");
        }

        /// <summary>
        /// MeasureGroupDelay_InitModule
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        /// <param name="tiaOutputPort">eunm TiaOutputPort</param>
        private void MeasureSPRR_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject,
            TiaEnum tia,int refPowerNum)
        {
            // Initialise module
            ModuleRun modRun;

            modRun = engine.AddModuleRun("Mod_MeasureSPRR_" + tia.ToString() + "_P" + refPowerNum.ToString(), "Mod_MeasureSPRR", "");

            // Add instruments
            modRun.ConfigData.AddReference("LightwaveComponentAnalyzer", progInfo.Instrs.LightwaveComponentAnalyzer);

            // Add config data
            //modRun.ConfigData.AddBool("qualTest", true);

            modRun.ConfigData.AddString("tiaOutputPort", tia.ToString());
            modRun.ConfigData.AddString("DutSerialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddString("testStage", dutObject.TestStage);
            modRun.ConfigData.AddDouble("sprrTestSeting_GHz", progInfo.TestConditions.SprrSetting_GHz);
            modRun.ConfigData.AddDouble("normalisationFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21normalisationFrequency_GHz"));

            modRun.ConfigData.AddBool("doMaskSPRR", progInfo.TestParamsConfig.GetBoolParam("doMaskSPRR"));
            string maskFitFile = progInfo.TestParamsConfig.GetStringParam("sprrMaskFitFile");
            maskFitFile = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\" + maskFitFile;
            modRun.ConfigData.AddString("sprrMaskFitFile", maskFitFile);

            // Tie up limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "SPRR_" + tia.ToString() + "_SIG_P" + refPowerNum.ToString(), "sprr_Sig_GHz");
            modRun.Limits.AddParameter(progInfo.MainSpec, "SPRR_" + tia.ToString() + "_LOC_P" + refPowerNum.ToString(), "sprr_Loc_GHz");
        }

        /// <summary>
        /// MeasureGroupDelay_InitModule
        /// </summary>
        /// <param name="engine">ITestEngineInit</param>
        /// <param name="instrs">InstrumentCollection</param>
        /// <param name="dutObject">DUTObject</param>
        /// <param name="tiaOutputPort">eunm TiaOutputPort</param>
        private void MeasureGroupDelayDivation_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;

            modRun = engine.AddModuleRun("Mod_MeasureGroupDelayDeviation", "Mod_MeasureGroupDelayDeviation", "");

            // Add instruments

            // Add config data
            modRun.ConfigData.AddDouble("TargetPointSkewGDFreq_GHz", progInfo.TestConditions.GroupDelaySetting_GHz);
            modRun.ConfigData.AddString("dutSerialNumber", dutObject.SerialNumber);

            // Tie up limits
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XIP", "DEV_GD_XIP");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XIN", "DEV_GD_XIN");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XQP", "DEV_GD_XQP");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XQN", "DEV_GD_XQN");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YIP", "DEV_GD_YIP");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YIN", "DEV_GD_YIN");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YQP", "DEV_GD_YQP");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YQN", "DEV_GD_YQN");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XI", "DEV_GD_XI");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_XQ", "DEV_GD_XQ");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YI", "DEV_GD_YI");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_YQ", "DEV_GD_YQ");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_X", "DEV_GD_X");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DEV_GD_Y", "DEV_GD_Y");
            modRun.Limits.AddParameter(progInfo.MainSpec, "DLP_GD_FILE", "DLP_GD_FILE");
        }


        private void Mod_TiaVgcAndBandwidthSweep_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject,
            HybirdChipEnum hybirdChip)
        {
            // Initialise module
            ModuleRun modRun;
            modRun = engine.AddModuleRun("Mod_TiaVgcAndBandwidthSweep" + hybirdChip.ToString(), "Mod_TiaVgcAndBandwidthSweep", "");

            // Add instruments
            modRun.ConfigData.AddReference("sigMeasureSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locMeasureSetupManage", locChainSetupManage);

            // Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            if (hybirdChip == HybirdChipEnum.X)
            {
                modRun.ConfigData.AddReference("Tia_I", progInfo.Instrs.Tia_X);
                modRun.ConfigData.AddReference("Tia_Q", progInfo.Instrs.Tia_X);
                modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam("calXS21ChipRegister"));
                modRun.ConfigData.AddDouble("vmcOfTia_V", progInfo.TestConditions.VmcTiaX_V);
                modRun.ConfigData.AddDouble("vgcOfTia_V", progInfo.TestConditions.VgcTiaXI_V);
                modRun.ConfigData.AddString("vgcSweepRfPort", progInfo.TestParamsConfig.GetStringParam("VgcSweepRfPort1"));
                modRun.ConfigData.AddString("bandwidthSweepRfPort", progInfo.TestParamsConfig.GetStringParam("bwSweepRfPort1"));
            }
            else
            {
                modRun.ConfigData.AddReference("Tia_I", progInfo.Instrs.Tia_Y);
                modRun.ConfigData.AddReference("Tia_Q", progInfo.Instrs.Tia_Y);
                modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
                modRun.ConfigData.AddSint32("CalRegister", progInfo.TestParamsConfig.GetIntParam("calYS21ChipRegister"));
                modRun.ConfigData.AddDouble("vmcOfTia_V", progInfo.TestConditions.VmcTiaY_V);
                modRun.ConfigData.AddDouble("vgcOfTia_V", progInfo.TestConditions.VgcTiaYI_V);
                modRun.ConfigData.AddString("vgcSweepRfPort", progInfo.TestParamsConfig.GetStringParam("VgcSweepRfPort2"));
                modRun.ConfigData.AddString("bandwidthSweepRfPort", progInfo.TestParamsConfig.GetStringParam("bwSweepRfPort2"));
            }

            modRun.ConfigData.AddDouble("vccOfTia_V", progInfo.TestConditions.VccTia_V);
            modRun.ConfigData.AddString("DutSerialNum", dutObject.SerialNumber);

            modRun.ConfigData.AddDouble("OsciStartFreq_GHz", progInfo.TestParamsConfig.GetDoubleParam("OsciStartFreq_GHz"));
            modRun.ConfigData.AddDouble("OsciStopFreq_GHz", progInfo.TestParamsConfig.GetDoubleParam("OsciStopFreq_GHz"));
            modRun.ConfigData.AddDouble("s21bandWidthPoint_dB", progInfo.TestParamsConfig.GetDoubleParam("s21bandWidthPoint_dB"));
            modRun.ConfigData.AddDouble("s21smoothingPoints", progInfo.TestParamsConfig.GetDoubleParam("s21smoothingPoints"));
            modRun.ConfigData.AddDouble("s21NormalisationFrequency_GHz", progInfo.TestParamsConfig.GetDoubleParam("s21NormalisationFrequency_GHz"));
            modRun.ConfigData.AddSint32("sweepPoints", progInfo.TestParamsConfig.GetIntParam("sweepPoints"));
            modRun.ConfigData.AddDouble("vgcSweepVoltage1", progInfo.TestConditions.VgcSweepVolt1_V);
            modRun.ConfigData.AddDouble("vgcSweepVoltage2", progInfo.TestConditions.VgcSweepVolt2_V);
            modRun.ConfigData.AddDouble("vgcSweepVoltage3", progInfo.TestConditions.VgcSweepVolt3_V);
            modRun.ConfigData.AddDouble("vgcSweepVoltage4", progInfo.TestConditions.VgcSweepVolt4_V);
            modRun.ConfigData.AddDouble("vgcSweepVoltage5", progInfo.TestConditions.VgcSweepVolt5_V);
            modRun.ConfigData.AddSint32("bwSettingM4", progInfo.TestConditions.bwSettingM4);
            modRun.ConfigData.AddSint32("bwSettingP4", progInfo.TestConditions.bwSwttingP4);
            modRun.ConfigData.AddListDatum("fcuNoiseCurrentList", fcuNoiseCurrentList);
            modRun.ConfigData.AddBool("doMCControl", progInfo.TestParamsConfig.GetBoolParam("doMCControl"));
            modRun.ConfigData.AddBool("doBWControl", progInfo.TestParamsConfig.GetBoolParam("doBWControl"));

            modRun.ConfigData.AddString("serialNumber", dutObject.SerialNumber);
            modRun.ConfigData.AddDouble("powerTolerance_dB", progInfo.TestConditions.InputPowerTolerance_dB);

            // Tie up limits
            string tempStr;
            if (hybirdChip == HybirdChipEnum.X)
            {
                tempStr = "1";
            }
            else
            {
                tempStr = "2";
            }
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_VGC_1", "S21_CH_" + tempStr + "_VGC_1");
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_VGC_2", "S21_CH_" + tempStr + "_VGC_2");
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_VGC_3", "S21_CH_" + tempStr + "_VGC_3");
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_VGC_4", "S21_CH_" + tempStr + "_VGC_4");
            modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_VGC_5", "S21_CH_" + tempStr + "_VGC_5");
            modRun.Limits.AddParameter(progInfo.MainSpec, "LANE_" + tempStr.ToString() + "_P2_VGC_FILE", "CH_" + tempStr + "_VGC_FILE");
            modRun.Limits.AddParameter(progInfo.MainSpec, "PLOT_LANE_" + tempStr.ToString() + "_P2_VGC", "PLOT_CH_" + tempStr + "_VGC");
            modRun.Limits.AddParameter(progInfo.MainSpec, "TEST_VGC_LANE_" + tempStr.ToString(), "TEST_VGC_CH_" + tempStr);

            if (progInfo.TestParamsConfig.GetBoolParam("doBWControl"))
            {
                //modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_BW_M4", "S21_CH_" + tempStr + "_BW_M4");
                //modRun.Limits.AddParameter(progInfo.MainSpec, "S21_LANE_" + tempStr.ToString() + "_P2_BW_P4", "S21_CH_" + tempStr + "_BW_P4");
                //modRun.Limits.AddParameter(progInfo.MainSpec, "LANE_" + tempStr.ToString() + "_P2_BW_FILE", "CH_" + tempStr + "_BW_FILE");
                //modRun.Limits.AddParameter(progInfo.MainSpec, "PLOT_LANE_" + tempStr.ToString() + "_P2_BW", "PLOT_CH_" + tempStr + "_BW");
                //modRun.Limits.AddParameter(progInfo.MainSpec, "TEST_BW_LANE_" + tempStr.ToString(), "TEST_BW_CH_" + tempStr);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <param name="dutObject"></param>
        private void Mod_CheckSpliterRatio_InitModule(ITestEngineInit engine, InstrumentCollection instrs, DUTObject dutObject)
        {
            // Initialise module
            ModuleRun modRun;


            modRun = engine.AddModuleRun("Mod_CheckSplitterRatio", "Mod_CheckSplitterRatio", "");



            // Add instruments
            modRun.ConfigData.AddReference("OPMMon_Sig", progInfo.Instrs.OPMMon_Sig);
            modRun.ConfigData.AddReference("OPMRef_Sig", progInfo.Instrs.OPMRef_Loc);
            modRun.ConfigData.AddReference("OPMMon_Loc", progInfo.Instrs.OPMMon_Loc);
            modRun.ConfigData.AddReference("OPMRef_Loc", progInfo.Instrs.OPMRef_Loc);
            modRun.ConfigData.AddReference("sigChainSetupManage", sigChainSetupManage);
            modRun.ConfigData.AddReference("locChainSetupManage", locChainSetupManage);

            //// Add config data
            double waveLength_nm = progInfo.TestConditions.LaserWavelength_nm;

            //modRun.ConfigData.AddDouble("splitRate_Sig", sigChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            //modRun.ConfigData.AddDouble("splitRate_Loc", locChainSetupManage.SplitterRatiosCalData.GetSrByWavelen(waveLength_nm));
            //modRun.ConfigData.AddDouble("powerRatioTolerance_dB", progInfo.TestParamsConfig.GetDoubleParam("powerRatioTolerance_dB"));
        }

        #endregion

        #region Private Helper Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="progInstrs"></param>
        private void MeasureFcuNoiseCurrent(ITestEngineInit engine, Prog_Product_GroupA_CoherentRxInstruments progInstrs)
        {


            double currentXIP_mA = 0.0;
            double currentXIN_mA = 0.0;
            double currentXQP_mA = 0.0;
            double currentXQN_mA = 0.0;
            double currentYIP_mA = 0.0;
            double currentYIN_mA = 0.0;
            double currentYQP_mA = 0.0;
            double currentYQN_mA = 0.0;

            SetPdBias(engine, 5, 2.0);

            System.Threading.Thread.Sleep(1000);

            //measure Xchip pd dark current 10 times
            if (progInstrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                UsedFrontOrBackTerminals(useFrontTerminals);
            }
            Thread.Sleep(1000);
            for (int i = 0; i < 10; i++)
            {
                currentXIP_mA += progInstrs.PdSource.PdSource_XIpos.CurrentActual_amp * 1000;
                currentXIN_mA += progInstrs.PdSource.PdSource_XIneg.CurrentActual_amp * 1000;
                currentXQP_mA += progInstrs.PdSource.PdSource_XQpos.CurrentActual_amp * 1000;
                currentXQN_mA += progInstrs.PdSource.PdSource_XQneg.CurrentActual_amp * 1000;
            }
           
            //measure Ychip pd dark current 10 times
            if (progInstrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                bool useFrontTerminals = progInfo.TestParamsConfig.GetBoolParam("2400FrontTerminalForXchip");
                UsedFrontOrBackTerminals(!useFrontTerminals);
            }
            Thread.Sleep(1000);
            for (int i = 0; i < 10; i++)
            {
                currentYIP_mA += progInstrs.PdSource.PdSource_YIpos.CurrentActual_amp * 1000;
                currentYIN_mA += progInstrs.PdSource.PdSource_YIneg.CurrentActual_amp * 1000;
                currentYQP_mA += progInstrs.PdSource.PdSource_YQpos.CurrentActual_amp * 1000;
                currentYQN_mA += progInstrs.PdSource.PdSource_YQneg.CurrentActual_amp * 1000;
            }

            currentXIP_mA /= 10;
            currentXIN_mA /= 10;
            currentXQP_mA /= 10;
            currentXQN_mA /= 10;
            currentYIP_mA /= 10;
            currentYIN_mA /= 10;
            currentYQP_mA /= 10;
            currentYQN_mA /= 10;

            fcuNoiseCurrentList.AddDouble("XIP", currentXIP_mA);
            fcuNoiseCurrentList.AddDouble("XIN", currentXIN_mA);
            fcuNoiseCurrentList.AddDouble("XQP", currentXQP_mA);
            fcuNoiseCurrentList.AddDouble("XQN", currentXQN_mA);
            fcuNoiseCurrentList.AddDouble("YIP", currentYIP_mA);
            fcuNoiseCurrentList.AddDouble("YIN", currentYIN_mA);
            fcuNoiseCurrentList.AddDouble("YQP", currentYQP_mA);
            fcuNoiseCurrentList.AddDouble("YQN", currentYQN_mA);

            SetPdBiasOFF();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetPdBiasOFF()
        {
            progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = false;
            progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tia"></param>
        private void SetTiaCtrlVoltageOff(TiaInstrument Tia)
        {
            if (Tia.BandWidth_H != null)
            {
                Tia.BandWidth_H.OutputEnabled = false;
                Tia.BandWidth_L.OutputEnabled = false;
            }

            if (Tia.Vgc != null)
            {
                Tia.Vgc.OutputEnabled = false ;
            }
        }

        /// <summary>
 /// <summary>
        /// 
        /// </summary>
        /// <param name="useFrontTerminals"></param>
        private void UsedFrontOrBackTerminals(bool useFrontTerminals)
        {
            Inst_Ke24xx pdBias;
            
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XIpos;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XIneg;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XQpos;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;
            pdBias = (Inst_Ke24xx)progInfo.Instrs.PdSource.PdSource_XQneg;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;

            // Need to change vgc keithley

            pdBias = (Inst_Ke24xx)progInfo.Instrs.Tia_X.Vgc;
            pdBias.UseFrontTerminals = useFrontTerminals;
            pdBias.OutputEnabled = true;

            // need to check if BW outputs are on or not
            pdBias = (Inst_Ke24xx)progInfo.Instrs.Tia_X.BandWidth_H;
            if (pdBias.OutputEnabled)
            {
                pdBias.UseFrontTerminals = useFrontTerminals;
                pdBias.OutputEnabled = true;
            }
            else
            {
                pdBias.UseFrontTerminals = useFrontTerminals;
            }

            pdBias = (Inst_Ke24xx)progInfo.Instrs.Tia_X.BandWidth_L;
            if (pdBias.OutputEnabled)
            {
                pdBias.UseFrontTerminals = useFrontTerminals;
                pdBias.OutputEnabled = true;
            }
            else
            {
                pdBias.UseFrontTerminals = useFrontTerminals;
            }



        }

        /// <summary>
        /// set pd bias volt
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="voltSet_V">pd bias volt</param>
        /// <param name="currentCompliance_mA">pd bias compliance current</param>
        private void SetPdBias(ITestEngineInit engine, double voltSet_V, double currentCompliance_mA)
        {

            progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = true;
            progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = true;

            progInfo.Instrs.PdSource.PdSource_XIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_XQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            progInfo.Instrs.PdSource.PdSource_YQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;

            if (!progInfo.Instrs.PdSource.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                int counts = (int)(voltSet_V / 0.2);
                for (int i = 1; i <= counts; i++)
                {
                    double voltageSet_V = 0.2 * i;
                    progInfo.Instrs.PdSource.PdSource_XIpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XIneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XQpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_XQneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YIpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YIneg.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YQpos.VoltageSetPoint_Volt = voltageSet_V;
                    progInfo.Instrs.PdSource.PdSource_YQneg.VoltageSetPoint_Volt = voltageSet_V;

                    if (i == 1)
                    {
                        List<double> voltSensList_V = new List<double>();
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XIpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XIneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XQpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_XQneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YIpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YIneg.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YQpos.VoltageActual_Volt);
                        voltSensList_V.Add(progInfo.Instrs.PdSource.PdSource_YQneg.VoltageActual_Volt);
                        foreach (double volt in voltSensList_V)
                        {
                            if (Math.Abs(volt - voltageSet_V) > 0.1)
                            {
                                progInfo.Instrs.PdSource.PdSource_XIpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XIneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XQpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_XQneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YIpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YIneg.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YQpos.OutputEnabled = false;
                                progInfo.Instrs.PdSource.PdSource_YQneg.OutputEnabled = false;

                                engine.ErrorInProgram("pd bias error,please check FCU2 pd bias whether demang");
                            }
                        }
                    }

                    Thread.Sleep(50);
                }
            }
            progInfo.Instrs.PdSource.PdSource_XIpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XIneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XQpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_XQneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YIpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YIneg.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YQpos.VoltageSetPoint_Volt = voltSet_V;
            progInfo.Instrs.PdSource.PdSource_YQneg.VoltageSetPoint_Volt = voltSet_V;

        }

        /// <summary>
        /// set fci noise current to trace
        /// </summary>
        private void SetFcuNoiseTraceData()
        {
            DatumList fcuNoiseTraceDataList = new DatumList();
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XIP", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XIN", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XQP", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_XQN", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YIP", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YIN", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YQP", 0);
            fcuNoiseTraceDataList.AddDouble("NF_FCU_YQN", 0);

            //fcuNoiseTraceDataList.AddDouble("NF_FCU_XIP", fcuNoiseCurrentList.ReadDouble("XIP"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_XIN", fcuNoiseCurrentList.ReadDouble("XIN"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_XQP", fcuNoiseCurrentList.ReadDouble("XQP"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_XQN", fcuNoiseCurrentList.ReadDouble("XQN"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_YIP", fcuNoiseCurrentList.ReadDouble("YIP"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_YIN", fcuNoiseCurrentList.ReadDouble("YIN"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_YQP", fcuNoiseCurrentList.ReadDouble("YQP"));
            //fcuNoiseTraceDataList.AddDouble("NF_FCU_YQN", fcuNoiseCurrentList.ReadDouble("YQN"));
            this.progInfo.MainSpec.SetTraceData(fcuNoiseTraceDataList);
        }

        /// <summary>
        /// Calculate s21 deviation
        /// </summary>
        private void CalculateS21Deviation(int refPowerNumber,List<double> listData)
        {
            listData.Sort();
            double s21Deviation_GHz = Math.Abs(listData[0] - listData[listData.Count - 1]);
            DatumList s21DevTraceDataList = new DatumList();
            s21DevTraceDataList.AddDouble("S21_DEV_P" + refPowerNumber.ToString(), s21Deviation_GHz);
            progInfo.MainSpec.SetTraceData(s21DevTraceDataList);
        }

        /// <summary>
        /// Switch rf to NVA
        /// </summary>
        /// <param name="rfPort"></param>
        private void SwitchRfPortToVNA(RfOutputEnum rfPort)
        {
            switch (rfPort)
            {
                case RfOutputEnum.XIP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XIpos);
                    break;
                case RfOutputEnum.XIN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XIneg);
                    break;
                case RfOutputEnum.XQP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XQpos);
                    break;
                case RfOutputEnum.XQN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.XQneg);
                    break;
                case RfOutputEnum.YIP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YIpos);
                    break;
                case RfOutputEnum.YIN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YIneg);
                    break;
                case RfOutputEnum.YQP:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YQpos);
                    break;
                case RfOutputEnum.YQN:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.YQneg);
                    break;
                case RfOutputEnum .OFFALL:
                    progInfo.Instrs.RfSwitch.SetState(Switch_LCA_RF.State.OffConnent);
                    break;
                default:
                    break;
            }

            Thread.Sleep(1000);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="moduleRunReturn"></param>
        /// <param name="rfPortName"></param>
        /// <param name="xyDataFile"></param>
        private void MeasureS22(ITestEngineRun engine, ModuleRunReturn moduleRunReturn, string rfPortName, string xyDataFile,
            string processingSprrFile, string s22PlotFile, ref DatumList s22PassFailStatus)
        {
            ModuleRun modRun = engine.GetModuleRun("Mod_MeasureS22_" + rfPortName);
            modRun.ConfigData.AddOrUpdateString("RfPortName", rfPortName);
            moduleRunReturn = engine.RunModule("Mod_MeasureS22_" + rfPortName, true, true);

            listS22MaskPassFail.Add(moduleRunReturn.ModuleRunData.ModuleData.ReadBool("maskPassFail"));

            if (moduleRunReturn.ModuleRunStatus.Status != MultiSpecPassFail.AllPass)
            {
                s22PassFailStatus.AddBool(rfPortName, false);
            }
            else
            {
                s22PassFailStatus.AddBool(rfPortName, true);
            }

            ArrayList s22PlotData =
                (ArrayList)moduleRunReturn.ModuleRunData.ModuleData.ReadReference("PlotData_S22");
            string plotHead = s22PlotData[0].ToString();
            s22PlotData.RemoveAt(0);
            string[] S22PlotDataArray = (string[])s22PlotData.ToArray(typeof(string));

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(xyDataFile, plotHead, S22PlotDataArray);
            savePlotData.AppendResultToFile(processingSprrFile, plotHead, S22PlotDataArray);

            if (rfPortName == "XIP")
            {
                plotHead = "Freq," + rfPortName;
                savePlotData.AppendResultToFile(s22PlotFile, plotHead, S22PlotDataArray);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[S22PlotDataArray.Length];
                for (int i = 0; i < S22PlotDataArray.Length; i++)
                {
                    tempPlotData[i] = S22PlotDataArray[i].Split(',')[1];
                }
                savePlotData.AppendResultToFile(s22PlotFile, plotHead, tempPlotData);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xyDataFile"></param>
        /// <param name="realAndImaginaryTrace"></param>
        private void SaveRealAndImaginaryData(string xyDataFile, Trace realAndImaginaryTrace,string powerInputPort,double wavelength_nm)
        {
            double[] freqArray = realAndImaginaryTrace.GetXArray();
            double[] realArray = realAndImaginaryTrace.GetYArray();
            double[] ImaginarrayArray = realAndImaginaryTrace.GetZArray();

            string[] PlotDataArray = new string[freqArray.Length];
            //string plotHead = "freq,imaginary,real";
            string plotHead = powerInputPort +"_"+wavelength_nm+ "_imaginary" + "," +
                powerInputPort + "_" + wavelength_nm + "_real";
            for (int i = 0; i < PlotDataArray.Length; i++)
            {
                PlotDataArray[i] = ImaginarrayArray[i].ToString() + "," + realArray[i].ToString();
            }
            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(xyDataFile, plotHead, PlotDataArray);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xyDataFile"></param>
        /// <param name="s21PlotFile"></param>
        /// <param name="rfPortName"></param>
        /// <param name="s21PlotData"></param>
        /// <param name="sweepFreqArray"></param>
        private void SaveS21MagintudeData(string xyDataFile, string s21PlotFile, string rfPortName, ArrayList s21PlotData, double[] sweepFreqArray)
        {
            string plotHead = s21PlotData[0].ToString();
            s21PlotData.RemoveAt(0);
            string[] s21PlotDataArray = (string[])s21PlotData.ToArray(typeof(string));

            Util_SavePlotData savePlotData = new Util_SavePlotData();
           // savePlotData.AppendResultToFile(xyDataFile, plotHead, s21PlotDataArray);

            if (rfPortName == "XIP")
            {
                plotHead = "Freq," + rfPortName;
                List<double> freqArray = new List<double>();
                string[] tempPlotData = new string[s21PlotDataArray.Length];
                for (int i = 0; i < s21PlotDataArray.Length; i++)
                {
                    tempPlotData[i] = s21PlotDataArray[i].Split(',')[1];
                }
                savePlotData.AppendResultToFile(s21PlotFile, "Freq", sweepFreqArray);
                savePlotData.AppendResultToFile(s21PlotFile, rfPortName, tempPlotData);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[s21PlotDataArray.Length];
                for (int i = 0; i < s21PlotDataArray.Length; i++)
                {
                    tempPlotData[i] = s21PlotDataArray[i].Split(',')[1];
                }
                savePlotData.AppendResultToFile(s21PlotFile, plotHead, tempPlotData);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s21PlotFile"></param>
        /// <param name="rfPortName"></param>
        /// <param name="s21PlotData"></param>
        /// <param name="sweepFreqArray"></param>
        private void SaveS21MagintudeData(string s21PlotFile, string rfPortName, ArrayList s21PlotData,double[] sweepFreqArray)
        {
            ArrayList rawData = s21PlotData.GetRange(1, s21PlotData.Count - 1);
            string plotHead = s21PlotData[0].ToString();

            string[] s21PlotDataArray = (string[])rawData.ToArray(typeof(string));

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            if (rfPortName == "XIP")
            {
                plotHead = "Freq," + rfPortName;
                List<double> freqArray = new List<double>();
                string[] tempPlotData = new string[s21PlotDataArray.Length];
                for (int i = 0; i < s21PlotDataArray.Length; i++)
                {
                    tempPlotData[i] = s21PlotDataArray[i].Split(',')[1];
                }
                savePlotData.AppendResultToFile(s21PlotFile, "Freq", sweepFreqArray);
                savePlotData.AppendResultToFile(s21PlotFile, rfPortName, tempPlotData);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[s21PlotDataArray.Length];
                for (int i = 0; i < s21PlotDataArray.Length; i++)
                {
                    tempPlotData[i] = s21PlotDataArray[i].Split(',')[1];
                }
                savePlotData.AppendResultToFile(s21PlotFile, plotHead, tempPlotData);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s21PlotFile"></param>
        /// <param name="rfPortName"></param>
        /// <param name="s21PlotData"></param>
        /// <param name="sweepFreqArray"></param>
        private void SaveSprrProcessingData(string sprrProcessingDataFile, ArrayList s21PlotData,double refPower_dBm)
        {
            string plotHead="";
            string[] tempStr = s21PlotData[0].ToString().Split(',');
            //for (int i = 0; i < tempStr.Length; i++)
            //{
            //    plotHead += tempStr[i] + "_WL" + wavelength_nm.ToString() + ",";
            //}
            plotHead += tempStr[1] + "_P" + refPower_dBm.ToString();
            //plotHead = plotHead.Remove((plotHead.Length - 1), 1);

            ArrayList rawData = new ArrayList();
            for (int i = 1; i < s21PlotData.Count; i++)
            {
                string[] str = s21PlotData[i].ToString().Split(',');
                rawData.Add(str[1]);
            }

            //ArrayList rawData = s21PlotData.GetRange(1, s21PlotData.Count - 1);
            string[] s21PlotDataArray = (string[])rawData.ToArray(typeof(string));

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(sprrProcessingDataFile, plotHead, s21PlotDataArray);
        }

        /// <summary>
        /// save sprr proccessing data
        /// </summary>
        /// <param name="sprrProcessingDataFile">sprr proccessing file</param>
        /// <param name="plotData">plot data</param>
        /// <param name="paramName">param name</param>
        /// <param name="wavelengthNumber">wavelength number</param>
        private void SaveSprrProcessingData(string sprrProcessingDataFile, double[] plotData, string paramName, double refPower_dBm)
        {
            string plotHead = paramName + "_P" + refPower_dBm.ToString();

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(sprrProcessingDataFile, plotHead, plotData);
        }

        /// <summary>
        /// Save sprr plot data to file
        /// </summary>
        /// <param name="sprrPlotDataFile">sprr plot data file name</param>
        /// <param name="freqArray"></param>
        /// <param name="plotData"></param>
        /// <param name="tiaName"></param>
        /// <param name="fibrePath"></param>
        private void SaveSprrPlotData(string sprrPlotDataFile,double[] freqArray, double[] plotData, string tiaName, string fibrePath)
        {
            Util_SavePlotData savePlotData = new Util_SavePlotData();
            if (!File.Exists(sprrPlotDataFile))
            {
                savePlotData.AppendResultToFile(sprrPlotDataFile, "freq_GHz", freqArray);
            }

            string plotHead = tiaName + "_" + fibrePath;
            savePlotData.AppendResultToFile(sprrPlotDataFile, plotHead, plotData);
        }

        /// <summary>
        /// save phase and dlp data to xyData file and dlp file
        /// </summary>
        /// <param name="xyDataFile"></param>
        /// <param name="phaseRawDataArray"></param>
        /// <param name="phaseUnwrapDataArray"></param>
        /// <param name="dlpArray"></param>
        private void SavePhaseAndDlpData(string xyDataFile, string dlpFile, string rfPortName, double[] phaseRawDataArray, 
            double[] phaseUnwrapDataArray, double[] phaseUnwrapLinearDataArray, double[] dlpRawArray, double[] dlpSmoothArray, 
            double[] sweepFreqArray)
        {

            if (phaseRawDataArray.Length != phaseUnwrapDataArray.Length &&
                phaseRawDataArray.Length != phaseUnwrapLinearDataArray.Length &&
                phaseRawDataArray.Length != dlpRawArray.Length &&
                phaseRawDataArray.Length != dlpSmoothArray.Length)
            {
                throw new Exception("the array's length need to equal");
            }

            string[] plotData = new string[dlpRawArray.Length];
            for (int i = 0; i < plotData.Length; i++)
            {
                plotData[i] = phaseRawDataArray[i].ToString() + "," + phaseUnwrapDataArray[i].ToString() +
                    "," + phaseUnwrapLinearDataArray[i].ToString() + "," + dlpRawArray[i].ToString() + ","
                    + dlpSmoothArray[i].ToString();
            }
            string plotHead = "Raw phase,Unwrapped phase,Linear phase,Dlp raw,Dlp smooth";
            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(xyDataFile, plotHead, plotData);

            //save all dlp in one .csv file
            if (rfPortName == "XIP")
            {
                List<double> freqArray = new List<double>();
                string[] tempPlotData = new string[dlpRawArray.Length];
                for (int i = 0; i < dlpRawArray.Length; i++)
                {
                    tempPlotData[i] = dlpRawArray[i].ToString("0.#####");
                }
                savePlotData.AppendResultToFile(dlpFile, "Freq", sweepFreqArray);
                savePlotData.AppendResultToFile(dlpFile, rfPortName, tempPlotData);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[dlpRawArray.Length];
                for (int i = 0; i < dlpRawArray.Length; i++)
                {
                    tempPlotData[i] = dlpRawArray[i].ToString("0.#####");
                }
                savePlotData.AppendResultToFile(dlpFile, plotHead, tempPlotData);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dlpPlotFile"></param>
        /// <param name="rfPortName"></param>
        /// <param name="dlpRawArray"></param>
        private void SaveDlpPlotData(string dlpPlotFile, string rfPortName, double[] dlpRawArray)
        {
            string plotHead = dlpRawArray[0].ToString();
            //dlpRawArray.RemoveAt(0);
            double[] dlpDataArray = dlpRawArray;

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            if (rfPortName == "XIP")
            {
                plotHead = "Freq," + rfPortName;
                savePlotData.AppendResultToFile(dlpPlotFile, plotHead, dlpDataArray);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[dlpDataArray.Length];
                for (int i = 0; i < dlpDataArray.Length; i++)
                {
                    tempPlotData[i] = dlpDataArray[i].ToString();
                }
                savePlotData.AppendResultToFile(dlpPlotFile, plotHead, tempPlotData);
            }
        }

        /// <summary>
        /// save group delay data .csv file
        /// </summary>
        /// <param name="xyDataFile">xyData file name</param>
        /// <param name="gDFile">group delay file name</param>
        /// <param name="rfPortName">current test rf port name</param>
        /// <param name="listGDArray">list contain current group delay data</param>
        /// <param name="sweepFreqArray">array contain sweep freq point</param>
        private static void SaveGroupDelayData(string xyDataFile, string gDFile, string rfPortName,
            DatumList listGDArray, double[] sweepFreqArray)
        {
            string plotHead = "";
            string[] plotContent;
            ArrayList datumNames = listGDArray.GetDatumNameList();
            for (int i = 0; i < datumNames.Count; i++)
            {
                plotHead += datumNames[i].ToString();
                plotHead += ",";

            }
            plotHead = plotHead.Substring(0, plotHead.Length - 1);
            int intCount = listGDArray.ReadDoubleArray(datumNames[0].ToString()).Length;
            plotContent = new string[intCount];
            for (int i = 0; i < intCount; i++)
            {
                for (int j = 0; j < datumNames.Count; j++)
                {
                    double[] tempArray = listGDArray.ReadDoubleArray(datumNames[j].ToString());
                    if (tempArray[i].ToString() == "NaN")
                    {
                        plotContent[i] += "";
                    }
                    else
                    {
                        plotContent[i] += tempArray[i].ToString();
                    }
                    plotContent[i] += ",";
                }
                plotContent[i].Substring(0, plotContent[i].Length - 1);
            }


            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(xyDataFile, plotHead, plotContent);

            //save all gd in one .csv file
            if (rfPortName == "XIP")
            {
                List<double> freqArray = new List<double>();
                string[] tempPlotData = new string[plotContent.Length];
                for (int i = 0; i < plotContent.Length; i++)
                {
                    tempPlotData[i] = plotContent[i].Split(',')[3];
                }
                savePlotData.AppendResultToFile(gDFile, "Freq", sweepFreqArray);
                savePlotData.AppendResultToFile(gDFile, rfPortName, tempPlotData);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[plotContent.Length];
                for (int i = 0; i < plotContent.Length; i++)
                {
                    tempPlotData[i] = plotContent[i].Split(',')[3];
                }
                savePlotData.AppendResultToFile(gDFile, plotHead, tempPlotData);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dlpPlotFile"></param>
        /// <param name="rfPortName"></param>
        /// <param name="dlpRawArray"></param>
        private void SaveGroupDelayPlotData(string dlpPlotFile, string rfPortName, double[] dlpRawArray)
        {
            string plotHead = dlpRawArray[0].ToString();
            //dlpRawArray.RemoveAt(0);
            double[] dlpDataArray = dlpRawArray;

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            if (rfPortName == "XIP")
            {
                plotHead = "Freq," + rfPortName;
                savePlotData.AppendResultToFile(dlpPlotFile, plotHead, dlpDataArray);
            }
            else
            {
                plotHead = rfPortName;
                string[] tempPlotData = new string[dlpDataArray.Length];
                for (int i = 0; i < dlpDataArray.Length; i++)
                {
                    tempPlotData[i] = dlpDataArray[i].ToString().Split(',')[1];
                }
                savePlotData.AppendResultToFile(dlpPlotFile, plotHead, tempPlotData);
            }
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleRunReturn"></param>
        /// <param name="hybirdChipName"></param>
        /// <param name="rfPortName"></param>
        private void SaveS21PowerAndPdCurrent(ModuleRunReturn moduleRunReturn, string hybirdChipName, string rfPortName)
        {
            double[] pdCurrentArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("pdCurrentArray");
            DatumList listDatumInputPower = moduleRunReturn.ModuleRunData.ModuleData.ReadListDatum("listDatumInputPower");

            DatumList listPwrAndPdCurrent = new DatumList();
            listPwrAndPdCurrent.AddDouble("Sig_Power_dBm", listDatumInputPower.ReadDouble("sigInputPower_dBm"));
            listPwrAndPdCurrent.AddDouble("Loc_Power_dBm", listDatumInputPower.ReadDouble("locInputPower_dBm"));

            if (hybirdChipName.Contains("X"))
            {
                listPwrAndPdCurrent.AddDouble("_pd1_mA", pdCurrentArray[0]);
                listPwrAndPdCurrent.AddDouble("_pd2_mA", pdCurrentArray[1]);
                listPwrAndPdCurrent.AddDouble("_pd3_mA", pdCurrentArray[2]);
                listPwrAndPdCurrent.AddDouble("_pd4_mA", pdCurrentArray[3]);
            }
            else
            {
                listPwrAndPdCurrent.AddDouble("_pd5_mA", pdCurrentArray[4]);
                listPwrAndPdCurrent.AddDouble("_pd6_mA", pdCurrentArray[5]);
                listPwrAndPdCurrent.AddDouble("_pd7_mA", pdCurrentArray[6]);
                listPwrAndPdCurrent.AddDouble("_pd8_mA", pdCurrentArray[7]);
            }

            string fileName;
            if (hybirdChipName.Contains("X"))
            {
                fileName = s21CurrentChipXFile;
            }
            else
            {
                fileName = s21CurrentChipYFile;
            }

            using (StreamWriter sw = new StreamWriter(fileName, true))
            {
                string head = "";
                string currentData = rfPortName;
                foreach (Datum var in listPwrAndPdCurrent)
                {
                    head += ",";
                    head += var.Name;
                    currentData += ",";
                    currentData += var.ValueToString();
                }

                if (rfPortName.Contains("IP"))
                {
                    sw.WriteLine(head);
                }
                sw.WriteLine(currentData);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleRunReturn"></param>
        /// <param name="hybirdChipName"></param>
        /// <param name="rfPortName"></param>
        /// <param name="bWriteFileFlag"></param>
        /// <param name="inPutPowerPortName"></param>
        private void SaveSprrPowerAndPdCurrent(ModuleRunReturn moduleRunReturn, string hybirdChipName, string rfPortName,
            bool bWriteFileFlag, string inPutPowerPortName, double refPower_dBm)
        {
            double[] pdCurrentArray = moduleRunReturn.ModuleRunData.ModuleData.ReadDoubleArray("pdCurrentArray");
            DatumList listDatumInputPower = moduleRunReturn.ModuleRunData.ModuleData.ReadListDatum("listDatumInputPower");

            listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_Sig_Power_dBm", listDatumInputPower.ReadDouble("sigInputPower_dBm"));
            listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_Loc_Power_dBm", listDatumInputPower.ReadDouble("locInputPower_dBm"));

            if (hybirdChipName.Contains("X"))
            {
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd1_mA", pdCurrentArray[0]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd2_mA", pdCurrentArray[1]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd3_mA", pdCurrentArray[2]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd4_mA", pdCurrentArray[3]);
                
            }
            else
            {
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd5_mA", pdCurrentArray[4]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd6_mA", pdCurrentArray[5]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd7_mA", pdCurrentArray[6]);
                listPwrAndPdCurrentSprrTest.AddDouble(inPutPowerPortName + "_pd8_mA", pdCurrentArray[7]);
            }

            if (bWriteFileFlag)
            {
                string sprrCurrentFile;
                //if (hybirdChipName.Contains("X"))
                //{
                    //sprrCurrentFile = sprrChipXCurrentFile;
                    sprrCurrentFile = listSprrCurrentFile.ReadString("sprrCurrent_P" + refPower_dBm.ToString());
                //    sprrCurrentFile = listSprrChipXCurrentFile.ReadString("sprrCurrent_ChipX_P" + refPower_dBm.ToString());
                //}
                //else
                //{
                //    //sprrCurrentFile = sprrChipYCurrentFile;
                //    sprrCurrentFile = listSprrChipYCurrentFile.ReadString("sprrCurrent_ChipY_P" + refPower_dBm.ToString());
                //}

                using (StreamWriter sw = new StreamWriter(sprrCurrentFile, true))
                {
                    string head = "";
                    string currentData = rfPortName;
                    foreach (Datum var in listPwrAndPdCurrentSprrTest)
                    {
                        head += ",";
                        head += var.Name;
                        currentData += ",";
                        currentData += var.ValueToString();
                    }

                    if (rfPortName.Contains("XIP"))
                    {
                        sw.WriteLine(head);
                    }
                    sw.WriteLine(currentData);
                }
            }
        }

        /// <summary>
        /// Save s21 statistics plot data to .csv
        /// </summary>
        /// <param name="s21StatisticsFile">s21 statistics file</param>
        /// <param name="listS21StatiData">list contain s21 3dB point data</param>
        private void SaveS21StatisticsPlotData(string s21StatisticsFile,string plotHead, List<double> listS21StatiData)
        {

            double[] s21DataArray = listS21StatiData.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            plotHead = "refPwr:" + plotHead + "dBm";
            if (!File.Exists(s21StatisticsFile))
            {
                string[] strTemp ={ "XIP", "XIN", "XQP", "XQN", "YIP", "YIN", "YQP", "YQN" };
                savePlotData.AppendResultToFile(s21StatisticsFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(s21StatisticsFile, plotHead, s21DataArray);
            }
            else
            {
                savePlotData.AppendResultToFile(s21StatisticsFile, plotHead, s21DataArray);
            }
        }

        /// <summary>
        /// Save sprr Statistics PlotData to .csv
        /// </summary>
        /// <param name="sprrStatisticsFile">sprr statistics file </param>
        /// <param name="plotHead">plot head</param>
        /// <param name="listSprrStatiData">list contain sprr data</param>
        private void SaveSprrStatisticsPlotData(ITestEngineRun engine, string sprrStatisticsFile, string plotHead,
            List<double> listSigSprrStatiData, List<double> listLocSprrStatiData)
        {
            if (listSigSprrStatiData.Count != listLocSprrStatiData.Count)
            {
                engine.ErrorInProgram("sig sprr counts not equal loc sprr counts!");
            }
            List<double> listSprrStatiData = new List<double>();
            for (int i = 0; i < listSigSprrData.Count; i++)
            {
                listSprrStatiData.Add(listSigSprrStatiData[i]);
                listSprrStatiData.Add(listLocSprrStatiData[i]);
            }
            double[] sprrDataArray = listSprrStatiData.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            plotHead = "refPwr:" + plotHead + "dBm";
            if (!File.Exists(sprrStatisticsFile))
            {
                string[] strTemp ={ "XI_Sig", "XI_Loc", "XQ_Sig", "XQ_Loc", "YI_Sig", "YI_Loc", "YQ_Sig", "YQ_Loc" };
                savePlotData.AppendResultToFile(sprrStatisticsFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(sprrStatisticsFile, plotHead, sprrDataArray);
            }
            else
            {
                savePlotData.AppendResultToFile(sprrStatisticsFile, plotHead, sprrDataArray);
            }
        }

        /// <summary>
        /// Save s22 mask pass fail result to .csv file
        /// </summary>
        /// <param name="s21MaskPassFailFile">s21 mask pass fail file</param>
        /// <param name="plotHead">plot head</param>
        /// <param name="listS21MaskResult"> list contain s21 mask result</param>
        private void SaveS22MaskPassFailData(string s22MaskPassFailFile, string plotHead, List<bool> listMaskResult)
        {

            List<string> listTemp = new List<string>();
            for (int i = 0; i < listMaskResult.Count; i++)
            {
                if (listMaskResult[i])
                {
                    listTemp.Add("Pass");
                }
                else
                {
                    listTemp.Add("Fail");
                }
            }
            string[] maskResultArray = listTemp.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            if (!File.Exists(s22MaskPassFailFile))
            {
                string[] strTemp ={ "XIP", "XIN", "XQP", "XQN", "YIP", "YIN", "YQP", "YQN" };
                savePlotData.AppendResultToFile(s22MaskPassFailFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(s22MaskPassFailFile, plotHead, maskResultArray);
            }
            else
            {
                savePlotData.AppendResultToFile(s22MaskPassFailFile, plotHead, maskResultArray);
            }
        }

        /// <summary>
        /// Save s21 mask pass fail result to .csv file
        /// </summary>
        /// <param name="s21MaskPassFailFile">s21 mask pass fail file</param>
        /// <param name="plotHead">plot head</param>
        /// <param name="listS21MaskResult"> list contain s21 mask result</param>
        private void SaveS21MaskPassFailData(string s21MaskPassFailFile, string plotHead, List<bool> listMaskResult)
        {

            List<string> listTemp = new List<string>();
            for (int i = 0; i < listMaskResult.Count; i++)
            {
                if (listMaskResult[i])
                {
                    listTemp.Add("Pass");
                }
                else
                {
                    listTemp.Add("Fail");
                }
            }
            string[] maskResultArray = listTemp.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            plotHead = "refPwr:" + plotHead + "dBm";
            if (!File.Exists(s21MaskPassFailFile))
            {
                string[] strTemp ={ "XIP", "XIN", "XQP", "XQN", "YIP", "YIN", "YQP", "YQN" };
                savePlotData.AppendResultToFile(s21MaskPassFailFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(s21MaskPassFailFile, plotHead, maskResultArray);
            }
            else
            {
                savePlotData.AppendResultToFile(s21MaskPassFailFile, plotHead, maskResultArray);
            }
        }

        /// <summary>
        /// Save sprr mask pass fail result to .csv file
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="sprrMaskPassFailFile"></param>
        /// <param name="plotHead"></param>
        /// <param name="listSigSprrMaskResult"></param>
        /// <param name="listLocSprrMaskResult"></param>
        private void SaveSprrMaskPassFailData(ITestEngineRun engine, string sprrMaskPassFailFile, string plotHead,
            List<bool> listSigSprrMaskResult, List<bool> listLocSprrMaskResult)
        {

            if (listSigSprrMaskResult.Count != listLocSprrMaskResult.Count)
            {
                engine.ErrorInProgram("sig sprr mask counts not equal loc mask sprr counts!");
            }
            List<bool> listSprrMaskResult = new List<bool>();
            for (int i = 0; i < listSigSprrMaskResult.Count; i++)
            {
                listSprrMaskResult.Add(listSigSprrMaskResult[i]);
                listSprrMaskResult.Add(listLocSprrMaskResult[i]);
            }

            List<string> listTemp = new List<string>();
            for (int i = 0; i < listSprrMaskResult.Count; i++)
            {
                if (listSprrMaskResult[i])
                {
                    listTemp.Add("Pass");
                }
                else
                {
                    listTemp.Add("Fail");
                }
            }
            string[] maskResultArray = listTemp.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            plotHead = "refPwr:" + plotHead + "dBm";
            if (!File.Exists(sprrMaskPassFailFile))
            {
                string[] strTemp ={ "XI_Sig", "XI_Loc", "XQ_Sig", "XQ_Loc", "YI_Sig", "YI_Loc", "YQ_Sig", "YQ_Loc" };
                savePlotData.AppendResultToFile(sprrMaskPassFailFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(sprrMaskPassFailFile, plotHead, maskResultArray);
            }
            else
            {
                savePlotData.AppendResultToFile(sprrMaskPassFailFile, plotHead, maskResultArray);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentRatioFile"></param>
        /// <param name="plotHead"></param>
        /// <param name="listCurrentRatio"></param>
        private void SavePdCurrentRatioData(string currentRatioFile, string plotHead, List<double> listCurrentRatio)
        {

            double[] currentRatioArray = listCurrentRatio.ToArray();

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            plotHead = "refPwr:" + plotHead + "dBm";
            if (!File.Exists(currentRatioFile))
            {
                string[] strTemp ={ "XIP", "XIN", "XQP", "XQN", "YIP", "YIN", "YQP", "YQN" };
                savePlotData.AppendResultToFile(currentRatioFile, "Lane", strTemp);
                savePlotData.AppendResultToFile(currentRatioFile, plotHead, currentRatioArray);
            }
            else
            {
                savePlotData.AppendResultToFile(currentRatioFile, plotHead, currentRatioArray);
            }
        }

        #endregion

        enum RfOutputEnum
        {
            XIP,
            XIN,
            XQP,
            XQN,
            YIP,
            YIN,
            YQP,
            YQN,
            OFFALL
        }

        enum TiaEnum
        {
            XI,
            XQ,
            YI,
            YQ
        }

        enum HybirdChipEnum
        {
            X,
            Y
        }

    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureGroupDelay.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureGroupDelay : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            //qualTest = configData.ReadBool("qualTest");

            #region init configure
            //s21 sweep configure
            //AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            //analyzerSetup_S21.startFrequency_GHz = configData.ReadDouble("s21StartFrequency_GHz");
            //analyzerSetup_S21.stopFrequency_GHz = configData.ReadDouble("s21StopFrequency_GHz");
            //analyzerSetup_S21.sweepPoints = (uint)configData.ReadSint32("sweepPoints");
            //analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            //analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            //analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            //analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            //analyzerSetup_S21.CalRegister = configData.ReadSint32("CalRegister");
            //analyzerSetup_S21.s21Scale = configData.ReadDouble("s21Scale");

            int s21CalRegister = configData.ReadSint32("CalRegister");

            double targetGroupDelayFreq_GHz = configData.ReadDouble("targetGroupDelayFreq_GHz");
            double phaseAnalysisStopFreq_GHz = configData.ReadDouble("phaseAnalysisStopFreq_GHz");

            int firstSmoothingPoints=configData.ReadSint32("fitstSmoothingPoints");
            int secondSmoothingPoints=configData.ReadSint32("secondSmoothingPoints");
            int ThirdSmoothingPoints = configData.ReadSint32("thirdSmoothingPoints");

            #endregion

            if (!configData.IsPresent("xyRawData"))
            {
                s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
            }
            else
            {
                s21RealAndImaginaryTrace = (Trace)configData.ReadReference("xyRawData");
            }

            s21PhaseTrace = SparamAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace, phaseAnalysisStopFreq_GHz);

            engine.SendToGui(s21PhaseTrace);

            
            Trace s21UnwrapPhaseTrace = new Trace();
            SparamAnalysise.UnwrappingPhaseData(engine, s21PhaseTrace, out s21UnwrapPhaseTrace, 360);

            double[] freqArray = s21UnwrapPhaseTrace.GetXArray();
            double[] unwrapPhaseArray = s21UnwrapPhaseTrace.GetYArray();

            int length = freqArray.Length;
            double[] groupDelayRawDataArray = new double[length];
            double[] GroupDelayDiscardBelow1GHzArray = new double[length];
            double[] firstSmoothGDArray=new double [length];
            double[] secondSmoothGDArray = new double[length];
            double[] thirdSmoothGDArray = new double[length];
            double[] unwrapPhaseRadianArray = new double[length];

            double[] gdArrayForALU = new double[length];
            double[] freqArrayForALU = new double[length];

            //radian=unwrapPhase/360
            for (int i = 0; i < groupDelayRawDataArray.Length; i++)
            {
                unwrapPhaseRadianArray[i] = unwrapPhaseArray[i]/ 360;
            }

            //slop((unwrapPhaseRadianArray(i-1,i+1)/Freq(i-1,i+1));
            groupDelayRawDataArray = DifferentiateArray(engine, freqArray, unwrapPhaseRadianArray);

            //groudDelay=slop * 1000;
            for (int i = 0; i < groupDelayRawDataArray.Length; i++)
            {
                groupDelayRawDataArray[i] *= 1000;
            }

            //smooth group delay 
            firstSmoothGDArray = Alg_Smoothing.AveragingSmoothing(groupDelayRawDataArray, firstSmoothingPoints);
            secondSmoothGDArray = Alg_Smoothing.AveragingSmoothing(firstSmoothGDArray, secondSmoothingPoints);

            //Discard all measurements below 1GHz,used 1GHz point to replace them
            int indexOf1GHz = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArray, 1);
            secondSmoothGDArray.CopyTo(GroupDelayDiscardBelow1GHzArray, 0);
            for (int i = 0; i < indexOf1GHz; i++)
            {
                GroupDelayDiscardBelow1GHzArray[i] = double.NaN;
            }
            int index = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArray, targetGroupDelayFreq_GHz);

            groupDelay_ps = GroupDelayDiscardBelow1GHzArray[index];

            if (configData.IsPresent("ALU"))
            {
                thirdSmoothGDArray = Alg_Smoothing.AveragingSmoothing(secondSmoothGDArray, ThirdSmoothingPoints);

                int indexOf10GHz = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArray, 10);
                double[] gdarrayFrom1To10GHz;
                double averageGDFron1To10GHz = 0.0;
                gdarrayFrom1To10GHz = Alg_ArrayFunctions.ExtractSubArray(thirdSmoothGDArray, indexOf1GHz, indexOf10GHz);

                //calculate Average group delay from 1GHz to 10GHz
                for (int i = 0; i < gdarrayFrom1To10GHz.Length; i++)
                {
                    averageGDFron1To10GHz += gdarrayFrom1To10GHz[i];
                }
                averageGDFron1To10GHz /= gdarrayFrom1To10GHz.Length;


                //GD = 35pts -average from 1GHz to 10GHz
                for (int i = 0; i < thirdSmoothGDArray.Length; i++)
                {
                    if (i < indexOf1GHz)
                    {
                        gdArrayForALU[i] = double.NaN;
                    }
                    else
                    {
                        gdArrayForALU[i] = thirdSmoothGDArray[i] - averageGDFron1To10GHz;
                    }
                }

                //calculate new freqArray
                // newFreq=(freq(i)+freq(i+1))/2
                for (int i = 0; i < freqArray.Length - 1; i++)
                {
                    freqArrayForALU[i] = (freqArray[i] + freqArray[i + 1]) / 2;
                }
                freqArrayForALU[freqArrayForALU.Length - 1] = freqArray[freqArray.Length - 1];

                int indexForALU = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArrayForALU, targetGroupDelayFreq_GHz);
                groupDelay_ps = freqArrayForALU[indexForALU];
            }
            
            

            // return data
            DatumList returnData = new DatumList();

            returnData.AddReference("s21RealAndImaginaryTrace", s21RealAndImaginaryTrace);
            returnData.AddReference("s21PhaseTrace", s21PhaseTrace);
            returnData.AddReference("s21UnwrapPhaseTrace", s21UnwrapPhaseTrace);
            returnData.AddDouble("groupDelay_ps", groupDelay_ps);

            returnData.AddDoubleArray("GDRawDataArray", groupDelayRawDataArray);
            returnData.AddDoubleArray("firstSmoothGDArray", firstSmoothGDArray);
            returnData.AddDoubleArray("secondSmoothGDArray", secondSmoothGDArray);

            if (configData.IsPresent("ALU"))
            {
                returnData.AddDoubleArray("thirdSmoothGDArray", thirdSmoothGDArray);
                returnData.AddDoubleArray("freqArrayForALU", freqArrayForALU);
                returnData.AddOrUpdateDoubleArray("gdArrayForALU", gdArrayForALU);
            }
            
            returnData.AddDoubleArray("SweepFreqArray", freqArray);
            returnData.AddDoubleArray("GroupDelayDiscardBelow1GHzArray", GroupDelayDiscardBelow1GHzArray);
            return returnData;
        }

        #region private function

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="xArray"></param>
        /// <param name="yArray"></param>
        /// <returns></returns>
        private double[] DifferentiateArray(ITestEngine engine, double[] xArray, double[] yArray)
        {
            if (xArray.Length != yArray.Length)
            {
                engine.ErrorInModule("Invalid xArray and yArray data!");
            }
            List<double> listDiffPlot = new List<double>();
            for (int i = 0; i < yArray.Length; i++)
            {
                //Can't do full differential, so do half point differential
                if (i == 0)
                {
                    if (yArray[i + 1] - yArray[i] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i + 1] - yArray[i]) / (xArray[i + 1] - xArray[i]);
                        listDiffPlot.Add(diffValue);
                    }
                }
                    //Can't do full differential, so do half point differential
                else if (i == yArray.Length - 1)
                {
                    if (yArray[i] - yArray[i - 1] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i] - yArray[i - 1]) / (xArray[i] - xArray[i - 1]);
                        listDiffPlot.Add(diffValue);
                    }
                }
                else
                {
                    if (yArray[i+1] - yArray[i - 1] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i + 1] - yArray[i - 1]) / (xArray[i + 1] - xArray[i - 1]);
                        listDiffPlot.Add(diffValue);
                    }
                }
            }

            double[] diffArray;
            diffArray=listDiffPlot.ToArray();
            return diffArray;

        }

        /// <summary>
        /// calculate new freqArray
        /// newFreq=(freq(i)+freq(i+1))/2
        /// </summary>
        /// <param name="freqArray"></param>
        /// <returns>new freqArray</returns>
        //private double[] CalculateNewFreq(double[] freqArray)
        //{
        //    double[] newFreqArray = new double[freqArray.Length];

        //    for (int i = 0; i < freqArray.Length - 1; i++)
        //    {
        //        newFreqArray[i] = (freqArray[i] + freqArray[i + 1]) / 2;
        //    }
        //    newFreqArray[newFreqArray.Length - 1] = freqArray[freqArray.Length - 1];

        //    return newFreqArray;
        //}

        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureGroupDelayGui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s21PhaseTrace = new Trace();

        Trace s21RealAndImaginaryTrace = new Trace();

        double groupDelay_ps;
        #endregion

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureEO.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;
using System.Threading;
using System.Collections;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_ConversionGain : ITestModule
    {
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {

            double[] VGC_V = configData.ReadDoubleArray("VGC_V");
            double[] SIG_POWER_DBM = configData.ReadDoubleArray("SIG_POWER_DBM");
            double[] LO_POWER_DBM = configData.ReadDoubleArray("LO_POWER_DBM");
            Trace[] S21TracesN = (Trace[])configData.ReadReference("S21TracesN");
            Trace[] S21TracesP = (Trace[])configData.ReadReference("S21TracesP");
            string serialNumber = configData.ReadString("DutSerialNumber");
            Inst_SR830.SensitivityEnum[] SensitivityLockInN = (Inst_SR830.SensitivityEnum[])configData.ReadReference("LockInSensitivityN");
            Inst_SR830.SensitivityEnum[] SensitivityLockInP = (Inst_SR830.SensitivityEnum[])configData.ReadReference("LockInSensitivityP");
            double[] VinRef = configData.ReadDoubleArray("VIN_REF");
            
            ArrayList plot = new ArrayList();
            
            /*
            string PlotEntry = string.Format("{0},{1},{2},{3},{4}", "VGC_V", "SIG_POWER_DBM", "LO_POWER_DBM", "VNA_INPUT_POWER_DBM", "VNA_INPUT_POWER_RMS_MV"); // inputs
            PlotEntry += string.Format(",{0},{1}", "RX_OUTPUT_VOLTAGE_RMS_MV", "RX_OUTPUT_VOLTAGE_MVPP"); // output voltage
            PlotEntry += string.Format(",{0},{1},{2}", "FREQUENCY_P", "S21_REAL_P", "S21_IMG_P"); // measurements P
            PlotEntry += string.Format(",{0},{1},{2}", "FREQUENCY_N", "S21_REAL_N", "S21_IMG_N"); // measurements N
            PlotEntry += string.Format(",{0},{1},{2}", "MEASUREMENT_DBE", "CONVERSION_GAIN", "CONVERSION_GAIN_DBE"); //result
            plot.Add(PlotEntry);
            */

            string PlotEntry = string.Format("{0},{1},{2},{3},{4}", "VGC_V", "SIG_POWER_DBM", "LO_POWER_DBM","SIG_POWER_W", "VIN_REF_mVpp"); // inputs
            PlotEntry += string.Format(",{0},{1},{2},{3},{4},{5}", "LOCK_IN_SENSITIVITY_P", "FREQUENCY_P", "S21_REAL_P", "S21_IMG_P", "S21_MAG_P", "VOUT_GPPO_P_mVpp"); // measurements P
            PlotEntry += string.Format(",{0},{1},{2},{3},{4},{5}", "LOCK_IN_SENSITIVITY_N", "FREQUENCY_N", "S21_REAL_N", "S21_IMG_N", "S21_MAG_N", "VOUT_GPPO_N_mVpp"); // measurements N
            PlotEntry += string.Format(",{0},{1}", "CONVERSION_GAIN", "CONVERSION_GAIN_DBE"); //result
            plot.Add(PlotEntry);


            List<double> ConversionGaindBe = new List<double>();
 
            for (int idx = 0 ;  idx < VGC_V.Length ; idx++){



                double RealP, ImagP, RealN, ImagN, freqP, freqN, MagN, MagP;
                
                GetValueFromTrace(1.0, S21TracesN[idx], out freqN, out RealN, out ImagN, out MagN);
                GetValueFromTrace(1.0, S21TracesP[idx], out freqP, out RealP, out ImagP, out MagP);
                

                ConversionGainData CGD = CalculateConversionGain(LO_POWER_DBM[idx], SIG_POWER_DBM[idx], RealP, ImagP, RealN, ImagN, freqP, freqN, VGC_V[idx], SensitivityLockInP[idx], SensitivityLockInN[idx], VinRef[idx], MagN, MagP);
                ConversionGaindBe.Add(CGD.CONVERSION_GAIN_DBE);

                //ConversionGainDataBakup CGD = CalculateConversionGainBackup(LO_POWER_DBM[idx], SIG_POWER_DBM[idx], -10, RealP, ImagP, RealN, ImagN, freqP, freqN, VGC_V[idx]);

                PlotEntry = string.Format("{0},{1},{2},{3},{4}", CGD.VGC_V, CGD.SIG_POWER_DBM, CGD.LO_POWER_DBM, CGD.SIG_POWER_W, CGD.VIN_REF_mVpp); // inputs
                PlotEntry += string.Format(",{0},{1},{2},{3},{4},{5}", CGD.LockInSensitivityP,CGD.FREQUENCY_P, CGD.S21_REAL_P, CGD.S21_IMG_P, CGD.S21_MAG_P, CGD.VOUT_GPPO_P_mVpp); // measurements P
                PlotEntry += string.Format(",{0},{1},{2},{3},{4},{5}", CGD.LockInSensitivityN,CGD.FREQUENCY_N, CGD.S21_REAL_N, CGD.S21_IMG_N, CGD.S21_MAG_N, CGD.VOUT_GPPO_N_mVpp); // measurements N
                PlotEntry += string.Format(",{0},{1}", CGD.CONVERSION_GAIN, CGD.CONVERSION_GAIN_DBE); //result
                plot.Add(PlotEntry);

                

                /*
                PlotEntry = string.Format("{0},{1},{2},{3},{4}", CGD.VGC_V , CGD.SIG_POWER_DBM, CGD.LO_POWER_DBM, CGD.VNA_INPUT_POWER_DBM, CGD.VNA_INPUT_POWER_RMS_MV ); // inputs
                PlotEntry += string.Format(",{0},{1}",CGD.RX_OUTPUT_VOLTAGE_RMS_MV, CGD.RX_OUTPUT_VOLTAGE_MVPP); // output voltage
                PlotEntry += string.Format(",{0},{1},{2}",CGD.FREQUENCY_P , CGD.S21_REAL_P, CGD.S21_IMG_P ); // measurements P
                PlotEntry += string.Format(",{0},{1},{2}",CGD.FREQUENCY_N , CGD.S21_REAL_N, CGD.S21_IMG_N ); // measurements N
                PlotEntry += string.Format(",{0},{1},{2}", CGD.MEASUREMENT_DBE, CGD.CONVERSION_GAIN, CGD.CONVERSION_GAIN_DBE); //result
                plot.Add(PlotEntry);
                */


            }

            DatumList returnData = new DatumList();

            bool check = true;
            double PreviousValue = double.MaxValue;
            //Sanity check the conversion gain they should be decreasing
            foreach (double cg in ConversionGaindBe) {

                check = check && cg < PreviousValue;
                PreviousValue = cg;
            
            }


            ConversionGaindBe.Sort();
            double min = ConversionGaindBe[0];
            double max = ConversionGaindBe[ConversionGaindBe.Count - 1];

            returnData.AddDouble("MaxConversionGain_dBe", max );
            returnData.AddDouble("MinConversionGain_dBe", min);
            returnData.AddBool("ConversionGainCheck", check);
            returnData.AddReference("CONVERSION_GAIN_PLOT", plot);
            return returnData;
        }







        private void GetValueFromTrace(double freq, Trace trace, out double ActualFreq, out double real, out double img, out double mag) {
            
            ActualFreq = -1;
            real = -1;
            img = -1;
            mag = -1;

            for (int i = 0;i < trace.Count; i++) {


                if (freq < trace.GetXArray()[i]) {

                    ActualFreq = trace.GetXArray()[i];
                    real = trace.GetYArray()[i];
                    img = trace.GetZArray()[i];
                    mag = 20*Math.Log10(Math.Sqrt(Math.Pow(real, 2) + Math.Pow(img, 2)));
                    break;
                }
            
            
            }

        }

        public ConversionGainDataBakup CalculateConversionGainBackup(double LoOpticalPower_dbm ,double SigOpticalPower_dbm, double VNA_Input_dbm, double RealP, double ImagP, double RealN, double ImagN, double freqP, double freqN, double VGC_V)
        {

            ConversionGainDataBakup result = new ConversionGainDataBakup();
            result.VNA_INPUT_POWER_DBM = VNA_Input_dbm;
            result.VNA_INPUT_POWER_RMS_MV = Math.Sqrt(Math.Pow(10, VNA_Input_dbm / 10) * 50);
            result.MEASUREMENT_DBE = 20 * Math.Log10(Math.Sqrt(Math.Pow(ImagP + ImagN, 2) + Math.Pow(RealP + RealN, 2)));
            result.RX_OUTPUT_VOLTAGE_RMS_MV = Math.Pow(10, result.MEASUREMENT_DBE / 20) * result.VNA_INPUT_POWER_RMS_MV;
            result.RX_OUTPUT_VOLTAGE_MVPP = result.RX_OUTPUT_VOLTAGE_RMS_MV / 0.3535;
            result.SIG_POWER_DBM = SigOpticalPower_dbm;
            result.LO_POWER_DBM = LoOpticalPower_dbm;
            result.CONVERSION_GAIN = result.RX_OUTPUT_VOLTAGE_MVPP / (2 * Math.Sqrt(Math.Pow(10, result.SIG_POWER_DBM / 10) * Math.Pow(10, result.SIG_POWER_DBM / 10)));
            result.CONVERSION_GAIN_DBE = 20 * Math.Log10(result.CONVERSION_GAIN);
            result.FREQUENCY_N = freqN;
            result.FREQUENCY_P = freqP;
            result.VGC_V = VGC_V;
            result.S21_REAL_N = RealN;
            result.S21_REAL_P = RealP;
            result.S21_IMG_N = ImagN;
            result.S21_IMG_P = ImagP;

            return result;

        }

        public struct ConversionGainDataBakup
        {

            public double VNA_INPUT_POWER_DBM;
            public double VNA_INPUT_POWER_RMS_MV;
            public double MEASUREMENT_DBE;
            public double RX_OUTPUT_VOLTAGE_RMS_MV;
            public double RX_OUTPUT_VOLTAGE_MVPP;
            public double SIG_POWER_DBM;
            public double LO_POWER_DBM;
            public double CONVERSION_GAIN;
            public double CONVERSION_GAIN_DBE;
            public double FREQUENCY_P;
            public double FREQUENCY_N;
            public double VGC_V;
            public double S21_REAL_P;
            public double S21_IMG_P;
            public double S21_REAL_N;
            public double S21_IMG_N;
        }

        public struct ConversionGainData
        {
            //input
            public double VGC_V;
            public double SIG_POWER_DBM;
            public double LO_POWER_DBM;
            public double SIG_POWER_W;
            public double VIN_REF_mVpp;

            //P parameters
            public string LockInSensitivityP;
            public double FREQUENCY_P;
            public double S21_REAL_P;
            public double S21_IMG_P;
            public double S21_MAG_P;
            public double VOUT_GPPO_P_mVpp;

            //N parameters
            public string LockInSensitivityN;
            public double FREQUENCY_N;
            public double S21_REAL_N;
            public double S21_IMG_N;
            public double S21_MAG_N;
            public double VOUT_GPPO_N_mVpp;

            // result
            public double CONVERSION_GAIN;
            public double CONVERSION_GAIN_DBE;
           
            
            
            

        }

        public ConversionGainData CalculateConversionGain(double LoOpticalPower_dbm, double SigOpticalPower_dbm, double RealP, double ImagP, double RealN, double ImagN, double freqP, double freqN, double VGC_V, Inst_SR830.SensitivityEnum Lock_In_SensitivityP, Inst_SR830.SensitivityEnum Lock_In_SensitivityN, double VinRef, double MagN, double MagP)
        {

            ConversionGainData result = new ConversionGainData();
            result.VGC_V = VGC_V;
            result.SIG_POWER_DBM = SigOpticalPower_dbm;
            result.LO_POWER_DBM = LoOpticalPower_dbm;
            result.SIG_POWER_W = Math.Pow(10, SigOpticalPower_dbm / 10) / 1000;
            result.VIN_REF_mVpp = VinRef;

            result.LockInSensitivityP = Lock_In_SensitivityP.ToString();
            result.FREQUENCY_P = freqP;
            result.S21_REAL_P = RealP;
            result.S21_IMG_P = ImagP;
            result.S21_MAG_P = MagP;
            result.VOUT_GPPO_P_mVpp = VinRef * Math.Pow(10, result.S21_MAG_P / 20);

            result.LockInSensitivityN = Lock_In_SensitivityN.ToString();
            result.FREQUENCY_N = freqN;
            result.S21_REAL_N = RealN;
            result.S21_IMG_N = ImagN;
            result.S21_MAG_N = MagN;
            result.VOUT_GPPO_N_mVpp = VinRef * Math.Pow(10, result.S21_MAG_N / 20);

            double LO_POWER_DBM_3DB = LoOpticalPower_dbm - 3;
            double LO_POWER_DBM_3DB_W = Math.Pow(10, LO_POWER_DBM_3DB / 10) / 1000;
            result.CONVERSION_GAIN = ((result.VOUT_GPPO_P_mVpp / 1000) + (result.VOUT_GPPO_N_mVpp / 1000)) / (2 * Math.Sqrt(result.SIG_POWER_W * LO_POWER_DBM_3DB_W));
            result.CONVERSION_GAIN_DBE = 20 * Math.Log10(result.CONVERSION_GAIN);

            return result;


        }
        


        public Type UserControl
        {
            get { return (typeof(Mod_ConversionGainGui)); }
        }

        

       
    }
}

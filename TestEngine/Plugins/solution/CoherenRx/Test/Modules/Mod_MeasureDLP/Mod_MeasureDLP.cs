// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureDLP.cs
//
// Author: sam.quan 2011.8
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureDLP : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// do s21 dlp calculate
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            #region init configure

            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            double targetDLP = configData.ReadDouble("targetDLP");
            int dlpSmoothPoints = configData.ReadSint32("dlpSmoothPoints");
            int s21CalRegister = configData.ReadSint32("CalRegister");
            double phaseAnalysisStopFreq_GHz = configData.ReadDouble("phaseAnalysisStopFreq_GHz");
            double DlpBestFitLineStartFreqGHz = configData.ReadDouble("DlpBestFitLineStartFreqGHz");
            double DlpBestFitLineStopFreqGHz = configData.ReadDouble("DlpBestFitLineStopFreqGHz");

            #endregion

            //get xy raw data
            if (!configData.IsPresent("xyRawData"))
            {
                s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
            }
            else
            {
                s21RealAndImaginaryTrace = (Trace)configData.ReadReference("xyRawData");
            }

            s21PhaseTrace = SparamAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace, phaseAnalysisStopFreq_GHz);


            double param_a;
            double param_b;
            Trace s21UnwrapPhaseTrace = new Trace();
            SparamAnalysise.UnwrappingPhaseData(engine, s21PhaseTrace, out s21UnwrapPhaseTrace, 360);
            SparamAnalysise.BestFitPhaseSlopeByForecast(s21UnwrapPhaseTrace, DlpBestFitLineStartFreqGHz, DlpBestFitLineStopFreqGHz, out param_a, out param_b);

            double[] freqArray_GHz = s21UnwrapPhaseTrace.GetXArray();
            double[] unWrapPhaseArray_deg = s21UnwrapPhaseTrace.GetYArray();
            

            int length = freqArray_GHz.Length;
            double[] dlpRawArray = new double[length];
            double[] dlpSmoothArray = new double[length];
            double[] unWrapPhaseLinearArray_deg = new double[length];
            
            //dlp=y-(a+bx)
            for (int i = 0; i < freqArray_GHz.Length; i++)
            {
                unWrapPhaseLinearArray_deg[i] = param_a + param_b * freqArray_GHz[i];
                dlpRawArray[i] = unWrapPhaseLinearArray_deg[i] - unWrapPhaseArray_deg[i];
            }



            Boolean phaseError = false;
            for (int i = 0; i < freqArray_GHz.Length -1 ; i++)
            {
                if ((unWrapPhaseLinearArray_deg[i] - unWrapPhaseLinearArray_deg[i + 1]) > 60)
                {

                    phaseError = true;
                }
                
            }
            
            dlpSmoothArray = Alg_Smoothing.AveragingSmoothing(dlpRawArray, 5);
            int targetDlpIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(dlpSmoothArray, targetDLP);

            dlpFreq_GHz = freqArray_GHz[targetDlpIndex];

            // return data
            DatumList returnData = new DatumList();

            returnData.AddReference("s21RealAndImaginaryTrace", s21RealAndImaginaryTrace);
            returnData.AddReference("s21PhaseTrace", s21PhaseTrace);
            returnData.AddReference("s21UnwrapPhaseTrace", s21UnwrapPhaseTrace);
            returnData.AddDouble("targetDLPFreq_GHz", dlpFreq_GHz);
            returnData.AddDoubleArray("dlpRawArray", dlpRawArray);
            returnData.AddDoubleArray("dlpSmoothArray", dlpSmoothArray);
            returnData.AddDoubleArray("unWrapPhaseLinearArray", unWrapPhaseLinearArray_deg);
            return returnData;
        }

        #region private function

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(Mod_MeasureDLPGui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s21PhaseTrace = new Trace();

        Trace s21RealAndImaginaryTrace = new Trace();

        double dlpFreq_GHz;
        #endregion

        #endregion
    }
}

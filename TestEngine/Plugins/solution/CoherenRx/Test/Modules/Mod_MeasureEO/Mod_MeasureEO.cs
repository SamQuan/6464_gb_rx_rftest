// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureEO.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;
using System.Threading;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureEO : ITestModule
    {
        Inst_SR830.SensitivityEnum[] SensitivityList;
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiShow();
            engine.GuiToFront();
            //          Instr_Ag33120A WaveformGenerator;
            bool is6464 = configData.ReadBool("is6464");
            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");
            string SweepMode = configData.ReadString("SweepMode");
            //qualTest = configData.ReadBool("qualTest");
            //if (targetPwrLoc_dBm == double.NegativeInfinity)
            //{
            //    WaveformGenerator.Amplitude_mV = waveformAmplitude_mV;
            //    WaveformGenerator.DcOffset_Volt = waveformDcOffset_V;

            //}
            #region init configure
            Instr_MS4640A_VNA LCAnayzer = (Instr_MS4640A_VNA)configData.ReadReference("LightwaveComponentAnalyzer");
            measureSetup_SIG = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            measureSetup_LOC = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");


            if (configData.IsPresent("sprrTest"))
            {
                sprrTest = configData.ReadBool("sprrTest");
            }
            if (configData.IsPresent("rfOutputPort"))
            {
                rfOutputPort = configData.ReadString("rfOutputPort");
            }

            // Dave Smith - Update 1 start

            // Note Add SPI library to references

            bool MeasurePkDetector = false;
            string PeakDetectorFilename = "";
            Instr_SPI instSPI = null;
            UInt16 PeakDetectorSPIChannel = 0;

            if (configData.IsPresent("DoPeakDetectorMeas"))
            {
                MeasurePkDetector = configData.ReadBool("DoPeakDetectorMeas");
                if (MeasurePkDetector)
                {
                    PeakDetectorFilename = configData.ReadString("PeakDetectorFilename");
                    instSPI = (Instr_SPI)configData.ReadReference("InstSPI");
                    int tmpVal = configData.ReadSint32("PkDetSPIChannelNum");
                    PeakDetectorSPIChannel = Convert.ToUInt16(tmpVal);
                }
            }

            // Dave Smith - Update 1 End

            SensitivityList = (Inst_SR830.SensitivityEnum[])configData.ReadReference("LOCK_IN_SENSITIVITY_LIST");

            DatumList fcuNoiseCurrentList = configData.ReadListDatum("fcuNoiseCurrentList");
            tolerancePwr_dB = configData.ReadDouble("powerTolerance_dB");
            targetPwrSig_dBm = configData.ReadDouble("targetPwrSig_dBm");
            targetPwrLoc_dBm = configData.ReadDouble("targetPwrLoc_dBm");
            spliteRate_SIG = configData.ReadDouble("splitRate_Sig");
            spliteRate_LOC = configData.ReadDouble("splitRate_Loc");

            //AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            //analyzerSetup_S21.startFrequency_GHz = configData.ReadDouble("s21StartFrequency_GHz");
            //analyzerSetup_S21.stopFrequency_GHz = configData.ReadDouble("s21StopFrequency_GHz");
            //analyzerSetup_S21.sweepPoints = (uint)configData.ReadSint32("sweepPoints");
            //analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            //analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            //analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            //analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            //analyzerSetup_S21.CalRegister = configData.ReadSint32("CalRegister");
            //analyzerSetup_S21.s21Scale = configData.ReadDouble("s21Scale");

            int s21CalRegister = configData.ReadSint32("CalRegister");

            DatumList guiMessage = new DatumList();
            guiMessage.AddOrUpdateSint32("progressBarValue", 5);
            engine.SendToGui(guiMessage);

            #endregion

            //Adjust VOA to set sig/loc input power
            double[] pdCurrentArray = null;
            double[] pdCurrentArray_180 = null;
            double currentRatio = 0;


            //to be modified for s21 investigation
            
            bool retryBW = false;
            int extraDelay6464Ms = 1;  //8000;
 

            DateTime ReadingTimeStamp = DateTime.Now;
            int iterationNb = 0;
            bool BW_OK = true;
            
            do
            {
                s21RealAndImaginaryTrace = new Trace();

                if (!engine.IsSimulation)
                {
                    if (LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
                    {
                        LightwaveComponentAnalyzer.RecallRegister(s21CalRegister);
                    }
                    else
                    {
                        Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)LightwaveComponentAnalyzer;
                        MS4046A.EmbedOrDeEmbedEnable = true;
                        MS4046A.ChannelCount = 1;
                        MS4046A.ActiveMeausurementChannel = 1;
                    }

                    LightwaveComponentAnalyzer.AutoScale();

                    #region set power level


                    // comment by yubo
                    //measureSetup_SIG.DelayLine.IsOnline = false;
                    //measureSetup_SIG.DelayLine.InstrumentChassis.IsOnline = false;
                    //measureSetup_LOC.DelayLine.IsOnline = false;
                    //measureSetup_LOC.DelayLine.InstrumentChassis.IsOnline = false;

                    //set signal path optical power
                    measureSetup_SIG.SetOpticalInputPower(engine, targetPwrSig_dBm, spliteRate_SIG, tolerancePwr_dB);

                    guiMessage.AddOrUpdateSint32("progressBarValue", 45);
                    engine.SendToGui(guiMessage);



                    //set local path optical power
                    measureSetup_LOC.SetOpticalInputPower(engine, targetPwrLoc_dBm, spliteRate_LOC, tolerancePwr_dB);
                    guiMessage.AddOrUpdateSint32("progressBarValue", 75);
                    engine.SendToGui(guiMessage);


                    #endregion

                    //if (targetPwrSig_dBm > -6)
                    //{
                    measureSetup_SIG.LockInAmplifier.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.A;
                    //}
                    //else
                    //{
                    //measureSetup_SIG.LockInAmplifier.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.I_10Mohm;
                    //}

                    //measureSetup_SIG.LockInAmplifier.AutoOffset(Inst_SR830.signalTypeEnum.X);
                    Thread.Sleep(500);
                    measureSetup_SIG.LockInAmplifier.ReferencePhase = 115; //Modified by Matt
                    measureSetup_SIG.LightwaveComponentAnalyzer.AutoScale();
                    Thread.Sleep(1000);

                    //do sweep ,get s21 real and maginary trace data
                    MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                    Thread.Sleep(1000);
                    if (is6464)
                    {
                        Thread.Sleep(extraDelay6464Ms);
                    }
                    engine.SendStatusMsg("wait for 1 second");
                    MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                    Thread.Sleep(2000);
                    
                    //                SetBestSensitivity();
//               LCAnayzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.HOLD; 

               s21RealAndImaginaryTrace1 = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);//af 22042013
//               LCAnayzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.CONT; 
//               s21RealAndImaginaryTrace1 = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);//af 22042013
//               Trace s21PhaseTrace = new Trace();
///               s21PhaseTrace = SparamAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace, 40);
              // SparamAnalysise.UnwrappingPhaseData(engine, s21PhaseTrace, out s21UnwrapPhaseTrace, 360);

                    if (targetPwrSig_dBm != double.NegativeInfinity && targetPwrLoc_dBm != double.NegativeInfinity)
                    {
                        //only do for dual input power
                        SaveTrace(@"C:\RawS21Plots\retries\" + ReadingTimeStamp.ToString("yyyyMMddHHmmss") + "_" + rfOutputPort + "_0deg_" + iterationNb + ".csv", rfOutputPort, s21RealAndImaginaryTrace1, retryBW);//debug only
                    }



                    //Check the currentRatio of 0 phase
                    pdCurrentArray = MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);

                    currentRatio = CaculatePdCurrentRatio(engine, pdCurrentArray, true);
                    // if (currentRatio > 1 && currentRatio < 10)
                    if (currentRatio < 10)
                    {
                        Thread.Sleep(1000);
                        pdCurrentArray = MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                        currentRatio = CaculatePdCurrentRatio(engine, pdCurrentArray, true);
                    }

                    if (SweepMode == "Sweep1" || targetPwrSig_dBm == double.NegativeInfinity || targetPwrLoc_dBm == double.NegativeInfinity)
                    {
                        s21RealAndImaginaryTrace = s21RealAndImaginaryTrace1;
                        pdCurrentArray_180 = pdCurrentArray;
                        s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace, is6464);
                        BW_OK = true;
                    }
                    else
                    {
                        //2nd Sweeping
                        measureSetup_SIG.LockInAmplifier.ReferencePhase = -65; //Modified by Matt
                        measureSetup_SIG.LightwaveComponentAnalyzer.AutoScale();
                        MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                        Thread.Sleep(1000);
                        engine.SendStatusMsg("wait for 1 second");
                        if (is6464)
                        {
                            Thread.Sleep(extraDelay6464Ms);
                        }
                        MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                        s21RealAndImaginaryTrace2 = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
                        SaveTrace(@"C:\RawS21Plots\retries\" + ReadingTimeStamp.ToString("yyyyMMddHHmmss") + "_" + rfOutputPort + "_180deg_" + iterationNb + ".csv", rfOutputPort, s21RealAndImaginaryTrace2, retryBW);//debug only
                        for (int i = 0; i < s21RealAndImaginaryTrace1.Count; i++)
                        {
                            TempPoint.X = s21RealAndImaginaryTrace1[i].X;
                            TempPoint.Y = s21RealAndImaginaryTrace2[i].Y - s21RealAndImaginaryTrace1[i].Y;
                            TempPoint.Z = s21RealAndImaginaryTrace2[i].Z - s21RealAndImaginaryTrace1[i].Z;
                            s21RealAndImaginaryTrace.Add(TempPoint);
                        }
                        pdCurrentArray_180 = MeasurePdCurrentSum(fcuNoiseCurrentList, is6464);
                        s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace, is6464);

                    //    Trace s21PhaseTrace = new Trace();
                    //    s21PhaseTrace = SparamAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace, 40);
                       

                        //calculate bw if fails then retry
                        AnalysisConfig_S21 analysisConfig_S21 = new AnalysisConfig_S21();
                        analysisConfig_S21.smoothingPoints = 5;
                        analysisConfig_S21.normalisationFrequency_GHz = 1;
                        analysisConfig_S21.bandWidthPoint_dB = -3;
                        analysisConfig_S21.doMask = false;
                        analysisConfig_S21.sweepPoints = 801;
                        SparamAnalysisResult analysis = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21Trace, analysisConfig_S21);
                        BW_OK = analysis.bandWidth_GHz > 35;

                        //add another condition requested by Matt 15.09.2017 if teh difference between two consecutive points is higher than 5dB then retry too (up to 35GHz). This ensure that the graph does not have glitches
                        for (int i = 0; i < s21Trace.GetXArray().Length; i++) { 
                        
                            if (s21Trace.GetXArray()[i] > 35) break; //this condition does not apply for above 35 GHz

                            BW_OK &= (Math.Abs(s21Trace.GetYArray()[i] - s21Trace.GetYArray()[i + 1]) < 5);
                           
                        }

                    }
                }
                else
                {
                    List<Trace> taceList = ReadXYData();
                    s21RealAndImaginaryTrace = taceList[0];

                    List<double> tempList = new List<double>();
                    for (int i = 1; i < 9; i++)
                    {
                        Random rd = new Random();
                        tempList.Add(i * rd.NextDouble());
                    }
                    pdCurrentArray = tempList.ToArray();
                    pdCurrentArray_180 = tempList.ToArray();
                    s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace, is6464);
                    BW_OK = true;

                }

                //           double currentRatio = CaculatePdCurrentRatio(engine, pdCurrentArray, true);
                //           if (currentRatio > 1 && currentRatio < 10)
                //           {
                //               pdCurrentArray = MeasurePdCurrentSum(fcuNoiseCurrentList);
                //               currentRatio = CaculatePdCurrentRatio(engine, pdCurrentArray, true);
                //           }

                guiMessage.AddOrUpdateSint32("progressBarValue", 100);
                engine.SendToGui(guiMessage);

            } 
         
            while (is6464 && retryBW && !BW_OK && iterationNb++ < 2); //do up to 3 retries on the s21 sweep for 6464

            //StringBuilder ErrMsg = new StringBuilder();
            //ushort ReadValue = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
            //if (ReadValue != 0x8A00)
            //{
            //    ErrMsg.Append("DAC Din control : Wrote 0x8A00 Read: " + ReadValue.ToString() + "\n");
            //    MessageBox.Show(ErrMsg.ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //}


            // Dave Smith - Update 1 start
            if (MeasurePkDetector)
            {
                StreamWriter SaveResults = null;

                string Filename = Path.Combine(Directory.GetCurrentDirectory() + @"\PkDetectorData", PeakDetectorFilename);
                if (File.Exists(Filename)) SaveResults = new StreamWriter(Filename, true);
                else
                {
                    SaveResults = new StreamWriter(Filename, false);
                    SaveResults.WriteLine("Freq(GHz),VNA Power, PeakDetector");
                }

                // we are going to put the analyser into CW mode and measure the Peak detector values at 5, 10 & 15GHz
                double MarkerAmp = 0, MarkerPhase = 0;
                
 
                for (int Freq = 1; Freq < 2; Freq += 1)
                    
                {
                    LightwaveComponentAnalyzer.SetCWSweepMode(Freq * 1E9, 0);
                    System.Threading.Thread.Sleep(500);                         // Need to review this
                    LightwaveComponentAnalyzer.SetMarkerMaximum();
                    LightwaveComponentAnalyzer.ReadActiveMarker(ref MarkerAmp, ref MarkerPhase);
                    measureSetup_SIG.SetOpticalInputPower(engine, targetPwrSig_dBm , spliteRate_SIG, tolerancePwr_dB);
                    measureSetup_LOC.SetOpticalInputPower(engine, targetPwrLoc_dBm , spliteRate_LOC, tolerancePwr_dB);
                    SaveResults.WriteLine("sig =" + (targetPwrSig_dBm) + "  lo =" + (targetPwrLoc_dBm ));

                    for (ushort gainval = 0x0000; gainval < 0xF000; gainval += 0x1000)
                    {                      
                        // Now measure Peak detector
                        instSPI.WriteDIO_SPI(System.Convert.ToUInt16(Convert.ToUInt16(PeakDetectorSPIChannel - 4)), gainval);
                        System.Threading.Thread.Sleep(500);   
                        LightwaveComponentAnalyzer.ReadActiveMarker(ref MarkerAmp, ref MarkerPhase);
                        instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);                    // Read it twice to ensure we get valid value.
                        UInt16 PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = Convert.ToUInt16(PeakSignal / 64);
                        UInt16 Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        SaveResults.WriteLine(Freq.ToString() + "," + MarkerAmp.ToString() + "," + PeakSignal.ToString() + "," + PeakDetectorSPIChannel.ToString() + "," + Gain.ToString("X"));
                    }
                    measureSetup_SIG.SetOpticalInputPower(engine, targetPwrSig_dBm + 3 , spliteRate_SIG, tolerancePwr_dB);
                    measureSetup_LOC.SetOpticalInputPower(engine, targetPwrLoc_dBm + 3 , spliteRate_LOC, tolerancePwr_dB);
                    SaveResults.WriteLine("increase sig =" + (targetPwrSig_dBm + 3) + " increase lo =" + (targetPwrLoc_dBm + 3));

                    for (ushort gainval = 0x0000; gainval < 0xF000; gainval += 0x1000)
                    {

                        // Now measure Peak detector
                        instSPI.WriteDIO_SPI(System.Convert.ToUInt16(Convert.ToUInt16(PeakDetectorSPIChannel - 4)), gainval);
                        System.Threading.Thread.Sleep(500);   
                        LightwaveComponentAnalyzer.ReadActiveMarker(ref MarkerAmp, ref MarkerPhase);
                        instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);                    // Read it twice to ensure we get valid value.
                        UInt16 PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = Convert.ToUInt16(PeakSignal / 64);
                        UInt16 Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        SaveResults.WriteLine(Freq.ToString() + "," + MarkerAmp.ToString() + "," + PeakSignal.ToString() + "," + PeakDetectorSPIChannel.ToString() + "," + Gain.ToString("X"));
                    }

                    measureSetup_SIG.SetOpticalInputPower(engine, targetPwrSig_dBm + 6, spliteRate_SIG, tolerancePwr_dB);
                    measureSetup_LOC.SetOpticalInputPower(engine, targetPwrLoc_dBm + 6, spliteRate_LOC, tolerancePwr_dB);
                    SaveResults.WriteLine("increase sig =" + (targetPwrSig_dBm + 6) + " increase lo =" + (targetPwrLoc_dBm +6) );

                    for (ushort gainval = 0x0000; gainval < 0xF000; gainval += 0x1000)
                    {

                        // Now measure Peak detector
                        instSPI.WriteDIO_SPI(System.Convert.ToUInt16(Convert.ToUInt16(PeakDetectorSPIChannel - 4)), gainval);
                        System.Threading.Thread.Sleep(500);   
                        LightwaveComponentAnalyzer.ReadActiveMarker(ref MarkerAmp, ref MarkerPhase);
                        instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);                    // Read it twice to ensure we get valid value.
                        UInt16 PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = instSPI.ReadDIO_SPI(PeakDetectorSPIChannel);
                        PeakSignal = Convert.ToUInt16(PeakSignal / 64);
                        UInt16 Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        Gain = instSPI.ReadDIO_SPI(Convert.ToUInt16(PeakDetectorSPIChannel - 4));
                        SaveResults.WriteLine(Freq.ToString() + "," + MarkerAmp.ToString() + "," + PeakSignal.ToString() + "," + PeakDetectorSPIChannel.ToString() + "," + Gain.ToString("X"));
                    }

        
                }

                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(Convert.ToUInt16(PeakDetectorSPIChannel - 4)), 0x8A00);

                SaveResults.Flush();
                SaveResults.Close();
                SaveResults = null;
            }


            // Dave Smith - Update 1 End


            DatumList listDatumInputPower = new DatumList();
            if (!engine.IsSimulation)
            {
                listDatumInputPower.AddDouble("sigInputPower_dBm", measureSetup_SIG.OPM_Ref.ReadPower() + spliteRate_SIG);
                listDatumInputPower.AddDouble("locInputPower_dBm", measureSetup_LOC.OPM_Ref.ReadPower() + spliteRate_LOC);
            }
            else
            {

                listDatumInputPower.AddDouble("sigInputPower_dBm", -99);
                listDatumInputPower.AddDouble("locInputPower_dBm", -66);
            }



            //            double currentRatio_180 = CaculatePdCurrentRatio(engine, pdCurrentArray_180, false);
            double currentRatio_180 = CaculatePdCurrentRatio(engine, pdCurrentArray, true);


            string pdreading = "";

            for (int i = 0; i < pdCurrentArray.Length; i++)
            {
                pdreading = pdreading + "  " + pdCurrentArray[i].ToString();
            }

            engine.SendStatusMsg("PD current " + pdreading);

            if (SweepMode == "Sweep2")
            {
                pdreading = "";
                for (int i = 0; i < pdCurrentArray_180.Length; i++)
                {
                    pdreading = pdreading + "  " + pdCurrentArray_180[i].ToString();
                }

                engine.SendStatusMsg("PD current_180 " + pdreading);
            }
            // return data

            DatumList returnData = new DatumList();
            returnData.AddReference("MagintudeRawData_EO", s21Trace);
            returnData.AddReference("XYRawData_EO", s21RealAndImaginaryTrace);

            for (int idx = 0; idx < s21RealAndImaginaryTrace.GetXArray().Length; idx++)
            {

                if (s21RealAndImaginaryTrace.GetXArray()[idx] > 1.0)
                {
                    double MagRaw1Ghz = 20 * Math.Log10(Math.Sqrt(Math.Pow(s21RealAndImaginaryTrace.GetYArray()[idx], 2) + Math.Pow(s21RealAndImaginaryTrace.GetZArray()[idx], 2)));
                    returnData.AddDouble("MagintudeRawData1Ghz", MagRaw1Ghz);
                    break;
                }

            }

            returnData.AddDouble("PdCurrentRatio", currentRatio);
            engine.SendStatusMsg("currentRatio " + currentRatio.ToString());

            if (SweepMode == "Sweep2")
            {
                returnData.AddReference("XYRawData_1", s21RealAndImaginaryTrace1);
                returnData.AddReference("XYRawData_2", s21RealAndImaginaryTrace2);
                returnData.AddDouble("PdCurrentRatio_180", currentRatio_180);
                engine.SendStatusMsg("currentRatio_180 " + currentRatio_180.ToString());
            }
            /*    else // Hack by john to allow single sweep.
            {
                returnData.AddReference("XYRawData_1", s21RealAndImaginaryTrace1);
                for (int i = 0; i < s21RealAndImaginaryTrace1.Count; i++)
                {
                    s21RealAndImaginaryTrace2.Add(0, 0, 0);

                }

                

                returnData.AddReference("XYRawData_2", s21RealAndImaginaryTrace2);
                returnData.AddDouble("PdCurrentRatio_180", currentRatio_180);
                engine.SendStatusMsg("currentRatio_180 " + currentRatio_180.ToString());
            }
            // End of hack :-)*/




            //returnData.AddDoubleArray("pdreading", pdCurrentArray);
            returnData.AddDoubleArray("pdCurrentArray", pdCurrentArray);
            returnData.AddListDatum("listDatumInputPower", listDatumInputPower);
            //LightwaveComponentAnalyzer.StartFrequency_GHz = 1;
            //LightwaveComponentAnalyzer.StopFrequency_GHz = 1;
            return returnData;
        }


        private void SaveTrace(string s21PlotFile, string rfPortName, Trace PlotData, bool retry)
        {

            double[] Freq = PlotData.GetXArray();
            double[] real = PlotData.GetYArray();
            double[] img = PlotData.GetZArray();
            double[] raw_mag = new double[img.Length];
            for (int i = 0; i < img.Length; i++)
            {

                double temp = Math.Sqrt(Math.Pow(real[i], 2) + Math.Pow(img[i], 2));
                raw_mag[i] = 20 * Math.Log10(temp);

            }


            Util_SavePlotData savePlotData = new Util_SavePlotData();

            savePlotData.AppendResultToFile(s21PlotFile, "Freq", Freq);
            savePlotData.AppendResultToFile(s21PlotFile, rfPortName + "_REAL", real);
            savePlotData.AppendResultToFile(s21PlotFile, rfPortName + "_IMG", img);
            savePlotData.AppendResultToFile(s21PlotFile, rfPortName + "_MAG", raw_mag);

            //save the retry setting true or false
            double [] retrySetting = new double [1]{retry? 1: 0};
            savePlotData.AppendResultToFile(s21PlotFile, "Retry", retrySetting);

        }

        private void SetBestSensitivity()
        {

            if ((rfOutputPort == "XIP" || rfOutputPort == "YIP") && (targetPwrSig_dBm != double.NegativeInfinity && targetPwrLoc_dBm != double.NegativeInfinity))
            {


                Inst_SR830.SensitivityEnum SensitivityLevel = Inst_SR830.SensitivityEnum._50mVperNA;
                double MaxRatio = double.MinValue;

                List<double> forDebugging = new List<double>();
                foreach (var level in SensitivityList)
                {

                    measureSetup_SIG.LockInAmplifier.Sensitivity = level;
                    Thread.Sleep(2000);

                    Inst_Ke24xx pos, neg;

                    if (rfOutputPort.Contains("XI"))
                    {
                        pos = measureSetup_SIG.PdBias.PdSource_XIpos;
                        neg = measureSetup_SIG.PdBias.PdSource_XIneg;
                    }

                    else if (rfOutputPort.Contains("YI"))
                    {
                        pos = measureSetup_SIG.PdBias.PdSource_YIpos;
                        neg = measureSetup_SIG.PdBias.PdSource_YIneg;

                    }
                    else if (rfOutputPort.Contains("XQ"))
                    {

                        pos = measureSetup_SIG.PdBias.PdSource_XQpos;
                        neg = measureSetup_SIG.PdBias.PdSource_XQneg;

                    }
                    else if (rfOutputPort.Contains("YQ"))
                    {

                        pos = measureSetup_SIG.PdBias.PdSource_YQpos;
                        neg = measureSetup_SIG.PdBias.PdSource_YQneg;

                    }
                    else
                    {

                        throw new Exception("unexpected channel");
                    }


                    double[] ratios = new double[40];  //for debugging of this code
                    double sumRatios = 0.0;

                    for (int ratioIdx = 0; ratioIdx < ratios.Length; ratioIdx++)
                    {


                        ratios[ratioIdx] = MeasureRatio(1, pos, neg);
                        forDebugging.Add(Math.Abs(10 * Math.Log10(ratios[ratioIdx])));

                        sumRatios += ratios[ratioIdx];
                        Thread.Sleep(100);
                    }

                    double AvgRatio = Math.Abs(10 * Math.Log10(sumRatios / ratios.Length));

                    if (AvgRatio > MaxRatio)
                    {

                        SensitivityLevel = level;
                        MaxRatio = AvgRatio;

                    }


                }

                measureSetup_SIG.LockInAmplifier.Sensitivity = SensitivityLevel;
                Thread.Sleep(3000);


            }



        }

        private double MeasureRatio(int numAvg, Inst_Ke24xx pos, Inst_Ke24xx neg)
        {

            double SumP = 0.0;
            double SumN = 0.0;

            for (int i = 0; i < numAvg; i++)
            {
                SumP += pos.CurrentActual_amp;
                SumN += neg.CurrentActual_amp;

            }

            double PhI_P = Math.Round(SumP / numAvg * 1000, 6);
            double PhI_N = Math.Round(SumN / numAvg * 1000, 6);

            return Math.Abs(PhI_P) / Math.Abs(PhI_N);

        }


        private double CaculatePdCurrentRatio(ITestEngine engine, double[] pdCurrentArray, bool isForward)
        {
            double currentRatio = 0.0;
            return 10;
            switch (rfOutputPort)
            {
                case "XIP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[0]) / Math.Abs(pdCurrentArray[1])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[0].ToString() + " / " + pdCurrentArray[1] + " ratio = " + currentRatio);
                    break;
                case "XIN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[1]) / Math.Abs(pdCurrentArray[0])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[1].ToString() + " / " + pdCurrentArray[0] + " ratio = " + currentRatio);
                    break;
                case "XQP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[2]) / Math.Abs(pdCurrentArray[3])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[2].ToString() + " / " + pdCurrentArray[3] + " ratio = " + currentRatio);
                    break;
                case "XQN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[3]) / Math.Abs(pdCurrentArray[2])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[3].ToString() + " / " + pdCurrentArray[2] + " ratio = " + currentRatio);
                    break;
                case "YIP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[4]) / Math.Abs(pdCurrentArray[5])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[4].ToString() + " / " + pdCurrentArray[5] + " ratio = " + currentRatio);
                    break;
                case "YIN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[5]) / Math.Abs(pdCurrentArray[4])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[5].ToString() + " / " + pdCurrentArray[4] + " ratio = " + currentRatio);
                    break;
                case "YQP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[6]) / Math.Abs(pdCurrentArray[7])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[6].ToString() + " / " + pdCurrentArray[7] + " ratio = " + currentRatio);
                    break;
                case "YQN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[7]) / Math.Abs(pdCurrentArray[6])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[7].ToString() + " / " + pdCurrentArray[6] + " ratio = " + currentRatio);
                    break;
                default:
                    engine.ErrorInModule("can not recognise rf output port name when calculte pd current ratio!");
                    break;
            }
            if (isForward == false)
            {
                currentRatio = 0 - currentRatio;
            }
            return currentRatio;
        }

        /// <summary>
        /// check phase lock whether stabile
        /// </summary>
        /// <param name="engine">reference ITestEngine</param>
        private void CheckPhaseLocked(ITestEngine engine)
        {
            int readPhaseTime = 0;
            do
            {
                List<double> phaseOutputList = new List<double>();
                for (int k = 0; k < 200; k++)
                {
                    double output = measureSetup_SIG.LockInAmplifier.ReadOutput(Inst_SR830.OutputTypeEnum.Phase);
                    phaseOutputList.Add(output);
                    Thread.Sleep(2);
                }


                phaseOutputList.Sort();
                double phaseChange_dge = Math.Abs((180 - Math.Abs(phaseOutputList[phaseOutputList.Count - 1])) - (180 - Math.Abs(phaseOutputList[0])));

                if (phaseChange_dge < 40)
                {
                    break;
                }
                Thread.Sleep(4000);

                readPhaseTime++;
            } while (readPhaseTime < 5);

            if (readPhaseTime >= 5)
            {
                engine.ShowContinueUserQuery("Please check the phase locked whether stable! and click continus");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fcuNoiseCurrentList"></param>
        /// <returns></returns>
        private double[] MeasurePdCurrentSum(DatumList fcuNoiseCurrentList, bool is6464)
        {
            List<double> pdCurrentList = new List<double>();

            if (is6464)
            {
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XIpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XQpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YIpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YQpos.CurrentActual_amp * 1000, 6));

            }
            else
            {
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XIpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XIneg.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XQpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XQneg.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YIpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YIneg.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YQpos.CurrentActual_amp * 1000, 6));
                pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YQneg.CurrentActual_amp * 1000, 6));
            }


            return pdCurrentList.ToArray();
        }

        #region private function

        public List<Trace> ReadXYData()
        {
            string fileName;
            fileName = Directory.GetCurrentDirectory() + "\\results\\Offline\\xyData.csv";

            CsvReader csvReader = new CsvReader();
            List<string[]> listXYData = csvReader.ReadFile(fileName);

            double[] freqArray = new double[listXYData.Count - 1];
            double[] realArray = new double[listXYData.Count - 1];
            double[] imaginaryArray = new double[listXYData.Count - 1];
            double[] magintudeArray = new double[listXYData.Count - 1];
            List<string> listContent = new List<string>();
            Trace s21RealAndImaginaryTrace = new Trace();
            Trace s21MagintudeTrace = new Trace();
            for (int i = 1; i < listXYData.Count; i++)
            {
                freqArray[i - 1] = double.Parse(listXYData[i][0]);
                imaginaryArray[i - 1] = double.Parse(listXYData[i][1]);
                realArray[i - 1] = double.Parse(listXYData[i][2]);
                double temp = Math.Sqrt(Math.Pow(realArray[i - 1], 2) + Math.Pow(imaginaryArray[i - 1], 2));
                double magintude_dB = 20 * Math.Log10(temp);
                magintudeArray[i - 1] = magintude_dB;

                s21RealAndImaginaryTrace.Add(freqArray[i - 1], realArray[i - 1], imaginaryArray[i - 1]);
                s21MagintudeTrace.Add(freqArray[i - 1], magintude_dB);
            }
            List<Trace> tracelist = new List<Trace>();
            tracelist.Add(s21RealAndImaginaryTrace);
            tracelist.Add(s21MagintudeTrace);

            return tracelist;
        }



        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureEOGui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        MeasureSetupManage measureSetup_SIG;
        MeasureSetupManage measureSetup_LOC;

        double targetPwrSig_dBm;
        double targetPwrLoc_dBm;
        double spliteRate_SIG;
        double spliteRate_LOC;
        double tolerancePwr_dB;

        Trace s21RealAndImaginaryTrace = new Trace();
        Trace s21RealAndImaginaryTrace1 = new Trace();
        Trace s21RealAndImaginaryTrace2 = new Trace();
        Point TempPoint = new Point();
        Trace s21Trace = new Trace();
        //bool maskPassFail_S21 = false;
        //bool qualTest;
        bool sprrTest = false;
        string rfOutputPort = null;
        #endregion

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_SplitterRatioCalGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_SplitterRatioCalGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_SplitterRatioCalGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        private void TM_CoRxSplitterRatioCalGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(GuiMsg.CautionOfLaserRqst))
            {
                //gbCaution.Visible = true;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(GuiMsg.CancelOflLaserCautionRqst))
            {
                //gbCaution.Visible = false;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(DatumString))
            {
                DatumString msg = payload as DatumString;

                if (msg.Name == "GuiTitle")
                {
                    lblTile.Text = msg.Value;
                    lblTile .Refresh();
                }
                else
                {
                    lblTestMsg.Text = msg.Value;
                    lblTestMsg .Refresh();
                }
            }
        }

        private void buttonContinues_Click(object sender, EventArgs e)
        {
            this.sendToWorker(new GuiMsg.CautionOfLaserRqst());
        }

      
    }
}

// [Copyright]
//
// Bookham [Coherent RX AC Test]
// Bookham.TestSolution.TestModules
//
// Mod_SplitterRatioCal.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Instruments;
using System.Threading;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Coherent RX AC Test, optical path splitter ratio calibration
    /// </summary>
    public class Mod_SplitterRatioCal : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiToFront();
            engine.GuiShow();

            double Sr_Min = configData.ReadDouble("SR_Min");
            double Sr_Max = configData.ReadDouble("SR_Max");
            double Voa_initAtt;
            double sourceOpcPower_dBm = configData.ReadDouble("LaserSourcePower_dBm");
            DatumString title = configData.GetDatumString("GuiTitle");
            string opticalPath = configData.ReadString("opticalPath");
            engine.SendToGui(title);

            //CoRxSourceInstrsChain OpcInstrs = (CoRxSourceInstrsChain)configData.ReadReference("OpcChainInstrs");
            MeasureSetupManage ChainSetupManage = (MeasureSetupManage)configData.ReadReference("ChainSetupManage");

            #region Get frequency information
            MeasureSetupManage.EnumFrequencySetMode freqSetMode = MeasureSetupManage.EnumFrequencySetMode.ByWavelenth;

            double[] CalFreqArray = new double[3];
            try
            {
                CalFreqArray = configData.ReadDoubleArray("CalFreqList");
                freqSetMode = MeasureSetupManage.EnumFrequencySetMode.ByFrequency;
            }
            catch
            { }

            try
            {
                CalFreqArray = configData.ReadDoubleArray("CalWavelengList");
                freqSetMode = MeasureSetupManage.EnumFrequencySetMode.ByWavelenth;
            }
            catch
            { }
            try
            {
                CalFreqArray = configData.ReadDoubleArray("CalChanList");
                freqSetMode = MeasureSetupManage.EnumFrequencySetMode.ByChan;
            }
            catch
            { }
            #endregion

            //if (freq_Start == double.NaN || freq_Stop == double.NaN || freq_Step == double.NaN)
            //{
            //    engine.ErrorInModule("Invalid cal freq setting!");
            //}

            // switch optical off and indicate operator to insert VOA ouput to OPM head
            if (!engine.IsSimulation)
            {
                ChainSetupManage.VOA.OutputEnabled = false;
            }
           
            bool isCalOk = false;

            engine.SendToGui(new DatumString("TestMsg",
                "Please Clean the " + opticalPath + " output fibre connector then plug into powermeter's input"));
            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
            engine.GuiUserAttention();
            engine.ReceiveFromGui();
            engine.GuiCancelUserAttention();
            engine.GuiHide();

            //OpcInstrs.LaserSource.Power_dBm = sourceOpcPower_dBm;
            if (!engine.IsSimulation)
            {
                ChainSetupManage.VOA.OutputEnabled = true;
                ChainSetupManage.SetOpticalInputPower(engine, -6, 9, 0.1);
            }

            

            double powerInput_dBm = ChainSetupManage.OPM_Mon.ReadPower();
            do
            {
                powerInput_dBm = ChainSetupManage.OPM_Mon.ReadPower();
                if (powerInput_dBm<-20)
                {
                    engine.ShowContinueUserQuery("please check if connect the right fibre!");
                }
            } while (powerInput_dBm < -20);


            ChainSetupManage.SplitterRatiosCalData.ClearCalData();
            Voa_initAtt = ChainSetupManage.VOA.Attenuation_dB;
            isCalOk = ChainSetupManage.MeasSplitterRatioByWavelen(CalFreqArray, Voa_initAtt, Sr_Max, Sr_Min);

            bool isCalDataSaveOK = false;
            if (isCalOk)
            {
                isCalDataSaveOK = ChainSetupManage.SplitterRatiosCalData.SaveSplitterRatioCalData();
            }
            engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
            engine.SendToGui(new DatumString("TestMsg",
                "Calibration completed, Please recover the optical ouput fibre connector"));
            // return data
            DatumList returnData = new DatumList();
            returnData.AddBool("IsSrCalOk", isCalOk);
            returnData.AddBool("IsSrCalSaveOk", isCalDataSaveOK);
            return returnData;
        }

        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(Mod_SplitterRatioCalGui)); }
        }

        #endregion
    }
}

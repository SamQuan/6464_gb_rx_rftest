// [Copyright]
//
// Bookham Test Engine
// TM_CoRxSplitterRatioCal
//
// Bookham.TestSolution.TestModules/TM_CoRxSplitterRatioCalGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_SplitterRatioCalGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTestMsg = new System.Windows.Forms.Label();
            this.lblTile = new System.Windows.Forms.Label();
            this.buttonContinues = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTestMsg
            // 
            this.lblTestMsg.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestMsg.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblTestMsg.Location = new System.Drawing.Point(14, 98);
            this.lblTestMsg.Name = "lblTestMsg";
            this.lblTestMsg.Size = new System.Drawing.Size(700, 95);
            this.lblTestMsg.TabIndex = 4;
            this.lblTestMsg.Text = "label1";
            // 
            // lblTile
            // 
            this.lblTile.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTile.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblTile.Location = new System.Drawing.Point(14, 9);
            this.lblTile.Name = "lblTile";
            this.lblTile.Size = new System.Drawing.Size(264, 36);
            this.lblTile.TabIndex = 5;
            this.lblTile.Text = "Tile";
            // 
            // buttonContinues
            // 
            this.buttonContinues.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonContinues.Location = new System.Drawing.Point(19, 244);
            this.buttonContinues.Name = "buttonContinues";
            this.buttonContinues.Size = new System.Drawing.Size(695, 78);
            this.buttonContinues.TabIndex = 6;
            this.buttonContinues.Text = "Continue";
            this.buttonContinues.UseVisualStyleBackColor = true;
            this.buttonContinues.Click += new System.EventHandler(this.buttonContinues_Click);
            // 
            // Mod_SplitterRatioCalGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonContinues);
            this.Controls.Add(this.lblTile);
            this.Controls.Add(this.lblTestMsg);
            this.Name = "Mod_SplitterRatioCalGui";
            this.Size = new System.Drawing.Size(749, 524);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.TM_CoRxSplitterRatioCalGui_MsgReceived);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTestMsg;
        private System.Windows.Forms.Label lblTile;
        private System.Windows.Forms.Button buttonContinues;
    }
}

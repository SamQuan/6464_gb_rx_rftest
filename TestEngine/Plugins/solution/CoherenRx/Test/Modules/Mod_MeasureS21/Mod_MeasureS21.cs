// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureS21.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;



namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureS21 : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Test or Calculate s21 bandwidth
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();
            bool is6464 = configData.ReadBool("is6464");
            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            if (configData.IsPresent("qualTest"))
            {
                qualTest = configData.ReadBool("qualTest");
            }
            

            #region init configure

            //AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            //analyzerSetup_S21.startFrequency_GHz = (double)configData.ReadDouble("s21StartFrequency_GHz");
            //analyzerSetup_S21.stopFrequency_GHz = (double)configData.ReadDouble("s21StopFrequency_GHz");
            //analyzerSetup_S21.sweepPoints = (uint)configData.ReadSint32("sweepPoints");
            //analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            //analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            //analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            //analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            //analyzerSetup_S21.CalRegister = configData.ReadSint32("CalRegister");
            //analyzerSetup_S21.s21Scale = configData.ReadDouble("s21Scale");

            AnalysisConfig_S21 analysisConfig_S21 = new AnalysisConfig_S21();
            analysisConfig_S21.smoothingPoints = configData.ReadSint32("s21smoothingPoints");
            analysisConfig_S21.normalisationFrequency_GHz = configData.ReadDouble("s21normalisationFrequency_GHz");
            analysisConfig_S21.bandWidthPoint_dB = configData.ReadDouble("s21bandWidthPoint_dB");
            analysisConfig_S21.doMask = false;
            analysisConfig_S21.sweepPoints = configData.ReadSint32("sweepPoints");

            int s21CalRegister = configData.ReadSint32("CalRegister");

            if (configData.IsPresent("doMask"))
            {
                analysisConfig_S21.doMask = configData.ReadBool("doMask");
                analysisConfig_S21.maskFitFile = configData.ReadString("maskFitFile");
            }
            #endregion

            SparamAnalysisResult s21AnaysiseResult = new SparamAnalysisResult();

            if (!configData.IsPresent("xyRawData"))
            {
                s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
            }
            else
            {
                s21RealAndImaginaryTrace = (Trace)configData.ReadReference("xyRawData");
            }

            s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace,is6464);

           
            engine.SendToGui(s21Trace);

            s21AnaysiseResult = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21Trace, analysisConfig_S21);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddReference("XYRawData_EO", s21RealAndImaginaryTrace);
            returnData.AddReference("MagintudeRowData", s21Trace);
            returnData.AddDouble("BandWidth_S21_GHz", s21AnaysiseResult.bandWidth_GHz);
            returnData.AddBool("S21MaskPassFail", s21AnaysiseResult.Sparam_MaskPassFail);
            returnData.AddReference("Plot_S21", s21AnaysiseResult.PlotData);
            return returnData;
        }


        #region private function

        #endregion


        public Type UserControl
        {
            get { return (typeof(Mod_MeasureS21Gui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s21RealAndImaginaryTrace = new Trace();
        Trace s21Trace = new Trace();
        //bool maskPassFail_S21 = false;
        bool qualTest;
        #endregion

        #endregion
    }
}

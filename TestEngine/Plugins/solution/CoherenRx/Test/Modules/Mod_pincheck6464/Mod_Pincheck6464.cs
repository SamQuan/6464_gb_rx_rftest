// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_Pincheck.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.Threading;
using System.Windows.Forms;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Power Up: Vpd-->Vcc-->Vgc (or other control inputs)
    /// Set signal and local input power to zero dB
    /// </summary>
    public class Mod_Pincheck6464 : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            if (!engine.IsSimulation)
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }

            #region init equipment

            Tia_X = (TiaInstrument)configData.ReadReference("Tia_X");
            Tia_Y = (TiaInstrument)configData.ReadReference("Tia_Y");

            pdBias = (PdBiasInstrument)configData.ReadReference("PdBias");
            //AuxPd = (AuxPhaseAlignInstrument)configData.ReadReference("AuxPd");

            WaveformGenerator = (Instr_Ag33120A)configData.ReadReference("WaveformGenerator");

            LockInAmplifier = (Inst_SR830)configData.ReadReference("LockInAmplifier");

            instSPI = (Instr_SPI)configData.ReadReference("InstSPI") ;

            sigMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            locMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");

            #endregion

            #region init configure

            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");
            double targetInputPower_Sig_dBm = configData.ReadDouble("targetInputPower_Sig_dBm");
            double targetInputPower_Loc_dBm = configData.ReadDouble("targetInputPower_Loc_dBm");

            vmcOfTiaX_V = configData.ReadDouble("vmcOfTiaX_V");
            vmcOfTiaY_V = configData.ReadDouble("vmcOfTiaY_V");
            vccOfTia_V = configData.ReadDouble("vccOfTia_V");
            vgcOfTiaX_V = configData.ReadDouble("vgcOfTiaX_V");
            vgcOfTiaY_V = configData.ReadDouble("vgcOfTiaY_V");
            pdBias_V = configData.ReadDouble("pdBais_V");
            bandWidthSetting = int.Parse(configData.ReadDouble("BandwidthSetting").ToString());
            MeasureSetupManage.TiaBandwidth tiaBandwidth =
                (MeasureSetupManage.TiaBandwidth)Enum.Parse(typeof(MeasureSetupManage.TiaBandwidth), bandWidthSetting.ToString());

            double pdComplanceCurrent_mA = configData.ReadDouble("pdComplanceCurrent_mA");
            double vccComplianceCurrent_mA = configData.ReadDouble("vccComplianceCurrent_mA");
            double vmcComplianceCurrent_mA = configData.ReadDouble("vmcComplianceCurrent_mA");
            double vgcComplianceCurrent_mA = configData.ReadDouble("vgcComplianceCurrent_mA");
            double bandWidthTiaComplianceCurrent_mA = configData.ReadDouble("bandWidthTiaComplianceCurrent_mA");

            double waveformFreq_Khz = configData.ReadDouble("waveformFreq_Khz");
            double waveformAmplitude_mV = configData.ReadDouble("waveformAmplitude_mV");
            double waveformDcOffset_V = configData.ReadDouble("waveformDcOffset_V");

            bool doBandwidthCtrl = configData.ReadBool("doBWControl");
            bool doMCCtrl = configData.ReadBool("doMCControl");


            #endregion
            string msgText = "";
            double CurrentTemp = 0;
            double limit = 5;

            instSPI.SPI_Reset(false);
            //Step1:set sig input power to targetInputPower_Sig_dBm(zero dB)
            sigMeasureSetupManage.SetOpticalInputPower(engine, 0, spliterRatio_Sig, powerTolerance_dB);

            locMeasureSetupManage.SetOpticalInputPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);

            engine.SendStatusMsg("set signal path input power to " + targetInputPower_Sig_dBm.ToString());
            sigMeasureSetupManage.SetOpticalInputPower(engine, targetInputPower_Sig_dBm, spliterRatio_Sig, powerTolerance_dB);
          
            inputPwrSig_dBm = sigMeasureSetupManage.OPM_Ref.ReadPower() + spliterRatio_Sig;

            //Step2:set local input power to targetInputPower_Loc_dBm(zero dB)

            engine.SendStatusMsg("set local path input power to " + targetInputPower_Loc_dBm.ToString());
            locMeasureSetupManage.SetOpticalInputPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);
            inputPwrLoc_dBm = locMeasureSetupManage.OPM_Ref.ReadPower() + spliterRatio_Loc;

            Tia_X.VccSupply.UseFrontTerminals = false;

            do
            {
                msgText = "Failed pin check on :- \r\n";

                //Step4:set vcc to 3.3v(150mA) moved to start
                engine.SendStatusMsg("set tia work status!");
                SetTiaVcc(vccOfTia_V, vccComplianceCurrent_mA);
                Thread.Sleep(1000);
                instSPI.SPI_Reset(true);
                System.Threading.Thread.Sleep(500);
                uint Temp = instSPI.ReadDIO_SPI(0);
                instSPI.WriteDIO_SPI(05, 01);  //reset TIA
                Temp = instSPI.ReadDIO_SPI(06); //read back CH_SHUTDOWN status
                instSPI.WriteDIO_SPI(06, 00);  // set CH_SHUTDOWN = 00,all channels active
                Temp = instSPI.ReadDIO_SPI(06); //read back CH_SHUTDOWN status
                instSPI.WriteDIO_SPI(07, 00);  //TIA Gain to be controlled in manual gain mode
                instSPI.WriteDIO_SPI(14, 03);  //
                instSPI.WriteDIO_SPI(15, 00);  //TIA PEAK_CONTROL in min peaking
                Temp = instSPI.ReadDIO_SPI(2);
                CurrentTemp = Math.Round(Tia_X.VccSupply.CurrentActual_amp * 1000, 6);
                if (CurrentTemp < 300 || CurrentTemp > 380)
                {
                    msgText = msgText + "TIAX VCC supply  " + CurrentTemp.ToString() + "A\r\n";
                }

            //    CurrentTemp = Math.Round(Tia_Y.VccSupply.CurrentActual_amp * 1000, 6);
            //    if (CurrentTemp < 130 || CurrentTemp > 180)
            //    {
            //        msgText = msgText + "TIAY VCC supply  " + CurrentTemp.ToString() + "A\r\n";
            //    }





                if (msgText == "Failed pin check on :- \r\n")

                {

                    //Step3:set pd to 5v(0.7mA) and check currents
                    pdBias.PdSource_XIpos.UseFrontTerminals = false;
                    pdBias.PdSource_XQpos.UseFrontTerminals = false;
                    pdBias.PdSource_YIpos.UseFrontTerminals = false;
                    pdBias.PdSource_YQpos.UseFrontTerminals = false;
  

                    engine.SendStatusMsg("set pd bias to " + pdBias_V.ToString() + "V");
                    SetPdBias(engine, pdBias_V, pdComplanceCurrent_mA);

                    //Measure XIP
                    CurrentTemp = Math.Round(pdBias.PdSource_XIpos.CurrentActual_amp * 1000000, 6);
                    engine.SendStatusMsg("XIP " + CurrentTemp.ToString() + "uA");
                    if (CurrentTemp < limit)
                    {
                        msgText = msgText + "Pd source XIP " + CurrentTemp.ToString() + "uA\r\n";
                    }
    
                    //Measure XQP
                    CurrentTemp = Math.Round(pdBias.PdSource_XQpos.CurrentActual_amp * 1000000, 6);
                    engine.SendStatusMsg("XQP " + CurrentTemp.ToString() + "uA");
                    if (CurrentTemp < limit)
                    {
                        msgText = msgText + "Pd source XQP " + CurrentTemp.ToString() + "uA\r\n";
                    }

                    //pdBias.PdSource_XIpos.OutputEnabled = false;
                   // pdBias.PdSource_XQpos.OutputEnabled = false;
                    //System.Threading.Thread.Sleep(1500);
                    // System.Threading.Thread.Sleep(1500);

                    CurrentTemp = Math.Round(Tia_X.VccSupply.CurrentActual_amp * 1000, 6);
                    engine.SendStatusMsg("X VCC " + CurrentTemp.ToString() + "mA");
                    if (CurrentTemp < 100)
                    {
                        msgText = msgText + "X VCC " + CurrentTemp.ToString() + "mA\r\n";
                    }


                    // switch to Y side
                    //pdBias.PdSource_XIpos.OutputEnabled = false;
                    //pdBias.PdSource_XQpos.OutputEnabled = false;
                    //System.Threading.Thread.Sleep(1500);
                   // pdBias.PdSource_XIpos.UseFrontTerminals = false;
                   // pdBias.PdSource_XQpos.UseFrontTerminals = false;
                   // pdBias.PdSource_XIpos.OutputEnabled = true;
                   // pdBias.PdSource_XQpos.OutputEnabled = true;
 
                    //Measure YIP
                    CurrentTemp = Math.Round(pdBias.PdSource_YIpos.CurrentActual_amp * 1000000, 6);
                    engine.SendStatusMsg("YIP " + CurrentTemp.ToString() + "uA");
                    if (CurrentTemp < limit)
                    {
                        msgText = msgText + "Pd source YIP " + CurrentTemp.ToString() + "uA\r\n";
                    }

                    //Measure YQP
                    CurrentTemp = Math.Round(pdBias.PdSource_YQpos.CurrentActual_amp * 1000000, 6);
                    engine.SendStatusMsg("YQP " + CurrentTemp.ToString() + "uA");
                    if (CurrentTemp < limit)
                    {
                        msgText = msgText + "Pd source YQP " + CurrentTemp.ToString() + "uA\r\n";
                    }
 
                }


                // turn everything off and set to front terminals
                pdBias.PdSource_XIpos.OutputEnabled = false;
                pdBias.PdSource_XQpos.OutputEnabled = false;

                pdBias.PdSource_YIpos.OutputEnabled = false;
                pdBias.PdSource_YQpos.OutputEnabled = false;

 
                Tia_X.VccSupply.OutputEnabled = false;
                //Tia_Y.VccSupply.OutputEnabled = false;

  


                if (msgText != "Failed pin check on :- \r\n")
                {
                    engine.SendStatusMsg("Pin check failed");
                    msgText = msgText + "Do you want to retry pin check ?";
                    DialogResult result1 = MessageBox.Show(new Form() { TopMost = true }, msgText, "Failed pin check", MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        msgText = "";
                    }
                    else
                    {
                        msgText = "Failed pin check on :- \r\n";
                        engine.SendStatusMsg("Pin check aborted");
                    }


                }


            } while (msgText != "Failed pin check on :- \r\n");

    
            Tia_X.VccSupply.OutputEnabled = false;
            System.Threading.Thread.Sleep(500);
            //Tia_Y.VccSupply.OutputEnabled = false;


            //Measure X side
            double VccTiaX_BW = 0;
            //Step6:measure icc of tia

            double iccTiaX_Amp = 0;
            //measrue igc 
            double igcTiaX_Amp = 0;

            //Measure Y side

            double iccTiaY_Amp = 0;

            double igcTiaY_Amp = 0;


            //measure imc 
            double imcTiaX_Amp = 0;
            double imcTiaY_Amp = 0;

            //get lockin amplifier sensitivity
            double sensitivity_mVpp = 0.0;


            //get lockin amplifier constant
            double timeConstant_ms = 0.0;


            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("actualInputPower_Sig", inputPwrSig_dBm);
            returnData.AddDouble("actualInputPower_Loc", inputPwrLoc_dBm);
            returnData.AddDouble("iccTia_XI", iccTiaX_Amp);
            returnData.AddDouble("iccTia_YI", iccTiaY_Amp);
            returnData.AddDouble("iccTia_XQ", iccTiaX_Amp);
            returnData.AddDouble("iccTia_YQ", iccTiaY_Amp);

            returnData.AddDouble("iccTia_X", iccTiaX_Amp);
            returnData.AddDouble("iccTia_Y", iccTiaY_Amp);

            returnData.AddDouble("igcTia_XI", igcTiaX_Amp);
            returnData.AddDouble("igcTia_XQ", igcTiaX_Amp);
            returnData.AddDouble("igcTia_YI", igcTiaY_Amp);
            returnData.AddDouble("igcTia_YQ", igcTiaY_Amp);

            returnData.AddDouble("imcTia_X", imcTiaX_Amp);
            returnData.AddDouble("imcTia_Y", imcTiaY_Amp);

            returnData.AddDouble("TIME_SENSITIVITY_LIA_X", timeConstant_ms);
            returnData.AddDouble("TIME_SENSITIVITY_LIA_Y", timeConstant_ms);
            returnData.AddDouble("VOLTAGE_SENSITIVITY_LIA_X", sensitivity_mVpp);
            returnData.AddDouble("VOLTAGE_SENSITIVITY_LIA_Y", sensitivity_mVpp);

            return returnData;
        }


        #region private function

        /// <summary>
        /// set pd bias volt
        /// </summary>
        /// <param name="voltSet_V">pd bias volt</param>
        /// <param name="currentCompliance_mA">pd bias compliance current</param>
        private void SetPdBias(ITestEngine engine, double voltSet_V, double currentCompliance_mA)
        {
            pdBias.PdSource_XIpos.OutputEnabled = true;
            pdBias.PdSource_XQpos.OutputEnabled = true;
            pdBias.PdSource_YIpos.OutputEnabled = true;
            pdBias.PdSource_YQpos.OutputEnabled = true;

            pdBias.PdSource_XIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_XQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
  
            if (!pdBias.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                int counts = (int)(voltSet_V / 0.2);
                for (int i = 1; i <= counts; i++)
                {
                    double voltageSet_V = 0.2 * i;
                    pdBias.PdSource_XIpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_XQpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_YIpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_YQpos.VoltageSetPoint_Volt = voltageSet_V;

                    if (i == 1)
                    {
                        List<double> voltSensList_V = new List<double>();
                        voltSensList_V.Add(pdBias.PdSource_XIpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_XQpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_YIpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_YQpos.VoltageActual_Volt);
                        foreach (double volt in voltSensList_V)
                        {
                            if (Math.Abs(volt - voltageSet_V) > 0.1)
                            {
                                pdBias.PdSource_XIpos.OutputEnabled = false;
                                pdBias.PdSource_XQpos.OutputEnabled = false;
                                pdBias.PdSource_YIpos.OutputEnabled = false;
                                pdBias.PdSource_YQpos.OutputEnabled = false;

                                engine.ErrorInModule("pd bias error,please check whether FCU2 pd bias channels or device demaged!");
                            }
                        }
                    }
                    Thread.Sleep(10);
                }
            }
            pdBias.PdSource_XIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_XQpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YQpos.VoltageSetPoint_Volt = voltSet_V;

            double CurrentTemp = pdBias.PdSource_XIpos.CurrentActual_amp;  // Let autoscale work 
            CurrentTemp = pdBias.PdSource_XQpos.CurrentActual_amp;
            CurrentTemp = pdBias.PdSource_YIpos.CurrentActual_amp;
            CurrentTemp = pdBias.PdSource_YQpos.CurrentActual_amp;

        }

        /// <summary>
        /// set tia vcc
        /// </summary>
        /// <param name="vcc_V">vcc volt in V</param>
        /// <param name="vccComplianceCurrent_mA">vcc compliance current in mA</param>
        private void SetTiaVcc(double vcc_V, double vccComplianceCurrent_mA)
        {


            Tia_X.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            Tia_X.VccSupply.VoltageSetPoint_Volt = vcc_V;
            Tia_X.VccSupply.OutputEnabled = true;
            //Tia_Y.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            //Tia_Y.VccSupply.VoltageSetPoint_Volt = vcc_V;
            //Tia_Y.VccSupply.OutputEnabled = true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tia"></param>
        /// <param name="vgc_V"></param>
        /// <param name="vgcComplianceCurrent_mA"></param>
        private void SetTiaVgc(TiaInstrument Tia, double vgc_V, double vgcComplianceCurrent_mA)
        {
            if (Tia.Vgc != null)
            {
                Tia.Vgc.OutputEnabled = true;
                Tia.Vgc.CurrentComplianceSetPoint_Amp = vgcComplianceCurrent_mA / 1000.0;
                Tia.Vgc.VoltageSetPoint_Volt = vgc_V;
            }
        }

        /// <summary>
        /// set tia module control status
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="Tia"> tia reference</param>
        /// <param name="vmc_V">mode control volt</param>
        /// <param name="complianceCurrent_mA">compliance current of mode control</param>
        private void SetTiaModeControl(ITestEngine engine, TiaInstrument Tia, double vmc_V, double complianceCurrent_mA)
        {
            if (Tia.ModeCtrlOfGC != null)
            {
                Tia.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
                Tia.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

                Tia.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;
                Tia.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;

                Tia.ModeCtrlOfGC.VoltageSetPoint_Volt = vmc_V;
                Tia.ModeCtrlOfGC.VoltageSetPoint_Volt = vmc_V;
            }
            else
            {
                //engine.ErrorInModule("tia is null,object not instantion! ");
            }
        }





        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_PincheckGui)); }
        }

        #region private variable
        TiaInstrument Tia_X;
        TiaInstrument Tia_Y;
        
        PdBiasInstrument pdBias;
        //AuxPhaseAlignInstrument AuxPd;

        Instr_Ag33120A WaveformGenerator;

        Inst_SR830 LockInAmplifier;

        Instr_SPI instSPI;
        
        MeasureSetupManage sigMeasureSetupManage;
        MeasureSetupManage locMeasureSetupManage;

        const double nullPower_dB = double.NegativeInfinity;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double powerTolerance_dB;
        double vccOfTia_V;
        double vmcOfTiaX_V;
        double vmcOfTiaY_V;
        double vgcOfTiaX_V;
        double vgcOfTiaY_V;


        int bandWidthSetting;
        double pdBias_V;
        double inputPwrSig_dBm;
        double inputPwrLoc_dBm;

        //        bool bMCCtrl;
        //        bool bBWCtrl;

        #endregion

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureTiaBandWidthControl.cs
//
// Author: sam.quan 2011.8
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_TiaVgcAndBandwidthSweep : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// do tia bandwidth setting sweeping and vgc control voltage setting sweeping,measure s21,calucate bandwith 
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();

            TiaInstrument Tia_I = (TiaInstrument)configData.ReadReference("Tia_I");
            TiaInstrument Tia_Q = (TiaInstrument)configData.ReadReference("Tia_Q");

            Instr_SPI instSPI;


            sigMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            locMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");
            instSPI = (Instr_SPI)configData.ReadReference("InstSPI");
            LightwaveComponentAnalyzer = sigMeasureSetupManage.LightwaveComponentAnalyzer;
            DatumList fcuNoiseCurrentList = configData.ReadListDatum("fcuNoiseCurrentList");
                        
            #region init configure
            string serialNumber = configData.ReadString("DutSerialNum");

            double targetPwrSig_dBm = configData.ReadDouble("targetPwrSig_dBm");
            double targetPwrLoc_dBm = configData.ReadDouble("targetPwrLoc_dBm");
            double tolerancePower_dB = configData.ReadDouble("powerTolerance_dB");

            double splitterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            double splitterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            bool is6464 = configData.ReadBool("is6464");
            //add bandwith setting for VGC sweep
         //   int BW_OVER_VGC = configData.ReadSint32("BW_OVER_VGC");
            int BW_OVER_VGC = 2;
            //qualTest = configData.ReadBool("qualTest");
            modeCtlOfTia_V = configData.ReadDouble("vmcOfTia_V");
            vccOfTia_V = configData.ReadDouble("vccOfTia_V");
            vgcOfTia_V = configData.ReadDouble("vgcOfTia_V");
            double tiaComplianceCurrent_mA = 2.0;
            double tiaCtrlVolt_V = vccOfTia_V;

            int s21CalRegister = configData.ReadSint32("CalRegister");

            AnalysisConfig_S21 analysisConfig_S21 = new AnalysisConfig_S21();
            analysisConfig_S21.smoothingPoints = (int)configData.ReadDouble("s21smoothingPoints");
            analysisConfig_S21.normalisationFrequency_GHz = configData.ReadDouble("s21NormalisationFrequency_GHz");
            analysisConfig_S21.bandWidthPoint_dB = configData.ReadDouble("s21bandWidthPoint_dB");
            analysisConfig_S21.doMask = false;
            analysisConfig_S21.sweepPoints = configData.ReadSint32("sweepPoints");

            //analysisConfig_S21.doMask = configData.ReadBool("doMask");
            
            List<double> listVgcCtrl = new List<double>();
            listVgcCtrl.Add(configData.ReadDouble("vgcSweepVoltage1"));
            listVgcCtrl.Add(configData.ReadDouble("vgcSweepVoltage2"));
            listVgcCtrl.Add(configData.ReadDouble("vgcSweepVoltage3"));
            listVgcCtrl.Add(configData.ReadDouble("vgcSweepVoltage4"));
            listVgcCtrl.Add(configData.ReadDouble("vgcSweepVoltage5"));

            List<string> listlockvgc = new List<string>();
            listlockvgc.Add(configData.ReadString("Lockvgc1"));
            listlockvgc.Add(configData.ReadString("Lockvgc2"));
            listlockvgc.Add(configData.ReadString("Lockvgc3"));
            listlockvgc.Add(configData.ReadString("Lockvgc4"));
            listlockvgc.Add(configData.ReadString("Lockvgc5"));

            List<int> listBwSetting = new List<int>();

            listBwSetting.Add(configData.ReadSint32("bwSettingM4"));
            listBwSetting.Add(0);
            listBwSetting.Add(configData.ReadSint32("bwSettingP4"));

            string vgcSweepRfPort = configData.ReadString("vgcSweepRfPort");
            string bandwidthSweepRfPort = configData.ReadString("bandwidthSweepRfPort");
            
            bool doBandwidthCtrl = configData.ReadBool("doBWControl");
            bool doMCCtrl = configData.ReadBool("doMCControl");
 
            #endregion

            //creat file name save sweep data
            string fileVgcSweep = Util_GenerateFileName.GenWithTimestamp("results", "vgcSweep_", serialNumber + "_" + vgcSweepRfPort, "csv");
            fileVgcSweep = Directory.GetCurrentDirectory() + "\\" + fileVgcSweep;

            string plotVgcSweep = Util_GenerateFileName.GenWithTimestamp("results", "vgcPlot_", serialNumber + "_" + vgcSweepRfPort, "csv");
            plotVgcSweep = Directory.GetCurrentDirectory() + "\\" + plotVgcSweep;
            
            string fileBandwidthSweep = Util_GenerateFileName.GenWithTimestamp("results", "bandwidthSweep_", serialNumber + "_" + bandwidthSweepRfPort, "csv");
            fileBandwidthSweep = Directory.GetCurrentDirectory() + "\\" + fileBandwidthSweep;

            string plotBandwidthSweep = Util_GenerateFileName.GenWithTimestamp("results", "bandwidthPlot_", serialNumber + "_" + bandwidthSweepRfPort, "csv");
            plotBandwidthSweep = Directory.GetCurrentDirectory() + "\\" + plotBandwidthSweep;


            #region tia vgc control
            //set  Signal = -3dB and Local = 0dB
            sigMeasureSetupManage.SetOpticalInputPower(engine, targetPwrSig_dBm, splitterRatio_Sig, tolerancePower_dB);
            locMeasureSetupManage.SetOpticalInputPower(engine, targetPwrLoc_dBm, splitterRatio_Loc, tolerancePower_dB);

            System.Threading.Thread.Sleep(8000);
            
            

            //set to maun GC mode
            if (doMCCtrl)
            {
                Tia_I.ModeCtrlOfGC.OutputEnabled = true;
                Tia_I.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
                Tia_I.ModeCtrlOfGC.VoltageSetPoint_Volt = 0;
                Tia_Q.ModeCtrlOfGC.OutputEnabled = true;
                Tia_Q.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
                Tia_Q.ModeCtrlOfGC.VoltageSetPoint_Volt = 0;
            }

            //set gc compliance current
            //Tia_I.Vgc.OutputEnabled = true;
            //Tia_I.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
            //Tia_Q.Vgc.OutputEnabled = true;
            //Tia_Q.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;

            //do vgc sweep

            engine.SendStatusMsg("Do VGC sweep!");
            // set bandwith
            MeasureSetupManage.TiaBandwidth tiaBandwidth_vgcsweep =
               (MeasureSetupManage.TiaBandwidth)Enum.Parse(typeof(MeasureSetupManage.TiaBandwidth),  BW_OVER_VGC.ToString());
            engine.SendStatusMsg("set bandwidth to " + BW_OVER_VGC.ToString());
            sigMeasureSetupManage.SetTiaBandWidth(Tia_I, tiaBandwidth_vgcsweep, tiaComplianceCurrent_mA, tiaCtrlVolt_V);
            sigMeasureSetupManage.SetTiaBandWidth(Tia_Q, tiaBandwidth_vgcsweep, tiaComplianceCurrent_mA, tiaCtrlVolt_V);
            Inst_SR830 LockInAmplifier;
            LockInAmplifier = (Inst_SR830)configData.ReadReference("LockInAmplifier");
            
           

            for (int i = 0; i < listVgcCtrl.Count; i++)
            {
                double Vgc_V = listVgcCtrl[i];

                //change vgc control voltage
                //Tia_I.Vgc.VoltageSetPoint_Volt = Vgc_V;
                //Tia_Q.Vgc.VoltageSetPoint_Volt = Vgc_V;

                double igcI_mA = 0;//Tia_I.Vgc.CurrentActual_amp * 1000;
                double igcQ_mA = 0;// Tia_Q.Vgc.CurrentActual_amp * 1000;

                engine.SendStatusMsg("set vgc to " + Vgc_V.ToString() + "_V");
                
                if (is6464)
                {
                    ushort Gain = 0x8A00;
                    if (Vgc_V == 0.5)
                    {
                        Gain = 0x3000;
                    }
                    else if (Vgc_V == 1)            
                    {
                        Gain = 0x7000;
                    }
                    else if (Vgc_V == 1.5)
                    {
                        Gain = 0xA000;
                    }
                    else if (Vgc_V == 2.0)
                    {
                        Gain = 0xD000;
                    }
                    else if (Vgc_V == 2.5)
                    {
                        Gain = 0xFF00;
                    }
                    else
                    {
                        Gain = 0x8A00;
                    }

                    for (uint offset = 0; offset < 385; offset += 128)                          // Writes to all 4 channels
                    {
                         instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), Gain);    // DAC DIN Control sets maximium gain
                    }

                }
                //sigMeasureSetupManage.MeasurePdCurrentSum(engine);
                LightwaveComponentAnalyzer.AutoScale();
                switch (listlockvgc[i])
                {

                    case "_10uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._10uVperPA;
                        break;
                    case "_20uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._20uVperPA;
                        break;
                    case "_50uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._50uVperPA;
                        break;
                    case "_100uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._100uVperPA;
                        break;
                    case "_200uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._200uVperPA;
                        break;
                    case "_500uVperPA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._500uVperPA;
                        break;
                    case "_1mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._1mVperNA;
                        break;
                    case "_2mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._2mVperNA;
                        break;
                    case "_5mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._5mVperNA;
                        break;
                    case "_10mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._10mVperNA;
                        break;
                    case "_20mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._20mVperNA;
                        break;
                    case "_50mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._50mVperNA;
                        break;
                    case "_100mVperNA":
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._100mVperNA;
                        break;
                    default:
                        LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._10mVperNA;
                        break;
                }

                //wait for stabile
                System.Threading.Thread.Sleep(12000);
                engine.SendStatusMsg("delay 12s finished");
                LightwaveComponentAnalyzer.AutoScale();
                //get X&Y data
                s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
                System.Threading.Thread.Sleep(1500);
                s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace,is6464);

                //calculate s21 bandwidth
                s21AnaysiseResult = new SparamAnalysisResult();
                s21AnaysiseResult = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21Trace, analysisConfig_S21);
                if (s21AnaysiseResult.bandWidth_GHz <20 && i > 1 )
                {
                    System.Threading.Thread.Sleep(12000);
                    engine.SendStatusMsg("bandwidth: "+ s21AnaysiseResult.bandWidth_GHz.ToString() + "Retry S21");
                    s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
                    System.Threading.Thread.Sleep(1500);
                    s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace,is6464);

                    //calculate s21 bandwidth
                    s21AnaysiseResult = new SparamAnalysisResult();
                    s21AnaysiseResult = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21Trace, analysisConfig_S21);
                    engine.SendStatusMsg("bandwidth: " + s21AnaysiseResult.bandWidth_GHz.ToString() );
                }

                listBwOfVgcSweep.Add(s21AnaysiseResult.bandWidth_GHz);

                //updata GUI info.
                engine.SendToGui("vgc:" + Vgc_V.ToString() + "v"+", bandwidth: "+ s21AnaysiseResult.bandWidth_GHz.ToString());
                engine.SendToGui(s21Trace);

                //save x&y data and plot
                double[] freq_GHz = s21Trace.GetXArray();

                if (i == 0)
                {
                    bSaveFreqArr = true;
                }
                else
                {
                    bSaveFreqArr = false;
                }
                SaveS21TestData(fileVgcSweep, s21RealAndImaginaryTrace, s21AnaysiseResult, Vgc_V,bSaveFreqArr);
                SavePlotData(plotVgcSweep, s21RealAndImaginaryTrace, s21AnaysiseResult, Vgc_V,bSaveFreqArr);
            }

            #endregion 

            //set to manu GC control
            if (doMCCtrl)
            {
                Tia_I.ModeCtrlOfGC.OutputEnabled = true;
                Tia_I.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
                Tia_I.ModeCtrlOfGC.VoltageSetPoint_Volt = modeCtlOfTia_V;
                Tia_Q.ModeCtrlOfGC.OutputEnabled = true;
                Tia_Q.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
                Tia_Q.ModeCtrlOfGC.VoltageSetPoint_Volt = modeCtlOfTia_V;
            }
            //set tia to vgcOfTia_V
            //Tia_I.Vgc.OutputEnabled = true;
            //Tia_I.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000;
            //Tia_I.Vgc.VoltageSetPoint_Volt = vgcOfTia_V;
            //Tia_Q.Vgc.OutputEnabled = true;
            //Tia_Q.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000;
            //Tia_Q.Vgc.VoltageSetPoint_Volt = vgcOfTia_V;

            #region tia bandwidth control
            //sweep 
            if (doBandwidthCtrl)
            {

                for (int i = 0; i < listBwSetting.Count; i++)
                {
                    int bwSetting = listBwSetting[i];
                    MeasureSetupManage.TiaBandwidth tiaBandwidth =
                (MeasureSetupManage.TiaBandwidth)Enum.Parse(typeof(MeasureSetupManage.TiaBandwidth), bwSetting.ToString());
                    engine.SendStatusMsg("set bandwidth to " + bwSetting.ToString());
                    //sigMeasureSetupManage.SetTiaBandWidth(Tia_I, tiaBandwidth, tiaComplianceCurrent_mA, tiaCtrlVolt_V);
                    //sigMeasureSetupManage.SetTiaBandWidth(Tia_Q, tiaBandwidth, tiaComplianceCurrent_mA, tiaCtrlVolt_V);

                    //wait for stabile
                    System.Threading.Thread.Sleep(2000);

                    //get x&y data
                    s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, LightwaveComponentAnalyzer, s21CalRegister);
                    s21Trace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace,is6464);

                    //calculate bandwidth
                    s21AnaysiseResult = new SparamAnalysisResult();
                    s21AnaysiseResult = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21Trace, analysisConfig_S21);

                    listBwOfBwSweep.Add(s21AnaysiseResult.bandWidth_GHz);

                    //updata GUI info.
                    engine.SendToGui("bwSetting:" + bwSetting.ToString() + ", bandwidth: " + s21AnaysiseResult.bandWidth_GHz.ToString());
                    engine.SendToGui(s21Trace);

                    //save sweep data and plot
                    if (i == 0)
                    {
                        bSaveFreqArr = true;
                    }
                    else
                    {
                        bSaveFreqArr = false;
                    }
                    SaveS21TestData(fileBandwidthSweep, s21RealAndImaginaryTrace, s21AnaysiseResult, (double)bwSetting, bSaveFreqArr);
                    SavePlotData(plotBandwidthSweep, s21RealAndImaginaryTrace, s21AnaysiseResult, (double)bwSetting, bSaveFreqArr);
                }

                //Tia_I.BandWidth_H.OutputEnabled = false;
                //Tia_I.BandWidth_L.OutputEnabled = false;
                //Tia_Q.BandWidth_H.OutputEnabled = false;
                //Tia_Q.BandWidth_L.OutputEnabled = false;
                
            }
            #endregion

            // return data

            #region add returnData

            DatumList returnData = new DatumList();

            for (int i = 0; i < listBwOfVgcSweep.Count; i++)
            {
                int temp = i + 1;
                if (vgcSweepRfPort.Contains("X"))
                    returnData.AddDouble("S21_CH_1_VGC_" + temp.ToString(), listBwOfVgcSweep[i]);
                else
                    returnData.AddDouble("S21_CH_2_VGC_" + temp.ToString(), listBwOfVgcSweep[i]);
            }

            if (vgcSweepRfPort.Contains("X"))
            {
                returnData.AddFileLink("CH_1_VGC_FILE", fileVgcSweep);
                returnData.AddFileLink("PLOT_CH_1_VGC", plotVgcSweep);
                returnData.AddString("TEST_VGC_CH_1", vgcSweepRfPort);
            }
            else
            {
                returnData.AddFileLink("CH_2_VGC_FILE", fileVgcSweep);
                returnData.AddFileLink("PLOT_CH_2_VGC", plotVgcSweep);
                returnData.AddString("TEST_VGC_CH_2", vgcSweepRfPort);
            }

            if (doBandwidthCtrl)
            {
                if (bandwidthSweepRfPort.Contains("X"))
                {
                    returnData.AddDouble("S21_CH_1_BW_M4", listBwOfBwSweep[0]);
                    returnData.AddDouble("S21_CH_1_BW_P4", listBwOfBwSweep[2]);
                    returnData.AddFileLink("CH_1_BW_FILE", fileBandwidthSweep);
                    returnData.AddFileLink("PLOT_CH_1_BW", plotBandwidthSweep);
                    returnData.AddString("TEST_BW_CH_1", bandwidthSweepRfPort);
                }
                else
                {
                    returnData.AddDouble("S21_CH_2_BW_M4", listBwOfBwSweep[0]);
                    returnData.AddDouble("S21_CH_2_BW_P4", listBwOfBwSweep[2]);
                    returnData.AddFileLink("CH_2_BW_FILE", fileBandwidthSweep);
                    returnData.AddFileLink("PLOT_CH_2_BW", plotBandwidthSweep);
                    returnData.AddString("TEST_BW_CH_2", bandwidthSweepRfPort);
                }
            }
            #endregion

            return returnData;
        }


        #region private function

       /// <summary>
       /// save s21 sweep test data to .csv
       /// </summary>
       /// <param name="file">file name</param>
        /// <param name="realAndImaginaryTrace">Trace include real and imaginary data</param>
        /// <param name="s21Result">s21 sweep data analysed result</param>
       /// <param name="testCondition">if vgc sweep testCondition is vgc control voltage,
       /// if bw sweep,test condition is bw control setting</param>
       /// <param name="times">sweep times</param>
        private void SaveS21TestData(string file, Trace realAndImaginaryTrace, SparamAnalysisResult s21Result, double testCondition,bool bSaveSweepFreq)
        {
            double[] freqArray = realAndImaginaryTrace.GetXArray();
            double[] realArray = realAndImaginaryTrace.GetYArray();
            double[] ImaginarrayArray = realAndImaginaryTrace.GetZArray();

            string[] PlotDataArray = new string[freqArray.Length];
            string plotHead;
            if (file.Contains("bandwidth"))
            {

                plotHead = "bwSetting_" + testCondition.ToString() + ",imaginary_" + testCondition.ToString() + ",real_" + testCondition.ToString() +
                    ",Raw S21_dB_" + testCondition.ToString() + ",Normalized S21_dB_" + testCondition.ToString();
            }
            else
            {
                plotHead = "vgc_V_" + testCondition.ToString() + ",imaginary_" + testCondition.ToString() + ",real_" + testCondition.ToString() +
                    ",Raw S21_dB_" + testCondition.ToString() + ",Normalized S21_dB_" + testCondition.ToString();
            }
            for (int i = 0; i < PlotDataArray.Length; i++)
            {
                if (i == 0)
                {
                    PlotDataArray[i] = testCondition.ToString() + "," + ImaginarrayArray[i].ToString() + "," +
                        realArray[i].ToString() + "," + s21Result.PlotData[i + 1].ToString();
                }
                else
                {
                    PlotDataArray[i] = " ," + ImaginarrayArray[i].ToString() + "," +
                        realArray[i].ToString() + "," + s21Result.PlotData[i + 1].ToString();
                }
            }
            Util_SavePlotData savePlotData = new Util_SavePlotData();
            if (bSaveSweepFreq)
            {
                savePlotData.AppendResultToFile(file, "freq_GHz", freqArray);
            }
            savePlotData.AppendResultToFile(file, plotHead, PlotDataArray);
        }

        /// <summary>
        /// Save plot data to .csv file
        /// </summary>
        /// <param name="PlotFile">plot file name</param>
        /// <param name="realAndImaginaryTrace">Trace include real and imaginary data</param>
        /// <param name="s21Result">s21 sweep data analysed result</param>
        /// <param name="times">sweep times</param>
        private void SavePlotData(string PlotFile, Trace realAndImaginaryTrace, SparamAnalysisResult s21Result, double testCondition, bool bSaveSweepFreq)
        {
            double[] freqArray = realAndImaginaryTrace.GetXArray();
            double[] realArray = realAndImaginaryTrace.GetYArray();
            double[] ImaginarrayArray = realAndImaginaryTrace.GetZArray();

            string plotHead;
            string[] tempPlotData;

            if (PlotFile.Contains("bandwidth"))
            {
                plotHead = "Normalized S21_dB_" + testCondition.ToString();

                tempPlotData = new string[s21Result.PlotData.Count - 1];
                for (int i = 0; i < s21Result.PlotData.Count - 1; i++)
                {
                    tempPlotData[i] = s21Result.PlotData[i + 1].ToString().Split(',')[1];
                }
            }
            else
            {
                plotHead = "Raw S21_dB_" + testCondition.ToString();

                tempPlotData = new string[s21Result.PlotData.Count - 1];
                for (int i = 0; i < s21Result.PlotData.Count - 1; i++)
                {
                    tempPlotData[i] = s21Result.PlotData[i + 1].ToString().Split(',')[0];
                }
            }

            Util_SavePlotData savePlotData = new Util_SavePlotData();

            if (bSaveSweepFreq)
            {
                savePlotData.AppendResultToFile(PlotFile, "freq_GHz", freqArray);
            }
            savePlotData.AppendResultToFile(PlotFile, plotHead, tempPlotData);
        }


        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_TiaVgcAndBandwidthSweepGui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;
        MeasureSetupManage sigMeasureSetupManage;
        MeasureSetupManage locMeasureSetupManage;


        Trace s21RealAndImaginaryTrace = new Trace();
        Trace s21Trace = new Trace();
        SparamAnalysisResult s21AnaysiseResult;
        List<double> listBwOfVgcSweep = new List<double>();
        List<double> listBwOfBwSweep = new List<double>();
        //bool maskPassFail_S21 = false;
       // bool qualTest;
        double modeCtlOfTia_V;
        double vccOfTia_V;
        double vgcOfTia_V;
        //double bWH_V;
        //double bWL_V;

        bool bSaveFreqArr;
        #endregion

        enum BWSetting
        {
            neg_4,
            zero,
            pos_4
        }

        #endregion
    }

   
}

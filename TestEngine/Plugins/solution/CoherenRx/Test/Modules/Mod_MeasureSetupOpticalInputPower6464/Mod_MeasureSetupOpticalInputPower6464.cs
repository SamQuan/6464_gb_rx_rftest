// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSetupOpticalInputPower.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.Threading;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Power Up: Vpd-->Vcc-->Vgc (or other control inputs)
    /// Set signal and local input power to zero dB
    /// </summary>
    public class Mod_MeasureSetupOpticalInputPower6464 : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            if (!engine.IsSimulation)
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }

            #region init equipment

            Tia_X = (TiaInstrument)configData.ReadReference("Tia_X");
            Tia_Y = (TiaInstrument)configData.ReadReference("Tia_Y");

            pdBias = (PdBiasInstrument)configData.ReadReference("PdBias");
            //AuxPd = (AuxPhaseAlignInstrument)configData.ReadReference("AuxPd");

            WaveformGenerator = (Instr_Ag33120A)configData.ReadReference("WaveformGenerator");

            LockInAmplifier = (Inst_SR830)configData.ReadReference("LockInAmplifier");
            instSPI = (Instr_SPI)configData.ReadReference("InstSPI");

            sigMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            locMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");

            #endregion

            #region init configure

            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");
            double targetInputPower_Sig_dBm = configData.ReadDouble("targetInputPower_Sig_dBm");
            double targetInputPower_Loc_dBm = configData.ReadDouble("targetInputPower_Loc_dBm");

            vmcOfTiaX_V = configData.ReadDouble("vmcOfTiaX_V");
            vmcOfTiaY_V = configData.ReadDouble("vmcOfTiaY_V");
            vccOfTia_V = configData.ReadDouble("vccOfTia_V");
            vgcOfTiaX_V = configData.ReadDouble("vgcOfTiaX_V");
            vgcOfTiaY_V = configData.ReadDouble("vgcOfTiaY_V"); 
            pdBias_V = configData.ReadDouble("pdBais_V");
            bandWidthSetting = int.Parse(configData.ReadDouble("BandwidthSetting").ToString());
            MeasureSetupManage.TiaBandwidth tiaBandwidth = 
                (MeasureSetupManage.TiaBandwidth)Enum.Parse(typeof(MeasureSetupManage.TiaBandwidth), bandWidthSetting.ToString());

            double pdComplanceCurrent_mA = configData.ReadDouble("pdComplanceCurrent_mA");
            double vccComplianceCurrent_mA = configData.ReadDouble("vccComplianceCurrent_mA");
            double vmcComplianceCurrent_mA = configData.ReadDouble("vmcComplianceCurrent_mA");
            double vgcComplianceCurrent_mA = configData.ReadDouble("vgcComplianceCurrent_mA");
            double bandWidthTiaComplianceCurrent_mA = configData.ReadDouble("bandWidthTiaComplianceCurrent_mA");

            double waveformFreq_Khz = configData.ReadDouble("waveformFreq_Khz");
            double waveformAmplitude_mV = configData.ReadDouble("waveformAmplitude_mV");
            double waveformDcOffset_V = configData.ReadDouble("waveformDcOffset_V");

            bool doBandwidthCtrl = configData.ReadBool("doBWControl");
            bool doMCCtrl = configData.ReadBool("doMCControl");
            
             
            #endregion
            //make sure exerything is off and wait 1 second

  //          for (int i = 1; i <= 100; i++)
   //         {

 
   //             Tia_X.BandWidth_H.OutputEnabled = false;
   //             Tia_X.BandWidth_L.OutputEnabled = false;
   //             Tia_X.Vgc.OutputEnabled = false;
                Tia_X.VccSupply.OutputEnabled = false;
                pdBias.PdSource_XIpos.OutputEnabled = false;
                pdBias.PdSource_XQpos.OutputEnabled = false;
    //            Tia_X.VccSupply.UseFrontTerminals = true;
    //            System.Threading.Thread.Sleep(5000);
    //            DialogResult result1 = MessageBox.Show(new Form() { TopMost = true }, "Please reinsert device", "device check", MessageBoxButtons.OK);
                System.Threading.Thread.Sleep(1000);


                //Step1:set pd to 5v(0.7mA)
                engine.SendStatusMsg("set pd bias to " + pdBias_V.ToString() + "V");
                SetPdBias(engine, pdBias_V, pdComplanceCurrent_mA);

                //Step2:set vcc to 3.3v(150mA)
                engine.SendStatusMsg("set tia work status!");
                SetTiaVcc(vccOfTia_V, vccComplianceCurrent_mA);

                //Step3:set Tia  mode
                if (doMCCtrl)
                {
                    SetTiaModeControl(engine, Tia_X, vmcOfTiaX_V, vmcComplianceCurrent_mA);
                    SetTiaModeControl(engine, Tia_Y, vmcOfTiaY_V, vmcComplianceCurrent_mA);
                }
                else
                {
                    //   Tia_X.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    //   Tia_Y.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                }
                
                //Step4:set control volt of each tia vgc 
      //          SetTiaVgc(Tia_X, vgcOfTiaX_V, vgcComplianceCurrent_mA);
      //          SetTiaVgc(Tia_Y, vgcOfTiaY_V, vgcComplianceCurrent_mA);
            
               
                instSPI.SPI_Reset(true);
                uint Temp = instSPI.ReadDIO_SPI(2);
                
            
            /*
                instSPI.WriteDIO_SPI(05, 01);   // Reset TIA
                instSPI.WriteDIO_SPI(06, 00);   // Set all channels to active 
                instSPI.WriteDIO_SPI(07, 02);        // Mode manual - Analog = 1


                

                bool XChip = configData.ReadString("ChipName").Contains("X");

                
                for (uint offset = 0; offset < 385; offset  +=128 )
                {

                    instSPI.WriteDIO_SPI(System.Convert.ToUInt16(08 + offset), 512);       // Set Monitor pin to monitor gain contro, voltage 

                    bool turnOffOppositePolarisation = true;
                    if (turnOffOppositePolarisation)
                    {
                        //set the bw control and the gain control on the channel of intrest only
                        if (XChip)
                        {
                            if (offset < 129)// (offset < 129) true
                            {
                                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x8A00);    // DAC DIN Control sets maximium gain
                            }
                            else
                            {
                                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x0000);    // DAC DIN Control sets maximium gain
                            }
                        }
                        else
                        {

                            if (offset < 129)
                            {
                                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x0000);    // DAC DIN Control sets minimum gain
                            }
                            else
                            {
                                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x8A00);    // DAC DIN Control sets maximium gain

                            }

                        }
                    }
                    else {

                        instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x8A00);    // DAC DIN Control sets maximium gain
                        //instSPI.WriteDIO_SPI(System.Convert.ToUInt16(14 + offset), 771);
                    
                    
                    }

                    //instSPI.WriteDIO_SPI(System.Convert.ToUInt16(10 + offset), 64);       // ADC Enabled
                    instSPI.WriteDIO_SPI(System.Convert.ToUInt16(15 + offset), 02);       // SUM_CIRCUIT_CONTROL
                }

                instSPI.WriteDIO_SPI(14, 771);       // PEAKING_CONTROL sets maximium peak
                instSPI.WriteDIO_SPI(10, 64);       // ADC Enabled
            */

            
                instSPI.WriteDIO_SPI(05, 01);   // Reset TIA
                instSPI.WriteDIO_SPI(06, 00);   // Set all channels to active 
                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(07), 02);        // Mode manual - Digital = 1
                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(14), 771);       // PEAKING_CONTROL sets maximium peak
                instSPI.WriteDIO_SPI(System.Convert.ToUInt16(10), 64);       // ADC Enabled


                for (uint offset = 0; offset < 385; offset += 128)
                {
                    System.Threading.Thread.Sleep(50);
                    instSPI.WriteDIO_SPI(System.Convert.ToUInt16(08 + offset), 512);       // Set Monitor pin to monitor gain contro, voltage 
                    System.Threading.Thread.Sleep(50);
                    instSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0x8A00);    // DAC DIN Control sets gain                  
                    //instSPI.WriteDIO_SPI(System.Convert.ToUInt16(15 + offset), 02);       // SUM_CIRCUIT_CONTROL
                }

                //***
                //
                //  Dave Smith - Update 1 Start
                //
                //***************************************************
                StringBuilder ErrMsg = new StringBuilder();
                for (uint offset = 0; offset < 385; offset += 128)
                {
                    instSPI.ReadDIO_SPI(System.Convert.ToUInt16(08 + offset));  // Double read to refresh register but value ignored
                    ushort ReadValue = instSPI.ReadDIO_SPI(System.Convert.ToUInt16(09 + offset));
                    if (ReadValue != 0x8A00) ErrMsg.Append("DAC Din control : Wrote 0x8A00 Read: " + ReadValue.ToString()+"\n");
                    instSPI.ReadDIO_SPI(System.Convert.ToUInt16(10 + offset));  // Double read to refersh register but value ignored
                    ReadValue = instSPI.ReadDIO_SPI(System.Convert.ToUInt16(10 + offset));
                    if (ReadValue != 64 && offset == 0) ErrMsg.Append("ADC Enabled : Wrote 64 Read: " + ReadValue.ToString()+"\n");
                    instSPI.ReadDIO_SPI(System.Convert.ToUInt16(14 + offset));  // Double read to refersh register but value ignored
                    ReadValue = instSPI.ReadDIO_SPI(System.Convert.ToUInt16(14 + offset));
                    if (ReadValue != 771 && offset == 0) ErrMsg.Append("Peaking Ctrl : Wrote 771 Read: " + ReadValue.ToString() + "\n");

                }

                if (ErrMsg.Length != 0)
                {
                    MessageBox.Show(ErrMsg.ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //  Dave Smith - Update 1 End


       //         Tia_X.Vgc.UseFrontTerminals = true;
       //         Tia_X.BandWidth_H.UseFrontTerminals = true;
       //         Tia_X.BandWidth_L.UseFrontTerminals = true;

                //  sigMeasureSetupManage.SetTiaBandWidth(Tia_XI, tiaBandwidth, bandWidthTiaComplianceCurrent_mA, vccOfTia_V);
                System.Threading.Thread.Sleep(200);
                //Measure X side
                double VccTiaX_BW = 0.0;
                //Step6:measure icc of tia
                 double iccTiaX_Amp = Tia_X.VccSupply.CurrentActual_amp * 1000;
                System.Threading.Thread.Sleep(200);
                //measrue igc 
                double igcTiaX_Amp = 0; // Tia_X.Vgc.CurrentActual_amp * 1000;
                engine.SendStatusMsg("IGC X " + igcTiaX_Amp.ToString() + "mA");
                // Remeasure ICC
                System.Threading.Thread.Sleep(200);
                iccTiaX_Amp = Tia_X.VccSupply.CurrentActual_amp * 1000;
                engine.SendStatusMsg("ICC X " + iccTiaX_Amp.ToString() + "mA");
                System.Threading.Thread.Sleep(200);

  
 



                //Measure Y side
                double iccTiaY_Amp = 0.0;
                double igcTiaY_Amp = 0; // Tia_Y.Vgc.CurrentActual_amp * 1000;
                engine.SendStatusMsg("IGC Y " + igcTiaY_Amp.ToString() + "mA");
                engine.SendStatusMsg("ICC Y " + iccTiaY_Amp.ToString() + "mA");


                //measure imc 
                double imcTiaX_Amp = 0;
                double imcTiaY_Amp = 0;
                //if (doMCCtrl)
                //{
                //    imcTiaX_Amp = Tia_X.ModeCtrlOfGC.CurrentActual_amp * 1000;
                //    imcTiaY_Amp = Tia_Y.ModeCtrlOfGC.CurrentActual_amp * 1000;
                //}

    //            Tia_X.VccSupply.UseFrontTerminals = true;
    //            Tia_X.Vgc.UseFrontTerminals = true;
    //            Tia_X.BandWidth_H.UseFrontTerminals = true;
    //            Tia_X.BandWidth_L.UseFrontTerminals = true;
                Tia_X.VccSupply.OutputEnabled = true;
                
   

    //            engine.SendStatusMsg("loop " + i.ToString());
     //       }
                if (WaveformGenerator.IsOnline)
                {
                    SetWaveformGenerator(waveformFreq_Khz, waveformAmplitude_mV, waveformDcOffset_V);
                }
                SetLockInAmplifier();

                //Step7:set sig input power to targetInputPower_Sig_dBm(zero dB)
                 engine.SendStatusMsg("set signal path input power to " + targetInputPower_Sig_dBm.ToString());
                
            sigMeasureSetupManage.SetOpticalInputPower(engine, targetInputPower_Sig_dBm, spliterRatio_Sig, powerTolerance_dB);
                inputPwrSig_dBm = sigMeasureSetupManage.OPM_Ref.ReadPower() + spliterRatio_Sig;

                //Step8:set local input power to targetInputPower_Loc_dBm(zero dB)

                engine.SendStatusMsg("set local path input power to " + targetInputPower_Loc_dBm.ToString());
                locMeasureSetupManage.SetOpticalInputPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);
               inputPwrLoc_dBm = locMeasureSetupManage.OPM_Ref.ReadPower() + spliterRatio_Loc;

                //Step9:set wave form generator and LCA
                
 
                //get lockin amplifier sensitivity
                double sensitivity_mVpp = 0.0;
                sensitivity_mVpp = GetLockinSensitivity();

                //get lockin amplifier constant
                double timeConstant_ms = 0.0;
                timeConstant_ms = GetLockinTimeConstant();
            
            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("actualInputPower_Sig", inputPwrSig_dBm);
            returnData.AddDouble("actualInputPower_Loc", inputPwrLoc_dBm);
            returnData.AddDouble("iccTia_XI", iccTiaX_Amp);
            returnData.AddDouble("iccTia_YI", iccTiaY_Amp);
            returnData.AddDouble("iccTia_XQ", iccTiaX_Amp);
            returnData.AddDouble("iccTia_YQ", iccTiaY_Amp);

            returnData.AddDouble("iccTia_X", iccTiaX_Amp);
            returnData.AddDouble("iccTia_Y", iccTiaY_Amp);

            returnData.AddDouble("igcTia_XI", igcTiaX_Amp);
            returnData.AddDouble("igcTia_XQ", igcTiaX_Amp);
            returnData.AddDouble("igcTia_YI", igcTiaY_Amp);
            returnData.AddDouble("igcTia_YQ", igcTiaY_Amp);

            returnData.AddDouble("imcTia_X", imcTiaX_Amp);
            returnData.AddDouble("imcTia_Y", imcTiaY_Amp);

            returnData.AddDouble("TIME_SENSITIVITY_LIA_X", timeConstant_ms);
            returnData.AddDouble("TIME_SENSITIVITY_LIA_Y", timeConstant_ms);
            returnData.AddDouble("VOLTAGE_SENSITIVITY_LIA_X", sensitivity_mVpp);
            returnData.AddDouble("VOLTAGE_SENSITIVITY_LIA_Y", sensitivity_mVpp);
        
            return returnData;
        }

       
        #region private function

        /// <summary>
        /// set pd bias volt
        /// </summary>
        /// <param name="voltSet_V">pd bias volt</param>
        /// <param name="currentCompliance_mA">pd bias compliance current</param>
        private void SetPdBias(ITestEngine engine, double voltSet_V, double currentCompliance_mA)
        {
            pdBias.PdSource_XIpos.OutputEnabled = true;
            pdBias.PdSource_XQpos.OutputEnabled = true;
            pdBias.PdSource_YIpos.OutputEnabled = true;
            pdBias.PdSource_YQpos.OutputEnabled = true;

            pdBias.PdSource_XIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_XQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;


            if (!pdBias.PdSource_XIpos.HardwareIdentity.Contains("KEITHLEY"))
            {
                int counts = (int)(voltSet_V / 0.2);
                for (int i = 1; i <= counts; i++)
                {
                    double voltageSet_V = 0.2 * i;
                    pdBias.PdSource_XIpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_XQpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_YIpos.VoltageSetPoint_Volt = voltageSet_V;
                    pdBias.PdSource_YQpos.VoltageSetPoint_Volt = voltageSet_V;

                    if (i == 1)
                    {
                        List<double> voltSensList_V = new List<double>();
                        voltSensList_V.Add(pdBias.PdSource_XIpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_XQpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_YIpos.VoltageActual_Volt);
                        voltSensList_V.Add(pdBias.PdSource_YQpos.VoltageActual_Volt);
 
                        foreach (double volt in voltSensList_V)
                        {
                            if (Math.Abs(volt - voltageSet_V) > 0.1)
                            {
                                pdBias.PdSource_XIpos.OutputEnabled = false;
                                pdBias.PdSource_XQpos.OutputEnabled = false;
                                pdBias.PdSource_YIpos.OutputEnabled = false;
                                pdBias.PdSource_YQpos.OutputEnabled = false;

                                engine.ErrorInModule("pd bias error,please check whether FCU2 pd bias channels or device demaged!");
                            }
                        }
                    }
                    Thread.Sleep(10);
                }
            }
            pdBias.PdSource_XIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_XQpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YQpos.VoltageSetPoint_Volt = voltSet_V;


            double CurrentTemp = pdBias.PdSource_XIpos.CurrentActual_amp;
            CurrentTemp = pdBias.PdSource_XQpos.CurrentActual_amp;
            CurrentTemp = pdBias.PdSource_YIpos.CurrentActual_amp;
            CurrentTemp = pdBias.PdSource_YQpos.CurrentActual_amp;


        }

        /// <summary>
        /// set tia vcc
        /// </summary>
        /// <param name="vcc_V">vcc volt in V</param>
        /// <param name="vccComplianceCurrent_mA">vcc compliance current in mA</param>
        private void SetTiaVcc(double vcc_V, double vccComplianceCurrent_mA)
        {

            
            Tia_X.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            Tia_X.VccSupply.VoltageSetPoint_Volt = vcc_V;
            Tia_X.VccSupply.OutputEnabled = true;
            //Tia_Y.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            //Tia_Y.VccSupply.VoltageSetPoint_Volt = vcc_V;
            //Tia_Y.VccSupply.OutputEnabled = true;
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tia"></param>
        /// <param name="vgc_V"></param>
        /// <param name="vgcComplianceCurrent_mA"></param>
        private void SetTiaVgc(TiaInstrument Tia, double vgc_V, double vgcComplianceCurrent_mA)
        {
            if (Tia.Vgc != null)
            {
                Tia.Vgc.OutputEnabled = true ;
                Tia.Vgc.CurrentComplianceSetPoint_Amp = vgcComplianceCurrent_mA / 1000.0;
                Tia.Vgc.VoltageSetPoint_Volt = vgc_V;
            }
        }

        /// <summary>
        /// set tia module control status
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="Tia"> tia reference</param>
        /// <param name="vmc_V">mode control volt</param>
        /// <param name="complianceCurrent_mA">compliance current of mode control</param>
        private void SetTiaModeControl(ITestEngine engine, TiaInstrument Tia, double vmc_V, double complianceCurrent_mA)
        {
            if (Tia.ModeCtrlOfGC != null)
            {
                Tia.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
                Tia.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

                Tia.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;
                Tia.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;

                Tia.ModeCtrlOfGC.VoltageSetPoint_Volt = vmc_V;
                Tia.ModeCtrlOfGC.VoltageSetPoint_Volt = vmc_V;
            }
            else
            {
                //engine.ErrorInModule("tia is null,object not instantion! ");
            }
        }

        /// <summary>
        /// set waveform generator work status
        /// </summary>
        /// <param name="waveformFreq_Khz">waveformFreq_Khz</param>
        /// <param name="waveformAmplitude_mV">waveformAmplitude_mV</param>
        /// <param name="waveformDcOffset_V">waveformDcOffset_V</param>
        private void SetWaveformGenerator(double waveformFreq_Khz, double waveformAmplitude_mV, double waveformDcOffset_V)
        {
            WaveformGenerator.WaveFunction = Instr_Ag33120A.WaveType.SIN;
            WaveformGenerator.Frequency_Khz = waveformFreq_Khz;
            WaveformGenerator.Amplitude_mV = waveformAmplitude_mV;
            WaveformGenerator.DcOffset_Volt = waveformDcOffset_V;
        }

        /// <summary>
        /// set lockin amplifier work status
        /// </summary>
        private void SetLockInAmplifier()
        {
    //        LockInAmplifier.ReferenceSource = Inst_SR830.ReferenceSourceEnum.External;
         //   LockInAmplifier.ReserveMode = Inst_SR830.ReserveModeEnum.Normal;
            LockInAmplifier.ReferencePhase = 121; //Modified by Matt
            //LockInAmplifier.AutoOffset(Inst_SR830.signalTypeEnum.X);
            //LockInAmplifier.InputCouple = Inst_SR830.InputCoupleTypeEnum.DC;
            //LockInAmplifier.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.A;
            //LockInAmplifier.LowerPassFilterSloper = Inst_SR830.FilterSlopeEnum.eighteen_dBperOct;
//            LockInAmplifier.NotchFilter = Inst_SR830.NotchFilterEnum.LineFilter;
            //LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._50mVperNA;
 //           LockInAmplifier.SetDisplayAndRatio(Inst_SR830.ChanNumEnum.Ch1, Inst_SR830.DisplayChanTowEnum.deg, Inst_SR830.RatioChanTwoEnum.None);
 //           LockInAmplifier.SetOffsetAndExpands(Inst_SR830.signalTypeEnum.X, Inst_SR830.ExpandEnum.noExpand, 0);
            //LockInAmplifier.ShieldGround = Inst_SR830.ShieldGroundEnum.Float;
            //LockInAmplifier.SynchronousFilter = false;
            //LockInAmplifier.TimeConstant = Inst_SR830.TimeConstantEnum._30us;
            //LockInAmplifier.ReferenceSource = Inst_SR830.ReferenceSourceEnum.External;
            
        }
        
        /// <summary>
        /// get lock in amperlifier time constant setting
        /// </summary>
        /// <returns></returns>
        private double GetLockinTimeConstant()
        {
            string tempStr = Enum.GetName(typeof(Inst_SR830.TimeConstantEnum), LockInAmplifier.TimeConstant);
            string str = "";
            char[] charArray = tempStr.ToCharArray(1, tempStr.Length - 1);
            for (int i = 0; i < charArray.Length; i++)
            {
                if (char.IsDigit(charArray[i]))
                {
                    str += charArray[i];
                }
                else
                {
                    str += charArray[i];
                    break;
                }
            }
            double timeConstant_ms = 0.0;
            if (str.Contains("u"))
            {
                timeConstant_ms = double.Parse(str.Substring(0, str.Length - 1)) / 1000;
            }
            else if (str.Contains("m"))
            {
                timeConstant_ms = double.Parse(str.Substring(0, str.Length - 1));
            }
            else if (str.Contains("k"))
            {
                timeConstant_ms = double.Parse(str.Substring(0, str.Length - 1)) * 1000000;
            }
            else
            {
                timeConstant_ms = double.Parse(str.Substring(0, str.Length - 1)) * 1000;
            }
            return timeConstant_ms;
        }

        /// <summary>
        /// get lock in amperlifier sensitivity setting
        /// </summary>
        /// <returns></returns>
        private double GetLockinSensitivity()
        {
            string tempStr = Enum.GetName(typeof(Inst_SR830.SensitivityEnum), LockInAmplifier.Sensitivity);
            string str = "";
            char[] charArray = tempStr.ToCharArray(1, tempStr.Length - 1);
            for (int i = 0; i < charArray.Length; i++)
            {
                if (char.IsDigit(charArray[i]))
                {
                    str += charArray[i];
                }
                else
                {
                    str += charArray[i];
                    break;
                }
            }

            double sensitivity = 0.0;
            if (str.Contains("n"))
            {
                sensitivity = double.Parse(str.Substring(0, str.Length - 1)) / 1000000;
            }
            else if (str.Contains("u"))
            {
                sensitivity = double.Parse(str.Substring(0, str.Length - 1)) / 1000;
            }
            else if (str.Contains("m"))
            {
                sensitivity = double.Parse(str.Substring(0, str.Length - 1));
            }
            else
            {
                sensitivity = double.Parse(str.Substring(0, str.Length - 1)) * 1000;
            }
            return sensitivity;
        }

        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSetupOpticalInputPowerGui)); }
        }

        #region private variable
        TiaInstrument Tia_X;
        TiaInstrument Tia_Y;
        PdBiasInstrument pdBias;
        //AuxPhaseAlignInstrument AuxPd;

        Instr_Ag33120A  WaveformGenerator;

        Inst_SR830 LockInAmplifier;

        Instr_SPI instSPI;

        MeasureSetupManage sigMeasureSetupManage;
        MeasureSetupManage locMeasureSetupManage;

        const double nullPower_dB = double.NegativeInfinity;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double powerTolerance_dB;
        double vccOfTia_V;
        double vmcOfTiaX_V;
        double vmcOfTiaY_V;
        double vgcOfTiaX_V;
        double vgcOfTiaY_V;
        

        int bandWidthSetting;
        double pdBias_V;
        double inputPwrSig_dBm;
        double inputPwrLoc_dBm;

//        bool bMCCtrl;
//        bool bBWCtrl;

        #endregion

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSetupOpticalPathDelay.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.IO;
using Bookham.TestLibrary.Algorithms;
using System.Threading;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSetupOpticalPathDelay : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// optimize optical delay line,let the s21 bandwidth maximum
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            #region off equipment log
            if (!engine.IsSimulation)
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }
            #endregion

            #region init equipment
            //class variable contain all signal and local path equipments
            sigMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            locMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");
            Instr_MS4640A_VNA LCAnayzer = (Instr_MS4640A_VNA)sigMeasureSetupManage.LightwaveComponentAnalyzer;
            #endregion

            #region init configure

            //dut serial number variable
            serialNum = configData.ReadString("serialNumber");
            double wavelength_nm = configData.ReadDouble("wavelength_nm");

            maximumDelayLineTime = configData.ReadDouble("maxDelayLineTime");
            minimumDelayLineTime = configData.ReadDouble("minDelayLineTime");
            defaultDelayTimeSig_ps = configData.ReadDouble("defaultDelayTimeSig_ps");
            defaultDelayTimeLoc_ps = configData.ReadDouble("defaultDelayTimeLoc_ps");

            //variable for each splitter ratio
            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            //variable as zero dB referent power
            zeroDBRefPowerSig_dBm = configData.ReadDouble("zeroDBRefPowerSig_dBm");
            zeroDBRefPowerLoc_dBm = configData.ReadDouble("zeroDBRefPowerLoc_dBm");
            //power tolerance when set input power
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");

            string hybirdChip = configData.ReadString("hybirdChip");

            //list variable contain noise current that all fcu channels for PD bias
            DatumList fcuNoiseCurrentList = configData.ReadListDatum("fcuNoiseCurrentList");

            //struct contain 8703 setup content
            //AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            //analyzerSetup_S21.startFrequency_GHz = configData.ReadDouble("s21StartFrequency_GHz");
            //analyzerSetup_S21.stopFrequency_GHz = configData.ReadDouble("s21StopFrequency_GHz");
            //analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            //analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            //analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            //analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            //analyzerSetup_S21.CalRegister = configData.ReadSint32("CalRegister");

            int s21CalRegister = configData.ReadSint32("CalRegister");

            //struct contain  configure data that use to analysis s21 parameter
            AnalysisConfig_S21 analysisConfig_S21 = new AnalysisConfig_S21();
            analysisConfig_S21.OscillationStartFreq_GHz = configData.ReadDouble("OsciStartFreq_GHz");
            analysisConfig_S21.OscillationStopFreq_GHz = configData.ReadDouble("OsciStopFreq_GHz");
            analysisConfig_S21.smoothingPoints = (int)configData.ReadDouble("s21smoothingPoints");
            analysisConfig_S21.normalisationFrequency_GHz = configData.ReadDouble("s21NormalisationFrequency_GHz");
            analysisConfig_S21.bandWidthPoint_dB = configData.ReadDouble("s21bandWidthPoint_dB");
            analysisConfig_S21.sweepPoints = (int)configData.ReadSint32("sweepPoints");

            #endregion

            #region set signal and local power level,Sig=-9dB,loc=-6dB

            //Adjust sig/loc VOAs so that Sig=-9dB,loc=-6dB
            double powerSetSignal_dBm = - 9;
            double powerSetLocal_dBm = - 6;

            LCAnayzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.HOLD;
            //set signal input power level
            engine.SendStatusMsg("set signal path input power to " + powerSetSignal_dBm.ToString() + "dBm");
            sigMeasureSetupManage.SetOpticalInputPower(engine, powerSetSignal_dBm, spliterRatio_Sig, powerTolerance_dB);
   //         sigMeasureSetupManage.SetOpticalInputPower(engine, 0, spliterRatio_Sig, powerTolerance_dB);
 

            //set local input power level
            engine.SendStatusMsg("set local path input power to " + powerSetLocal_dBm.ToString() + "dBm");
            locMeasureSetupManage.SetOpticalInputPower(engine, powerSetLocal_dBm, spliterRatio_Loc, powerTolerance_dB);

            LCAnayzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.CONT;

            #endregion

            if (sigMeasureSetupManage.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                sigMeasureSetupManage.LightwaveComponentAnalyzer.RecallRegister(s21CalRegister);
            }

            engine.SendStatusMsg(" set LOCKIN : I_10Mohm ");
            //sigMeasureSetupManage.LockInAmplifier.AutoOffset(Inst_SR830.signalTypeEnum.X);
            sigMeasureSetupManage.LightwaveComponentAnalyzer.AutoScale();

           
            //generate 1 file name that save delayline adjust trace 
            //and phase data use to estimate if phase lock stable
            string delayLineAdjustFile;
            string pdCurrentFile;

            if (hybirdChip.Contains("X"))
            {
                delayLineAdjustFile = Util_GenerateFileName.GenWithTimestamp("results", "Delay_Line_Time",
                    serialNum + Enum.GetName(typeof(HybirdChipEnum), HybirdChipEnum.ChipX) + wavelength_nm.ToString(), "csv");
                delayLineAdjustFile = Directory.GetCurrentDirectory() + "\\" + delayLineAdjustFile;

                pdCurrentFile = Util_GenerateFileName.GenWithTimestamp("results", "pdCurrent_DelayLineOptimized",
                    serialNum + Enum.GetName(typeof(HybirdChipEnum), HybirdChipEnum.ChipX) + wavelength_nm.ToString(), "csv");
                pdCurrentFile = Directory.GetCurrentDirectory() + "\\" + pdCurrentFile;

                OptimizeOpticalDelay(engine, s21CalRegister, analysisConfig_S21, HybirdChipEnum.ChipX, fcuNoiseCurrentList,
                ref delayLineAdjustFile, ref pdCurrentFile);
                engine.SendStatusMsg("OptimizeOpticalDelay");
                double locDelays = locMeasureSetupManage.DelayLine.Delay_ps;
                double sigDelays = sigMeasureSetupManage.DelayLine.Delay_ps;
                double delDelays = locDelays - sigDelays;
                if (delDelays > 0)
                {
                    engine.SendStatusMsg("Adjust Delay");
//                    double sigDelayMin_ps = sigMeasureSetupManage.DelayLine.DelayMin_ps;
//                    sigMeasureSetupManage.DelayLine.Delay_ps = Math.Round(sigDelayMin_ps, 2);
//                    locMeasureSetupManage.DelayLine.Delay_ps = Math.Round(locDelays - (sigDelays - sigDelayMin_ps), 2);
                }

            }
            else
            {
                delayLineAdjustFile = Util_GenerateFileName.GenWithTimestamp("results", "Delay_Line_Time",
                    serialNum + Enum.GetName(typeof(HybirdChipEnum), HybirdChipEnum.ChipY) + wavelength_nm.ToString(), "csv");
                delayLineAdjustFile = Directory.GetCurrentDirectory() + "\\" + delayLineAdjustFile;

                pdCurrentFile = Util_GenerateFileName.GenWithTimestamp("results", "pdCurrent_DelayLineOptimized",
                    serialNum + Enum.GetName(typeof(HybirdChipEnum), HybirdChipEnum.ChipY) + wavelength_nm.ToString(), "csv");
                pdCurrentFile = Directory.GetCurrentDirectory() + "\\" + pdCurrentFile;

                OptimizeOpticalDelay(engine, s21CalRegister, analysisConfig_S21, HybirdChipEnum.ChipY, fcuNoiseCurrentList,
                    ref delayLineAdjustFile, ref pdCurrentFile);
            }

            if (sigMeasureSetupManage.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                Thread.Sleep(15000);
            }
            else
            {
                Thread.Sleep(2500);
            }
            engine.SendStatusMsg("setup LightwaveComponentAnalyzer");

            // return data
            DatumList returnData = new DatumList();
            double delay_ps = Math.Abs(sigMeasureSetupManage.DelayLine.Delay_ps - locMeasureSetupManage.DelayLine.Delay_ps);

            returnData.AddDouble("delaySettingSig_ps", sigMeasureSetupManage.DelayLine.Delay_ps);
            returnData.AddDouble("delaySettingLoc_ps", locMeasureSetupManage.DelayLine.Delay_ps);
            returnData.AddDouble("delayTime_ps", delay_ps);
            returnData.AddFileLink("delayLineAdjustFile", delayLineAdjustFile);
            returnData.AddFileLink("pdCurrentFile", pdCurrentFile);
            returnData.AddListDatum("DelayLineOptimizeInfo", DelayLineOptimizeInfo);
            return returnData;
        }

        /// <summary>
        /// optimize optical delay line let the s21 bandwidth maximum
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="analyzerSetup">VNA setup configure data</param>
        /// <param name="analysisConfig">s21 analysis Config</param>
        /// <param name="hybirdChip">hybirdChip name</param>
        /// <param name="DelayLineSweepFile">delay line sweep data file</param>
        /// <param name="pdCurrentFile">pd current data file</param>
        /// <returns>delay time was set</returns>
        private void OptimizeOpticalDelay(ITestEngine engine, int s21CalRegister,
            AnalysisConfig_S21 analysisConfig, HybirdChipEnum hybirdChip, DatumList fcuNoiseCurrentList,
            ref string DelayLineSweepFile, ref string pdCurrentFile)
        {
            //sign of delay change,true as +,falase as -
            int signDelayChange = -1;

            double delayChange = 0.0;
            //variable record current delay line time
            double currDelay_ps = 0.0;
            //variable first null point frequency
            double firstNullPointFreq_GHz = double.NaN;
            //variable current bandwidth in GHz
            double bandwidth_GHz = 0.0;
            double bandwidthStart_GHz = 0.0;

            //s21 sweep data trace
            Trace s21RealAndImaginaryTrace = new Trace();
            Trace s21MagintudeTrace = new Trace();
            SparamAnalysisResult s21result = new SparamAnalysisResult();

            //list for save delay line sweep data
            List<double> bandWidthList = new List<double>();
            List<double> delayTimeList = new List<double>();
            List<double[]> s21MagintudeTraceList = new List<double[]>();
            List<double> nullFreqList_GHz = new List<double>();

            Util_SavePlotData saveDelayTimeData = new Util_SavePlotData();

            //bool signDelayChangeCheck = false;
            int loopCounts = 0;
            int firstNullPointsAppeardTimes = 0;
            //bool setdelayPassFail = true;
            //bool InterpolateFail = false;
            double lastDelay_ps = 0.0;

            //set the two delay to the default delay time and select which delay line to change
            InstType_ODL InstrDelayLine;
            if (hybirdChip == HybirdChipEnum.ChipX)
            {
                //locMeasureSetupManage.DelayLine.Delay_ps = locMeasureSetupManage.DelayLine.DelayMin_ps;
                locMeasureSetupManage.DelayLine.Delay_ps = 0;
                sigMeasureSetupManage.DelayLine.Delay_ps = maximumDelayLineTime;
                //sigMeasureSetupManage.DelayLine.Delay_ps = 10;
                InstrDelayLine = sigMeasureSetupManage.DelayLine;
            }
            else
            {
                InstrDelayLine = locMeasureSetupManage.DelayLine;
             //   InstrDelayLine.Delay_ps = 170;
             //   signDelayChange = -1;
            //    locMeasureSetupManage.DelayLine.Delay_ps = 0;
            //    sigMeasureSetupManage.DelayLine.Delay_ps = 170;
            //    InstrDelayLine = sigMeasureSetupManage.DelayLine;
            }

            //maximum and minimum delay line time that can be seted
            double maxDelayLineTime = maximumDelayLineTime;
            double mindelayLineTime = 0; // InstrDelayLine.DelayMin_ps;
           

            //Thread.Sleep(15000);
            Thread.Sleep(5000);
            if (hybirdChip == HybirdChipEnum.ChipX)
            {
                do
                {

                    //CheckPhaseLocked(engine);

                    loopCounts++;
                    //record pd current to .csv
                    MeasAndSavePdCurrent(pdCurrentFile, fcuNoiseCurrentList, loopCounts);

                    //read back curernt delay time
                    currDelay_ps = InstrDelayLine.Delay_ps;


                    //Perform an S21 plot and calculate the 3dB bandwidth 

                    s21RealAndImaginaryTrace = SparamMeasurement.Get_S21(engine, sigMeasureSetupManage.LightwaveComponentAnalyzer, s21CalRegister);

   //                 sigMeasureSetupManage.DelayLine.Delay_ps = 50;
  //                  locMeasureSetupManage.DelayLine.Delay_ps = 170;

                    s21MagintudeTrace = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace);

                    s21result = SparamAnalysise.CalculateBandwidthAndMask_S21(engine, s21MagintudeTrace, analysisConfig);

                    bandwidth_GHz = s21result.bandWidth_GHz;

                    //find the first null

 //                   sigMeasureSetupManage.SetOpticalInputPower(engine, 0, spliterRatio_Sig, powerTolerance_dB);

                    firstNullPointFreq_GHz = SparamAnalysise.FindNullPoint_S21(engine, s21MagintudeTrace, analysisConfig);

 //                   locMeasureSetupManage.DelayLine.Delay_ps = 100;
 //                   sigMeasureSetupManage.DelayLine.Delay_ps = 70;


                    if (analysisConfig.OscillationStopFreq_GHz <= 20 && firstNullPointFreq_GHz > 18.5)
                    {
                        firstNullPointFreq_GHz = double.NaN;
                    }
                    else if (firstNullPointFreq_GHz > 25)
                    {
                        firstNullPointFreq_GHz = double.NaN;
                    }


                    //updata s21 curve to GUI
                    engine.SendToGui("Null point: " + firstNullPointFreq_GHz.ToString() +
                        "  sig: " + sigMeasureSetupManage.DelayLine.Delay_ps.ToString() +
                        " loc��" + locMeasureSetupManage.DelayLine.Delay_ps.ToString() +
                        " BW: " + s21result.bandWidth_GHz.ToString());
                    engine.SendToGui(s21MagintudeTrace);
                    engine.SendStatusMsg("Null point: " + firstNullPointFreq_GHz.ToString() +
                        "  sig: " + sigMeasureSetupManage.DelayLine.Delay_ps.ToString() +
                        " loc��" + locMeasureSetupManage.DelayLine.Delay_ps.ToString() +
                        " BW: " + s21result.bandWidth_GHz.ToString());

                    if (double.IsNaN(firstNullPointFreq_GHz))
                    {
                        //save bandwidth and delay to list
                        bandWidthList.Add(s21result.bandWidth_GHz);
                        // record and save s21 sweep data
                        delayTimeList.Add(currDelay_ps);
                    }
                    else
                    {
                        firstNullPointsAppeardTimes++;
                    }

                    s21MagintudeTraceList.Add(s21MagintudeTrace.GetYArray());
                    saveDelayTimeData.AppendResultToFile(DelayLineSweepFile, currDelay_ps.ToString() + "ps", s21MagintudeTrace.GetYArray());

                    if (loopCounts == 1)
                    {
                        bandwidthStart_GHz = s21result.bandWidth_GHz;
                    }

                    //Check if we can't get the bandwidth as there is no 3dB point.
                    if (s21result.bandWidth_GHz == 0 && firstNullPointFreq_GHz == 0)
                    {
                        break;
                    }
                    engine.SendStatusMsg("1");
                    //Check if exceeded number of attempts
                    if ((loopCounts - firstNullPointsAppeardTimes) >= 8)
                    {
                        if ((loopCounts - firstNullPointsAppeardTimes) >= 5)
                        {
                            //object delaySort = (object)delayTimeList.ToArray();
                            //object bandwidthSort = (object)bandWidthList.ToArray();
                            //object delayInterpolated = (object)delayChange;

                            //Alg_Math.MathClass math = new Alg_Math.MathClass();
                            ////Sort the parameters so we can perform a curve fit
                            //math.Sort(ref delaySort, ref bandwidthSort);

                            ////find the delay corresponding to maximum interpolated bandwidth
                            //math.FindPeak(ref bandwidthSort, ref delaySort, ref delayInterpolated, 1, ref InterpolateFail, false);

                            ////Change the delay to the interpolated delay
                            //if (!InterpolateFail)
                            //{
                            //    currDelay_ps = (double)delayInterpolated;
                            //    engine.SendStatusMsg("interpolated delay time is: " + currDelay_ps.ToString());
                            //}

                            double[] delaySort = delayTimeList.ToArray();
                            double[] bandwidthSort = bandWidthList.ToArray();

                            Sort2DimenArray(delaySort, bandwidthSort);

                            Bookham.TestLibrary.Algorithms.Alg_FindTurningPointUsing2ndOrderFit.TurningPointData turningPointData =
                            Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(delaySort, bandwidthSort);

                            if (turningPointData.Success == true)
                            {
                                currDelay_ps = turningPointData.TurningPointXvalue;
                                engine.SendStatusMsg("interpolated delay time is: " + currDelay_ps.ToString());
                            }
                            //set delay time
                            SetDelayLine(engine, InstrDelayLine, maxDelayLineTime, mindelayLineTime, currDelay_ps);
                        }

                        MeasAndSavePdCurrent(pdCurrentFile, fcuNoiseCurrentList, loopCounts);
                        break;
                    }
                    
                    //Check that we are going in the right direction
                    double deltaBandWidth = 0.0;
                    int tempCounts = loopCounts - firstNullPointsAppeardTimes;
                    if (tempCounts > 1)
                    {
                        deltaBandWidth = Math.Round(bandWidthList[tempCounts - 1], 2) - Math.Round(bandWidthList[tempCounts - 2], 2);
                        if (deltaBandWidth < 0)
                        {
                            //Reverse the sign
                            signDelayChange = -signDelayChange;
                        }
                    }

                    //if can't not find first null point,then "currDelay_ps-25ps"(experience value)
                    if (double.IsNaN(firstNullPointFreq_GHz))
                    {
                        delayChange = 2 + Math.Abs(deltaBandWidth);
                    }
                    else
                    {
                        //calculate new delay
                        delayChange = 1000 / (2 * firstNullPointFreq_GHz);
                    }
                    //change delay


                    currDelay_ps = currDelay_ps + delayChange * signDelayChange;

                    engine.SendStatusMsg("CalculateDelay:" + currDelay_ps);

                     ////if first calculate delay time out of range,that meas local delay line should be set to max delay time
                    //    if (hybirdChip == HybirdChipEnum.ChipX)
                    //    {
                    if (loopCounts == 1 && currDelay_ps < 55)
                    {

                        if (currDelay_ps < -20)
                        {
                            engine.SendStatusMsg("change local to max , signal to min ");
                            locMeasureSetupManage.DelayLine.Delay_ps = maximumDelayLineTime;
                            sigMeasureSetupManage.DelayLine.Delay_ps = sigMeasureSetupManage.DelayLine.DelayMin_ps;
                            signDelayChange = 1;
                        }
                        else
                        {
                            engine.SendStatusMsg("change signal and local to mid point");
                            locMeasureSetupManage.DelayLine.Delay_ps = 130;
                            sigMeasureSetupManage.DelayLine.Delay_ps = 130;
                        }
                        currDelay_ps = InstrDelayLine.Delay_ps;
                        bandWidthList.Clear();
                        delayTimeList.Clear();
                        loopCounts = 0;
                        firstNullPointsAppeardTimes = 0;
                        continue;
                    }

                    if (currDelay_ps < 0)
                    {
                        engine.SendStatusMsg("change local to max , signal to min ");
                        locMeasureSetupManage.DelayLine.Delay_ps = maximumDelayLineTime;
                        sigMeasureSetupManage.DelayLine.Delay_ps = sigMeasureSetupManage.DelayLine.DelayMin_ps;
                        signDelayChange = 1;
                        currDelay_ps = 50;
                    }
                    
                    //else if (currDelay_ps < 50)
                    //{
                    //    engine.SendStatusMsg("Set delay line mid range");
                    //    sigMeasureSetupManage.DelayLine.Delay_ps = 100;
                    //    locMeasureSetupManage.DelayLine.Delay_ps = 100;
                    //    currDelay_ps = 100;

                    //}
                    //   }

                    if ((loopCounts - firstNullPointsAppeardTimes) >= 5)
                    {
                        ////object delaySort = (object)delayTimeList.ToArray();
                        ////object bandwidthSort = (object)bandWidthList.ToArray();
                        ////object delayInterpolated = (object)delayChange;

                        ////Alg_Math.MathClass math = new Alg_Math.MathClass();
                        //////Sort the parameters so we can perform a curve fit
                        ////math.Sort(ref delaySort, ref bandwidthSort);

                        //////find the delay corresponding to maximum interpolated bandwidth
                        ////math.FindPeak(ref bandwidthSort, ref delaySort, ref delayInterpolated, 1, ref InterpolateFail, false);
                        //////math.FindPeak2(ref bandwidthSort, ref delaySort, ref delayInterpolated, 1, ref InterpolateFail);

                        double[] delaySort = delayTimeList.ToArray();
                        double[] bandwidthSort = bandWidthList.ToArray();

                        Sort2DimenArray(delaySort, bandwidthSort);

                        Bookham.TestLibrary.Algorithms.Alg_FindTurningPointUsing2ndOrderFit.TurningPointData turningPointData =
                        Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(delaySort, bandwidthSort);

                        if (turningPointData.Success == true)
                        {
                            currDelay_ps = turningPointData.TurningPointXvalue;
                        }
                        engine.SendStatusMsg("turning Delay:" + currDelay_ps);
                        //double bandwidth = turningPointData.TurningPointYvalue;

                        ////Change the delay to the interpolated delay
                        //if (!InterpolateFail)
                        //{
                        //   currDelay_ps = (double)delayInterpolated;
                        //}

                        //see if we have arrived ata the same point as last time
                        if (double.Equals(Math.Round(currDelay_ps, 0), Math.Round(lastDelay_ps, 0)))
                        {
                            if (s21result.bandWidth_GHz > bandwidthStart_GHz)
                            {
                                break;
                            }
                        }
                    }

                    //set delay time
                    SetDelayLine(engine, InstrDelayLine, maxDelayLineTime, mindelayLineTime, currDelay_ps);
                    engine.SendStatusMsg("set Delay:" + currDelay_ps);
                    if (sigMeasureSetupManage.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
                    {
                        Thread.Sleep(8000);
                    }
                    else
                    {
                        Thread.Sleep(3000);
                    }

                    //sigMeasureSetupManage.LockInAmplifier.AutoOffset(Inst_SR830.signalTypeEnum.X);

                    if (sigMeasureSetupManage.LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
                    {
                        Thread.Sleep(15000);
                    }
                    else
                    {
                        Thread.Sleep(3000);
                    }

                    lastDelay_ps = currDelay_ps;

                } while (true);
                engine.SendStatusMsg("exit delay loop");
            }
            //sigMeasureSetupManage.LockInAmplifier.AutoGain();
            //save each sweep delaytime and bandwidth to .csv
            
            saveDelayTimeData.AppendResultToFile(DelayLineSweepFile, "delay", delayTimeList.ToArray());
            saveDelayTimeData.AppendResultToFile(DelayLineSweepFile, "bandwidth", bandWidthList.ToArray());

            DelayLineOptimizeInfo.AddDoubleArray("delay", delayTimeList.ToArray());
            DelayLineOptimizeInfo.AddDoubleArray("bandwidth", bandWidthList.ToArray());

        }

        /// set delay time
        /// </summary>
        /// <param name="engine">reference ITestEngine</param>
        /// <param name="InstrDelayLine">object of delayline</param>
        /// <param name="maxDelayLineTime">max vaild delay line time</param>
        /// <param name="mindelayLineTime">min vaild delay line time</param>
        /// <param name="delay_ps">delay time need to set</param>
        private void SetDelayLine(ITestEngine engine, InstType_ODL InstrDelayLine, 
            double maxDelayLineTime, double mindelayLineTime, double delay_ps)
        {
            if (delay_ps > maxDelayLineTime || delay_ps < mindelayLineTime)
            {
                engine.ErrorInModule("out of valid range to set delay line");
            }
            else
            {
                InstrDelayLine.Delay_ps = delay_ps;
            }
        }

        /// <summary>
        /// measure and save pd current
        /// </summary>
        /// <param name="pdCurrentFile">file that save pd current</param>
        /// <param name="fcuNoiseCurrentList">Datumlist contains fcu noise curren</param>
        /// <param name="testCounts">the times that save current,when testCounts>1 will not save file head</param>
        private void MeasAndSavePdCurrent(string pdCurrentFile, DatumList fcuNoiseCurrentList,int testCounts)
        {

            //Perform an S21 plot and calculate the 3dB bandwidth 
            List<double> pdCurrentList = new List<double>();
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_XIpos.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("XIP"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_XIneg.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("XIN"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_XQpos.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("XQP"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_XQneg.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("XQN"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_YIpos.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("YIP"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_YIneg.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("YIN"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_YQpos.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("YQP"));
            pdCurrentList.Add(sigMeasureSetupManage.PdBias.PdSource_YQneg.CurrentActual_amp * 1000 - fcuNoiseCurrentList.ReadDouble("YQN"));
            
            string headerString = "XIP,XIN,XQP,XQN,YIP,YIN,YQP,YQN,";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pdCurrentList.Count; i++)
            {
                sb.Append(pdCurrentList[i].ToString());
                sb.Append(",");
            }

            StreamWriter sw = new StreamWriter(pdCurrentFile, true);
            if (testCounts == 1)
            {
                sw.WriteLine(headerString);
            }
            sw.WriteLine(sb);
            sw.Close();

        }

        /// <summary>
        /// check phase lock whether stabile
        /// </summary>
        /// <param name="engine">reference ITestEngine</param>
        private void CheckPhaseLocked(ITestEngine engine)
        {
            int readPhaseTime = 0;
            do
            {
                List<double> phaseOutputList = new List<double>();
                for (int k = 0; k < 400; k++)
                {
                    double output = sigMeasureSetupManage.LockInAmplifier.ReadOutput(Inst_SR830.OutputTypeEnum.Phase);
                    phaseOutputList.Add(output);
                    Thread.Sleep(2);
                }


                phaseOutputList.Sort();
                double phaseChange_dge = Math.Abs((180 - Math.Abs(phaseOutputList[phaseOutputList.Count - 1])) - (180 - Math.Abs(phaseOutputList[0])));

                if (phaseChange_dge < 40)
                {
                    break;
                }
                Thread.Sleep(4000);

                readPhaseTime++;
            } while (readPhaseTime < 5);

            if (readPhaseTime >= 5)
            {
                engine.ShowContinueUserQuery("Please check the phase locked whether stable! and click continus");
            }
        }

        private void Sort2DimenArray(double[] xArray, double[] yArray)
        {
            for (int i = 1; i < xArray.Length; i++)
            {
                for (int j = 0; j < xArray.Length - i; j++)
                {
                    if (xArray[j + 1] < xArray[j])
                    {

                        double temp = xArray[j];
                        xArray[j] = xArray[j + 1];
                        xArray[j + 1] = temp;

                        temp = yArray[j];
                        yArray[j] = yArray[j + 1];
                        yArray[j + 1] = temp;
                    }
                }
            }
        }

        
        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSetupOpticalPathDelayGui)); }
        }

        #region private variable

        MeasureSetupManage sigMeasureSetupManage;
        MeasureSetupManage locMeasureSetupManage;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double zeroDBRefPowerSig_dBm;
        double zeroDBRefPowerLoc_dBm;
        double powerTolerance_dB;
        //double delaySettingSig_ps;
        //double delaySettingLoc_ps;

        double maximumDelayLineTime;
        double minimumDelayLineTime;
        double defaultDelayTimeSig_ps;
        double defaultDelayTimeLoc_ps;
        string serialNum;

        DatumList DelayLineOptimizeInfo = new DatumList();
        #endregion

        /// <summary>
        /// Hybird Chip Enum
        /// </summary>
        public enum HybirdChipEnum
        {
            ChipX,
            ChipY
        }

        #endregion
    }
}

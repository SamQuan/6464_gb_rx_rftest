// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_S22Calibration.cs
//
// Author: sam.quan 2012
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.Framework.Messages;
using Bookham.TestLibrary.InstrTypes;
using System.Threading;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using System.IO;
using Bookham.TestSolution.CoherentRxTestCommonData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_S22Calibration : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // TODO - add your code here!
            bool calibrationFinished = false;

            InstType_OpticalPowerMeter OPMeter = (InstType_OpticalPowerMeter)instruments["OPMeter"];

            MeasureSetupManage sigChainSetupManage = (MeasureSetupManage)configData.ReadReference("sigChainSetupManage");

            double spliterRatio=configData.ReadDouble("spliterRatio_Sig");
            double powerTolerance_dB=configData.ReadDouble("powerTolerance_dB");

            Instr_MS4640A_VNA LCAnalyzer = (Instr_MS4640A_VNA)instruments["LCAnalyzer"];

            Switch_LCA_RF RfSwitch = (Switch_LCA_RF)configData.ReadReference("RfSwitch");

            string hybirdChip = (string)configData.ReadString("hybridChip");

            MsgContainer recieveData;

            ConfigData cfgData = new ConfigData(configData);

            ControlData sendCtrlData;


            engine.GuiShow();
            engine.GuiToFront();

            engine.SendToGui(hybirdChip);

            if (!engine.IsSimulation)
            {
                LCAnalyzer.ChannelCount = 1;
                LCAnalyzer.ActiveMeausurementChannel = 1;
                LCAnalyzer.SourceOutputPower_dBm = configData.ReadDouble("SourcePower_dBm");
                
                sigChainSetupManage.SetOpticalInputPower(engine, -6, spliterRatio, powerTolerance_dB);
            }



            do
            {
                recieveData = engine.ReceiveFromGui();

                if (recieveData.Payload.GetType() != typeof(ControlData))
                {
                    throw new TypeLoadException("The type of the message GUI sent is Wrong!");
                }

                ControlData recieveCtrlData = (ControlData)recieveData.Payload;

                sendCtrlData = recieveCtrlData;
              
                switch (recieveCtrlData.PressedButton)
                {
                    case ButtonName.S22Open:

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);

                        sendCtrlData.S22OpenCalibrated = true;
                        break;
                    case ButtonName.S22Short:
                        if (!recieveCtrlData.S22OpenCalibrated)
                        {
                            throw new Exception("Must do the S22Open Calibration before S22Short Calibration!");
                        }

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S22ShortCalibrated = true;
                        break;
                    case ButtonName.S22Load:
                        if (!recieveCtrlData.S22ShortCalibrated)
                        {
                            throw new Exception("Must do the S22Short Calibration before S22Load Calibration");
                        }

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S22LoadCalibrated = true;
                        break;
                    case ButtonName.S22Done:
                        if (!recieveCtrlData.S22LoadCalibrated)
                        {
                            engine.ShowContinueUserQuery("Must calibrate all of the standards to proceed!");
                            break;
                        }

                        DoneCalibration(LCAnalyzer, recieveCtrlData);

                        calibrationFinished = true;
                        break;
                    default:
                        throw new Exception("Error Type of ControlData");
                }

                engine.SendToGui(sendCtrlData);

            } while (!(calibrationFinished || (recieveData == null)));

            

            SaveS22CalSetupData(LCAnalyzer, cfgData);

            engine.ShowContinueUserQuery("Please remove the cal kit!");
            LCAnalyzer.AutoScale();


            // return data
            DatumList returnData = new DatumList();
            returnData.AddBool("calResultStatus", true);
            return returnData;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ag8703a"></param>
        /// <param name="cfgData"></param>
        /// <param name="ctrlData"></param>
        private void S22Calibration(Instr_MS4640A_VNA LCAnalyzer,
            ConfigData cfgData, ControlData ctrlData)
        {
            if (!ctrlData.S22OpenCalibrated)
            {
                System.Threading.Thread.Sleep(100);
                LCAnalyzer.CalibrationPortType = Instr_MS4640A_VNA.CalibrationPortTypes.OnePort;
                Thread.Sleep(1000);
                LCAnalyzer.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT2;
                Thread.Sleep(1000);
                LCAnalyzer.CalibrationMethod = Instr_MS4640A_VNA.CalibrationMethods.SOLR;
                Thread.Sleep(1000);
            }

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S22Open:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.OPEN,
                        Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //the instrument takes this long to cal

                    break;
                case ButtonName.S22Short:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.SHORT,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //this is how long instrument takes to cal
                    break;
                case ButtonName.S22Load:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.LOAD,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
       }

        /// <summary>
        /// Done Calibration
        /// </summary>
        /// <param name="LCAnalyzer"></param>
        /// <param name="ctrlData"></param>
        private void DoneCalibration(Instr_MS4640A_VNA LCAnalyzer, ControlData ctrlData)
        {

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S22Done:
                    LCAnalyzer.DoneCalibration();
                    System.Threading.Thread.Sleep(4000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
        }

        /// <summary>
        /// save s22 calculate setup data
        /// </summary>
        /// <param name="LCAnalyzer">LCA instr</param>
        /// <param name="cfgData">configure data</param>
        private void SaveS22CalSetupData(Instr_MS4640A_VNA LCAnalyzer, ConfigData cfgData)
        {
            LCAnalyzer.SaveCalDataForSpecificChannel(cfgData.s22SetupFileName);
            Thread.Sleep(10000);
        }

        public Type UserControl
        {
            get { return (typeof(Mod_S22CalibrationGui)); }
        }

        #endregion

        private struct ConfigData
        {
            public ConfigData(DatumList inputData)
            {
                SourcePower_dBm = inputData.ReadDouble("SourcePower_dBm");

                s22SetupFileName = inputData.ReadString("s22SetupFileName");
                calResultFile = inputData.ReadString("calResultFileName");
                vnaAddress = inputData.ReadString("VNA_Address");
            }

            public double SourcePower_dBm;

            public string s22SetupFileName;

            public string vnaAddress;

            public string calResultFile;
        }
    }
}

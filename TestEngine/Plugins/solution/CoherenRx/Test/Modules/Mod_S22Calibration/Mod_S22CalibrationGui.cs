// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_S22CalibrationGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_S22CalibrationGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_S22CalibrationGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {

            if (payload.GetType() == typeof(string))
            {
                string str = labelCalInfor.Text;
                str += "----Current Chip is " + (string)payload;
                labelCalInfor.Text = str;
            }
            else if(payload.GetType()==typeof(ConnCalKit))
            {
                ConnCalKit connCalKit = (ConnCalKit)payload;
                Image image = this.pictureBoxPic.Image;
                //show connet cal kit picture
                //this.pictureBoxPic.Image = global::Mod_S22Calibration.Properties.Resources;
                MessageBox.Show(connCalKit.ConnCalKitMessage);
                connCalKit.continueCal = true;
                sendToWorker(connCalKit.continueCal);
            }
            else if (payload.GetType() == typeof(ControlData))
            {
                ControlData recCtrlData = (ControlData)payload;

                switch (recCtrlData.PressedButton)
                {
                    case ButtonName.S22Open:
                        if (recCtrlData.S22OpenCalibrated)
                        {
                            labelFinalStatusResult.Text = "Ongoing......";

                            checkBoxS22Open.CheckState = CheckState.Checked;
                            checkBoxS22Open.Enabled = false;
                            buttonS22Open.Enabled = false;

                            checkBoxS22Short.Enabled = true;
                            buttonS22Short.Enabled = true;
                        }
                        break;
                    case ButtonName.S22Short:
                        {
                            checkBoxS22Short.CheckState = CheckState.Checked;
                            checkBoxS22Short.Enabled = false;
                            buttonS22Short.Enabled = false;

                            checkBoxS22Load.Enabled = true;
                            buttonS22Load.Enabled = true;
                        }
                        break;
                    case ButtonName.S22Load:
                        {
                            checkBoxS22Load.CheckState = CheckState.Checked;
                            checkBoxS22Load.Enabled = false;
                            buttonS22Load.Enabled = false;

                            this.pictureBoxPic.Image = global::Mod_S22Calibration.Properties.Resources.S22Diag;
                            this.tabControl.SelectedIndex = 2;

                            buttonS22Done.Enabled = true;
                        }
                        break;
                    case ButtonName.S22Done:
                        if (recCtrlData.S22LoadCalibrated && 
                            recCtrlData.S22OpenCalibrated &&
                            recCtrlData.S22ShortCalibrated)
                        {
                            buttonS22Done.Enabled = false;

                            this.pictureBoxPic.Image = global::Mod_S22Calibration.Properties.Resources.S22Diag;
                            this.tabControl.SelectedIndex = 3;
                        }
                        break;
                    default:
                        labelFinalStatusResult.Text = "Error Break";
                        throw new TypeLoadException("The type of GUI recieve is Wrong!");
                }
            }
            else
            {
                throw new TypeLoadException("The type of the message GUI sent is Wrong!");
            }
        }

        private void buttonS22Open_Click(object sender, EventArgs e)
        {
            cmdS22Open = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Open;
            
            this.sendToWorker(commandType);
        }

        private void buttonS22Short_Click(object sender, EventArgs e)
        {
            cmdS22Short = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Short;
            
            this.sendToWorker(commandType);
        }

        private void buttonS22Load_Click(object sender, EventArgs e)
        {
            cmdS22Load = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Load;

            this.sendToWorker(commandType);
        }

        private void buttonS22Done_Click(object sender, EventArgs e)
        {
            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Done;

            this.sendToWorker(commandType);
        }

        private void DisableAllButton()
        {
            buttonS22Open.Enabled = false;
            buttonS22Short.Enabled = false;
            buttonS22Load.Enabled = false;
            buttonS22Done.Enabled = false;
        }

        private void EnableAllButton()
        {
            buttonS22Open.Enabled = true;
            buttonS22Short.Enabled = true;
            buttonS22Load.Enabled = true;
            buttonS22Done.Enabled = true;
        }

        /// <summary>
        /// This subroutine is to make sure the checkbox indicates
        /// correctly wether the appropriate cal has been performed or not
        /// </summary>
        /// <param name="NameofBotton"></param>
        /// <returns></returns>
        private CheckState CheckButtonClick(ButtonName nameofBotton)
        {
            CheckState rtnValue;
            switch (nameofBotton)
            {
                case ButtonName.S22Open:
                    if (cmdS22Open)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Short:
                    if (cmdS22Short)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Load:
                    if (cmdS22Load)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Done:
                default:
                    rtnValue = CheckState.Indeterminate;
                    break;
            }

            return rtnValue;
        }

        private void checkBoxS22Open_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Open.CheckState = CheckButtonClick(ButtonName.S22Open);
        }

        private void checkBoxS22Short_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Short.CheckState = CheckButtonClick(ButtonName.S22Short);
        }

        private void checkBoxS22Load_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Load.CheckState = CheckButtonClick(ButtonName.S22Load);
        }

        private bool cmdS22Open = false;
        private bool cmdS22Short = false;
        private bool cmdS22Load = false;

        private ControlData commandType = new ControlData();




        



        

    }

   
}

// [Copyright]
//
// Bookham Test Engine
// Mod_S22Calibration
//
// Bookham.TestSolution.TestModules/Mod_S22CalibrationGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_S22CalibrationGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBoxPic = new System.Windows.Forms.PictureBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageS22 = new System.Windows.Forms.TabPage();
            this.panelS22 = new System.Windows.Forms.Panel();
            this.labelS22Information = new System.Windows.Forms.Label();
            this.labelS22 = new System.Windows.Forms.Label();
            this.buttonS22Load = new System.Windows.Forms.Button();
            this.checkBoxS22Load = new System.Windows.Forms.CheckBox();
            this.buttonS22Short = new System.Windows.Forms.Button();
            this.checkBoxS22Short = new System.Windows.Forms.CheckBox();
            this.buttonS22Open = new System.Windows.Forms.Button();
            this.checkBoxS22Open = new System.Windows.Forms.CheckBox();
            this.labelCalInfor = new System.Windows.Forms.Label();
            this.labelFinalStatus = new System.Windows.Forms.Label();
            this.labelFinalStatusResult = new System.Windows.Forms.Label();
            this.labelS22Note = new System.Windows.Forms.Label();
            this.buttonS22Done = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPic)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageS22.SuspendLayout();
            this.panelS22.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 48);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.Controls.Add(this.pictureBoxPic);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AllowDrop = true;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl);
            this.splitContainer1.Size = new System.Drawing.Size(958, 427);
            this.splitContainer1.SplitterDistance = 396;
            this.splitContainer1.TabIndex = 4;
            // 
            // pictureBoxPic
            // 
            this.pictureBoxPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxPic.Image = global::Mod_S22Calibration.Properties.Resources.S22Diag;
            this.pictureBoxPic.InitialImage = global::Mod_S22Calibration.Properties.Resources.S22Diag;
            this.pictureBoxPic.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPic.Name = "pictureBoxPic";
            this.pictureBoxPic.Size = new System.Drawing.Size(396, 427);
            this.pictureBoxPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPic.TabIndex = 8;
            this.pictureBoxPic.TabStop = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageS22);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(558, 427);
            this.tabControl.TabIndex = 6;
            // 
            // tabPageS22
            // 
            this.tabPageS22.Controls.Add(this.panelS22);
            this.tabPageS22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageS22.Location = new System.Drawing.Point(4, 29);
            this.tabPageS22.Name = "tabPageS22";
            this.tabPageS22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageS22.Size = new System.Drawing.Size(550, 394);
            this.tabPageS22.TabIndex = 0;
            this.tabPageS22.Text = "S22";
            this.tabPageS22.UseVisualStyleBackColor = true;
            // 
            // panelS22
            // 
            this.panelS22.Controls.Add(this.labelS22Note);
            this.panelS22.Controls.Add(this.buttonS22Done);
            this.panelS22.Controls.Add(this.labelS22Information);
            this.panelS22.Controls.Add(this.labelS22);
            this.panelS22.Controls.Add(this.buttonS22Load);
            this.panelS22.Controls.Add(this.checkBoxS22Load);
            this.panelS22.Controls.Add(this.buttonS22Short);
            this.panelS22.Controls.Add(this.checkBoxS22Short);
            this.panelS22.Controls.Add(this.buttonS22Open);
            this.panelS22.Controls.Add(this.checkBoxS22Open);
            this.panelS22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelS22.Location = new System.Drawing.Point(3, 3);
            this.panelS22.Name = "panelS22";
            this.panelS22.Size = new System.Drawing.Size(544, 388);
            this.panelS22.TabIndex = 3;
            this.panelS22.Tag = "";
            // 
            // labelS22Information
            // 
            this.labelS22Information.Location = new System.Drawing.Point(42, 186);
            this.labelS22Information.Name = "labelS22Information";
            this.labelS22Information.Size = new System.Drawing.Size(276, 74);
            this.labelS22Information.TabIndex = 9;
            this.labelS22Information.Text = "Please set up the equipment as indicated then click on the buttons above to calib" +
                "rate that particular standard";
            // 
            // labelS22
            // 
            this.labelS22.AutoSize = true;
            this.labelS22.Location = new System.Drawing.Point(144, 7);
            this.labelS22.Name = "labelS22";
            this.labelS22.Size = new System.Drawing.Size(38, 20);
            this.labelS22.TabIndex = 8;
            this.labelS22.Text = "S22";
            // 
            // buttonS22Load
            // 
            this.buttonS22Load.Enabled = false;
            this.buttonS22Load.Location = new System.Drawing.Point(80, 131);
            this.buttonS22Load.Name = "buttonS22Load";
            this.buttonS22Load.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Load.TabIndex = 6;
            this.buttonS22Load.Text = "Load";
            this.buttonS22Load.UseVisualStyleBackColor = true;
            this.buttonS22Load.Click += new System.EventHandler(this.buttonS22Load_Click);
            // 
            // checkBoxS22Load
            // 
            this.checkBoxS22Load.Enabled = false;
            this.checkBoxS22Load.Location = new System.Drawing.Point(206, 131);
            this.checkBoxS22Load.Name = "checkBoxS22Load";
            this.checkBoxS22Load.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Load.TabIndex = 5;
            this.checkBoxS22Load.Text = "Calibrated";
            this.checkBoxS22Load.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Load.UseVisualStyleBackColor = false;
            this.checkBoxS22Load.CheckedChanged += new System.EventHandler(this.checkBoxS22Load_CheckedChanged);
            // 
            // buttonS22Short
            // 
            this.buttonS22Short.Enabled = false;
            this.buttonS22Short.Location = new System.Drawing.Point(80, 84);
            this.buttonS22Short.Name = "buttonS22Short";
            this.buttonS22Short.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Short.TabIndex = 4;
            this.buttonS22Short.Text = "Short";
            this.buttonS22Short.UseVisualStyleBackColor = true;
            this.buttonS22Short.Click += new System.EventHandler(this.buttonS22Short_Click);
            // 
            // checkBoxS22Short
            // 
            this.checkBoxS22Short.Enabled = false;
            this.checkBoxS22Short.Location = new System.Drawing.Point(206, 84);
            this.checkBoxS22Short.Name = "checkBoxS22Short";
            this.checkBoxS22Short.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Short.TabIndex = 3;
            this.checkBoxS22Short.Text = "Calibrated";
            this.checkBoxS22Short.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Short.UseVisualStyleBackColor = false;
            this.checkBoxS22Short.CheckedChanged += new System.EventHandler(this.checkBoxS22Short_CheckedChanged);
            // 
            // buttonS22Open
            // 
            this.buttonS22Open.Location = new System.Drawing.Point(80, 36);
            this.buttonS22Open.Name = "buttonS22Open";
            this.buttonS22Open.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Open.TabIndex = 2;
            this.buttonS22Open.Text = "Open";
            this.buttonS22Open.UseVisualStyleBackColor = true;
            this.buttonS22Open.Click += new System.EventHandler(this.buttonS22Open_Click);
            // 
            // checkBoxS22Open
            // 
            this.checkBoxS22Open.Location = new System.Drawing.Point(206, 36);
            this.checkBoxS22Open.Name = "checkBoxS22Open";
            this.checkBoxS22Open.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Open.TabIndex = 1;
            this.checkBoxS22Open.Text = "Calibrated";
            this.checkBoxS22Open.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Open.UseVisualStyleBackColor = false;
            this.checkBoxS22Open.CheckedChanged += new System.EventHandler(this.checkBoxS22Open_CheckedChanged);
            // 
            // labelCalInfor
            // 
            this.labelCalInfor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCalInfor.Location = new System.Drawing.Point(73, 12);
            this.labelCalInfor.Name = "labelCalInfor";
            this.labelCalInfor.Size = new System.Drawing.Size(680, 23);
            this.labelCalInfor.TabIndex = 5;
            this.labelCalInfor.Text = "Welcome to Calibrate VNA! Best Regards!";
            this.labelCalInfor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFinalStatus
            // 
            this.labelFinalStatus.AutoSize = true;
            this.labelFinalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFinalStatus.Location = new System.Drawing.Point(3, 478);
            this.labelFinalStatus.Name = "labelFinalStatus";
            this.labelFinalStatus.Size = new System.Drawing.Size(61, 13);
            this.labelFinalStatus.TabIndex = 6;
            this.labelFinalStatus.Text = "Test Status";
            // 
            // labelFinalStatusResult
            // 
            this.labelFinalStatusResult.AutoSize = true;
            this.labelFinalStatusResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFinalStatusResult.Location = new System.Drawing.Point(75, 478);
            this.labelFinalStatusResult.Name = "labelFinalStatusResult";
            this.labelFinalStatusResult.Size = new System.Drawing.Size(43, 13);
            this.labelFinalStatusResult.TabIndex = 7;
            this.labelFinalStatusResult.Text = "Starting";
            // 
            // labelS22Note
            // 
            this.labelS22Note.Location = new System.Drawing.Point(324, 175);
            this.labelS22Note.Name = "labelS22Note";
            this.labelS22Note.Size = new System.Drawing.Size(213, 110);
            this.labelS22Note.TabIndex = 12;
            this.labelS22Note.Text = "NOTE: Ensure all the Calibration have been done when pressing \'Done\'";
            this.labelS22Note.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonS22Done
            // 
            this.buttonS22Done.Enabled = false;
            this.buttonS22Done.Location = new System.Drawing.Point(328, 288);
            this.buttonS22Done.Name = "buttonS22Done";
            this.buttonS22Done.Size = new System.Drawing.Size(194, 54);
            this.buttonS22Done.TabIndex = 11;
            this.buttonS22Done.Text = "Done";
            this.buttonS22Done.UseVisualStyleBackColor = true;
            this.buttonS22Done.Click += new System.EventHandler(this.buttonS22Done_Click);
            // 
            // Mod_S22CalibrationGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelFinalStatusResult);
            this.Controls.Add(this.labelFinalStatus);
            this.Controls.Add(this.labelCalInfor);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Mod_S22CalibrationGui";
            this.Size = new System.Drawing.Size(1194, 789);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPic)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageS22.ResumeLayout(false);
            this.panelS22.ResumeLayout(false);
            this.panelS22.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBoxPic;
        internal System.Windows.Forms.Label labelCalInfor;
        private System.Windows.Forms.Label labelFinalStatus;
        private System.Windows.Forms.Label labelFinalStatusResult;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageS22;
        private System.Windows.Forms.Panel panelS22;
        private System.Windows.Forms.Label labelS22Information;
        private System.Windows.Forms.Label labelS22;
        private System.Windows.Forms.Button buttonS22Load;
        private System.Windows.Forms.CheckBox checkBoxS22Load;
        private System.Windows.Forms.Button buttonS22Short;
        private System.Windows.Forms.CheckBox checkBoxS22Short;
        private System.Windows.Forms.Button buttonS22Open;
        private System.Windows.Forms.CheckBox checkBoxS22Open;
        private System.Windows.Forms.Label labelS22Note;
        private System.Windows.Forms.Button buttonS22Done;
    }
}

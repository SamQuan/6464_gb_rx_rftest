using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
//using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    class TestSetDriver
    {
        const string TestPresenceFailure = "PRESENCE TEST FAILED";

        public bool Initialise8703()
        {
            return true;
        }

        public bool InitialiseRFSwitchController()
        {
            return true;
        }

        public void InitialiseS_Parameter(InstType_LightwaveComponentAnalyzer ag8703a)
        {
            ag8703a.SetDefaultState();
            ag8703a.EnableLogging = true;
            ag8703a.Timeout_ms = 90000;
            //ag8703a.ChannelDisplayNumber = ChannelDisplayNumber.One;
        }

        public void ReadS_ParameterTraceData()
        {

        }

        public void RecallS_ParameterCal()
        {

        }

        public void SaveS_ParameterCal()
        {

        }

        public void SetRFSwitch()
        {

        }


    }    

    public enum RfArmType
    {
        enXChip = 0,
        enYChip = 1
    }

    public enum Switch
    {
        enOff = 0,
        enOn = 1
    }
}

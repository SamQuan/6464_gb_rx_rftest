using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestModules
{
    class ControlData
    {
        public bool S22OpenCalibrated
        {
            get
            {
                return _S22OpenCalibrated;
            }
            set
            {
                _S22OpenCalibrated = value;
            }
        }

        public bool S22ShortCalibrated
        {
            get
            {
                return _S22ShortCalibrated;
            }
            set
            {
                _S22ShortCalibrated = value;
            }
        }

        public bool S22LoadCalibrated
        {
            get
            {
                return _S22LoadCalibrated;
            }
            set
            {
                _S22LoadCalibrated = value;
            }
        }

        public ButtonName PressedButton
        {
            get
            {
                return _pressedButton;
            }
            set
            {
                _pressedButton = value;
            }
        }

        /// <summary>
        /// The Flag of the parameters whether has been calibrated.
        /// </summary>
        /// 
        private bool _S22OpenCalibrated = false;
        private bool _S22ShortCalibrated = false;
        private bool _S22LoadCalibrated = false;

        /// <summary>
        /// The Flag of the parameters whether has been calibrated.
        /// </summary>
        private ButtonName _pressedButton;
    }

    /// <summary>
    /// Name of Botton which has been pressed
    /// </summary>
    public enum ButtonName
    {
        S22Open = 1,
        S22Short = 2,
        S22Load = 3,
        S22Done = 4,

        S11Open = 5,
        S11Short = 6,
        S11Load = 7,

    }
}

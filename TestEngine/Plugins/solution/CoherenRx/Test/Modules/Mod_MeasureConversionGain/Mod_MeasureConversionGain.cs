// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureEO.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;
using System.Threading;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureConversionGain : ITestModule
    {
        #region ITestModule Members
        Inst_SR830.SensitivityEnum OptimiseSensitivity;
        //Inst_SR830.SensitivityEnum PreviousLockInSensitivity;

        double vgc;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            DatumList returnData = new DatumList();

            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiShow();
            engine.GuiToFront();

            #region init configure
            double Vgc_V = configData.ReadDouble("Vgc_V");

            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");
            TiaInstrument Tia_I = (TiaInstrument)configData.ReadReference("Tia_I");
            TiaInstrument Tia_Q = (TiaInstrument)configData.ReadReference("Tia_Q");

            measureSetup_SIG = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            measureSetup_LOC = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");
            rfOutputPort = configData.ReadString("rfOutputPort");

            tolerancePwr_dB = configData.ReadDouble("powerTolerance_dB");
            targetPwrSig_dBm = configData.ReadDouble("targetPwrSig_dBm");
            targetPwrLoc_dBm = configData.ReadDouble("targetPwrLoc_dBm");

            spliteRate_SIG = configData.ReadDouble("splitRate_Sig");
            spliteRate_LOC = configData.ReadDouble("splitRate_Loc");

            int s21CalRegister = configData.ReadSint32("CalRegister");

            OptimiseSensitivity = (Inst_SR830.SensitivityEnum) configData.ReadEnum("OptimiseLockinSensitivity");
            //PreviousLockInSensitivity = (Inst_SR830.SensitivityEnum)configData.ReadReference("PREVIOUS_LOCKIN_SENSITIVITY");

            #endregion



            DatumList guiMessage = new DatumList();

            guiMessage.AddOrUpdateSint32("progressBarValue", 5);
            engine.SendToGui(guiMessage);
            engine.SendToGui(string.Format("Conversion Gain  VGC:{0}V  SIG:{1}dBm  LO:{2}dBm", Vgc_V, targetPwrSig_dBm, targetPwrLoc_dBm));

            //set gc compliance current
            Tia_I.Vgc.OutputEnabled = true;
            Tia_I.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;
            Tia_Q.Vgc.OutputEnabled = true;
            Tia_Q.Vgc.CurrentComplianceSetPoint_Amp = 2.0 / 1000.0;


            if (LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            {
                LightwaveComponentAnalyzer.RecallRegister(s21CalRegister);
            }
            else
            {
                Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)LightwaveComponentAnalyzer;
                MS4046A.EmbedOrDeEmbedEnable = true;
                MS4046A.ChannelCount = 1;
                MS4046A.ActiveMeausurementChannel = 1;
            }

            LightwaveComponentAnalyzer.AutoScale();

            #region set power level


            //set signal path optical power
            measureSetup_SIG.SetOpticalInputPower(engine, targetPwrSig_dBm, spliteRate_SIG, tolerancePwr_dB);
            guiMessage.AddOrUpdateSint32("progressBarValue", 45);
            engine.SendToGui(guiMessage);

            //set local path optical power
            measureSetup_LOC.SetOpticalInputPower(engine, targetPwrLoc_dBm, spliteRate_LOC, tolerancePwr_dB);
             guiMessage.AddOrUpdateSint32("progressBarValue", 75);
            engine.SendToGui(guiMessage);


            #endregion

            Tia_I.Vgc.VoltageSetPoint_Volt = Vgc_V;
            Tia_Q.Vgc.VoltageSetPoint_Volt = Vgc_V;
            this.vgc = Vgc_V;
            measureSetup_SIG.LockInAmplifier.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.A;
            
            GetPdReadings();

            Thread.Sleep(1000);
            measureSetup_SIG.LockInAmplifier.ReferencePhase = 0;
            measureSetup_SIG.LightwaveComponentAnalyzer.AutoScale();
            Thread.Sleep(1000);

            //SetBestSensitivity();
            measureSetup_SIG.LockInAmplifier.Sensitivity = OptimiseSensitivity;
            GetPdReadings();
            System.Threading.Thread.Sleep(3000);
            returnData.AddReference("LockInSensitivity", measureSetup_SIG.LockInAmplifier.Sensitivity);

            s21RealAndImaginaryTrace1 = SparamMeasurement.Get_S21_Fast(engine, LightwaveComponentAnalyzer, s21CalRegister);

            guiMessage.AddOrUpdateSint32("progressBarValue", 100);
            engine.SendToGui(guiMessage);

            DatumList listDatumInputPower = new DatumList();

            listDatumInputPower.AddDouble("sigInputPower_dBm", measureSetup_SIG.OPM_Ref.ReadPower() + spliteRate_SIG);
            listDatumInputPower.AddDouble("locInputPower_dBm", measureSetup_LOC.OPM_Ref.ReadPower() + spliteRate_LOC);


            // return data

            returnData.AddOrUpdateReference("XYRawData_EO", s21RealAndImaginaryTrace1);
            returnData.AddListDatum("listDatumInputPower", listDatumInputPower);



            return returnData;
        }


        private void GetPdReadings(){


            //pd have been seen locking up when the optical power is changed. they start working after triggering a read. to be investigated further. Ali Faraj 13072016

            double result;
            if (rfOutputPort.Contains("X")) {


                result = measureSetup_SIG.PdBias.PdSource_XQpos.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_XQneg.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_XIpos.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_XIneg.CurrentActual_amp;
            
            
            } else if (rfOutputPort.Contains("Y")){



                result = measureSetup_SIG.PdBias.PdSource_YQpos.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_YQneg.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_YIpos.CurrentActual_amp;
                result = measureSetup_SIG.PdBias.PdSource_YIneg.CurrentActual_amp;

            }
        
        }
        /*
        private void SetBestSensitivity()
        {
            
                if (rfOutputPort.Contains("XIP") || rfOutputPort.Contains("YIP"))
                {
                    System.IO.StreamWriter file =
   new System.IO.StreamWriter(string.Format("c:\\VGC_{0}_{1}_{2}.txt", vgc.ToString("0.00"),rfOutputPort, DateTime.Now.ToString("hhmmss")), true);

                    if (OptimiseSensitivity)
                    {

                        Inst_SR830.SensitivityEnum[] SensitivityLevels = new Inst_SR830.SensitivityEnum[] { Inst_SR830.SensitivityEnum._50mVperNA, Inst_SR830.SensitivityEnum._20mVperNA, Inst_SR830.SensitivityEnum._10mVperNA, Inst_SR830.SensitivityEnum._5mVperNA, Inst_SR830.SensitivityEnum._2mVperNA, Inst_SR830.SensitivityEnum._1mVperNA, Inst_SR830.SensitivityEnum._500uVperPA, Inst_SR830.SensitivityEnum._200uVperPA };

                        Inst_SR830.SensitivityEnum SensitivityLevel = Inst_SR830.SensitivityEnum._50mVperNA;
                        double MaxRatio = double.MinValue;

                        List<double> forDebugging = new List<double>();
                        foreach (var level in SensitivityLevels)
                        {
                            file.WriteLine(level + "\t" + level + " log ratio");
                            measureSetup_SIG.LockInAmplifier.Sensitivity = level;
                            Thread.Sleep(2000);

                            Inst_Ke24xx pos, neg;

                            if (rfOutputPort.Contains("XI"))
                            {
                                pos = measureSetup_SIG.PdBias.PdSource_XIpos;
                                neg = measureSetup_SIG.PdBias.PdSource_XIneg;
                            }

                            else if (rfOutputPort.Contains("YI"))
                            {
                                pos = measureSetup_SIG.PdBias.PdSource_YIpos;
                                neg = measureSetup_SIG.PdBias.PdSource_YIneg;

                            }
                            else if (rfOutputPort.Contains("XQ"))
                            {

                                pos = measureSetup_SIG.PdBias.PdSource_XQpos;
                                neg = measureSetup_SIG.PdBias.PdSource_XQneg;

                            }
                            else if (rfOutputPort.Contains("YQ"))
                            {

                                pos = measureSetup_SIG.PdBias.PdSource_YQpos;
                                neg = measureSetup_SIG.PdBias.PdSource_YQneg;

                            }
                            else
                            {

                                throw new Exception("unexpected channel");
                            }


                            double[] ratios = new double[100];  //for debugging of this code
                            double sumRatios = 0.0;

                            for (int ratioIdx = 0; ratioIdx < ratios.Length; ratioIdx++)
                            {


                                ratios[ratioIdx] = MeasureRatio(1, pos, neg);
                                forDebugging.Add(Math.Abs(10 * Math.Log10(ratios[ratioIdx])));
                                file.WriteLine(ratios[ratioIdx] + "\t" + Math.Abs(10 * Math.Log10(ratios[ratioIdx])));
                                sumRatios += ratios[ratioIdx];
                                Thread.Sleep(100);
                            }

                            double AvgRatio = Math.Abs(10 * Math.Log10(sumRatios / ratios.Length));

                            if (AvgRatio > MaxRatio)
                            {

                                SensitivityLevel = level;
                                MaxRatio = AvgRatio;

                            }


                        }

                        measureSetup_SIG.LockInAmplifier.Sensitivity = SensitivityLevel;
                        Thread.Sleep(3000);
                        file.Close();
                        
                    }
                    else
                    {

                        //do nothing if its an xip or yip but the config file says that we don't need to optimise sensitivity for this level (in other words its using the sensitivity level of teh previous vgc condition)
                    }



                }
                else {

                    measureSetup_SIG.LockInAmplifier.Sensitivity = PreviousLockInSensitivity;
                    Thread.Sleep(3000);
                
                }


                
           

        }
        */

        private double MeasureRatio(int numAvg, Inst_Ke24xx pos, Inst_Ke24xx neg) {

            double SumP = 0.0;
            double SumN = 0.0;

            for (int i = 0; i < numAvg; i++)
            {
                SumP += pos.CurrentActual_amp;
                SumN += neg.CurrentActual_amp;

            }

            double PhI_P = Math.Round(SumP / numAvg * 1000, 6);
            double PhI_N = Math.Round(SumN / numAvg * 1000, 6);

            return Math.Abs(PhI_P) / Math.Abs(PhI_N);
            
        }

        private double CaculatePdCurrentRatio(ITestEngine engine, double[] pdCurrentArray, bool isForward)
        {
            double currentRatio = 0.0;
            switch (rfOutputPort)
            {
                case "XIP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[0]) / Math.Abs(pdCurrentArray[1])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[0].ToString() + " / " + pdCurrentArray[1] + " ratio = " + currentRatio);
                    break;
                case "XIN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[1]) / Math.Abs(pdCurrentArray[0])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[1].ToString() + " / " + pdCurrentArray[0] + " ratio = " + currentRatio);
                    break;
                case "XQP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[2]) / Math.Abs(pdCurrentArray[3])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[2].ToString() + " / " + pdCurrentArray[3] + " ratio = " + currentRatio);
                    break;
                case "XQN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[3]) / Math.Abs(pdCurrentArray[2])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[3].ToString() + " / " + pdCurrentArray[2] + " ratio = " + currentRatio);
                    break;
                case "YIP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[4]) / Math.Abs(pdCurrentArray[5])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[4].ToString() + " / " + pdCurrentArray[5] + " ratio = " + currentRatio);
                    break;
                case "YIN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[5]) / Math.Abs(pdCurrentArray[4])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[5].ToString() + " / " + pdCurrentArray[4] + " ratio = " + currentRatio);
                    break;
                case "YQP":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[6]) / Math.Abs(pdCurrentArray[7])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[6].ToString() + " / " + pdCurrentArray[7] + " ratio = " + currentRatio);
                    break;
                case "YQN":
                    currentRatio = Math.Round(10 * Math.Log10(Math.Abs(pdCurrentArray[7]) / Math.Abs(pdCurrentArray[6])), 2);
                    engine.SendStatusMsg(rfOutputPort.ToString() + " PD current " + pdCurrentArray[7].ToString() + " / " + pdCurrentArray[6] + " ratio = " + currentRatio);
                    break;
                default:
                    engine.ErrorInModule("can not recognise rf output port name when calculte pd current ratio!");
                    break;
            }
            if (isForward == false)
            {
                currentRatio = 0 - currentRatio;
            }
            return currentRatio;
        }

        /// <summary>
        /// check phase lock whether stabile
        /// </summary>
        /// <param name="engine">reference ITestEngine</param>
        private void CheckPhaseLocked(ITestEngine engine)
        {
            int readPhaseTime = 0;
            do
            {
                List<double> phaseOutputList = new List<double>();
                for (int k = 0; k < 200; k++)
                {
                    double output = measureSetup_SIG.LockInAmplifier.ReadOutput(Inst_SR830.OutputTypeEnum.Phase);
                    phaseOutputList.Add(output);
                    Thread.Sleep(2);
                }


                phaseOutputList.Sort();
                double phaseChange_dge = Math.Abs((180 - Math.Abs(phaseOutputList[phaseOutputList.Count - 1])) - (180 - Math.Abs(phaseOutputList[0])));

                if (phaseChange_dge < 40)
                {
                    break;
                }
                Thread.Sleep(4000);

                readPhaseTime++;
            } while (readPhaseTime < 5);

            if (readPhaseTime >= 5)
            {
                engine.ShowContinueUserQuery("Please check the phase locked whether stable! and click continus");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fcuNoiseCurrentList"></param>
        /// <returns></returns>
        private double[] MeasurePdCurrentSum(DatumList fcuNoiseCurrentList)
        {
            List<double> pdCurrentList = new List<double>();

            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XIpos.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XIneg.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XQpos.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_XQneg.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YIpos.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YIneg.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YQpos.CurrentActual_amp * 1000, 6));
            pdCurrentList.Add(Math.Round(measureSetup_SIG.PdBias.PdSource_YQneg.CurrentActual_amp * 1000, 6));

            return pdCurrentList.ToArray();
        }

        #region private function

        public List<Trace> ReadXYData()
        {
            string fileName;
            fileName = Directory.GetCurrentDirectory() + "\\results\\Offline\\xyData.csv";

            CsvReader csvReader = new CsvReader();
            List<string[]> listXYData = csvReader.ReadFile(fileName);

            double[] freqArray = new double[listXYData.Count - 1];
            double[] realArray = new double[listXYData.Count - 1];
            double[] imaginaryArray = new double[listXYData.Count - 1];
            double[] magintudeArray = new double[listXYData.Count - 1];
            List<string> listContent = new List<string>();
            Trace s21RealAndImaginaryTrace = new Trace();
            Trace s21MagintudeTrace = new Trace();
            for (int i = 1; i < listXYData.Count; i++)
            {
                freqArray[i - 1] = double.Parse(listXYData[i][0]);
                imaginaryArray[i - 1] = double.Parse(listXYData[i][1]);
                realArray[i - 1] = double.Parse(listXYData[i][2]);
                double temp = Math.Sqrt(Math.Pow(realArray[i - 1], 2) + Math.Pow(imaginaryArray[i - 1], 2));
                double magintude_dB = 20 * Math.Log10(temp);
                magintudeArray[i - 1] = magintude_dB;

                s21RealAndImaginaryTrace.Add(freqArray[i - 1], realArray[i - 1], imaginaryArray[i - 1]);
                s21MagintudeTrace.Add(freqArray[i - 1], magintude_dB);
            }
            List<Trace> tracelist = new List<Trace>();
            tracelist.Add(s21RealAndImaginaryTrace);
            tracelist.Add(s21MagintudeTrace);

            return tracelist;
        }



        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureConversionGainGui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        MeasureSetupManage measureSetup_SIG;
        MeasureSetupManage measureSetup_LOC;

        double targetPwrSig_dBm;
        double targetPwrLoc_dBm;
        double spliteRate_SIG;
        double spliteRate_LOC;
        double tolerancePwr_dB;

        Trace s21RealAndImaginaryTrace = new Trace();
        Trace s21RealAndImaginaryTrace1 = new Trace();
        Trace s21RealAndImaginaryTrace2 = new Trace();
        Point TempPoint = new Point();

        //bool maskPassFail_S21 = false;
        //bool qualTest;

        string rfOutputPort = null;
        #endregion

        #endregion
    }
}

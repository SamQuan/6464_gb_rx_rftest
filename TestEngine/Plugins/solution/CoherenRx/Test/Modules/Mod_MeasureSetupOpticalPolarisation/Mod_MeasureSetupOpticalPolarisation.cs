// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSetupOpticalPolarisation.cs
//
// Author: sam.quan 2011.8
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSetupOpticalPolarisation : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// optimize local and signal path polarisation
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();
           
            #region disable instruments log
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }

            #endregion

            #region init equipment

            measSetupSig = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            measSetupLoc = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");
            Instr_MS4640A_VNA LCAnalyzer = (Instr_MS4640A_VNA)measSetupSig.LightwaveComponentAnalyzer;
            Inst_Sub20ToAsic sub20sig = (Inst_Sub20ToAsic)measSetupSig.sub20_Pol;
            Inst_Sub20ToAsic sub20loc = (Inst_Sub20ToAsic)measSetupLoc.sub20_Pol;
            sub20loc.SetDefaultState();
  //          sub20loc.pol_write(1009, 0);
     

 //           TiaInstrument Tia_XI = (TiaInstrument)configData.ReadReference("Tia_XI");
 //           TiaInstrument Tia_XQ = (TiaInstrument)configData.ReadReference("Tia_XQ");
 //           TiaInstrument Tia_YI = (TiaInstrument)configData.ReadReference("Tia_YI");
 //           TiaInstrument Tia_YQ = (TiaInstrument)configData.ReadReference("Tia_YQ");
            
            #endregion

            #region init configure

            is6464 = configData.ReadBool("is6464");
            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");
            powerSetSOPC_Sig_dB = configData.ReadDouble("powerSetSOPC_Sig_dB");
            powerSetSOPC_Loc_dB = configData.ReadDouble("powerSetSOPC_Loc_dB");
            hybirdChip = configData.ReadString("hybirdChip");
            hybirdChipEnum = (MeasureSetupManage.HybirdChipEnum)Enum.Parse(typeof(MeasureSetupManage.HybirdChipEnum), "Chip" + hybirdChip);
            serialNumber=configData.ReadString("dutSerialNum");

            MeasureSetupManage.SOPCSweepConfig polSweepConfig = new MeasureSetupManage.SOPCSweepConfig();
            polSweepConfig.sweepStart_V = configData.ReadDouble("sopcSweeStartVoltage_V"); ;
            polSweepConfig.sweepStop_V = configData.ReadDouble("sopcSweeStopVoltage_V");
            polSweepConfig.sweepStep_V = configData.ReadDouble("sopcSweeStepVoltage_V");
            polSweepConfig.sweepDelayTime_ms = configData.ReadDouble("sopcSweeDelayTime_ms");
            polSweepConfig.IcomplianceSOPC_A = configData.ReadDouble("sopcComplianceCurrent_mA") / 1000;

            //creat file name save polarization sweep data
            sopcAdjustTraceFileLoc = Util_GenerateFileName.GenWithTimestamp("results", "SopcSweep_Loc", serialNumber + "Chip" + hybirdChip, "csv");
            sopcAdjustTraceFileLoc = Directory.GetCurrentDirectory() + "\\" + sopcAdjustTraceFileLoc;

            sopcAdjustTraceFileSig = Util_GenerateFileName.GenWithTimestamp("results", "SopcSweep_SIG", serialNumber + "Chip" + hybirdChip, "csv");
            sopcAdjustTraceFileSig = Directory.GetCurrentDirectory() + "\\" + sopcAdjustTraceFileSig;

            #endregion

            LCAnalyzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.HOLD;

            #region optimize local polarisation controller

            //Adjust VOAs so that Sig = null,Loc = 6dB
         //   if (hybirdChip.Contains("z"))
         //   {
                engine.SendStatusMsg("set signal input power to null power");
                measSetupSig.SetOpticalInputPower(engine, nullPower_dB, spliterRatio_Sig, powerTolerance_dB);



                engine.SendStatusMsg("set local input power to " + powerSetSOPC_Loc_dB.ToString() + "dB");
                measSetupLoc.SetOpticalInputPower(engine, powerSetSOPC_Loc_dB, spliterRatio_Loc, powerTolerance_dB);



                // Adjust Pol_sig ,read PD current,find the max current,fix polarisation to this point
                engine.SendToGui("Local polarisation controller");
                engine.SendStatusMsg("Adjust local path polarisztion!");
            

                sopcCtrlVoltage_Loc = measSetupLoc.SetOpticalPolarization(engine, MeasureSetupManage.OpticalPathEnum.LOC,
                         hybirdChipEnum, polSweepConfig, sopcAdjustTraceFileLoc,measSetupLoc);

                //calculate the pd current sum
           
                pdCurrentSum_Loc = MeasureEachChipPdCurrentSum(is6464);
       //     }
            #endregion

            #region optimize signal polarisztion controller

            //Adjust sig VOAs so that sig = 0dB
            engine.SendStatusMsg("set Signal input power to " + powerSetSOPC_Sig_dB + "dB");
            measSetupSig.SetOpticalInputPower(engine, powerSetSOPC_Sig_dB, spliterRatio_Sig, powerTolerance_dB);

            //Adjust loc VOAs so that loc=null
            engine.SendStatusMsg("set local input power to null power");
            measSetupLoc.SetOpticalInputPower(engine, nullPower_dB, spliterRatio_Loc, powerTolerance_dB);


            // Adjust Pol_sig ,read PD current,find the max current,fix polarisation to this point
            engine.SendToGui("Signal polarisation controller");
            engine.SendStatusMsg("Adjust signal path polarisztion!");

            sopcCtrlVoltage_Sig = measSetupSig.SetOpticalPolarization(engine, MeasureSetupManage.OpticalPathEnum.SIG,
                    hybirdChipEnum, polSweepConfig,  sopcAdjustTraceFileSig, measSetupSig);
            
            //calculate the pd current sum
            pdCurrentSum_Sig = MeasureEachChipPdCurrentSum(is6464);

            #endregion

            LCAnalyzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.CONT;

            #region return data
            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("polCtrlVoltSigChan1", sopcCtrlVoltage_Sig[0]);
            returnData.AddDouble("polCtrlVoltSigChan2", sopcCtrlVoltage_Sig[1]);
  //          returnData.AddDouble("polCtrlVoltSigChan3", sopcCtrlVoltage_Sig[2]);
  //          returnData.AddDouble("polCtrlVoltSigChan4", sopcCtrlVoltage_Sig[3]);
   //        if (hybirdChip.Contains("z"))
   //         {

                returnData.AddDouble("polCtrlVoltLocChan1", sopcCtrlVoltage_Loc[0]);
                returnData.AddDouble("polCtrlVoltLocChan2", sopcCtrlVoltage_Loc[1]);

    //                        returnData.AddDouble("polCtrlVoltLocChan3", sopcCtrlVoltage_Loc[2]);
    //                        returnData.AddDouble("polCtrlVoltLocChan4", sopcCtrlVoltage_Loc[3]);
  //           }
            returnData.AddDouble("pdCurrentSum_Loc", pdCurrentSum_Loc);
            returnData.AddFileLink("polLocSweepFile", sopcAdjustTraceFileLoc);
            returnData.AddDouble("pdCurrentSum_Sig", pdCurrentSum_Sig);


            returnData.AddFileLink("polSigSweepFile", sopcAdjustTraceFileSig);


            #endregion

            return returnData;                                                 
        }

        private static void SetTiaOnOff(TiaInstrument Tia, Inst_SMUTI_TriggeredSMU.OutputState outputState,double vcc_V,double vccComplanceCurrent_mA)
        {
//            Tia.VccSupply.OutputEnable = outputState;
            if (outputState== Inst_SMUTI_TriggeredSMU.OutputState .ON)
            {
  //              Tia.VccSupply.CurrentComplianceSetPoint_Amp = vccComplanceCurrent_mA / 1000;
  //              Tia.VccSupply.VoltageSetPoint_Volt = vcc_V;

            }
        }

        private double MeasureEachChipPdCurrentSum(bool is6464)
        {
            double pdCurrentSum = 0.0;
            if (hybirdChip.Contains("X"))
            {
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XIpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XQpos.CurrentActual_amp * 1000);
                if (!is6464)
                {
                    pdCurrentSum += (measSetupSig.PdBias.PdSource_XIneg.CurrentActual_amp * 1000);
                    pdCurrentSum += (measSetupSig.PdBias.PdSource_XQneg.CurrentActual_amp * 1000);
                }
            }
            else
            {
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YIpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YQpos.CurrentActual_amp * 1000 );
                if (!is6464)
                {
                    pdCurrentSum += (measSetupSig.PdBias.PdSource_YIneg.CurrentActual_amp * 1000);
                    pdCurrentSum += (measSetupSig.PdBias.PdSource_YQneg.CurrentActual_amp * 1000);
                }
            }
            return pdCurrentSum;
        }

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSetupOpticalPolarisationGui)); }
        }

        #region private variable

        MeasureSetupManage measSetupSig;
        MeasureSetupManage measSetupLoc;

        const double nullPower_dB = double.NegativeInfinity;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double powerTolerance_dB;
        double powerSetSOPC_Sig_dB;
        double powerSetSOPC_Loc_dB;

        double[] sopcCtrlVoltage_Sig;
        double[] sopcCtrlVoltage_Loc;

        double pdCurrentSum_Loc;
        double pdCurrentSum_Sig;

        string hybirdChip;
        MeasureSetupManage.HybirdChipEnum hybirdChipEnum;

        string serialNumber;

        string sopcAdjustTraceFileLoc;
        string sopcAdjustTraceFileSig;
        bool is6464;
       
        #endregion

        #endregion
    }
}

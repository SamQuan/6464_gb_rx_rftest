// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureGroupDelayDeviationGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using NPlot;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_MeasureGroupDelayDeviationGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_MeasureGroupDelayDeviationGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(Trace))
            //{
            {
                try
                {
                    Trace sweepData = (Trace)payload;
                        //this.MessageLabel.Text = "Completed " + sweepData.Type.ToString() + " sweep on " + sweepData.SrcMeter.ToString();

                    double[] xData;
                    double[] yData;
                    xData = sweepData.GetXArray();
                    yData = sweepData.GetYArray();

                    s21plot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "GroupDelayDeviation";

                    s21plot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    s21plot.XAxis1.Label = "Optical Frequency (GHz)";

                    if (s21plot.YAxis2 != null)
                        s21plot.YAxis2.Label = "Deviation(ps)";

                    this.s21plot.Visible = true;
                    s21plot.Refresh();
                }
                catch (SystemException)
                {
                    this.s21plot.Visible = false;
                    // Any GUI errors should not be fatal.
                }
            }
            else if (payload.GetType() == typeof(String))
            {
                this.MessageLabel.Text = (string)payload;
            }
        }
    }
}

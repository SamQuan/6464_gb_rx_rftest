// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureDLP.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;
using System.Collections;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureGroupDelayDeviation : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            string serialNumber = configData.ReadString("dutSerialNumber");
            string testStage = "";
            if (configData.IsPresent("testStage"))
            {
                testStage = configData.ReadString("testStage");
            }

            //  Dave Smith - Update 1 Start
            // Get the max freq to include in deviation analysis
            double AnalysisFrequency_GHz = configData.ReadDouble("GDDeviationMaxFreqGHz");
            // Dave Smith - Update 1   End

            string gDelayDeviationPlotFile = Util_GenerateFileName.GenWithTimestamp("results", "GroupDelayDeviation_", serialNumber, "csv");
            gDelayDeviationPlotFile = Directory.GetCurrentDirectory() + "\\" + gDelayDeviationPlotFile;

            listGroupDelay = (DatumList)configData.ReadListDatum("listGroupDelayData");
            skewTargetPointFreq_GHz = configData.ReadDouble("TargetPointSkewGDFreq_GHz");

            double[] freqArray_GHz = listGroupDelay.ReadDoubleArray("sweepFreqArray_GHz");
            double[] gdArray_XIpos = listGroupDelay.ReadDoubleArray("GDAarray_XIP");
            double[] gdArray_XIneg = listGroupDelay.ReadDoubleArray("GDAarray_XIN");
            double[] gdArray_XQpos = listGroupDelay.ReadDoubleArray("GDAarray_XQP");
            double[] gdArray_XQneg = listGroupDelay.ReadDoubleArray("GDAarray_XQN");
            double[] gdArray_YIpos = listGroupDelay.ReadDoubleArray("GDAarray_YIP");
            double[] gdArray_YIneg = listGroupDelay.ReadDoubleArray("GDAarray_YIN");
            double[] gdArray_YQpos = listGroupDelay.ReadDoubleArray("GDAarray_YQP");
            double[] gdArray_YQneg = listGroupDelay.ReadDoubleArray("GDAarray_YQN");

            int sweepPoints = gdArray_XIneg.Length;

            double[] AverageGDArray_XI = new double[sweepPoints];
            double[] AverageGDArray_XQ = new double[sweepPoints];
            double[] AverageGDArray_YI = new double[sweepPoints];
            double[] AverageGDArray_YQ = new double[sweepPoints];
            double[] AverageGDArray_X = new double[sweepPoints];
            double[] AverageGDArray_Y = new double[sweepPoints];
            double[] AverageGDArray_XY = new double[sweepPoints];

            double[] DeviationGDArray_XIpos = new double[sweepPoints];
            double[] DeviationGDArray_XIneg = new double[sweepPoints];
            double[] DeviationGDArray_XQpos = new double[sweepPoints];
            double[] DeviationGDArray_XQneg = new double[sweepPoints];
            double[] DeviationGDArray_YIpos = new double[sweepPoints];
            double[] DeviationGDArray_YIneg = new double[sweepPoints];
            double[] DeviationGDArray_YQpos = new double[sweepPoints];
            double[] DeviationGDArray_YQneg = new double[sweepPoints];
            double[] DeviationGDArray_XI = new double[sweepPoints];
            double[] DeviationGDArray_XQ = new double[sweepPoints];
            double[] DeviationGDArray_YI = new double[sweepPoints];
            double[] DeviationGDArray_YQ = new double[sweepPoints];
            double[] DeviationGDArray_X = new double[sweepPoints];
            double[] DeviationGDArray_Y = new double[sweepPoints];


            if (configData.IsPresent("dlpArrayData") || testStage == "rf_groupa")
            {
                DatumList tempDatumList = configData.ReadListDatum("dlpArrayData");
                listDlpArray.AddDoubleArray("freq_GHz", freqArray_GHz);
                listDlpArray.AddListItemsClone(tempDatumList);

                
                SaveGDDeviationData(gDelayDeviationPlotFile,listDlpArray);
            }

            //Calculate gd mean values
            for (int i = 0; i < sweepPoints; i++)
            {
                AverageGDArray_XI[i] = (gdArray_XIpos[i] + gdArray_XIneg[i]) / 2;
                AverageGDArray_XQ[i] = (gdArray_XQpos[i] + gdArray_XQneg[i]) / 2;
                AverageGDArray_YI[i] = (gdArray_YIpos[i] + gdArray_YIneg[i]) / 2;
                AverageGDArray_YQ[i] = (gdArray_YQpos[i] + gdArray_YQneg[i]) / 2;
            }

            //Calculate deviation of differential RF skew
            for (int i = 0; i < sweepPoints; i++)
            {
                DeviationGDArray_XIpos[i] = gdArray_XIpos[i] - AverageGDArray_XI[i];
                DeviationGDArray_XIneg[i] = gdArray_XIneg[i] - AverageGDArray_XI[i];
                DeviationGDArray_XQpos[i] = gdArray_XQpos[i] - AverageGDArray_XQ[i];
                DeviationGDArray_XQneg[i] = gdArray_XQneg[i] - AverageGDArray_XQ[i];
                DeviationGDArray_YIpos[i] = gdArray_YIpos[i] - AverageGDArray_YI[i];
                DeviationGDArray_YIneg[i] = gdArray_YIneg[i] - AverageGDArray_YI[i];
                DeviationGDArray_YQpos[i] = gdArray_YQpos[i] - AverageGDArray_YQ[i];
                DeviationGDArray_YQneg[i] = gdArray_YQneg[i] - AverageGDArray_YQ[i];
            }

            //Calculate deviation of inter-polarization GD skew
            for (int i = 0; i < sweepPoints; i++)
            {
                AverageGDArray_X[i] = (AverageGDArray_XI[i] + AverageGDArray_XQ[i]) / 2;
                AverageGDArray_Y[i] = (AverageGDArray_YI[i] + AverageGDArray_YQ[i]) / 2;
            }
            for (int i = 0; i < sweepPoints; i++)
            {
                DeviationGDArray_XI[i] = AverageGDArray_XI[i] - AverageGDArray_X[i];
                DeviationGDArray_XQ[i] = AverageGDArray_XQ[i] - AverageGDArray_X[i];
                DeviationGDArray_YI[i] = AverageGDArray_YI[i] - AverageGDArray_Y[i];
                DeviationGDArray_YQ[i] = AverageGDArray_YQ[i] - AverageGDArray_Y[i];
            }

            ////Calculate deviation of inter-polarization GD skew
            for (int i = 0; i < sweepPoints; i++)
            {
                AverageGDArray_XY[i] = (AverageGDArray_X[i] + AverageGDArray_Y[i]) / 2;
            }

            for (int i = 0; i < sweepPoints; i++)
            {
                DeviationGDArray_X[i] = AverageGDArray_X[i] - AverageGDArray_XY[i];
                DeviationGDArray_Y[i] = AverageGDArray_Y[i] - AverageGDArray_XY[i];
            }

            int index = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArray_GHz, skewTargetPointFreq_GHz);

            double deviationGD_XIP = DeviationGDArray_XIpos[index];
            double deviationGD_XIN = DeviationGDArray_XIneg[index];
            double deviationGD_XQP = DeviationGDArray_XQpos[index];
            double deviationGD_XQN = DeviationGDArray_XQneg[index];
            double deviationGD_YIP = DeviationGDArray_YIpos[index];
            double deviationGD_YIN = DeviationGDArray_YIneg[index];
            double deviationGD_YQP = DeviationGDArray_YQpos[index];
            double deviationGD_YQN = DeviationGDArray_YQneg[index];
            double deviationGD_XI = DeviationGDArray_XI[index];
            double deviationGD_XQ = DeviationGDArray_XQ[index];
            double deviationGD_YI = DeviationGDArray_YI[index];
            double deviationGD_YQ = DeviationGDArray_YQ[index];
            double deviationGD_X = DeviationGDArray_X[index];
            double deviationGD_Y = DeviationGDArray_Y[index];

            double deviationGD_XIP_MAX = DeviationGDArray_XIpos[index];
            double deviationGD_XIN_MAX = DeviationGDArray_XIneg[index];
            double deviationGD_XQP_MAX = DeviationGDArray_XQpos[index];
            double deviationGD_XQN_MAX = DeviationGDArray_XQneg[index];
            double deviationGD_YIP_MAX = DeviationGDArray_YIpos[index];
            double deviationGD_YIN_MAX = DeviationGDArray_YIneg[index];
            double deviationGD_YQP_MAX = DeviationGDArray_YQpos[index];
            double deviationGD_YQN_MAX = DeviationGDArray_YQneg[index];
            double deviationGD_XI_MAX = DeviationGDArray_XI[index];
            double deviationGD_XQ_MAX = DeviationGDArray_XQ[index];
            double deviationGD_YI_MAX = DeviationGDArray_YI[index];
            double deviationGD_YQ_MAX = DeviationGDArray_YQ[index];
            double deviationGD_X_MAX = DeviationGDArray_X[index];
            double deviationGD_Y_MAX = DeviationGDArray_Y[index];
            //finds max value upto 16GHz
            for (int i = 0; i < sweepPoints; i++)
            {

                //  Dave Smith - Update 1 Start                
                //if (freqArray_GHz[i] > 16)
                if (freqArray_GHz[i] > AnalysisFrequency_GHz)     // Dave Smith - Update 1   End
                {
                    break;
                }
                if (Math.Abs(DeviationGDArray_XIpos[i]) > Math.Abs(deviationGD_XIP_MAX))
                {
                    deviationGD_XIP_MAX = DeviationGDArray_XIpos[i];
                }
                if (Math.Abs(DeviationGDArray_XIneg[i]) > Math.Abs(deviationGD_XIN_MAX))
                {
                    deviationGD_XIN_MAX = DeviationGDArray_XIneg[i];
                }
                if (Math.Abs(DeviationGDArray_XQpos[i]) > Math.Abs(deviationGD_XQP_MAX))
                {
                    deviationGD_XQP_MAX = DeviationGDArray_XQpos[i];
                }
                if (Math.Abs(DeviationGDArray_XQneg[i]) > Math.Abs(deviationGD_XQN_MAX))
                {
                    deviationGD_XQN_MAX = DeviationGDArray_XQneg[i];
                }

                if (Math.Abs(DeviationGDArray_YIpos[i]) > Math.Abs(deviationGD_YIP_MAX))
                {
                    deviationGD_YIP_MAX = DeviationGDArray_YIpos[i];
                }
                if (Math.Abs(DeviationGDArray_YIneg[i]) > Math.Abs(deviationGD_YIN_MAX))
                {
                    deviationGD_YIN_MAX = DeviationGDArray_YIneg[i];
                }
                if (Math.Abs(DeviationGDArray_YQpos[i]) > Math.Abs(deviationGD_YQP_MAX))
                {
                    deviationGD_YQP_MAX = DeviationGDArray_YQpos[i];
                }
                if (Math.Abs(DeviationGDArray_YQneg[i]) > Math.Abs(deviationGD_YQN_MAX))
                {
                    deviationGD_YQN_MAX = DeviationGDArray_YQneg[i];
                }
                if (Math.Abs(DeviationGDArray_XI[i]) > Math.Abs(deviationGD_XI_MAX))
                {
                    deviationGD_XI_MAX = DeviationGDArray_XI[i];
                }
                if (Math.Abs(DeviationGDArray_XQ[i]) > Math.Abs(deviationGD_XQ_MAX))
                {
                    deviationGD_XQ_MAX = DeviationGDArray_XQ[i];
                }
                if (Math.Abs(DeviationGDArray_YI[index]) > Math.Abs(deviationGD_YI_MAX))
                {
                    deviationGD_YI_MAX = DeviationGDArray_YI[index];
                }
                if (Math.Abs(DeviationGDArray_YQ[index]) > Math.Abs(deviationGD_YQ_MAX))
                {
                    deviationGD_YQ_MAX = DeviationGDArray_YQ[index];
                }
                if (Math.Abs(DeviationGDArray_X[index]) > Math.Abs(deviationGD_X_MAX))
                {
                    deviationGD_X_MAX = DeviationGDArray_X[index];
                }
                if (Math.Abs(DeviationGDArray_Y[index]) > Math.Abs(deviationGD_Y_MAX))
                {
                    deviationGD_Y_MAX = DeviationGDArray_Y[index];
                }
            }

            //save group delay deviation data
            listDeviationArray.AddDoubleArray("Freq_GHz", freqArray_GHz);
            listDeviationArray.AddDoubleArray("DEV_GD_XIP",DeviationGDArray_XIpos);
            listDeviationArray.AddDoubleArray("DEV_GD_XQP",DeviationGDArray_XQpos);
            listDeviationArray.AddDoubleArray("DEV_GD_YIP",DeviationGDArray_YIpos);
            listDeviationArray.AddDoubleArray("DEV_GD_YQP",DeviationGDArray_YQpos);
            listDeviationArray.AddDoubleArray("DEV_GD_XI",DeviationGDArray_XI);
            listDeviationArray.AddDoubleArray("DEV_GD_YI",DeviationGDArray_YI);
            listDeviationArray.AddDoubleArray("DEV_GD_X", DeviationGDArray_X);

            SaveGDDeviationData(gDelayDeviationPlotFile, listDeviationArray);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("DEV_GD_XIP", deviationGD_XIP);
            returnData.AddDouble("DEV_GD_XIN", deviationGD_XIN);
            returnData.AddDouble("DEV_GD_XQP", deviationGD_XQP);
            returnData.AddDouble("DEV_GD_XQN", deviationGD_XQN);
            returnData.AddDouble("DEV_GD_YIP", deviationGD_YIP);
            returnData.AddDouble("DEV_GD_YIN", deviationGD_YIN);
            returnData.AddDouble("DEV_GD_YQP", deviationGD_YQP);
            returnData.AddDouble("DEV_GD_YQN", deviationGD_YQN);
            returnData.AddDouble("DEV_GD_XI", deviationGD_XI);
            returnData.AddDouble("DEV_GD_XQ", deviationGD_XQ);
            returnData.AddDouble("DEV_GD_YI", deviationGD_YI);
            returnData.AddDouble("DEV_GD_YQ", deviationGD_YQ);
            returnData.AddDouble("DEV_GD_X", deviationGD_X);
            returnData.AddDouble("DEV_GD_Y", deviationGD_Y);

            returnData.AddDouble("DEV_GD_XIP_MAX", deviationGD_XIP_MAX);
            returnData.AddDouble("DEV_GD_XIN_MAX", deviationGD_XIN_MAX);
            returnData.AddDouble("DEV_GD_XQP_MAX", deviationGD_XQP_MAX);
            returnData.AddDouble("DEV_GD_XQN_MAX", deviationGD_XQN_MAX);
            returnData.AddDouble("DEV_GD_YIP_MAX", deviationGD_YIP_MAX);
            returnData.AddDouble("DEV_GD_YIN_MAX", deviationGD_YIN_MAX);
            returnData.AddDouble("DEV_GD_YQP_MAX", deviationGD_YQP_MAX);
            returnData.AddDouble("DEV_GD_YQN_MAX", deviationGD_YQN_MAX);
            returnData.AddDouble("DEV_GD_XI_MAX", deviationGD_XI_MAX);
            returnData.AddDouble("DEV_GD_XQ_MAX", deviationGD_XQ_MAX);
            returnData.AddDouble("DEV_GD_YI_MAX", deviationGD_YI_MAX);
            returnData.AddDouble("DEV_GD_YQ_MAX", deviationGD_YQ_MAX);
            returnData.AddDouble("DEV_GD_X_MAX", deviationGD_X_MAX);
            returnData.AddDouble("DEV_GD_Y_MAX", deviationGD_Y_MAX);

            returnData.AddFileLink("gDDevPlotFile", gDelayDeviationPlotFile);

            if (testStage == "rf_groupa"||testStage=="rf_spc")
            {
                returnData.AddFileLink("DLP_GD_FILE", gDelayDeviationPlotFile);
            }
            

            return returnData;
        }

        #region private function

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="xArray"></param>
        /// <param name="yArray"></param>
        /// <returns></returns>
        private double[] DifferentiateArray(ITestEngine engine, double[] xArray, double[] yArray)
        {
            if (xArray.Length != yArray.Length)
            {
                engine.ErrorInModule("Invalid xArray and yArray data!");
            }
            List<double> listDiffPlot = new List<double>();
            for (int i = 0; i < yArray.Length; i++)
            {
                //Can't do full differential, so do half point differential
                if (i == 0)
                {
                    if (yArray[i + 1] - yArray[i] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i + 1] - yArray[i]) / (xArray[i + 1] - xArray[i]);
                        listDiffPlot.Add(diffValue);
                    }
                }
                    //Can't do full differential, so do half point differential
                else if (i == yArray.Length - 1)
                {
                    if (yArray[i] - yArray[i - 1] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i] - yArray[i - 1]) / (xArray[i] - xArray[i - 1]);
                        listDiffPlot.Add(diffValue);
                    }
                }
                else
                {
                    if (yArray[i+1] - yArray[i - 1] == 0)
                    {
                        listDiffPlot.Add(0.0);
                    }
                    else
                    {
                        double diffValue = (yArray[i + 1] - yArray[i - 1]) / (xArray[i + 1] - xArray[i - 1]);
                        listDiffPlot.Add(diffValue);
                    }
                }
            }

            double[] diffArray;
            diffArray=listDiffPlot.ToArray();
            return diffArray;

        }

        /// <summary>
        /// save group delay deviation to .csv file
        /// </summary>
        /// <param name="xyDataFile">xyData file name</param>
        /// <param name="gDArray">group daley data</param>
        private static void SaveGDDeviationData(string fileName,DatumList listGDDevRawData)
        {
            string plotHead = "";
            string[] plotContent;
            ArrayList datumNames = listGDDevRawData.GetDatumNameList();
            for (int i = 0; i < datumNames.Count; i++)
            {
                plotHead += datumNames[i].ToString();
                plotHead += ",";

            }
            plotHead = plotHead.Substring(0, plotHead.Length - 1);
            int intCount = listGDDevRawData.ReadDoubleArray(datumNames[0].ToString()).Length;
            plotContent = new string[intCount];
            for (int i = 0; i < intCount; i++)
            {
                for (int j = 0; j < datumNames.Count; j++)
                {
                    double[] tempArray = listGDDevRawData.ReadDoubleArray(datumNames[j].ToString());
                    if (tempArray[i].ToString() == "NaN")
                    {
                        plotContent[i] += "";
                    }
                    else
                    {
                        plotContent[i] += tempArray[i].ToString();
                    }
                    plotContent[i] += ",";
                }
                plotContent[i].Substring(0, plotContent[i].Length - 1);
            }

            Util_SavePlotData savePlotData = new Util_SavePlotData();
            savePlotData.AppendResultToFile(fileName, plotHead, plotContent);
        }

        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureGroupDelayDeviationGui)); }
        }

        #region private variable


        //InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s21PhaseTrace = new Trace();

        Trace s21RealAndImaginaryTrace = new Trace();

        DatumList listGroupDelay = new DatumList();
        DatumList listDeviationArray = new DatumList();
        DatumList listDlpArray = new DatumList();

        double skewTargetPointFreq_GHz = 0.0;
        #endregion

        #endregion
    }
}

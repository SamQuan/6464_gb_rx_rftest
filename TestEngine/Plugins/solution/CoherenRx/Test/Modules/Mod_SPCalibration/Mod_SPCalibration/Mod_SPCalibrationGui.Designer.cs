// [Copyright]
//
// Bookham Test Engine
// Mod_SPCalibration
//
// Bookham.TestSolution.TestModules/Mod_SPCalibrationGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class Mod_SPCalibrationGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxS22Open = new System.Windows.Forms.CheckBox();
            this.buttonS22Open = new System.Windows.Forms.Button();
            this.panelS22 = new System.Windows.Forms.Panel();
            this.labelS22Information = new System.Windows.Forms.Label();
            this.labelS22 = new System.Windows.Forms.Label();
            this.buttonS22Load = new System.Windows.Forms.Button();
            this.checkBoxS22Load = new System.Windows.Forms.CheckBox();
            this.buttonS22Short = new System.Windows.Forms.Button();
            this.checkBoxS22Short = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageS11 = new System.Windows.Forms.TabPage();
            this.labelS11Information = new System.Windows.Forms.Label();
            this.labelS11 = new System.Windows.Forms.Label();
            this.buttonS11Load = new System.Windows.Forms.Button();
            this.checkBoxS11Load = new System.Windows.Forms.CheckBox();
            this.buttonS11Short = new System.Windows.Forms.Button();
            this.checkBoxS11Short = new System.Windows.Forms.CheckBox();
            this.buttonS11Open = new System.Windows.Forms.Button();
            this.checkBoxS11Open = new System.Windows.Forms.CheckBox();
            this.tabPageS22 = new System.Windows.Forms.TabPage();
            this.tabPageS21 = new System.Windows.Forms.TabPage();
            this.panelS21 = new System.Windows.Forms.Panel();
            this.labelS21Note = new System.Windows.Forms.Label();
            this.labelS21 = new System.Windows.Forms.Label();
            this.buttonS21Done = new System.Windows.Forms.Button();
            this.buttonThru = new System.Windows.Forms.Button();
            this.checkBoxThru = new System.Windows.Forms.CheckBox();
            this.labelCalInfor = new System.Windows.Forms.Label();
            this.labelFinalStatus = new System.Windows.Forms.Label();
            this.labelFinalStatusResult = new System.Windows.Forms.Label();
            this.pictureBoxPic = new System.Windows.Forms.PictureBox();
            this.panelS22.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageS11.SuspendLayout();
            this.tabPageS22.SuspendLayout();
            this.tabPageS21.SuspendLayout();
            this.panelS21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPic)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxS22Open
            // 
            this.checkBoxS22Open.Location = new System.Drawing.Point(206, 36);
            this.checkBoxS22Open.Name = "checkBoxS22Open";
            this.checkBoxS22Open.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Open.TabIndex = 1;
            this.checkBoxS22Open.Text = "Calibrated";
            this.checkBoxS22Open.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Open.UseVisualStyleBackColor = false;
            this.checkBoxS22Open.CheckedChanged += new System.EventHandler(this.checkBoxS22Open_CheckedChanged);
            // 
            // buttonS22Open
            // 
            this.buttonS22Open.Location = new System.Drawing.Point(80, 36);
            this.buttonS22Open.Name = "buttonS22Open";
            this.buttonS22Open.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Open.TabIndex = 2;
            this.buttonS22Open.Text = "Open";
            this.buttonS22Open.UseVisualStyleBackColor = true;
            this.buttonS22Open.Click += new System.EventHandler(this.buttonS22Open_Click);
            // 
            // panelS22
            // 
            this.panelS22.Controls.Add(this.labelS22Information);
            this.panelS22.Controls.Add(this.labelS22);
            this.panelS22.Controls.Add(this.buttonS22Load);
            this.panelS22.Controls.Add(this.checkBoxS22Load);
            this.panelS22.Controls.Add(this.buttonS22Short);
            this.panelS22.Controls.Add(this.checkBoxS22Short);
            this.panelS22.Controls.Add(this.buttonS22Open);
            this.panelS22.Controls.Add(this.checkBoxS22Open);
            this.panelS22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelS22.Location = new System.Drawing.Point(3, 3);
            this.panelS22.Name = "panelS22";
            this.panelS22.Size = new System.Drawing.Size(544, 388);
            this.panelS22.TabIndex = 3;
            this.panelS22.Tag = "";
            // 
            // labelS22Information
            // 
            this.labelS22Information.Location = new System.Drawing.Point(42, 186);
            this.labelS22Information.Name = "labelS22Information";
            this.labelS22Information.Size = new System.Drawing.Size(276, 74);
            this.labelS22Information.TabIndex = 9;
            this.labelS22Information.Text = "Please set up the equipment as indicated then click on the buttons above to calib" +
                "rate that particular standard";
            // 
            // labelS22
            // 
            this.labelS22.AutoSize = true;
            this.labelS22.Location = new System.Drawing.Point(144, 7);
            this.labelS22.Name = "labelS22";
            this.labelS22.Size = new System.Drawing.Size(38, 20);
            this.labelS22.TabIndex = 8;
            this.labelS22.Text = "S22";
            // 
            // buttonS22Load
            // 
            this.buttonS22Load.Enabled = false;
            this.buttonS22Load.Location = new System.Drawing.Point(80, 131);
            this.buttonS22Load.Name = "buttonS22Load";
            this.buttonS22Load.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Load.TabIndex = 6;
            this.buttonS22Load.Text = "Load";
            this.buttonS22Load.UseVisualStyleBackColor = true;
            this.buttonS22Load.Click += new System.EventHandler(this.buttonS22Load_Click);
            // 
            // checkBoxS22Load
            // 
            this.checkBoxS22Load.Enabled = false;
            this.checkBoxS22Load.Location = new System.Drawing.Point(206, 131);
            this.checkBoxS22Load.Name = "checkBoxS22Load";
            this.checkBoxS22Load.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Load.TabIndex = 5;
            this.checkBoxS22Load.Text = "Calibrated";
            this.checkBoxS22Load.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Load.UseVisualStyleBackColor = false;
            this.checkBoxS22Load.CheckedChanged += new System.EventHandler(this.checkBoxS22Load_CheckedChanged);
            // 
            // buttonS22Short
            // 
            this.buttonS22Short.Enabled = false;
            this.buttonS22Short.Location = new System.Drawing.Point(80, 84);
            this.buttonS22Short.Name = "buttonS22Short";
            this.buttonS22Short.Size = new System.Drawing.Size(120, 32);
            this.buttonS22Short.TabIndex = 4;
            this.buttonS22Short.Text = "Short";
            this.buttonS22Short.UseVisualStyleBackColor = true;
            this.buttonS22Short.Click += new System.EventHandler(this.buttonS22Short_Click);
            // 
            // checkBoxS22Short
            // 
            this.checkBoxS22Short.Enabled = false;
            this.checkBoxS22Short.Location = new System.Drawing.Point(206, 84);
            this.checkBoxS22Short.Name = "checkBoxS22Short";
            this.checkBoxS22Short.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS22Short.TabIndex = 3;
            this.checkBoxS22Short.Text = "Calibrated";
            this.checkBoxS22Short.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS22Short.UseVisualStyleBackColor = false;
            this.checkBoxS22Short.CheckedChanged += new System.EventHandler(this.checkBoxS22Short_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 48);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.Controls.Add(this.pictureBoxPic);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AllowDrop = true;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl);
            this.splitContainer1.Size = new System.Drawing.Size(958, 427);
            this.splitContainer1.SplitterDistance = 396;
            this.splitContainer1.TabIndex = 4;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageS11);
            this.tabControl.Controls.Add(this.tabPageS22);
            this.tabControl.Controls.Add(this.tabPageS21);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(558, 427);
            this.tabControl.TabIndex = 6;
            // 
            // tabPageS11
            // 
            this.tabPageS11.Controls.Add(this.labelS11Information);
            this.tabPageS11.Controls.Add(this.labelS11);
            this.tabPageS11.Controls.Add(this.buttonS11Load);
            this.tabPageS11.Controls.Add(this.checkBoxS11Load);
            this.tabPageS11.Controls.Add(this.buttonS11Short);
            this.tabPageS11.Controls.Add(this.checkBoxS11Short);
            this.tabPageS11.Controls.Add(this.buttonS11Open);
            this.tabPageS11.Controls.Add(this.checkBoxS11Open);
            this.tabPageS11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageS11.Location = new System.Drawing.Point(4, 29);
            this.tabPageS11.Name = "tabPageS11";
            this.tabPageS11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageS11.Size = new System.Drawing.Size(550, 394);
            this.tabPageS11.TabIndex = 4;
            this.tabPageS11.Text = "S11";
            this.tabPageS11.UseVisualStyleBackColor = true;
            // 
            // labelS11Information
            // 
            this.labelS11Information.Location = new System.Drawing.Point(54, 178);
            this.labelS11Information.Name = "labelS11Information";
            this.labelS11Information.Size = new System.Drawing.Size(310, 64);
            this.labelS11Information.TabIndex = 19;
            this.labelS11Information.Text = "Please set up the equipment as indicated then click on the buttons above to calib" +
                "rate that particular standard";
            // 
            // labelS11
            // 
            this.labelS11.AutoSize = true;
            this.labelS11.Location = new System.Drawing.Point(152, 6);
            this.labelS11.Name = "labelS11";
            this.labelS11.Size = new System.Drawing.Size(38, 20);
            this.labelS11.TabIndex = 18;
            this.labelS11.Text = "S11";
            // 
            // buttonS11Load
            // 
            this.buttonS11Load.Enabled = false;
            this.buttonS11Load.Location = new System.Drawing.Point(86, 130);
            this.buttonS11Load.Name = "buttonS11Load";
            this.buttonS11Load.Size = new System.Drawing.Size(122, 32);
            this.buttonS11Load.TabIndex = 16;
            this.buttonS11Load.Text = "Load";
            this.buttonS11Load.UseVisualStyleBackColor = true;
            this.buttonS11Load.Click += new System.EventHandler(this.buttonS11Load_Click);
            // 
            // checkBoxS11Load
            // 
            this.checkBoxS11Load.Enabled = false;
            this.checkBoxS11Load.Location = new System.Drawing.Point(214, 130);
            this.checkBoxS11Load.Name = "checkBoxS11Load";
            this.checkBoxS11Load.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS11Load.TabIndex = 15;
            this.checkBoxS11Load.Text = "Calibrated";
            this.checkBoxS11Load.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS11Load.UseVisualStyleBackColor = false;
            this.checkBoxS11Load.CheckedChanged += new System.EventHandler(this.checkBoxS11Load_CheckedChanged);
            // 
            // buttonS11Short
            // 
            this.buttonS11Short.Enabled = false;
            this.buttonS11Short.Location = new System.Drawing.Point(86, 83);
            this.buttonS11Short.Name = "buttonS11Short";
            this.buttonS11Short.Size = new System.Drawing.Size(122, 32);
            this.buttonS11Short.TabIndex = 14;
            this.buttonS11Short.Text = "Short";
            this.buttonS11Short.UseVisualStyleBackColor = true;
            this.buttonS11Short.Click += new System.EventHandler(this.buttonS11Short_Click);
            // 
            // checkBoxS11Short
            // 
            this.checkBoxS11Short.Enabled = false;
            this.checkBoxS11Short.Location = new System.Drawing.Point(214, 83);
            this.checkBoxS11Short.Name = "checkBoxS11Short";
            this.checkBoxS11Short.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS11Short.TabIndex = 13;
            this.checkBoxS11Short.Text = "Calibrated";
            this.checkBoxS11Short.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS11Short.UseVisualStyleBackColor = false;
            this.checkBoxS11Short.CheckedChanged += new System.EventHandler(this.checkBoxS11Short_CheckedChanged);
            // 
            // buttonS11Open
            // 
            this.buttonS11Open.Location = new System.Drawing.Point(86, 35);
            this.buttonS11Open.Name = "buttonS11Open";
            this.buttonS11Open.Size = new System.Drawing.Size(122, 32);
            this.buttonS11Open.TabIndex = 12;
            this.buttonS11Open.Text = "Open";
            this.buttonS11Open.UseVisualStyleBackColor = true;
            this.buttonS11Open.Click += new System.EventHandler(this.buttonS11Open_Click);
            // 
            // checkBoxS11Open
            // 
            this.checkBoxS11Open.Location = new System.Drawing.Point(214, 35);
            this.checkBoxS11Open.Name = "checkBoxS11Open";
            this.checkBoxS11Open.Size = new System.Drawing.Size(102, 32);
            this.checkBoxS11Open.TabIndex = 11;
            this.checkBoxS11Open.Text = "Calibrated";
            this.checkBoxS11Open.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxS11Open.UseVisualStyleBackColor = false;
            this.checkBoxS11Open.CheckedChanged += new System.EventHandler(this.checkBoxS11Open_CheckedChanged);
            // 
            // tabPageS22
            // 
            this.tabPageS22.Controls.Add(this.panelS22);
            this.tabPageS22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageS22.Location = new System.Drawing.Point(4, 29);
            this.tabPageS22.Name = "tabPageS22";
            this.tabPageS22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageS22.Size = new System.Drawing.Size(550, 394);
            this.tabPageS22.TabIndex = 0;
            this.tabPageS22.Text = "S22";
            this.tabPageS22.UseVisualStyleBackColor = true;
            // 
            // tabPageS21
            // 
            this.tabPageS21.Controls.Add(this.panelS21);
            this.tabPageS21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageS21.Location = new System.Drawing.Point(4, 29);
            this.tabPageS21.Name = "tabPageS21";
            this.tabPageS21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageS21.Size = new System.Drawing.Size(550, 394);
            this.tabPageS21.TabIndex = 1;
            this.tabPageS21.Text = "S21";
            this.tabPageS21.UseVisualStyleBackColor = true;
            // 
            // panelS21
            // 
            this.panelS21.Controls.Add(this.labelS21Note);
            this.panelS21.Controls.Add(this.labelS21);
            this.panelS21.Controls.Add(this.buttonS21Done);
            this.panelS21.Controls.Add(this.buttonThru);
            this.panelS21.Controls.Add(this.checkBoxThru);
            this.panelS21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelS21.Location = new System.Drawing.Point(3, 3);
            this.panelS21.Name = "panelS21";
            this.panelS21.Size = new System.Drawing.Size(544, 388);
            this.panelS21.TabIndex = 11;
            this.panelS21.Tag = "";
            // 
            // labelS21Note
            // 
            this.labelS21Note.Location = new System.Drawing.Point(39, 140);
            this.labelS21Note.Name = "labelS21Note";
            this.labelS21Note.Size = new System.Drawing.Size(276, 78);
            this.labelS21Note.TabIndex = 10;
            this.labelS21Note.Text = "NOTE: Ensure all the Calibration have been done when pressing \'Done\'";
            this.labelS21Note.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelS21
            // 
            this.labelS21.AutoSize = true;
            this.labelS21.Location = new System.Drawing.Point(149, 10);
            this.labelS21.Name = "labelS21";
            this.labelS21.Size = new System.Drawing.Size(38, 20);
            this.labelS21.TabIndex = 8;
            this.labelS21.Text = "S21";
            // 
            // buttonS21Done
            // 
            this.buttonS21Done.Enabled = false;
            this.buttonS21Done.Location = new System.Drawing.Point(88, 302);
            this.buttonS21Done.Name = "buttonS21Done";
            this.buttonS21Done.Size = new System.Drawing.Size(130, 50);
            this.buttonS21Done.TabIndex = 7;
            this.buttonS21Done.Text = "Done";
            this.buttonS21Done.UseVisualStyleBackColor = true;
            this.buttonS21Done.Click += new System.EventHandler(this.buttonS21Done_Click);
            // 
            // buttonThru
            // 
            this.buttonThru.Enabled = false;
            this.buttonThru.Location = new System.Drawing.Point(74, 39);
            this.buttonThru.Name = "buttonThru";
            this.buttonThru.Size = new System.Drawing.Size(121, 32);
            this.buttonThru.TabIndex = 2;
            this.buttonThru.Text = "Thru";
            this.buttonThru.UseVisualStyleBackColor = true;
            this.buttonThru.Click += new System.EventHandler(this.buttonThru_Click);
            // 
            // checkBoxThru
            // 
            this.checkBoxThru.Enabled = false;
            this.checkBoxThru.Location = new System.Drawing.Point(201, 39);
            this.checkBoxThru.Name = "checkBoxThru";
            this.checkBoxThru.Size = new System.Drawing.Size(105, 32);
            this.checkBoxThru.TabIndex = 1;
            this.checkBoxThru.Text = "Calibrated";
            this.checkBoxThru.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxThru.UseVisualStyleBackColor = false;
            this.checkBoxThru.CheckedChanged += new System.EventHandler(this.checkBoxThru_CheckedChanged);
            // 
            // labelCalInfor
            // 
            this.labelCalInfor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCalInfor.Location = new System.Drawing.Point(73, 12);
            this.labelCalInfor.Name = "labelCalInfor";
            this.labelCalInfor.Size = new System.Drawing.Size(680, 23);
            this.labelCalInfor.TabIndex = 5;
            this.labelCalInfor.Text = "Welcome to Calibrate VNA!";
            this.labelCalInfor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFinalStatus
            // 
            this.labelFinalStatus.AutoSize = true;
            this.labelFinalStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFinalStatus.Location = new System.Drawing.Point(3, 478);
            this.labelFinalStatus.Name = "labelFinalStatus";
            this.labelFinalStatus.Size = new System.Drawing.Size(61, 13);
            this.labelFinalStatus.TabIndex = 6;
            this.labelFinalStatus.Text = "Test Status";
            // 
            // labelFinalStatusResult
            // 
            this.labelFinalStatusResult.AutoSize = true;
            this.labelFinalStatusResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFinalStatusResult.Location = new System.Drawing.Point(75, 478);
            this.labelFinalStatusResult.Name = "labelFinalStatusResult";
            this.labelFinalStatusResult.Size = new System.Drawing.Size(43, 13);
            this.labelFinalStatusResult.TabIndex = 7;
            this.labelFinalStatusResult.Text = "Starting";
            // 
            // pictureBoxPic
            // 
            this.pictureBoxPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S11Diag;
            this.pictureBoxPic.InitialImage = global::Mod_SPCalibration.Properties.Resources.S11Diag;
            this.pictureBoxPic.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPic.Name = "pictureBoxPic";
            this.pictureBoxPic.Size = new System.Drawing.Size(396, 427);
            this.pictureBoxPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPic.TabIndex = 8;
            this.pictureBoxPic.TabStop = false;
            // 
            // Mod_SPCalibrationGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelFinalStatusResult);
            this.Controls.Add(this.labelFinalStatus);
            this.Controls.Add(this.labelCalInfor);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Mod_SPCalibrationGui";
            this.Size = new System.Drawing.Size(1194, 789);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.panelS22.ResumeLayout(false);
            this.panelS22.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPageS11.ResumeLayout(false);
            this.tabPageS11.PerformLayout();
            this.tabPageS22.ResumeLayout(false);
            this.tabPageS21.ResumeLayout(false);
            this.panelS21.ResumeLayout(false);
            this.panelS21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxS22Open;
        private System.Windows.Forms.Button buttonS22Open;
        private System.Windows.Forms.Panel panelS22;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBoxPic;
        private System.Windows.Forms.Button buttonS22Load;
        private System.Windows.Forms.CheckBox checkBoxS22Load;
        private System.Windows.Forms.Button buttonS22Short;
        private System.Windows.Forms.CheckBox checkBoxS22Short;
        private System.Windows.Forms.Label labelS22;
        private System.Windows.Forms.Label labelS22Information;
        private System.Windows.Forms.Panel panelS21;
        private System.Windows.Forms.Label labelS21Note;
        private System.Windows.Forms.Label labelS21;
        private System.Windows.Forms.Button buttonS21Done;
        private System.Windows.Forms.Button buttonThru;
        private System.Windows.Forms.CheckBox checkBoxThru;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageS22;
        private System.Windows.Forms.TabPage tabPageS21;
        internal System.Windows.Forms.Label labelCalInfor;
        private System.Windows.Forms.Label labelFinalStatus;
        private System.Windows.Forms.Label labelFinalStatusResult;
        private System.Windows.Forms.TabPage tabPageS11;
        private System.Windows.Forms.Label labelS11Information;
        private System.Windows.Forms.Label labelS11;
        private System.Windows.Forms.Button buttonS11Load;
        private System.Windows.Forms.CheckBox checkBoxS11Load;
        private System.Windows.Forms.Button buttonS11Short;
        private System.Windows.Forms.CheckBox checkBoxS11Short;
        private System.Windows.Forms.Button buttonS11Open;
        private System.Windows.Forms.CheckBox checkBoxS11Open;
    }
}

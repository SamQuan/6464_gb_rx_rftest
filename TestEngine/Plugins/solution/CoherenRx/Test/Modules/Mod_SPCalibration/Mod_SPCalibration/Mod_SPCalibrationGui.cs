// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_SPCalibrationGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_SPCalibrationGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_SPCalibrationGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {

            if (payload.GetType() == typeof(string))
            {
                string str = labelCalInfor.Text;
                str += "----Current Chip is " + (string)payload;
                labelCalInfor.Text = str;
                this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S11Diag;
            }
            else if(payload.GetType()==typeof(ConnCalKit))
            {
                ConnCalKit connCalKit = (ConnCalKit)payload;
                Image image = this.pictureBoxPic.Image;
                //show connet cal kit picture
                this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S11Diag;
                MessageBox.Show(connCalKit.ConnCalKitMessage);
                connCalKit.continueCal = true;
                sendToWorker(connCalKit.continueCal);
            }
            else if (payload.GetType() == typeof(ControlData))
            {
                ControlData recCtrlData = (ControlData)payload;

                switch (recCtrlData.PressedButton)
                {

                    case ButtonName.S11Open:
                        if (recCtrlData.S11OpenCalibrated)
                        {
                            labelFinalStatusResult.Text = "Ongoing......";

                            checkBoxS11Open.CheckState = CheckState.Checked;
                            checkBoxS11Open.Enabled = false;
                            buttonS11Open.Enabled = false;

                            checkBoxS11Short.Enabled = true;
                            buttonS11Short.Enabled = true;
                        }
                        break;
                    case ButtonName.S11Short:
                        {
                            checkBoxS11Short.CheckState = CheckState.Checked;
                            checkBoxS11Short.Enabled = false;
                            buttonS11Short.Enabled = false;

                            checkBoxS11Load.Enabled = true;
                            buttonS11Load.Enabled = true;
                        }
                        break;
                    case ButtonName.S11Load:
                        {
                            checkBoxS11Load.CheckState = CheckState.Checked;
                            checkBoxS11Load.Enabled = false;
                            buttonS11Load.Enabled = false;

                            //buttonS11Done.Enabled = true;
                            this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S22Diag;
                            this.tabControl.SelectedIndex = 1;

                            buttonS22Open.Enabled = true;
                            checkBoxS22Open.Enabled = true;
                        }
                        break;


                    case ButtonName.S22Open:
                        if (recCtrlData.S22OpenCalibrated)
                        {
                            labelFinalStatusResult.Text = "Ongoing......";

                            checkBoxS22Open.CheckState = CheckState.Checked;
                            checkBoxS22Open.Enabled = false;
                            buttonS22Open.Enabled = false;

                            checkBoxS22Short.Enabled = true;
                            buttonS22Short.Enabled = true;
                        }
                        break;
                    case ButtonName.S22Short:
                        {
                            checkBoxS22Short.CheckState = CheckState.Checked;
                            checkBoxS22Short.Enabled = false;
                            buttonS22Short.Enabled = false;

                            checkBoxS22Load.Enabled = true;
                            buttonS22Load.Enabled = true;
                        }
                        break;
                    case ButtonName.S22Load:
                        {
                            checkBoxS22Load.CheckState = CheckState.Checked;
                            checkBoxS22Load.Enabled = false;
                            buttonS22Load.Enabled = false;

                            this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S21Diag;
                            this.tabControl.SelectedIndex = 2;

                            checkBoxThru.Enabled = true;
                            buttonThru.Enabled = true;
                        }
                        break;
                    case ButtonName.S21Thru:
                        if (recCtrlData.S21ThruCalibrated)
                        {
                            checkBoxThru.CheckState = CheckState.Checked;
                            checkBoxThru.Enabled = false;
                            buttonThru.Enabled = false;

                            buttonS21Done.Enabled = true;
                        }
                        break;

                    //case ButtonName.S11Done:
                    //    if (recCtrlData.S11LoadCalibrated && recCtrlData.S11OpenCalibrated &&
                    //        recCtrlData.S11ShortCalibrated)
                    //    {
                    //        buttonS11Done.Enabled = false;

                    //        this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S11Diag;
                    //        this.tabControl.SelectedIndex = 1;

                    //        buttonS22Open.Enabled = true;
                    //        checkBoxS22Open.Enabled = true;

                    //    }
                    //    break;
                    //case ButtonName.S22Done:
                    //    if (recCtrlData.S22LoadCalibrated && recCtrlData.S22OpenCalibrated &&
                    //        recCtrlData.S22ShortCalibrated)
                    //    {
                    //        buttonS22Done.Enabled = false;

                    //        this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.S21Diag;
                    //        this.tabControl.SelectedIndex = 2;

                    //        checkBoxThru.Enabled = true;
                    //        buttonThru.Enabled = true;
                    //    }
                    //    break;
                    case ButtonName.S21Done:
                        if (recCtrlData.S22LoadCalibrated && recCtrlData.S22OpenCalibrated &&
                            recCtrlData.S22ShortCalibrated && recCtrlData.S21ThruCalibrated)
                        {
                            buttonS21Done.Enabled = false;

                            this.pictureBoxPic.Image = global::Mod_SPCalibration.Properties.Resources.De_Emb;
                            this.tabControl.SelectedIndex = 3;
                        }
                        break;
                    default:
                        labelFinalStatusResult.Text = "Error Break";
                        throw new TypeLoadException("The type of GUI recieve is Wrong!");
                }
            }
            else
            {
                throw new TypeLoadException("The type of the message GUI sent is Wrong!");
            }
        }

        private void buttonS11Open_Click(object sender, EventArgs e)
        {
            cmdS11Open = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S11Open;

            this.sendToWorker(commandType);
        }

        private void buttonS11Short_Click(object sender, EventArgs e)
        {
            cmdS11Short = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S11Short;

            this.sendToWorker(commandType);
        }

        private void buttonS11Load_Click(object sender, EventArgs e)
        {
            cmdS11Load = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S11Load;

            this.sendToWorker(commandType);
        }

        //private void buttonS11Done_Click(object sender, EventArgs e)
        //{
        //    DisableAllButton();

        //    commandType.PressedButton = ButtonName.S11Done;

        //    this.sendToWorker(commandType);
        //}

        private void buttonS22Open_Click(object sender, EventArgs e)
        {
            cmdS22Open = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Open;
            
            this.sendToWorker(commandType);
        }

        private void buttonS22Short_Click(object sender, EventArgs e)
        {
            cmdS22Short = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Short;
            
            this.sendToWorker(commandType);
        }

        private void buttonS22Load_Click(object sender, EventArgs e)
        {
            cmdS22Load = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S22Load;

            this.sendToWorker(commandType);
        }

        //private void buttonS22Done_Click(object sender, EventArgs e)
        //{
        //    DisableAllButton();

        //    commandType.PressedButton = ButtonName.S22Done;

        //    this.sendToWorker(commandType);
        //}

        private void buttonThru_Click(object sender, EventArgs e)
        {
            cmdS21Thru = true;

            DisableAllButton();

            commandType.PressedButton = ButtonName.S21Thru;

            this.sendToWorker(commandType); 
        }

        private void buttonS21Done_Click(object sender, EventArgs e)
        {
            DisableAllButton();

            commandType.PressedButton = ButtonName.S21Done;

            this.sendToWorker(commandType);
        }

        private void DisableAllButton()
        {
            buttonS11Open.Enabled = false;
            buttonS11Short.Enabled = false;
            buttonS11Load.Enabled = false;
            //buttonS11Done.Enabled = false;
            buttonS22Open.Enabled = false;
            buttonS22Short.Enabled = false;
            buttonS22Load.Enabled = false;
            //buttonS22Done.Enabled = false;
            buttonThru.Enabled = false;
            buttonS21Done.Enabled = false;

        }

        private void EnableAllButton()
        {
            buttonS11Open.Enabled = true;
            buttonS11Short.Enabled = true;
            buttonS11Load.Enabled = true;
            //buttonS11Done.Enabled = true;
            buttonS22Open.Enabled = true;
            buttonS22Short.Enabled = true;
            buttonS22Load.Enabled = true;
            //buttonS22Done.Enabled = true;
            buttonThru.Enabled = true;
            buttonS21Done.Enabled = true;
        }

        /// <summary>
        /// This subroutine is to make sure the checkbox indicates
        /// correctly wether the appropriate cal has been performed or not
        /// </summary>
        /// <param name="NameofBotton"></param>
        /// <returns></returns>
        private CheckState CheckButtonClick(ButtonName nameofBotton)
        {
            CheckState rtnValue;
            switch (nameofBotton)
            {
                case ButtonName.S11Open:
                    if (cmdS11Open)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S11Short:
                    if (cmdS11Short)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S11Load:
                    if (cmdS11Load)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Open:
                    if (cmdS22Open)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Short:
                    if (cmdS22Short)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S22Load:
                    if (cmdS22Load)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;
                case ButtonName.S21Thru:
                    if (cmdS21Thru)
                    {
                        rtnValue = CheckState.Checked;
                    }
                    else
                    {
                        rtnValue = CheckState.Unchecked;
                    }
                    break;

                //case ButtonName.S11Done:
                //case ButtonName.S22Done:
                case ButtonName.S21Done:
                default:
                    rtnValue = CheckState.Indeterminate;
                    break;
            }

            return rtnValue;
        }

        private void checkBoxS11Open_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS11Open.CheckState = CheckButtonClick(ButtonName.S11Open);
        }

        private void checkBoxS11Short_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS11Short.CheckState = CheckButtonClick(ButtonName.S11Short);
        }

        private void checkBoxS11Load_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS11Load.CheckState = CheckButtonClick(ButtonName.S11Load);
        }

        private void checkBoxS22Open_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Open.CheckState = CheckButtonClick(ButtonName.S22Open);
        }

        private void checkBoxS22Short_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Short.CheckState = CheckButtonClick(ButtonName.S22Short);
        }

        private void checkBoxS22Load_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxS22Load.CheckState = CheckButtonClick(ButtonName.S22Load);
        }

        private void checkBoxThru_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxThru.CheckState = CheckButtonClick(ButtonName.S21Thru);
        }

        private bool cmdS11Open = false;
        private bool cmdS11Short = false;
        private bool cmdS11Load = false;
        private bool cmdS22Open = false;
        private bool cmdS22Short = false;
        private bool cmdS22Load = false;
        private bool cmdS21Thru = false;

        private ControlData commandType = new ControlData();



        



        

    }

   
}

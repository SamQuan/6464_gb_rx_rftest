// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_SPCalibration.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
//using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.Framework.Messages;
using Bookham.TestLibrary.InstrTypes;
using System.Threading;
using Bookham.TestLibrary.Utilities;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using System.IO;
using Bookham.TestSolution.CoherentRxTestCommonData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_SPCalibration : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments,
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // TODO - add your code here!
            bool calibrationFinished = false;
            bool is6464 = configData.ReadBool("is6464");

            InstType_OpticalPowerMeter OPMeter = (InstType_OpticalPowerMeter)instruments["OPMeter"];

            MeasureSetupManage sigChainSetupManage = (MeasureSetupManage)configData.ReadReference("sigChainSetupManage");

            double spliterRatio=configData.ReadDouble("spliterRatio_Sig");
            double powerTolerance_dB=configData.ReadDouble("powerTolerance_dB");

            Instr_MS4640A_VNA LCAnalyzer = (Instr_MS4640A_VNA)instruments["LCAnalyzer"];

            Switch_LCA_RF RfSwitch = (Switch_LCA_RF)configData.ReadReference("RfSwitch");

            string hybirdChip = (string)configData.ReadString("hybridChip");

            MsgContainer recieveData;

            ConfigData cfgData = new ConfigData(configData);

            ControlData sendCtrlData;

            
            engine.GuiToFront();

            engine.SendToGui(hybirdChip);

            if (!engine.IsSimulation)
            {
                LCAnalyzer.ChannelCount = 1;
                LCAnalyzer.ActiveMeausurementChannel = 1;
                LCAnalyzer.SourceOutputPower_dBm = configData.ReadDouble("SourcePower_dBm");
            }

            sigChainSetupManage.SetOpticalInputPower(engine, -6, spliterRatio, powerTolerance_dB);
            engine.GuiShow();

            do
            {
                recieveData = engine.ReceiveFromGui();

                if (recieveData.Payload.GetType() != typeof(ControlData))
                {
                    throw new TypeLoadException("The type of the message GUI sent is Wrong!");
                }

                ControlData recieveCtrlData = (ControlData)recieveData.Payload;

                sendCtrlData = recieveCtrlData;
              
                switch (recieveCtrlData.PressedButton)
                {

                    case ButtonName.S11Open:

                        S11Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S11OpenCalibrated = true;
                        break;
                    case ButtonName.S11Short:
                        if (!recieveCtrlData.S11OpenCalibrated)
                        {
                            throw new Exception("Must do the S11Open Calibration before S11Short Calibration!");
                        }
                        S11Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S11ShortCalibrated = true;
                        break;
                    case ButtonName.S11Load:
                        if (!recieveCtrlData.S11ShortCalibrated)
                        {
                            throw new Exception("Must do the S11Short Calibration before S11Load Calibration");
                        }
                        S11Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S11LoadCalibrated = true;
                        break;

                    case ButtonName.S22Open:
                        if (!recieveCtrlData.S11LoadCalibrated)
                        {
                            throw new Exception("Must do the S11Load Calibration before S22Open Calibration");
                        }

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);

                        sendCtrlData.S22OpenCalibrated = true;
                        break;
                    case ButtonName.S22Short:
                        if (!recieveCtrlData.S22OpenCalibrated)
                        {
                            throw new Exception("Must do the S22Open Calibration before S22Short Calibration!");
                        }

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S22ShortCalibrated = true;
                        break;
                    case ButtonName.S22Load:
                        if (!recieveCtrlData.S22ShortCalibrated)
                        {
                            throw new Exception("Must do the S22Short Calibration before S22Load Calibration");
                        }

                        S22Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S22LoadCalibrated = true;
                        break;
                    case ButtonName.S21Thru:
                        if (!(recieveCtrlData.S22LoadCalibrated && recieveCtrlData.S22OpenCalibrated &&
                            recieveCtrlData.S22ShortCalibrated))
                        {
                            throw new Exception("Must do the S22 Calibration before S21 Calibration");
                        }

                        S21Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S21ThruCalibrated = true;
                        break;

                    case ButtonName.S21Done:
                        if (!(recieveCtrlData.S22LoadCalibrated && recieveCtrlData.S22OpenCalibrated &&
                            recieveCtrlData.S22ShortCalibrated && recieveCtrlData.S21ThruCalibrated))
                        {
                            engine.ShowContinueUserQuery("Must calibrate all of the standards to proceed!");
                            break;
                        }

                        S21Calibration(LCAnalyzer, cfgData, recieveCtrlData);
                        sendCtrlData.S21CalibratedDond = true;
                        calibrationFinished = true;
                        break;
                    default:
                        throw new Exception("Error Type of ControlData");
                }

                engine.SendToGui(sendCtrlData);

            } while (!(calibrationFinished || (recieveData == null)));

            LCAnalyzer.CalibrationStatus = false;
            Thread.Sleep(1000);
            LCAnalyzer.CalibrationStatus = true;
            Thread.Sleep(1000);
            LCAnalyzer.FlexibleCalStatus = true;
            Thread.Sleep(1000);
            LCAnalyzer.AutoScale();
            Thread.Sleep(1000);

            //SaveS22CalSetupData(LCAnalyzer, cfgData);
            bool calResultStatus = false;
            if (hybirdChip.Contains("X"))
            {
                SaveS21CalSetupData(engine, LCAnalyzer, OPMeter, cfgData);

                Thread.Sleep(500);

                LCAnalyzer.EmbedOrDeEmbedEnable = true;
                Thread.Sleep(500);
                LCAnalyzer.RefPlaneExtensionInDistance = 1.0;
                Thread.Sleep(500);
                LCAnalyzer.SaveCalDataForSpecificChannel("c:\\testset\\calDebug\\OE.txt");
                Thread.Sleep(2000);
                LCAnalyzer.AddNetWorks();
                Thread.Sleep(1000);
                LCAnalyzer.SetNetwortType(Instr_MS4640A_VNA.NetwortType.S2Pfile);
                Thread.Sleep(1000);
                LCAnalyzer.NetWorksMode = Instr_MS4640A_VNA.EmbedType.DEEM;
                Thread.Sleep(1000);
                LCAnalyzer.LoadS2pFile(cfgData.standardOEs2pFileName);
                Thread.Sleep(1000);
                LCAnalyzer.EmbedOrDeEmbedEnable = false;
                Thread.Sleep(1000);
                LCAnalyzer.EmbedOrDeEmbedEnable = true;
                Thread.Sleep(1000);
                LCAnalyzer.FlexibleCalStatus = true;
                Thread.Sleep(1000);

                

                Trace xyData ;
                Trace calResultTrace  = new Trace();
                try
                {
                    xyData = LCAnalyzer.GetTraceData();
                    calResultTrace = SparamAnalysise.Gets21MagintudeData(xyData,is6464);
                }
                catch (Exception e)
                {
                     
                }
                double[] freqArr_GHz = calResultTrace.GetXArray();
                double[] magintudeArr_dB = calResultTrace.GetYArray();
                double[] array_20G = new double[401]  ;

                try
                {

                    Array.Copy(magintudeArr_dB, array_20G, 401);
                   

                }
                catch (Exception e)
                { }


                int indexOfMaxPeak = Alg_FindFeature.FindIndexForMaxPeak(array_20G);
                int indexOfMinVally = Alg_FindFeature.FindIndexForMinValley(array_20G);
                double ripper = magintudeArr_dB[indexOfMaxPeak] - magintudeArr_dB[indexOfMinVally];
                // if calibrate to 40G  , use 20G point to determine if the calibration is  ok 

              






                if (Math.Abs(ripper / 2) < 0.2)
                {
                    calResultStatus = true;

                    string mzFilePath = cfgData.vnaAddress + cfgData.mzS2pFileName.Split(':')[1];
                    string[] temp = cfgData.mzS2pFileName.Split('\\');
                    try
                    {
                        File.Copy(mzFilePath, cfgData.vnaAddress + "\\testset\\cal\\" + temp[temp.Length - 1], true);
                        Thread.Sleep(1000);
                    }
                    catch(Exception e)
                    {
                    }
                }
            }
            else
            {
                LCAnalyzer.SaveCalDataForSpecificChannel(cfgData.s21SetupFileName);
                //wait for save file finish
                Thread.Sleep(10000);
                engine.SendStatusMsg("wait for save cal data");
                calResultStatus = true;
            }


            // return data
            DatumList returnData = new DatumList();
            returnData.AddBool("calResultStatus", calResultStatus);
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ag8703a"></param>
        /// <param name="cfgData"></param>
        /// <param name="ctrlData"></param>
        private void S11Calibration(Instr_MS4640A_VNA LCAnalyzer,
            ConfigData cfgData, ControlData ctrlData)
        {
            if (!ctrlData.S11OpenCalibrated)
            {
                //LCAnalyzer.ActiveMeausurementChannel = 1; 
                //LCAnalyzer.StartFrequency_GHz = cfgData.startFreq_GHz;
                //Thread.Sleep(300);
                //LCAnalyzer.StopFrequency_GHz = cfgData.stopFreq_GHz;
                //Thread.Sleep(300);
                //LCAnalyzer.NumberOfSweepPoint = cfgData.sweepPoints;

                LCAnalyzer.CalibrationPortType = Instr_MS4640A_VNA.CalibrationPortTypes.TwoPort;
                Thread.Sleep(500);
                //LCAnalyzer.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT12;
                Thread.Sleep(500);
                LCAnalyzer.CalibrationMethod = Instr_MS4640A_VNA.CalibrationMethods.SOLR;

                //System.Threading.Thread.Sleep(300);
                //LCAnalyzer.ForwardCurrentBandwidth = cfgData.forwardCurrentBandwidth;

                System.Threading.Thread.Sleep(500);
                LCAnalyzer.SetDataDisplayMode(DataDisplayMode.DataOnly);

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.ActiveChannelAveragingEnable = false;

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.SmoothingEnable = false;

                System.Threading.Thread.Sleep(500);
                LCAnalyzer.SweepTime = double.MaxValue;
            }

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S11Open:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.OPEN, 
                        Instr_MS4640A_VNA.CalibrationPorts.PORT1);
                    System.Threading.Thread.Sleep(2000);  //the instrument takes this long to cal
                    
                    break;
                case ButtonName.S11Short:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.SHORT,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT1);
                    System.Threading.Thread.Sleep(2000);  //this is how long instrument takes to cal
                    break;
                case ButtonName.S11Load:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.LOAD,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT1);
                    System.Threading.Thread.Sleep(2000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ag8703a"></param>
        /// <param name="cfgData"></param>
        /// <param name="ctrlData"></param>
        private void S22Calibration(Instr_MS4640A_VNA LCAnalyzer,
            ConfigData cfgData, ControlData ctrlData)
        {
            if (!ctrlData.S22OpenCalibrated)
            {
            //    LCAnalyzer.ActiveMeausurementChannel = 1;
            //    LCAnalyzer.StartFrequency_GHz = cfgData.startFreq_GHz;
            //    LCAnalyzer.StopFrequency_GHz = cfgData.stopFreq_GHz;
            //    LCAnalyzer.NumberOfSweepPoint = cfgData.sweepPoints;

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.CalibrationPortType = Instr_MS4640A_VNA.CalibrationPortTypes.TwoPort;
                //LCAnalyzer.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT12;
               // Thread.Sleep(300);
                //LCAnalyzer.CalibrationMethod = Instr_MS4640A_VNA.CalibrationMethods.SOLR;

            //    System.Threading.Thread.Sleep(100);
            //    LCAnalyzer.ForwardCurrentBandwidth = cfgData.forwardCurrentBandwidth;

            //    System.Threading.Thread.Sleep(100);
            //    LCAnalyzer.SetDataDisplayMode(DataDisplayMode.DataOnly);

            //    //System.Threading.Thread.Sleep(100);
            //    //LCAnalyzer.ActiveChannelAveragingEnable = false;

            //    //System.Threading.Thread.Sleep(100);
            //    //LCAnalyzer.SmoothingEnable = false;

            //    System.Threading.Thread.Sleep(100);
            //    LCAnalyzer.SweepTime = double.MaxValue;
            }

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S22Open:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.OPEN,
                        Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //the instrument takes this long to cal

                    break;
                case ButtonName.S22Short:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.SHORT,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //this is how long instrument takes to cal
                    break;
                case ButtonName.S22Load:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.LOAD,
                         Instr_MS4640A_VNA.CalibrationPorts.PORT2);
                    System.Threading.Thread.Sleep(1000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
       }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ag8703a"></param>
        /// <param name="cfgData"></param>
        /// <param name="ctrlData"></param>
        private void S21Calibration(Instr_MS4640A_VNA LCAnalyzer,
            ConfigData cfgData, ControlData ctrlData)
        {
            if (!ctrlData.S21ThruCalibrated)
            {
                //LCAnalyzer.ActiveMeausurementChannel = 1;
                //LCAnalyzer.StartFrequency_GHz = cfgData.startFreq_GHz;
                //LCAnalyzer.StopFrequency_GHz = cfgData.stopFreq_GHz;
                //LCAnalyzer.NumberOfSweepPoint = cfgData.sweepPoints;

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.CalibrationPortType = Instr_MS4640A_VNA.CalibrationPortTypes.TwoPort;
                //LCAnalyzer.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT12;
                //Thread.Sleep(300);
                //LCAnalyzer.CalibrationMethod = Instr_MS4640A_VNA.CalibrationMethods.SOLR;

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.ForwardCurrentBandwidth = cfgData.forwardCurrentBandwidth;

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.SetDataDisplayMode(DataDisplayMode.DataOnly);

                //System.Threading.Thread.Sleep(100);
                //LCAnalyzer.SweepTime = double.MaxValue;
            }

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S21Thru:
                    LCAnalyzer.ExecuteCalibration(Instr_MS4640A_VNA.CalibrationStandardType.THRU,
                        Instr_MS4640A_VNA.CalibrationPorts.PORT12);
                    System.Threading.Thread.Sleep(2000);  //the instrument takes this long to cal
                    break;

                case ButtonName.S21Done:
                    DoneCalibration(LCAnalyzer, ctrlData);
                    System.Threading.Thread.Sleep(2000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
        }

        /// <summary>
        /// Done Calibration
        /// </summary>
        /// <param name="LCAnalyzer"></param>
        /// <param name="ctrlData"></param>
        private void DoneCalibration(Instr_MS4640A_VNA LCAnalyzer, ControlData ctrlData)
        {

            switch (ctrlData.PressedButton)
            {
                case ButtonName.S21Done:
                    LCAnalyzer.DoneCalibration();
                    System.Threading.Thread.Sleep(4000);  //the instrument takes this long to cal
                    break;
                default:
                    throw new Exception("Error Type of ControlData");
            }
        }

        /// <summary>
        /// save s22 calculate setup data
        /// </summary>
        /// <param name="LCAnalyzer">LCA instr</param>
        /// <param name="cfgData">configure data</param>
        private void SaveS22CalSetupData(Instr_MS4640A_VNA LCAnalyzer, ConfigData cfgData)
        {
            LCAnalyzer.FlexibleCal = FlexibleCalType.S22;
            Thread.Sleep(300);
            LCAnalyzer.FlexibleCalStatus = true;
            Thread.Sleep(300);
            LCAnalyzer.SparameterMeasurementMode = SparameterMeasurementMode.S22;
            Thread.Sleep(300);
            LCAnalyzer.SaveCalDataForSpecificChannel(cfgData.s22SetupFileName);
            Thread.Sleep(300);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="LCAnalyzer"></param>
        /// <param name="OPMeter"></param>
        /// <param name="cfgData"></param>
        private void SaveS21CalSetupData(ITestEngine engine, Instr_MS4640A_VNA LCAnalyzer, InstType_OpticalPowerMeter OPMeter, ConfigData cfgData)
        {
            //flexible setup
            Thread.Sleep(2000);
            LCAnalyzer.FlexibleCal = FlexibleCalType.S21;
            Thread.Sleep(2000);
            LCAnalyzer.FlexibleCalStatus = true;
            //response s21
            Thread.Sleep(2000);
            LCAnalyzer.SparameterMeasurementMode = SparameterMeasurementMode.S21;

            ConnCalKit conCalKit = new ConnCalKit();
            engine.SendToGui(conCalKit);
            engine.ReceiveFromGui();

            Thread.Sleep(2000);
            LCAnalyzer.DeleteNetworks();
            Thread.Sleep(2000);
            LCAnalyzer.AddNetWorks();
            Thread.Sleep(2000);
            LCAnalyzer.NetWorksMode = Instr_MS4640A_VNA.EmbedType.DEEM;
            Thread.Sleep(2000);
            LCAnalyzer.SetNetwortType(Instr_MS4640A_VNA.NetwortType.S2Pfile);
            Thread.Sleep(2000);
            LCAnalyzer.LoadS2pFile(cfgData.standardOEs2pFileName);
            Thread.Sleep(2000);
            LCAnalyzer.EmbedOrDeEmbedEnable = false;
            Thread.Sleep(2000);
            LCAnalyzer.EmbedOrDeEmbedEnable = true;

            Thread.Sleep(2000);
            //remove additional fibre(1m)
            LCAnalyzer.RefPlaneExtensionInDistance = 1.0;
            Thread.Sleep(2000);
            LCAnalyzer.SaveCalDataForSpecificChannel(cfgData.mzS2pFileName);
            Thread.Sleep(2000);
            string sourceFileName = cfgData.vnaAddress + cfgData.mzS2pFileName.Split(':')[1];
            string[] temp = cfgData.mzS2pFileName.Split('\\');
            string path = Directory.GetCurrentDirectory() + "\\results";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string destFileName = path + "\\" + temp[temp.Length - 1];

            File.Copy(sourceFileName, destFileName, true);
            ModifyS2pFile(destFileName);
            File.Copy(destFileName, sourceFileName, true);


            LCAnalyzer.EmbedOrDeEmbedEnable = false;
            Thread.Sleep(2000);
            LCAnalyzer.LoadS2pFile(cfgData.mzS2pFileName);
            Thread.Sleep(2000);
            LCAnalyzer.EmbedOrDeEmbedEnable = true;
            Thread.Sleep(2000);
            //
            LCAnalyzer.RefPlaneExtensionInDistance = 0;
            Thread.Sleep(2000);
            LCAnalyzer.SaveCalDataForSpecificChannel(cfgData.s21SetupFileName);
            //save OE module test result

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="LCAnalyzer"></param>
        /// <param name="s2pFile"></param>
        private void EditEmbAndDeEmbNetwork(Instr_MS4640A_VNA LCAnalyzer,string s2pFile)
        {
            LCAnalyzer.DeleteNetworks();
            LCAnalyzer.AddNetWorks();
            LCAnalyzer.SetNetwortType(Instr_MS4640A_VNA.NetwortType.S2Pfile);
            LCAnalyzer.LoadS2pFile(s2pFile);
        }

        /// <summary>
        /// modify the s2p file
        /// </summary>
        /// <param name="fileName"></param>
        private void ModifyS2pFile(string fileName)
        {
            List<string> listStr = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string str = "";
                do
                {
                    str = sr.ReadLine();
                    if (str == null)
                    {
                        break;
                    }
                    listStr.Add(str);
                } while (true);
            }
            for (int i = 0; i < listStr.Count; i++)
            {
                if (listStr[i].Contains("!") || listStr[i].Contains("#"))
                {
                    continue;
                }
                string[] str = listStr[i].Split(' ');

                int icouts = 0;
                for (int j = 0; j < str.Length; j++)
                {
                    if (str[j] != "")
                    {

                        if (icouts != 0 && icouts != 3 && icouts != 4)
                        {
                            int length = str[j].Length;
                            str[j] = "0 ";
                            str[j] = str[j].PadLeft(length + 2);
                        }
                        else
                        {
                            str[j] += " ";
                        }
                        icouts++;
                    }
                    else
                    {
                        str[j] = " ";
                    }
                }

                string str2 = str[0];
                for (int k = 1; k < str.Length; k++)
                {
                    str2 = str2 + str[k];
                }
                listStr[i] = str2;

            }
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                for (int i = 0; i < listStr.Count; i++)
                {
                    sw.WriteLine(listStr[i]);
                }
            }
        }

        public Type UserControl
        {
            get { return (typeof(Mod_SPCalibrationGui)); }
        }

        #endregion

        private struct ConfigData
        {
            public ConfigData(DatumList inputData)
            {
                startFreq_GHz = inputData.ReadDouble("Start_Frequency");
                stopFreq_GHz = inputData.ReadDouble("Stop_Frequency");

                sweepPoints = (uint)inputData.ReadSint32("sweepPoints");
                averagingFactor = inputData.ReadDouble("averagingFactor");
                smoothingPercent = inputData.ReadDouble("smoothingPercent");
                SourcePower_dBm = inputData.ReadDouble("SourcePower_dBm");
                forwardCurrentBandwidth = inputData.ReadDouble("forwardCurrentBandwidth");

                s22SetupFileName = inputData.ReadString("s22SetupFileName");
                s21SetupFileName = inputData.ReadString("s21SetupFileName");
                standardOEs2pFileName = inputData.ReadString("standardOEs2pFileName");
                mzS2pFileName = inputData.ReadString("mzS2pFileName");
                calResultFile = inputData.ReadString("calResultFileName");
                vnaAddress = inputData.ReadString("VNA_Address");
            }

            public double startFreq_GHz;
            public double stopFreq_GHz;

            public uint sweepPoints;

            public double averagingFactor;
            public double smoothingPercent;

            public double SourcePower_dBm;

            public double forwardCurrentBandwidth;

            public string s22SetupFileName;

            public string s21SetupFileName;

            public string standardOEs2pFileName;

            public string mzS2pFileName;

            public string vnaAddress;

            public string calResultFile;
        }
    }
}

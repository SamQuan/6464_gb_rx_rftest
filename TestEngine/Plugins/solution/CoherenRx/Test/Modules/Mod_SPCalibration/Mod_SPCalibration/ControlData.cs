using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestModules
{
    class ControlData
    {
        public bool S11OpenCalibrated
        {
            get
            {
                return _S11OpenCalibrated;
            }
            set
            {
                _S11OpenCalibrated = value;
            }
        }

        public bool S11ShortCalibrated
        {
            get
            {
                return _S11ShortCalibrated;
            }
            set
            {
                _S11ShortCalibrated = value;
            }
        }

        public bool S11LoadCalibrated
        {
            get
            {
                return _S11LoadCalibrated;
            }
            set
            {
                _S11LoadCalibrated = value;
            }
        }

        public bool S22OpenCalibrated
        {
            get
            {
                return _S22OpenCalibrated;
            }
            set
            {
                _S22OpenCalibrated = value;
            }
        }

        public bool S22ShortCalibrated
        {
            get
            {
                return _S22ShortCalibrated;
            }
            set
            {
                _S22ShortCalibrated = value;
            }
        }

        public bool S22LoadCalibrated
        {
            get
            {
                return _S22LoadCalibrated;
            }
            set
            {
                _S22LoadCalibrated = value;
            }
        }

        public bool S21ThruCalibrated
        {
            get
            {
                return _ThruCalibrated;
            }
            set
            {
                _ThruCalibrated = value;
            }
        }

        public bool S21CalibratedDond
        {
            get
            {
                return _S21CalibratedDond;
            }
            set
            {
                _S21CalibratedDond = value;
            }
        }

        public ButtonName PressedButton
        {
            get
            {
                return _pressedButton;
            }
            set
            {
                _pressedButton = value;
            }
        }

        /// <summary>
        /// The Flag of the parameters whether has been calibrated.
        /// </summary>

        private bool _S11OpenCalibrated = false;
        private bool _S11ShortCalibrated = false;
        private bool _S11LoadCalibrated = false;
        private bool _S22OpenCalibrated = false;
        private bool _S22ShortCalibrated = false;
        private bool _S22LoadCalibrated = false;
        private bool _ThruCalibrated = false;
        private bool _S21CalibratedDond = false;

        /// <summary>
        /// The Flag of the parameters whether has been calibrated.
        /// </summary>
        private ButtonName _pressedButton;
    }

    /// <summary>
    /// Name of Botton which has been pressed
    /// </summary>
    public enum ButtonName
    {
        S11Open = 1,
        S11Short = 2,
        S11Load = 3,

        S22Open = 4,
        S22Short = 5,
        S22Load = 6,

        S21Thru = 7,

        //S11Done = 8,
        //S22Done = 9,
        S21Done = 8,

    }
}

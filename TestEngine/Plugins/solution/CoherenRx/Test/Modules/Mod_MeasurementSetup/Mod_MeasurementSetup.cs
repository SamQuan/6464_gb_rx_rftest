// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasurementSetup.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using CoherentRxTestCommonData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSetupOpticalPathDelay : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            #region init equipment

            Tia_X = (TiaInstrument)configData.ReadReference("Tia_X");
            Tia_Y = (TiaInstrument)configData.ReadReference("Tia_Y");
            pdBias = (PdBiasInstrument)configData.ReadReference("PdBias");
            AuxPd = (AuxPhaseAlignInstrument)configData.ReadReference("AuxPd");

            ThermalPhaseCtrl_X = (Inst_SMUTI_TriggeredSMU)configData.ReadReference("ThermalPhaseCtrl_X");
            ThermalPhaseCtrl_Y = (Inst_SMUTI_TriggeredSMU)configData.ReadReference("ThermalPhaseCtrl_Y");

            WaveformGenerator = (Instr_AgWaveformGenerator)configData.ReadReference("WaveformGenerator");
            LightwaveComponentAnalyzer = (Inst_Ag8703_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            //OPM_SigRef = (InstType_OpticalPowerMeter)configData.ReadReference("OPM_SigRef");
            //OPM_LocRef = (InstType_OpticalPowerMeter)configData.ReadReference("OPM_LocRef");

            LockInAmplifier = (Inst_SR830)configData.ReadReference("LockInAmplifier");

            RfSwitch = (Switch_LCA_RF)configData.ReadReference("RfSwitch");

            MeasureSetupManage sigMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            MeasureSetupManage locMeasureSetupManage = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");


            #endregion

            //RxSplitterRatioCalData

            #region init configure

            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");
            targetInputPower_Sig_dBm = configData.ReadDouble("targetInputPower_Sig_dBm");
            targetInputPower_Loc_dBm = configData.ReadDouble("targetInputPower_Loc_dBm");

            vmcOfTia_V = configData.ReadDouble("vmcOfTia_V");
            vccOfTia_V = configData.ReadDouble("vccOfTia_V");
            vgcOfTia_V = configData.ReadDouble("vgcOfTia_V");
            pdBais_V = configData.ReadDouble("pdBais_V");
            bandWidthSetting = int.Parse(configData.ReadDouble("BandwidthSetting").ToString());
            TiaBandwidth tiaBandwidth = (TiaBandwidth)Enum.Parse(typeof(TiaBandwidth), bandWidthSetting.ToString());


            AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            analyzerSetup_S21.startFrequency_GHz = configData.ReadDouble("s21StartFrequency_GHz");
            analyzerSetup_S21.stopFrequency_GHz = configData.ReadDouble("s21StopFrequency_GHz");
            analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            analyzerSetup_S21.CalRegister = configData.ReadSint32("s21CalRegister");

            AnalysisConfig_S21 analysisConfig_S21 = new AnalysisConfig_S21();
            analysisConfig_S21.OscillationStartFreq_GHz = configData.ReadDouble("OsciStartFreq_GHz");
            analysisConfig_S21.OscillationStopFreq_GHz = configData.ReadDouble("OsciStopFreq_GHz");
            analysisConfig_S21.smoothingPercent = configData.ReadDouble("s21smoothingAperturePercent");
            analysisConfig_S21.OscillationS21Limit = configData.ReadDouble("OscillationS21Limit");
            analysisConfig_S21.normalisationFrequency_GHz = configData.ReadDouble("s21NormalisationFrequency_GHz");

            MeasureSetupManage.PolSweepConfig polSweepConfig = new MeasureSetupManage.PolSweepConfig();
            polSweepConfig.sweepStart_V = 0.0;
            polSweepConfig.sweepStop_V = 5.0;
            polSweepConfig.sweepStep_V = 0.1;
            polSweepConfig.sweepDelayTime_ms = 2;
            polSweepConfig.pdBiasVoltage_V = 5.0;
            polSweepConfig.IcompliancePD_A = 3.0 / 1000;
            polSweepConfig.IcompliancePC_A = 20.0 / 1000;
            #endregion

            //set pd to 5v(2mA)
            engine.SendStatusMsg("set tia!");
            double currentComplance_mA = 2.0;
            SetPdBias(pdBais_V, currentComplance_mA);

            //set vcc to 3.3v(150mA)
            double vccComplianceCurrent_mA = 150.0;
            SetTiaVcc(vccOfTia_V, vccComplianceCurrent_mA);

            //set Tia mode
            double complianceCurrent_mA = 150.0;
            SetTiaModeControl(vmcOfTia_V, complianceCurrent_mA);

            //set vgc
            double vgcComplianceCurrent_mA = 50;
            SetTiaVgc(vgcOfTia_V, vgcComplianceCurrent_mA);

            //set band width control,BWH BWL
            double bandWidthComplianceCurrent_mA = 50;
            SetTiaBandWidthCtrl(tiaBandwidth, bandWidthComplianceCurrent_mA, vccOfTia_V);

            //measure icc current of tia
            double iccTiaX_Amp = Tia_X.VccSupply.CurrentActual_Amp;
            double iccTiaY_Amp = Tia_Y.VccSupply.CurrentActual_Amp;

            //set LO/SIG output power and Adjust VOA so that to be equal
            engine.SendStatusMsg("set signal path input power to " + targetInputPower_Sig_dBm.ToString());
            bool setPwrOk = sigMeasureSetupManage.SetOpticalPower(engine, targetInputPower_Sig_dBm, spliterRatio_Sig, powerTolerance_dB);
            inputPwrSig_dBm = sigMeasureSetupManage.OPM_Ref.ReadPower() * spliterRatio_Sig;

            engine.SendStatusMsg("set signal path input power to " + targetInputPower_Sig_dBm.ToString());
            locMeasureSetupManage.SetOpticalPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);
            inputPwrSig_dBm = locMeasureSetupManage.OPM_Ref.ReadPower() * spliterRatio_Loc;

            //set wave form generator and LCA
            WaveformGenerator.WaveFunction = Instr_AgWaveformGenerator.WaveType.SIN;
            WaveformGenerator.Frequency_Hz = 100 * 1000;
            WaveformGenerator.DcOffset_Volt = 0.5;
            WaveformGenerator.Amplitude_Volt = 0.5;

            LockInAmplifier.ReferenceFreq_Hz = 100 * 1000;
            LockInAmplifier.ReferenceSource = Inst_SR830.ReferenceSourceEnum.External;
            LockInAmplifier.ReserveMode = Inst_SR830.ReserveModeEnum.Normal;
            LockInAmplifier.AutoOffset(Inst_SR830.signalTypeEnum.X);
            LockInAmplifier.AutoGain();
            LockInAmplifier.AutoPhase();
            LockInAmplifier.AutoReserve();
            LockInAmplifier.InputCouple = Inst_SR830.InputCoupleTypeEnum.AC;
            LockInAmplifier.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.A;
            LockInAmplifier.LowerPassFilterSloper = Inst_SR830.FilterSlopeEnum.six_dBperOct;
            LockInAmplifier.NotchFilter = Inst_SR830.NotchFilterEnum.LineFilter;
            LockInAmplifier.Sensitivity = Inst_SR830.SensitivityEnum._10uVperPA;
            LockInAmplifier.SetDisplayAndRatio(Inst_SR830.ChanNumEnum.Ch1, Inst_SR830.DisplayChanTowEnum.deg, Inst_SR830.RatioChanTwoEnum.None);
            LockInAmplifier.SetOffsetAndExpands(Inst_SR830.signalTypeEnum.X, Inst_SR830.ExpandEnum.noExpand, 10);
            LockInAmplifier.ShieldGround = Inst_SR830.ShieldGroundEnum.Ground;
            LockInAmplifier.SynchronousFilter = true;
            LockInAmplifier.TimeConstant = Inst_SR830.TimeConstantEnum._30us;


            //RF switch to XQN
            engine.SendStatusMsg("switch rf to XIP!");
            RfSwitch.SetState(Switch_LCA_RF.State.XIpos);

            //set delay lines to mid-point
            engine.SendStatusMsg("set delay lines to mid-point!");
            sigMeasureSetupManage.DelayLine.Delay_ps = 5;
            locMeasureSetupManage.DelayLine.Delay_ps = 5;

            //Adjust sig VOAs so that Sig=-3dB
            double powerSet_dBm;
            powerSet_dBm = targetInputPower_Sig_dBm - 3;
            sigMeasureSetupManage.SetOpticalPower(engine, powerSet_dBm, spliterRatio_Sig, powerTolerance_dB);

            //Adjust loc VOAs so that loc=null
            locMeasureSetupManage.SetOpticalPower(engine, nullPower_dB, spliterRatio_Loc, powerTolerance_dB);

            // Adjust Pol_sig ,read PD_X,find the max current
            sopcCtrlVoltage_Sig = sigMeasureSetupManage.AdjustPolMakePdCurrMaximised(engine, MeasureSetupManage.OpticalPath.SIG, polSweepConfig);

            //Adjust sig VOAs so that Sig = null
            sigMeasureSetupManage.SetOpticalPower(engine, nullPower_dB, spliterRatio_Sig, powerTolerance_dB);

            //Adjust loc VOAs so that loc = 0dB
            locMeasureSetupManage.SetOpticalPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);

            // Adjust Pol_sig ,read PD_Y,find the max current
            sopcCtrlVoltage_Loc = locMeasureSetupManage.AdjustPolMakePdCurrMaximised(engine, MeasureSetupManage.OpticalPath.LOC, polSweepConfig);

            //manual adjust delay line 
            engine.ShowContinueUserQuery("Please do MANU adjust delay line");


            ////Adjust sig/loc VOAs so that Sig=-3dB,loc=0dB
            //sigMeasureSetupManage.SetOpticalPower(engine, powerSet_dBm, spliterRatio_Sig, powerTolerance_dB);
            //locMeasureSetupManage.SetOpticalPower(engine, targetInputPower_Loc_dBm, spliterRatio_Loc, powerTolerance_dB);

            ////do s21 sweep and find null point           
            //delaySettingSig_ps = sigMeasureSetupManage.OptimiseOpticalPathDelay(engine, analyzerSetup_S21, analysisConfig_S21);
            //delaySettingLoc_ps = locMeasureSetupManage.OptimiseOpticalPathDelay(engine, analyzerSetup_S21, analysisConfig_S21);

            if (!engine.IsSimulation)
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }

            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("actualInputPower_Sig", inputPwrSig_dBm);
            returnData.AddDouble("actualInputPower_Loc", inputPwrLoc_dBm);
            returnData.AddDouble("iccTia_X", iccTiaX_Amp);
            returnData.AddDouble("iccTia_Y", iccTiaY_Amp);
            returnData.AddDoubleArray("pcSigCtrlVoltage_V", sopcCtrlVoltage_Sig);
            returnData.AddDoubleArray("pcLocCtrlVoltage_V", sopcCtrlVoltage_Loc);
            //returnData.AddDouble("delaySettingSig_ps", delaySettingSig_ps);
            //returnData.AddDouble("delaySettingLoc_ps", delaySettingLoc_ps);

            return returnData;
        }



        #region private function

        private void SetPdBias(double voltSet_V, double currentCompliance_mA)
        {
            pdBias.PdSource_XIpos.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_XIneg.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_XQpos.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_XQneg.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_YIpos.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_YIneg.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_YQpos.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            pdBias.PdSource_YQneg.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            pdBias.PdSource_XIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_XIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_XQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_XQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YIpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YIneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YQpos.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            pdBias.PdSource_YQneg.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;

            pdBias.PdSource_XIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_XIneg.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_XQpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_XQneg.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YIpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YIneg.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YQpos.VoltageSetPoint_Volt = voltSet_V;
            pdBias.PdSource_YQneg.VoltageSetPoint_Volt = voltSet_V;
        }

        private void SetTiaVcc(double vcc_V, double vccComplianceCurrent_mA)
        {
            Tia_X.VccSupply.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_Y.VccSupply.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_X.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            Tia_X.VccSupply.VoltageSetPoint_Volt = vcc_V;
            Tia_Y.VccSupply.CurrentComplianceSetPoint_Amp = vccComplianceCurrent_mA / 1000.0;
            Tia_Y.VccSupply.VoltageSetPoint_Volt = vcc_V;
        }

        private void SetTiaVgc(double vgc_V, double vgcComplianceCurrent_mA)
        {
            Tia_X.Vgc.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_Y.Vgc.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_X.Vgc.CurrentComplianceSetPoint_Amp = vgcComplianceCurrent_mA / 1000.0;
            Tia_Y.Vgc.CurrentComplianceSetPoint_Amp = vgcComplianceCurrent_mA / 1000.0;
            Tia_X.Vgc.VoltageSetPoint_Volt = vgc_V;
            Tia_Y.Vgc.VoltageSetPoint_Volt = vgc_V;
        }

        private void SetTiaModeControl(double modeCtrlVolt_V, double complianceCurrent_mA)
        {
            Tia_X.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_Y.ModeCtrlOfGC.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            Tia_X.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;
            Tia_Y.ModeCtrlOfGC.CurrentComplianceSetPoint_Amp = complianceCurrent_mA / 1000.0;

            Tia_X.ModeCtrlOfGC.VoltageSetPoint_Volt = modeCtrlVolt_V;
            Tia_Y.ModeCtrlOfGC.VoltageSetPoint_Volt = modeCtrlVolt_V;
        }

        private void SetTiaBandWidthCtrl(TiaBandwidth tiaBandwidth, double currentCompliance_mA, double tiaVcc_V)
        {
            Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            Tia_X.BandWidth_H.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            Tia_X.BandWidth_L.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            Tia_Y.BandWidth_H.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;
            Tia_Y.BandWidth_L.CurrentComplianceSetPoint_Amp = currentCompliance_mA / 1000.0;

            switch (tiaBandwidth)
            {
                case TiaBandwidth._10point0_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    break;
                case TiaBandwidth._10point5_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    break;
                case TiaBandwidth._10point9_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_X.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    break;
                case TiaBandwidth._11point1_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    break;
                case TiaBandwidth._11point5_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    break;
                case TiaBandwidth._12point8_GHz:
                    Tia_X.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_X.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_H.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_Y.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    break;
                case TiaBandwidth._14point0_GHz:
                    Tia_X.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    Tia_Y.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.GND;
                    break;
                case TiaBandwidth._14point8_GHz:
                    Tia_X.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_X.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    Tia_Y.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_L.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.OFF;
                    break;
                case TiaBandwidth._15point4_GHz:
                    Tia_X.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_X.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_H.VoltageSetPoint_Volt = tiaVcc_V;
                    Tia_Y.BandWidth_L.VoltageSetPoint_Volt = tiaVcc_V;
                    break;
                default:
                    break;
            }
        }

        #endregion


        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSetupOpticalPathDelayGui)); }
        }

        #region private variable
        TiaInstrument Tia_X;
        TiaInstrument Tia_Y;
        PdBiasInstrument pdBias;
        AuxPhaseAlignInstrument AuxPd;

        Inst_SMUTI_TriggeredSMU ThermalPhaseCtrl_X;
        Inst_SMUTI_TriggeredSMU ThermalPhaseCtrl_Y;

        Instr_AgWaveformGenerator WaveformGenerator;
        Inst_Ag8703_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;
        InstType_OpticalPowerMeter OPM_SigRef;
        InstType_OpticalPowerMeter OPM_LocRef;

        Inst_SR830 LockInAmplifier;
        //Inst_iTLATunableLaserSource LaserSource;
        Switch_LCA_RF RfSwitch;

        const double nullPower_dB = double.NegativeInfinity;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double powerTolerance_dB;
        double targetInputPower_Sig_dBm;
        double targetInputPower_Loc_dBm;
        double vmcOfTia_V;
        double vccOfTia_V;
        double vgcOfTia_V;
        double bandWidthCtrlH_V;
        double bandWidthCtrlL_V;
        int bandWidthSetting;
        double pdBais_V;
        double inputPwrSig_dBm;
        double inputPwrLoc_dBm;
        double delaySettingSig_ps;
        double delaySettingLoc_ps;


        double[] sopcCtrlVoltage_Sig;
        double[] sopcCtrlVoltage_Loc;

        Trace s21RealAndImaginaryTrace = new Trace();
        Trace s21Trace = new Trace();
        Trace s22Trace = new Trace();
        bool maskPassFail_S22 = false;


        #endregion

        enum TiaBandwidth
        {
            _10point0_GHz = -4,
            _10point5_GHz = -3,
            _10point9_GHz = -2,
            _11point1_GHz = -1,
            _11point5_GHz = 0,
            _12point8_GHz = 1,
            _14point0_GHz = 2,
            _14point8_GHz = 3,
            _15point4_GHz = 4,

        }

        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Library
//
// ChamberControl.cs
//
// Author: Paul.Worsey, October 2006
// Design: As per Chamber Control Module DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;



namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Simple Temperature Control module
    /// </summary>
    public class SimpleTempControl : ITestModule
    {
        /// <summary>
        /// This does the test for us,
        /// </summary>
        /// <param name="engine">The itest engine engine</param>
        /// <param name="userType">operator engineer or technician</param>
        /// <param name="configData">a datum list of all required config data consisting of:
        /// DatumDouble("MaxExpectedTimeForOperation_s")
        /// DatumDouble("RqdStabilisationTime_s")
        /// DatumSint32("TempTimeBtwReadings_ms")
        /// DatumDouble("SetPointTemperature_C")
        /// DatumDouble("TemperatureTolerance_C");
        /// </param>
        /// <param name="instruments">Should contain instruments["Controller"]</param>
        /// <param name="chassis">the chassis</param>
        /// <param name="calData">any cal data (none reqd)</param>
        /// <param name="previousTestData">any previous data (none reqd)</param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
             
             // Setup Config Data as a struct
            ConfigData CfgData = new ConfigData(configData);
          
            // Setup and show the GUI
            DatumString MaxTime = new DatumString("MaxTime", "0:0:0");
            DatumDouble ActualTemp = new DatumDouble("ActualTemp",0);
            DatumString StabilisationTime = new DatumString("StabilTime", "0:0:0");
            engine.SendToGui(configData.GetDatum("SetPointTemperature_C"));
            
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui(MaxTime.Clone());
            engine.SendToGui(ActualTemp.Clone());
            engine.SendToGui(StabilisationTime.Clone());
            
            // Setup Instrument
            IInstType_SimpleTempControl tempControl = (IInstType_SimpleTempControl)instruments["Controller"];
            engine.SendToGui(tempControl.Name);

            // Set Temp and Switch On
            tempControl.SensorTemperatureSetPoint_C = CfgData.SetPointTemperature_C;
            if (tempControl.OutputEnabled == false) 
            { 
                tempControl.OutputEnabled = true; 
            }

            // Ramp and Settle Routine
            bool LogSettleTime = true; // for activating the Stabilisation count down timer
            bool SetTemp_Success = false; // achieved temperature if true

            DateTime StartTime = DateTime.Now; // Time process began
            TimeSpan MaxTimeOut = new TimeSpan(0, 0, Convert.ToInt32(CfgData.MaxExpectedTimeForOperation_s)); // Maximum TimeOut used for count down timer
            TimeSpan SettleTimeOut = new TimeSpan(0, 0, Convert.ToInt32(CfgData.RqdStabilisationTime_s)); // Stabilisation TimeOut used for count down timer
            DateTime SettleTime = new DateTime(); // stabilsation start time
            TimeSpan TimeDiff = new TimeSpan(); // used for determine the length of time difference between dateTime
 
            do
            {
                System.Threading.Thread.Sleep(CfgData.TempTimeBtwReadings_ms);
                
                ActualTemp.Update(tempControl.SensorTemperatureActual_C); //Read Temp and send to Gui
                   
                engine.SendToGui(ActualTemp.Clone());
				
				// Get and Check TimeOut
                DateTime TimeNow = DateTime.Now;

                TimeDiff = TimeNow - StartTime;  // Get Timespan difference between Now and StartTime for Maximum TimeOut 

                if (TimeDiff > MaxTimeOut)
                {
                    engine.ErrorInModule("Chamber Error: Timeout timer has elapsed, failed to acheive temperature within max time specified");
                }

                //Display to GUI as a count down timer displaying Maximum Time counting down to zero
                TimeDiff = MaxTimeOut.Subtract(TimeDiff);
                MaxTime.Update(TimeDateToString(TimeDiff));
                engine.SendToGui(MaxTime.Clone());

				// In Tolerance, start stabilisation TimeOut
                if (Math.Abs(ActualTemp.Value - CfgData.SetPointTemperature_C) < CfgData.TemperatureTolerance_C)
                {
                    if (LogSettleTime) // log stabilisation TimeOut
                    {
                        // Get Stabilisation TimeOut
                        SettleTime = DateTime.Now;
                        LogSettleTime = false;  // only need to log start of stabilisation time once if temp continues to be with tolerance
                    }

                    TimeDiff = TimeNow - SettleTime;  // Get Timespan difference between Now and SettleTime for Stabilisation TimeOut 
                    
                    if (TimeDiff > SettleTimeOut ) { SetTemp_Success = true; } //Success chamber has been stabil for x amount at required temp

                    //Display to GUI as a count down timer displaying Stabilisation Time counting down to zero
                    TimeDiff = SettleTimeOut.Subtract(TimeDiff);
                    StabilisationTime.Update(TimeDateToString(TimeDiff));
                   
                }
                else  // Not In Tolerance, reset stabilisation TimeOut
                {
                    LogSettleTime = true; // allow reset of stabilisation TimeOut
                    SettleTime = TimeNow;
                    StabilisationTime.Update(SettleTimeOut.ToString());  
                }
                engine.SendToGui(StabilisationTime.Clone());
			}
            while (!SetTemp_Success);

            return null;

        }
        /// <summary>
        /// Returns a TimeSpan to string 
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        static string TimeDateToString(TimeSpan ts)
        {

            char sign;

            if (ts.Ticks >= 0) sign = ' ';

            else sign = '-';

            string timeDiffStr = String.Format("{0}{1:00}:{2:00}:{3:00}",

                sign, Math.Abs(ts.Hours), Math.Abs(ts.Minutes), Math.Abs(ts.Seconds));

            return timeDiffStr;

        }

        /// <summary>
        /// The user control we are using for display
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }


        #region DatumLists Information

        /// <summary>
        /// Struct that converts configdata datumlist into usable values
        /// will also convert usable value to datumlist
        /// </summary>
        private struct ConfigData
        {

            public ConfigData(DatumList InputData)
            {
                this.TempTimeBtwReadings_ms = InputData.ReadSint32("TempTimeBtwReadings_ms");
                this.MaxExpectedTimeForOperation_s = InputData.ReadDouble("datumMaxExpectedTimeForOperation_s");
                this.RqdStabilisationTime_s = InputData.ReadDouble("RqdStabilisationTime_s");
                this.TemperatureTolerance_C = InputData.ReadDouble("TemperatureTolerance_C");
                this.SetPointTemperature_C = InputData.ReadDouble("SetPointTemperature_C");
            }

            public DatumList ToDatumList()
            {
                DatumList myLocalDatumList = new DatumList();

                DatumSint32 TempTimeBtwReadings_ms = new DatumSint32("TempTimeBtwReadings_ms", this.TempTimeBtwReadings_ms);
                DatumDouble MaxExpectedTimeForOperation_s = new DatumDouble("MaxExpectedTimeForOperation_s", this.MaxExpectedTimeForOperation_s);
                DatumDouble RqdStabilisationTime_s = new DatumDouble("RqdStabilisationTime_s", this.RqdStabilisationTime_s);
                DatumDouble SetPointTemperature_C = new DatumDouble("SetPointTemperature_C", this.SetPointTemperature_C);
                DatumDouble TemperatureTolerance_C = new DatumDouble("TemperatureTolerance_C", this.TemperatureTolerance_C);

                myLocalDatumList.Add(TempTimeBtwReadings_ms);
                myLocalDatumList.Add(MaxExpectedTimeForOperation_s);
                myLocalDatumList.Add(RqdStabilisationTime_s);
                myLocalDatumList.Add(TemperatureTolerance_C);
                myLocalDatumList.Add(SetPointTemperature_C);

                return (myLocalDatumList);
            }

            //List of all parameters
            public Int32 TempTimeBtwReadings_ms;
            public double MaxExpectedTimeForOperation_s;
            public double RqdStabilisationTime_s;
            public double SetPointTemperature_C;
            public double TemperatureTolerance_C;
        }

        #endregion

    }
}

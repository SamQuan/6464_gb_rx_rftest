// [Copyright]
//
// Bookham Test Engine
// 
//
// Bookham.TestSolution.TestModules/ModuleGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestLibrary.TestModules
{
    partial class ModuleGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSetPointTemperature = new System.Windows.Forms.Label();
            this.labelStabilisationTimer = new System.Windows.Forms.Label();
            this.labelMaxTimer = new System.Windows.Forms.Label();
            this.labelActTemperature = new System.Windows.Forms.Label();
            this.SpValue = new System.Windows.Forms.Label();
            this.StabilisationTimer = new System.Windows.Forms.Label();
            this.ActualValue = new System.Windows.Forms.Label();
            this.MaxTimer = new System.Windows.Forms.Label();
            this.InstName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelSetPointTemperature
            // 
            this.labelSetPointTemperature.AutoSize = true;
            this.labelSetPointTemperature.Location = new System.Drawing.Point(16, 16);
            this.labelSetPointTemperature.Name = "labelSetPointTemperature";
            this.labelSetPointTemperature.Size = new System.Drawing.Size(123, 13);
            this.labelSetPointTemperature.TabIndex = 0;
            this.labelSetPointTemperature.Text = "SetPointTemperature [C]";
            // 
            // labelStabilisationTimer
            // 
            this.labelStabilisationTimer.AutoSize = true;
            this.labelStabilisationTimer.Location = new System.Drawing.Point(320, 16);
            this.labelStabilisationTimer.Name = "labelStabilisationTimer";
            this.labelStabilisationTimer.Size = new System.Drawing.Size(92, 13);
            this.labelStabilisationTimer.TabIndex = 1;
            this.labelStabilisationTimer.Text = "Stabilisation Timer";
            // 
            // labelMaxTimer
            // 
            this.labelMaxTimer.AutoSize = true;
            this.labelMaxTimer.Location = new System.Drawing.Point(506, 16);
            this.labelMaxTimer.Name = "labelMaxTimer";
            this.labelMaxTimer.Size = new System.Drawing.Size(80, 13);
            this.labelMaxTimer.TabIndex = 2;
            this.labelMaxTimer.Text = "Maximum Timer";
            // 
            // labelActTemperature
            // 
            this.labelActTemperature.AutoSize = true;
            this.labelActTemperature.Location = new System.Drawing.Point(166, 16);
            this.labelActTemperature.Name = "labelActTemperature";
            this.labelActTemperature.Size = new System.Drawing.Size(116, 13);
            this.labelActTemperature.TabIndex = 3;
            this.labelActTemperature.Text = "Actual Temperature [C]";
            // 
            // SpValue
            // 
            this.SpValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.SpValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpValue.Location = new System.Drawing.Point(19, 50);
            this.SpValue.Name = "SpValue";
            this.SpValue.Size = new System.Drawing.Size(110, 45);
            this.SpValue.TabIndex = 4;
            this.SpValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StabilisationTimer
            // 
            this.StabilisationTimer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.StabilisationTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StabilisationTimer.Location = new System.Drawing.Point(299, 50);
            this.StabilisationTimer.Name = "StabilisationTimer";
            this.StabilisationTimer.Size = new System.Drawing.Size(140, 45);
            this.StabilisationTimer.TabIndex = 5;
            this.StabilisationTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActualValue
            // 
            this.ActualValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ActualValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActualValue.Location = new System.Drawing.Point(162, 50);
            this.ActualValue.Name = "ActualValue";
            this.ActualValue.Size = new System.Drawing.Size(110, 45);
            this.ActualValue.TabIndex = 6;
            this.ActualValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MaxTimer
            // 
            this.MaxTimer.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MaxTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaxTimer.Location = new System.Drawing.Point(464, 50);
            this.MaxTimer.Name = "MaxTimer";
            this.MaxTimer.Size = new System.Drawing.Size(160, 45);
            this.MaxTimer.TabIndex = 7;
            this.MaxTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InstName
            // 
            this.InstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstName.ForeColor = System.Drawing.Color.Maroon;
            this.InstName.Location = new System.Drawing.Point(168, 156);
            this.InstName.Name = "InstName";
            this.InstName.Size = new System.Drawing.Size(456, 50);
            this.InstName.TabIndex = 8;
            this.InstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ModuleGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.InstName);
            this.Controls.Add(this.MaxTimer);
            this.Controls.Add(this.ActualValue);
            this.Controls.Add(this.StabilisationTimer);
            this.Controls.Add(this.SpValue);
            this.Controls.Add(this.labelActTemperature);
            this.Controls.Add(this.labelMaxTimer);
            this.Controls.Add(this.labelStabilisationTimer);
            this.Controls.Add(this.labelSetPointTemperature);
            this.Name = "ModuleGui";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSetPointTemperature;
        private System.Windows.Forms.Label labelStabilisationTimer;
        private System.Windows.Forms.Label labelMaxTimer;
        private System.Windows.Forms.Label labelActTemperature;
        private System.Windows.Forms.Label SpValue;
        private System.Windows.Forms.Label StabilisationTimer;
        private System.Windows.Forms.Label ActualValue;
        private System.Windows.Forms.Label MaxTimer;
        private System.Windows.Forms.Label InstName;

    }
}

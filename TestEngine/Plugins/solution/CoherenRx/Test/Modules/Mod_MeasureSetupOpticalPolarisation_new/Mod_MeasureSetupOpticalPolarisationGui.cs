// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasurementSetupGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using NPlot;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_MeasureSetupOpticalPolarisationGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_MeasureSetupOpticalPolarisationGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(DatumList))
            {
                try
                {
                    DatumList infoFromWork = (DatumList)payload;

                    double[] xData;
                    double[] yData;
                    xData = infoFromWork.ReadDoubleArray("sweepVoltage");
                    yData = infoFromWork.ReadDoubleArray("pdCurrentSum");

                    plot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "SOPC sweep";

                    plot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    plot.XAxis1.Label = "sweep voltage (Volt)";

                    if (plot.YAxis2 != null)
                        plot.YAxis2.Label = "pdCurrentSum (mA)";

                    this.plot.Visible = true;
                    plot.Refresh();
                }
                catch (SystemException)
                {
                    this.plot.Visible = false;
                    // Any GUI errors should not be fatal.
                }
            }
            else if (payload.GetType() == typeof(string))
            {
                this.MessageLabel.Text = (string)payload;
            }
            else
            { 
            
            }
        }

        private void Mod_MeasureSetupOpticalPolarisationGui_Load(object sender, EventArgs e)
        {

        }
    }
}

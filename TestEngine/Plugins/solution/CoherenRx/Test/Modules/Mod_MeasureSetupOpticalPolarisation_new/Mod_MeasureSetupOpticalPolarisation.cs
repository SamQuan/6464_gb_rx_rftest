// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSetupOpticalPolarisation.cs
//
// Author: sam.quan 2011.8
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSetupOpticalPolarisation : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// optimize local and signal path polarisation
        /// </summary>
        /// <param name="engine">Access to the Test Engine</param>
        /// <param name="userType">what access the user has</param>
        /// <param name="configData">Test Module configuration data</param>
        /// <param name="instruments">List of available instruments</param>
        /// <param name="chassis">List of available instrument chassis</param>
        /// <param name="calData">Test Module calibration data.</param>
        /// <param name="previousTestData">Previous test data required by the module</param>
        /// <returns>Results of the module in a DatumList</returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();
           
            #region disable instruments log
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }

            #endregion

            #region init equipment
            measSetupSig = (MeasureSetupManage)configData.ReadReference("sigMeasureSetupManage");
            measSetupLoc = (MeasureSetupManage)configData.ReadReference("locMeasureSetupManage");
            Instr_MS4640A_VNA LCAnalyzer = (Instr_MS4640A_VNA)measSetupSig.LightwaveComponentAnalyzer;
            sub_ctrl_sig = (Inst_Sub20ToAsic)configData.ReadReference("polsig");
            sub_ctrl_loc = (Inst_Sub20ToAsic)configData.ReadReference("polloc");
            
            TiaInstrument Tia_X = (TiaInstrument)configData.ReadReference("Tia_X");
            TiaInstrument Tia_Y = (TiaInstrument)configData.ReadReference("Tia_Y");
            
            #endregion

            #region init configure

            DatumList fcuNoiseCurrentList = configData.ReadListDatum("fcuNoiseCurrentList");

            spliterRatio_Sig = configData.ReadDouble("splitRate_Sig");
            spliterRatio_Loc = configData.ReadDouble("splitRate_Loc");
            powerTolerance_dB = configData.ReadDouble("powerTolerance_dB");
            powerSetSOPC_Sig_dB = configData.ReadDouble("powerSetSOPC_Sig_dB");
            powerSetSOPC_Loc_dB = configData.ReadDouble("powerSetSOPC_Loc_dB");
            hybirdChip = configData.ReadString("hybirdChip");
            hybirdChipEnum = (MeasureSetupManage.HybirdChipEnum)Enum.Parse(typeof(MeasureSetupManage.HybirdChipEnum), "Chip" + hybirdChip);
            serialNumber=configData.ReadString("dutSerialNum");

            MeasureSetupManage.SOPCSweepConfig polSweepConfig = new MeasureSetupManage.SOPCSweepConfig();
            polSweepConfig.sweepStart_V = configData.ReadDouble("sopcSweeStartVoltage_V"); ;
            polSweepConfig.sweepStop_V = configData.ReadDouble("sopcSweeStopVoltage_V");
            polSweepConfig.sweepStep_V = configData.ReadDouble("sopcSweeStepVoltage_V");
            polSweepConfig.sweepDelayTime_ms = configData.ReadDouble("sopcSweeDelayTime_ms");
            polSweepConfig.IcomplianceSOPC_A = configData.ReadDouble("sopcComplianceCurrent_mA") / 1000;

            //creat file name save polarization sweep data
            sopcAdjustTraceFileLoc = Util_GenerateFileName.GenWithTimestamp("results", "SopcSweep_Loc", serialNumber + "Chip" + hybirdChip, "csv");
            sopcAdjustTraceFileLoc = Directory.GetCurrentDirectory() + "\\" + sopcAdjustTraceFileLoc;

            sopcAdjustTraceFileSig = Util_GenerateFileName.GenWithTimestamp("results", "SopcSweep_SIG", serialNumber + "Chip" + hybirdChip, "csv");
            sopcAdjustTraceFileSig = Directory.GetCurrentDirectory() + "\\" + sopcAdjustTraceFileSig;

            #endregion

            LCAnalyzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.HOLD;

            #region optimize local polarisation controller

            //Adjust VOAs so that Sig = null,Loc = 6dB
         //   if (hybirdChip.Contains("x"))
         //   {
                engine.SendStatusMsg("set signal input power to null power");
                measSetupSig.SetOpticalInputPower(engine, nullPower_dB, spliterRatio_Sig, powerTolerance_dB);



                engine.SendStatusMsg("set local input power to " + powerSetSOPC_Loc_dB.ToString() + "dB");
                measSetupLoc.SetOpticalInputPower(engine, powerSetSOPC_Loc_dB, spliterRatio_Loc, powerTolerance_dB);

                // Adjust Pol_sig ,read PD current,find the max current,fix polarisation to this point
                engine.SendToGui("Local polarisation controller");
                engine.SendStatusMsg("Adjust local path polarisation!");


 //               sopcCtrlVoltage_Loc = measSetupLoc.SetOpticalPolarization(engine, MeasureSetupManage.OpticalPathEnum.LOC,
 //                        hybirdChipEnum, polSweepConfig, fcuNoiseCurrentList, sopcAdjustTraceFileLoc);

                sopcCtrlVoltage_Loc = setuppolarizationloc(engine,configData);
                
                //calculate the pd current sum
                pdCurrentSum_Loc = MeasureEachChipPdCurrentSum();
       //     }
            #endregion

            #region optimize signal polarisztion controller

            //Adjust sig VOAs so that sig = 0dB
            engine.SendStatusMsg("set Signal input power to " + powerSetSOPC_Sig_dB + "dB");
            measSetupSig.SetOpticalInputPower(engine, powerSetSOPC_Sig_dB, spliterRatio_Sig, powerTolerance_dB);

            //Adjust loc VOAs so that loc=null
            engine.SendStatusMsg("set local input power to null power");
            measSetupLoc.SetOpticalInputPower(engine, nullPower_dB, spliterRatio_Loc, powerTolerance_dB);

            // Adjust Pol_sig ,read PD current,find the max current,fix polarisation to this point
            engine.SendToGui("Signal polarisation controller");
            engine.SendStatusMsg("Adjust signal path polarisztion!");

    //        sopcCtrlVoltage_Sig = measSetupSig.SetOpticalPolarization(engine, MeasureSetupManage.OpticalPathEnum.SIG,
    //                hybirdChipEnum, polSweepConfig, fcuNoiseCurrentList, sopcAdjustTraceFileSig);

            sopcCtrlVoltage_Sig = setuppolarizationloc(engine, configData);
            
            //calculate the pd current sum
            pdCurrentSum_Sig = MeasureEachChipPdCurrentSum();

            #endregion

            LCAnalyzer.SweepMode = Instr_MS4640A_VNA.SweepModeEnum.CONT;

            #region return data
            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("polCtrlVoltSigChan1", sopcCtrlVoltage_Sig[0]);
            returnData.AddDouble("polCtrlVoltSigChan2", sopcCtrlVoltage_Sig[1]);
            returnData.AddDouble("polCtrlVoltSigChan3", sopcCtrlVoltage_Sig[2]);
            returnData.AddDouble("polCtrlVoltSigChan4", sopcCtrlVoltage_Sig[3]);
   //        if (hybirdChip.Contains("z"))
   //         {

                returnData.AddDouble("polCtrlVoltLocChan1", sopcCtrlVoltage_Loc[0]);
                returnData.AddDouble("polCtrlVoltLocChan2", sopcCtrlVoltage_Loc[1]);
                            returnData.AddDouble("polCtrlVoltLocChan3", sopcCtrlVoltage_Loc[2]);
                            returnData.AddDouble("polCtrlVoltLocChan4", sopcCtrlVoltage_Loc[3]);
  //           }
            returnData.AddDouble("pdCurrentSum_Loc", pdCurrentSum_Loc);
            returnData.AddFileLink("polLocSweepFile", sopcAdjustTraceFileLoc);
            returnData.AddDouble("pdCurrentSum_Sig", pdCurrentSum_Sig);


            returnData.AddFileLink("polSigSweepFile", sopcAdjustTraceFileSig);


            #endregion

            return returnData;                                                 
        }

 

        private double MeasureEachChipPdCurrentSum()
        {
            double pdCurrentSum = 0.0;
            if (hybirdChip.Contains("X"))
            {
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XIpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XIneg.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XQpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_XQneg.CurrentActual_amp * 1000 );
            }
            else
            {
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YIpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YIneg.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YQpos.CurrentActual_amp * 1000 );
                pdCurrentSum += (measSetupSig.PdBias.PdSource_YQneg.CurrentActual_amp * 1000 );
            }
            return pdCurrentSum;
        }

        private double[] setuppolarizationloc(ITestEngine engine, DatumList configData)
        {
            DatumList infoListToGui = new DatumList();
            double peakPointCtrlVoltage_V = 0.0;
            List<double> maxPeakOfPdList = new List<double>();
            List<double[]> listSopcCtrlVolt = new List<double[]>();
            List<double> listSopcFinalCtrlVolt = new List<double>();
            List<double> listSopcFinalPeakCurrent = new List<double>();

   //need this jd         Util_SavePlotData saveSopcAdjustTrace = new Util_SavePlotData();

            int maxLoopCounts = 2;
            double currentFinalSopcSet_mA = 0.0;
            double maxPeakCurrent_mA = 0.0;
            double[] arr_IfinalSopc_mA;  
            double sweepCounts = (2.5 - 0) / .05 + 1;
            
            // initialise loc polarisation controller
            sub_ctrl_loc = (Inst_Sub20ToAsic)configData.ReadReference("polloc");
            sub_ctrl_loc.SetDefaultState();
            for (uint I = 0; I < 4; I++)
            {
                sub_ctrl_loc.pol_write(0, I);
            }
            
           
            do
            {
                //if optimising one time can not reach best polarization status,then redo again
                maxPeakOfPdList.Clear();
                listSopcCtrlVolt.Clear();
                listSopcFinalCtrlVolt.Clear();
                listSopcFinalPeakCurrent.Clear();
                currentFinalSopcSet_mA = 0.0;
                maxPeakCurrent_mA = 0.0;
                //indexOfMaxPeak = 0;

                //optimising the polarisation voltage for each channel in turn(sweep ctrl volt,meas pd current sum,find peak point)
                for (uint chanNum = 0; chanNum < 2; chanNum++)
                //for (int iCount = 0; iCount < 2; iCount++)
                {
               
                    engine.SendStatusMsg("Adjust channel " + chanNum.ToString());
                    

                    //init sopc channels default sweep start volt setting
                    double[] sopcCtrlVoltArray_V = new double[4];

                    //for (int i = 0; i < sopcCtrlVoltArray_V.Length; i++)
                    //{
                    //    if (chanNum == 1)
                    //    {   //because can't set the control voltage to zero volt,so would set to sweepStart_V
                    //        sopcCtrlVoltArray_V[i] = sopcSweepConfig.sweepStart_V;
                    //    }
                    //    else
                    //    {
                    //        sopcCtrlVoltArray_V[i] = listSopcCtrlVolt[chanNum - 2][i];
                    //    }
                    //}

                    //variable save polarization sweep data
                    List<double> listPdCurrentSum = new List<double>();
                    List<double[]> listEachPdCurrent = new List<double[]>();
                    List<double> listSweepVoltage = new List<double>();
                    double[] pdCurrentArray_mA = new double[4];

                    //do sopc control voltage sweep,read pd current
                    for (uint i = 0; i < 50; i++)
                    {
                         //set polarization control voltage
                        sub_ctrl_loc.pol_write(i * 81, chanNum);
                        //Thread.Sleep((int)sopcSweepConfig.sweepDelayTime_ms);

                        //measure pd current
                        double pdCurrentSum_mA = MeasureEachChipPdCurrentSum();

                        //save pd current to list
                        listSweepVoltage.Add(i * 81);
                        listEachPdCurrent.Add(pdCurrentArray_mA);
                        listPdCurrentSum.Add(pdCurrentSum_mA);
                    }

                    //find max peak of pd current,save sopc ctrl voltage and max pd current
                    //will not get the maximum current if directly setting to peak point ctrl volt,so not interpolating the peak
                    //int index = Alg_FindFeature.FindIndexForMaxPeak(listPdCurrentSum.ToArray());
                    //peakPointCtrlVoltage_V = sopcSweepConfig.sweepStart_V + sopcSweepConfig.sweepStep_V * index;

                    //sopcCtrlVoltArray_V[chanNum - 1] = peakPointCtrlVoltage_V;

                    //listSopcCtrlVolt.Add(sopcCtrlVoltArray_V);
                    //maxPeakOfPdList.Add(listPdCurrentSum[index]);

                    ////set sopc control voltage to pd current peak point,
                    ////will not get the maximum current if directly setting to peak point ctrl volt
                    //for (int i = 0; i < index; i++)
                    //{
                    //    sopcSource.VoltageSetPoint_Volt = sopcSweepConfig.sweepStart_V + i * sopcSweepConfig.sweepStep_V;
                    //    Thread.Sleep(2);
                    //}

                    ////measure pd current
                    //double currentSum_mA = MeasurePdCurrent(hybirdChip, PdBias, fcuNoiseCurrentList, out pdCurrentArray_mA);

                    //listSopcFinalCtrlVolt.Add(sopcSource.VoltageSetPoint_Volt);
                    //listSopcFinalPeakCurrent.Add(currentSum_mA);

                    ////save sopc voltage sweep data
                    //saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "Chan" + chanNum.ToString() + "_SweepVolt_V", listSweepVoltage.ToArray());

                    ////save each pd sweep current to .csv
                    //for (int i = 0; i < pdCurrentArray_mA.Length; i++)
                    //{
                    //    List<double> tempList_arrPdCurrent = new List<double>();
                    //    foreach (double[] arrPdCurrent in listEachPdCurrent)
                    //    {
                    //        tempList_arrPdCurrent.Add(arrPdCurrent[i]);
                    //    }
                    //    double pdNumber = i + 1;
                    //    if (hybirdChip == HybirdChipEnum.ChipY)
                    //    {
                    //        //Ychip pd number is pd5/pd6/pd7/pd8,so here pdNumber = i + 4
                    //        pdNumber = i + 4;
                    //    }
                    //    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile,
                    //        "Chan" + chanNum.ToString() + "_PD" + pdNumber.ToString() + "_mA", tempList_arrPdCurrent.ToArray());
                    //}

                    ////save pd current sum and peak point control voltage to .csv
                    //saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, sopcSource.Name + "Sum_mA", listPdCurrentSum.ToArray());
                    //saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "Chan" + chanNum.ToString() + "_PeakCtrlVolt_V", sopcCtrlVoltArray_V);

                    //updata pdSum curve to GUI
                    infoListToGui = new DatumList();
                    infoListToGui.AddDoubleArray("sweepVoltage", listSweepVoltage.ToArray());
                    infoListToGui.AddDoubleArray("pdCurrentSum", listPdCurrentSum.ToArray());
                    engine.SendToGui("channel " + chanNum.ToString() + " sweep");
                    engine.SendToGui(infoListToGui);
                }

                maxPeakCurrent_mA = maxPeakOfPdList[0];
                for (int i = 0; i < maxPeakOfPdList.Count; i++)
                {
                    if (maxPeakCurrent_mA < maxPeakOfPdList[i])
                    {
                        maxPeakCurrent_mA = maxPeakOfPdList[i];
                    }
                }

              //  Thread.Sleep(1000);
             //   currentFinalSopcSet_mA = MeasurePdCurrent(hybirdChip, PdBias, fcuNoiseCurrentList, out arr_IfinalSopc_mA);

                maxLoopCounts--;
                engine.SendStatusMsg("do " + maxLoopCounts.ToString() + " times SOPC sweep!");

            } while (Math.Abs(currentFinalSopcSet_mA - maxPeakCurrent_mA) > maxPeakCurrent_mA * 0.07 && maxLoopCounts >= 1);

            //save sopc final setting voltage
        //    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "real control volt", listSopcFinalCtrlVolt.ToArray());
        //    saveSopcAdjustTrace.AppendResultToFile(sopcAdjustTraceFile, "pdCurrent_mA", listSopcFinalPeakCurrent.ToArray());

            return listSopcFinalCtrlVolt.ToArray();
        }


        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSetupOpticalPolarisationGui)); }
        }

        #region private variable

        MeasureSetupManage measSetupSig;
        MeasureSetupManage measSetupLoc;
        Inst_Sub20ToAsic sub_ctrl_sig;
        Inst_Sub20ToAsic sub_ctrl_loc;

        const double nullPower_dB = double.NegativeInfinity;

        double spliterRatio_Sig;
        double spliterRatio_Loc;
        double powerTolerance_dB;
        double powerSetSOPC_Sig_dB;
        double powerSetSOPC_Loc_dB;

        double[] sopcCtrlVoltage_Sig;
        double[] sopcCtrlVoltage_Loc;

        double pdCurrentSum_Loc;
        double pdCurrentSum_Sig;

        string hybirdChip;
        MeasureSetupManage.HybirdChipEnum hybirdChipEnum;

        string serialNumber;

        string sopcAdjustTraceFileLoc;
        string sopcAdjustTraceFileSig;

       
        #endregion

        #endregion
    }
}

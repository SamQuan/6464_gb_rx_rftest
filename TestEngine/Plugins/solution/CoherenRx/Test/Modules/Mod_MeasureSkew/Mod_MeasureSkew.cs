// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureDLP.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using CoherentRxTestCommonData;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSkew : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            LightwaveComponentAnalyzer = (Inst_Ag8703_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            //qualTest = configData.ReadBool("qualTest");

            #region init configure

            AnalyzerSetup analyzerSetup_S21 = new AnalyzerSetup();
            analyzerSetup_S21.startFrequency_GHz = configData.ReadDouble("s21StartFrequency_GHz");
            analyzerSetup_S21.stopFrequency_GHz = configData.ReadDouble("s21StopFrequency_GHz");
            analyzerSetup_S21.smoothingAperturePercent = configData.ReadDouble("s21smoothingAperturePercent");
            analyzerSetup_S21.referencePosition = configData.ReadDouble("s21referencePosition");
            analyzerSetup_S21.IFBandWidth = configData.ReadDouble("s21IFBandWidth");
            analyzerSetup_S21.activeChannelAveragingFactor = configData.ReadDouble("s21ActiveChannelAveragingFactor");
            analyzerSetup_S21.CalRegister = configData.ReadSint32("s21CalRegister");

            skewFreq_GHz = configData.ReadDouble("skewFreq_GHz");

            #endregion


            s21RealAndImaginaryTrace_Pos = (Trace)configData.ReadReference("s21RealAndImaginaryTrace_Pos");
            s21RealAndImaginaryTrace_Neg = (Trace)configData.ReadReference("s21RealAndImaginaryTrace_Neg");

            s21PhaseTrace_Pos = SparameterAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace_Pos);
            s21PhaseTrace_Neg = SparameterAnalysise.Gets21PhaseData(s21RealAndImaginaryTrace_Neg);

            skewFreq_GHz = SparameterAnalysise.SkewTest_Calculate(s21PhaseTrace_Pos, s21PhaseTrace_Neg, skewFreq_GHz, 360);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddReference("s21PhaseTrace_Pos", s21PhaseTrace_Pos);
            returnData.AddReference("s21PhaseTrace_Neg", s21PhaseTrace_Neg);
            returnData.AddDouble("DlpFreq_GHz", skewFreq_GHz);
            return returnData;
        }

        #region private function

        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSkewGui)); }
        }

        #region private variable
        Instr_AgWaveformGenerator WaveGenerator;

        Inst_Ag8703_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s21RealAndImaginaryTrace_Pos = new Trace();
        Trace s21RealAndImaginaryTrace_Neg = new Trace();
        Trace s21PhaseTrace_Pos = new Trace();
        Trace s21PhaseTrace_Neg = new Trace();

        double skewFreq_GHz;
        #endregion

        #endregion
    }
}

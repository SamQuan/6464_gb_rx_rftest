// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSPRR.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.Collections;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureSPRR : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            //qualTest = configData.ReadBool("qualTest");

            string testStage = configData.ReadString("testStage");
            string tiaOutputPort = configData.ReadString("tiaOutputPort");
            string serialNumber = configData.ReadString("DutSerialNumber");
            bool is6464 = configData.ReadBool("is6464");
            double testWavelength_nm;
            string plotName;
            if (configData.IsPresent("wavelengthNum"))
            {
                testWavelength_nm = configData.ReadDouble("wavelengthNum");
                plotName = "SPRR_" + tiaOutputPort + "_WL" + testWavelength_nm.ToString();
            }
            else
            {
                plotName = "SPRR_" + tiaOutputPort;
            }

            if (configData.IsPresent("doMaskSPRR"))
            {
                doMask = configData.ReadBool("doMaskSPRR");
                maskFitFile = configData.ReadString("sprrMaskFitFile");
            }

            double sprrSpecialPoint_GHz = configData.ReadDouble("sprrTestSeting_GHz");

            normalisationFrequency_GHz = configData.ReadDouble("normalisationFrequency_GHz");

            DatumList listEoSweepData = (DatumList)configData.ReadListDatum("listEoSweepData");

            Trace s21RealAndImaginaryTrace_Pdual = (Trace)listEoSweepData.ReadReference("P_S21");
            Trace s21RealAndImaginaryTrace_Psingle_Sig = (Trace)listEoSweepData.ReadReference("P_S21_SIG");
            Trace s21RealAndImaginaryTrace_Psingle_Loc = (Trace)listEoSweepData.ReadReference("P_S21_LOC");

            Trace s21RealAndImaginaryTrace_Ndual = (Trace)listEoSweepData.ReadReference("N_S21");
            Trace s21RealAndImaginaryTrace_Nsingle_Sig = (Trace)listEoSweepData.ReadReference("N_S21_SIG");
            Trace s21RealAndImaginaryTrace_Nsingle_Loc = (Trace)listEoSweepData.ReadReference("N_S21_LOC");


            Trace s21MagnitudeTrace_Pdual = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Pdual,is6464);
            Trace s21MagnitudeTrace_Psingle_Sig = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Psingle_Sig,is6464);
            Trace s21MagnitudeTrace_Psingle_Loc = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Psingle_Loc,is6464);

            Trace s21MagnitudeTrace_Ndual = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Ndual,is6464);
            Trace s21MagnitudeTrace_Nsingle_Sig = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Nsingle_Sig,is6464);
            Trace s21MagnitudeTrace_Nsingle_Loc = SparamAnalysise.Gets21MagintudeData(s21RealAndImaginaryTrace_Nsingle_Loc,is6464);


            Trace sprrTrace_Sig = new Trace();
            Trace sprrTrace_Loc = new Trace();
            Trace sprrTrace_P_Sig = new Trace();
            Trace sprrTrace_N_Sig = new Trace();
            Trace sprrTrace_P_Loc = new Trace();
            Trace sprrTrace_N_Loc = new Trace();


            sprrTrace_P_Sig = CalculateSprr(s21MagnitudeTrace_Pdual, s21MagnitudeTrace_Ndual, s21MagnitudeTrace_Psingle_Sig);
            sprrTrace_P_Loc = CalculateSprr(s21MagnitudeTrace_Pdual, s21MagnitudeTrace_Ndual, s21MagnitudeTrace_Psingle_Loc);
            sprrTrace_N_Sig = CalculateSprr(s21MagnitudeTrace_Pdual, s21MagnitudeTrace_Ndual, s21MagnitudeTrace_Nsingle_Sig);
            sprrTrace_N_Loc = CalculateSprr(s21MagnitudeTrace_Pdual, s21MagnitudeTrace_Ndual, s21MagnitudeTrace_Nsingle_Loc);

            sprrTrace_Sig = CalculateS21Average(engine, sprrTrace_P_Sig, sprrTrace_N_Sig);
            sprrTrace_Loc = CalculateS21Average(engine, sprrTrace_P_Loc, sprrTrace_N_Loc);

            //Get sprr of special point(-20GHz)
            double[] freqArray_GHz = s21RealAndImaginaryTrace_Pdual.GetXArray();
            double sigmax = GetSprrPowOfSpecialPoint(engine, sprrTrace_Sig, sprrSpecialPoint_GHz);
            double locmax = GetSprrPowOfSpecialPoint(engine, sprrTrace_Sig, sprrSpecialPoint_GHz);
            sprrPwrOfSpecialPoint_Sig_dB = GetSprrPowOfSpecialPoint(engine, sprrTrace_Sig, sprrSpecialPoint_GHz);
            sprrPwrOfSpecialPoint_Loc_dB = GetSprrPowOfSpecialPoint(engine, sprrTrace_Loc, sprrSpecialPoint_GHz);

            for (int i = 0; i < freqArray_GHz.Length; i++)
            {
                if (freqArray_GHz[i] > 38.5)
                {
                    break;
                }
                sprrPwrOfSpecialPoint_Sig_dB = GetSprrPowOfSpecialPoint(engine, sprrTrace_Sig, freqArray_GHz[i]);
                if (sprrPwrOfSpecialPoint_Sig_dB > sigmax)
                {
                    sigmax = sprrPwrOfSpecialPoint_Sig_dB;
                }
                sprrPwrOfSpecialPoint_Loc_dB = GetSprrPowOfSpecialPoint(engine, sprrTrace_Loc, freqArray_GHz[i]);
                if (sprrPwrOfSpecialPoint_Loc_dB > locmax)
                {
                    locmax = sprrPwrOfSpecialPoint_Loc_dB;
                }
 
            }
            sprrPwrOfSpecialPoint_Sig_dB = sigmax;
            sprrPwrOfSpecialPoint_Loc_dB = locmax;
            //save sprr data
            if (testStage.Contains("rf qual") || testStage.Contains("rf test") || testStage.Contains("rf"))
            {
                ArrayList plotSprr = new ArrayList();
                plotSprr.Add("FREQ," +
                                "P_dual Real,P_dual img,P_dual mag," +
                                "N_dual Real,N_dual img,N_dual mag," +
                                "P_single_sig real,P_single_sig img,P_single_sig mag," +
                                "P_single_loc real,P_single_loc img,P_single_loc mag," +
                                "N_single_sig real,N_single_sig img,N_single_sig mag," +
                                "N_single_loc real,N_single_loc img,N_single_loc mag," +
                                "SPRR_P_SIG,SPRR_P_LOC,SPRR_N_SIG,SPRR_N_LOC," +
                                "SPRR_SIG,SPRR_LOC,");

                for (int i = 0; i < freqArray_GHz.Length; i++)
                {
                    plotSprr.Add(freqArray_GHz[i] + ",");
                }

                CreatSprrPlotData(s21RealAndImaginaryTrace_Pdual, s21MagnitudeTrace_Pdual, ref plotSprr);
                CreatSprrPlotData(s21RealAndImaginaryTrace_Ndual, s21MagnitudeTrace_Ndual, ref plotSprr);
                CreatSprrPlotData(s21RealAndImaginaryTrace_Psingle_Sig, s21MagnitudeTrace_Psingle_Sig, ref plotSprr);
                CreatSprrPlotData(s21RealAndImaginaryTrace_Psingle_Loc, s21MagnitudeTrace_Psingle_Loc, ref plotSprr);
                CreatSprrPlotData(s21RealAndImaginaryTrace_Nsingle_Sig, s21MagnitudeTrace_Nsingle_Sig, ref plotSprr);
                CreatSprrPlotData(s21RealAndImaginaryTrace_Nsingle_Loc, s21MagnitudeTrace_Nsingle_Loc, ref plotSprr);
                CreatSprrPlotData(sprrTrace_P_Sig, ref plotSprr);
                CreatSprrPlotData(sprrTrace_P_Loc, ref plotSprr);
                CreatSprrPlotData(sprrTrace_N_Sig, ref plotSprr);
                CreatSprrPlotData(sprrTrace_N_Loc, ref plotSprr);
                CreatSprrPlotData(sprrTrace_Sig, ref plotSprr);
                CreatSprrPlotData(sprrTrace_Loc, ref plotSprr);

                sprrPlotFile = WritePlotData(plotSprr, plotName, serialNumber);
            }

            bool sprrSigMaskCheck = false;
            bool sprrLocMaskCheck = false;
            if (doMask)
            {
                sprrSigMaskCheck = SparamAnalysise.MaskAnalysis_Sprr(engine, sprrTrace_Sig, maskFitFile);
                sprrLocMaskCheck = SparamAnalysise.MaskAnalysis_Sprr(engine, sprrTrace_Loc, maskFitFile);
            }

            // return data
            DatumList returnData = new DatumList();
            returnData.AddDouble("sprr_Sig_GHz", sprrPwrOfSpecialPoint_Sig_dB);
            returnData.AddDouble("sprr_Loc_GHz", sprrPwrOfSpecialPoint_Loc_dB);
            returnData.AddDoubleArray("sprrArray_Sig", sprrTrace_Sig.GetYArray());
            returnData.AddDoubleArray("sprrArray_Loc", sprrTrace_Loc.GetYArray());
            returnData.AddDoubleArray("sprrArray_P_Sig", sprrTrace_P_Sig.GetYArray());
            returnData.AddDoubleArray("sprrArray_P_Loc", sprrTrace_P_Loc.GetYArray());
            returnData.AddDoubleArray("sprrArray_N_Sig", sprrTrace_N_Sig.GetYArray());
            returnData.AddDoubleArray("sprrArray_N_Loc", sprrTrace_N_Loc.GetYArray());
            returnData.AddBool("sprrSigMaskCheck", sprrSigMaskCheck);
            returnData.AddBool("sprrLocMaskCheck", sprrLocMaskCheck);
            if (sprrPlotFile != null)
            {
                returnData.AddFileLink("sprrPlotFile", sprrPlotFile);
            }

            return returnData;
        }

        /// <summary>
        /// get the sprr power of special frequency point
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="sprrTrace">trace include sprr data</param>
        /// <param name="sprrSpecialPoint_GHz">the special power point</param>
        /// <returns></returns>
        private double GetSprrPowOfSpecialPoint(ITestEngine engine, Trace sprrTrace, double sprrSpecialPoint_GHz)
        {
            double[] frequencyArray = sprrTrace.GetXArray();
            double[] sprrArray = sprrTrace.GetYArray();

            sweepPoints = frequencyArray.Length;

            double[] unsmoothedArray = sprrArray;
            double[] smoothedArray = new double[sweepPoints];

            double sprrPowOfSpecialPoint = 0.0;
            int smoothingCount = 7;
            List<double> analyseData = new List<double>();

            try
            {
                smoothedArray = Alg_Smoothing.AveragingSmoothing(unsmoothedArray, smoothingCount);

                int indexOfsprrSpecialFreqPoint = Alg_ArrayFunctions.FindIndexOfNearestElement(frequencyArray, sprrSpecialPoint_GHz);


                for (int i = 0; i <= indexOfsprrSpecialFreqPoint; i++)
                {
                    analyseData.Add(smoothedArray[i]);
                }

                analyseData.Sort();

                sprrPowOfSpecialPoint = analyseData[analyseData.Count - 1];

            }
            catch (Exception ex)
            {

                engine.ErrorInModule(ex.Message);
            }

            return sprrPowOfSpecialPoint;

        }

        /// <summary>
        /// add magnitude data int to plotData
        /// </summary>
        /// <param name="traceData">trace data include magnitude</param>
        /// <param name="plotData"></param>
        private void CreatSprrPlotData(Trace traceData, ref ArrayList plotData)
        {
            double[] yData = traceData.GetYArray();

            for (int i = 0; i < yData.Length; i++)
            {
                string str = yData[i] + ",";
                plotData[i+1] += str;
            }
        }

        /// <summary>
        /// add real imaginary and magnitude data int to plotData
        /// </summary>
        /// <param name="s21SweepData">s21 sweep data ,include real imaginary</param>
        /// <param name="s21MagnitudeTrace">s21 magnitude trace data</param>
        /// <param name="plotData"></param>
        private void CreatSprrPlotData(Trace s21RealAndImaginaryTrace, Trace s21MagnitudeTrace, ref ArrayList plotData)
        {
            double[] realArray = s21RealAndImaginaryTrace.GetYArray();
            double[] imaginaryArray = s21RealAndImaginaryTrace.GetZArray();
            double[] magnitudeArray_dB = s21MagnitudeTrace.GetYArray();
            for (int i = 0; i < realArray.Length; i++)
            {
                string str = realArray[i] + "," + imaginaryArray[i] + "," + magnitudeArray_dB[i] + ",";
                plotData[i+1] += str;
            }
        }

        /// <summary>
        /// calculate the s21 average,averageValue = (s21Trace_P+s21Trace_N)/2
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="s21Trace_P">s21 trace of P output include magnitude data</param>
        /// <param name="s21Trace_N">s21 trace of N output include magnitude data</param>
        /// <returns></returns>
        private Trace CalculateS21Average(ITestEngine engine, Trace s21Trace_P, Trace s21Trace_N)
        {
            Trace s21Trace_Avg = new Trace();

            double[] xArray_P = s21Trace_P.GetXArray();
            double[] yArray_P = s21Trace_P.GetYArray();

            double[] xArray_N = s21Trace_N.GetXArray();
            double[] yArray_N = s21Trace_N.GetYArray();

            double[] xArray_Avg = new double[xArray_P.Length];
            double[] yArray_Avg = new double[yArray_P.Length];

            if (xArray_P.Length != xArray_N.Length)
            {
                engine.ErrorInModule("P and N sweep point is not equal,can not calculate Equal value!");
            }

            for (int i = 0; i < xArray_P.Length; i++)
            {
                xArray_Avg[i] = (xArray_P[i] + xArray_N[i]) / 2;
                yArray_Avg[i] = (yArray_P[i] + yArray_N[i]) / 2;

                s21Trace_Avg.Add(xArray_Avg[i], yArray_Avg[i]);
            }
            return s21Trace_Avg;
        }

        /// <summary>
        ///  Calculate SPRR
        ///  SPRR =single(f)�C 20*log10( 10^(Ndual(f)/20)+10^(Pdual(f)/20))
        /// </summary>
        /// <param name="s21Trace_Pdual"></param>
        /// <param name="s21Trace_Ndual"></param>
        /// <param name="s21Trace_single"></param>
        /// <returns></returns>
        public Trace CalculateSprr(Trace s21Trace_Pdual,Trace s21Trace_Ndual, Trace s21Trace_single)
        {
            Trace sprrTrace = new Trace();

            double[] freqArray = s21Trace_Pdual.GetXArray();
            double[] magnitudeArray_Pdual = s21Trace_Pdual.GetYArray();
            double[] magnitudeArray_Ndual = s21Trace_Ndual.GetYArray();
            double[] magnitudeArray_single = s21Trace_single.GetYArray();

            for (int i = 0; i < freqArray.Length; i++)
            {
                
   //             double magnitude_sprr = magnitudeArray_single[i] - 
   //                 20 * Math.Log10(Math.Pow(10, magnitudeArray_Ndual[i] / 20) + Math.Pow(10, magnitudeArray_Pdual[i] / 20));

                double magnitude_sprr = magnitudeArray_single[i] - magnitudeArray_Ndual[i];
                double magnitude_sprr1 = magnitudeArray_single[i] - magnitudeArray_Pdual[i];

                sprrTrace.Add(freqArray[i], (magnitude_sprr + magnitude_sprr1)/2);
            }

            return sprrTrace;
        }

        /// <summary>
        /// Writes plot data to file ready for processing by the DataWrite component.
        /// </summary>
        /// <param name="plotData">List of comma seperated data in string format.</param>
        /// <param name="serialNo"></param>
        /// <param name="arm"></param>
        /// <returns>The name of the file created.</returns>
        private string WritePlotData(ArrayList plotData, string plotName, string serialNo)
        {
            string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "results");
            string fileName = "";

            try
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "results");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                fileName = plotName + "_" + serialNo + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                fileName = System.IO.Path.Combine(dropArea, fileName);
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fileName);
               
                foreach (string data in plotData)
                {
                    writer.WriteLine(data);
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }

            return fileName;
        }

        #region private function

        #endregion

        public Type UserControl
        {
            get { return (typeof(Mod_MeasureSPRRGui)); }
        }

        #region private variable


        //InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        //bool qualTest;
        int sweepPoints;
        string sprrPlotFile;

        bool doMask = false;
        string maskFitFile;

        double sprrPwrOfSpecialPoint_Sig_dB;
        double sprrPwrOfSpecialPoint_Loc_dB;
        double normalisationFrequency_GHz;
        

        #endregion

        #endregion
    }
}

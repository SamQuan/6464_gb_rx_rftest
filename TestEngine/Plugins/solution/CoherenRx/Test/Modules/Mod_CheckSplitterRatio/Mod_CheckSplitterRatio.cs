// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasureSetupOpticalInputPower.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.ToolKit.ConherenRx;


namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_CheckSplitterRatio : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            if (!engine.IsSimulation)
            {
                foreach (Chassis aChassis in chassis.Values)
                {
                    aChassis.EnableLogging = false;
                }
                foreach (Instrument var in instruments.Values)
                {
                    var.EnableLogging = false;
                }
            }
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();

            #region init equipment

            OPMMon_Sig = (InstType_OpticalPowerMeter)configData.ReadReference("OPMMon_Sig");
            OPMRef_Sig = (InstType_OpticalPowerMeter)configData.ReadReference("OPMRef_Sig");
            OPMMon_Loc = (InstType_OpticalPowerMeter)configData.ReadReference("OPMMon_Loc");
            OPMRef_Loc = (InstType_OpticalPowerMeter)configData.ReadReference("OPMRef_Loc");

            sigChainSetupManage = (MeasureSetupManage)configData.ReadReference("sigChainSetupManage");
            locChainSetupManage = (MeasureSetupManage)configData.ReadReference("locChainSetupManage");

            double powerRatioTolerance_dB = configData.ReadDouble("PowerRatioTolerance");
            double powerRatioSig = configData.ReadDouble("splitRate_Sig");
            double powerRatioLoc = configData.ReadDouble("splitRate_Loc");

            #endregion

            engine.ShowContinueUserQuery("Please move the Signal fibre and Local fibre to the external power head!");

            ButtonId respond = ButtonId.Cancel;

            do
            {
                double powerMonSig_dBm = OPMMon_Sig.ReadPower();
                double powerRefSig_dBm = OPMRef_Sig.ReadPower();
                double powerMonLoc_dBm = OPMMon_Loc.ReadPower();
                double powerRefLoc_dBm = OPMRef_Loc.ReadPower();

                //check powerRatioSig if out of tolerance
                double powerRatio = powerRefSig_dBm / powerMonSig_dBm;
                double distance = Math.Abs(powerRatio - powerRefSig_dBm);
                string message = "";
                if (distance > powerRatioTolerance_dB)
                {
                    message += "sig filter power Ratio is out of tolerance!\r\n";
                    checkRatioOK = false;
                }
                else
                {
                    checkRatioOK = true;
                }

                //check powerRatioLoc if out of tolerance
                powerRatio = powerMonLoc_dBm / powerRefLoc_dBm;
                distance = Math.Abs(powerRatio - powerRatioTolerance_dB);
                if (distance > powerRatioTolerance_dB)
                {
                    message += "loc filter power Ratio is out of tolerance!\r\n";
                    checkRatioOK = false;
                }
                else
                {
                    checkRatioOK = true;
                }

                if (!checkRatioOK)
                {
                    message += "please clean fibre or click Cancle ,exit test to do Ratio calibration!";
                    respond = engine.ShowContinueCancelUserQuery(message);
                }

            } while (respond != ButtonId.Cancel);


            // return data
            DatumList returnData = new DatumList();

            returnData.AddBool("RationCheckOk",checkRatioOK);

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(Mod_CheckSplitterRatioGui)); }
        }

        #region private variable

        InstType_OpticalPowerMeter OPMMon_Sig;
        InstType_OpticalPowerMeter OPMMon_Loc;
        InstType_OpticalPowerMeter OPMRef_Sig;
        InstType_OpticalPowerMeter OPMRef_Loc;
        MeasureSetupManage sigChainSetupManage;
        MeasureSetupManage locChainSetupManage;

        bool checkRatioOK = false;
        #endregion

       
        #endregion
    }
}

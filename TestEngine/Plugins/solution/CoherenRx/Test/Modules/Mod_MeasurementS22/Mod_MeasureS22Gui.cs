// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Mod_MeasurementSetupGui.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using NPlot;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class Mod_MeasureS22Gui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Mod_MeasureS22Gui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(Trace))
            //{
            {
                try
                {
                    Trace sweepData = (Trace)payload;
                        //this.MessageLabel.Text = "Completed " + sweepData.Type.ToString() + " sweep on " + sweepData.SrcMeter.ToString();

                    double[] xData;
                    double[] yData;
                    xData = sweepData.GetXArray();
                    yData = sweepData.GetYArray();

                    s22plot.Clear();
                    LinePlot plotData = new LinePlot(yData, xData);
                    plotData.Color = Color.DarkBlue;
                    plotData.Label = "S22 sweep";

                    s22plot.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                    s22plot.XAxis1.Label = "Optical Frequency (GHz)";

                    if (s22plot.YAxis2 != null)
                        s22plot.YAxis2.Label = "Optical power (dBm)";

                    this.s22plot.Visible = true;
                    s22plot.Refresh();
                }
                catch (SystemException)
                {
                    this.s22plot.Visible = false;
                    // Any GUI errors should not be fatal.
                }
            }
            else if (payload.GetType() == typeof(String))
            {
                this.labelStatus.Text = (string)payload;
            }
        }

        private void s22plot_Click(object sender, EventArgs e)
        {

        }
    }
}

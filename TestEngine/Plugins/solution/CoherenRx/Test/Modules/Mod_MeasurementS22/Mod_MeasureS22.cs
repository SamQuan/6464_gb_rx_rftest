// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Initialise.cs
//
// Author: 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.ToolKit.ConherenRx;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoherentRxTestCommonData;
using System.Threading;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Mod_MeasureS22 : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// measure s22 at one RF port,and pick up the bandwidth of s22
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType,
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis,
            DatumList calData, DatumList previousTestData)
        {
            engine.StopTestProcessTimer();
            engine.StartTestProcessTimer();
            engine.GuiToFront();
            engine.GuiShow();


            LightwaveComponentAnalyzer = (InstType_LightwaveComponentAnalyzer)configData.ReadReference("LightwaveComponentAnalyzer");

            if (configData.IsPresent("qualTest"))
            {
                qualTest = configData.ReadBool("qualTest");
            }

            string rfPortName = configData.ReadString("RfPortName");

            #region init configure


            //AnalyzerSetup analyzerSetup_S22 = new AnalyzerSetup();
            //analyzerSetup_S22.startFrequency_GHz = configData.ReadDouble("s22StartFrequency_GHz");
            //analyzerSetup_S22.stopFrequency_GHz = configData.ReadDouble("s22StopFrequency_GHz");
            //analyzerSetup_S22.sweepPoints = (uint)configData.ReadSint32("sweepPoints");
            //analyzerSetup_S22.smoothingAperturePercent = configData.ReadDouble("s22smoothingAperturePercent");
            //analyzerSetup_S22.referencePosition = configData.ReadDouble("s22referencePosition");
            //analyzerSetup_S22.IFBandWidth = configData.ReadDouble("s22IFBandWidth");
            //analyzerSetup_S22.activeChannelAveragingFactor = configData.ReadDouble("s22ActiveChannelAveragingFactor");
            //analyzerSetup_S22.CalRegister = configData.ReadSint32("CalRegister");

            int s22CalRegister = 0;
            bool is6464 = false;
            //if (LightwaveComponentAnalyzer.HardwareIdentity.Contains("8703"))
            //{
            //    s22CalRegister = configData.ReadSint32("CalRegister");
            //}
            // remove possible 8703 code as sometimes error happen when query the HardwareIdentity  
                Instr_MS4640A_VNA MS4046A = (Instr_MS4640A_VNA)LightwaveComponentAnalyzer;

                MS4046A.EmbedOrDeEmbedEnable = true;
                Thread.Sleep(300);
                MS4046A.ChannelCount = 1;
                Thread.Sleep(300);
                MS4046A.ActiveMeausurementChannel = 1;
                Thread.Sleep(300);
             


            AnalysisConfig_S22 analysisConfig_S22 = new AnalysisConfig_S22();
            analysisConfig_S22.smoothingPoints = configData.ReadDouble("s22smoothingPoints");
            analysisConfig_S22.normalisationFrequency_GHz = configData.ReadDouble("s22normalisationFrequency_GHz");
            analysisConfig_S22.qualPointPower_dB = configData.ReadDouble("specialPowerOfS22_dB");
            analysisConfig_S22.maskFitFile = configData.ReadString("s22MaskFitFile");
            analysisConfig_S22.sweepPoints = (int)LightwaveComponentAnalyzer.NumberOfSweepPoint;
            analysisConfig_S22.doMask = configData.ReadBool("doS22MaskTest");
            is6464 = configData.ReadBool("is6464");

            #endregion

            SparamAnalysisResult s22AnaysiseResult = new SparamAnalysisResult();


            engine.SendToGui(rfPortName + "_S22 sweeping.....");
            engine.SendStatusMsg(rfPortName + "_S22 sweeping.....");

            s22Trace = SparamMeasurement.Get_S22(engine, LightwaveComponentAnalyzer, s22CalRegister);

            s22AnaysiseResult = SparamAnalysise.SimpleAnalysis_S22(engine, s22Trace, analysisConfig_S22);


            engine.SendToGui(s22Trace);

            // return data
            DatumList returnData = new DatumList();

            returnData.AddDouble("bandWidth_S22_GHz", s22AnaysiseResult.bandWidth_GHz);
            returnData.AddReference("PlotData_S22", s22AnaysiseResult.PlotData);
            if (analysisConfig_S22.doMask)
            {
                returnData.AddBool("maskPassFail", s22AnaysiseResult.Sparam_MaskPassFail);
            }
            else
            {
                returnData.AddBool("maskPassFail", true);
            }
            return returnData;
        }


        #region private function

        #endregion


        public Type UserControl
        {
            get { return (typeof(Mod_MeasureS22Gui)); }
        }

        #region private variable
        //Instr_AgWaveformGenerator WaveGenerator;

        InstType_LightwaveComponentAnalyzer LightwaveComponentAnalyzer;

        Trace s22Trace = new Trace();
        bool qualTest;
        #endregion

        #endregion
    }
}

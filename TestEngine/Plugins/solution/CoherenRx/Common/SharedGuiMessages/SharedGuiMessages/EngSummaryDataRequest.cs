using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsgs
{
    public class EngSummaryDataRequest
    {
        public EngSummaryDataRequest(string summaryData)
        {
            this.summaryData = summaryData;
        }

        private string summaryData;

        public string SummaryData
        {
            get { return summaryData; }
            set { summaryData = value; }
        }
    }
}

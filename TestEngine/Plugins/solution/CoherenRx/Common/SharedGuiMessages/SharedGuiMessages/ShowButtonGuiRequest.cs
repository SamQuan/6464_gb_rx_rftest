using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsgs
{
    public class ShowButtonGuiRequest
    {
        public ShowButtonGuiRequest(bool showButton)
        {
            this.showButton = showButton;
        }

        private bool showButton;

        public bool ShowButton
        {
            get { return showButton; }
            set { showButton = value; }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsgs
{
    public class SetPlotTitleRequest
    {
        public SetPlotTitleRequest(string title)
        {
            this.title = title;
        }

        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
    }
}
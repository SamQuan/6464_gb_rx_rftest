using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Module;
using System.Collections;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.CoherentRxTestCommonData
{
    /// <summary>
    /// sparameter analysis function wrapper
    /// </summary>
    public sealed class SparamAnalysise
    {
        SparamAnalysise()
        {

        }

        /// <summary>
        /// Cal Magnitude infomation data from X Y data,
        /// </summary>
        /// <param name="s21RealAndImaginaryTrace">Trace inculde X Y data,Xarray is freq_GHz,Yarray is real,Zarray is imaginary</param>
        /// <returns>Trace inculde frequency and Magnitude,Xarray is freq_GHz,Yarray is Magnitude</returns>
        public static Trace
            Gets21MagintudeData(Trace s21RealAndImaginaryTrace, bool is6464)
        {
            Trace s21MagintudeTrace = new Trace();
            double[] freqArray = s21RealAndImaginaryTrace.GetXArray();
            double[] realArray = s21RealAndImaginaryTrace.GetYArray();
            double[] imaginaryArray = s21RealAndImaginaryTrace.GetZArray();
            double[] smoothedArray = new double[1401];
            double[] unsmoothedArray = new double[1401];
            double tempmag;
            string YIPFile;


            if (is6464)
            {
                YIPFile = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\6464loss.csv";
            }
            else
            {
                YIPFile = Directory.GetCurrentDirectory() + "\\Configuration\\ConherentRx\\YIP.csv";
            }


            using (StreamReader sr = new StreamReader(YIPFile))

                for (int i = 0; i < realArray.Length; i++)
                {
                    double temp = Math.Sqrt(Math.Pow(realArray[i], 2) + Math.Pow(imaginaryArray[i], 2));
                    double magintude_dB = 20 * Math.Log10(temp);
                    tempmag = magintude_dB;
                    string LossVal = sr.ReadLine();
                    //de-embedding pcb and cable inser loss
                    magintude_dB -= double.Parse(LossVal);
            //        Temparray[i] = magintude_dB;
                    if (tempmag == magintude_dB)
                    {
                        magintude_dB = 1;
                    }
                    //s21MagintudeTrace.Add(freqArray[i], magintude_dB);
                    unsmoothedArray[i] = magintude_dB;
                }

           // double[] tempasd = s21MagintudeTrace.GetYArray();
                       smoothedArray = Alg_Smoothing.AveragingSmoothing(unsmoothedArray, 9);
                       for (int i = 0; i < realArray.Length; i++)
                       {
                           s21MagintudeTrace.Add(freqArray[i], smoothedArray[i]);
                       }

            return s21MagintudeTrace;
        }

        /// <summary>
        /// Cal phase infomation data from X Y data,
        /// </summary>
        /// <param name="s21RealAndImaginaryTrace">Trace inculde X Y data,Xarray is real,Yarray is imaginary,Zarray is freq_GHz</param>
        /// <returns>Trace inculde frequency and Magnitude,Xarray is freq_GHz,Yarray is Phase</returns>
        public static Trace Gets21PhaseData(Trace s21RealAndImaginaryTrace, double phaseAnalysisStopFreq_GHz)
        {
            Trace s21PhaseTrace = new Trace();
            double[] freqArray = s21RealAndImaginaryTrace.GetXArray();
            double[] realArray = s21RealAndImaginaryTrace.GetYArray();
            double[] imaginaryArray = s21RealAndImaginaryTrace.GetZArray();

            int index = Alg_ArrayFunctions.FindIndexOfNearestElement(freqArray, phaseAnalysisStopFreq_GHz);
            double[] freqAnalyseArray = new double[index + 1];
            double[] realAnalyseArray = new double[index + 1];
            double[] imaginaryAnalyseArray = new double[index + 1];
            Array.Copy(freqArray, freqAnalyseArray, index + 1);
            Array.Copy(realArray, realAnalyseArray, index + 1);
            Array.Copy(imaginaryArray, imaginaryAnalyseArray, index + 1);

            for (int i = 0; i < realAnalyseArray.Length; i++)
            {
                double phase_dge = (Math.Atan2(realAnalyseArray[i], imaginaryAnalyseArray[i]) / Math.PI) * 180;

                s21PhaseTrace.Add(freqAnalyseArray[i], phase_dge);
            }
            return s21PhaseTrace;

        }

        /// <summary>
        /// Analysis s21 data ,find null points,
        /// return the first null point frequency in GHz,
        /// if no null was found then return double.NaN
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="s21MagintudeData">s21 sweep data(magintude)</param>
        /// <param name="config">Analysis config data</param>
        /// <returns>return the first null point frequency in GHz,if no null was found then return double.NaN </returns>
        public static double FindNullPoint_S21(ITestEngine engine, Trace s21MagintudeData, AnalysisConfig_S21 config)
        {
            // Arrays required for analysis.
            int sweepPoints = config.sweepPoints;
            double[] frequency = new double[sweepPoints];
            double[] unsmoothedArray = new double[sweepPoints];
            double[] smoothedArray = new double[sweepPoints];

            double[] normalisedArray = new double[sweepPoints];

            double[] s21Array = new double[sweepPoints];
            double[] s21XArray = new double[sweepPoints];
            double[] s21YArray = new double[sweepPoints];

            double[] s21NormalisationYArray = new double[sweepPoints];

            double normalisationFrequency_GHz = config.normalisationFrequency_GHz;
            double analysisStartFreq_GHz = config.OscillationStartFreq_GHz;
            double analysisStopFreq_GHz = config.OscillationStopFreq_GHz;

            //double firstNullPoint = 0.0;
            int firstNullPointIndex = 0;
            int normalisationPoint;

            if (s21MagintudeData != null)
            {
                s21XArray = s21MagintudeData.GetXArray();
                s21YArray = s21MagintudeData.GetYArray();
            }

            if (sweepPoints > 801)
            {
                string errorMsg = "Warning, returned " + sweepPoints.ToString() + " points, which implies SP cal done with too many points.  Unlikely to work.";
                engine.ErrorInModule(errorMsg);
            }

            // Transfer plot data into these single dim arrays for analyse
            frequency = s21XArray;

            // Transfer plot data into these single dim arrays for analyse
            unsmoothedArray = s21YArray;

            // get smoothingCount from configuration
            int smoothingCount = (int)config.smoothingPoints;

            try
            {
                smoothedArray = Alg_Smoothing.AveragingSmoothing(unsmoothedArray, smoothingCount);
                smoothedArray = Alg_Smoothing.AveragingSmoothing(smoothedArray, smoothingCount * 3);

                normalisationPoint = Alg_ArrayFunctions.FindIndexOfNearestElement(frequency, normalisationFrequency_GHz);

                normalisedArray = Alg_Normalisation.LogNormalise(smoothedArray, normalisationPoint);

                int analysisStartIndex = FindRightPointofElement(frequency, analysisStartFreq_GHz, SearchCondition.First);
                int analysisStopIndex = FindRightPointofElement(frequency, analysisStopFreq_GHz, SearchCondition.First);

                double[] OsciAnalysisArray = Alg_ArrayFunctions.ExtractSubArray(normalisedArray, analysisStartIndex, analysisStopIndex);

                int[] valleyIndexes = Alg_FindFeature.FindIndexesForAllValleys(OsciAnalysisArray, 2);


                //ajudge if more than 2 points < Osci limit in all valley points

                for (int i = 0; i < valleyIndexes.Length; i++)
                {

                    int loopTimes = 0;

                    int comparePoints = 30;
                    if ((valleyIndexes[i] - analysisStartIndex) < comparePoints)
                    {
                        comparePoints = valleyIndexes[i] - analysisStartIndex;
                    }
                    if (analysisStopIndex - (valleyIndexes[i]) < comparePoints)
                    {
                        comparePoints = analysisStopIndex - (valleyIndexes[i]);
                    }

                    List<double> tempList = new List<double>();
                    for (int j = 1; j < comparePoints; j++)
                    {
                        int indexOfVally = valleyIndexes[i];

                        double tempValue1 = Math.Round(OsciAnalysisArray[indexOfVally], 4);
                        double tempValue2 = Math.Round(OsciAnalysisArray[indexOfVally + j], 4);
                        double tempValue3 = Math.Round(OsciAnalysisArray[indexOfVally - j], 4);
                        tempList.Add(tempValue2);
                        tempList.Add(tempValue3);

                        if (tempValue1 > tempValue2 || tempValue1 > tempValue3)
                        {
                            break;
                        }
                        loopTimes++;
                    }

                    int minimumMatchPoints = (int)comparePoints * 2 / 3;

                    //if (minimumMatchPoints<15)
                    //{
                    //    minimumMatchPoints = 15;
                    //}
                    tempList.Sort();
                    if (loopTimes >= minimumMatchPoints && (tempList[tempList.Count - 1] - OsciAnalysisArray[valleyIndexes[i]]) > 2.5)
                    {
                        firstNullPointIndex = valleyIndexes[i] + analysisStartIndex;
                        break;
                    }
                }

                //if (firstNullPoint <= 0.0)
                //{
                //    firstNullPointIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(normalisedArray, firstNullPoint);
                //}
            }
            catch (Exception ex)
            {
                engine.ErrorInModule(ex.Message);
            }

            if (firstNullPointIndex != 0)
            {
                return frequency[firstNullPointIndex];
            }
            else
            {
                return double.NaN;
            }
        }

        /// <summary>
        /// Calculate 3dB bandwidth, and do fit mask if configure need to do
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="s21MagintudeData">Trace include magintude and freq</param>
        /// <param name="config">struct of s21 analysis configure</param>
        /// <returns>struct of analysis result</returns>
        public static SparamAnalysisResult CalculateBandwidthAndMask_S21(ITestEngine engine, Trace s21MagintudeData, AnalysisConfig_S21 config)
        {
            // Arrays required for analysis.
            SparamAnalysisResult rtnData = new SparamAnalysisResult();
            string errorString = "";

            int sweepPoints = config.sweepPoints;
            double[] frequency = new double[sweepPoints];
            double[] unsmoothedArray = new double[sweepPoints];
            double[] smoothedArray = new double[sweepPoints];

            double[] normalisedArray = new double[sweepPoints];

            double[] s21Array = new double[sweepPoints];
            double[] s21XArray = new double[sweepPoints];
            double[] s21YArray = new double[sweepPoints];

            double[] s21NormalisationYArray = new double[sweepPoints];

            double normalisationFrequency_GHz = config.normalisationFrequency_GHz;

            bool s21ComplexMaskFlag = false;

            int normalisationPointIndex;

            double s21Frequency;

            if (s21MagintudeData != null)
            {
                s21XArray = s21MagintudeData.GetXArray();
                s21YArray = s21MagintudeData.GetYArray();
            }
            else
            {
                errorString = "S21 sweep data is null!";
                engine.ErrorInModule(errorString);
            }

            ArrayList plotS21 = new ArrayList();
            plotS21.Add("Raw S21_dB,Normalized S21_dB,AfterSmoothingBeforeNormalisation_dB"); 

            if (sweepPoints > 801)
            {
                errorString = errorString + "Warning, returned " + sweepPoints.ToString() + " points, which implies SP cal done with too many points.  Unlikely to work.";
                engine.ErrorInModule(errorString);
            }

            // Transfer plot data into these single dim arrays for analyse
            frequency = s21XArray;

            // Transfer plot data into these single dim arrays for analyse
            unsmoothedArray = s21YArray;


            // get smoothingCount from configuration
            int smoothingCount = config.smoothingPoints;

            //find -3dB point freq
            try
            {
                //smoothedArray = Alg_Smoothing.PascalTriangleSmooth(unsmoothedArray, smoothingCount);
                smoothedArray = Alg_Smoothing.AveragingSmoothing(unsmoothedArray, smoothingCount);
                normalisationPointIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(frequency, normalisationFrequency_GHz);
                normalisedArray = Alg_Normalisation.LogNormalise(smoothedArray, normalisationPointIndex);

                int bandWidthIndex;

                bandWidthIndex = FindRightPointofElement(normalisedArray, config.bandWidthPoint_dB, SearchCondition.First);

                if (bandWidthIndex == 0)
                {
                    s21Frequency = 999;
                }
                else
                {
                    s21Frequency = frequency[bandWidthIndex];
                }
            }
            catch (Exception ex)
            {
                s21Frequency = 999;
                engine.ErrorInModule(ex.Message);
            }

            //add return data
            rtnData.bandWidth_GHz = s21Frequency;

            // Store the raw normalised S21 data
            for (int j = 0; j < unsmoothedArray.Length; j++)
            {
                plotS21.Add(unsmoothedArray[j].ToString() + "," + normalisedArray[j].ToString() + "," + smoothedArray[j].ToString());
            }

            // Do the S21 mask fitting, for upper mask:    
            // Perform complex mask fit using the macro pointed file 'S21 Mask file'
            if (config.doMask)
            {
                List<Dictionary<string, double>> maskCfgData = GetMaskLimit(config.maskFitFile);

                s21ComplexMaskFlag = complexMask(s21XArray, normalisedArray, maskCfgData);

                // Store the result of the mask fit. 
                rtnData.Sparam_MaskPassFail = s21ComplexMaskFlag;
            }

            //rtnData.PlotFile = WritePlotData(plotS21);
            rtnData.PlotData = plotS21;
            //System.Threading.Thread.Sleep(2000);

            return rtnData;
        }

        /// <summary>
        /// do mask for s22 sweep data,return pass/fail flat
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="s22Data">Trace of s22</param>
        /// <param name="cfgData">struct of s22 analysis configure</param>
        /// <returns>mask pass/fail result</returns>
        private static bool MaskAnalysis_S22(ITestEngine engine, Trace s22Data, AnalysisConfig_S22 cfgData)
        {
            int sweepPoints;
            string errorString = "";
            string s22MaskFile = cfgData.maskFitFile;
            List<Dictionary<string, double>> maskCfgData = GetMaskLimit(s22MaskFile);

            sweepPoints = cfgData.sweepPoints;

            // get smootingCount from configure
            int smoothingCount = (int)cfgData.smoothingPoints;


            double[] frequency = new double[sweepPoints];
            double[] unsmoothedArray = new double[sweepPoints];
            double[] smoothedArray = new double[sweepPoints];

            double[] normalisedArray = new double[sweepPoints];

            double[] s22Array = new double[sweepPoints];
            double[] s22XArray = new double[sweepPoints];
            double[] s22YArray = new double[sweepPoints];

            bool s22ComplexMaskFlag = false;

            if (s22Data != null)
            {
                s22XArray = s22Data.GetXArray();
                s22YArray = s22Data.GetYArray();
            }

            //          if (sweepPoints > 801)
            //          {
            //              errorString = errorString + "Warning, returned " + sweepPoints.ToString() + " points, which implies SP cal done with too many points.  Unlikely to work.";
            //              engine.ErrorInModule(errorString);
            //          }

            // Transfer plot data into these single dim arrays for use with analyse routines
            frequency = s22XArray;

            // Transfer plot data into these single dim arrays for use with analyse routines
            unsmoothedArray = s22YArray;

            smoothedArray = Alg_Smoothing.PascalTriangleSmooth(unsmoothedArray, smoothingCount);

            // Perform a complex mask fit using the macro pointed file 'S22 Mask file'
            s22ComplexMaskFlag = complexMask(s22XArray, smoothedArray, maskCfgData);

            return s22ComplexMaskFlag;
        }

        /// <summary>
        /// simple analyses s22 data,extraction s22 value in the special freq_GHz 
        /// </summary>
        /// <param name="engine">ITestEngine</param>
        /// <param name="s22Data">Trace</param>
        /// <param name="cfgData">struct of s22 analysis configure</param>
        /// <returns>struct of analysis result</returns>
        public static SparamAnalysisResult SimpleAnalysis_S22(ITestEngine engine, Trace s22Data, AnalysisConfig_S22 cfgData)
        {
            int sweepPoints;
            sweepPoints = cfgData.sweepPoints;

            string errorString = "";

            double[] frequencyArray = new double[sweepPoints];
            double[] unsmoothedArray = new double[sweepPoints];
            double[] smoothedArray = new double[sweepPoints];

            double[] normalisedArray = new double[sweepPoints];

            double[] s22XArray = new double[sweepPoints];
            double[] s22YArray = new double[sweepPoints];

            double normalisationFrequency_GHz = cfgData.normalisationFrequency_GHz;
            //int normalisationPointIndex;
            double s22Freq;

            if (s22Data != null)
            {
                s22XArray = s22Data.GetXArray();
                s22YArray = s22Data.GetYArray();
            }
            else
            {
                engine.ErrorInModule("S22 sweep data is null!");
            }

            ArrayList plotS22 = new ArrayList();
            plotS22.Add("Frequency_GHz, S22_dB");

            //    if (sweepPoints > 801)
            //    {
            //        errorString = errorString + "Warning, returned " + sweepPoints.ToString() + " points, which implies SP cal done with too many points.  Unlikely to work.";
            //        engine.ErrorInModule(errorString);
            //    }

            // Transfer plot data into these single dim arrays for use with analyse routines
            frequencyArray = s22XArray;

            // Transfer plot data into these single dim arrays for use with analyse routines
            unsmoothedArray = s22YArray;

            // get smothing count from configure
            int smoothingCount = (int)cfgData.smoothingPoints;

            //find special power point
            try
            {
                smoothedArray = Alg_Smoothing.AveragingSmoothing(unsmoothedArray, smoothingCount);

                int s22AtSpecialPowerPoint = smoothedArray.Length - 1;
                for (int i = 0; i < smoothedArray.Length; i++)
                {
                    if (smoothedArray[i] >= -10)
                    {
                        s22AtSpecialPowerPoint = i;
                        break;
                    }
                }

                s22Freq = frequencyArray[s22AtSpecialPowerPoint];
            }
            catch (Exception ex)
            {
                s22Freq = 999999;
                engine.ErrorInModule(ex.Message);
            }

            // Store the raw normalised S21 data
            for (int j = 0; j < sweepPoints; j++)
            {
                plotS22.Add(frequencyArray[j].ToString() + "," + smoothedArray[j].ToString());
            }

            bool s22MaskTestResult = false;
            if (cfgData.doMask)
            {
                s22MaskTestResult = MaskAnalysis_S22(engine, s22Data, cfgData);
            }

            SparamAnalysisResult rtnData = new SparamAnalysisResult();

            rtnData.bandWidth_GHz = s22Freq;
            rtnData.PlotData = plotS22;
            rtnData.Sparam_MaskPassFail = s22MaskTestResult;
            return rtnData;
        }

        /// <summary>
        /// Unwrapping phase data
        /// </summary>
        /// <param name="engine">reference ITestEngine</param>
        /// <param name="rawData">raw data</param>
        /// <param name="unwrapData">unwrap data</param>
        /// <param name="unwrapDegree">unwrap degree</param>
        /// <returns>whether unwrap phase data success,true is success,false is unsuccess</returns>
        public static bool UnwrappingPhaseData(ITestEngine engine, Trace rawData, out Trace unwrapData, double unwrapDegree)
        {
            bool bUnwrap = false;
            double[] XArray = rawData.GetXArray();
            double[] YArray = rawData.GetYArray();

            List<double> listSngCount = new List<double>();
            List<double> listsngArrayNeg = new List<double>();
            List<double> listsngArrayPos = new List<double>();

            for (int i = 0; i < YArray.Length; i++)
            {

                if (i == 0)
                {
                    listSngCount.Add(0);
                }
                else
                {
                    if ((Math.Abs(YArray[i] - YArray[i - 1])) > 180)
                    {
                        listSngCount.Add(listSngCount[i - 1] + 1);
                    }
                    else
                    {
                        listSngCount.Add(listSngCount[i - 1]);
                    }
                }
                listsngArrayNeg.Add(YArray[i] - listSngCount[i] * unwrapDegree);
                listsngArrayPos.Add(YArray[i] + listSngCount[i] * unwrapDegree);
            }

            bool bFlagPos = false;
            bool bFlagNeg = false;
            int flagPos = 0;
            int flagNeg = 0;

            if ((listsngArrayNeg[1] - listsngArrayNeg[0]) >= 0)
            {
                bFlagPos = true;
            }
            else
            {
                bFlagNeg = true;
            }

            for (int i = 0; i < YArray.Length - 1; i++)
            {
                if (bFlagPos)
                {
                    if ((listsngArrayNeg[i + 1] - listsngArrayNeg[i]) < 0)
                    {
                        flagNeg = flagNeg + 1;
                    }
                    if ((listsngArrayPos[i + 1] - listsngArrayPos[i]) < 0)
                    {
                        flagPos = flagPos + 1;
                    }
                }
                if (bFlagNeg)
                {
                    if ((listsngArrayNeg[i + 1] - listsngArrayNeg[i]) > 0)
                    {
                        flagNeg = flagNeg + 1;
                    }
                    if ((listsngArrayPos[i + 1] - listsngArrayPos[i]) > 0)
                    {
                        flagPos = flagPos + 1;
                    }
                }
            }

            Trace unwrapTraceData = new Trace();
            try
            {
                if (flagNeg > flagPos)
                {
                    for (int i = 0; i < XArray.Length; i++)
                    {
                        double x = XArray[i];
                        double y = listsngArrayPos[i];
                        unwrapTraceData.Add(x, y);
                    }
                    bUnwrap = true;
                }
                if (flagPos > flagNeg)
                {
                    for (int i = 0; i < XArray.Length; i++)
                    {
                        double x = XArray[i];
                        double y = listsngArrayNeg[i];
                        unwrapTraceData.Add(x, y);
                    }
                    bUnwrap = true;
                }
            }
            catch (Exception)
            {
                engine.ErrorInModule("Unwrap phase data error!");
            }

            // remove phase glitch

            //double[] XArray = unwrapData.GetXArray();
            YArray = unwrapTraceData.GetYArray();

            Boolean phaseError = false;
            double offset = 0;
            double step = YArray[10] - YArray[11];

            for (int i = 0; i < YArray.Length - 1; i++)
            {
                step = YArray[i] - YArray[i + 1];
                if (Math.Abs((YArray[i] - YArray[i + 1])) > 200)
                {

                    phaseError = true;
                    offset = YArray[i] - YArray[i + 1] - 5;
                    for (int r = i + 1; r < YArray.Length; r++)
                    {
                        YArray[r] = YArray[r] + offset;
                    }

                }
                
            }
            Trace unwrapData1 = new Trace();
            for (int i = 0; i < XArray.Length; i++)
            {
                unwrapData1.Add(XArray[i], YArray[i]);
            }

            unwrapData = unwrapData1;
            return bUnwrap;
        }

        /// <summary>
        /// y=a+bx,
        /// Extract Sub data from startFreq to StopFreq,calculate "a" and "b"
        /// </summary>
        /// <param name="unwrapData">raw data ,include Xdata and Ydata</param>
        /// <param name="lineariseStartFreq_GHz"></param>
        /// <param name="lineariseStopFreq_GHz"></param>
        /// <param name="Param_a">a = average(Y)-b*average(X)</param>
        /// <param name="Param_b">b = (X-average(X))*(Y-average(Y))/(X-average(X))*(X-average(X))</param>
        public static void BestFitPhaseSlopeByForecast(Trace unwrapData, double lineariseStartFreq_GHz, double lineariseStopFreq_GHz, out double Param_a, out double Param_b)
        {
            double[] XArray = unwrapData.GetXArray();
            double[] YArray = unwrapData.GetYArray();

            double averageX = 0.0;
            double averageY = 0.0;
            double sumXY = 0.0;
            double sumX2 = 0.0;

            int lineariseStartPointIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(XArray, lineariseStartFreq_GHz);
            int lineariseStopPointIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(XArray, lineariseStopFreq_GHz);
            double[] lineariseXArray = Alg_ArrayFunctions.ExtractSubArray(XArray, lineariseStartPointIndex, lineariseStopPointIndex);
            double[] lineariseYArray = Alg_ArrayFunctions.ExtractSubArray(YArray, lineariseStartPointIndex, lineariseStopPointIndex);

            for (int i = 0; i < lineariseXArray.Length; i++)
            {
                averageX += lineariseXArray[i];
                averageY += lineariseYArray[i];
            }
            averageX /= lineariseXArray.Length;
            averageY /= lineariseYArray.Length;

            for (int i = 0; i < lineariseXArray.Length; i++)
            {
                sumXY += (lineariseXArray[i] - averageX) * (lineariseYArray[i] - averageY);
                sumX2 += (lineariseXArray[i] - averageX) * (lineariseXArray[i] - averageX);
            }

            if (sumX2 == 0)
            {
                throw new AlgorithmException("the sumX2 can't be zero.");
            }
            else
            {
                Param_b = sumXY / sumX2;
                Param_a = averageY - Param_b * averageX;
            }

        }

        /// <summary>
        /// calculate skew 
        /// </summary>
        /// <param name="engine">reference of ITestEngine</param>
        /// <param name="rawData1">raw data 1</param>
        /// <param name="rawData2">raw data 2</param>
        /// <param name="skewFreq_GHz">skew freq</param>
        /// <param name="unwrapDegree">unwrap degree</param>
        /// <returns></returns>
        public static double SkewTest_Calculate(ITestEngine engine, Trace rawData1, Trace rawData2, double skewFreq_GHz, double unwrapDegree)
        {
            Trace unwrapData1 = new Trace();
            Trace unwrapData2 = new Trace();
            double[] XArray1 = rawData1.GetXArray();
            double[] YArray1 = rawData1.GetYArray();
            double[] XArray2 = rawData2.GetXArray();
            double[] YArray2 = rawData2.GetYArray();

            int skewIndex1 = 0;
            int skewIndex2 = 0;
            double phase1 = 0.0;
            double phase2 = 0.0;
            double skewPhase = 0.0;


            UnwrappingPhaseData(engine, rawData1, out unwrapData1, unwrapDegree);
            UnwrappingPhaseData(engine, rawData2, out unwrapData2, unwrapDegree);

            skewIndex1 = Alg_ArrayFunctions.FindIndexOfNearestElement(XArray1, skewFreq_GHz);
            skewIndex2 = Alg_ArrayFunctions.FindIndexOfNearestElement(XArray2, skewFreq_GHz);

            phase1 = YArray1[skewIndex1];
            phase2 = YArray2[skewIndex2];

            if (phase2 > phase1)
            {
                phase2 -= 180;
            }
            else
            {
                phase2 += 180;
            }

            skewPhase = phase1 - phase2;
            skewPhase = (skewPhase) * (1000 / skewFreq_GHz) / 360;

            return skewPhase;

        }

        /// <summary>
        /// do mask for sprr 
        /// </summary>
        /// <param name="engine">reference of ITestEngine</param>
        /// <param name="sprrData">sprr trace data</param>
        /// <param name="sprrMaskFitFile">sprr mask fit file</param>
        /// <returns></returns>
        public static bool MaskAnalysis_Sprr(ITestEngine engine, Trace sprrData, string sprrMaskFitFile)
        {
            //int sweepPoints;
            //string errorString = "";
            List<Dictionary<string, double>> maskCfgData = GetMaskLimit(sprrMaskFitFile);

            double[] frequency;
            double[] sprrArray;

            bool sprrComplexMaskFlag = false;

            if (sprrData != null)
            {
                frequency = sprrData.GetXArray();
                sprrArray = sprrData.GetYArray();

                // Perform a complex mask fit using the macro pointed file 'Sprr Mask file'
                sprrComplexMaskFlag = complexMask(frequency, sprrArray, maskCfgData);
            }

            return sprrComplexMaskFlag;
        }




        #region private function

        /// <summary>
        /// complex mask
        /// </summary>
        /// <param name="xData"></param>
        /// <param name="yData"></param>
        /// <param name="maskCfgData"></param>
        /// <returns>if complexMask pass ,return true,else return false</returns>
        private static bool complexMask(double[] xData, double[] yData, List<Dictionary<string, double>> maskCfgData)
        {
            double[,] maskData = new double[3, 2];
            bool lineMaskFlag = false;

            for (int i = 0; i < maskCfgData.Count; i++)
            {
                // Do Mask
                double SlopeUpper;
                double SlopeLower;


                SlopeUpper = (maskCfgData[i]["StopYvalueUpper"] - maskCfgData[i]["StartYvalueUpper"]) /
                    (maskCfgData[i]["StopXvalueUpper"] - maskCfgData[i]["StartXvalueUpper"]);

                SlopeLower = (maskCfgData[i]["StopYvalueLower"] - maskCfgData[i]["StartYvalueLower"]) /
                    (maskCfgData[i]["StopXvalueLower"] - maskCfgData[i]["StartXvalueLower"]);

                //y=kx+b
                maskData[0, 1] = maskCfgData[i]["StartYvalueUpper"]; //b_upper
                maskData[0, 0] = maskCfgData[i]["StartYvalueLower"]; //b_lower
                maskData[1, 1] = SlopeUpper; //k_upper
                maskData[1, 0] = SlopeLower; //k_lower
                maskData[2, 0] = maskCfgData[i]["StartXvalueUpper"]; //x_start
                maskData[2, 1] = maskCfgData[i]["StopXvalueUpper"];  //x_stop

                if (maskData[2, 1] != 0)
                {
                    lineMaskFlag = lineMask(xData, yData, maskData);
                }
                if (!lineMaskFlag)
                {
                    return lineMaskFlag;
                }
            }
            return lineMaskFlag;
        }

        /// <summary>
        ///  mask adjust,if mask fail,return false,if mask pass,return true
        /// </summary>
        /// <param name="xData"></param>
        /// <param name="yData"></param>
        /// <param name="maskData"></param>
        /// <returns>if mask fail,return false,if mask pass,return true</returns>
        private static bool lineMask(double[] xData, double[] yData, double[,] maskData)
        {
            int lowerPtr;
            int upperPtr;
            int count;

            lowerPtr = findPlotDataSpot(xData, maskData[2, 0]);
            if (lowerPtr == -999)
            {
                throw new Exception("The value of the mask start x : " + maskData[2, 0].ToString() + " is outside of the data range.");
            }

            upperPtr = findPlotDataSpot(xData, maskData[2, 1]);
            if (upperPtr == -999)
            {
                throw new Exception("The value of the mask stop x : " + maskData[2, 1].ToString() + " is outside of the data range.");
            }

            for (count = lowerPtr; count <= upperPtr; count++)
            {
                if ((yData[count] > xData[count] * maskData[1, 1] + maskData[0, 1]) ||
                    (yData[count] < xData[count] * maskData[1, 0] + maskData[0, 0]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plotData"></param>
        /// <param name="searchData"></param>
        /// <returns></returns>
        private static int findPlotDataSpot(double[] plotData, double searchData)
        {
            double leastAbsDiff = 9999;
            int searchPoint = -999;

            for (int i = 0; i < plotData.Length; i++)
            {
                if (System.Math.Abs(plotData[i] - searchData) < leastAbsDiff)
                {
                    leastAbsDiff = System.Math.Abs(plotData[i] - searchData);
                    searchPoint = i;
                }
            }

            return searchPoint;
        }

        /// <summary>
        /// Find the right point fitting the element point
        /// </summary>
        /// <param name="plotArray">plot array</param>
        /// <param name="targetElement">element point</param>
        /// <param name="searchPoint">search condition</param>
        /// <returns>target point index</returns>
        private static int FindRightPointofElement(double[] plotArray, double targetElement, SearchCondition searchPoint)
        {
            int[] allFittingPoints = new int[plotArray.Length];
            int fittingPointIndex = 0;

            for (int i = 0; i < plotArray.Length - 1; i++)
            {
                if ((targetElement - plotArray[i]) * (targetElement - plotArray[i + 1]) <= 0)
                {
                    if (System.Math.Abs(targetElement - plotArray[i]) <= System.Math.Abs(targetElement - plotArray[i + 1]))
                    {
                        allFittingPoints[fittingPointIndex] = i;
                        fittingPointIndex++;
                    }
                    else
                    {
                        allFittingPoints[fittingPointIndex] = i + 1;
                        fittingPointIndex++;
                    }
                }
            }

            int[] tempArray = allFittingPoints;

            if (tempArray.Length == 0)
            {
                return -999;    // Don't find any points to fit the element point
            }
            else if (tempArray.Length == 1)
            {
                return tempArray[0];   // Only find one point! so send it back
            }

            int rtnValue = -999;

            switch (searchPoint)
            {
                case SearchCondition.First:
                    rtnValue = tempArray[0];
                    break;
                case SearchCondition.Last:
                    rtnValue = tempArray[tempArray.Length - 1];
                    break;
                case SearchCondition.FirstPreceding:
                    rtnValue = tempArray[0] - 1;
                    if (rtnValue < 0)
                    {
                        rtnValue = -999;
                    }
                    break;
                case SearchCondition.FirstFollowing:
                    rtnValue = tempArray[0] + 1;
                    break;
                case SearchCondition.LastPreceding:
                    rtnValue = tempArray[tempArray.Length - 1] - 1;
                    break;
                case SearchCondition.LastFollowing:
                    rtnValue = tempArray[plotArray.Length - 1] + 1;
                    if (rtnValue >= plotArray.Length)
                    {
                        rtnValue = -999;
                    }
                    break;
                default:
                    break;
            }

            return rtnValue;
        }

        /// <summary>
        /// Writes plot data to file ready for processing by the DataWrite component.
        /// </summary>
        /// <param name="plotData">List of comma seperated data in string format.</param>
        /// <returns>The name of the file created.</returns>
        public static string WritePlotData(ArrayList plotData)
        {
            string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
            string fileName = "";

            try
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                System.IO.StreamWriter writer = new System.IO.StreamWriter(System.IO.Path.Combine(dropArea, fileName));

                foreach (string line in plotData)
                {
                    writer.WriteLine(line);
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }

            // DatumFileLink fileLink = new DatumFileLink("SweepPlot", dropArea, fileName);
            fileName = dropArea + fileName;
            return fileName;
        }

        /// <summary>
        /// Writes plot data to file ready for processing by the DataWrite component.
        /// </summary>
        /// <param name="plotData">List of comma seperated data in string format.</param>
        /// <param name="serialNo"></param>
        /// <returns>The name of the file created.</returns>
        private static void WritePlotData(ArrayList plotData, string serialNo)
        {
            string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
            string fileName = "";

            try
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + serialNo.ToString() + ".csv";
                System.IO.StreamWriter writer = new System.IO.StreamWriter(System.IO.Path.Combine(dropArea, fileName));

                foreach (string line in plotData)
                {
                    writer.WriteLine(line);
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }

            DatumFileLink fileLink = new DatumFileLink("LockSweepPlot", dropArea, fileName);
            return;
        }

        /// <summary>
        /// Writes plot data to file ready for processing by the DataWrite component.
        /// </summary>
        /// <param name="plotData">List of comma seperated data in string format.</param>
        /// <param name="arm"></param>
        /// <param name="serialNo"></param>
        private static void WritePlotData(ArrayList plotData, string arm, string serialNo)
        {
            string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
            string fileName = "";

            try
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + arm + "_" + serialNo + ".csv";
                System.IO.StreamWriter writer = new System.IO.StreamWriter(System.IO.Path.Combine(dropArea, fileName));

                foreach (string line in plotData)
                {
                    writer.WriteLine(line);
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }

            DatumFileLink fileLink = new DatumFileLink("LockSweepPlot", dropArea, fileName);
            return;
        }

        /// <summary>
        /// Writes plot data to file ready for processing by the DataWrite component.
        /// </summary>
        /// <param name="plotData">List of comma seperated data in string format.</param>
        /// <param name="plotName">The name of the file created.</param>
        /// <param name="serialNo"></param>
        private static void WritePlotData(double[] plotData, string plotName, string serialNo)
        {
            string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
            string fileName = "";

            try
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "pcas\\plot");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                fileName = plotName + "_" + serialNo + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                System.IO.StreamWriter writer = new System.IO.StreamWriter(System.IO.Path.Combine(dropArea, fileName));

                foreach (double data in plotData)
                {
                    writer.WriteLine(data);
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }

            DatumFileLink fileLink = new DatumFileLink("LockSweepPlot", dropArea, fileName);
            return;
        }

        private static List<Dictionary<string, double>> GetMaskLimit(string maskLimitFile)
        {
            Dictionary<string, double> maskData = new Dictionary<string, double>();
            List<Dictionary<string, double>> listMaskData = new List<Dictionary<string, double>>();

            CsvReader cr = new CsvReader();
            List<string[]> list = cr.ReadFile(maskLimitFile);
            for (int i = 1; i < list.Count; i++)
            {
                maskData.Clear();
                for (int j = 0; j < list[0].Length; j++)
                {
                    maskData.Add(list[0][j], double.Parse(list[i][j]));
                }

                listMaskData.Add(maskData);

            }
            return listMaskData;
        }

        #endregion

    }

    #region Public Data Structures

    /// <summary>
    /// struct of S21 analysis configure
    /// </summary>
    public struct AnalysisConfig_S21
    {
        /// <summary>
        ///Sweep points
        /// </summary>
        public int sweepPoints;
        /// <summary>
        /// normalisation Frequency_GHz
        /// </summary>
        public double normalisationFrequency_GHz;
        /// <summary>
        /// 
        /// </summary>
        public double OscillationStartFreq_GHz;
        /// <summary>
        /// 
        /// </summary>
        public double OscillationStopFreq_GHz;
        /// <summary>
        /// 
        /// </summary>
        public double OscillationS21Limit;

        /// <summary>
        /// smoothing points of Average smooth s21 data
        /// </summary>
        public int smoothingPoints;
        /// <summary>
        /// 3dB bandwidth
        /// </summary>
        public double bandWidthPoint_dB;
        /// <summary>
        /// flag of if need to do mask
        /// </summary>
        public bool doMask;
        /// <summary>
        /// mask fit data file
        /// </summary>
        public string maskFitFile;

    }

    /// <summary>
    /// struct of S22 analysis configure
    /// </summary>
    public struct AnalysisConfig_S22
    {
        /// <summary>
        ///Sweep points
        /// </summary>
        public int sweepPoints;
        /// <summary>
        /// normalisation Frequency_GHz
        /// </summary>
        public double normalisationFrequency_GHz;
        /// <summary>
        /// smoothing points of smoothing s21 data
        /// </summary>
        public double smoothingPoints;
        /// <summary>
        /// s22 value in the special freq_GHz
        /// </summary>
        public double qualPointPower_dB;
        /// <summary>
        /// mask fit data file
        /// </summary>
        public string maskFitFile;
        /// <summary>
        /// boolen variable for whether do mask test
        /// </summary>
        public bool doMask;
    }

    /// <summary>
    /// sparameter analysis result
    /// </summary>
    public struct SparamAnalysisResult
    {
        /// <summary>
        /// pass/fail result of do mask
        /// </summary>
        public bool Sparam_MaskPassFail;

        /// <summary>
        /// bandwidth
        /// </summary>
        public double bandWidth_GHz;

        /// <summary>
        /// 
        /// </summary>
        public ArrayList PlotData;

    }

    /// <summary>
    /// Define the search condition of the plot finding
    /// </summary>
    public enum SearchCondition
    {
        /// <summary>
        /// Find the first point of the Target Element in the plotArray 
        /// </summary>
        First,

        /// <summary>
        /// Find the last point of the Target Element in the plotArray 
        /// </summary>
        Last,

        /// <summary>
        /// Find the point preceding the first point
        /// </summary>
        FirstPreceding,

        /// <summary>
        /// Find the point following the first point
        /// </summary>
        FirstFollowing,

        /// <summary>
        /// Find the point preceding the last point
        /// </summary>
        LastPreceding,

        /// <summary>
        /// Find the point following the last point
        /// </summary>
        LastFollowing
    }

    #endregion

}

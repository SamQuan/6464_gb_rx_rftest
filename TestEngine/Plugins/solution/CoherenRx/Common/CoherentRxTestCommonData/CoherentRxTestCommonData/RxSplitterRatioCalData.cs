using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data ;
using System.Data .OleDb ;
using System.IO;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;

namespace Bookham.TestSolution.CoherentRxTestCommonData
{
    /// <summary>
    /// Splitter Ratio calibration data
    /// </summary>
    public class RxSplitterRatioCalData:IDisposable 
    {
        /// <summary>
        /// const infinteRatio = 999
        /// </summary>
        public const double InfinteRatio = 999;
        private List<SplitterRatioCalDataUnit> splitterRatioList;
        private SplitterRatioCalDataUnit[] arrSplitterRatio;
        
        private double frequencyTolerance_Ghz;
        private readonly string splitterName;

        #region Database Define and operation

        /// <summary>
        /// 
        /// </summary>
        string dbConfigFilePath;
        /// <summary>
        /// 
        /// </summary>
        string dbConfigTableName;

        /// <summary>
        /// 
        /// </summary>
        public bool IsDBConfigOK;
        /// <summary>
        /// 
        /// </summary>
        public bool IsDBCalDataOk;
        SRCalDatabaseDef databaseDef;
        /// <summary>
        /// 
        /// </summary>
        public string DataSourceStr;
        /// <summary>
        /// 
        /// </summary>
        public string GetSRDataStr;  

        private OleDbConnection connDatabase;
        private OleDbDataAdapter adpGetSRData;        
        private DataSet dsCalDatabase;
        private OleDbCommandBuilder cbGetSRData;
        private OleDbCommand cmdGetSRData;
        private ConfigDataAccessor dbSettingConfig;

        /// <summary>
        /// Update this splitter's last cal time to present
        /// </summary>
        public void UpdateCalDate()
        {
            try
            {
                dbSettingConfig = new ConfigDataAccessor(dbConfigFilePath, dbConfigTableName);
            }
            catch (Exception ex)
            {
                throw new Exception( string .Format ("Can't update {0)'s last calibarate time"+
                    " to current time with exception of {1}", this .splitterName ,ex .Message ));
            }

            if (dbSettingConfig == null) return ;
            
            StringDictionary dataKey = new StringDictionary();
            dataKey.Add("SplitterName", this.splitterName);
            dataKey.Add("ParameaterName", "LastCalDateTime");
            DatumList dl = dbSettingConfig.GetData(dataKey, false);
            if (dl == null)
            {
                dl = new DatumList();
                dl.AddString("ParameaterName", "LastCalDateTime");
                dl.AddString("Value", DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss:fff"));
                dl.AddString("SplItterName", this.splitterName);
                dbSettingConfig.AddData(dl);
            }
            else
            {
                DatumList newDl = dl.CloneList();
                newDl.UpdateString("Value", DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss:fff"));
                dbSettingConfig.UpdateData(dl,newDl);
            }
            dbSettingConfig.SaveChangesToFile();
            dbSettingConfig = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configFilePath"></param>
        /// <param name="dataTableName"></param>
        /// <returns></returns>
        public bool GetDatabaseDef( string configFilePath, string dataTableName)
        {
            if (!File.Exists(configFilePath)) return false;
            //databaseDef = null;

            #region Read database definition from config file
            try
            {
                dbSettingConfig = new ConfigDataAccessor(configFilePath, dataTableName);
                if (dbSettingConfig == null) return false;

                databaseDef = new SRCalDatabaseDef();
                DatumList dataRow = null;

                StringDictionary dataKey = new StringDictionary();
                dataKey.Add("SplItterName", this.splitterName);

                dataKey.Add("ParameaterName", "DatabasePath");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    string databaseFile = Directory.GetCurrentDirectory() + "\\" + dataRow["Value"].ValueToString();
                    if (!File.Exists(databaseFile))
                    { 
                        throw new Exception("File doesn't exist @ " + dataRow["Value"] + 
                             this .splitterName +"'s splitter ratio calibration data is not available");
                       // return false ;
                    }
                    databaseDef.DatabasePath = databaseFile;
                }
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SplitterRatioCalTable");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.DataTableName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");
                
                dataKey.Add("ParameaterName", "SplitterNameField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.SplitterNameFieldName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRFrequencyField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.FrequencyFieldName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRWavelenField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.WavelenFieldName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRChanNumField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.ChanNumFieldName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRRatioField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.RatioFieldName = dataRow["Value"].ValueToString();
                }
                dataKey.Remove("ParameaterName");

                //dataKey.Add("ParameaterName", "SRLastModifyDateField");
                //dataRow = dbSettingConfig.GetData(dataKey, false);
                //if (dataRow != null)
                //{
                //    databaseDef..= dataRow["Value"];
                //}
                //dataKey.Remove("ParameaterName");

            }
            catch(Exception)
            { 
                return false;
            }

            finally
            {
                dbSettingConfig = null;
            }
            IsDBConfigOK = true;
            dbConfigTableName = dataTableName;
            dbConfigFilePath = configFilePath;

            return IsDBConfigOK;
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool GetSplitterRationCalData()
        {
            // if database define is not correct, return false
            if (!IsDBConfigOK) return false;

            IsDBCalDataOk = false;
            #region Database initialise
            DataSourceStr = @" Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = " + databaseDef.DatabasePath;
            //GetSRDataStr = "SELECT DISTINCT [" + GenericCodeField + "] FROM " + TableName;
            GetSRDataStr = "SELECT DISTINCT * FROM [" + databaseDef.DataTableName +
                "] WHERE [" + databaseDef.SplitterNameFieldName + "] = " +
                "\'" + this.splitterName + "\'";
            try
            {
                connDatabase = new OleDbConnection(DataSourceStr);

                cmdGetSRData = new OleDbCommand();
                cmdGetSRData.Connection = connDatabase;
                cmdGetSRData.CommandText = GetSRDataStr;

                adpGetSRData = new OleDbDataAdapter(cmdGetSRData);
                cbGetSRData = new OleDbCommandBuilder(adpGetSRData);
                cbGetSRData.QuotePrefix = "[";
                cbGetSRData.QuoteSuffix = "]";

                adpGetSRData.InsertCommand = cbGetSRData.GetInsertCommand();
                string cmdStr = string.Format("DELETE * FROM [{0}] WHERE [{1}] = '{2}'",
                    databaseDef.DataTableName, databaseDef.SplitterNameFieldName, splitterName);
                adpGetSRData.DeleteCommand = new OleDbCommand(cmdStr, connDatabase);
                string chanParamStr = "@" + databaseDef.ChanNumFieldName;
                string srParamStr = "@" + databaseDef.RatioFieldName;
                //string spNameParamStr = "@" + databaseDef.SplitterNameFieldName;
                string freqParamStr = "@" + databaseDef.FrequencyFieldName;
                string wavelenParamStr = "@" + databaseDef.WavelenFieldName;
                cmdStr = string.Format("UPDATE [{0}] \n" +
                    "SET [{1}] =  {2}, [{3}] = {4}\n" +
                    "WHERE {5} = {6} AND {7}={8}", databaseDef.DataTableName,
                    databaseDef.ChanNumFieldName, chanParamStr, databaseDef.RatioFieldName, srParamStr,
                    databaseDef.SplitterNameFieldName, splitterName, databaseDef.FrequencyFieldName, freqParamStr);

                adpGetSRData.UpdateCommand = new OleDbCommand(cmdStr);


            }
            catch (Exception ex)
            {
                IsDBConfigOK = false;
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            dsCalDatabase = new DataSet();


            #endregion

            #region Get splitter ratio cal data from database
            try
            {
                connDatabase.Open();
                dsCalDatabase.Tables.Add(databaseDef.DataTableName);
                adpGetSRData.Fill(dsCalDatabase, databaseDef.DataTableName);
                DataTable dbTable = dsCalDatabase.Tables[databaseDef.DataTableName];
                if (dbTable == null || dbTable.Rows.Count == 0 || dbTable.Columns.Count == 0)
                {
                    throw new Exception("RxSplitterRatioCalData.GetSplitterRationCalData: there is no record in the database with connection of " +
                        connDatabase.DataSource + " with sql " + cmdGetSRData.CommandText);
                    //return false;
                }
                if (splitterRatioList == null)
                    splitterRatioList = new List<SplitterRatioCalDataUnit>();
                else
                    splitterRatioList.Clear();
                foreach (DataRow row in dbTable.Rows)
                {
                    SplitterRatioCalDataUnit dataUnit = new SplitterRatioCalDataUnit();
                    dataUnit.ChanNum = int.Parse(row[databaseDef.ChanNumFieldName].ToString());
                    dataUnit.Frequency_Ghz = double.Parse(row[databaseDef.FrequencyFieldName].ToString());
                    dataUnit.Ratio = double.Parse(row[databaseDef.RatioFieldName].ToString());
                    dataUnit.Wavelen_nM = double.Parse(row[databaseDef.WavelenFieldName].ToString());
                    if (dataUnit.Frequency_Ghz == 0 && dataUnit.Wavelen_nM != 0)
                        dataUnit.Frequency_Ghz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(dataUnit.Wavelen_nM);
                    if (dataUnit.Wavelen_nM == 0 && dataUnit.Frequency_Ghz != 0)
                        dataUnit.Wavelen_nM = Math.Round(Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(dataUnit.Frequency_Ghz), 3);
                    splitterRatioList.Add(dataUnit);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            // sorted the cal data by frequency to ensure the data in array is sequencialy in frequency
            if (splitterRatioList != null)
            {
                splitterRatioList.Sort();
                arrSplitterRatio = splitterRatioList.ToArray();
            }
            IsDBCalDataOk = (arrSplitterRatio.Length > 0);
            #endregion

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool SaveSplitterRatioCalData()
        {
            bool bDataSaved = false;
            // if no cal data, return false
            if (splitterRatioList == null || splitterRatioList.Count == 0)
                return bDataSaved;
            // if no cal data achieved, re connect the database to rechieve cal data
            //if (dsCalDatabase == null || 
            //    !dsCalDatabase.Tables.Contains(databaseDef.DataTableName))
            //{
            //    if (!GetSplitterRationCalData()) return bDataSaved;
            //}

            if (dsCalDatabase == null) dsCalDatabase = new DataSet();

            try
            {
                connDatabase.Open();
                if (!dsCalDatabase.Tables.Contains(databaseDef.DataTableName))
                {
                    dsCalDatabase.Tables.Add(databaseDef.DataTableName);
                    adpGetSRData.Fill(dsCalDatabase, databaseDef.DataTableName);
                }
                // If there is cal data available, Clear all old record and update to database

                DataTable dbTable = dsCalDatabase.Tables[databaseDef.DataTableName];
                //if (dbTable != null && dbTable.Rows.Count != 0)
                //{
                    //dsCalDatabase.Tables[databaseDef.DataTableName].Rows.Clear();
                    //adpGetSRData.DeleteCommand.ExecuteNonQuery();
                    //DataSet changedDS=null;
                    //if (changedDS != null) adpGetSRData.Update(changedDS, databaseDef.DataTableName);
                    //OleDbCommand cmmd= adpGetSRData.DeleteCommand;
                    adpGetSRData.DeleteCommand.ExecuteNonQuery();
                    //adpGetSRData.Update(dsCalDatabase.Tables[databaseDef.DataTableName]);
                //}

                // Add new cal data to database
                foreach (SplitterRatioCalDataUnit data in splitterRatioList)
                {
                    DataRow newRow = dbTable.NewRow();
                    newRow[databaseDef.SplitterNameFieldName] = this.splitterName;
                    newRow[databaseDef.FrequencyFieldName] = data.Frequency_Ghz;
                    newRow[databaseDef.WavelenFieldName] = data.Wavelen_nM;
                    newRow[databaseDef.ChanNumFieldName] = data.ChanNum;
                    newRow[databaseDef.RatioFieldName] = data.Ratio;
                    dbTable.Rows.Add(newRow);
                }
                DataSet newDataDS = dsCalDatabase.GetChanges(DataRowState.Added);
                if (newDataDS != null) adpGetSRData.Update(newDataDS, databaseDef.DataTableName);
                
                // Modify last cal date  in cinfigureation file to current time
                UpdateCalDate();
                bDataSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            return bDataSaved;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="splitterName"></param>
        /// <param name="configFilePath"></param>
        /// <param name="dataTableName"></param>
        public  RxSplitterRatioCalData(string splitterName, string configFilePath, string dataTableName)
        {
            this.splitterName = splitterName;
            GetDatabaseDef(configFilePath, dataTableName);
            splitterRatioList = null; ;
            if (IsDBConfigOK ) GetSplitterRationCalData();
        }

        /// <summary>
        /// 
        /// </summary>
        public double FrequencyTolerance_Ghz
        {
            get { return frequencyTolerance_Ghz; }
            set { frequencyTolerance_Ghz = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string SplitterName
        {
            get { return splitterName; }
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freq_Ghz"></param>
        /// <returns></returns>
        public double GetSrByFrequency(double freq_Ghz)
        {
            if (arrSplitterRatio == null || arrSplitterRatio.Length < 1) return InfinteRatio;
            
            int dataIndx = -1;
            for (int indx = 0; indx < arrSplitterRatio.Length; indx++)
            {
                if (Math.Abs(arrSplitterRatio[indx].Frequency_Ghz - freq_Ghz) <= frequencyTolerance_Ghz)
                {
                    dataIndx = indx;
                    break;
                }
            }

            if (dataIndx == -1)
                return InfinteRatio;
            else
                return arrSplitterRatio[dataIndx].Ratio;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="wavelen_nM"></param>
        /// <returns></returns>
        public double GetSrByWavelen(double wavelen_nM)
        {
            double freq_Ghz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_nM);
            return GetSrByFrequency(freq_Ghz);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chan"></param>
        /// <returns></returns>
        public double GetSrByChan(int chan)
        {
            int dataIndx = -1;
            for (int indx = 0; indx < arrSplitterRatio.Length; indx++)
            {
                if (Math.Abs(arrSplitterRatio[indx].ChanNum) == chan)
                {
                    dataIndx = indx;
                    break;
                }
            }

            if (dataIndx == -1)
                return InfinteRatio;
            else
                return arrSplitterRatio[dataIndx].Ratio;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newCalDataPoint"></param>
        public void AddCalData(SplitterRatioCalDataUnit newCalDataPoint)
        {
            if (splitterRatioList == null) splitterRatioList = new List<SplitterRatioCalDataUnit>();
            if (!splitterRatioList.Contains(newCalDataPoint))
            { 
                splitterRatioList.Add(newCalDataPoint);
                splitterRatioList.Sort();
                arrSplitterRatio = splitterRatioList.ToArray();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearCalData()
        {
            splitterRatioList.Clear();
            arrSplitterRatio = null;
        }

        #region IDisposable Members

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            splitterRatioList=null;
            arrSplitterRatio = null;
            connDatabase.Dispose();
            adpGetSRData.Dispose() ; 
            dsCalDatabase.Dispose();
            cbGetSRData.Dispose();
            cmdGetSRData.Dispose();
        }

        #endregion
    }
    /// <summary>
    /// Splitter ratio @ frequency or wavelength
    /// </summary>
    public struct SplitterRatioCalDataUnit:IComparable 
    {
        /// <summary>
        /// 
        /// </summary>
        public double Frequency_Ghz;
        /// <summary>
        /// 
        /// </summary>
        public double Wavelen_nM;
        /// <summary>
        /// 
        /// </summary>
        public double Ratio;
        /// <summary>
        /// 
        /// </summary>
        public int ChanNum;
        
        #region IComparable Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            //SplitterRatioCalDataUnit data = obj as SplitterRatioCalDataUnit;
            SplitterRatioCalDataUnit data = (SplitterRatioCalDataUnit)obj;

            //if (data == null) throw new Exception(" Can't capare type of " + obj.GetType() +
            //       " with variant of RxSplitterRatioCalData data type");

            if (Frequency_Ghz > data.Frequency_Ghz)
                return 1;
            else if (Frequency_Ghz == data.Frequency_Ghz)
                return 0;
            else
                return -1;
        }

        #endregion

       // Predicate<SplitterRatioCalDataUnit> match;
        
    }

    /// <summary>
    /// Define splitter ratio cal data's database
    /// </summary>
    public struct SRCalDatabaseDef
    {   
        /// <summary>
        /// 
        /// </summary>
        public string DatabasePath;
        /// <summary>
        /// 
        /// </summary>
        public string DataTableName;
        /// <summary>
        /// 
        /// </summary>
        public string SplitterNameFieldName;
        /// <summary>
        /// 
        /// </summary>
        public string FrequencyFieldName;
        /// <summary>
        /// 
        /// </summary>
        public string WavelenFieldName;
        /// <summary>
        /// 
        /// </summary>
        public string ChanNumFieldName;
        /// <summary>
        /// 
        /// </summary>
        public string RatioFieldName;       
    }
}
                                                                                                                                                                                                                                                        
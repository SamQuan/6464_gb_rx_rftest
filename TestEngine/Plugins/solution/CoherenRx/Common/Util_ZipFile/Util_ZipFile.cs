using System;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{

    class Tester
    {
        static void Main()
        {
            string currdir = Environment.CurrentDirectory;
            //Util_UnZipFile.UnZip(@"..\bin.zip", @"..\unzip");
            string[] files = Directory.GetFiles(@"..\bin");

            using (Util_ZipFile zipfile=new Util_ZipFile(@"..\test.zip"))
            {
                
            }
        
            foreach (string var in files)
            {
                using (Util_ZipFile zipfile = new Util_ZipFile(@"..\test.zip"))
                {
                    zipfile.AddFileToZip(var);
                }
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Util_UnZipFile
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipFileName"></param>
        /// <param name="OutputPath"></param>
        public static void UnZip(string zipFileName, string OutputPath)
        {
            if (!Directory.Exists(OutputPath))
                throw new DirectoryNotFoundException();

            ZipFile zipFile;
            FileInfo fi = new FileInfo(zipFileName);
            if (fi.Exists && fi.Length > 0)
                zipFile = new ZipFile(zipFileName);
            else
                throw new FileNotFoundException();

            foreach (ZipEntry item in zipFile)
            {
                if (item.IsDirectory)
                {
                    string path = Path.GetFullPath(Path.Combine(OutputPath, item.Name));
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }
                else if (item.IsFile)
                {
                    string path = Path.Combine(OutputPath, item.Name);
                    string dir = Path.GetDirectoryName(path);
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    using (Stream s = zipFile.GetInputStream(item))
                    {
                        using (FileStream fs = new FileStream(path, FileMode.Create))
                        {
                            do
                            {
                                int b = s.ReadByte();
                                if (b == -1) break;
                                fs.WriteByte((byte)b);
                            } while (true);
                        }
                    }
                }
            }
            zipFile.Close();

        }

    }
    /// <summary>
    /// Class to create and add to a zip file.
    /// </summary>
    public class Util_ZipFile : IDisposable
    {
        private ZipFile zipFile;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="zipFileName">The name of the zip archive file.</param>
        public Util_ZipFile(string zipFileName)
        {
            FileInfo fi = new FileInfo(zipFileName);

            if (fi.Exists && fi.Length > 0)
                zipFile = new ZipFile(zipFileName);
            else
                zipFile = ZipFile.Create(zipFileName);
        }
        /// <summary>
        /// Adds a file to our zip file
        /// </summary>
        /// <param name="fileToAdd">file name to add (relative path)</param>
        public void AddFileToZip(string fileToAdd)
        {
            if (!File.Exists(fileToAdd)) return;
            zipFile.BeginUpdate();
            zipFile.Add(fileToAdd);
            zipFile.CommitUpdate();
        }

        /// <summary>
        /// dispose
        /// </summary>
        public void Dispose()
        {
            if (zipFile != null)
            {
                zipFile.Close();
                zipFile = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        ~Util_ZipFile()
        {
            Dispose();
        }
    }
}

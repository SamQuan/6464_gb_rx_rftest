﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace SPC.Rule
{
    public class FileAccess
    {
        public static List<string> ReadTxtByLine(string file,string pattern = "")
        {
            if (!File.Exists(file)) return null;

            List<string> content = new List<string>();
            using (StreamReader sr=new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    content.Add(sr.ReadLine());
                }
            }

            if (!string.IsNullOrEmpty(pattern))
            {
                Regex reg = new Regex(pattern);

                content.RemoveAll(s => !reg.IsMatch(s));
            }

            return content;
        }

        public static void WriteToFile(string file,string content, bool append = false)
        {
            using (StreamWriter sw=new StreamWriter (file,append))
            {
                sw.WriteLine(content);
            }
        }

        public static List<String> getParameters(string file)
        {
            if (!File.Exists(file)) return null;

            List<string> ret = new List<string>();
            string content = string.Empty;
            using (StreamReader sr = new StreamReader(file))
            {
                content = sr.ReadLine();
            }

            string[] ps = content.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 2; i < ps.Length; i++)
            {
                ret.Add(ps[i]);
            }


            return ret;
        }
    }
}

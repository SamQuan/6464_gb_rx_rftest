﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Config;
using BookhamNUnitTests;
using System.IO;
using SPC.Rule;

namespace Bookham.TestLibrary.Utilities
{
    public static class SpcConfigReader
    {
        private readonly static string Config = @"Configuration\SpcConfigParam.xml";
        private readonly static ConfigDataAccessor ConfigAccess = new ConfigDataAccessor(Config, "SPC");

        private static string Node;
        private static string TestStage;

        public static void Initialise(string node, string stage)
        {
            Node = node;
            TestStage = stage;
        }
        private static string Read(string param)
        {
            StringDictionary map = new StringDictionary();
            map.Add("ParamName", param);
            map.Add("Node", Node);
            map.Add("TestStage", TestStage);
            string retData = default(string);

            try
            {
                retData = ConfigAccess.GetData(map, true).ReadString("ParamValue");
            }
            catch
            {
                //throw new Exception(string.Format("No parameter name like {0} exists!", param));
                retData = string.Empty;
            }

            return retData;
        }

        public static string ReadString(string param)
        {
            return Read(param);
        }
        public static bool ReadBool(string param)
        {
            return bool.Parse(Read(param));
        }
        public static int ReadInt(string param)
        {
            return int.Parse(Read(param));
        }
        public static double ReadDouble(string param)
        {
            return double.Parse(Read(param));
        }

    }

    public enum ActionResult
    {
        Empty = 0,
        Continue = 1,
        Information = 2,
        Stop = 4,
        ForceStop = 8
    }
    public class SpcHelper
    {
        private static bool PcasSpcPass(ITestEngine engine, string node, string spcStage,out string time, string[] timeSpan = null, bool useWeekly = false)
        {
            IDataRead read = engine.GetDataReader();

            StringDictionary mapKeys = new StringDictionary();
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string PartCode = SpcConfigReader.ReadString("DeviceType");

            int week = SpcConfigReader.ReadInt("DayOfWeekly");

            if ((int)DateTime.Now.DayOfWeek == week)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
            else
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");

            if (useWeekly)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");

            mapKeys.Add("SCHEMA", PcasSchema);
            mapKeys.Add("SERIAL_NO", SerialNumber);
            mapKeys.Add("TEST_STAGE", spcStage);
            //mapKeys.Add("DEVICE_TYPE", PartCode);
            //string.Format("TIME_DATE >= {0}{1}",  DateTime.Now.AddDays(-1).ToString("yyyyMMdd"),"235960")
            //             "node=2128 and time_date>=20160131235960"
            DatumList[] lists = new DatumList[0];

            //DatumList list = null;

            if (timeSpan == null)
            {
                string timeDateT = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                lists = read.GetResults(mapKeys, false, string.Format("node={0} and TIME_DATE >= {1}{2}", node, timeDateT, "235960"));
            }
            else
                lists = read.GetResults(mapKeys, false, string.Format("node={0} and TIME_DATE BETWEEN {1} and {2}", node, timeSpan[0], timeSpan[1]));

            bool retValue = false;
            time = string.Empty;

            if (lists == null) retValue = false;
            else if (lists.Length == 0) retValue = false;
            //else if (!list.IsPresent("TEST_STATUS")) retValue = false;
            else
            {
                DatumList list = lists[lists.Length - 1];
               ////foreach (DatumList list in lists)
               // {
               //     if (list.ReadString("TEST_STATUS").ToLower().Contains("pass"))
               //     {
                        retValue = true;
                        time = list.ReadString("TIME_DATE");
               //         //break;
               //     }
               // }
            }

            return retValue;
        }

        private static bool IgnoreConfig()
        {
            string productLine = SpcConfigReader.ReadString("ProductLine");
            if (!string.IsNullOrEmpty(productLine) && productLine.Contains("Pass"))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Continue: No Time Limit or Current Time less than Start Time
        /// Information: Between Start and End
        /// Stop: Over End Time
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static ActionResult TimeSpcPass(string node)
        {
            //ActionResult result= ActionResult.Information;

            //if (!SpcConfigReader.ReadBool("UseTimeLimit"))
            //    result = ActionResult.Continue;
            ////add by xiaojiang for case operator modify config   20161115
            //#region
            //if (result == ActionResult.Continue && IgnoreConfig())
            //{
            //    return result;
            //}

            //#endregion

            DateTime now = DateTime.Now;

            string[] timeLimit = SpcConfigReader.ReadString("TimeLimitStart").Split(new char[] { ':' });
            if (now.Hour < int.Parse(timeLimit[0]) ||
                (now.Hour == int.Parse(timeLimit[0]) && now.Minute < int.Parse(timeLimit[1])))
                return ActionResult.Continue;

            timeLimit = SpcConfigReader.ReadString("TimeLimitEnd").Split(new char[] { ':' });
            if (now.Hour > int.Parse(timeLimit[0]) ||
                (now.Hour == int.Parse(timeLimit[0]) && now.Minute > int.Parse(timeLimit[1])))
                return ActionResult.Stop;

            return ActionResult.Information;
        }
        private static bool DailySpcPass(string node)
        {
            SpcResult spcResult = new SpcResult(SpcConfigReader.ReadString("DataBaseName"));

            string StageName = SpcConfigReader.ReadString("TestStage");
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string fileName = string.Empty;
            string dir = string.Empty;

            int week = SpcConfigReader.ReadInt("DayOfWeekly");

            if ((int)DateTime.Now.DayOfWeek == week)
            {
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
                fileName = SpcConfigReader.ReadString("WeeklyFileName");
                dir = SpcConfigReader.ReadString("WeeklyResultDirectory");
            }
            else
            {
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");
                fileName = SpcConfigReader.ReadString("DailyFileName");
                dir = SpcConfigReader.ReadString("ResultDirectory");
            }

            return spcResult.Check(SerialNumber, StageName, node, PcasSchema, fileName, dir);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static ActionResult MustDoSpcTest(ITestEngine engine, int nodeId, string testStage)
        {
            bool useWekklyNo = false;

            string node = nodeId.ToString();
            SpcConfigReader.Initialise(node, testStage);

            string spcStage = SpcConfigReader.ReadString("SpcStageName");

            //today, spc has been passed

            //ignore local temperary file since it is not necessary for normal situation----21061115
            //if (ReadFile(spcStage)) return ActionResult.Continue;

            ActionResult retValue = ActionResult.Empty;

            bool downOnFail = SpcConfigReader.ReadBool("DownIfFail");
            bool useSpcRule = SpcConfigReader.ReadBool("UseSpcRule");
            ActionResult timeAction = ActionResult.Empty;

            int timeDelay = SpcConfigReader.ReadInt("TimeDelay");
            int flag = -1;
            string[] timeSpan = null;
            string[] t1 = SpcConfigReader.ReadString("TimeLimi1").Split(new char[] { ':' });
            string[] t2 = SpcConfigReader.ReadString("TimeLimi2").Split(new char[] { ':' });
            string[] t3 = SpcConfigReader.ReadString("TimeLimi3").Split(new char[] { ':' });

            if (!SpcConfigReader.ReadBool("MutiGrade"))
                timeAction = TimeSpcPass(node);
            else
            {
                timeSpan = new string[2];
                DateTime now = DateTime.Now;
                if (now.Hour < int.Parse(t1[0]) || (now.Hour == int.Parse(t1[0]) && now.Minute < int.Parse(t1[1])))
                {
                    if (int.Parse(t3[0]) > int.Parse(t1[0]))
                    {
                        timeSpan[0] = now.AddDays(-1).ToString("yyyyMMdd") + t3[0] + t3[1] + "00";
                        int lastTime = int.Parse(t3[0]) + timeDelay;
                        timeSpan[1] = (lastTime >= 24 ? now.ToString("yyyyMMdd") : now.AddDays(-1).ToString("yyyyMMdd")) +
                            (lastTime >= 24 ? lastTime - 24 : lastTime).ToString("00") + t3[1] + "00";
                    }
                    else
                    {
                        int hour = int.Parse(t1[0]) - int.Parse(t3[0]) + 24;
                        timeSpan[0] = now.ToString("yyyyMMdd") + t3[0] + t3[1] + "00";
                        timeSpan[1] = now.ToString("yyyyMMdd") + (int.Parse(t3[0]) + timeDelay).ToString("00") + t3[1] + "00";
                    }
                    flag = 0;
                }
                else if (now.Hour < int.Parse(t2[0]) || (now.Hour == int.Parse(t2[0]) && now.Minute < int.Parse(t2[1])))
                {
                    //int hour = int.Parse(t2[0]) - int.Parse(t1[0]);
                    timeSpan[0] = now.ToString("yyyyMMdd") + t1[0] + t1[1] + "00";
                    timeSpan[1] = now.ToString("yyyyMMdd") + (int.Parse(t1[0]) + timeDelay).ToString("00") + t1[1] + "00";

                    flag = 1;
                }
                else
                {
                    timeSpan[0] = now.ToString("yyyyMMdd") + t2[0] + t2[1] + "00";
                    timeSpan[1] = now.ToString("yyyyMMdd") + (int.Parse(t2[0]) + timeDelay).ToString("00") + t2[1] + "00";
                    flag = 2;
                }
            }

            string timeDate = "";
            //20161115---xiaojiang
            downOnFail = downOnFail ? downOnFail : !IgnoreConfig();

            engine.SendStatusMsg(string.Format("TimePass: [{0}] ; DownOnFail: [{1}]", timeAction, downOnFail));

            if (timeAction == ActionResult.Continue)//use time limit
            {
                retValue = ActionResult.Continue;
            }
            else
            {
                engine.SendStatusMsg("SPC Check: Query daily spc...");
                bool pcasPass = PcasSpcPass(engine, node, spcStage, out timeDate, timeSpan);

                //add is daily spc device broken
                //if (!pcasPass && timeAction == ActionResult.Stop)
                //{
                //    useWekklyNo=true;
                //    pcasPass = PcasSpcPass(engine, node, spcStage,out timeDate,timeSpan, true);
                //}


                bool spcPass = true;

                if (!useSpcRule)
                {
                    if (pcasPass)
                        retValue = ActionResult.Continue;
                    else if (!downOnFail)
                        retValue = ActionResult.Information;
                    else
                        retValue = timeAction;
                }
                else
                {
                    spcPass = DailySpcPass(node);

                    if (pcasPass && spcPass)
                        retValue = ActionResult.Continue;
                    else if (!downOnFail)
                        retValue = ActionResult.Information;
                    else
                        retValue = timeAction;
                }

                //WriteFile(spcStage, spcPass && pcasPass);
            }

            engine.SendStatusMsg(retValue.ToString());

            if (retValue == ActionResult.Continue && downOnFail)
            {
                engine.SendStatusMsg("Begine Check SPC Rule Limit...");

                if (Debug_xiaojiang_stop(engine, node, useWekklyNo, timeDate))
                    retValue = ActionResult.ForceStop;

            }

            return retValue;
        }

        public static bool SPC_Post_Checks(ITestEngine engine, string nodeId, DatumList spcResult)
        {
            Identifier idy = new Identifier();

            idy.DeviceType = SpcConfigReader.ReadString("DeviceType");
            idy.NodeId = nodeId;

            StringDictionary mapKeys = new StringDictionary();
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string PartCode = SpcConfigReader.ReadString("DeviceType");
            idy.TestStage = SpcConfigReader.ReadString("SpcStageName");
            int week = SpcConfigReader.ReadInt("DayOfWeekly");


            string spc_folder = SpcConfigReader.ReadString("SPC_DEFINITION_PATH");
            if (string.IsNullOrEmpty(spc_folder))
                spc_folder = @"\\10.22.26.210\Test_Public\Production_Line\CFP2_RX_GB\SPC LIMIT DEFINATION\Test";

            //if (Directory.Exists(@"\\10.22.26.210\Test_Public\peroson\Destiny.Wang\TX\SPC LIMIT DEFINATION"))
            //    spc_folder = @"\\10.22.26.210\Test_Public\peroson\Destiny.Wang\TX\SPC LIMIT DEFINATION";

            //if (!spc_folder.EndsWith(nodeId))
            //    spc_folder += "\\" + nodeId;

            //string spc_define_file = Path.Combine(spc_folder, "SPC Limit Definition Daily.csv");
            string spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Daily.csv");

            if ((int)DateTime.Now.DayOfWeek == week)
            {
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
                spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Weekly.csv");
            }
            else
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");

            bool useWeekly = false;

            if (useWeekly)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");

            idy.SerialNumber = SerialNumber;
            //idy.TestStage = SpcConfigReader.ReadString("SpcStageName");
            idy.TimeForm = "";
            idy.TimeTo = "";

            // string[] parameters = new string[] { "" };

            if (!File.Exists(spc_define_file)) return false;

            string resultDir = "Results";
            if (!Directory.Exists(resultDir)) Directory.CreateDirectory(resultDir);

            string destFile = Path.Combine(resultDir, nodeId + idy.TestStage + " SPC Limit Definition Inuse.csv");
            try
            {
                if (!File.Exists(destFile))
                    File.Copy(spc_define_file, destFile);

                spc_define_file = destFile;
            }
            catch
            {
                engine.SendStatusMsg("Make a copy of spc limit defination file failed!");
            }

            bool data = false;
            try
            {
                PcasHelper ph = new PcasHelper(engine);
                string[] paramsTest = SPC.Rule.FileAccess.getParameters(spc_define_file).ToArray();
                List<ElementCollection> es = ph.ObtainData(new List<DatumList>(new DatumList[] { spcResult }), new List<string>(paramsTest));



                Limits limits = new Limits(spc_define_file);

                if (File.Exists(destFile))
                    File.Delete(destFile);

                ResourceCollection rc = new ResourceCollection(es, (List<LimitDefine>)limits.LimitsDef[int.Parse(nodeId)]);

                string timeDate = spcResult.ReadString("TIME_DATE");

                for (int i = 0; i < rc.Count; i++)
                {
                    data = data || rc[i].TestData[timeDate].OverRange;
                }

                if (rc.Count > 0 && data)
                {
                    string msgContent = "SPC Failed,请重测：\r\n\r\n";
                    for (int i = 0; i < rc.Count; i++)
                    {
                        Element element = rc[i].TestData[timeDate];
                        if (element.OverRange)
                        {
                            msgContent += element.Name + "=" + element.Value.ToString("0.###") + " (Limit: " + rc[i].LowerLimit.ToString("0.###") + "--" + rc[i].UpperLimit.ToString("0.###") + " )\r\n\r\n";
                        }
                    }
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, msgContent);
                }

                if (rc.Count > 0 && IfSendMail(data))
                {
                    try
                    {
                        engine.PageToFront(TestControlCtl.TestControl);
                        SendEmail(rc, timeDate, SerialNumber, nodeId, SpcConfigReader.ReadString("SpcStageName"));
                    }
                    catch (Exception ex)
                    {
                        engine.SendStatusMsg(ex.ToString());
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                engine.SendStatusMsg("Something is wrong with detect spc data...\n" + ex.ToString());
                return false;
            }

        }

        public static DatumList ReadSpcParams(string nodeId)
        {
            Identifier idy = new Identifier();

            idy.DeviceType = SpcConfigReader.ReadString("DeviceType");
            idy.NodeId = nodeId;

            StringDictionary mapKeys = new StringDictionary();
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string PartCode = SpcConfigReader.ReadString("DeviceType");
            idy.TestStage = SpcConfigReader.ReadString("SpcStageName");
            int week = SpcConfigReader.ReadInt("DayOfWeekly");


            string spc_folder = SpcConfigReader.ReadString("SPC_DEFINITION_PATH");
            if (string.IsNullOrEmpty(spc_folder))
                return null;

            string spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Daily.csv");

            if ((int)DateTime.Now.DayOfWeek == week)
            {
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
                spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Weekly.csv");
            }
            else
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");

            bool useWeekly = false;

            if (useWeekly)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");

            idy.SerialNumber = SerialNumber;
            //idy.TestStage = SpcConfigReader.ReadString("SpcStageName");
            idy.TimeForm = "";
            idy.TimeTo = "";

            // string[] parameters = new string[] { "" };

            if (!File.Exists(spc_define_file)) return null;

            string resultDir = "Results";
            if (!Directory.Exists(resultDir)) Directory.CreateDirectory(resultDir);

            string destFile = Path.Combine(resultDir, nodeId + idy.TestStage + " SPC Limit Definition Inuse.csv");
            try
            {
                if (!File.Exists(destFile))
                    File.Copy(spc_define_file, destFile);

                spc_define_file = destFile;
            }
            catch
            {
               // engine.SendStatusMsg("Make a copy of spc limit defination file failed!");
            }

            bool data = false;
            try
            {
                Limits limits = new Limits(spc_define_file);
                DatumList listSPCParams = new DatumList();
                foreach (var item in ((List<LimitDefine>)limits.LimitsDef[int.Parse(nodeId)]))
                {
                    if (item.Node == nodeId)
                    {
                        listSPCParams.AddDouble(item.Name, item.Mean);
                    }
                }

                if (File.Exists(destFile))
                    File.Delete(destFile);

                return listSPCParams;
            }
            catch (Exception ex)
            {
                //engine.SendStatusMsg("Something is wrong with detect spc data...\n" + ex.ToString());
                return null;
            }

        }


        #region xiaojiang spc rule

        private static bool Debug_xiaojiang_stop(ITestEngine engine, string nodeId, bool useWeekly, string timeDate)
        {
            Identifier idy = new Identifier();

            idy.DeviceType = SpcConfigReader.ReadString("DeviceType");
            idy.NodeId = nodeId;

            StringDictionary mapKeys = new StringDictionary();
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string PartCode = SpcConfigReader.ReadString("DeviceType");
            idy.TestStage = SpcConfigReader.ReadString("SpcStageName");
            int week = SpcConfigReader.ReadInt("DayOfWeekly");


            string spc_folder = SpcConfigReader.ReadString("SPC_DEFINITION_PATH");
            if (string.IsNullOrEmpty(spc_folder))
            {
                spc_folder = @"\\10.22.3.108\departments\SZ_Public\Share\Sam.Quan\RX\SPC LIMIT DEFINATION";

                if (Directory.Exists(@"\\10.22.26.210\Test_Public\Production_Line\CFP2_RX_GB\SPC LIMIT DEFINATION"))
                    spc_folder = @"\\10.22.26.210\Test_Public\Production_Line\CFP2_RX_GB\SPC LIMIT DEFINATION";
            }
            //if (!spc_folder.EndsWith(nodeId))
            //    spc_folder += "\\" + nodeId;

            string spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Daily.csv");


            if ((int)DateTime.Now.DayOfWeek == week)
            {
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
                spc_define_file = Path.Combine(spc_folder, idy.TestStage + "_SPC Limit Definition Weekly.csv");
            }
            else
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");

            if (useWeekly)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");

            idy.SerialNumber = SerialNumber;
            idy.TimeForm = "";
            idy.TimeTo = "";

            // string[] parameters = new string[] { "" };

            if (!File.Exists(spc_define_file)) return false;

            string resultDir = "Results";
            if (!Directory.Exists(resultDir)) Directory.CreateDirectory(resultDir);

            string destFile = Path.Combine(resultDir, nodeId + idy.TestStage + " SPC Limit Definition Inuse.csv");
            try
            {
                if(!File.Exists(destFile))
                    File.Copy(spc_define_file, destFile);

                spc_define_file = destFile;
            }
            catch
            {
                engine.SendStatusMsg("Make a copy of spc limit defination file failed!");
            }

            bool data = false;
            try
            {
                PcasHelper ph = new PcasHelper(engine);
                string[] paramsTest = SPC.Rule.FileAccess.getParameters(spc_define_file).ToArray();
                List<ElementCollection> es = ph.ObtainData(idy, paramsTest);

                Limits limits = new Limits(spc_define_file);

                //if (File.Exists(destFile))
                //    File.Delete(destFile);

                ResourceCollection rc = new ResourceCollection(es, (List<LimitDefine>)limits.LimitsDef[int.Parse(nodeId)]);


                for (int i = 0; i < rc.Count; i++)
                {
                    data = data || rc[i].TestData[timeDate].OverRange;
                }

                System.Threading.Thread.Sleep(200);
                if (rc.Count > 0 && data)
                {
                    string msgContent = "SPC Failed,请重测：\r\n\r\n";
                    for (int i = 0; i < rc.Count; i++)
                    {
                        Element element = rc[i].TestData[timeDate];
                        if (element.OverRange)
                        {
                            msgContent += element.Name + "=" + element.Value.ToString("0.###") + " (Limit: " + rc[i].LowerLimit.ToString("0.###") + "--" + rc[i].UpperLimit.ToString("0.###") + " )\r\n\r\n";
                        }
                    }
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, msgContent);
                }

                if (rc.Count > 0 && IfSendMail(data))
                {
                    try
                    {
                        SendEmail(rc, timeDate, SerialNumber, nodeId, SpcConfigReader.ReadString("SpcStageName"));
                    }
                    catch (Exception ex)
                    {
                        engine.SendStatusMsg(ex.ToString());
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                engine.SendStatusMsg("Something is wrong with detect spc data...\n" + ex.ToString());
                return false;
            }

        }


        private static void SendEmail(ResourceCollection rc,string timeDate,string serialNo,string node,string stage)
        {
            string iniFile = "Configuration\\spc_email_config.ini";
            FileInfo info = new FileInfo(iniFile);
            iniFile = info.FullName;

            string host = INI.GetStringValue(iniFile, "Common", "Host", "nasmtp.int.oclaro.com");
            string from = INI.GetStringValue(iniFile, "Common", "From", "sam.quan@oclaro.com");
            string to = INI.GetStringValue(iniFile, "Common", "To", "Ted.Jiang@oclaro.com");
            string password = INI.GetStringValue(iniFile, "Common", "Password", "Oclaro123");
            SymmetricEncryption se = new SymmetricEncryption();
            password = se.Decrypto(password);


            string EmailContent = string.Empty;

            EmailContent = "Dears,<br>";
            EmailContent = EmailContent + "&nbsp Please see the bellow Failed parameter result about :<br>";
            EmailContent = EmailContent + " &nbsp &nbsp SerialNo: "+serialNo+"<br>";
            EmailContent = EmailContent + " &nbsp &nbsp TestStage: "+stage+"<br>";
            EmailContent = EmailContent + " &nbsp &nbsp Node: "+node+"<br>";
            EmailContent = EmailContent + " &nbsp &nbsp Date: " + timeDate + "<br>";

            EmailContent += Mail. HeaderString();

            for (int i = 0; i < rc.Count; i++)
            {
                Element element = rc[i].TestData[timeDate];
                EmailContent += Mail.ValueString(element.Name, element.Value, rc[i].LowerLimit, rc[i].Mean, rc[i].UpperLimit, element.OverRange ? "Out Of Range" : "");
            }

            Mail.SendEmail(host, from, to, "SPC FAIL ALERT - Node" + node, EmailContent, password);
        }

        private static bool IfSendMail(bool thisStatus)//if thisStatus = true, that means over range, then send email always
        {
            bool bSend = false;

            if (thisStatus)
            {
                using (StreamWriter sw = new StreamWriter("Configuration\\spc_email_status"))
                {
                    sw.WriteLine(0);
                }

                return true;
            }
            else
            {
                if (!File.Exists("Configuration\\spc_email_status")) return false;
            }

            //if there is a value that equals 0(which indicates fail the last test status), then resend mail
            //else not send and reset it to 1

            using (StreamReader sr = new StreamReader("Configuration\\spc_email_status"))
            {
                if (sr.ReadLine().Trim() == "0") bSend = true;
                else bSend = false;
            }


            using (StreamWriter sw = new StreamWriter("Configuration\\spc_email_status"))
            {
                sw.WriteLine(1);
            }
            

            return bSend;
        }

        #endregion

        #region xiaojiang

        private static bool ReadFile(string spcStage)
        {
            string fileName = "Configuration\\spc";

            if (!File.Exists(fileName)) return false;

            FileInfo info = new FileInfo(fileName);
            if (info.LastWriteTime.Day != DateTime.Today.Day) return false;

            string content = bool.FalseString;
            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] line = sr.ReadLine().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                        if (line[0] == spcStage.ToLower())
                        {
                            content = line[1];
                            break;
                        }
                    }
                }

            }
            catch
            { }

            bool pass;
            if (bool.TryParse(content, out pass) && pass)
            {
                return true;
            }

            return false;
        }

        private static void WriteFile(string spcStage, bool pass)
        {
            string fileName = "Configuration\\spc";

            if (!File.Exists(fileName))
                File.Create(fileName).Dispose();

            StringDictionary previousContent = new StringDictionary();

            using (StreamReader sr = new StreamReader(fileName))
            {
                string[] content = sr.ReadToEnd().Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                if (content != null)
                {
                    foreach (string item in content)
                    {
                        string[] line = item.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        previousContent.Add(line[0], line[1]);
                    }
                }
            }

            previousContent[spcStage] = pass.ToString();

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                foreach (string item in previousContent.Keys)
                {
                    sw.WriteLine(string.Format("{0}:{1}", item, previousContent[item]));
                }
            }
        }
        #endregion
    }
}

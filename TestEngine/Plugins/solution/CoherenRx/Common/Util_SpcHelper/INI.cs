﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Bookham.TestLibrary.Utilities
{
    public static class Win32API
    {
        [DllImport("kernel32", CharSet = CharSet.Auto)]
        public static extern uint GetPrivateProfileSectionNames(IntPtr lpszReturnBuffer, uint nSize, string lpFileName);
        [DllImport("kernel32", CharSet = CharSet.Auto)]
        public static extern uint GetPrivateProfileSection(string lpAppName, IntPtr lpReturnedString, uint nSize, string lpFileName);
        [DllImport("kernel32", CharSet = CharSet.Auto)]
        public static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault,
                                                                            [In, Out] char[] lpReturnedString, uint nSize, string lpFileName);
        [DllImport("kernel32", CharSet = CharSet.Auto)]
        public static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault,
                                                                            StringBuilder lpReturnedString, uint nSize, string lpFileName);
        [DllImport("kernel32", CharSet = CharSet.Auto)]
        public static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault,
                                                                            string lpReturnedString, uint nSize, string lpFileName);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool WritePrivateProfileSection(string lpAppName, string lpString, string lpFileName);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
    }
    /// <summary>
    /// Ini file operation
    /// </summary>
    public sealed class INI
    {
        #region Package

        /// <summary>
        /// Get all the section name of a ini file
        /// </summary>
        /// <param name="iniFile"></param>
        /// <returns></returns>
        public static string[] GetAllSectionNames(string iniFile)
        {
            uint MAX_BUFFER = 32767;//default value
            string[] sections = new string[0];

            IntPtr pReturnedString = Marshal.AllocCoTaskMem((int)MAX_BUFFER * sizeof(char));

            uint bytesReturned = Win32API.GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, iniFile);

            if (bytesReturned != 0)
            {
                string local = Marshal.PtrToStringAuto(pReturnedString, (int)bytesReturned);

                sections = local.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
            }

            Marshal.FreeCoTaskMem(pReturnedString);

            return sections;
        }

        /// <summary>
        /// Get all the items(key=value) of a specified section
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static string[] GetAllItems(string iniFile, string section)
        {
            uint MAX_BUFFER = 32767;//default value
            string[] items = new string[0];

            IntPtr pReturnedString = Marshal.AllocCoTaskMem((int)MAX_BUFFER * sizeof(char));

            uint bytesReturned = Win32API.GetPrivateProfileSection(section, pReturnedString, MAX_BUFFER, iniFile);
            if (bytesReturned != MAX_BUFFER - 2 || bytesReturned == 0)
            {
                string returnedString = Marshal.PtrToStringAuto(pReturnedString, (int)bytesReturned);

                items = returnedString.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
            }
            Marshal.FreeCoTaskMem(pReturnedString);

            return items;
        }

        /// <summary>
        /// Get all the key list under a specified section
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static string[] GetAllItemKeys(string iniFile, string section)
        {
            string[] value = new string[0];
            const int SIZE = 1024 * 10;

            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");

            char[] chars = new char[SIZE];

            uint bytesReturned = Win32API.GetPrivateProfileString(section, null, null, chars, SIZE, iniFile);

            if (bytesReturned != 0)
            {
                value = new string(chars).Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
            }
            chars = null;

            return value;
        }

        /// <summary>
        /// Get the value of a specified key
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section">section name</param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetStringValue(string iniFile, string section, string key, string defaultValue)
        {
            string value = defaultValue;

            const int SIZE = 1024 * 10;
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Must specified a key!", "key");

            StringBuilder sb = new StringBuilder(SIZE);

            uint bytesReturned = Win32API.GetPrivateProfileString(section, key, defaultValue, sb, SIZE, iniFile);

            if (bytesReturned != 0)
                value = sb.ToString();

            sb = null;

            return value;
        }

        /// <summary>
        /// In ini file, write the pair of key and value under the specified section, if exists then replace it
        /// </summary>
        /// <param name="iniFile">ini file</param>
        /// <param name="section">section name, if not exist then create it</param>
        /// <param name="items">pair of key and value, Split them with \0 if more exist, like key1=value1\0key2=value2</param>
        /// <returns></returns>
        public static bool WriteItems(string iniFile, string section, string items)
        {
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");
            if (string.IsNullOrEmpty(items))
                throw new ArgumentException("Must specified a pair of key and value at least!", "items");

            return Win32API.WritePrivateProfileSection(section, items, iniFile);
        }

        /// <summary>
        /// In ini file, write the pair of key and value under the specified section, if exists then replace it; if not exist then create it.
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool WriteValue(string iniFile, string section, string key, string value)
        {
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Must specified a key!", "key");
            if (value == null)
                throw new ArgumentException("Value could not be null!", "value");

            return Win32API.WritePrivateProfileString(section, key, value, iniFile);
        }

        /// <summary>
        /// In ini fiel delete the key of a specified section
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool DeleteKey(string iniFile, string section, string key)
        {
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Must specified a key!", "key");

            return Win32API.WritePrivateProfileString(section, key, null, iniFile);
        }

        /// <summary>
        /// In ini file, delete the specified section
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static bool DeleteSection(string iniFile, string section)
        {
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");

            return Win32API.WritePrivateProfileString(section, null, null, iniFile);
        }

        /// <summary>
        /// In ini file, delete all the content under the specified section
        /// </summary>
        /// <param name="iniFile"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static bool EmptySection(string iniFile, string section)
        {
            if (string.IsNullOrEmpty(section))
                throw new ArgumentException("Must specified a section!", "section");

            return Win32API.WritePrivateProfileSection(section, string.Empty, iniFile);
        }
        #endregion Package
    }
}

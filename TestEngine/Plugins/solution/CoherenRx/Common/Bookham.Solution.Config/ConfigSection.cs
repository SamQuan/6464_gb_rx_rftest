using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections.Specialized;
using System.Data;

namespace Bookham.Solution.Config
{
    internal abstract class ConfigSection
    {
        protected XmlNode sectionNode;

        public ConfigSection(XmlNode sectionNode)
        {
            this.sectionNode = sectionNode;
        }

        internal abstract object GetSection(string sectionName);
    }

    class KeyValueConfigSection : ConfigSection
    {
        internal KeyValueConfigSection(XmlNode sectionNode)
            : base(sectionNode)
        {
        }

        internal override object GetSection(string sectionName)
        {
            NameValueCollection c = new NameValueCollection();
            foreach (XmlNode var in sectionNode.ChildNodes)
            {
                if (var.NodeType != XmlNodeType.Element) continue;
                string key = var.Attributes["key"].Value;
                string value = var.Attributes["value"].Value;
                c.Add(key, value);
            }
            return c.Count > 0 ? c : null;
        }

    }

    class TableConfigSection : ConfigSection
    {
        internal TableConfigSection(XmlNode sectionNode)
            : base(sectionNode)
        {
        }

        internal override object GetSection(string sectionName)
        {
            if (sectionNode.FirstChild == null || sectionNode.FirstChild.ChildNodes.Count == 0)
                return null;
            DataTable dt = new DataTable(sectionNode.Name);
            foreach (XmlNode var in sectionNode.FirstChild.ChildNodes)
            {
                if (var.NodeType != XmlNodeType.Element) continue;
                DataColumn col = new DataColumn(var.Name, typeof(string));
                dt.Columns.Add(col);
            }

            foreach (XmlNode Child in sectionNode.ChildNodes)
            {
                if (Child.NodeType != XmlNodeType.Element) continue;
                DataRow row = dt.NewRow();
                foreach (XmlNode var in Child.ChildNodes)
                {
                    row[var.Name] = var.InnerText.Trim();
                }
                dt.Rows.Add(row);
            }
            return dt.Rows.Count > 0 ? dt : null;
        }

    }


    interface IConfigSectionFactory
    {
        ConfigSection CreateConfigSection(XmlNode sectionNode);
    }
    class keyvalueConfigSectionFactory : IConfigSectionFactory
    {

        #region IConfigSectionFactory Members

        public ConfigSection CreateConfigSection(XmlNode sectionNode)
        {
            return new KeyValueConfigSection(sectionNode);
        }

        #endregion
    }
    class tableConfigSectionFactory : IConfigSectionFactory
    {
        #region IConfigSectionFactory Members

        public ConfigSection CreateConfigSection(XmlNode sectionNode)
        {
            return new TableConfigSection(sectionNode);            
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections.Specialized;
using System.Data;


namespace Bookham.Solution.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigurationManager
    {
        string fileName;
        /// <summary>
        /// 
        /// </summary>
        protected XmlDocument doc;
        /// <summary>
        /// 
        /// </summary>
        protected XmlElement configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public ConfigurationManager(string fileName)
        {
            //load xml
            doc = new XmlDocument();
            doc.Load(fileName);
            this.fileName = fileName;
            configuration = doc.DocumentElement;
        }

        /// <summary>
        /// Retrieves a specified configuration section
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public object GetSection(string sectionName)
        {
            XmlNode sectionNode = configuration.SelectSingleNode(sectionName);
            if (sectionNode == null) throw new Exception("Node '" + sectionName + "' not found");
            XmlAttribute attr = sectionNode.Attributes["type"];
            if (attr == null) throw new Exception("Attribute 'type' not found.");
            string sectionType = attr.Value.Trim().ToLower();

            //Create instance
            IConfigSectionFactory factory = (IConfigSectionFactory)Activator.CreateInstance(null,
                "Bookham.Solution.Config." + sectionType + "ConfigSectionFactory").Unwrap();
            return factory.CreateConfigSection(sectionNode).GetSection(sectionName);

        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get
            {
                return fileName;
            }
        }
    }
}

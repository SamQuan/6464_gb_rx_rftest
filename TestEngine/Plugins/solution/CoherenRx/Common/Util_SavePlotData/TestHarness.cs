// [Copyright]
//
// Bookham [Full Project Name]
//
// Author: mark.fullalove, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using NUnit.Framework;

namespace Util_SummaryDataTest
{
    /// <summary>
    /// Quick test harness
    /// </summary>
    [TestFixture]
    class TestHarness
    {
        private TestHarness()
        {
        }

        [Test]
        public void Test()
        {
            string fileName = "C:\\Documents and Settings\\Sam.quan\\Desktop\\test\\UnwrapDataFunctions\\ConsoleApplication17\\bin\\Debug\\Result\\xyData.csv";

            CsvReader csvReader = new CsvReader();
            List<string[]> listXYData = csvReader.ReadFile(fileName);

            double[] freqArray = new double[listXYData.Count - 1];
            double[] realArray = new double[listXYData.Count - 1];
            double[] imaginaryArray = new double[listXYData.Count - 1];
            double[] magintudeArray = new double[listXYData.Count - 1];
            List<string> listContent = new List<string>();
            for (int i = 1; i < listXYData.Count; i++)
            {
                freqArray[i - 1] = double.Parse(listXYData[i][0]);
                realArray[i - 1] = double.Parse(listXYData[i][1]);
                imaginaryArray[i - 1] = double.Parse(listXYData[i][2]);
                double temp = Math.Sqrt(Math.Pow(realArray[i - 1], 2) + Math.Pow(imaginaryArray[i - 1], 2));
                double magintude_dB = 20 * Math.Log10(temp);
                magintudeArray[i - 1] = magintude_dB;
            }

            string head = "magintude";


            Util_SavePlotData util_SavePlotData = new Util_SavePlotData();
            Util_SavePlotData.SetupPath("Result");

            util_SavePlotData.AppendResultToFile("xyData.csv", head, magintudeArray);

        }
    }
}

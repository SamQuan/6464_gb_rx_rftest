// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// Util_SavePlotData.cs
//
// Author: Rob.Taylor-Collins, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using System.IO;


namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class Util_SavePlotData
    {
        /// <summary>
        /// Append String context to file, even if the column length is different
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="header">Header to Add</param>
        /// <param name="contextArray">Context to add</param>
        public void AppendResultToFile(string fileName, string header, string[] contextArray)
        {
            // Get the File Path
            //string dir = AppDomain.CurrentDomain.BaseDirectory;
            //dir = Path.Combine(dir, FilePath);
            //string fullFileName = Path.Combine(dir, fileName);
            string fullFileName = fileName;
            FileStream fs = null;
            StreamReader sr = null;
            StreamWriter sw = null;

            try
            {
                // Create File stream
                fs = new FileStream(fullFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                sr = new StreamReader(fs);
                sw = new StreamWriter(fs);

                // Access the context which exist in file and read out
                string exitContext = sr.ReadToEnd();
                Dictionary<string, string[]> originalContext = new Dictionary<string, string[]>();

                if (!string.IsNullOrEmpty(exitContext))
                {
                    string[] originalContextArray = exitContext.Trim().Replace("\r", "").Split('\n');
                    string[] originalHeaderArray = originalContextArray[0].Trim().Split(',');

                    for (int i = 0; i < originalHeaderArray.Length; i++)
                    {
                        List<string> result = new List<string>();
                        for (int j = 1; j < originalContextArray.Length; j++)
                        {
                            result.Add(originalContextArray[j].Split(',')[i]);
                        }
                        string originalHeader = originalHeaderArray[i];
                        if (originalContext.ContainsKey(originalHeader))
                        {
                            originalHeader += " ";
                        }
                        originalContext.Add(originalHeader, result.ToArray());
                    }

                }

                // Add new context to collection
                if (originalContext.ContainsKey(header))
                {
                    header = header + " ";
                }
                originalContext.Add(header, contextArray);

                // get the max length for each column
                int totalCount = 0;
                foreach (string key in originalContext.Keys)
                {
                    if (originalContext[key].Length > totalCount)
                    {
                        totalCount = originalContext[key].Length;
                    }
                }

                // Clear file
                fs.SetLength(0);

                // Write the Header to file
                int k = 0;
                foreach (string head in originalContext.Keys)
                {
                    sw.Write(head);
                    if (k == originalContext.Keys.Count - 1)
                    {
                        sw.WriteLine();
                    }
                    else
                    {
                        sw.Write(",");
                    }

                    k++;
                }


                // Write the Context to file
                for (int Idata = 0; Idata < totalCount; Idata++)
                {
                    int i = 0;
                    foreach (string key in originalContext.Keys)
                    {
                        try
                        {
                            sw.Write(originalContext[key][Idata]);
                        }
                        catch (Exception ex)
                        {
                            string error = ex.Message.ToString();
                        }
                        if (i == originalContext.Keys.Count - 1)
                        {
                            sw.WriteLine();
                        }
                        else
                        {
                            sw.Write(",");
                        }
                        i++;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message.ToString();
            }
            finally
            {
                // Close File   
                if (sw != null)
                {
                    sw.Close();
                }
                if (sr != null)
                {
                    sr.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }
            

        }

        public  delegate void SendErrorMsgDelegate(object sender, string errorMsg);
        public  event SendErrorMsgDelegate SendErrorMsgEvent;

        private string errorMessage;

        public string ErrorMsg
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    if (SendErrorMsgEvent != null)
                    {
                        SendErrorMsgEvent(this, errorMessage);
                    }
                }
            }
        }
          
        /// <summary>
        /// Append double context to file, even if the column length is different
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="header">Header to Add</param>
        /// <param name="contextArray">Context to add</param>
        public void AppendResultToFile(string fileName, string header, double[] contextArray)
        {
            List<string> stringResult = new List<string>();
            foreach (double var in contextArray)
            {
                stringResult.Add(var.ToString());
            }
            AppendResultToFile(fileName, header, stringResult.ToArray());

        }

        /// <summary>
        /// Append int context to file, even if the column length is different
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="header">Header to Add</param>
        /// <param name="contextArray">Context to add</param>
        public void AppendResultToFile(string fileName, string header, int[] contextArray)
        {
            List<string> stringResult = new List<string>();
            foreach (int var in contextArray)
            {
                stringResult.Add(var.ToString());
            }
            AppendResultToFile(fileName, header, stringResult.ToArray());

        }

        /// <summary>
        /// Append ushort context to file, even if the column length is different
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="header">Header to Add</param>
        /// <param name="contextArray">Context to add</param>
        public void AppendResultToFile(string fileName, string header, ushort[] contextArray)
        {
            List<string> stringResult = new List<string>();
            foreach (ushort var in contextArray)
            {
                stringResult.Add(var.ToString());
            }
            AppendResultToFile(fileName, header, stringResult.ToArray());

        }

        public static void SetupPath(string filePath)
        {
            FilePath = filePath;
        }

        private static string FilePath;

        #region Private Data

        //string filePath;
        //string fileName;

        #endregion
    }
}

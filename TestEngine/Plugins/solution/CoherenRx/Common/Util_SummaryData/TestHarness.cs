// [Copyright]
//
// Bookham [Full Project Name]
//
// Author: mark.fullalove, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using NUnit.Framework;

namespace Util_SummaryDataTest
{
    /// <summary>
    /// Quick test harness
    /// </summary>
    [TestFixture]
    class TestHarness
    {
        private TestHarness()
        {
        }

        [Test]
        public void Test()
        {
            Util_SummaryData<int, string> intAndString = new Util_SummaryData<int, string>();
            intAndString.Add(1, "one");
            intAndString.Add(2, "two");
            List<string> summary = intAndString.ContentsToString(",");

            foreach (int i in intAndString.Keys)
            {
                string s = intAndString[i];                
            }

            Util_SummaryData<int, List<int>> plotData = new Util_SummaryData<int, List<int>>();
            List<int> plot1 = new List<int>();
            plot1.Add(1);
            plot1.Add(2);
            plot1.Add(3);
            plot1.Add(4);
            plotData.Add(1, plot1);

            List<int> plot2 = new List<int>();
            plot2.Add(10);
            plot2.Add(20);
            plot2.Add(30);
            plot2.Add(40);
            plotData.Add(2, plot2);
            foreach (int i in plotData.Keys)
            {
                List<int> l = plotData[i];
                foreach (int ii in l)
                {
                    int iii = ii;
                }
            }
        }
    }
}

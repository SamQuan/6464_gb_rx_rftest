// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// Util_SummaryData.cs
//
// Author: mark.fullalove, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Utility to store sets of data for a number of conditions
    /// </summary>
    /// <typeparam name="TKey">Key ( e.g. channel )</typeparam>
    /// <typeparam name="TValue">The data to be stored</typeparam>
    public class Util_SummaryData<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public Util_SummaryData()
            : base()
        {
        }

        /// <summary>
        /// Return the contents as a string value per entry by calling ToString() on keys and values
        /// </summary>
        /// <param name="seperator">Character to seperate the key and value</param>
        /// <returns>A list of strings</returns>
        public List<string> ContentsToString(string seperator)
        {           
            List<string> fileContents = new List<string>(base.Count);
            foreach (TKey key in base.Keys)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(key.ToString());
                sb.Append(seperator);
                sb.Append(base[key].ToString());
                fileContents.Add(sb.ToString());
            }
            return fileContents;
        }
        
        /// <summary>
        /// Adds the specified key and value to the dictionary.
        /// Existing entries wil be overwritten.
        /// 
        /// Exceptions:
        //   System.ArgumentNullException if key is null.        /// </summary>
        /// <param name="key">The key of the element to add.</param>
        /// <param name="value">The value of the element to add. The value can be null for reference types.</param>
        public void AddOrUpdate(TKey key, TValue value)
        {
            if (base.ContainsKey(key))
            {
                base.Remove(key);
            }
            base.Add(key, value);
        }
    }
}

<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.PluginInterfaces.Module</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo">
            <summary>
            Container for a single button for use with ITestEngine.ShowUserQuery.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo.ButtonText">
            <summary>
            Text to write on button.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo.ResponsePayload">
            <summary>
            Message to send in response to a click of this button.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo.#ctor(System.String,System.Object)">
            <summary>
            Construct information for button.
            </summary>
            <param name="buttonText">Text to write on button.</param>
            <param name="responsePayload">Message to send in response.</param>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.ButtonId">
            <summary>
            Enum indicating a user query response.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonId.Yes">
            <summary>User clicked a "Yes" button.</summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonId.No">
            <summary>User clicked a "No" button.</summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonId.Continue">
            <summary>User clicked a "Continue" button.</summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ButtonId.Cancel">
            <summary>User clicked a "Cancel" button.</summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine">
            <summary>
            Interface from Test Module to Test Engine resources 
            </summary>
            <remarks>Every call made on the interface will cause the Test Engine code
            to perform an implicit check for a UserAbort. To make an explicit check, 
            use the CheckForAbort method</remarks>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GuiShow">
            <summary>
            Show the GUI custom control attached to this test module (if specified)
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GuiHide">
            <summary>
            Hide the GUI custom control attached to this test module (if specified)
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GuiToFront">
            <summary>
            Bring the GUI to the front
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GuiUserAttention">
            <summary>
            Sets the User Attention status to true - highlighting the Test Module
            tab in the Main Form
            </summary>
            <remarks>Also implicitly brings the control to the front</remarks>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GuiCancelUserAttention">
            <summary>
            Cancels the attention required status of the GUI
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ErrorInModule(System.String)">
            <summary>
            Raise an error in the module - this will cause the termination of the module.
            </summary>
            <param name="errorDescription">Error description to go in error log</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.RaiseNonParamFail(System.Int32,System.String)">
            <summary>
            <para>
            Raise an exception for a Non-Parametric Failure - this will cause the termination of the module.
            </para>
            This method can be used where the DUT has failed in some operational way rather than producing a
            parametric failure, e.g. where a DUT never reaches it's required operating temperature and causes
            a timeout. It may be used for algorthmic failures which are not considered to be software errors.
            <para>
            The fail message string will get truncated on the GUI if it's too long.
            </para>
            <para>
            The optional device fail code can hold a test solution -specific enum value cast to an integer. 
            Recommend passing in a '0' if unused. 
            </para>
            </summary>
            <param name="optionalDutErrorCode">Optional device error code integer value for test results</param>
            <param name="deviceErrorDescription">Error description to go in error log</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.SendToGui(System.Object)">
            <summary>
            Send a message to the GUI custom control so it can update GUI visible information
            </summary>
            <param name="msg">Message to send</param>
            <returns>Sequence number of the message sent</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.SendToGui(System.Object,System.Int64)">
            <summary>
            Send a message to the GUI custom control so it can update GUI visible information
            </summary>
            <param name="msg">Message to send</param>
            <param name="responseSeqNum">Sequence number of the original 
            message this is responding to</param>
            <returns>Sequence number of the message sent</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ReceiveFromGui">
            <summary>
            Receive a message from the GUI custom control, bring GUI input data. 
            </summary>
            <returns>Message container</returns>
            <remarks>No timeout expiry, waits forever.</remarks>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ReceiveFromGui(System.Int32)">
            <summary>
            Receive a message from the GUI custom control, bring GUI input data. 
            </summary>
            <param name="timeout_ms">Maximum time-out in milliseconds</param>
            <returns>Message container or null if timed out</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.CheckForAbort">
            <summary>
            Checks for a system abort voluntarily
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.SendStatusMsg(System.String)">
            <summary>
            Send a status message to the Status control in the main basic GUI.
            The Test Engine will log all of these messages with its standard 
            Logging Framework.
            </summary>
            <param name="msg">Message to display</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ShowUserQuery(System.String,Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo[])">
            <summary>
            Show a user query within the test program tab and wait for a response.
            </summary>
            <remarks>"User Attention" LED will blink.</remarks>
            <param name="questionText">Text to show to user.</param>
            <param name="buttons">A set of choices for the user to respond to.</param>
            <returns>The responsePayload object for the button clicked.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ShowYesNoUserQuery(System.String)">
            <summary>
            Show a user query with Yes/No buttons and wait for a response.
            </summary>
            <remarks>"User Attention" LED will blink.</remarks>
            <param name="questionText">Text to show to the user.</param>
            <returns>Enum value indicating button clicked.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ShowYesNoCancelUserQuery(System.String)">
            <summary>
            Show a user query with Yes/No/Cancel buttons and wait for a response.
            </summary>
            <remarks>"User Attention" LED will blink.</remarks>
            <param name="questionText">Text to show to the user.</param>
            <returns>Enum value indicating button clicked.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ShowContinueUserQuery(System.String)">
            <summary>
            Show a user query with a Continue button and wait for a response.
            </summary>
            <remarks>"User Attention" LED will blink.</remarks>
            <param name="questionText">Text to show to the user.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.ShowContinueCancelUserQuery(System.String)">
            <summary>
            Show a user query with Continue/Cancel buttons and wait for a response.
            </summary>
            <remarks>"User Attention" LED will blink.</remarks>
            <param name="questionText">Text to show to the user.</param>
            <returns>Enum value indicating button clicked.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.StartTestProcessTimer">
            <summary>
            Starts Test Process Timer before doing testing, eg. call this after showing message box to user and ready to do testing.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.StopTestProcessTimer">
            <summary>
            Stops Test Process Timer after doing testing,eg. call this before showing Message Box to user.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.WriteDeviceTraceLog(System.String)">
            <summary>
            Write device trace log.
            </summary>
            <param name="userMessage">User Message</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.GetCurrentModuleTestProcessTime">
            <summary>
            Gets current test module test process time in seconds.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.SendEmail(System.String,System.String,System.String)">
            <summary>
            Sends email.
            This method only works if EmailerEnable is true in config "EmailerConfig.xml".
            </summary>
            <param name="emailType">The type of email.</param>
            <param name="emailSubject">The subject of email.</param>
            <param name="emailBody">The body of email.</param>
        </member>
        <member name="P:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.IsSimulation">
            <summary>
            Is the module in simulation mode?
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.IsDebug">
            <summary>
            Is the module in debug mode?
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.PluginInterfaces.Module.ITestEngine.UsingUtcDateTime">
            <summary>
            Gets UsingUtcDateTime flag,which tells user whether TestEngine Core uses UTC date time or local date time.
            If true, TE uses UTC.Otherwise,TE uses local time.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.ITestModule">
            <summary>
            The basis of a Test Module: all modules must implement this interface to be plugged-in to 
            Test Engine framework.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Module.ITestModule.DoTest(Bookham.TestEngine.PluginInterfaces.Module.ITestEngine,Bookham.TestEngine.PluginInterfaces.Module.ModulePrivilegeLevel,Bookham.TestEngine.Framework.InternalData.DatumList,Bookham.TestEngine.Equipment.InstrumentCollection,Bookham.TestEngine.Equipment.ChassisCollection,Bookham.TestEngine.Framework.InternalData.DatumList,Bookham.TestEngine.Framework.InternalData.DatumList)">
            <summary>
            Run a test module
            </summary>
            <param name="engine">Test Engine facilities</param>
            <param name="userType">User type</param>
            <param name="configData">Configuration data that applies to the running of the Module</param>
            <param name="instruments">Instrument collection</param>
            <param name="chassis">Chassis collection</param>
            <param name="calData">Calibration Data</param>
            <param name="previousTestData">Input Data: i.e. feed-forward data from a 
            previous test module or stage</param>
            <returns>Output Data</returns>
        </member>
        <member name="P:Bookham.TestEngine.PluginInterfaces.Module.ITestModule.UserControl">
            <summary>
            Get type of optional User Control - return null object reference if no custom control
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.ModulePrivilegeLevel">
            <summary>
            Privilege level of the user
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ModulePrivilegeLevel.Operator">
            <summary>
            Operator privilege level
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Module.ModulePrivilegeLevel.Technician">
            <summary>
            Technician privilege level
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Module.NamespaceDoc">
            <summary>
            Test Engine Modules plugin definitions. 
            These classes are needed by Test Software developers producing Test Modules.
            </summary>
        </member>
    </members>
</doc>

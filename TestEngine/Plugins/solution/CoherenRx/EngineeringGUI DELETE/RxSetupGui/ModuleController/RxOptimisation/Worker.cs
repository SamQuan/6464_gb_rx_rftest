using System;
using System.Collections.Generic;
using System.Text;
using LabGUI;
using RxOptimisationGUI.RxOptimisation;
using Bookham.TestLibrary.InstrTypes;


namespace RxOptimisationGUI
{
    public static class Worker
    {

        public static MainForm mainForm;

        public static void DoSomething()
        {
            Instruments.sourcemeters[Instruments.SourceMeter.DemuxPhase_I].VoltageSetPoint_Volt = 0;
        }

        public static void FullAutoSetup(SetupConditions smuSetup)
        {
             // Initialise
            #region smuSourceLevels
            Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_I].VoltageSetPoint_Volt = smuSetup.HeaterPhaseI_V;
            Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_I].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_Q].VoltageSetPoint_Volt = smuSetup.HeaterPhaseQ_V;
            Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_Q].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_I].VoltageSetPoint_Volt = smuSetup.RxThresholdI_V;
            Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_I].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_Q].VoltageSetPoint_Volt = smuSetup.RxThresholdQ_V;
            Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_Q].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.DemuxThreshold_I].VoltageSetPoint_Volt = smuSetup.DemuxThresholdI_V;
            Instruments.sourcemeters[Instruments.SourceMeter.DemuxThreshold_I].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.DemuxThreshold_Q].VoltageSetPoint_Volt = smuSetup.DemuxThresholdQ_V;
            Instruments.sourcemeters[Instruments.SourceMeter.DemuxThreshold_Q].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.DemuxPhase_I].VoltageSetPoint_Volt = smuSetup.DemuxPhaseI_V;
            Instruments.sourcemeters[Instruments.SourceMeter.DemuxPhase_I].OutputEnabled = true;

            Instruments.sourcemeters[Instruments.SourceMeter.DemuxPhase_Q].VoltageSetPoint_Volt = smuSetup.DemuxPhaseQ_V;
            Instruments.sourcemeters[Instruments.SourceMeter.DemuxPhase_Q].OutputEnabled = true;
            #endregion

            #region Initialise BERTs
            Worker.mainForm.DebugMessage("", "", "", "", "Auto Search I & Q");
            Instruments.BertI.AutoSetup();
            Instruments.BertQ.AutoSetup();

            // Wait for I error detector to complete.
            // Q Bert needs another 10 seconds.
            System.Threading.Thread.Sleep(15000);
            #endregion

            //
            // Optimise Rx Thr I
            //
            Worker.mainForm.DebugMessage("RxThreshold_I", "", "", "", "Optimising RxThreshold");
            InstType_ElectricalSource smu = Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_I];
            double step_V = 0.005;
            // Find lower threshold
            double lowerRxThrI = OptimiseForMinEr(0, -0.5, step_V, 250, Instruments.BertI, smu );
            // Find upper threshold
            double upperRxThrI = OptimiseForMinEr(0, 0.5, step_V, 250, Instruments.BertI, smu);
            // Scan between upper and lower thresholds using a smaller step size
            step_V = 0.0005;
            double rxThr_I = OptimiseForMinEr(lowerRxThrI, upperRxThrI, step_V, 250, Instruments.BertI, smu);
            // AutoSearch I
            Instruments.BertI.AutoSetup();


            //
            // Optimise Rx Thr Q
            //
            smu = Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_Q];
            step_V = 0.005;
            // Find lower threshold
            double lowerRxThr_Q = OptimiseForMinEr(0, -0.5, step_V, 250, Instruments.BertQ, smu);
            // Find upper threshold
            double upperRxThr_Q = OptimiseForMinEr(0, 0.5, step_V, 250, Instruments.BertQ, smu);
            // Scan between upper and lower thresholds using a smaller step size
            step_V = 0.0005;
            double rxThr_Q = OptimiseForMinEr(lowerRxThr_Q, upperRxThr_Q, step_V, 250, Instruments.BertQ, smu);
            // AutoSearch I
            Instruments.BertQ.AutoSetup();


            //
            // Scan the heater I voltage
            //
            smu = Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_I];
            step_V = 0.002;
            double start_V = smuSetup.HeaterPhaseI_V;

            // Find lower threshold
            double lowerRxHtr_I = OptimiseForMinEr(start_V, 0, step_V, 250, Instruments.BertI, smu);
            // Find upper threshold
            double upperRxHtr_I = OptimiseForMinEr(start_V, 5, step_V, 250, Instruments.BertI, smu);
            // Scan between upper and lower thresholds using a smaller step size
            step_V = 0.0005;
            double rxHtr_I = OptimiseForMinEr(lowerRxHtr_I, upperRxHtr_I, step_V, 250, Instruments.BertI, smu);
            // AutoSearch I
            Instruments.BertI.AutoSetup();

            //
            // Scan the heater Q voltage
            //
            smu = Instruments.sourcemeters[Instruments.SourceMeter.RxHeater_Q];
            step_V = 0.002;
            start_V = smuSetup.HeaterPhaseQ_V;

            // Find lower threshold
            double lowerRxHtr_Q = OptimiseForMinEr(start_V, 0, step_V, 250, Instruments.BertQ, smu);
            // Find upper threshold
            double upperRxHtr_Q = OptimiseForMinEr(start_V, 5, step_V, 250, Instruments.BertQ, smu);
            // Scan between upper and lower thresholds using a smaller step size
            step_V = 0.0005;
            double rxHtr_Q = OptimiseForMinEr(lowerRxHtr_Q, upperRxHtr_Q, step_V, 250, Instruments.BertQ, smu);
            // AutoSearch I
            Instruments.BertQ.AutoSetup(); 
            
            // Wait for completion
            System.Threading.Thread.Sleep(25000); 

            //
            // Re check Rx Thr I
            //
            smu = Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_I];
            rxThr_I = OptimiseForMinEr(lowerRxThrI, upperRxThrI, step_V, 250, Instruments.BertI, smu);

            //##################################

            //
            // Re check Rx Thr Q
            //
            smu = Instruments.sourcemeters[Instruments.SourceMeter.RxThreshold_Q];
            rxThr_Q = OptimiseForMinEr(lowerRxThr_Q, upperRxThr_Q, step_V, 250, Instruments.BertQ, smu);
        }

        public static double OptimiseForMinEr(double startBias, double stopBias, double stepBias, int delay_ms, IInstType_BertErrorAnalyser bert, InstType_ElectricalSource smu)
        {
            double lastErrorRate = 0;
            double bestER = 999999;
            double bestBias = startBias;
            int wrongDirectionCount = 0;
            int erMargin = 3;
            int maxLoopCount = 10;

            // MUST search away from center to avoid sync loss
            stepBias = startBias < stopBias ? Math.Abs(stepBias) : -Math.Abs(stepBias);
            int stepCount = (int)Math.Abs((startBias - stopBias) / stepBias) + 1;

            double biasV = startBias;
            do
            {
                smu.VoltageSetPoint_Volt = biasV;

                lastErrorRate = ReadErrorRate(bert, delay_ms);

                if (lastErrorRate < bestER)
                {
                    bestER = lastErrorRate;
                    bestBias = biasV;
                    wrongDirectionCount = 0;
                }
                else
                {
                    wrongDirectionCount++;
                }

                if (lastErrorRate == 0)
                    break;
                if (lastErrorRate > bestER * erMargin)
                    break;
                if (wrongDirectionCount > maxLoopCount)
                    break;

                Worker.mainForm.DebugMessage(smu.Name, biasV.ToString(), lastErrorRate.ToString(), bestER.ToString(), "");

                biasV += stepBias;
                stepCount--;
            } while (stepCount >= 0);

            return bestBias;
        }

        public static double ReadErrorRate(IInstType_BertErrorAnalyser bert, int delay_ms)
        {
            bert.AnalyserStop();
            bert.AnalyserStart();

            // Wait for a decent number of errors
            // TODO - check for sync loss

            long errorCount = 0;
            int minErrorCount = 25;
            int maxLoopCount = 25;

            int loopCount = 0;
            do
            {
                System.Threading.Thread.Sleep(delay_ms);
                errorCount = bert.ErroredBits;
                
                // Check for sync loss
                if (errorCount == long.MaxValue)
                    return 1;

                loopCount++;
            } while ( errorCount < minErrorCount && loopCount < maxLoopCount );

            return bert.ErrorRatio;
        }
    }
}

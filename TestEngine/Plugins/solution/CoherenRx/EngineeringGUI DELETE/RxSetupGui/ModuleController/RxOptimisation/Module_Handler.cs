using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using System.IO;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.Framework.Limits;


namespace LabGUI
{
    /// <summary>
    /// Apart from the Run() method, this was lifted straight from the production TestProgram.
    /// It provides the necessary inputs required to run the test module.
    /// </summary>
    public class CalDAC_Handler
    {

        public CalDAC_Handler()
        {
        }

        /// <summary>
        /// Performs initialisation on the module, then runs it.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        public DatumList Run(LocalEngine engine, DUTObject dutObject)
        {
            tsffPcbCalParamsConfig = new TestParamConfigAccessor(dutObject,
                "Configuration/TSFF/tsffPcbCalTestParams.xml", "ALL", "TsffPcbCalTestParams");

            this.dutModule = InstrumentLoader.InstrumentList["BlackBox"];
            this.switchChassis = InstrumentLoader.ChassisList["Chassis_Ag34970A"];
            this.dauDMM_0 = InstrumentLoader.InstrumentList["dauDMM_1"];
            this.dauDMM_1 = InstrumentLoader.InstrumentList["dauDMM_2"];
            this.dauDMM_2 = InstrumentLoader.InstrumentList["dauDMM_3"];

            ITestEngineInit engineInit = (ITestEngineInit)engine;
            DacTest_InitModule(engineInit);

            ITestEngineRun engineRun = (ITestEngineRun)engine;
            return DacTest(engineRun);
        }



        /// <summary>
        /// The Dac test init (or set up of test params / data etc)
        /// </summary>
        private void DacTest_InitModule(ITestEngineInit engine)
        {

            ModuleRun modRun = engine.GetModuleRun("ThisModule");

            modRun.Instrs.Add("dauDMM", this.dauDMM_0);

            modRun.Instrs.Add("CalModule", this.dutModule);
            modRun.Chassis.Add("Chassis_Ag34970A", this.switchChassis);

            modRun.ConfigData.AddBool("SomeParam", true);

            //
            // COMMENTED OUT
            //

            //Need to provide some test limits (only if necessary)
            //    modRun.Limits.AddParameter(this.testSpec,"SLOPE_GRADIENT1", "SlopeGradient1");
        }

        /// <summary>
        /// Does the test
        /// </summary>
        /// <param name="engine">our test engine</param>
        /// <returns>a datumlist containing all the plots</returns>
        private DatumList DacTest(ITestEngineRun engine)
        {
            ModuleRunReturn modRunRtn;

            modRunRtn = engine.RunModule(("ThisModule"));
            DatumList myDatumResults = modRunRtn.ModuleRunData.ModuleData;
            return myDatumResults;
        }

        /// <summary>
        /// Generate a unique filename to store some blob data in. 
        /// Filename shall be derived from the current system date and time, in the following
        /// format:
        ///       YYYYMMDDHHNNSSUUU
        ///
        /// where YYYY is the four digit year
        ///       MM is the month
        ///       DD is the day
        ///       HH is the hour
        ///       NN is the minute
        ///      SS is the second
        ///      UUU is the milliseconds
        /// 
        /// The caller must append a file extension to the generated filename.
        /// </summary>
        /// <returns>The filename</returns>
        internal string generateUniqueFilename()
        {
            System.DateTime now = DateTime.UtcNow;
            //Do the next line to guarantee we can't have same filename in same ms on this clock
            System.Threading.Thread.Sleep(1);
            return now.ToString("yyyyMMddHHmmssfff");
        }


        #region Private data
        /// <summary>
        /// The test boards +1.8V supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc1pt8Supply;
        /// <summary>
        /// The test boards +3V supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc3Supply;
        /// <summary>
        /// The test board +5V  supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc5Supply;
        /// <summary>
        /// The testt board -5.2V supply
        /// </summary>
        InstType_ElectricalSource testBoardVee5pt2Supply;
        ///// <summary>
        ///// Our TSFF chassis
        ///// </summary>
        //Chassis TsffChassis;
        /// <summary>
        /// Our DUT 
        /// </summary>
        Instrument dutModule;
        /// <summary>
        /// The switch chassis we are using
        /// </summary>
        Chassis switchChassis;
        /// <summary>
        /// Multimeter 0 (Channel 120)
        /// </summary>
        Instrument dauDMM_0;
        /// <summary>
        /// Multimeter 1 (Channel 121)
        /// </summary>
        Instrument dauDMM_1;
        /// <summary>
        /// Multimeter 2 (Channel 122)
        /// </summary>
        Instrument dauDMM_2;

        /// <summary>
        /// Our Line Wide test parameters
        /// </summary>
        TestParamConfigAccessor tsffPcbCalParamsConfig;

        /// <summary>
        /// Our Node Specific  test parameters
        /// </summary>
        TestParamConfigAccessor nodeSpecificTestParamsConfig;
        /// <summary>
        /// Our test specification
        /// </summary>
        Specification testSpec;
        /// <summary>
        /// Our test conditions
        /// </summary>
        DatumList testConditions;

        /// <summary>
        /// Our Blob File Link
        /// </summary>
        DatumFileLink blobFileData;
        #endregion
    
    }
}

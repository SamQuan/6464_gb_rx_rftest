using System;
using System.Collections.Generic;
using System.Text;

namespace RxOptimisationGUI.RxOptimisation
{
    public struct SetupConditions
    {
        public double HeaterPhaseI_V;
        public double HeaterPhaseQ_V;
        public double RxThresholdI_V;
        public double RxThresholdQ_V;
        public double DemuxThresholdI_V;
        public double DemuxThresholdQ_V; 
        public double DemuxPhaseI_V;
        public double DemuxPhaseQ_V;
    }
}

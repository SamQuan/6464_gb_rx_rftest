using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.PluginInterfaces.Program;

namespace LabGUI
{
    public static class GuiConfig
    {
        public static void Initialise()
        {
            string teBaseDir;
            if (AppDomain.CurrentDomain.BaseDirectory.Contains("TestEngine"))
            {
                teBaseDir = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.LastIndexOf("TestEngine") + 10);
            }
            else
            {
                teBaseDir = ".";
            }
            DUTObject dutObject = new DUTObject();
            dutObject.TestStage = "All";
            dutObject.PartCode = "All";

            SetupData = new TestParamConfigAccessor(dutObject, teBaseDir + "/Configuration/LabGui/Config.xml", "ALL", "SetupData");
        }

        public static TestParamConfigAccessor SetupData;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.InstrTypes;

namespace LabGUI
{
    /// <summary>
    /// Handles the communication with the instrumentation.
    /// </summary>
    static class Instruments
    {        
        /// <summary>
        /// Setup up logging and instantiate the instruments.
        /// </summary>
        internal static void Initialise(string ApplicationName, List<string> smuNames, List<string> dmmNames, string NameOfBertI, string NameOfBertQ)
        {
            try
            {
                InstrumentLoader.Initialise(ApplicationName);
            }
            catch (Bookham.TestEngine.Framework.PluginLoader.PluginInstantiatingFailException pif)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("\r\n" + pif.Message);

                errorMessage.Append(Program.BuildErrorTrace(pif.InnerException));

                throw new SystemException("Error loading instrument driver :\r\n" + errorMessage );
            }


            //
            // Assign instruments here :
            //
            //chassis = InstrumentLoader.ChassisList["BlackBoxChassis"];
            InstType_ElectricalSource smu;
            InstType_MultiMeter dmm;

            // Create collection of SMUs
            foreach (string instName in smuNames)
            {
                smu = (InstType_ElectricalSource)InstrumentLoader.InstrumentList[instName];
                SourceMeter smuName = (SourceMeter)Enum.Parse(typeof(SourceMeter), instName);
                sourcemeters.Add(smuName, smu);
            }

            //// Create collection of DMMs
            //foreach (string instName in dmmNames)
            //{
            //    dmm = (InstType_MultiMeter)InstrumentLoader.InstrumentList[instName];
            //    MultiMeter dmmName = (MultiMeter)Enum.Parse(typeof(MultiMeter), instName);
            //    multimeters.Add(dmmName, dmm);
            //}


            // Error detectors
            BertI = (IInstType_BertErrorAnalyser)InstrumentLoader.InstrumentList[NameOfBertI];
            BertQ = (IInstType_BertErrorAnalyser)InstrumentLoader.InstrumentList[NameOfBertQ];
        }

        public enum SourceMeter
        {
            RxHeater_I,
            RxThreshold_I,
            DemuxPhase_I,
            DemuxThreshold_I,
            Pin1_I,
            Pin2_I,
            RxHeater_Q,
            RxThreshold_Q,
            DemuxPhase_Q,
            DemuxThreshold_Q,
            Pin1_Q,
            Pin2_Q
        }

        public enum MultiMeter
        {
            RxDcOffset_I,
            RxDcOffset_Q
        }


        /// <summary>
        /// Black box laser driver
        /// </summary>
        private static IDsdbrLaserSetup dsdbrLaser;
        /// <summary>
        /// Black box single MZ driver
        /// </summary>
        private static ITcmzLaserSetup tcmzLaser;

        private static Chassis chassis;
        private static Instrument instrument;

        public static Dictionary<SourceMeter, InstType_ElectricalSource> sourcemeters =new Dictionary<SourceMeter,InstType_ElectricalSource>();
        public static Dictionary<MultiMeter, InstType_MultiMeter> multimeters = new Dictionary<MultiMeter,InstType_MultiMeter>();

        public static IInstType_BertErrorAnalyser BertQ;
        public static IInstType_BertErrorAnalyser BertI;
    }
}

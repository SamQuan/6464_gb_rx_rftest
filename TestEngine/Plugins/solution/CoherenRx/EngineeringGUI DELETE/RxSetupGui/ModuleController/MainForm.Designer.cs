namespace LabGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Pin2_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.RxThreshold_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.RxHeater_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.DemuxPhase_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.Pin1_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.DemuxThreshold_Q = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Pin2_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.Pin1_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.DemuxThreshold_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.DemuxPhase_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.RxThreshold_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.RxHeater_I = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider5 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider4 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider3 = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider2 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider1 = new ModuleController.UserControls.scrollboxAndSlider();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtTestProgress = new System.Windows.Forms.TextBox();
            this.txtBestBer = new System.Windows.Forms.TextBox();
            this.lblBestBER = new System.Windows.Forms.Label();
            this.txtBer = new System.Windows.Forms.TextBox();
            this.lblBER = new System.Windows.Forms.Label();
            this.txtSetting = new System.Windows.Forms.TextBox();
            this.lblSetting = new System.Windows.Forms.Label();
            this.lblTestProgress = new System.Windows.Forms.Label();
            this.btnRxSetup = new System.Windows.Forms.Button();
            this.btnAutoSetup = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(742, 471);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(734, 445);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Start";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Pin2_Q);
            this.groupBox4.Controls.Add(this.RxThreshold_Q);
            this.groupBox4.Controls.Add(this.RxHeater_Q);
            this.groupBox4.Controls.Add(this.DemuxPhase_Q);
            this.groupBox4.Controls.Add(this.Pin1_Q);
            this.groupBox4.Controls.Add(this.DemuxThreshold_Q);
            this.groupBox4.Location = new System.Drawing.Point(271, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 229);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Rx Q";
            // 
            // Pin2_Q
            // 
            this.Pin2_Q.CheckboxChecked = true;
            this.Pin2_Q.Location = new System.Drawing.Point(6, 52);
            this.Pin2_Q.Name = "Pin2_Q";
            this.Pin2_Q.Size = new System.Drawing.Size(223, 28);
            this.Pin2_Q.TabIndex = 5;
            this.Pin2_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Pin2_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.Pin2_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // RxThreshold_Q
            // 
            this.RxThreshold_Q.CheckboxChecked = true;
            this.RxThreshold_Q.Location = new System.Drawing.Point(8, 124);
            this.RxThreshold_Q.Name = "RxThreshold_Q";
            this.RxThreshold_Q.Size = new System.Drawing.Size(223, 28);
            this.RxThreshold_Q.TabIndex = 1;
            this.RxThreshold_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RxThreshold_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.RxThreshold_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // RxHeater_Q
            // 
            this.RxHeater_Q.CheckboxChecked = true;
            this.RxHeater_Q.Location = new System.Drawing.Point(6, 86);
            this.RxHeater_Q.Name = "RxHeater_Q";
            this.RxHeater_Q.Size = new System.Drawing.Size(223, 32);
            this.RxHeater_Q.TabIndex = 0;
            this.RxHeater_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RxHeater_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.RxHeater_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // DemuxPhase_Q
            // 
            this.DemuxPhase_Q.CheckboxChecked = true;
            this.DemuxPhase_Q.Location = new System.Drawing.Point(6, 158);
            this.DemuxPhase_Q.Name = "DemuxPhase_Q";
            this.DemuxPhase_Q.Size = new System.Drawing.Size(223, 28);
            this.DemuxPhase_Q.TabIndex = 2;
            this.DemuxPhase_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DemuxPhase_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.DemuxPhase_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // Pin1_Q
            // 
            this.Pin1_Q.CheckboxChecked = true;
            this.Pin1_Q.Location = new System.Drawing.Point(6, 18);
            this.Pin1_Q.Name = "Pin1_Q";
            this.Pin1_Q.Size = new System.Drawing.Size(223, 28);
            this.Pin1_Q.TabIndex = 4;
            this.Pin1_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Pin1_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.Pin1_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // DemuxThreshold_Q
            // 
            this.DemuxThreshold_Q.CheckboxChecked = true;
            this.DemuxThreshold_Q.Location = new System.Drawing.Point(6, 192);
            this.DemuxThreshold_Q.Name = "DemuxThreshold_Q";
            this.DemuxThreshold_Q.Size = new System.Drawing.Size(223, 28);
            this.DemuxThreshold_Q.TabIndex = 3;
            this.DemuxThreshold_Q.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DemuxThreshold_Q.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.DemuxThreshold_Q.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Pin2_I);
            this.groupBox3.Controls.Add(this.Pin1_I);
            this.groupBox3.Controls.Add(this.DemuxThreshold_I);
            this.groupBox3.Controls.Add(this.DemuxPhase_I);
            this.groupBox3.Controls.Add(this.RxThreshold_I);
            this.groupBox3.Controls.Add(this.RxHeater_I);
            this.groupBox3.Location = new System.Drawing.Point(25, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 229);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rx I";
            // 
            // Pin2_I
            // 
            this.Pin2_I.CheckboxChecked = true;
            this.Pin2_I.Location = new System.Drawing.Point(11, 52);
            this.Pin2_I.Name = "Pin2_I";
            this.Pin2_I.Size = new System.Drawing.Size(223, 28);
            this.Pin2_I.TabIndex = 5;
            this.Pin2_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Pin2_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.Pin2_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // Pin1_I
            // 
            this.Pin1_I.CheckboxChecked = true;
            this.Pin1_I.Location = new System.Drawing.Point(11, 19);
            this.Pin1_I.Name = "Pin1_I";
            this.Pin1_I.Size = new System.Drawing.Size(223, 28);
            this.Pin1_I.TabIndex = 4;
            this.Pin1_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Pin1_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.Pin1_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // DemuxThreshold_I
            // 
            this.DemuxThreshold_I.CheckboxChecked = true;
            this.DemuxThreshold_I.Location = new System.Drawing.Point(11, 192);
            this.DemuxThreshold_I.Name = "DemuxThreshold_I";
            this.DemuxThreshold_I.Size = new System.Drawing.Size(223, 28);
            this.DemuxThreshold_I.TabIndex = 3;
            this.DemuxThreshold_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DemuxThreshold_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.DemuxThreshold_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // DemuxPhase_I
            // 
            this.DemuxPhase_I.CheckboxChecked = true;
            this.DemuxPhase_I.Location = new System.Drawing.Point(11, 158);
            this.DemuxPhase_I.Name = "DemuxPhase_I";
            this.DemuxPhase_I.Size = new System.Drawing.Size(223, 28);
            this.DemuxPhase_I.TabIndex = 2;
            this.DemuxPhase_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DemuxPhase_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.DemuxPhase_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // RxThreshold_I
            // 
            this.RxThreshold_I.CheckboxChecked = true;
            this.RxThreshold_I.Location = new System.Drawing.Point(11, 123);
            this.RxThreshold_I.Name = "RxThreshold_I";
            this.RxThreshold_I.Size = new System.Drawing.Size(223, 28);
            this.RxThreshold_I.TabIndex = 1;
            this.RxThreshold_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RxThreshold_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.RxThreshold_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // RxHeater_I
            // 
            this.RxHeater_I.CheckboxChecked = true;
            this.RxHeater_I.Location = new System.Drawing.Point(11, 86);
            this.RxHeater_I.Name = "RxHeater_I";
            this.RxHeater_I.Size = new System.Drawing.Size(223, 32);
            this.RxHeater_I.TabIndex = 0;
            this.RxHeater_I.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RxHeater_I.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.Smu_ValueChanged);
            this.RxHeater_I.CheckboxChanged += new ModuleController.UserControls.scrollboxAndSlider.CheckboxChangedHandler(this.Smu_CheckboxChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scrollboxAndSlider5);
            this.groupBox2.Controls.Add(this.scrollboxAndSlider4);
            this.groupBox2.Controls.Add(this.scrollboxAndSlider3);
            this.groupBox2.Location = new System.Drawing.Point(25, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 126);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            this.groupBox2.Visible = false;
            // 
            // scrollboxAndSlider5
            // 
            this.scrollboxAndSlider5.CheckboxChecked = true;
            this.scrollboxAndSlider5.Location = new System.Drawing.Point(6, 90);
            this.scrollboxAndSlider5.Name = "scrollboxAndSlider5";
            this.scrollboxAndSlider5.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider5.TabIndex = 2;
            this.scrollboxAndSlider5.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider4
            // 
            this.scrollboxAndSlider4.CheckboxChecked = true;
            this.scrollboxAndSlider4.Location = new System.Drawing.Point(6, 56);
            this.scrollboxAndSlider4.Name = "scrollboxAndSlider4";
            this.scrollboxAndSlider4.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider4.TabIndex = 1;
            this.scrollboxAndSlider4.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider3
            // 
            this.scrollboxAndSlider3.CheckboxChecked = true;
            this.scrollboxAndSlider3.Location = new System.Drawing.Point(6, 19);
            this.scrollboxAndSlider3.Name = "scrollboxAndSlider3";
            this.scrollboxAndSlider3.Size = new System.Drawing.Size(223, 31);
            this.scrollboxAndSlider3.TabIndex = 0;
            this.scrollboxAndSlider3.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.scrollboxAndSlider2);
            this.groupBox1.Controls.Add(this.scrollboxAndSlider1);
            this.groupBox1.Location = new System.Drawing.Point(271, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 93);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            this.groupBox1.Visible = false;
            // 
            // scrollboxAndSlider2
            // 
            this.scrollboxAndSlider2.CheckboxChecked = true;
            this.scrollboxAndSlider2.Location = new System.Drawing.Point(8, 55);
            this.scrollboxAndSlider2.Name = "scrollboxAndSlider2";
            this.scrollboxAndSlider2.Size = new System.Drawing.Size(226, 29);
            this.scrollboxAndSlider2.TabIndex = 1;
            this.scrollboxAndSlider2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider1
            // 
            this.scrollboxAndSlider1.CheckboxChecked = true;
            this.scrollboxAndSlider1.Location = new System.Drawing.Point(8, 19);
            this.scrollboxAndSlider1.Name = "scrollboxAndSlider1";
            this.scrollboxAndSlider1.Size = new System.Drawing.Size(226, 31);
            this.scrollboxAndSlider1.TabIndex = 0;
            this.scrollboxAndSlider1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.btnRxSetup);
            this.tabPage2.Controls.Add(this.btnAutoSetup);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(734, 445);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Rx Setup";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtTestProgress);
            this.groupBox5.Controls.Add(this.txtBestBer);
            this.groupBox5.Controls.Add(this.lblBestBER);
            this.groupBox5.Controls.Add(this.txtBer);
            this.groupBox5.Controls.Add(this.lblBER);
            this.groupBox5.Controls.Add(this.txtSetting);
            this.groupBox5.Controls.Add(this.lblSetting);
            this.groupBox5.Controls.Add(this.lblTestProgress);
            this.groupBox5.Location = new System.Drawing.Point(8, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(623, 142);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Debug";
            // 
            // txtTestProgress
            // 
            this.txtTestProgress.Location = new System.Drawing.Point(161, 13);
            this.txtTestProgress.Multiline = true;
            this.txtTestProgress.Name = "txtTestProgress";
            this.txtTestProgress.Size = new System.Drawing.Size(456, 123);
            this.txtTestProgress.TabIndex = 7;
            // 
            // txtBestBer
            // 
            this.txtBestBer.Location = new System.Drawing.Point(65, 91);
            this.txtBestBer.Name = "txtBestBer";
            this.txtBestBer.Size = new System.Drawing.Size(65, 20);
            this.txtBestBer.TabIndex = 6;
            // 
            // lblBestBER
            // 
            this.lblBestBER.AutoSize = true;
            this.lblBestBER.Location = new System.Drawing.Point(6, 94);
            this.lblBestBER.Name = "lblBestBER";
            this.lblBestBER.Size = new System.Drawing.Size(53, 13);
            this.lblBestBER.TabIndex = 5;
            this.lblBestBER.Text = "Best BER";
            // 
            // txtBer
            // 
            this.txtBer.Location = new System.Drawing.Point(65, 68);
            this.txtBer.Name = "txtBer";
            this.txtBer.Size = new System.Drawing.Size(65, 20);
            this.txtBer.TabIndex = 4;
            // 
            // lblBER
            // 
            this.lblBER.AutoSize = true;
            this.lblBER.Location = new System.Drawing.Point(8, 71);
            this.lblBER.Name = "lblBER";
            this.lblBER.Size = new System.Drawing.Size(29, 13);
            this.lblBER.TabIndex = 3;
            this.lblBER.Text = "BER";
            // 
            // txtSetting
            // 
            this.txtSetting.Location = new System.Drawing.Point(65, 46);
            this.txtSetting.Name = "txtSetting";
            this.txtSetting.Size = new System.Drawing.Size(65, 20);
            this.txtSetting.TabIndex = 2;
            // 
            // lblSetting
            // 
            this.lblSetting.AutoSize = true;
            this.lblSetting.Location = new System.Drawing.Point(6, 49);
            this.lblSetting.Name = "lblSetting";
            this.lblSetting.Size = new System.Drawing.Size(40, 13);
            this.lblSetting.TabIndex = 1;
            this.lblSetting.Text = "Setting";
            // 
            // lblTestProgress
            // 
            this.lblTestProgress.AutoSize = true;
            this.lblTestProgress.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTestProgress.Location = new System.Drawing.Point(8, 16);
            this.lblTestProgress.Name = "lblTestProgress";
            this.lblTestProgress.Size = new System.Drawing.Size(38, 13);
            this.lblTestProgress.TabIndex = 0;
            this.lblTestProgress.Text = "Ready";
            // 
            // btnRxSetup
            // 
            this.btnRxSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRxSetup.Location = new System.Drawing.Point(637, 55);
            this.btnRxSetup.Name = "btnRxSetup";
            this.btnRxSetup.Size = new System.Drawing.Size(91, 28);
            this.btnRxSetup.TabIndex = 7;
            this.btnRxSetup.Text = "Rx Setup";
            this.btnRxSetup.UseVisualStyleBackColor = true;
            this.btnRxSetup.Click += new System.EventHandler(this.btnRxSetup_Click);
            // 
            // btnAutoSetup
            // 
            this.btnAutoSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoSetup.Location = new System.Drawing.Point(637, 22);
            this.btnAutoSetup.Name = "btnAutoSetup";
            this.btnAutoSetup.Size = new System.Drawing.Size(91, 28);
            this.btnAutoSetup.TabIndex = 6;
            this.btnAutoSetup.Text = "Auto Setup";
            this.btnAutoSetup.UseVisualStyleBackColor = true;
            this.btnAutoSetup.Click += new System.EventHandler(this.btnAutoSetup_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 501);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(742, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(38, 17);
            this.toolStripStatusLabel1.Text = "Ready";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(742, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 523);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider5;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider4;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider3;
        private System.Windows.Forms.GroupBox groupBox1;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider2;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider1;
        private System.Windows.Forms.GroupBox groupBox4;
        private ModuleController.UserControls.scrollboxAndSlider Pin2_Q;
        private ModuleController.UserControls.scrollboxAndSlider Pin1_Q;
        private ModuleController.UserControls.scrollboxAndSlider DemuxThreshold_Q;
        private ModuleController.UserControls.scrollboxAndSlider DemuxPhase_Q;
        private ModuleController.UserControls.scrollboxAndSlider RxThreshold_Q;
        private ModuleController.UserControls.scrollboxAndSlider RxHeater_Q;
        private System.Windows.Forms.GroupBox groupBox3;
        private ModuleController.UserControls.scrollboxAndSlider Pin2_I;
        private ModuleController.UserControls.scrollboxAndSlider Pin1_I;
        private ModuleController.UserControls.scrollboxAndSlider DemuxThreshold_I;
        private ModuleController.UserControls.scrollboxAndSlider DemuxPhase_I;
        private ModuleController.UserControls.scrollboxAndSlider RxThreshold_I;
        private ModuleController.UserControls.scrollboxAndSlider RxHeater_I;
        private System.Windows.Forms.Button btnRxSetup;
        private System.Windows.Forms.Button btnAutoSetup;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtBestBer;
        private System.Windows.Forms.Label lblBestBER;
        private System.Windows.Forms.TextBox txtBer;
        private System.Windows.Forms.Label lblBER;
        private System.Windows.Forms.TextBox txtSetting;
        private System.Windows.Forms.Label lblSetting;
        private System.Windows.Forms.Label lblTestProgress;
        private System.Windows.Forms.TextBox txtTestProgress;
    }
}
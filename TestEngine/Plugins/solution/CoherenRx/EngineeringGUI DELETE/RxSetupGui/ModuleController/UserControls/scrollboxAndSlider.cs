using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ModuleController.UserControls
{
    public partial class scrollboxAndSlider : UserControl
    {
        public scrollboxAndSlider()
        {
            InitializeComponent();
        }

        public void Setup(string description, decimal max, decimal min, int decimalPlaces, int scrollBarScale)
        {
            this.scrollBarScale = scrollBarScale;
            checkBox.Text = description;
            trackBar.Maximum = (int)(max * scrollBarScale);
            scrollBox.Maximum = max; 
            trackBar.Minimum = (int)(min * scrollBarScale);
            scrollBox.Minimum = min;
            trackBar.TickFrequency = 1 + Math.Abs((trackBar.Maximum - trackBar.Minimum) / 10);
            trackBar.SmallChange = Math.Abs((trackBar.Maximum - trackBar.Minimum) / scrollBarScale);
            trackBar.LargeChange = trackBar.TickFrequency - 1;
            scrollBox.DecimalPlaces = decimalPlaces;
        }

        public decimal Value
        {
            get { return scrollBox.Value; }
            set { scrollBox.Value = value; }
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            if (trackBarUpdating) return;
            trackBarUpdating = true;
            scrollBox.Value = (decimal)trackBar.Value / scrollBarScale;
            trackBarUpdating = false;
        }

        private void scrollBox_ValueChanged(object sender, EventArgs e)
        {

            trackBar.Value = (int)(scrollBox.Value * scrollBarScale);
            OnValueChanged();
        }

        public bool CheckboxChecked
        {
            get { return checkBox.Checked; }
            set { checkBox.Checked = value; }
        }


        public delegate void CheckboxChangedHandler(bool ticked, string ControlName);
        public event CheckboxChangedHandler CheckboxChanged;
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            // If an event has no subscribers registered, it will evaluate to null.
            // Check this before calling the event itself.
            if (CheckboxChanged != null)
            {
                CheckboxChanged(checkBox.Checked, this.Name);  // Notify Subscribers
            }
        }

        public delegate void ValueChangedHandler(decimal newValue, string ControlName);
        public event ValueChangedHandler ValueChanged;
        protected virtual void OnValueChanged()
        {
            // If an event has no subscribers registered, it will evaluate to null.
            // Check this before calling the event itself.
            if (ValueChanged != null)
            {
                ValueChanged(scrollBox.Value, this.Name);  // Notify Subscribers
            }
        }

        private int scrollBarScale;
        private bool trackBarUpdating;
    }
}

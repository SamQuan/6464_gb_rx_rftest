using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Framework.Logging;
using ModuleController.UserControls;
using RxOptimisationGUI;
using Bookham.TestLibrary.InstrTypes;
using RxOptimisationGUI.RxOptimisation;

namespace LabGUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            Worker.mainForm = this;

            SetupInstruments();
        }

        private void SetupInstruments()
        {
            GuiConfig.Initialise();
            int smuCount = GuiConfig.SetupData.GetIntParam("NumberOfSMUs");
            int dmmCount = GuiConfig.SetupData.GetIntParam("NumberOfDMMs");
            int decPl = 4;

            // Setup SMUs
            List<string> smuNames = new List<string>();
            for (int i = 1; i <= smuCount; i++)
            {
                string smuName = GuiConfig.SetupData.GetStringParam("Smu" + i.ToString() + "Name");
                smuNames.Add(smuName);
                Control ctrl = FindControlByName(smuName);
                scrollboxAndSlider sb = (scrollboxAndSlider)ctrl;
                int maxVal = GuiConfig.SetupData.GetIntParam(smuName + "_MaxVal");
                int minVal = GuiConfig.SetupData.GetIntParam(smuName + "_MinVal");
                sb.Setup(smuName, maxVal, minVal, decPl, 1000);
            }

            // Setup DMMs
            List<string> dmmNames = new List<string>();
            for (int i = 1; i <= dmmCount; i++)
            {
                string dmmName = GuiConfig.SetupData.GetStringParam("Dmm" + i.ToString() + "Name");
                dmmNames.Add(dmmName);
            }

            // Instantiate all instruments
            Instruments.Initialise(Application.ProductName, smuNames, dmmNames, "Bert_I", "Bert_Q");

            //  Read back settings fom insts to GUI
            foreach (Instruments.SourceMeter smuEnum in Enum.GetValues(typeof(Instruments.SourceMeter)))
            {
                Control ctrl = FindControlByName(smuEnum.ToString());
                scrollboxAndSlider sb = (scrollboxAndSlider)ctrl;

                if (Instruments.sourcemeters[smuEnum].IsOnline)
                {
                    try
                    {
                        //sb.Value = (decimal)Instruments.sourcemeters[smuEnum].VoltageActual_Volt;
                        sb.Value = (decimal)Instruments.sourcemeters[smuEnum].VoltageSetPoint_Volt;
                    }
                    catch (SystemException)
                    {
                    }

                    sb.CheckboxChecked =  Instruments.sourcemeters[smuEnum].OutputEnabled;
                }
                else
                {
                    sb.CheckboxChecked = false;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        System.Collections.Hashtable htFrm = new System.Collections.Hashtable();
        private Control FindControlByName(string ctrlName)
        {
            Form frm = this.FindForm();
            if (htFrm.Count == 0)
                AddControlToHashTable(frm.Name, frm);
            return (Control)htFrm[ctrlName];
        }
        private void AddControlToHashTable(string ctrlName, Control Ctrl)
        {
            if (htFrm.ContainsKey(ctrlName))
                return;
            htFrm.Add(ctrlName, Ctrl);
            htFrm[ctrlName] = Ctrl;
            if (Ctrl.HasChildren)
            {
                foreach (Control ctrl2 in Ctrl.Controls)
                    // recursive call adds all controls and any children
                    AddControlToHashTable(ctrl2.Name, ctrl2);
            }
        }

        private void Smu_ValueChanged(decimal newValue, string ControlName)
        {
            Instruments.SourceMeter smu = (Instruments.SourceMeter)Enum.Parse(typeof(Instruments.SourceMeter), ControlName);
            Instruments.sourcemeters[smu].VoltageSetPoint_Volt = Convert.ToDouble(newValue);
        }

        private void Smu_CheckboxChanged(bool ticked, string ControlName)
        {
            Instruments.SourceMeter smu = (Instruments.SourceMeter)Enum.Parse(typeof(Instruments.SourceMeter), ControlName);
            if ( Instruments.sourcemeters[smu].IsOnline )
                Instruments.sourcemeters[smu].OutputEnabled = ticked;
        }

        private void btnAutoSetup_Click(object sender, EventArgs e)
        {
            SetupConditions smuSetup = new SetupConditions();
            smuSetup.DemuxPhaseI_V = (double)DemuxPhase_I.Value;
            smuSetup.DemuxPhaseQ_V = (double)DemuxPhase_Q.Value;
            smuSetup.DemuxThresholdI_V = (double)DemuxThreshold_I.Value;
            smuSetup.DemuxThresholdQ_V = (double)DemuxThreshold_Q.Value;
            smuSetup.HeaterPhaseI_V = (double)RxHeater_I.Value;
            smuSetup.HeaterPhaseQ_V = (double)RxHeater_Q.Value;
            smuSetup.RxThresholdI_V = (double)RxThreshold_I.Value;
            smuSetup.RxThresholdQ_V = (double)RxThreshold_Q.Value;

            Worker.FullAutoSetup(smuSetup);
        }

        private void btnRxSetup_Click(object sender, EventArgs e)
        {

        }

        public void DebugMessage(string instName, string instBias, string BER, string bestBER, string debugMessage)
        {
            if (instName.Length > 0)
                this.lblTestProgress.Text = instName;

            if (instBias.Length > 0)
                this.txtSetting.Text = instBias;

            if (BER.Length > 0)
                this.txtBer.Text = BER;

            if (bestBER.Length > 0)
                this.txtBestBer.Text = bestBER;

            if (debugMessage.Length > 0)
            {
                this.txtTestProgress.Text += "\r\n" + debugMessage;
                txtTestProgress.SelectionStart = txtTestProgress.Text.Length;
                txtTestProgress.ScrollToCaret();
            }

            Application.DoEvents();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using System.Collections;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestEngine.Framework.PluginLoader;

namespace LabGUI
{
    public class LocalEngine : ITestEngineInit, ITestEngineRun, ITestEngine
    {
        #region ITestEngineInit Members

        /// <summary>
        /// Add module run to program
        /// </summary>
        /// <param name="moduleRunName">Module run name: name of this module run</param>
        /// <param name="moduleName">Module name</param>
        /// <param name="moduleVersion">Module version: if blank use latest found</param>
        /// <returns>Module run object</returns>
        public ModuleRun AddModuleRun(string moduleRunName, string moduleName, string moduleVersion)
        {
            #region Check Inputs
            if ((moduleRunName == null) || (moduleRunName.Length == 0))
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "AddModuleRun: Null or empty moduleRunName");
            }
            if ((moduleName == null) || (moduleName.Length == 0))
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "AddModuleRun: Null or empty moduleName");
            }
            if (moduleVersion == null)
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "AddModuleRun: Null moduleVersion");
            }
            // check there is no duplicate
            if (moduleRunList.ContainsKey(moduleRunName))
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "AddModuleRun: Duplicate ModuleRunName: " + moduleRunName);
            } 
            #endregion

            // Create a new ModuleRun object and add it to the collection
            ModuleRun newModuleRun = new ModuleRun(moduleRunName, moduleName, moduleVersion);
            moduleRunList.Add(moduleRunName, newModuleRun);

            // Create a lookup of module info vs module name 
            ModRunInfo pluginInfo;
            pluginInfo.Name = moduleName;
            pluginInfo .Version = moduleVersion;
            moduleRunInfo.Add(moduleRunName, pluginInfo);

            return newModuleRun;
        }


        public Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead GetDataReader(string label)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead GetDataReader()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead GetLimitReader(string label)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead GetLimitReader()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ModuleRun GetModuleRun(string moduleRunName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSpecificationList(Bookham.TestEngine.Framework.Limits.SpecList specList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void WriteDeviceTraceLog(string userMessage)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region ITestEngineBase Members

        public void ErrorInProgram(string errorDescription)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void GuiCancelUserAttention()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void GuiHide()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void GuiShow()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void GuiToFront()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void GuiUserAttention()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool IsDebug
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool IsSimulation
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public ProgPrivilegeLevel PrivilegeLevel
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public Bookham.TestEngine.Framework.Messages.MsgContainer ReceiveFromGui(int timeout_ms)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.Framework.Messages.MsgContainer ReceiveFromGui()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void ResetProgramStartTime()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SendStatusMsg(string msg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public long SendToGui(object msg, long responseSeqNum)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public long SendToGui(object msg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.Program.ButtonId ShowContinueCancelUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void ShowContinueUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public object ShowUserQuery(string questionText, params Bookham.TestEngine.PluginInterfaces.Program.ButtonInfo[] buttons)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.Program.ButtonId ShowYesNoCancelUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.PluginInterfaces.Program.ButtonId ShowYesNoUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region ITestEngineRun Members

        public Bookham.TestEngine.Framework.Limits.ModRunData GetModuleRunData(string moduleRunName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Bookham.TestEngine.Framework.Limits.MultiSpecStatus GetProgramDataStatus()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ProgramStatus GetProgramRunStatus()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool IsContinueOnFail
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public Bookham.TestEngine.Framework.Limits.MultiSpecStatus ProgramDataStatus
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public ProgramStatus ProgramRunStatus
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void RaiseNonParamFail(int errorCode, string errorDescription)
        {
            throw new Exception(errorDescription);
        }

        public ModuleRunReturn RunModule(string moduleRunName, int maxNumberOfRetries, bool continueOnFail)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ModuleRunReturn RunModule(string moduleRunName, bool continueOnFail)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ModuleRunReturn RunModule(string moduleRunName, int maxNumberOfRetries)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ModuleRunReturn RunModule(string moduleRunName)
        {
            // check inputs
            if ((moduleRunName == null) || (moduleRunName.Length == 0))
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "RunModule: Null or empty moduleRunName");
            }

            // find the module run object
            object modRunObj = moduleRunList[moduleRunName];
            if (modRunObj == null)
            {
                throw new ProgramException(ProgramStatus.ArgumentsInvalid,
                    "RunModule: Specified Module Run doesn't exist: " + moduleRunName);
            }
            ModuleRun modRun = (ModuleRun)modRunObj;

            // Load module
            ITestModule iModRun = TestModuleLoader.LoadModule(moduleRunInfo[moduleRunName].Name , moduleRunInfo[moduleRunName].Version);
            ITestEngine eng = (ITestEngine)this;
            DatumList modData = iModRun.DoTest(eng, ModulePrivilegeLevel.Technician, modRun.ConfigData, modRun.Instrs, modRun.Chassis, modRun.CalData, modRun.PreviousTestData);
            ModRunSpecs modRunSpecs = new ModRunSpecs();
            ModRunData modRunData = new ModRunData(moduleRunName, modRunSpecs, modData);
            ModuleRunReturn moduleRunReturn = new ModuleRunReturn(modRunData,
                new MultiSpecStatus(MultiSpecPassFail.AllPass, MultiSpecComplete.AllComplete),
                new MultiSpecStatus(MultiSpecPassFail.AllPass, MultiSpecComplete.AllComplete));
            return moduleRunReturn;
        }

        public Bookham.TestEngine.Framework.Limits.SpecStatus SpecificationStatus(string specificationName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double TestProcessTime
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region ITestEngineInit Members


        Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead ITestEngineInit.GetDataReader(string label)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead ITestEngineInit.GetDataReader()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead ITestEngineInit.GetLimitReader(string label)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.ExternalData.ILimitRead ITestEngineInit.GetLimitReader()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region ITestEngineBase Members


        Bookham.TestEngine.Framework.Messages.MsgContainer ITestEngineBase.ReceiveFromGui(int timeout_ms)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.Framework.Messages.MsgContainer ITestEngineBase.ReceiveFromGui()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion



        /// <summary>
        /// Hashtable that contains all the ModuleRun data for the current program
        /// </summary>
        Hashtable moduleRunList = new Hashtable();

        /// <summary>
        /// Lookup table between module run (string) and module that was loaded.
        /// </summary>
        Dictionary<string, ModRunInfo> moduleRunInfo = new Dictionary<string,ModRunInfo>();

        internal struct ModRunInfo
        {
            internal string Name;
            internal string Version;
        }

        #region ITestEngine Members

        void ITestEngine.CheckForAbort()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.ErrorInModule(string errorDescription)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        double ITestEngine.GetCurrentModuleTestProcessTime()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.GuiCancelUserAttention()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.GuiHide()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.GuiShow()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.GuiToFront()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.GuiUserAttention()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        bool ITestEngine.IsDebug
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        bool ITestEngine.IsSimulation
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        void ITestEngine.RaiseNonParamFail(int optionalDutErrorCode, string deviceErrorDescription)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.Framework.Messages.MsgContainer ITestEngine.ReceiveFromGui(int timeout_ms)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.Framework.Messages.MsgContainer ITestEngine.ReceiveFromGui()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.SendStatusMsg(string msg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        long ITestEngine.SendToGui(object msg, long responseSeqNum)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        long ITestEngine.SendToGui(object msg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.Module.ButtonId ITestEngine.ShowContinueCancelUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.ShowContinueUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        object ITestEngine.ShowUserQuery(string questionText, params Bookham.TestEngine.PluginInterfaces.Module.ButtonInfo[] buttons)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.Module.ButtonId ITestEngine.ShowYesNoCancelUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        Bookham.TestEngine.PluginInterfaces.Module.ButtonId ITestEngine.ShowYesNoUserQuery(string questionText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.StartTestProcessTimer()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.StopTestProcessTimer()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ITestEngine.WriteDeviceTraceLog(string userMessage)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

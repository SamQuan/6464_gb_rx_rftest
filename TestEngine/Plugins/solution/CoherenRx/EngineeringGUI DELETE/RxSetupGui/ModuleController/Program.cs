using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Text;
using Bookham.TestEngine.Framework.Logging;

namespace LabGUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.WriteLine("Startup");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.Run(new MainForm());
            Environment.Exit(0);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string errorMessage;
            Exception exc = e.ExceptionObject as Exception;
            if (exc != null)
            {
                errorMessage = BuildErrorTrace(exc);
                Log.ErrorWriteFromException(exc, "Fatal Error", Bookham.TestEngine.Framework.Logging.ErrorLogSubtype.UnhandledException);
                MessageBox.Show(errorMessage, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
            else
            {
                errorMessage = e.ToString() + "\r\n" + e.ExceptionObject.ToString();
                Log.ErrorWrite(e.ToString(), ErrorLogSubtype.UnhandledException);
                MessageBox.Show(errorMessage, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            Environment.Exit(0xBADF00D);
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
                MessageBox.Show(BuildErrorTrace(e.Exception), "ThreadException Error");
        }
        
        static void OnGuiUnhandledException(object obj, ThreadExceptionEventArgs args)
        {
            MessageBox.Show(BuildErrorTrace(args.Exception), "GUI Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static string BuildErrorTrace(Exception e)
        {
            errorTrace = new StringBuilder();
            while (e != null)
            {
                GetStackTrace(ref e);
            }
            return errorTrace.ToString();
        }

        static void GetStackTrace(ref Exception e)
        {
            errorTrace.Append(e.Message);
            e = e.InnerException;
        }

        private static StringBuilder errorTrace;
    }
}
// [Copyright]
//
// Bookham TSFF Firmware Download & PCB Calibration
// 
//
// Tsff_CalDAC_Test_Config.cs
// Design: TSFF PCBA Cal Test Modules DD 
// Author: Nick Lee & Joseph Olajubu

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;



namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but not a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct Tsff_CalDAC_Test_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        /// <returns>datumlist</returns>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();

            DatumSint32 DacStartValue = new DatumSint32("DacStartValue", this.DacStartValue);
            DatumSint32 DacStopValue = new DatumSint32("DacStopValue", this.DacStopValue);
            DatumSint32 DacStepSize = new DatumSint32("DacStepSize", this.DacStepSize);

            DatumBool CalculateGrad1Value = new DatumBool("CalculateGrad1Value", this.CalculateGrad1Value);
            DatumBool CalculateGrad2Value = new DatumBool("CalculateGrad2Value", this.CalculateGrad2Value);

            DatumSint32 DacStartValue_Grad1Calc = new DatumSint32("DacStartValue_Grad1Calc", this.DacStartValue_Grad1Calc); 
            DatumSint32 DacStopValue_Grad1Calc  = new DatumSint32("DacStopValue_Grad1Calc", this.DacStopValue_Grad1Calc); 
            DatumSint32 DacStartValue_Grad2Calc = new DatumSint32("DacStartValue_Grad2Calc", this.DacStartValue_Grad2Calc); 
            DatumSint32 DacStopValue_Grad2Calc  = new DatumSint32("DacStopValue_Grad2Calc", this.DacStopValue_Grad2Calc); 
            DatumSint32 DacValForMinOutput = new DatumSint32("DacValForMinOutput",this.DacValForMinOutput);
            DatumSint32 DsdbrFrontSection = new DatumSint32("DsdbrFrontSection", this.DsdbrFrontSection);
            DatumEnum DacChannel = new DatumEnum("DacChannel", this.DacChannel);

            myLocalDatumList.Add(DacStartValue);
            myLocalDatumList.Add(DacStopValue);
            myLocalDatumList.Add(DacStepSize);
            myLocalDatumList.Add(CalculateGrad1Value);
            myLocalDatumList.Add(CalculateGrad2Value);
            myLocalDatumList.Add(DacStartValue_Grad1Calc);
            myLocalDatumList.Add(DacStopValue_Grad1Calc);
            myLocalDatumList.Add(DacStartValue_Grad2Calc);
            myLocalDatumList.Add(DacStopValue_Grad2Calc);
            myLocalDatumList.Add(DacChannel);
            myLocalDatumList.Add(DacValForMinOutput);
            myLocalDatumList.Add(DsdbrFrontSection);


            return (myLocalDatumList);
        }




        /// <summary>
        /// Constructor which takes a datumList and populates our Tsff_CalDAC_Test_Config
        /// </summary>
        /// <param name="mydata"></param>
        public Tsff_CalDAC_Test_Config(DatumList mydata)
        {
            this.DacStartValue = mydata.ReadSint32("DacStartValue");
            this.DacStopValue = mydata.ReadSint32("DacStopValue");
            this.DacStepSize = mydata.ReadSint32("DacStepSize");
            this.CalculateGrad1Value = mydata.ReadBool("CalculateGrad1Value");
            this.CalculateGrad2Value = mydata.ReadBool("CalculateGrad2Value");
            this.DacStartValue_Grad1Calc = mydata.ReadSint32("DacStartValue_Grad1Calc");
            this.DacStopValue_Grad1Calc = mydata.ReadSint32("DacStopValue_Grad1Calc");
            this.DacStartValue_Grad2Calc = mydata.ReadSint32("DacStartValue_Grad2Calc");
            this.DacStopValue_Grad2Calc = mydata.ReadSint32("DacStopValue_Grad2Calc");
            this.DacChannel = (CalDacChannel) mydata.ReadEnum("DacChannel");
            this.DsdbrFrontSection = mydata.ReadSint32("DsdbrFrontSection");
            this.DacValForMinOutput = mydata.ReadSint32("DacValForMinOutput");
        }

        /// <summary>
        /// Killing two birds with one stone, i need to define the caldac channels, but its also
        /// better to set them up with the value of the dau slot / channel position they correspond to
        /// </summary>
        public enum CalDacChannel
        {
            FRONTODD = 301,
            REAR = 302,
            SOA  = 303,
            FRONTEVEN = 304,
            GAIN = 305,
            PHASE = 306,
            RIGHTIMB = 307,
            LEFTIMB = 308,
            MZRIGHTBIAS = 312,
            MZLEFTBIAS = 317
        }

        //List of all my parameters i will use

        /// <summary>
        /// The Dac start value for my sweep
        /// </summary>
        public Int32 DacStartValue;
        /// <summary>
        /// The Dac stop value for my sweep
        /// </summary>
        public Int32 DacStopValue;
        /// <summary>
        /// The DAC step size we wish to apply
        /// </summary>
        public Int32 DacStepSize;
        /// <summary>
        /// A flag to say whether we should calculate gradient 1 value
        /// </summary>
        public bool CalculateGrad1Value;
        /// <summary>
        /// A flag to say whether we should calculate gradient 2 value
        /// </summary>
        public bool CalculateGrad2Value;
        /// <summary>
        /// If we are doing grad1 calculation, this is the start point for the calc
        /// </summary>
        public Int32 DacStartValue_Grad1Calc;
        /// <summary>
        /// If we are doing grad1 calculation, this is the stop point for the calc
        /// </summary>
        public Int32 DacStopValue_Grad1Calc;
        /// <summary>
        /// If we are doing grad2 calculation, this is the start point for the calc
        /// </summary>
        public Int32 DacStartValue_Grad2Calc;
        /// <summary>
        /// If we are doing grad1 calculation, this is the stop point for the calc
        /// </summary>
        public Int32 DacStopValue_Grad2Calc;
        /// <summary>
        /// This is the particular Dac Channel we are trying to measure
        /// </summary>
        public CalDacChannel DacChannel;
        /// <summary>
        /// If we are doing a front section, we can choose the particular front section
        /// we wish to use
        /// </summary>
        public Int32 DsdbrFrontSection;
        /// <summary>
        /// This is the value that we must leave the Dac in, in order to ensure
        /// current/voltage transients are kept to a minimum
        /// </summary>
        public Int32 DacValForMinOutput;
    }
}

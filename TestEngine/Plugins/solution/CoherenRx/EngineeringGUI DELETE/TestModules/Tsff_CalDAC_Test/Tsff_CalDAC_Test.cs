// [Copyright]
//
// Bookham Test Engine Library
// 
//
// Tsff_CalDAC_Test.cs
// Design: PCBA Cal Test Modules DD 
// Author: Nick Lee & Joseph Olajubu
// [Copyright]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using System.Collections;
using Bookham.TestLibrary.ChassisNS;
using System.Diagnostics;
using Bookham.TestLibrary.BlackBoxes;

namespace Bookham.TestSolution.TestModules
{
    public class Tsff_CalDAC_Test : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// This is the main function that does anything.  It will cycle thru the
        /// required dac settings and measure the currents seen.
        /// </summary>
        /// <param name="engine">The test engine</param>
        /// <param name="userType">user</param>
        /// <param name="configData">see CalDAC_Test_config structure</param>
        /// <param name="instruments">CalModule, dauDMM_1</param>
        /// <param name="chassis">Chassis_Ag34970A</param>
        /// <param name="calData">None</param>
        /// <param name="previousTestData">None</param>
        /// <returns>a Datum plot containing our results, and the following Datum doubles SlopeGradient1, YIntercept1, CorrelationCoefficient1, and SlopeGradient2, YIntercept2, CorrelationCoefficient2, </returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            DatumList myTestModuleData = new DatumList();
                        
            Tsff_CalDAC_Test_Config myConfigData = new Tsff_CalDAC_Test_Config(configData);

            //Cast our instrument / unit to IDsdbrLaserSetup 
            IDsdbrLaserSetup moduleAsIDsdbrLaserSetup = (IDsdbrLaserSetup)instruments["CalModule"];

            //after looking at the setMeasurements path, it seems easier to put the 3 lines of code within this test module instead
            Chassis_Ag34970A mySwitchChassis = (Chassis_Ag34970A)chassis["Chassis_Ag34970A"];

            // Chassis Channel number is slot number * 100 plus the channel number
            // Enum value is typically 302 to 306
            Int32 dacChannelValue = (Int32) myConfigData.DacChannel; // eg Gives 302 
            Int32 slotNumber = (dacChannelValue / 100); //gives us 302/100 = 3.02, as (int) therefore = 3
            Int32 channel = dacChannelValue - (slotNumber * 100); //ie  302 - (3*100) = 2

            mySwitchChassis.CloseSwitch(slotNumber, channel, true);

            Inst_Ag34970A_DMM dauDMM_1 = (Inst_Ag34970A_DMM)(instruments["dauDMM"]);
            
            //Let the DMM Autorange.
            dauDMM_1.MeterRange = InstType_MultiMeter.AutoRange;

            if ((myConfigData.DacChannel == Tsff_CalDAC_Test_Config.CalDacChannel.MZLEFTBIAS) ||
                (myConfigData.DacChannel == Tsff_CalDAC_Test_Config.CalDacChannel.MZRIGHTBIAS))
            {
                this.voltageDac = true;
            }

            //Setup the DMM to measure Voltage or Current as appropriate to the DAC Channel.
            if (this.voltageDac)
            {
                dauDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            }
            else
            {
                dauDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            }

            //Lets do some simple checks on Pass Params;
            if (myConfigData.DacStartValue > myConfigData.DacStopValue)    
            {
                engine.ErrorInModule("DacStartValue > DacStopValue");
            }
            if (myConfigData.DacStepSize > (myConfigData.DacStopValue - myConfigData.DacStartValue))
            {
                engine.ErrorInModule("Step size too big to implement");
            }
            if ((myConfigData.DacStartValue < 0) || ((myConfigData.DacStopValue < 0)))
            {
                engine.ErrorInModule("Cannot have dac start value < 0 or dac stop value < 0");
            }

            // Not trying to be too smart here:
            // You have told me the start and stop points and size of steps
            // I will basically start where you tell me, and increment by the amount you tell me.
            // If a step takes me over the stop point i will merely use the stop point instead.
            Int32 dacCounter = myConfigData.DacStartValue;
            Int32 dacStartCount = myConfigData.DacStartValue;
            Int32 dacStopCount = myConfigData.DacStopValue;
            Int32 stepSize = myConfigData.DacStepSize;
            Int32 loopCounter = 0;

            ArrayList dataXValues = new ArrayList();
            ArrayList dataYValues = new ArrayList();

            if ((myConfigData.DacChannel == Tsff_CalDAC_Test_Config.CalDacChannel.FRONTEVEN)||
            (myConfigData.DacChannel == Tsff_CalDAC_Test_Config.CalDacChannel.FRONTODD))
            {
                //Sets control reg front section (ie 1 as Odd, 2 as even
                //Example: if i pass it SetControlRegFrontSection(4) it will set 4 as even and 5 as Odd.
                // Nb you cannot physically set front section 8, you can only set front section 7
                // which will be the odd and front section 8 automatically is set as the even
                if (myConfigData.DsdbrFrontSection == 8)
                {
                    moduleAsIDsdbrLaserSetup.SetFrontSectionPair(7);
                }
                else  //anything not recognised will cause a exception
                {
                    moduleAsIDsdbrLaserSetup.SetFrontSectionPair(myConfigData.DsdbrFrontSection);
                }
            }

            do
            {
                // Adjust Dac by the Dac step size * the loop value
                dacCounter = dacStartCount + (stepSize * loopCounter);

                if (dacCounter > dacStopCount)
                {
                    dacCounter = dacStopCount;
                }

                //Set Dac
                SetDacValue(engine, instruments["CalModule"],  myConfigData.DacChannel, dacCounter);
                
                //give it chance to do the operation before reading the current
                System.Threading.Thread.Sleep(10); //10ms is ideal apparently according to electornics design engineer for TSFF

                //Measure current or voltage
                dataXValues.Add((double)dacCounter);
                double myReading;

                if (this.voltageDac)
                {
                    myReading = dauDMM_1.GetReading(); //Store Reading in Volts
                }
                else
                {
                    myReading = (dauDMM_1.GetReading() * 1000); //Store reading in mA
                }

                if (!this.voltageDac)
                {
                    //Experiencing problems where dsdbr wants 0ma, but we have reading of 0.000002ma
                    if (((myReading > 0) && (myReading < 0.0001)) || 
                        ((myReading < 0) && (myReading > -0.0001)))
                    {
                        myReading = 0;
                    }
                }

                if (this.voltageDac)
                {
                    if ((myReading < 0) && (myReading > -0.005))
                    {
                        myReading = 0;
                    }
                }

                dataYValues.Add(myReading); 
                
                loopCounter++;

            } while (dacCounter < dacStopCount);


            //Remember to set the Dac back to its minimum output setting point
            SetDacValue(engine, instruments["CalModule"],  myConfigData.DacChannel, myConfigData.DacValForMinOutput);
                
            //My DAC values are truly integer.  This is what my CalModule expects
            //However, my Algorithm works with doubles for x values..
            //and PlotAxis also use doubles... 
            //so i WILL put my x axis into double array
            //and handle the conversion back to int when i do a save to file
            double[] xValuesDoubleArray = (double[])dataXValues.ToArray(typeof(double)); 
            double[] yValuesArray = (double[])dataYValues.ToArray(typeof(double));

            //Ok, so now i have my arrays of data.
            //Next... need to work out the linear gradients for the regions i have been asked for:
            if (myConfigData.CalculateGrad1Value)
            {
                //Need to find the array index points of our calc values
                BoundingIndices boundingIndicesX1start= BoundingIndicesAlgorithm.Calculate(xValuesDoubleArray,myConfigData.DacStartValue_Grad1Calc);
                BoundingIndices boundingIndicesX1stop= BoundingIndicesAlgorithm.Calculate(xValuesDoubleArray,myConfigData.DacStopValue_Grad1Calc);

                LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate(xValuesDoubleArray, yValuesArray, boundingIndicesX1start.IndexOfArrayValueGreaterThan, boundingIndicesX1stop.IndexOfArrayValueLessThan );
                DatumDouble SlopeGradient1 = new DatumDouble("SlopeGradient1", linLeastSqFit.Slope * 1000); //stored as gradient per 1000 Dac steps
                DatumDouble YIntercept1 = new DatumDouble("YIntercept1", linLeastSqFit.YIntercept);
                DatumDouble CorrelationCoefficient1 = new DatumDouble("CorrelationCoefficient1", linLeastSqFit.CorrelationCoefficient);

                myTestModuleData.Add(SlopeGradient1); 
                myTestModuleData.Add(YIntercept1);
                myTestModuleData.Add(CorrelationCoefficient1);
            }

            //For the SOA, where it may be V shaped, its worthwhile splitting the things out
            //into 2 seperate checks
            if (myConfigData.CalculateGrad2Value)
            {
                //Need to find the array index points of our calc values
                BoundingIndices boundingIndicesX2start = BoundingIndicesAlgorithm.Calculate(xValuesDoubleArray, myConfigData.DacStartValue_Grad2Calc);
                BoundingIndices boundingIndicesX2stop = BoundingIndicesAlgorithm.Calculate(xValuesDoubleArray, myConfigData.DacStopValue_Grad2Calc);

                LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate(xValuesDoubleArray, yValuesArray, boundingIndicesX2start.IndexOfArrayValueGreaterThan, boundingIndicesX2stop.IndexOfArrayValueLessThan);

                DatumDouble SlopeGradient2 = new DatumDouble("SlopeGradient2", linLeastSqFit.Slope * 1000); //stored as gradient per 1000 Dac steps
                DatumDouble YIntercept2 = new DatumDouble("YIntercept2", linLeastSqFit.YIntercept);
                DatumDouble CorrelationCoefficient2 = new DatumDouble("CorrelationCoefficient2", linLeastSqFit.CorrelationCoefficient);

                myTestModuleData.Add(SlopeGradient2); 
                myTestModuleData.Add(YIntercept2);
                myTestModuleData.Add(CorrelationCoefficient2);
    
            }
            
            // Need to pass out the X and Y data somehow
            PlotAxis plotXAxis = new PlotAxis("DacValue","CamoflagedInt32",xValuesDoubleArray);
            PlotAxis plotYAxis;

            if (this.voltageDac)
            {
                plotYAxis = new PlotAxis("Readings", "V", yValuesArray);
            }
            else
            {
                plotYAxis = new PlotAxis("Readings", "mA", yValuesArray);
            }
            DatumPlot dacDataPlot = new DatumPlot(myConfigData.DacChannel.ToString(), plotXAxis,plotYAxis);
            myTestModuleData.Add(dacDataPlot);

            return (myTestModuleData);
        }

        /// <summary>
        /// Not using this in this test module at the moment
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }


        /// <summary>
        /// Sets the relevent Dac channel up
        /// </summary>
        /// <param name="engine">needed for error handling</param>
        /// <param name="module">our module we are talking to</param>
        /// <param name="tcmzGoldBox">True if the PCB contains DACs to control a TCMZ Goldbox</param>
        /// <param name="myDacChannel">The particular Dac channel, front rear soa etc</param>
        /// <param name="dacValue">The integer value we wish the Dac set to</param>
        private void SetDacValue(ITestEngine engine, Instrument module, Tsff_CalDAC_Test_Config.CalDacChannel myDacChannel, Int32 dacValue)
        {
            IDsdbrLaserSetup moduleAsIDsdbrLaserSetup = (IDsdbrLaserSetup)module;
            switch (myDacChannel)
            {
                case Tsff_CalDAC_Test_Config.CalDacChannel.FRONTEVEN:
                    {
                        moduleAsIDsdbrLaserSetup.SetFrontEvenDAC(dacValue);
                        break;
                    }
                case Tsff_CalDAC_Test_Config.CalDacChannel.FRONTODD:
                    {
                        moduleAsIDsdbrLaserSetup.SetFrontOddDAC(dacValue);  
                        break;
                    }
                case Tsff_CalDAC_Test_Config.CalDacChannel.GAIN:
                    {
                        moduleAsIDsdbrLaserSetup.SetGainDAC(dacValue);
                        break;
                    }
                case Tsff_CalDAC_Test_Config.CalDacChannel.PHASE:
                    {
                        moduleAsIDsdbrLaserSetup.SetPhaseDAC(dacValue);
                        break;
                    }
                case Tsff_CalDAC_Test_Config.CalDacChannel.REAR:
                    {
                        moduleAsIDsdbrLaserSetup.SetRearDAC(dacValue);
                        break;
                    }
                case Tsff_CalDAC_Test_Config.CalDacChannel.SOA:
                    {
                        moduleAsIDsdbrLaserSetup.SetSOADAC(dacValue);
                        break;
                    }

                case Tsff_CalDAC_Test_Config.CalDacChannel.LEFTIMB:
                    {
                        //Instr_TSFF tsffPcb = (Instr_TSFF)module;  // No No No !!!
                        ITcmzLaserSetup tsffPcb = (ITcmzLaserSetup)module;
                        tsffPcb.SetMzLeftImbalDac(dacValue);
                    
                        break;
                    }

                case Tsff_CalDAC_Test_Config.CalDacChannel.RIGHTIMB:
                    {
                        //Instr_TSFF tsffPcb = (Instr_TSFF)module;  // No No No !!!
                        ITcmzLaserSetup tsffPcb = (ITcmzLaserSetup)module; 
                        tsffPcb.SetMzRightImbalDac(dacValue);
 
                        break;
                    }

                case Tsff_CalDAC_Test_Config.CalDacChannel.MZLEFTBIAS:
                    {
                        //Instr_TSFF tsffPcb = (Instr_TSFF)module;  // No No No !!!
                        ITcmzLaserSetup tsffPcb = (ITcmzLaserSetup)module; 
                        tsffPcb.SetMzLeftBiasDac(dacValue);

                        break;
                    }

                case Tsff_CalDAC_Test_Config.CalDacChannel.MZRIGHTBIAS:
                    {
                        //Instr_TSFF tsffPcb = (Instr_TSFF)module;  // No No No !!!
                        ITcmzLaserSetup tsffPcb = (ITcmzLaserSetup)module; 
                        tsffPcb.SetMzRightBiasDac(dacValue);

                        break;
                    }

                default:
                    {
                        engine.ErrorInModule("CalDAC_Test_Config.CalDacChannel" + myDacChannel + " not recognised");
                        break;
                    }
            }        
        }
        #endregion

        #region Private Data
        //True if current DAC Channel is a Voltage DAC.
        private bool voltageDac;
        #endregion
    }

    
}

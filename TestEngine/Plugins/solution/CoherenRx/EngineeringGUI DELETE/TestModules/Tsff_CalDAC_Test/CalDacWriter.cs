// [Copyright]
//
// Bookham Test Engine Library
// 
//
// CalDacWriter.cs
// Design: PCBA Cal Test Modules DD 
// Author: Nick Lee & Joseph Olajubu
// [Copyright]

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;

namespace Bookham.TestSolution.TestPrograms.TunableModules
{
    /// <summary>
    /// Simple CsvWriter class for dealing with my specific requirements. 
    /// </summary>
    /// <remarks>This class is disposable, if disposed will close the file</remarks>
    public class CalDacWriter : IDisposable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CalDacWriter(ITestEngineRun engine)
        {
            streamWriter = null;
            engine2 = engine;
        }

        /// <summary>
        /// Open file
        /// </summary>
        /// <param name="csvFile">File name to create</param>
        public void CreateFile(string csvFile)
        {
            //I am telling it to overwrite the file if it exists already.(not appending)
            streamWriter = new StreamWriter(@csvFile, false);
        }


        /// <summary>
        /// Close file
        /// </summary>
        public void CloseFile()
        {
            if (streamWriter == null)
            {
                engine2.ErrorInProgram("Can't close the CsvWriter file, none open!");
            }

            this.Dispose();
        }



        /// <summary>
        /// A function to write out a csv table from a string array
        /// </summary>
        /// <param name="myArray"></param>
        public void WriteCsvTable(string[,] myArray)
        {
            Int32 rows = myArray.GetUpperBound(0) + 1;
            Int32 columns = myArray.GetUpperBound(1) + 1;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    streamWriter.Write(myArray[i, j]);
                    //Last number in row does not need a comma
                    if (j != columns - 1)
                    {
                        streamWriter.Write(",");
                    }
                }
                //Last row of data does not need a carriage return after it
                if (i != rows - 1)
                {
                    streamWriter.Write("\n");
                }
            }
        }

        #region IDisposable Members

        /// <summary>
        /// IDisposable method
        /// </summary>
        public void Dispose()
        {
            if (streamWriter != null)
            {
                streamWriter.Dispose();
                streamWriter = null;
            }
        }

        #endregion

        #region Private Data
        private StreamWriter streamWriter;
        private ITestEngineRun engine2;
        #endregion


    }
}

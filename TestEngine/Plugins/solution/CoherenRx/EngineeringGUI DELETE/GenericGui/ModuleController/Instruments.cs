using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using System.IO;
using Bookham.TestLibrary.Utilities;

namespace LabGUI
{
    /// <summary>
    /// Handles the communication with the instrumentation.
    /// </summary>
    static class Instruments
    {        
        /// <summary>
        /// Setup up logging and instantiate the instruments.
        /// </summary>
        internal static void Initialise(string ApplicationName)
        {
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);

            try
            {
                InstrumentLoader.Initialise(ApplicationName);
            }
            catch (Bookham.TestEngine.Framework.PluginLoader.PluginInstantiatingFailException pif)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("\r\n" + pif.Message);

                errorMessage.Append(Program.BuildErrorTrace(pif.InnerException));

                throw new SystemException("Error loading instrument driver :\r\n" + errorMessage );
            }


            //
            // Assign instruments here :
            //
            chassis = InstrumentLoader.ChassisList["BlackBoxChassis"];
            instrument = InstrumentLoader.InstrumentList["BlackBox"];

            // Check whether the module supports the dsdbr interface
            dsdbrLaser = instrument as IDsdbrLaserSetup;
            if (dsdbrLaser == null)
            {
                //throw;
            }
            instrument.Timeout_ms = 1000;
            chassis.Timeout_ms = 1000;
            chassis.EnableLogging = true;
            

            if (chassis.IsOnline)
            {
                try
                {
                    dsdbrLaser.EnterLaserSetupMode(); // Send password
                }
                catch (Exception)
                {
                    System.Windows.Forms.MessageBox.Show("Error communicating with DUT. Please check power and connections","Communication error");
                }
                
            }
        }


        /// <summary>
        /// Black box laser driver
        /// </summary>
        private static IDsdbrLaserSetup dsdbrLaser;
        /// <summary>
        /// Black box single MZ driver
        /// </summary>
        private static ITcmzLaserSetup tcmzLaser;

        private static Chassis chassis;
        private static Instrument instrument; 
    }
}

namespace LabGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider1 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider2 = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider3 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider4 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider5 = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider6 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider7 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider8 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider9 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider10 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider11 = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.scrollboxAndSlider12 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider13 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider14 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider15 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider16 = new ModuleController.UserControls.scrollboxAndSlider();
            this.scrollboxAndSlider17 = new ModuleController.UserControls.scrollboxAndSlider();
            this.btnAutoSetup = new System.Windows.Forms.Button();
            this.btnRxSetup = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(742, 471);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnRxSetup);
            this.tabPage1.Controls.Add(this.btnAutoSetup);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(734, 445);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Start";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(683, 393);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 501);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(742, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(38, 17);
            this.toolStripStatusLabel1.Text = "Ready";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(742, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.scrollboxAndSlider2);
            this.groupBox1.Controls.Add(this.scrollboxAndSlider1);
            this.groupBox1.Location = new System.Drawing.Point(271, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 93);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // scrollboxAndSlider1
            // 
            this.scrollboxAndSlider1.Location = new System.Drawing.Point(8, 19);
            this.scrollboxAndSlider1.Name = "scrollboxAndSlider1";
            this.scrollboxAndSlider1.Size = new System.Drawing.Size(226, 31);
            this.scrollboxAndSlider1.TabIndex = 0;
            this.scrollboxAndSlider1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider2
            // 
            this.scrollboxAndSlider2.Location = new System.Drawing.Point(8, 55);
            this.scrollboxAndSlider2.Name = "scrollboxAndSlider2";
            this.scrollboxAndSlider2.Size = new System.Drawing.Size(226, 29);
            this.scrollboxAndSlider2.TabIndex = 1;
            this.scrollboxAndSlider2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scrollboxAndSlider5);
            this.groupBox2.Controls.Add(this.scrollboxAndSlider4);
            this.groupBox2.Controls.Add(this.scrollboxAndSlider3);
            this.groupBox2.Location = new System.Drawing.Point(25, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 126);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // scrollboxAndSlider3
            // 
            this.scrollboxAndSlider3.Location = new System.Drawing.Point(6, 19);
            this.scrollboxAndSlider3.Name = "scrollboxAndSlider3";
            this.scrollboxAndSlider3.Size = new System.Drawing.Size(223, 31);
            this.scrollboxAndSlider3.TabIndex = 0;
            this.scrollboxAndSlider3.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider4
            // 
            this.scrollboxAndSlider4.Location = new System.Drawing.Point(6, 56);
            this.scrollboxAndSlider4.Name = "scrollboxAndSlider4";
            this.scrollboxAndSlider4.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider4.TabIndex = 1;
            this.scrollboxAndSlider4.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider5
            // 
            this.scrollboxAndSlider5.Location = new System.Drawing.Point(6, 90);
            this.scrollboxAndSlider5.Name = "scrollboxAndSlider5";
            this.scrollboxAndSlider5.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider5.TabIndex = 2;
            this.scrollboxAndSlider5.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.scrollboxAndSlider11);
            this.groupBox3.Controls.Add(this.scrollboxAndSlider10);
            this.groupBox3.Controls.Add(this.scrollboxAndSlider9);
            this.groupBox3.Controls.Add(this.scrollboxAndSlider6);
            this.groupBox3.Controls.Add(this.scrollboxAndSlider7);
            this.groupBox3.Controls.Add(this.scrollboxAndSlider8);
            this.groupBox3.Location = new System.Drawing.Point(25, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 229);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // scrollboxAndSlider6
            // 
            this.scrollboxAndSlider6.Location = new System.Drawing.Point(6, 91);
            this.scrollboxAndSlider6.Name = "scrollboxAndSlider6";
            this.scrollboxAndSlider6.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider6.TabIndex = 2;
            this.scrollboxAndSlider6.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider7
            // 
            this.scrollboxAndSlider7.Location = new System.Drawing.Point(6, 56);
            this.scrollboxAndSlider7.Name = "scrollboxAndSlider7";
            this.scrollboxAndSlider7.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider7.TabIndex = 1;
            this.scrollboxAndSlider7.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider8
            // 
            this.scrollboxAndSlider8.Location = new System.Drawing.Point(6, 19);
            this.scrollboxAndSlider8.Name = "scrollboxAndSlider8";
            this.scrollboxAndSlider8.Size = new System.Drawing.Size(223, 32);
            this.scrollboxAndSlider8.TabIndex = 0;
            this.scrollboxAndSlider8.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider9
            // 
            this.scrollboxAndSlider9.Location = new System.Drawing.Point(6, 125);
            this.scrollboxAndSlider9.Name = "scrollboxAndSlider9";
            this.scrollboxAndSlider9.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider9.TabIndex = 3;
            this.scrollboxAndSlider9.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider10
            // 
            this.scrollboxAndSlider10.Location = new System.Drawing.Point(6, 159);
            this.scrollboxAndSlider10.Name = "scrollboxAndSlider10";
            this.scrollboxAndSlider10.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider10.TabIndex = 4;
            this.scrollboxAndSlider10.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider11
            // 
            this.scrollboxAndSlider11.Location = new System.Drawing.Point(6, 193);
            this.scrollboxAndSlider11.Name = "scrollboxAndSlider11";
            this.scrollboxAndSlider11.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider11.TabIndex = 5;
            this.scrollboxAndSlider11.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.scrollboxAndSlider12);
            this.groupBox4.Controls.Add(this.scrollboxAndSlider13);
            this.groupBox4.Controls.Add(this.scrollboxAndSlider14);
            this.groupBox4.Controls.Add(this.scrollboxAndSlider15);
            this.groupBox4.Controls.Add(this.scrollboxAndSlider16);
            this.groupBox4.Controls.Add(this.scrollboxAndSlider17);
            this.groupBox4.Location = new System.Drawing.Point(271, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 229);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // scrollboxAndSlider12
            // 
            this.scrollboxAndSlider12.Location = new System.Drawing.Point(6, 193);
            this.scrollboxAndSlider12.Name = "scrollboxAndSlider12";
            this.scrollboxAndSlider12.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider12.TabIndex = 5;
            this.scrollboxAndSlider12.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider13
            // 
            this.scrollboxAndSlider13.Location = new System.Drawing.Point(6, 159);
            this.scrollboxAndSlider13.Name = "scrollboxAndSlider13";
            this.scrollboxAndSlider13.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider13.TabIndex = 4;
            this.scrollboxAndSlider13.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider14
            // 
            this.scrollboxAndSlider14.Location = new System.Drawing.Point(6, 125);
            this.scrollboxAndSlider14.Name = "scrollboxAndSlider14";
            this.scrollboxAndSlider14.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider14.TabIndex = 3;
            this.scrollboxAndSlider14.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider15
            // 
            this.scrollboxAndSlider15.Location = new System.Drawing.Point(6, 91);
            this.scrollboxAndSlider15.Name = "scrollboxAndSlider15";
            this.scrollboxAndSlider15.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider15.TabIndex = 2;
            this.scrollboxAndSlider15.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider16
            // 
            this.scrollboxAndSlider16.Location = new System.Drawing.Point(6, 56);
            this.scrollboxAndSlider16.Name = "scrollboxAndSlider16";
            this.scrollboxAndSlider16.Size = new System.Drawing.Size(223, 28);
            this.scrollboxAndSlider16.TabIndex = 1;
            this.scrollboxAndSlider16.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // scrollboxAndSlider17
            // 
            this.scrollboxAndSlider17.Location = new System.Drawing.Point(6, 19);
            this.scrollboxAndSlider17.Name = "scrollboxAndSlider17";
            this.scrollboxAndSlider17.Size = new System.Drawing.Size(223, 32);
            this.scrollboxAndSlider17.TabIndex = 0;
            this.scrollboxAndSlider17.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnAutoSetup
            // 
            this.btnAutoSetup.Location = new System.Drawing.Point(527, 20);
            this.btnAutoSetup.Name = "btnAutoSetup";
            this.btnAutoSetup.Size = new System.Drawing.Size(91, 28);
            this.btnAutoSetup.TabIndex = 4;
            this.btnAutoSetup.Text = "Auto Setup";
            this.btnAutoSetup.UseVisualStyleBackColor = true;
            // 
            // btnRxSetup
            // 
            this.btnRxSetup.Location = new System.Drawing.Point(527, 54);
            this.btnRxSetup.Name = "btnRxSetup";
            this.btnRxSetup.Size = new System.Drawing.Size(91, 28);
            this.btnRxSetup.TabIndex = 5;
            this.btnRxSetup.Text = "Rx Setup";
            this.btnRxSetup.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 523);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider5;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider4;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider3;
        private System.Windows.Forms.GroupBox groupBox1;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider2;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider1;
        private System.Windows.Forms.Button btnRxSetup;
        private System.Windows.Forms.Button btnAutoSetup;
        private System.Windows.Forms.GroupBox groupBox4;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider12;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider13;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider14;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider15;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider16;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider17;
        private System.Windows.Forms.GroupBox groupBox3;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider11;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider10;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider9;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider6;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider7;
        private ModuleController.UserControls.scrollboxAndSlider scrollboxAndSlider8;
    }
}
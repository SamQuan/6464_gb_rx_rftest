using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.PluginLoader;

namespace LabGUI
{
    public static class TestModuleLoader
    {

        public static void Initialise()
        {
            // None
        }

        public static ITestModule LoadModule(string moduleName, string moduleVersion)
        {
            
            string[] pluginSearchDirs = { "." };
            try
            {
                Loader.AddPluginType(moduleName, typeof(ITestModule), pluginSearchDirs);
            }
            catch ( System.ArgumentException )
            {
                // ignore "Plugin already loaded"
            }

            // Load the plugin
            PluginInfo pluginInfo = null;
            try
            {
                pluginInfo = Loader.LoadPlugin(moduleName, moduleName, moduleVersion);
            }
            catch (PluginNotFoundException ex)
            {
                throw new Exception("Unable to find test module: " + moduleName, ex);
            }

            // get an instance of the plugin
            ITestModule moduleObj = InstantiateModule(moduleName, moduleName, moduleVersion);
            return moduleObj;
        }


        private static ITestModule InstantiateModule(string familyName, string moduleName, string version)
        {
            PluginInfo pluginInfo = null;
            pluginInfo = Loader.LoadPlugin(familyName, moduleName, version);
            // get an instance of the plugin
            return (ITestModule)pluginInfo.Instantiate();
        }
    }
}

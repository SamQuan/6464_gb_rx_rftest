namespace Bookham.Toolkit.CloseGrid
{
    partial class DSDBRSectionsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GainCurrent = new System.Windows.Forms.NumericUpDown();
            this.SoaCurrent = new System.Windows.Forms.NumericUpDown();
            this.GainEnabled = new System.Windows.Forms.CheckBox();
            this.SoaEnabled = new System.Windows.Forms.CheckBox();
            this.RearCurrent = new System.Windows.Forms.NumericUpDown();
            this.RearEnabled = new System.Windows.Forms.CheckBox();
            this.PhaseCurrent = new System.Windows.Forms.NumericUpDown();
            this.PhaseEnabled = new System.Windows.Forms.CheckBox();
            this.Front1Current = new System.Windows.Forms.NumericUpDown();
            this.Front1Enabled = new System.Windows.Forms.CheckBox();
            this.Front2Current = new System.Windows.Forms.NumericUpDown();
            this.Front2Enabled = new System.Windows.Forms.CheckBox();
            this.Front3Current = new System.Windows.Forms.NumericUpDown();
            this.Front3Enabled = new System.Windows.Forms.CheckBox();
            this.Front4Current = new System.Windows.Forms.NumericUpDown();
            this.Front4Enabled = new System.Windows.Forms.CheckBox();
            this.Front5Current = new System.Windows.Forms.NumericUpDown();
            this.Front5Enabled = new System.Windows.Forms.CheckBox();
            this.Front6Current = new System.Windows.Forms.NumericUpDown();
            this.Front6Enabled = new System.Windows.Forms.CheckBox();
            this.Front7Current = new System.Windows.Forms.NumericUpDown();
            this.Front7Enabled = new System.Windows.Forms.CheckBox();
            this.Front8Current = new System.Windows.Forms.NumericUpDown();
            this.Front8Enabled = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.GainCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoaCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RearCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhaseCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front1Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front2Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front3Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front4Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front5Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front6Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front7Current)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front8Current)).BeginInit();
            this.SuspendLayout();
            // 
            // GainCurrent
            // 
            this.GainCurrent.DecimalPlaces = 2;
            this.GainCurrent.Location = new System.Drawing.Point(66, 7);
            this.GainCurrent.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.GainCurrent.Name = "GainCurrent";
            this.GainCurrent.Size = new System.Drawing.Size(120, 20);
            this.GainCurrent.TabIndex = 0;
            // 
            // SoaCurrent
            // 
            this.SoaCurrent.DecimalPlaces = 2;
            this.SoaCurrent.Location = new System.Drawing.Point(66, 33);
            this.SoaCurrent.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.SoaCurrent.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.SoaCurrent.Name = "SoaCurrent";
            this.SoaCurrent.Size = new System.Drawing.Size(120, 20);
            this.SoaCurrent.TabIndex = 2;
            // 
            // GainEnabled
            // 
            this.GainEnabled.AutoSize = true;
            this.GainEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.GainEnabled.Location = new System.Drawing.Point(12, 8);
            this.GainEnabled.Name = "GainEnabled";
            this.GainEnabled.Size = new System.Drawing.Size(48, 17);
            this.GainEnabled.TabIndex = 4;
            this.GainEnabled.Text = "Gain";
            this.GainEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.GainEnabled.UseVisualStyleBackColor = true;
            // 
            // SoaEnabled
            // 
            this.SoaEnabled.AutoSize = true;
            this.SoaEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SoaEnabled.Location = new System.Drawing.Point(12, 34);
            this.SoaEnabled.Name = "SoaEnabled";
            this.SoaEnabled.Size = new System.Drawing.Size(48, 17);
            this.SoaEnabled.TabIndex = 4;
            this.SoaEnabled.Text = "SOA";
            this.SoaEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SoaEnabled.UseVisualStyleBackColor = true;
            // 
            // RearCurrent
            // 
            this.RearCurrent.DecimalPlaces = 2;
            this.RearCurrent.Location = new System.Drawing.Point(66, 59);
            this.RearCurrent.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.RearCurrent.Name = "RearCurrent";
            this.RearCurrent.Size = new System.Drawing.Size(120, 20);
            this.RearCurrent.TabIndex = 2;
            // 
            // RearEnabled
            // 
            this.RearEnabled.AutoSize = true;
            this.RearEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RearEnabled.Location = new System.Drawing.Point(11, 60);
            this.RearEnabled.Name = "RearEnabled";
            this.RearEnabled.Size = new System.Drawing.Size(49, 17);
            this.RearEnabled.TabIndex = 4;
            this.RearEnabled.Text = "Rear";
            this.RearEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RearEnabled.UseVisualStyleBackColor = true;
            // 
            // PhaseCurrent
            // 
            this.PhaseCurrent.DecimalPlaces = 2;
            this.PhaseCurrent.Location = new System.Drawing.Point(66, 85);
            this.PhaseCurrent.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.PhaseCurrent.Name = "PhaseCurrent";
            this.PhaseCurrent.Size = new System.Drawing.Size(120, 20);
            this.PhaseCurrent.TabIndex = 2;
            // 
            // PhaseEnabled
            // 
            this.PhaseEnabled.AutoSize = true;
            this.PhaseEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PhaseEnabled.Location = new System.Drawing.Point(4, 86);
            this.PhaseEnabled.Name = "PhaseEnabled";
            this.PhaseEnabled.Size = new System.Drawing.Size(56, 17);
            this.PhaseEnabled.TabIndex = 4;
            this.PhaseEnabled.Text = "Phase";
            this.PhaseEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PhaseEnabled.UseVisualStyleBackColor = true;
            // 
            // Front1Current
            // 
            this.Front1Current.DecimalPlaces = 2;
            this.Front1Current.Location = new System.Drawing.Point(66, 111);
            this.Front1Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front1Current.Name = "Front1Current";
            this.Front1Current.Size = new System.Drawing.Size(120, 20);
            this.Front1Current.TabIndex = 2;
            // 
            // Front1Enabled
            // 
            this.Front1Enabled.AutoSize = true;
            this.Front1Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front1Enabled.Location = new System.Drawing.Point(4, 112);
            this.Front1Enabled.Name = "Front1Enabled";
            this.Front1Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front1Enabled.TabIndex = 4;
            this.Front1Enabled.Text = "Front1";
            this.Front1Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front1Enabled.UseVisualStyleBackColor = true;
            // 
            // Front2Current
            // 
            this.Front2Current.DecimalPlaces = 2;
            this.Front2Current.Location = new System.Drawing.Point(66, 137);
            this.Front2Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front2Current.Name = "Front2Current";
            this.Front2Current.Size = new System.Drawing.Size(120, 20);
            this.Front2Current.TabIndex = 2;
            // 
            // Front2Enabled
            // 
            this.Front2Enabled.AutoSize = true;
            this.Front2Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front2Enabled.Location = new System.Drawing.Point(4, 138);
            this.Front2Enabled.Name = "Front2Enabled";
            this.Front2Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front2Enabled.TabIndex = 4;
            this.Front2Enabled.Text = "Front2";
            this.Front2Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front2Enabled.UseVisualStyleBackColor = true;
            // 
            // Front3Current
            // 
            this.Front3Current.DecimalPlaces = 2;
            this.Front3Current.Location = new System.Drawing.Point(66, 163);
            this.Front3Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front3Current.Name = "Front3Current";
            this.Front3Current.Size = new System.Drawing.Size(120, 20);
            this.Front3Current.TabIndex = 2;
            // 
            // Front3Enabled
            // 
            this.Front3Enabled.AutoSize = true;
            this.Front3Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front3Enabled.Location = new System.Drawing.Point(4, 164);
            this.Front3Enabled.Name = "Front3Enabled";
            this.Front3Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front3Enabled.TabIndex = 4;
            this.Front3Enabled.Text = "Front3";
            this.Front3Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front3Enabled.UseVisualStyleBackColor = true;
            // 
            // Front4Current
            // 
            this.Front4Current.DecimalPlaces = 2;
            this.Front4Current.Location = new System.Drawing.Point(66, 189);
            this.Front4Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front4Current.Name = "Front4Current";
            this.Front4Current.Size = new System.Drawing.Size(120, 20);
            this.Front4Current.TabIndex = 2;
            // 
            // Front4Enabled
            // 
            this.Front4Enabled.AutoSize = true;
            this.Front4Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front4Enabled.Location = new System.Drawing.Point(4, 190);
            this.Front4Enabled.Name = "Front4Enabled";
            this.Front4Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front4Enabled.TabIndex = 4;
            this.Front4Enabled.Text = "Front4";
            this.Front4Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front4Enabled.UseVisualStyleBackColor = true;
            // 
            // Front5Current
            // 
            this.Front5Current.DecimalPlaces = 2;
            this.Front5Current.Location = new System.Drawing.Point(66, 215);
            this.Front5Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front5Current.Name = "Front5Current";
            this.Front5Current.Size = new System.Drawing.Size(120, 20);
            this.Front5Current.TabIndex = 2;
            // 
            // Front5Enabled
            // 
            this.Front5Enabled.AutoSize = true;
            this.Front5Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front5Enabled.Location = new System.Drawing.Point(4, 216);
            this.Front5Enabled.Name = "Front5Enabled";
            this.Front5Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front5Enabled.TabIndex = 4;
            this.Front5Enabled.Text = "Front5";
            this.Front5Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front5Enabled.UseVisualStyleBackColor = true;
            // 
            // Front6Current
            // 
            this.Front6Current.DecimalPlaces = 2;
            this.Front6Current.Location = new System.Drawing.Point(66, 241);
            this.Front6Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front6Current.Name = "Front6Current";
            this.Front6Current.Size = new System.Drawing.Size(120, 20);
            this.Front6Current.TabIndex = 2;
            // 
            // Front6Enabled
            // 
            this.Front6Enabled.AutoSize = true;
            this.Front6Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front6Enabled.Location = new System.Drawing.Point(4, 242);
            this.Front6Enabled.Name = "Front6Enabled";
            this.Front6Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front6Enabled.TabIndex = 4;
            this.Front6Enabled.Text = "Front6";
            this.Front6Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front6Enabled.UseVisualStyleBackColor = true;
            // 
            // Front7Current
            // 
            this.Front7Current.DecimalPlaces = 2;
            this.Front7Current.Location = new System.Drawing.Point(66, 267);
            this.Front7Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front7Current.Name = "Front7Current";
            this.Front7Current.Size = new System.Drawing.Size(120, 20);
            this.Front7Current.TabIndex = 2;
            // 
            // Front7Enabled
            // 
            this.Front7Enabled.AutoSize = true;
            this.Front7Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front7Enabled.Location = new System.Drawing.Point(4, 268);
            this.Front7Enabled.Name = "Front7Enabled";
            this.Front7Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front7Enabled.TabIndex = 4;
            this.Front7Enabled.Text = "Front7";
            this.Front7Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front7Enabled.UseVisualStyleBackColor = true;
            // 
            // Front8Current
            // 
            this.Front8Current.DecimalPlaces = 2;
            this.Front8Current.Location = new System.Drawing.Point(66, 291);
            this.Front8Current.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Front8Current.Name = "Front8Current";
            this.Front8Current.Size = new System.Drawing.Size(120, 20);
            this.Front8Current.TabIndex = 2;
            // 
            // Front8Enabled
            // 
            this.Front8Enabled.AutoSize = true;
            this.Front8Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Front8Enabled.Location = new System.Drawing.Point(4, 292);
            this.Front8Enabled.Name = "Front8Enabled";
            this.Front8Enabled.Size = new System.Drawing.Size(56, 17);
            this.Front8Enabled.TabIndex = 4;
            this.Front8Enabled.Text = "Front8";
            this.Front8Enabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Front8Enabled.UseVisualStyleBackColor = true;
            // 
            // DSDBRSectionsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Front8Enabled);
            this.Controls.Add(this.Front7Enabled);
            this.Controls.Add(this.Front6Enabled);
            this.Controls.Add(this.Front5Enabled);
            this.Controls.Add(this.Front4Enabled);
            this.Controls.Add(this.Front3Enabled);
            this.Controls.Add(this.Front2Enabled);
            this.Controls.Add(this.Front1Enabled);
            this.Controls.Add(this.PhaseEnabled);
            this.Controls.Add(this.RearEnabled);
            this.Controls.Add(this.SoaEnabled);
            this.Controls.Add(this.GainEnabled);
            this.Controls.Add(this.Front8Current);
            this.Controls.Add(this.Front7Current);
            this.Controls.Add(this.Front6Current);
            this.Controls.Add(this.Front5Current);
            this.Controls.Add(this.Front4Current);
            this.Controls.Add(this.Front3Current);
            this.Controls.Add(this.Front2Current);
            this.Controls.Add(this.Front1Current);
            this.Controls.Add(this.PhaseCurrent);
            this.Controls.Add(this.RearCurrent);
            this.Controls.Add(this.SoaCurrent);
            this.Controls.Add(this.GainCurrent);
            this.Name = "DSDBRSectionsCtrl";
            this.Size = new System.Drawing.Size(199, 321);
            ((System.ComponentModel.ISupportInitialize)(this.GainCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoaCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RearCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhaseCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front1Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front2Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front3Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front4Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front5Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front6Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front7Current)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Front8Current)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown GainCurrent;
        private System.Windows.Forms.NumericUpDown SoaCurrent;
        private System.Windows.Forms.CheckBox GainEnabled;
        private System.Windows.Forms.CheckBox SoaEnabled;
        private System.Windows.Forms.NumericUpDown RearCurrent;
        private System.Windows.Forms.CheckBox RearEnabled;
        private System.Windows.Forms.NumericUpDown PhaseCurrent;
        private System.Windows.Forms.CheckBox PhaseEnabled;
        private System.Windows.Forms.NumericUpDown Front1Current;
        private System.Windows.Forms.CheckBox Front1Enabled;
        private System.Windows.Forms.NumericUpDown Front2Current;
        private System.Windows.Forms.CheckBox Front2Enabled;
        private System.Windows.Forms.NumericUpDown Front3Current;
        private System.Windows.Forms.CheckBox Front3Enabled;
        private System.Windows.Forms.NumericUpDown Front4Current;
        private System.Windows.Forms.CheckBox Front4Enabled;
        private System.Windows.Forms.NumericUpDown Front5Current;
        private System.Windows.Forms.CheckBox Front5Enabled;
        private System.Windows.Forms.NumericUpDown Front6Current;
        private System.Windows.Forms.CheckBox Front6Enabled;
        private System.Windows.Forms.NumericUpDown Front7Current;
        private System.Windows.Forms.CheckBox Front7Enabled;
        private System.Windows.Forms.NumericUpDown Front8Current;
        private System.Windows.Forms.CheckBox Front8Enabled;
    }
}

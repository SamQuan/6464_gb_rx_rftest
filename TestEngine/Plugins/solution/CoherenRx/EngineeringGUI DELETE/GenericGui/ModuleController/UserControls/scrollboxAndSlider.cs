using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ModuleController.UserControls
{
    public partial class scrollboxAndSlider : UserControl
    {
        public scrollboxAndSlider()
        {
            InitializeComponent();
        }

        public void Setup(string description, decimal max, decimal min)
        {
            label.Text = description;
            trackBar.Maximum = (int)max;
            scrollBox.Maximum = max; 
            trackBar.Minimum = (int)min;
            scrollBox.Minimum = min;
            trackBar.TickFrequency = 1 + Math.Abs((trackBar.Maximum - trackBar.Minimum) / 16);
            trackBar.LargeChange = trackBar.TickFrequency;
            scrollBox.DecimalPlaces = 0;
        }

        public decimal Value
        {
            get { return scrollBox.Value; }
            set { scrollBox.Value = value; }
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            scrollBox.Value = trackBar.Value;
        }

        private void scrollBox_ValueChanged(object sender, EventArgs e)
        {
            trackBar.Value = (int)scrollBox.Value;
            OnValueChanged();
        }

        public delegate void ValueChangedHandler(decimal newValue);
        public event ValueChangedHandler ValueChanged;
        protected virtual void OnValueChanged()
        {
            // If an event has no subscribers registered, it will evaluate to null.
            // Check this before calling the event itself.
            if (ValueChanged != null)
            {
                ValueChanged(scrollBox.Value);  // Notify Subscribers
            }
        }
    }
}

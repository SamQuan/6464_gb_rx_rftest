using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.PluginLoader;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.Core.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using System.Data;
using System.IO;

namespace ModuleController
{
    public class InstrumentLoader
    {
        public InstrumentLoader()
        {
            Initialise();
        }

        public static void Initialise()
        {
            string teBaseDir;
            if (AppDomain.CurrentDomain.BaseDirectory.Contains("TestEngine"))
            {
                teBaseDir = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.LastIndexOf("TestEngine") + 10);
            }
            else
            {
                teBaseDir = ".";
            }

            // On initialisation the PluginLoader creates a cache of dlls under ./Plugins
            Directory.SetCurrentDirectory(teBaseDir);

            string[] pluginDirs = new string[] { "." };

            // Register chassis type with plugin manager 
            Loader.AddPluginType("Chassis", typeof(IChassis), pluginDirs);

            // Register instrument type with plugin manager 
            Loader.AddPluginType("Instrument", typeof(IInstrument), pluginDirs);

            instrumentCollection = new InstrumentCollection();
            chassisCollection = new ChassisCollection();


            string directoryPath = teBaseDir + @"\Configuration\Core";
            string filePath = Path.GetFullPath(Path.Combine(directoryPath, "GUIEquipmentConfig.xml"));

            TestEquipmentConfigDataSet equipmentConfigDataSet = new TestEquipmentConfigDataSet();
            equipmentConfigDataSet.ReadXml(filePath, XmlReadMode.ReadSchema);

            // Create the Internal Chassis Collection by extracting data from the populated equipmentConfigDataSet.
            int numOfChassis = equipmentConfigDataSet.Chassis.Count;
            for (int i = 0; i < numOfChassis; i++)
            {
                Chassis newChassis = CreateNewChassis(equipmentConfigDataSet.Chassis[i].ChassisName, equipmentConfigDataSet.Chassis[i].DriverName, equipmentConfigDataSet.Chassis[i].DriverVersion, equipmentConfigDataSet.Chassis[i].ResourceString);
                newChassis.IsOnline = equipmentConfigDataSet.Chassis[i].Online;

                chassisCollection.Add(equipmentConfigDataSet.Chassis[i].ChassisName, newChassis);
            }
            // Create the Internal Instrument Collection by extracting data from the populated equipmentConfigDataSet and linking to the appropriate chassis.
            for (int ii = 0; ii < equipmentConfigDataSet.Instrument.Count; ii++)
            {
                Instrument newInst = CreateNewInstrument(equipmentConfigDataSet.Instrument[ii].InstrumentName, equipmentConfigDataSet.Instrument[ii].DriverName, equipmentConfigDataSet.Instrument[ii].DriverVersion, equipmentConfigDataSet.Instrument[ii].Slot, equipmentConfigDataSet.Instrument[ii].SubSlot, chassisCollection[equipmentConfigDataSet.Instrument[ii].ChassisName]);
                newInst.IsOnline = equipmentConfigDataSet.Instrument[ii].Online;

                instrumentCollection.Add(equipmentConfigDataSet.Instrument[ii].InstrumentName, newInst);
            }
        }

        /// <summary>
        /// Instantiate a new chassis driver 
        /// </summary>
        /// <param name="name">Name of the driver. e.g. 'MyChassisDriver'</param>
        /// <param name="driver">Driver class name</param>
        /// <param name="driverVersion">Driver version number</param>
        /// <param name="resourceInfo">Resource information for driver e.g. GPIB address, channel number</param>
        private static Chassis CreateNewChassis(string name, string driver, string driverVersion, string resourceInfo)
        {
            // Plugin details object
            PluginInfo newDriverInfo = Loader.LoadPlugin("Chassis", driver, driverVersion);

            // Arguments for driver 
            object[] args = new object[] { name, driver, resourceInfo };

            // Instantiate the new driver & cast to 'Chassis'
            return (Chassis)newDriverInfo.Instantiate(args);
        }

		/// <summary>
		/// Instantiate a new instrument driver 
		/// </summary>
		/// <param name="name">Name of the driver. e.g. 'Laser Current source'</param>
		/// <param name="driver">Driver class name</param>
		/// <param name="driverVersion">Driver version number</param>
		/// <param name="slot">Instrument slot number</param>
		/// <param name="subSlot">Instrument sub slot number</param>
		/// <param name="chassis">Reference to instruments chassis</param>
        private static Instrument CreateNewInstrument(
            string name,
            string driver,
            string driverVersion,
            string slot,
            string subSlot,
            Chassis chassis)
        {
            // Plugin details object
            PluginInfo newDriverInfo = Loader.LoadPlugin("Instrument", driver, driverVersion);

            // Arguments for driver 
            object[] args = new object[] { name, driver, slot, subSlot, chassis };

            // Instantiate the new driver & cast to 'Instrument'
            return (Instrument)newDriverInfo.Instantiate(args);
        }

        private static InstrumentCollection instrumentCollection;
        private static ChassisCollection chassisCollection;


        public static ChassisCollection ChassisList
        {
            get { return chassisCollection; }
        }

        public static InstrumentCollection InstrumentList
        {
            get { return instrumentCollection; }
        }

	

    }
}

namespace Bookham.Toolkit.ModuleController
{
    partial class DSDBRSectionsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GainCurrent = new System.Windows.Forms.NumericUpDown();
            this.SoaCurrent = new System.Windows.Forms.NumericUpDown();
            this.RearCurrent = new System.Windows.Forms.NumericUpDown();
            this.PhaseCurrent = new System.Windows.Forms.NumericUpDown();
            this.FSPair = new System.Windows.Forms.NumericUpDown();
            this.FSConstant = new System.Windows.Forms.NumericUpDown();
            this.FSNonConstant = new System.Windows.Forms.NumericUpDown();
            this.lblGain = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GainCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoaCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RearCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhaseCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSPair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSConstant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSNonConstant)).BeginInit();
            this.SuspendLayout();
            // 
            // GainCurrent
            // 
            this.GainCurrent.DecimalPlaces = 2;
            this.GainCurrent.Location = new System.Drawing.Point(89, 7);
            this.GainCurrent.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.GainCurrent.Name = "GainCurrent";
            this.GainCurrent.Size = new System.Drawing.Size(120, 20);
            this.GainCurrent.TabIndex = 0;
            this.GainCurrent.ValueChanged += new System.EventHandler(this.GainCurrent_ValueChanged);
            this.GainCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GainCurrent_KeyPress);
            this.GainCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GainCurrent_MouseUp);
            // 
            // SoaCurrent
            // 
            this.SoaCurrent.DecimalPlaces = 2;
            this.SoaCurrent.Location = new System.Drawing.Point(89, 33);
            this.SoaCurrent.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.SoaCurrent.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.SoaCurrent.Name = "SoaCurrent";
            this.SoaCurrent.Size = new System.Drawing.Size(120, 20);
            this.SoaCurrent.TabIndex = 1;
            this.SoaCurrent.ValueChanged += new System.EventHandler(this.SoaCurrent_ValueChanged);
            this.SoaCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoaCurrent_KeyPress);
            this.SoaCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SoaCurrent_MouseUp);
            // 
            // RearCurrent
            // 
            this.RearCurrent.DecimalPlaces = 2;
            this.RearCurrent.Location = new System.Drawing.Point(89, 59);
            this.RearCurrent.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.RearCurrent.Name = "RearCurrent";
            this.RearCurrent.Size = new System.Drawing.Size(120, 20);
            this.RearCurrent.TabIndex = 2;
            this.RearCurrent.ValueChanged += new System.EventHandler(this.RearCurrent_ValueChanged);
            this.RearCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RearCurrent_KeyPress);
            this.RearCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RearCurrent_MouseUp);
            // 
            // PhaseCurrent
            // 
            this.PhaseCurrent.DecimalPlaces = 2;
            this.PhaseCurrent.Location = new System.Drawing.Point(89, 85);
            this.PhaseCurrent.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.PhaseCurrent.Name = "PhaseCurrent";
            this.PhaseCurrent.Size = new System.Drawing.Size(120, 20);
            this.PhaseCurrent.TabIndex = 3;
            this.PhaseCurrent.ValueChanged += new System.EventHandler(this.PhaseCurrent_ValueChanged);
            this.PhaseCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PhaseCurrent_KeyPress);
            this.PhaseCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PhaseCurrent_MouseUp);
            // 
            // FSPair
            // 
            this.FSPair.DecimalPlaces = 2;
            this.FSPair.Location = new System.Drawing.Point(89, 111);
            this.FSPair.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.FSPair.Name = "FSPair";
            this.FSPair.Size = new System.Drawing.Size(120, 20);
            this.FSPair.TabIndex = 4;
            this.FSPair.ValueChanged += new System.EventHandler(this.FSPair_ValueChanged);
            this.FSPair.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FSPair_KeyPress);
            this.FSPair.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FSPair_MouseUp);
            // 
            // FSConstant
            // 
            this.FSConstant.DecimalPlaces = 2;
            this.FSConstant.Location = new System.Drawing.Point(89, 137);
            this.FSConstant.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.FSConstant.Name = "FSConstant";
            this.FSConstant.Size = new System.Drawing.Size(120, 20);
            this.FSConstant.TabIndex = 5;
            this.FSConstant.ValueChanged += new System.EventHandler(this.FSConstant_ValueChanged);
            this.FSConstant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FSConstant_KeyPress);
            this.FSConstant.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FSConstant_MouseUp);
            // 
            // FSNonConstant
            // 
            this.FSNonConstant.DecimalPlaces = 2;
            this.FSNonConstant.Location = new System.Drawing.Point(89, 163);
            this.FSNonConstant.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.FSNonConstant.Name = "FSNonConstant";
            this.FSNonConstant.Size = new System.Drawing.Size(120, 20);
            this.FSNonConstant.TabIndex = 6;
            this.FSNonConstant.ValueChanged += new System.EventHandler(this.FSNonConstant_ValueChanged);
            this.FSNonConstant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FSNonConstant_KeyPress);
            this.FSNonConstant.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FSNonConstant_MouseUp);
            // 
            // lblGain
            // 
            this.lblGain.AutoSize = true;
            this.lblGain.Location = new System.Drawing.Point(3, 9);
            this.lblGain.Name = "lblGain";
            this.lblGain.Size = new System.Drawing.Size(29, 13);
            this.lblGain.TabIndex = 7;
            this.lblGain.Text = "Gain";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "SOA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Rear";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Phase";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "FS Pair";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "FS Constant";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "FS NonConstant";
            // 
            // DSDBRSectionsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblGain);
            this.Controls.Add(this.FSNonConstant);
            this.Controls.Add(this.FSConstant);
            this.Controls.Add(this.FSPair);
            this.Controls.Add(this.PhaseCurrent);
            this.Controls.Add(this.RearCurrent);
            this.Controls.Add(this.SoaCurrent);
            this.Controls.Add(this.GainCurrent);
            this.Name = "DSDBRSectionsCtrl";
            this.Size = new System.Drawing.Size(212, 191);
            ((System.ComponentModel.ISupportInitialize)(this.GainCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoaCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RearCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhaseCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSPair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSConstant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FSNonConstant)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown GainCurrent;
        private System.Windows.Forms.NumericUpDown SoaCurrent;
        private System.Windows.Forms.NumericUpDown RearCurrent;
        private System.Windows.Forms.NumericUpDown PhaseCurrent;
        private System.Windows.Forms.NumericUpDown FSPair;
        private System.Windows.Forms.NumericUpDown FSConstant;
        private System.Windows.Forms.NumericUpDown FSNonConstant;
        private System.Windows.Forms.Label lblGain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

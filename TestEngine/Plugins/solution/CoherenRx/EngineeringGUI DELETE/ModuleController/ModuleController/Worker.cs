using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.BlackBoxes;
using ModuleController.Data;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using System.IO;
using Bookham.TestLibrary.Utilities;

namespace ModuleController
{
    /// <summary>
    /// Worker class to handle the communication with the black box module
    /// </summary>
    static class Worker
    {
        /// <summary>
        /// Set all laser and MZ sections
        /// </summary>
        /// <param name="laserCurrents">Collection of laser section settings</param>
        /// <param name="pulseCarverMz">Collection of MZ section settings</param>
        /// <param name="topMz">Collection of MZ section settings</param>
        /// <param name="bottomMz">Collection of MZ section settings</param>
        /// <param name="outerMz">Collection of MZ section settings</param>
        internal static void setLaserAndMz(DSDBRSectionCurrents laserCurrents, MZBiasSettings pulseCarverMz, MZBiasSettings topMz, MZBiasSettings bottomMz, MZBiasSettings outerMz)
        {

            // Set sections (order gain-front sections-rear-phase-soa for clockwise startup)

            if ( laserCurrents.Gain.ValueChanged )
                DsdbrLaser.SetGainDAC((int)laserCurrents.Gain.Current_mA);

            // front sections
            DSDBRSectionCurrent fsOdd = new DSDBRSectionCurrent();
            DSDBRSectionCurrent fsEven = new DSDBRSectionCurrent();
            int fsPair = (int)laserCurrents.FSPair.Current_mA;
            fsOdd.Current_mA = laserCurrents.FsConstant.Current_mA;
            fsEven.Current_mA = laserCurrents.FsNonConstant.Current_mA;
           
            DsdbrLaser.SetFrontSectionPair(fsPair);
            DsdbrLaser.SetFrontOddDAC((int)fsOdd.Current_mA);
            DsdbrLaser.SetFrontEvenDAC((int)fsEven.Current_mA);

            if (laserCurrents.Rear.ValueChanged)
                DsdbrLaser.SetRearDAC((int)laserCurrents.Rear.Current_mA);

            if (laserCurrents.Phase.ValueChanged)
                DsdbrLaser.SetPhaseDAC((int)laserCurrents.Phase.Current_mA);

            if (laserCurrents.SOA.ValueChanged)
                DsdbrLaser.SetSOADAC((int)laserCurrents.SOA.Current_mA);


            /* PULSE CARVER */
            if ( pulseCarverMz.leftPhase.ValueChanged )
                SetMzLeftPhaseDac(MzSectionName.PulseCarver, (int)pulseCarverMz.leftPhase.Current_mA);

            if (pulseCarverMz.rightPhase.ValueChanged)
                SetMzRightPhaseDac(MzSectionName.PulseCarver, (int)pulseCarverMz.rightPhase.Current_mA);

            if (pulseCarverMz.leftModulation.ValueChanged)
                SetMzLeftRfBiasDac(MzSectionName.PulseCarver, (int)pulseCarverMz.leftModulation.Voltage_V); 
            
            if (pulseCarverMz.rightModulation.ValueChanged)
                SetMzRightRfBiasDac(MzSectionName.PulseCarver, (int)pulseCarverMz.rightModulation.Voltage_V);


            /* OUTER */
            if (outerMz.leftPhase.ValueChanged)
                SetMzLeftPhaseDac(MzSectionName.Outer, (int)outerMz.leftPhase.Current_mA);

            if (outerMz.rightPhase.ValueChanged)
                SetMzRightPhaseDac(MzSectionName.Outer, (int)outerMz.rightPhase.Current_mA);


            /* TOP */
            if (topMz.leftPhase.ValueChanged)
                SetMzLeftPhaseDac(MzSectionName.Top, (int)pulseCarverMz.leftPhase.Current_mA);

            if (topMz.rightPhase.ValueChanged)
                SetMzRightPhaseDac(MzSectionName.Top, (int)topMz.rightPhase.Current_mA);

            if (topMz.leftModulation.ValueChanged)
                SetMzLeftRfBiasDac(MzSectionName.Top, (int)topMz.leftModulation.Voltage_V);

            if (topMz.rightModulation.ValueChanged)
                SetMzRightRfBiasDac(MzSectionName.Top, (int)topMz.rightModulation.Voltage_V);


            /* BOTTOM */
            if (bottomMz.leftPhase.ValueChanged)
                SetMzLeftPhaseDac(MzSectionName.Bottom, (int)bottomMz.leftPhase.Current_mA);

            if (bottomMz.rightPhase.ValueChanged)
                SetMzRightPhaseDac(MzSectionName.Bottom, (int)bottomMz.rightPhase.Current_mA);

            if (bottomMz.leftModulation.ValueChanged)
                SetMzLeftRfBiasDac(MzSectionName.Bottom, (int)bottomMz.leftModulation.Voltage_V);

            if (bottomMz.rightModulation.ValueChanged)
                SetMzRightRfBiasDac(MzSectionName.Bottom, (int)bottomMz.rightModulation.Voltage_V);

        }

        /// <summary>
        /// Set all laser and MZ sections
        /// </summary>
        /// <param name="laserCurrents">Collection of laser section settings</param>
        /// <param name="pulseCarverMz">Collection of MZ section settings</param>
        /// <param name="topMz">Collection of MZ section settings</param>
        /// <param name="bottomMz">Collection of MZ section settings</param>
        /// <param name="outerMz">Collection of MZ section settings</param>
        internal static void getLaserAndMz(ref DSDBRSectionCurrents laserCurrents, ref MZBiasSettings pulseCarverMz, ref MZBiasSettings topMz, ref MZBiasSettings bottomMz, ref MZBiasSettings outerMz)
        {
            laserCurrents.Gain.Current_mA = DsdbrLaser.GetGainDAC();
            laserCurrents.Gain.Enabled = true;

            laserCurrents.Rear.Current_mA = DsdbrLaser.GetRearDAC();
            laserCurrents.Rear.Enabled = true;

            laserCurrents.Phase.Current_mA = DsdbrLaser.GetPhaseDAC();
            laserCurrents.Phase.Enabled = true;

            laserCurrents.SOA.Current_mA = DsdbrLaser.GetSOADAC();
            laserCurrents.SOA.Enabled = true;

            int fsPair = DsdbrLaser.GetFrontSectionPair();
            int fsOdd = DsdbrLaser.GetFrontOddDAC();
            int fsEven = DsdbrLaser.GetFrontEvenDAC();

            laserCurrents.FSPair.Current_mA = fsPair;
            if (fsPair % 2 == 0)
            {
                // Pair is Even
                laserCurrents.FsConstant.Current_mA = fsEven;
                laserCurrents.FsNonConstant.Current_mA = fsOdd;
            }
            else
            {
                // Pair is odd
                laserCurrents.FsConstant.Current_mA = fsOdd;
                laserCurrents.FsNonConstant.Current_mA = fsEven;
            }

            /* PULSE CARVER */
            pulseCarverMz.leftPhase.Current_mA = GetMzLeftPhaseDac(MzSectionName.PulseCarver);
            pulseCarverMz.rightPhase.Current_mA = GetMzRightPhaseDac(MzSectionName.PulseCarver);
            pulseCarverMz.leftModulation.Voltage_V = GetMzLeftRfBiasDac(MzSectionName.PulseCarver);
            pulseCarverMz.rightModulation.Voltage_V = GetMzRightRfBiasDac(MzSectionName.PulseCarver);

            /* OUTER */
            outerMz.leftPhase.Current_mA = GetMzLeftPhaseDac(MzSectionName.Outer);
            outerMz.rightPhase.Current_mA = GetMzRightPhaseDac(MzSectionName.Outer);

            /* TOP */
            topMz.leftPhase.Current_mA = GetMzLeftPhaseDac(MzSectionName.Top);
            topMz.rightPhase.Current_mA = GetMzRightPhaseDac(MzSectionName.Top);
            topMz.leftModulation.Voltage_V = GetMzLeftRfBiasDac(MzSectionName.Top);
            topMz.rightModulation.Voltage_V = GetMzRightRfBiasDac(MzSectionName.Top);

            /* BOTTOM */
            bottomMz.leftPhase.Current_mA = GetMzLeftPhaseDac(MzSectionName.Bottom);
            bottomMz.rightPhase.Current_mA = GetMzRightPhaseDac(MzSectionName.Bottom);
            bottomMz.leftModulation.Voltage_V = GetMzLeftRfBiasDac(MzSectionName.Bottom);
            bottomMz.rightModulation.Voltage_V = GetMzRightRfBiasDac(MzSectionName.Bottom);
        }



        internal static void SetMzLeftPhaseDac(MzSectionName sectionName, int dacValue)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    dqpskSetup.SetPCMZLImbalance(dacValue);
                    break;
                case MzSectionName.Top:
                    dqpskSetup.SetTopMZLImbalance(dacValue);
                    break;
                case MzSectionName.Bottom:
                    dqpskSetup.SetBottomMZLImbalance(dacValue);
                    break;
                case MzSectionName.Outer:
                    dqpskSetup.SetOuterMZLImbalance(dacValue);
                    break;
                default:
                    break;
            }
        }

        internal static int GetMzLeftPhaseDac(MzSectionName sectionName)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    return dqpskSetup.GetPCMZLImbalance();
                case MzSectionName.Top:
                    return dqpskSetup.GetTopMZLImbalance();
                case MzSectionName.Bottom:
                    return dqpskSetup.GetBottomMZLImbalance();
                case MzSectionName.Outer:
                    return dqpskSetup.GetOuterMZLImbalance();
                default:
                    break;
            }
            return int.MinValue;
        }

        internal static void SetMzRightPhaseDac(MzSectionName sectionName, int dacValue)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    dqpskSetup.SetPCMZRImbalance(dacValue);
                    break;
                case MzSectionName.Top:
                    dqpskSetup.SetTopMZRImbalance(dacValue);
                    break;
                case MzSectionName.Bottom:
                    dqpskSetup.SetBottomMZRImbalance(dacValue);
                    break;
                case MzSectionName.Outer:
                    dqpskSetup.SetOuterMZRImbalance(dacValue);
                    break;
                default:
                    break;
            }
        }

        internal static int GetMzRightPhaseDac(MzSectionName sectionName)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    return dqpskSetup.GetPCMZRImbalance();
                case MzSectionName.Top:
                    return dqpskSetup.GetTopMZRImbalance();
                case MzSectionName.Bottom:
                    return dqpskSetup.GetBottomMZRImbalance();
                case MzSectionName.Outer:
                    return dqpskSetup.GetOuterMZRImbalance();
                default:
                    break;
            }
            return int.MinValue;
        }

        internal static void SetMzRightRfBiasDac(MzSectionName sectionName, int dacValue)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    dqpskSetup.SetPCMZRBias(dacValue);
                    break;
                case MzSectionName.Top:
                    dqpskSetup.SetTopMZRBias(dacValue);
                    break;
                case MzSectionName.Bottom:
                    dqpskSetup.SetBottomMZRBias(dacValue);
                    break;
                case MzSectionName.Outer:
                    throw new ArgumentException("Outer MZ has no RF control");
                default:
                    break;
            }
        }

        internal static int GetMzRightRfBiasDac(MzSectionName sectionName)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    return dqpskSetup.GetPCMZRBias();
                case MzSectionName.Top:
                    return dqpskSetup.GetTopMZRBias();
                case MzSectionName.Bottom:
                    return dqpskSetup.GetBottomMZRBias();
                case MzSectionName.Outer:
                    throw new ArgumentException("Outer MZ has no RF control");
                default:
                    break;
            }
            return int.MinValue;
        }

        internal static void SetMzLeftRfBiasDac(MzSectionName sectionName, int dacValue)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    dqpskSetup.SetPCMZLBias(dacValue);
                    break;
                case MzSectionName.Top:
                    dqpskSetup.SetTopMZLBias(dacValue);
                    break;
                case MzSectionName.Bottom:
                    dqpskSetup.SetBottomMZLBias(dacValue);
                    break;
                case MzSectionName.Outer:
                    throw new ArgumentException("Outer MZ has no RF control");
                default:
                    break;
            }
        }

        internal static int GetMzLeftRfBiasDac(MzSectionName sectionName)
        {
            switch (sectionName)
            {
                case MzSectionName.PulseCarver:
                    return dqpskSetup.GetPCMZLBias();
                case MzSectionName.Top:
                    return dqpskSetup.GetTopMZLBias();
                case MzSectionName.Bottom:
                    return dqpskSetup.GetBottomMZLBias();
                case MzSectionName.Outer:
                    throw new ArgumentException("Outer MZ has no RF control");
                default:
                    break;
            }
            return int.MinValue;
        }

        internal static void Initialise()
        {
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);

            try
            {
                InstrumentLoader.Initialise();
            }
            catch (Bookham.TestEngine.Framework.PluginLoader.PluginInstantiatingFailException pif)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("\r\n" + pif.Message);
                if (pif.InnerException != null)
                {
                    errorMessage.Append("\r\n" + pif.InnerException.Message);
                    if ( pif.InnerException.InnerException != null )
                    {
                        errorMessage.Append("\r\n" + pif.InnerException.InnerException.Message);
                    }
                }
                throw new SystemException("Error loading instrument driver :\r\n" + errorMessage );
            }

            chassis = InstrumentLoader.ChassisList["BlackBoxChassis"];
            instrument = InstrumentLoader.InstrumentList["BlackBox"];
            testBoardChassis = InstrumentLoader.ChassisList["TestBoardChassis"];
            testBoard = InstrumentLoader.InstrumentList["TestBoard"];

            // Check whether the module supports the dsdbr interface
            dsdbrLaser = instrument as IDsdbrLaserSetup;
            if (dsdbrLaser == null)
            {
                //throw;
            }
            instrument.Timeout_ms = 1000;
            chassis.Timeout_ms = 1000;
            chassis.EnableLogging = true;
            

            if (chassis.IsOnline)
            {
                try
                {
                    dsdbrLaser.EnterLaserSetupMode(); // Send password
                }
                catch (Exception)
                {

                    System.Windows.Forms.MessageBox.Show("Error communicating with DUT. Please check power and connections","Communication error");
                }
                
            }

            // Check whether the module supports the tcmz interfaces
            tcmzLaser = instrument as ITcmzLaserSetup;
            if (tcmzLaser == null)
            {
                //throw;
            }

            // Check whether the module supports the tcmz interfaces
            dqpskSetup = instrument as IDqpskMzSetup;
            if (dqpskSetup == null)
            {
                //throw;
            }
  
            itla = instrument as IMSA_ITLA;
            if (itla == null)
            {
                //throw;
            }

            registerDebug = instrument as IOifRegisterDebug;
            if (registerDebug == null)
            {
                // throw
            }          

            blackBoxIdentity = instrument as IBlackBoxIdentity;
            if (blackBoxIdentity == null)
            {
                // throw
            }

            // Test Board
            testBoardSetup = testBoard as I40GTestBoard;
            if (testBoardSetup == null)
            {
                // Test board unavailable
            }

            evalBoardRegisterDebug = TestBoard as IOifRegisterDebug;
            if (evalBoardRegisterDebug == null)
            {
                // throw
            }

        }


        public static IDsdbrLaserSetup DsdbrLaser
        {
            get
            {
                try
                {
                    return dsdbrLaser;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static ITcmzLaserSetup TcmzLaser
        {
            get
            {
                try
                {
                    return tcmzLaser;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static IOifRegisterDebug RegisterDebug
        {
            get
            {
                try
                {
                    return registerDebug;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static IOifRegisterDebug EvalBoardRegisterDebug
        {
            get
            {
                try
                {
                    return evalBoardRegisterDebug;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static IDqpskMzSetup DqpskSetup
        {
            get
            {
                try
                {
                    return dqpskSetup;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static IMSA_ITLA Itla
        {
            get
            {
                try
                {
                    return itla;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static IBlackBoxIdentity BlackBoxIdentity
        {
            get
            {
                try
                {
                    return blackBoxIdentity;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }

        public static I40GTestBoard TestBoard
        {
            get
            {
                try
                {
                    return testBoardSetup ;
                }
                catch (TunableModuleException)
                {
                    return null;
                }
            }
        }


        public static void PopulateGridFromFile(string fileName, System.Windows.Forms.DataGridView grid)
        {
            // Read data from file
            List<string[]> fileElements = null;
            // Read the delimited file
            using (DelimitedFileReader reader = new DelimitedFileReader(DelimitedFileDelimiter.Comma, true))
            {
                fileElements = reader.ReadFile(fileName);
            }
            grid.ColumnCount = fileElements[0].Length;
            // Add to grid
            bool doneHeaders = false;
            foreach (string[] row in fileElements)
            {
                if (!doneHeaders)
                {
                    for (int i = 0; i < row.Length; i++)
                    {
                        grid.Columns[i].Name = row[i];
                    }
                    doneHeaders = true;
                    continue;
                }
                grid.Rows.Add(row);
            }
        }

        // Instrument
        private static IBlackBoxIdentity blackBoxIdentity;
        private static IMSA_ITLA itla;
        private static IMSA_XFP xfp;
        private static IMSA300pin msa300pin;
        private static IReceiverSetup receiver;
        private static IDqpskMzSetup dqpskSetup;
        private static IOifRegisterDebug registerDebug;
        private static IOifRegisterDebug evalBoardRegisterDebug;

        // Test board
        private static I40GTestBoard testBoardSetup;

        /// <summary>
        /// Black box laser driver
        /// </summary>
        private static IDsdbrLaserSetup dsdbrLaser;
        /// <summary>
        /// Black box single MZ driver
        /// </summary>
        private static ITcmzLaserSetup tcmzLaser;

        private static Chassis chassis;
        private static Instrument instrument; 
        private static Chassis testBoardChassis;
        private static Instrument testBoard;

        internal enum MzSectionName
        {
            PulseCarver,
            Top,
            Bottom,
            Outer
        }
    }
}

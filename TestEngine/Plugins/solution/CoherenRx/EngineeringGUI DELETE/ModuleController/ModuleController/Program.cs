using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Text;

namespace ModuleController
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.WriteLine("Startup");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.Run(new MainForm());
            Environment.Exit(0);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string errorMessage;
            Exception exc = e.ExceptionObject as Exception;
            if (exc != null)
            {
                errorMessage = BuildErrorTrace(exc);
            }
            else
            {
                errorMessage = e.ToString() + "\r\n" + e.ExceptionObject.ToString();
            }
            MessageBox.Show(errorMessage, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Environment.Exit(0xBADF00D);
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
                MessageBox.Show(BuildErrorTrace(e.Exception), "Error");
        }
        
        static void OnGuiUnhandledException(object obj, ThreadExceptionEventArgs args)
        {
            MessageBox.Show(BuildErrorTrace(args.Exception), "Exception!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static string BuildErrorTrace(Exception e)
        {
            errorTrace = new StringBuilder();
            while (e != null)
            {
                GetStackTrace(ref e);
            }
            return errorTrace.ToString();
        }

        static void GetStackTrace(ref Exception e)
        {
            errorTrace.Append(e.Message);
            e = e.InnerException;
        }

        private static StringBuilder errorTrace;
    }
}
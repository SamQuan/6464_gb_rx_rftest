using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ModuleController.Data;
using ModuleController;

namespace Bookham.Toolkit.ModuleController
{
    /// <summary>
    /// DSDBR Section custom control - shows all the laser section current values!
    /// </summary>
    public partial class DSDBRSectionsCtrl : UserControl
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DSDBRSectionsCtrl()
        {
            InitializeComponent();
            SetUpperLimits(true);
        }

        /// <summary>
        /// Readback / Apply the DSDBR currents
        /// </summary>
        public DSDBRSectionCurrents Value
        {
            get
            {
                DSDBRSectionCurrents d = new DSDBRSectionCurrents();
                d.Gain.Current_mA = (double) GainCurrent.Value;
                d.Gain.ValueChanged = Convert.ToBoolean(GainCurrent.Tag);
                d.SOA.Current_mA = (double) SoaCurrent.Value;
                d.SOA.ValueChanged = Convert.ToBoolean(SoaCurrent.Tag);
                d.Rear.Current_mA = (double)RearCurrent.Value;
                d.Rear.ValueChanged = Convert.ToBoolean(RearCurrent.Tag);
                d.Phase.Current_mA = (double)PhaseCurrent.Value;
                d.Phase.ValueChanged = Convert.ToBoolean(PhaseCurrent.Tag);

                d.FSPair.Current_mA = (double)FSPair.Value;
                d.FSPair.ValueChanged = Convert.ToBoolean(FSPair.Tag);
                d.FsConstant.Current_mA = (double)FSConstant.Value;
                d.FsConstant.ValueChanged = Convert.ToBoolean(FSConstant.Tag);
                d.FsNonConstant.Current_mA = (double)FSNonConstant.Value;
                d.FsNonConstant.ValueChanged = Convert.ToBoolean(FSNonConstant.Tag);

                return d;
            }
            set
            {
                GainCurrent.Value = (decimal) value.Gain.Current_mA;
                GainCurrent.Tag = value.Gain.ValueChanged;
                SoaCurrent.Value = (decimal)value.SOA.Current_mA;
                SoaCurrent.Tag = value.SOA.ValueChanged;
                RearCurrent.Value = (decimal)value.Rear.Current_mA;
                RearCurrent.Tag = value.Rear.ValueChanged;
                PhaseCurrent.Value = (decimal)value.Phase.Current_mA;
                PhaseCurrent.Tag = value.Phase.ValueChanged;

                FSPair.Value = (decimal)value.FSPair.Current_mA;
                FSConstant.Tag = value.FSPair.ValueChanged;
                FSConstant.Value = (decimal)value.FsConstant.Current_mA;
                FSConstant.Tag = value.FsConstant.ValueChanged;
                FSNonConstant.Value = (decimal)value.FsNonConstant.Current_mA;
                FSNonConstant.Tag = value.FsNonConstant.ValueChanged;
            }
        }

        public void ResetColours()
        {
            GainCurrent.ForeColor = Color.Black;
            SoaCurrent.ForeColor = Color.Black;
            RearCurrent.ForeColor = Color.Black;
            PhaseCurrent.ForeColor = Color.Black;
            FSPair.ForeColor = Color.Black;
            FSConstant.ForeColor = Color.Black;
            FSNonConstant.ForeColor = Color.Black;
        }

        public void SetUpperLimits(bool dacMode)
        {
            if (dacMode)
            {
                GainCurrent.Maximum = 65535;
                SoaCurrent.Maximum = 65535;
                RearCurrent.Maximum = 65535;
                PhaseCurrent.Maximum = 65535;
                FSPair.Maximum = 7;
                FSConstant.Maximum = 65535;
                FSNonConstant.Maximum = 65535;
                GainCurrent.DecimalPlaces = 0;
                SoaCurrent.DecimalPlaces = 0;
                RearCurrent.DecimalPlaces = 0;
                PhaseCurrent.DecimalPlaces = 0;
                FSPair.DecimalPlaces = 0;
                FSConstant.DecimalPlaces = 0;
                FSNonConstant.DecimalPlaces = 0;
             }
            else
            {
                GainCurrent.Maximum = 300;
                SoaCurrent.Maximum = 300;
                RearCurrent.Maximum = 50;
                PhaseCurrent.Maximum = 20;
                FSPair.Maximum = 7;
                FSConstant.Maximum = 5;
                FSNonConstant.Maximum = 5;
                GainCurrent.DecimalPlaces = 3;
                SoaCurrent.DecimalPlaces = 3;
                RearCurrent.DecimalPlaces = 3;
                PhaseCurrent.DecimalPlaces = 3;
                FSPair.DecimalPlaces = 0;
                FSConstant.DecimalPlaces = 3;
                FSNonConstant.DecimalPlaces = 3;
            }
        }

        private void GainCurrent_ValueChanged(object sender, EventArgs e)
        {
            GainCurrent.Tag = true;
            GainCurrent.ForeColor = Color.IndianRed;
        }

        private void SoaCurrent_ValueChanged(object sender, EventArgs e)
        {
            SoaCurrent.Tag = true;
            SoaCurrent.ForeColor = Color.IndianRed;
        }

        private void RearCurrent_ValueChanged(object sender, EventArgs e)
        {
            RearCurrent.Tag = true;
            RearCurrent.ForeColor = Color.IndianRed;
        }

        private void PhaseCurrent_ValueChanged(object sender, EventArgs e)
        {
            PhaseCurrent.Tag = true;
            PhaseCurrent.ForeColor = Color.IndianRed;
        }

        private void FSPair_ValueChanged(object sender, EventArgs e)
        {
            FSPair.Tag = true;
            FSPair.ForeColor = Color.IndianRed;
        }

        private void FSConstant_ValueChanged(object sender, EventArgs e)
        {
            FSConstant.Tag = true;
            FSConstant.ForeColor = Color.IndianRed;
        }

        private void FSNonConstant_ValueChanged(object sender, EventArgs e)
        {
            FSNonConstant.Tag = true;
            FSNonConstant.ForeColor = Color.IndianRed;
        }

        private void GainCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateGainCurrent();
        }

        private void UpdateGainCurrent()
        {
            Worker.DsdbrLaser.SetGainDAC((int)GainCurrent.Value);
            GainCurrent.ForeColor = Color.Black;
        }

        private void SoaCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateSoaCurrent();
        }

        private void UpdateSoaCurrent()
        {
            Worker.DsdbrLaser.SetSOADAC((int)SoaCurrent.Value);
            SoaCurrent.ForeColor = Color.Black;
        }

        private void RearCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateRearCurrent();
        }

        private void UpdateRearCurrent()
        {
            Worker.DsdbrLaser.SetRearDAC((int)RearCurrent.Value);
            RearCurrent.ForeColor = Color.Black;
        }

        private void PhaseCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            UpdatePhaseCurrent();
        }

        private void UpdatePhaseCurrent()
        {
            Worker.DsdbrLaser.SetPhaseDAC((int)PhaseCurrent.Value);
            PhaseCurrent.ForeColor = Color.Black;
        }

        private void FSPair_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateFsPair();
        }

        private void UpdateFsPair()
        {
            Worker.DsdbrLaser.SetFrontSectionPair((int)FSPair.Value);
            FSPair.ForeColor = Color.Black;
        }

        private void FSNonConstant_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateFsNonConstant();
        }

        private void UpdateFsNonConstant()
        {
            if (FSPair.Value % 2 == 0)
            {
                // Pair number is EVEN, so non-const front section is odd.
                Worker.DsdbrLaser.SetFrontOddDAC((int)FSNonConstant.Value);
            }
            else
            {
                // Pair number is ODD, so non-const front section is even.
                Worker.DsdbrLaser.SetFrontEvenDAC((int)FSNonConstant.Value);
            }
            FSNonConstant.ForeColor = Color.Black;
        }

        private void FSConstant_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateFsConstant();
        }

        private void UpdateFsConstant()
        {
            if (FSPair.Value % 2 == 0)
            {
                // Pair number is EVEN, so constant front section is even.
                Worker.DsdbrLaser.SetFrontEvenDAC((int)FSConstant.Value);
            }
            else
            {
                // Pair number is ODD, so constant front section is odd.
                Worker.DsdbrLaser.SetFrontOddDAC((int)FSConstant.Value);
            }
            FSConstant.ForeColor = Color.Black;
        }

        private void GainCurrent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateGainCurrent();
        }

        private void SoaCurrent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateSoaCurrent();
        }

        private void RearCurrent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateRearCurrent();
        }

        private void PhaseCurrent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdatePhaseCurrent();
        }

        private void FSPair_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateFsPair();
        }

        private void FSConstant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateFsConstant();
        }

        private void FSNonConstant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                UpdateFsNonConstant();
        }
    }
}

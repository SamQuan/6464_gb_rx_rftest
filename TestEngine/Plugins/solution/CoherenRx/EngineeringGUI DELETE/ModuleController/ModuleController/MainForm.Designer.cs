namespace ModuleController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            ModuleController.Data.MZBiasSettings mzBiasSettings1 = new ModuleController.Data.MZBiasSettings();
            ModuleController.Data.MZBiasSettings mzBiasSettings2 = new ModuleController.Data.MZBiasSettings();
            ModuleController.Data.MZBiasSettings mzBiasSettings3 = new ModuleController.Data.MZBiasSettings();
            ModuleController.Data.MZBiasSettings mzBiasSettings4 = new ModuleController.Data.MZBiasSettings();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btnReadUpperModuleIdRegisters = new System.Windows.Forms.Button();
            this.btnWriteModuleIdentity = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFwVersionWrite = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBuildDateWrite = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPcbSerialWrite = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tstLaserSerialWrite = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtModSerialWrite = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnReadModuleIdentity = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFwBackRO = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFwRelRO = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMfgDateRO = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtModelRO = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtManufacturerRO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDevTypeRO = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ckLaserAutoUpdate = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtLsPhaseSense = new System.Windows.Forms.TextBox();
            this.lblLsPhaseSense = new System.Windows.Forms.Label();
            this.lblLaserMon = new System.Windows.Forms.Label();
            this.txtLaserMon = new System.Windows.Forms.TextBox();
            this.lblRxMon = new System.Windows.Forms.Label();
            this.txtRxMon = new System.Windows.Forms.TextBox();
            this.lblTxMon = new System.Windows.Forms.Label();
            this.txtTxMon = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSaveSetup = new System.Windows.Forms.Button();
            this.channelSelect = new System.Windows.Forms.NumericUpDown();
            this.btnRecallChannel = new System.Windows.Forms.Button();
            this.btnSelectChannel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dsdbrSectionsCtrl1 = new Bookham.Toolkit.ModuleController.DSDBRSectionsCtrl();
            this.btnRead = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.absoluteMode = new System.Windows.Forms.RadioButton();
            this.dacMode = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.numBottomDataModCtrl = new System.Windows.Forms.NumericUpDown();
            this.tkBottomDataModCtrl = new System.Windows.Forms.TrackBar();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tkTopDataModCtrl = new System.Windows.Forms.TrackBar();
            this.numTopDataModCtrl = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mzSectionsCtrl_L = new ModuleController.MZSectionsCtrl();
            this.mzSectionsCtrl_R = new ModuleController.MZSectionsCtrl();
            this.mzSectionsCtrl_O = new ModuleController.MZSectionsCtrl();
            this.mzSectionsCtrl_PC = new ModuleController.MZSectionsCtrl();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.cbFastUpdate = new System.Windows.Forms.CheckBox();
            this.checkAutoUpdate = new System.Windows.Forms.CheckBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblCarverTapRf = new System.Windows.Forms.Label();
            this.txtCarverTapRf = new System.Windows.Forms.TextBox();
            this.lblOutputRfDc = new System.Windows.Forms.Label();
            this.txtOutputRfDc = new System.Windows.Forms.TextBox();
            this.lblOutInlineDc = new System.Windows.Forms.Label();
            this.txtOutInlineDc = new System.Windows.Forms.TextBox();
            this.lblLmmiDc = new System.Windows.Forms.Label();
            this.txtLmmiDc = new System.Windows.Forms.TextBox();
            this.lblUmmiDc = new System.Windows.Forms.Label();
            this.txtUmmiDc = new System.Windows.Forms.TextBox();
            this.lblCarverTap = new System.Windows.Forms.Label();
            this.txtCarverTap = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.ckCtrlReg0 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg15 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg14 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg13 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg12 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg11 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg10 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg9 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg8 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg7 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg6 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg5 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg4 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg3 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg2 = new System.Windows.Forms.CheckBox();
            this.ckCtrlReg1 = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.ckCtrlStatus0 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus15 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus14 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus13 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus12 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus11 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus10 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus9 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus8 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus7 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus6 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus5 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus4 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus3 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus2 = new System.Windows.Forms.CheckBox();
            this.ckCtrlStatus1 = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.ckVen0 = new System.Windows.Forms.CheckBox();
            this.ckVen15 = new System.Windows.Forms.CheckBox();
            this.ckVen14 = new System.Windows.Forms.CheckBox();
            this.ckVen13 = new System.Windows.Forms.CheckBox();
            this.ckVen12 = new System.Windows.Forms.CheckBox();
            this.ckVen11 = new System.Windows.Forms.CheckBox();
            this.ckVen10 = new System.Windows.Forms.CheckBox();
            this.ckVen9 = new System.Windows.Forms.CheckBox();
            this.ckVen8 = new System.Windows.Forms.CheckBox();
            this.ckVen7 = new System.Windows.Forms.CheckBox();
            this.ckVen6 = new System.Windows.Forms.CheckBox();
            this.ckVen5 = new System.Windows.Forms.CheckBox();
            this.ckVen4 = new System.Windows.Forms.CheckBox();
            this.ckVen3 = new System.Windows.Forms.CheckBox();
            this.ckVen2 = new System.Windows.Forms.CheckBox();
            this.ckVen1 = new System.Windows.Forms.CheckBox();
            this.btnReadTcmzMonitors = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lblLaserTemp = new System.Windows.Forms.Label();
            this.txtLaserTemp = new System.Windows.Forms.TextBox();
            this.lblMzTemp = new System.Windows.Forms.Label();
            this.lblPcbTemp = new System.Windows.Forms.Label();
            this.txtMzTemp = new System.Windows.Forms.TextBox();
            this.txtPcbTempSense = new System.Windows.Forms.TextBox();
            this.lbl5v2Sense = new System.Windows.Forms.Label();
            this.txt5v2Sense = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.RxTimerEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.edfaPowerControl = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.evalDigiOut3 = new System.Windows.Forms.CheckBox();
            this.evalDigiOut2 = new System.Windows.Forms.CheckBox();
            this.evalDigiOut1 = new System.Windows.Forms.CheckBox();
            this.evalDigiOut0 = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.evalPcbVref = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.evalPcbStm32Temp = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.evalEdfaPower = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.evalPcbTemp = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ReadRx = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Rx2NegVout = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Rx2PosVout = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Rx2NegMon_I = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Rx2PosMon_I = new System.Windows.Forms.TextBox();
            this.rx2PeakCtrlV = new ModuleController.UserControls.scrollboxAndSlider();
            this.rx2NegDcOffsetV = new ModuleController.UserControls.scrollboxAndSlider();
            this.rx2PosDcOffsetV = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.demodulatorThermistor = new System.Windows.Forms.TextBox();
            this.tecCtrl = new ModuleController.UserControls.scrollboxAndSlider();
            this.arm2HeaterCtrl = new ModuleController.UserControls.scrollboxAndSlider();
            this.arm1HeaterCtrl = new ModuleController.UserControls.scrollboxAndSlider();
            this.mainHeaterCtrl = new ModuleController.UserControls.scrollboxAndSlider();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Rx1NegVout = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Rx1PosVout = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Rx1NegMon_I = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Rx1PosMon_I = new System.Windows.Forms.TextBox();
            this.rx1PeakCtrlV = new ModuleController.UserControls.scrollboxAndSlider();
            this.rx1NegDcOffsetV = new ModuleController.UserControls.scrollboxAndSlider();
            this.rx1PosDcOffsetV = new ModuleController.UserControls.scrollboxAndSlider();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.messageLog = new System.Windows.Forms.TextBox();
            this.ckLoggingEnabled = new System.Windows.Forms.CheckBox();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReadStr = new System.Windows.Forms.TextBox();
            this.txtWriteStr = new System.Windows.Forms.TextBox();
            this.btnReadStr = new System.Windows.Forms.Button();
            this.btnWriteStr = new System.Windows.Forms.Button();
            this.btnReadVal = new System.Windows.Forms.Button();
            this.btnWriteByte = new System.Windows.Forms.Button();
            this.txtReadByte = new System.Windows.Forms.TextBox();
            this.txtByteVal = new System.Windows.Forms.TextBox();
            this.lblRegisterNo = new System.Windows.Forms.Label();
            this.txtRegisterNumber = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtUnlockStatus = new System.Windows.Forms.TextBox();
            this.btnCheckLockStatus = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnSendPassword = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.calibrationTabs = new System.Windows.Forms.TabControl();
            this.calTabDAC = new System.Windows.Forms.TabPage();
            this.dacCalStartCal = new System.Windows.Forms.Button();
            this.dacCalSaveFile = new System.Windows.Forms.Button();
            this.dacCalLoadFile = new System.Windows.Forms.Button();
            this.dacCalDataGrid = new System.Windows.Forms.DataGridView();
            this.dacCalOverallProgressBar = new System.Windows.Forms.ProgressBar();
            this.dacCalChannelProgressBar = new System.Windows.Forms.ProgressBar();
            this.calTabGoldBox = new System.Windows.Forms.TabPage();
            this.loadGBData = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.calTabPlots = new System.Windows.Forms.TabPage();
            this.btnNextPlot = new System.Windows.Forms.Button();
            this.btnPreviousPlot = new System.Windows.Forms.Button();
            this.plotSurface2D1 = new NPlot.Windows.PlotSurface2D();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.mzTabTimer = new System.Windows.Forms.Timer(this.components);
            this.laserTabTimer = new System.Windows.Forms.Timer(this.components);
            this.cmbReadingMode = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.debugLogTimer = new System.Windows.Forms.Timer(this.components);
            this.rxTabTimer = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.setupFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channelSelect)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBottomDataModCtrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBottomDataModCtrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkTopDataModCtrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTopDataModCtrl)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.calibrationTabs.SuspendLayout();
            this.calTabDAC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dacCalDataGrid)).BeginInit();
            this.calTabGoldBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.calTabPlots.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage6);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage7);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(16, 34);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(744, 494);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(736, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btnReadUpperModuleIdRegisters);
            this.groupBox12.Controls.Add(this.btnWriteModuleIdentity);
            this.groupBox12.Controls.Add(this.label14);
            this.groupBox12.Controls.Add(this.txtFwVersionWrite);
            this.groupBox12.Controls.Add(this.label15);
            this.groupBox12.Controls.Add(this.txtBuildDateWrite);
            this.groupBox12.Controls.Add(this.label16);
            this.groupBox12.Controls.Add(this.txtPcbSerialWrite);
            this.groupBox12.Controls.Add(this.label17);
            this.groupBox12.Controls.Add(this.tstLaserSerialWrite);
            this.groupBox12.Controls.Add(this.label18);
            this.groupBox12.Controls.Add(this.txtModSerialWrite);
            this.groupBox12.Location = new System.Drawing.Point(6, 58);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(260, 213);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Module Identity - Setup";
            // 
            // btnReadUpperModuleIdRegisters
            // 
            this.btnReadUpperModuleIdRegisters.Location = new System.Drawing.Point(179, 181);
            this.btnReadUpperModuleIdRegisters.Name = "btnReadUpperModuleIdRegisters";
            this.btnReadUpperModuleIdRegisters.Size = new System.Drawing.Size(64, 24);
            this.btnReadUpperModuleIdRegisters.TabIndex = 21;
            this.btnReadUpperModuleIdRegisters.Text = "Read";
            this.btnReadUpperModuleIdRegisters.UseVisualStyleBackColor = true;
            this.btnReadUpperModuleIdRegisters.Click += new System.EventHandler(this.btnReadUpperModuleIdRegisters_Click);
            // 
            // btnWriteModuleIdentity
            // 
            this.btnWriteModuleIdentity.Location = new System.Drawing.Point(109, 181);
            this.btnWriteModuleIdentity.Name = "btnWriteModuleIdentity";
            this.btnWriteModuleIdentity.Size = new System.Drawing.Size(64, 24);
            this.btnWriteModuleIdentity.TabIndex = 20;
            this.btnWriteModuleIdentity.Text = "Write";
            this.btnWriteModuleIdentity.UseVisualStyleBackColor = true;
            this.btnWriteModuleIdentity.Click += new System.EventHandler(this.btnWriteModuleIdentity_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 132);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "FW Version";
            // 
            // txtFwVersionWrite
            // 
            this.txtFwVersionWrite.Location = new System.Drawing.Point(80, 129);
            this.txtFwVersionWrite.Name = "txtFwVersionWrite";
            this.txtFwVersionWrite.Size = new System.Drawing.Size(163, 20);
            this.txtFwVersionWrite.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Build Date";
            // 
            // txtBuildDateWrite
            // 
            this.txtBuildDateWrite.Location = new System.Drawing.Point(80, 103);
            this.txtBuildDateWrite.Name = "txtBuildDateWrite";
            this.txtBuildDateWrite.Size = new System.Drawing.Size(163, 20);
            this.txtBuildDateWrite.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Pcb Serial";
            // 
            // txtPcbSerialWrite
            // 
            this.txtPcbSerialWrite.Location = new System.Drawing.Point(80, 77);
            this.txtPcbSerialWrite.Name = "txtPcbSerialWrite";
            this.txtPcbSerialWrite.Size = new System.Drawing.Size(163, 20);
            this.txtPcbSerialWrite.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 54);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Laser Serial";
            // 
            // tstLaserSerialWrite
            // 
            this.tstLaserSerialWrite.Location = new System.Drawing.Point(80, 51);
            this.tstLaserSerialWrite.Name = "tstLaserSerialWrite";
            this.tstLaserSerialWrite.Size = new System.Drawing.Size(163, 20);
            this.tstLaserSerialWrite.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "Module Serial";
            // 
            // txtModSerialWrite
            // 
            this.txtModSerialWrite.Location = new System.Drawing.Point(80, 25);
            this.txtModSerialWrite.Name = "txtModSerialWrite";
            this.txtModSerialWrite.Size = new System.Drawing.Size(163, 20);
            this.txtModSerialWrite.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(244, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(230, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "Black Box Control Utility";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btnReadModuleIdentity);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.txtFwBackRO);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.txtFwRelRO);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Controls.Add(this.txtMfgDateRO);
            this.groupBox11.Controls.Add(this.label9);
            this.groupBox11.Controls.Add(this.txtModelRO);
            this.groupBox11.Controls.Add(this.label8);
            this.groupBox11.Controls.Add(this.txtManufacturerRO);
            this.groupBox11.Controls.Add(this.label7);
            this.groupBox11.Controls.Add(this.txtDevTypeRO);
            this.groupBox11.Location = new System.Drawing.Point(272, 58);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(259, 214);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Module Identity - Readback";
            // 
            // btnReadModuleIdentity
            // 
            this.btnReadModuleIdentity.Location = new System.Drawing.Point(179, 181);
            this.btnReadModuleIdentity.Name = "btnReadModuleIdentity";
            this.btnReadModuleIdentity.Size = new System.Drawing.Size(64, 24);
            this.btnReadModuleIdentity.TabIndex = 12;
            this.btnReadModuleIdentity.Text = "Read";
            this.btnReadModuleIdentity.UseVisualStyleBackColor = true;
            this.btnReadModuleIdentity.Click += new System.EventHandler(this.btnReadModuleIdentity_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "FW Back";
            // 
            // txtFwBackRO
            // 
            this.txtFwBackRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtFwBackRO.Location = new System.Drawing.Point(80, 155);
            this.txtFwBackRO.Name = "txtFwBackRO";
            this.txtFwBackRO.ReadOnly = true;
            this.txtFwBackRO.Size = new System.Drawing.Size(163, 20);
            this.txtFwBackRO.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 132);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "FW Rel";
            // 
            // txtFwRelRO
            // 
            this.txtFwRelRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtFwRelRO.Location = new System.Drawing.Point(80, 129);
            this.txtFwRelRO.Name = "txtFwRelRO";
            this.txtFwRelRO.ReadOnly = true;
            this.txtFwRelRO.Size = new System.Drawing.Size(163, 20);
            this.txtFwRelRO.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Mfg. Date";
            // 
            // txtMfgDateRO
            // 
            this.txtMfgDateRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtMfgDateRO.Location = new System.Drawing.Point(80, 103);
            this.txtMfgDateRO.Name = "txtMfgDateRO";
            this.txtMfgDateRO.ReadOnly = true;
            this.txtMfgDateRO.Size = new System.Drawing.Size(163, 20);
            this.txtMfgDateRO.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Model";
            // 
            // txtModelRO
            // 
            this.txtModelRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtModelRO.Location = new System.Drawing.Point(80, 77);
            this.txtModelRO.Name = "txtModelRO";
            this.txtModelRO.ReadOnly = true;
            this.txtModelRO.Size = new System.Drawing.Size(163, 20);
            this.txtModelRO.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Manufacturer";
            // 
            // txtManufacturerRO
            // 
            this.txtManufacturerRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtManufacturerRO.Location = new System.Drawing.Point(80, 51);
            this.txtManufacturerRO.Name = "txtManufacturerRO";
            this.txtManufacturerRO.ReadOnly = true;
            this.txtManufacturerRO.Size = new System.Drawing.Size(163, 20);
            this.txtManufacturerRO.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Device Type";
            // 
            // txtDevTypeRO
            // 
            this.txtDevTypeRO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDevTypeRO.Location = new System.Drawing.Point(80, 25);
            this.txtDevTypeRO.Name = "txtDevTypeRO";
            this.txtDevTypeRO.ReadOnly = true;
            this.txtDevTypeRO.Size = new System.Drawing.Size(163, 20);
            this.txtDevTypeRO.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ckLaserAutoUpdate);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.btnRead);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(736, 468);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Laser Control";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ckLaserAutoUpdate
            // 
            this.ckLaserAutoUpdate.AutoSize = true;
            this.ckLaserAutoUpdate.Location = new System.Drawing.Point(614, 405);
            this.ckLaserAutoUpdate.Name = "ckLaserAutoUpdate";
            this.ckLaserAutoUpdate.Size = new System.Drawing.Size(108, 17);
            this.ckLaserAutoUpdate.TabIndex = 11;
            this.ckLaserAutoUpdate.Text = "Continuous Read";
            this.ckLaserAutoUpdate.UseVisualStyleBackColor = true;
            this.ckLaserAutoUpdate.CheckedChanged += new System.EventHandler(this.ckLaserAutoUpdate_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtLsPhaseSense);
            this.groupBox6.Controls.Add(this.lblLsPhaseSense);
            this.groupBox6.Controls.Add(this.lblLaserMon);
            this.groupBox6.Controls.Add(this.txtLaserMon);
            this.groupBox6.Controls.Add(this.lblRxMon);
            this.groupBox6.Controls.Add(this.txtRxMon);
            this.groupBox6.Controls.Add(this.lblTxMon);
            this.groupBox6.Controls.Add(this.txtTxMon);
            this.groupBox6.Location = new System.Drawing.Point(249, 123);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(167, 135);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Monitors";
            // 
            // txtLsPhaseSense
            // 
            this.txtLsPhaseSense.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLsPhaseSense.Location = new System.Drawing.Point(87, 104);
            this.txtLsPhaseSense.Name = "txtLsPhaseSense";
            this.txtLsPhaseSense.ReadOnly = true;
            this.txtLsPhaseSense.Size = new System.Drawing.Size(56, 20);
            this.txtLsPhaseSense.TabIndex = 11;
            // 
            // lblLsPhaseSense
            // 
            this.lblLsPhaseSense.AutoSize = true;
            this.lblLsPhaseSense.Location = new System.Drawing.Point(6, 104);
            this.lblLsPhaseSense.Name = "lblLsPhaseSense";
            this.lblLsPhaseSense.Size = new System.Drawing.Size(78, 13);
            this.lblLsPhaseSense.TabIndex = 10;
            this.lblLsPhaseSense.Text = "LsPhaseSense";
            this.lblLsPhaseSense.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaserMon
            // 
            this.lblLaserMon.AutoSize = true;
            this.lblLaserMon.Location = new System.Drawing.Point(6, 78);
            this.lblLaserMon.Name = "lblLaserMon";
            this.lblLaserMon.Size = new System.Drawing.Size(57, 13);
            this.lblLaserMon.TabIndex = 8;
            this.lblLaserMon.Text = "Laser Mon";
            this.lblLaserMon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLaserMon
            // 
            this.txtLaserMon.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLaserMon.Location = new System.Drawing.Point(87, 78);
            this.txtLaserMon.Name = "txtLaserMon";
            this.txtLaserMon.ReadOnly = true;
            this.txtLaserMon.Size = new System.Drawing.Size(56, 20);
            this.txtLaserMon.TabIndex = 7;
            // 
            // lblRxMon
            // 
            this.lblRxMon.AutoSize = true;
            this.lblRxMon.Location = new System.Drawing.Point(6, 52);
            this.lblRxMon.Name = "lblRxMon";
            this.lblRxMon.Size = new System.Drawing.Size(44, 13);
            this.lblRxMon.TabIndex = 4;
            this.lblRxMon.Text = "Rx Mon";
            this.lblRxMon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRxMon
            // 
            this.txtRxMon.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtRxMon.Location = new System.Drawing.Point(87, 52);
            this.txtRxMon.Name = "txtRxMon";
            this.txtRxMon.ReadOnly = true;
            this.txtRxMon.Size = new System.Drawing.Size(56, 20);
            this.txtRxMon.TabIndex = 3;
            // 
            // lblTxMon
            // 
            this.lblTxMon.AutoSize = true;
            this.lblTxMon.Location = new System.Drawing.Point(6, 26);
            this.lblTxMon.Name = "lblTxMon";
            this.lblTxMon.Size = new System.Drawing.Size(43, 13);
            this.lblTxMon.TabIndex = 2;
            this.lblTxMon.Text = "Tx Mon";
            this.lblTxMon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTxMon
            // 
            this.txtTxMon.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtTxMon.Location = new System.Drawing.Point(87, 26);
            this.txtTxMon.Name = "txtTxMon";
            this.txtTxMon.ReadOnly = true;
            this.txtTxMon.Size = new System.Drawing.Size(56, 20);
            this.txtTxMon.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSaveSetup);
            this.groupBox5.Controls.Add(this.channelSelect);
            this.groupBox5.Controls.Add(this.btnRecallChannel);
            this.groupBox5.Controls.Add(this.btnSelectChannel);
            this.groupBox5.Location = new System.Drawing.Point(249, 36);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(466, 81);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Channel";
            // 
            // btnSaveSetup
            // 
            this.btnSaveSetup.Location = new System.Drawing.Point(365, 30);
            this.btnSaveSetup.Name = "btnSaveSetup";
            this.btnSaveSetup.Size = new System.Drawing.Size(82, 29);
            this.btnSaveSetup.TabIndex = 10;
            this.btnSaveSetup.Text = "Save Setup";
            this.btnSaveSetup.UseVisualStyleBackColor = true;
            this.btnSaveSetup.Click += new System.EventHandler(this.btnSaveSetup_Click);
            // 
            // channelSelect
            // 
            this.channelSelect.BackColor = System.Drawing.Color.Black;
            this.channelSelect.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channelSelect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.channelSelect.Location = new System.Drawing.Point(20, 29);
            this.channelSelect.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.channelSelect.Name = "channelSelect";
            this.channelSelect.Size = new System.Drawing.Size(61, 29);
            this.channelSelect.TabIndex = 2;
            // 
            // btnRecallChannel
            // 
            this.btnRecallChannel.Location = new System.Drawing.Point(275, 30);
            this.btnRecallChannel.Name = "btnRecallChannel";
            this.btnRecallChannel.Size = new System.Drawing.Size(84, 29);
            this.btnRecallChannel.TabIndex = 1;
            this.btnRecallChannel.Text = "Recall Setup";
            this.btnRecallChannel.UseVisualStyleBackColor = true;
            this.btnRecallChannel.Click += new System.EventHandler(this.btnRecallChannel_Click);
            // 
            // btnSelectChannel
            // 
            this.btnSelectChannel.Location = new System.Drawing.Point(87, 29);
            this.btnSelectChannel.Name = "btnSelectChannel";
            this.btnSelectChannel.Size = new System.Drawing.Size(64, 29);
            this.btnSelectChannel.TabIndex = 0;
            this.btnSelectChannel.Text = "Select";
            this.btnSelectChannel.UseVisualStyleBackColor = true;
            this.btnSelectChannel.Click += new System.EventHandler(this.btnSelectChannel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dsdbrSectionsCtrl1);
            this.groupBox1.Location = new System.Drawing.Point(19, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(224, 222);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DSDBR";
            // 
            // dsdbrSectionsCtrl1
            // 
            this.dsdbrSectionsCtrl1.Location = new System.Drawing.Point(3, 19);
            this.dsdbrSectionsCtrl1.Name = "dsdbrSectionsCtrl1";
            this.dsdbrSectionsCtrl1.Size = new System.Drawing.Size(215, 192);
            this.dsdbrSectionsCtrl1.TabIndex = 1;
            this.dsdbrSectionsCtrl1.Load += new System.EventHandler(this.dsdbrSectionsCtrl1_Load);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(549, 405);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(59, 31);
            this.btnRead.TabIndex = 4;
            this.btnRead.Text = "Read";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.absoluteMode);
            this.panel1.Controls.Add(this.dacMode);
            this.panel1.Location = new System.Drawing.Point(19, 405);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(206, 49);
            this.panel1.TabIndex = 3;
            // 
            // absoluteMode
            // 
            this.absoluteMode.AutoSize = true;
            this.absoluteMode.Enabled = false;
            this.absoluteMode.Location = new System.Drawing.Point(3, 26);
            this.absoluteMode.Name = "absoluteMode";
            this.absoluteMode.Size = new System.Drawing.Size(140, 17);
            this.absoluteMode.TabIndex = 1;
            this.absoluteMode.Text = "Current / Voltage values";
            this.absoluteMode.UseVisualStyleBackColor = true;
            this.absoluteMode.CheckedChanged += new System.EventHandler(this.absoluteMode_CheckedChanged);
            // 
            // dacMode
            // 
            this.dacMode.AutoSize = true;
            this.dacMode.Checked = true;
            this.dacMode.Location = new System.Drawing.Point(3, 3);
            this.dacMode.Name = "dacMode";
            this.dacMode.Size = new System.Drawing.Size(81, 17);
            this.dacMode.TabIndex = 0;
            this.dacMode.TabStop = true;
            this.dacMode.Text = "DAC values";
            this.dacMode.UseVisualStyleBackColor = true;
            this.dacMode.CheckedChanged += new System.EventHandler(this.dacMode_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(277, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Device control";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label32);
            this.tabPage6.Controls.Add(this.groupBox13);
            this.tabPage6.Controls.Add(this.groupBox2);
            this.tabPage6.Controls.Add(this.groupBox14);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(736, 468);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "MZ";
            this.tabPage6.UseVisualStyleBackColor = true;
            this.tabPage6.Click += new System.EventHandler(this.tabPage6_Click);
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(264, 9);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(110, 24);
            this.label32.TabIndex = 12;
            this.label32.Text = "MZ control";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.numBottomDataModCtrl);
            this.groupBox13.Controls.Add(this.tkBottomDataModCtrl);
            this.groupBox13.Controls.Add(this.label20);
            this.groupBox13.Controls.Add(this.label19);
            this.groupBox13.Controls.Add(this.tkTopDataModCtrl);
            this.groupBox13.Controls.Add(this.numTopDataModCtrl);
            this.groupBox13.Location = new System.Drawing.Point(23, 337);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(242, 88);
            this.groupBox13.TabIndex = 11;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Modulation Control";
            // 
            // numBottomDataModCtrl
            // 
            this.numBottomDataModCtrl.Location = new System.Drawing.Point(169, 52);
            this.numBottomDataModCtrl.Name = "numBottomDataModCtrl";
            this.numBottomDataModCtrl.Size = new System.Drawing.Size(50, 20);
            this.numBottomDataModCtrl.TabIndex = 5;
            this.numBottomDataModCtrl.ValueChanged += new System.EventHandler(this.numBottomDataModCtrl_ValueChanged);
            // 
            // tkBottomDataModCtrl
            // 
            this.tkBottomDataModCtrl.LargeChange = 50;
            this.tkBottomDataModCtrl.Location = new System.Drawing.Point(97, 52);
            this.tkBottomDataModCtrl.Name = "tkBottomDataModCtrl";
            this.tkBottomDataModCtrl.Size = new System.Drawing.Size(66, 34);
            this.tkBottomDataModCtrl.TabIndex = 4;
            this.tkBottomDataModCtrl.Scroll += new System.EventHandler(this.tkBottomDataModCtrl_Scroll);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(30, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 12);
            this.label20.TabIndex = 3;
            this.label20.Text = "Bottom Data";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(30, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "Top Data";
            // 
            // tkTopDataModCtrl
            // 
            this.tkTopDataModCtrl.LargeChange = 50;
            this.tkTopDataModCtrl.Location = new System.Drawing.Point(97, 19);
            this.tkTopDataModCtrl.Name = "tkTopDataModCtrl";
            this.tkTopDataModCtrl.Size = new System.Drawing.Size(66, 34);
            this.tkTopDataModCtrl.TabIndex = 1;
            this.tkTopDataModCtrl.Scroll += new System.EventHandler(this.tkTopDataModCtrl_Scroll);
            // 
            // numTopDataModCtrl
            // 
            this.numTopDataModCtrl.Location = new System.Drawing.Point(169, 19);
            this.numTopDataModCtrl.Name = "numTopDataModCtrl";
            this.numTopDataModCtrl.Size = new System.Drawing.Size(50, 20);
            this.numTopDataModCtrl.TabIndex = 0;
            this.numTopDataModCtrl.ValueChanged += new System.EventHandler(this.numTopDataModCtrl_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(23, 55);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(484, 276);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MZ";
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(223, -22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(144, 24);
            this.label30.TabIndex = 12;
            this.label30.Text = "Device control";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.mzSectionsCtrl_L, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mzSectionsCtrl_R, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.mzSectionsCtrl_O, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.mzSectionsCtrl_PC, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(472, 250);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // mzSectionsCtrl_L
            // 
            this.mzSectionsCtrl_L.EnableSection = false;
            this.mzSectionsCtrl_L.HideRfControls = false;
            this.mzSectionsCtrl_L.Location = new System.Drawing.Point(3, 3);
            this.mzSectionsCtrl_L.Name = "mzSectionsCtrl_L";
            this.mzSectionsCtrl_L.SendImmediately = false;
            this.mzSectionsCtrl_L.Size = new System.Drawing.Size(210, 119);
            this.mzSectionsCtrl_L.TabIndex = 0;
            this.mzSectionsCtrl_L.Value = mzBiasSettings1;
            // 
            // mzSectionsCtrl_R
            // 
            this.mzSectionsCtrl_R.EnableSection = false;
            this.mzSectionsCtrl_R.HideRfControls = false;
            this.mzSectionsCtrl_R.Location = new System.Drawing.Point(239, 3);
            this.mzSectionsCtrl_R.Name = "mzSectionsCtrl_R";
            this.mzSectionsCtrl_R.SendImmediately = false;
            this.mzSectionsCtrl_R.Size = new System.Drawing.Size(219, 119);
            this.mzSectionsCtrl_R.TabIndex = 1;
            this.mzSectionsCtrl_R.Value = mzBiasSettings2;
            this.mzSectionsCtrl_R.Load += new System.EventHandler(this.mzSectionsCtrl_R_Load);
            // 
            // mzSectionsCtrl_O
            // 
            this.mzSectionsCtrl_O.EnableSection = false;
            this.mzSectionsCtrl_O.HideRfControls = false;
            this.mzSectionsCtrl_O.Location = new System.Drawing.Point(3, 128);
            this.mzSectionsCtrl_O.Name = "mzSectionsCtrl_O";
            this.mzSectionsCtrl_O.SendImmediately = false;
            this.mzSectionsCtrl_O.Size = new System.Drawing.Size(220, 119);
            this.mzSectionsCtrl_O.TabIndex = 2;
            this.mzSectionsCtrl_O.Value = mzBiasSettings3;
            // 
            // mzSectionsCtrl_PC
            // 
            this.mzSectionsCtrl_PC.EnableSection = false;
            this.mzSectionsCtrl_PC.HideRfControls = false;
            this.mzSectionsCtrl_PC.Location = new System.Drawing.Point(239, 128);
            this.mzSectionsCtrl_PC.Name = "mzSectionsCtrl_PC";
            this.mzSectionsCtrl_PC.SendImmediately = false;
            this.mzSectionsCtrl_PC.Size = new System.Drawing.Size(219, 119);
            this.mzSectionsCtrl_PC.TabIndex = 3;
            this.mzSectionsCtrl_PC.Value = mzBiasSettings4;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.cbFastUpdate);
            this.groupBox14.Controls.Add(this.checkAutoUpdate);
            this.groupBox14.Controls.Add(this.btnUpdate);
            this.groupBox14.Controls.Add(this.lblCarverTapRf);
            this.groupBox14.Controls.Add(this.txtCarverTapRf);
            this.groupBox14.Controls.Add(this.lblOutputRfDc);
            this.groupBox14.Controls.Add(this.txtOutputRfDc);
            this.groupBox14.Controls.Add(this.lblOutInlineDc);
            this.groupBox14.Controls.Add(this.txtOutInlineDc);
            this.groupBox14.Controls.Add(this.lblLmmiDc);
            this.groupBox14.Controls.Add(this.txtLmmiDc);
            this.groupBox14.Controls.Add(this.lblUmmiDc);
            this.groupBox14.Controls.Add(this.txtUmmiDc);
            this.groupBox14.Controls.Add(this.lblCarverTap);
            this.groupBox14.Controls.Add(this.txtCarverTap);
            this.groupBox14.Location = new System.Drawing.Point(513, 55);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(162, 276);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "MZ Monitors";
            // 
            // cbFastUpdate
            // 
            this.cbFastUpdate.AutoSize = true;
            this.cbFastUpdate.Location = new System.Drawing.Point(32, 249);
            this.cbFastUpdate.Name = "cbFastUpdate";
            this.cbFastUpdate.Size = new System.Drawing.Size(98, 17);
            this.cbFastUpdate.TabIndex = 40;
            this.cbFastUpdate.Text = "Average Rf DC";
            this.cbFastUpdate.UseVisualStyleBackColor = true;
            this.cbFastUpdate.CheckedChanged += new System.EventHandler(this.cbFastUpdate_CheckedChanged);
            // 
            // checkAutoUpdate
            // 
            this.checkAutoUpdate.AutoSize = true;
            this.checkAutoUpdate.Location = new System.Drawing.Point(32, 228);
            this.checkAutoUpdate.Name = "checkAutoUpdate";
            this.checkAutoUpdate.Size = new System.Drawing.Size(86, 17);
            this.checkAutoUpdate.TabIndex = 39;
            this.checkAutoUpdate.Text = "Auto Update";
            this.checkAutoUpdate.UseVisualStyleBackColor = true;
            this.checkAutoUpdate.CheckedChanged += new System.EventHandler(this.checkAutoUpdate_CheckedChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(32, 197);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(88, 25);
            this.btnUpdate.TabIndex = 38;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblCarverTapRf
            // 
            this.lblCarverTapRf.AutoSize = true;
            this.lblCarverTapRf.Location = new System.Drawing.Point(15, 149);
            this.lblCarverTapRf.Name = "lblCarverTapRf";
            this.lblCarverTapRf.Size = new System.Drawing.Size(69, 13);
            this.lblCarverTapRf.TabIndex = 36;
            this.lblCarverTapRf.Text = "Carver Tap 2";
            this.lblCarverTapRf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCarverTapRf
            // 
            this.txtCarverTapRf.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtCarverTapRf.Location = new System.Drawing.Point(88, 149);
            this.txtCarverTapRf.Name = "txtCarverTapRf";
            this.txtCarverTapRf.ReadOnly = true;
            this.txtCarverTapRf.Size = new System.Drawing.Size(56, 20);
            this.txtCarverTapRf.TabIndex = 35;
            // 
            // lblOutputRfDc
            // 
            this.lblOutputRfDc.AutoSize = true;
            this.lblOutputRfDc.Location = new System.Drawing.Point(15, 123);
            this.lblOutputRfDc.Name = "lblOutputRfDc";
            this.lblOutputRfDc.Size = new System.Drawing.Size(62, 13);
            this.lblOutputRfDc.TabIndex = 34;
            this.lblOutputRfDc.Text = "O/P RF DC";
            this.lblOutputRfDc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOutputRfDc
            // 
            this.txtOutputRfDc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtOutputRfDc.Location = new System.Drawing.Point(88, 123);
            this.txtOutputRfDc.Name = "txtOutputRfDc";
            this.txtOutputRfDc.ReadOnly = true;
            this.txtOutputRfDc.Size = new System.Drawing.Size(56, 20);
            this.txtOutputRfDc.TabIndex = 33;
            // 
            // lblOutInlineDc
            // 
            this.lblOutInlineDc.AutoSize = true;
            this.lblOutInlineDc.Location = new System.Drawing.Point(15, 97);
            this.lblOutInlineDc.Name = "lblOutInlineDc";
            this.lblOutInlineDc.Size = new System.Drawing.Size(70, 13);
            this.lblOutInlineDc.TabIndex = 32;
            this.lblOutInlineDc.Text = "Out Inline DC";
            this.lblOutInlineDc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOutInlineDc
            // 
            this.txtOutInlineDc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtOutInlineDc.Location = new System.Drawing.Point(88, 97);
            this.txtOutInlineDc.Name = "txtOutInlineDc";
            this.txtOutInlineDc.ReadOnly = true;
            this.txtOutInlineDc.Size = new System.Drawing.Size(56, 20);
            this.txtOutInlineDc.TabIndex = 31;
            // 
            // lblLmmiDc
            // 
            this.lblLmmiDc.AutoSize = true;
            this.lblLmmiDc.Location = new System.Drawing.Point(15, 71);
            this.lblLmmiDc.Name = "lblLmmiDc";
            this.lblLmmiDc.Size = new System.Drawing.Size(52, 13);
            this.lblLmmiDc.TabIndex = 30;
            this.lblLmmiDc.Text = "LMMI DC";
            this.lblLmmiDc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLmmiDc
            // 
            this.txtLmmiDc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLmmiDc.Location = new System.Drawing.Point(88, 71);
            this.txtLmmiDc.Name = "txtLmmiDc";
            this.txtLmmiDc.ReadOnly = true;
            this.txtLmmiDc.Size = new System.Drawing.Size(56, 20);
            this.txtLmmiDc.TabIndex = 29;
            // 
            // lblUmmiDc
            // 
            this.lblUmmiDc.AutoSize = true;
            this.lblUmmiDc.Location = new System.Drawing.Point(15, 45);
            this.lblUmmiDc.Name = "lblUmmiDc";
            this.lblUmmiDc.Size = new System.Drawing.Size(54, 13);
            this.lblUmmiDc.TabIndex = 28;
            this.lblUmmiDc.Text = "UMMI DC";
            this.lblUmmiDc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUmmiDc
            // 
            this.txtUmmiDc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtUmmiDc.Location = new System.Drawing.Point(88, 45);
            this.txtUmmiDc.Name = "txtUmmiDc";
            this.txtUmmiDc.ReadOnly = true;
            this.txtUmmiDc.Size = new System.Drawing.Size(56, 20);
            this.txtUmmiDc.TabIndex = 27;
            // 
            // lblCarverTap
            // 
            this.lblCarverTap.AutoSize = true;
            this.lblCarverTap.Location = new System.Drawing.Point(15, 22);
            this.lblCarverTap.Name = "lblCarverTap";
            this.lblCarverTap.Size = new System.Drawing.Size(60, 13);
            this.lblCarverTap.TabIndex = 26;
            this.lblCarverTap.Text = "Carver Tap";
            this.lblCarverTap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCarverTap
            // 
            this.txtCarverTap.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtCarverTap.Location = new System.Drawing.Point(88, 19);
            this.txtCarverTap.Name = "txtCarverTap";
            this.txtCarverTap.ReadOnly = true;
            this.txtCarverTap.Size = new System.Drawing.Size(56, 20);
            this.txtCarverTap.TabIndex = 25;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Controls.Add(this.groupBox9);
            this.tabPage5.Controls.Add(this.groupBox8);
            this.tabPage5.Controls.Add(this.btnReadTcmzMonitors);
            this.tabPage5.Controls.Add(this.groupBox7);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(736, 468);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Device Control";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.ckCtrlReg0);
            this.groupBox10.Controls.Add(this.ckCtrlReg15);
            this.groupBox10.Controls.Add(this.ckCtrlReg14);
            this.groupBox10.Controls.Add(this.ckCtrlReg13);
            this.groupBox10.Controls.Add(this.ckCtrlReg12);
            this.groupBox10.Controls.Add(this.ckCtrlReg11);
            this.groupBox10.Controls.Add(this.ckCtrlReg10);
            this.groupBox10.Controls.Add(this.ckCtrlReg9);
            this.groupBox10.Controls.Add(this.ckCtrlReg8);
            this.groupBox10.Controls.Add(this.ckCtrlReg7);
            this.groupBox10.Controls.Add(this.ckCtrlReg6);
            this.groupBox10.Controls.Add(this.ckCtrlReg5);
            this.groupBox10.Controls.Add(this.ckCtrlReg4);
            this.groupBox10.Controls.Add(this.ckCtrlReg3);
            this.groupBox10.Controls.Add(this.ckCtrlReg2);
            this.groupBox10.Controls.Add(this.ckCtrlReg1);
            this.groupBox10.Location = new System.Drawing.Point(6, 237);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(360, 197);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Control Register";
            // 
            // ckCtrlReg0
            // 
            this.ckCtrlReg0.AutoSize = true;
            this.ckCtrlReg0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg0.Location = new System.Drawing.Point(6, 18);
            this.ckCtrlReg0.Name = "ckCtrlReg0";
            this.ckCtrlReg0.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg0.TabIndex = 18;
            this.ckCtrlReg0.Text = "bit 0";
            this.ckCtrlReg0.UseVisualStyleBackColor = true;
            this.ckCtrlReg0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg15
            // 
            this.ckCtrlReg15.AutoSize = true;
            this.ckCtrlReg15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg15.Location = new System.Drawing.Point(182, 176);
            this.ckCtrlReg15.Name = "ckCtrlReg15";
            this.ckCtrlReg15.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg15.TabIndex = 17;
            this.ckCtrlReg15.Text = "bit 15";
            this.ckCtrlReg15.UseVisualStyleBackColor = true;
            this.ckCtrlReg15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg14
            // 
            this.ckCtrlReg14.AutoSize = true;
            this.ckCtrlReg14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg14.Location = new System.Drawing.Point(182, 153);
            this.ckCtrlReg14.Name = "ckCtrlReg14";
            this.ckCtrlReg14.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg14.TabIndex = 16;
            this.ckCtrlReg14.Text = "bit 14";
            this.ckCtrlReg14.UseVisualStyleBackColor = true;
            this.ckCtrlReg14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg13
            // 
            this.ckCtrlReg13.AutoSize = true;
            this.ckCtrlReg13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg13.Location = new System.Drawing.Point(182, 131);
            this.ckCtrlReg13.Name = "ckCtrlReg13";
            this.ckCtrlReg13.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg13.TabIndex = 15;
            this.ckCtrlReg13.Text = "bit 13";
            this.ckCtrlReg13.UseVisualStyleBackColor = true;
            this.ckCtrlReg13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg12
            // 
            this.ckCtrlReg12.AutoSize = true;
            this.ckCtrlReg12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg12.Location = new System.Drawing.Point(182, 108);
            this.ckCtrlReg12.Name = "ckCtrlReg12";
            this.ckCtrlReg12.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg12.TabIndex = 14;
            this.ckCtrlReg12.Text = "bit 12";
            this.ckCtrlReg12.UseVisualStyleBackColor = true;
            this.ckCtrlReg12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg11
            // 
            this.ckCtrlReg11.AutoSize = true;
            this.ckCtrlReg11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg11.Location = new System.Drawing.Point(182, 86);
            this.ckCtrlReg11.Name = "ckCtrlReg11";
            this.ckCtrlReg11.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg11.TabIndex = 13;
            this.ckCtrlReg11.Text = "bit 11";
            this.ckCtrlReg11.UseVisualStyleBackColor = true;
            this.ckCtrlReg11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg10
            // 
            this.ckCtrlReg10.AutoSize = true;
            this.ckCtrlReg10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg10.Location = new System.Drawing.Point(182, 64);
            this.ckCtrlReg10.Name = "ckCtrlReg10";
            this.ckCtrlReg10.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlReg10.TabIndex = 12;
            this.ckCtrlReg10.Text = "bit 10";
            this.ckCtrlReg10.UseVisualStyleBackColor = true;
            this.ckCtrlReg10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg9
            // 
            this.ckCtrlReg9.AutoSize = true;
            this.ckCtrlReg9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg9.Location = new System.Drawing.Point(182, 40);
            this.ckCtrlReg9.Name = "ckCtrlReg9";
            this.ckCtrlReg9.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg9.TabIndex = 11;
            this.ckCtrlReg9.Text = "bit 9";
            this.ckCtrlReg9.UseVisualStyleBackColor = true;
            this.ckCtrlReg9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg8
            // 
            this.ckCtrlReg8.AutoSize = true;
            this.ckCtrlReg8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg8.Location = new System.Drawing.Point(182, 18);
            this.ckCtrlReg8.Name = "ckCtrlReg8";
            this.ckCtrlReg8.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg8.TabIndex = 10;
            this.ckCtrlReg8.Text = "bit 8";
            this.ckCtrlReg8.UseVisualStyleBackColor = true;
            this.ckCtrlReg8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg7
            // 
            this.ckCtrlReg7.AutoSize = true;
            this.ckCtrlReg7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg7.Location = new System.Drawing.Point(6, 177);
            this.ckCtrlReg7.Name = "ckCtrlReg7";
            this.ckCtrlReg7.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg7.TabIndex = 9;
            this.ckCtrlReg7.Text = "bit 7";
            this.ckCtrlReg7.UseVisualStyleBackColor = true;
            this.ckCtrlReg7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg6
            // 
            this.ckCtrlReg6.AutoSize = true;
            this.ckCtrlReg6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg6.Location = new System.Drawing.Point(6, 154);
            this.ckCtrlReg6.Name = "ckCtrlReg6";
            this.ckCtrlReg6.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg6.TabIndex = 8;
            this.ckCtrlReg6.Text = "bit 6";
            this.ckCtrlReg6.UseVisualStyleBackColor = true;
            this.ckCtrlReg6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg5
            // 
            this.ckCtrlReg5.AutoSize = true;
            this.ckCtrlReg5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg5.Location = new System.Drawing.Point(6, 132);
            this.ckCtrlReg5.Name = "ckCtrlReg5";
            this.ckCtrlReg5.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg5.TabIndex = 4;
            this.ckCtrlReg5.Text = "bit 5";
            this.ckCtrlReg5.UseVisualStyleBackColor = true;
            this.ckCtrlReg5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg4
            // 
            this.ckCtrlReg4.AutoSize = true;
            this.ckCtrlReg4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg4.Location = new System.Drawing.Point(6, 109);
            this.ckCtrlReg4.Name = "ckCtrlReg4";
            this.ckCtrlReg4.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg4.TabIndex = 3;
            this.ckCtrlReg4.Text = "bit 4";
            this.ckCtrlReg4.UseVisualStyleBackColor = true;
            this.ckCtrlReg4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg3
            // 
            this.ckCtrlReg3.AutoSize = true;
            this.ckCtrlReg3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg3.Location = new System.Drawing.Point(6, 87);
            this.ckCtrlReg3.Name = "ckCtrlReg3";
            this.ckCtrlReg3.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg3.TabIndex = 2;
            this.ckCtrlReg3.Text = "bit 3";
            this.ckCtrlReg3.UseVisualStyleBackColor = true;
            this.ckCtrlReg3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg2
            // 
            this.ckCtrlReg2.AutoSize = true;
            this.ckCtrlReg2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg2.Location = new System.Drawing.Point(6, 64);
            this.ckCtrlReg2.Name = "ckCtrlReg2";
            this.ckCtrlReg2.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg2.TabIndex = 1;
            this.ckCtrlReg2.Text = "bit 2";
            this.ckCtrlReg2.UseVisualStyleBackColor = true;
            this.ckCtrlReg2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // ckCtrlReg1
            // 
            this.ckCtrlReg1.AutoSize = true;
            this.ckCtrlReg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlReg1.Location = new System.Drawing.Point(6, 41);
            this.ckCtrlReg1.Name = "ckCtrlReg1";
            this.ckCtrlReg1.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlReg1.TabIndex = 0;
            this.ckCtrlReg1.Text = "bit 1";
            this.ckCtrlReg1.UseVisualStyleBackColor = true;
            this.ckCtrlReg1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlReg_MouseUp);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.ckCtrlStatus0);
            this.groupBox9.Controls.Add(this.ckCtrlStatus15);
            this.groupBox9.Controls.Add(this.ckCtrlStatus14);
            this.groupBox9.Controls.Add(this.ckCtrlStatus13);
            this.groupBox9.Controls.Add(this.ckCtrlStatus12);
            this.groupBox9.Controls.Add(this.ckCtrlStatus11);
            this.groupBox9.Controls.Add(this.ckCtrlStatus10);
            this.groupBox9.Controls.Add(this.ckCtrlStatus9);
            this.groupBox9.Controls.Add(this.ckCtrlStatus8);
            this.groupBox9.Controls.Add(this.ckCtrlStatus7);
            this.groupBox9.Controls.Add(this.ckCtrlStatus6);
            this.groupBox9.Controls.Add(this.ckCtrlStatus5);
            this.groupBox9.Controls.Add(this.ckCtrlStatus4);
            this.groupBox9.Controls.Add(this.ckCtrlStatus3);
            this.groupBox9.Controls.Add(this.ckCtrlStatus2);
            this.groupBox9.Controls.Add(this.ckCtrlStatus1);
            this.groupBox9.Location = new System.Drawing.Point(373, 30);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(336, 201);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Control Status";
            // 
            // ckCtrlStatus0
            // 
            this.ckCtrlStatus0.AutoSize = true;
            this.ckCtrlStatus0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus0.Location = new System.Drawing.Point(9, 19);
            this.ckCtrlStatus0.Name = "ckCtrlStatus0";
            this.ckCtrlStatus0.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus0.TabIndex = 18;
            this.ckCtrlStatus0.Text = "bit 0";
            this.ckCtrlStatus0.UseVisualStyleBackColor = true;
            this.ckCtrlStatus0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus15
            // 
            this.ckCtrlStatus15.AutoSize = true;
            this.ckCtrlStatus15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus15.Location = new System.Drawing.Point(169, 176);
            this.ckCtrlStatus15.Name = "ckCtrlStatus15";
            this.ckCtrlStatus15.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus15.TabIndex = 17;
            this.ckCtrlStatus15.Text = "bit 15";
            this.ckCtrlStatus15.UseVisualStyleBackColor = true;
            this.ckCtrlStatus15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus14
            // 
            this.ckCtrlStatus14.AutoSize = true;
            this.ckCtrlStatus14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus14.Location = new System.Drawing.Point(169, 153);
            this.ckCtrlStatus14.Name = "ckCtrlStatus14";
            this.ckCtrlStatus14.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus14.TabIndex = 16;
            this.ckCtrlStatus14.Text = "bit 14";
            this.ckCtrlStatus14.UseVisualStyleBackColor = true;
            this.ckCtrlStatus14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus13
            // 
            this.ckCtrlStatus13.AutoSize = true;
            this.ckCtrlStatus13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus13.Location = new System.Drawing.Point(169, 131);
            this.ckCtrlStatus13.Name = "ckCtrlStatus13";
            this.ckCtrlStatus13.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus13.TabIndex = 15;
            this.ckCtrlStatus13.Text = "bit 13";
            this.ckCtrlStatus13.UseVisualStyleBackColor = true;
            this.ckCtrlStatus13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus12
            // 
            this.ckCtrlStatus12.AutoSize = true;
            this.ckCtrlStatus12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus12.Location = new System.Drawing.Point(169, 108);
            this.ckCtrlStatus12.Name = "ckCtrlStatus12";
            this.ckCtrlStatus12.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus12.TabIndex = 14;
            this.ckCtrlStatus12.Text = "bit 12";
            this.ckCtrlStatus12.UseVisualStyleBackColor = true;
            this.ckCtrlStatus12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus11
            // 
            this.ckCtrlStatus11.AutoSize = true;
            this.ckCtrlStatus11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus11.Location = new System.Drawing.Point(169, 86);
            this.ckCtrlStatus11.Name = "ckCtrlStatus11";
            this.ckCtrlStatus11.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus11.TabIndex = 13;
            this.ckCtrlStatus11.Text = "bit 11";
            this.ckCtrlStatus11.UseVisualStyleBackColor = true;
            this.ckCtrlStatus11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus10
            // 
            this.ckCtrlStatus10.AutoSize = true;
            this.ckCtrlStatus10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus10.Location = new System.Drawing.Point(169, 64);
            this.ckCtrlStatus10.Name = "ckCtrlStatus10";
            this.ckCtrlStatus10.Size = new System.Drawing.Size(52, 17);
            this.ckCtrlStatus10.TabIndex = 12;
            this.ckCtrlStatus10.Text = "bit 10";
            this.ckCtrlStatus10.UseVisualStyleBackColor = true;
            this.ckCtrlStatus10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus9
            // 
            this.ckCtrlStatus9.AutoSize = true;
            this.ckCtrlStatus9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus9.Location = new System.Drawing.Point(169, 40);
            this.ckCtrlStatus9.Name = "ckCtrlStatus9";
            this.ckCtrlStatus9.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus9.TabIndex = 11;
            this.ckCtrlStatus9.Text = "bit 9";
            this.ckCtrlStatus9.UseVisualStyleBackColor = true;
            this.ckCtrlStatus9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus8
            // 
            this.ckCtrlStatus8.AutoSize = true;
            this.ckCtrlStatus8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus8.Location = new System.Drawing.Point(169, 18);
            this.ckCtrlStatus8.Name = "ckCtrlStatus8";
            this.ckCtrlStatus8.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus8.TabIndex = 10;
            this.ckCtrlStatus8.Text = "bit 8";
            this.ckCtrlStatus8.UseVisualStyleBackColor = true;
            this.ckCtrlStatus8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus7
            // 
            this.ckCtrlStatus7.AutoSize = true;
            this.ckCtrlStatus7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus7.Location = new System.Drawing.Point(9, 177);
            this.ckCtrlStatus7.Name = "ckCtrlStatus7";
            this.ckCtrlStatus7.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus7.TabIndex = 9;
            this.ckCtrlStatus7.Text = "bit 7";
            this.ckCtrlStatus7.UseVisualStyleBackColor = true;
            this.ckCtrlStatus7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus6
            // 
            this.ckCtrlStatus6.AutoSize = true;
            this.ckCtrlStatus6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus6.Location = new System.Drawing.Point(9, 154);
            this.ckCtrlStatus6.Name = "ckCtrlStatus6";
            this.ckCtrlStatus6.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus6.TabIndex = 8;
            this.ckCtrlStatus6.Text = "bit 6";
            this.ckCtrlStatus6.UseVisualStyleBackColor = true;
            this.ckCtrlStatus6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus5
            // 
            this.ckCtrlStatus5.AutoSize = true;
            this.ckCtrlStatus5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus5.Location = new System.Drawing.Point(9, 132);
            this.ckCtrlStatus5.Name = "ckCtrlStatus5";
            this.ckCtrlStatus5.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus5.TabIndex = 4;
            this.ckCtrlStatus5.Text = "bit 5";
            this.ckCtrlStatus5.UseVisualStyleBackColor = true;
            this.ckCtrlStatus5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus4
            // 
            this.ckCtrlStatus4.AutoSize = true;
            this.ckCtrlStatus4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus4.Location = new System.Drawing.Point(9, 109);
            this.ckCtrlStatus4.Name = "ckCtrlStatus4";
            this.ckCtrlStatus4.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus4.TabIndex = 3;
            this.ckCtrlStatus4.Text = "bit 4";
            this.ckCtrlStatus4.UseVisualStyleBackColor = true;
            this.ckCtrlStatus4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus3
            // 
            this.ckCtrlStatus3.AutoSize = true;
            this.ckCtrlStatus3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus3.Location = new System.Drawing.Point(9, 88);
            this.ckCtrlStatus3.Name = "ckCtrlStatus3";
            this.ckCtrlStatus3.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus3.TabIndex = 2;
            this.ckCtrlStatus3.Text = "bit 3";
            this.ckCtrlStatus3.UseVisualStyleBackColor = true;
            this.ckCtrlStatus3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus2
            // 
            this.ckCtrlStatus2.AutoSize = true;
            this.ckCtrlStatus2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus2.Location = new System.Drawing.Point(9, 65);
            this.ckCtrlStatus2.Name = "ckCtrlStatus2";
            this.ckCtrlStatus2.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus2.TabIndex = 1;
            this.ckCtrlStatus2.Text = "bit 2";
            this.ckCtrlStatus2.UseVisualStyleBackColor = true;
            this.ckCtrlStatus2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // ckCtrlStatus1
            // 
            this.ckCtrlStatus1.AutoSize = true;
            this.ckCtrlStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckCtrlStatus1.Location = new System.Drawing.Point(9, 42);
            this.ckCtrlStatus1.Name = "ckCtrlStatus1";
            this.ckCtrlStatus1.Size = new System.Drawing.Size(46, 17);
            this.ckCtrlStatus1.TabIndex = 0;
            this.ckCtrlStatus1.Text = "bit 1";
            this.ckCtrlStatus1.UseVisualStyleBackColor = true;
            this.ckCtrlStatus1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckCtrlStatus_MouseUp);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.ckVen0);
            this.groupBox8.Controls.Add(this.ckVen15);
            this.groupBox8.Controls.Add(this.ckVen14);
            this.groupBox8.Controls.Add(this.ckVen13);
            this.groupBox8.Controls.Add(this.ckVen12);
            this.groupBox8.Controls.Add(this.ckVen11);
            this.groupBox8.Controls.Add(this.ckVen10);
            this.groupBox8.Controls.Add(this.ckVen9);
            this.groupBox8.Controls.Add(this.ckVen8);
            this.groupBox8.Controls.Add(this.ckVen7);
            this.groupBox8.Controls.Add(this.ckVen6);
            this.groupBox8.Controls.Add(this.ckVen5);
            this.groupBox8.Controls.Add(this.ckVen4);
            this.groupBox8.Controls.Add(this.ckVen3);
            this.groupBox8.Controls.Add(this.ckVen2);
            this.groupBox8.Controls.Add(this.ckVen1);
            this.groupBox8.Location = new System.Drawing.Point(10, 30);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(356, 201);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Vendor Specific Status";
            // 
            // ckVen0
            // 
            this.ckVen0.AutoSize = true;
            this.ckVen0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen0.Location = new System.Drawing.Point(6, 18);
            this.ckVen0.Name = "ckVen0";
            this.ckVen0.Size = new System.Drawing.Size(46, 17);
            this.ckVen0.TabIndex = 18;
            this.ckVen0.Tag = "0";
            this.ckVen0.Text = "bit 0";
            this.ckVen0.UseVisualStyleBackColor = true;
            this.ckVen0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen15
            // 
            this.ckVen15.AutoSize = true;
            this.ckVen15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen15.Location = new System.Drawing.Point(182, 177);
            this.ckVen15.Name = "ckVen15";
            this.ckVen15.Size = new System.Drawing.Size(52, 17);
            this.ckVen15.TabIndex = 17;
            this.ckVen15.Text = "bit 15";
            this.ckVen15.UseVisualStyleBackColor = true;
            this.ckVen15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen14
            // 
            this.ckVen14.AutoSize = true;
            this.ckVen14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen14.Location = new System.Drawing.Point(182, 154);
            this.ckVen14.Name = "ckVen14";
            this.ckVen14.Size = new System.Drawing.Size(52, 17);
            this.ckVen14.TabIndex = 16;
            this.ckVen14.Text = "bit 14";
            this.ckVen14.UseVisualStyleBackColor = true;
            this.ckVen14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen13
            // 
            this.ckVen13.AutoSize = true;
            this.ckVen13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen13.Location = new System.Drawing.Point(182, 132);
            this.ckVen13.Name = "ckVen13";
            this.ckVen13.Size = new System.Drawing.Size(52, 17);
            this.ckVen13.TabIndex = 15;
            this.ckVen13.Text = "bit 13";
            this.ckVen13.UseVisualStyleBackColor = true;
            this.ckVen13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen12
            // 
            this.ckVen12.AutoSize = true;
            this.ckVen12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen12.Location = new System.Drawing.Point(182, 109);
            this.ckVen12.Name = "ckVen12";
            this.ckVen12.Size = new System.Drawing.Size(52, 17);
            this.ckVen12.TabIndex = 14;
            this.ckVen12.Text = "bit 12";
            this.ckVen12.UseVisualStyleBackColor = true;
            this.ckVen12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen11
            // 
            this.ckVen11.AutoSize = true;
            this.ckVen11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen11.Location = new System.Drawing.Point(182, 87);
            this.ckVen11.Name = "ckVen11";
            this.ckVen11.Size = new System.Drawing.Size(52, 17);
            this.ckVen11.TabIndex = 13;
            this.ckVen11.Text = "bit 11";
            this.ckVen11.UseVisualStyleBackColor = true;
            this.ckVen11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen10
            // 
            this.ckVen10.AutoSize = true;
            this.ckVen10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen10.Location = new System.Drawing.Point(182, 65);
            this.ckVen10.Name = "ckVen10";
            this.ckVen10.Size = new System.Drawing.Size(52, 17);
            this.ckVen10.TabIndex = 12;
            this.ckVen10.Text = "bit 10";
            this.ckVen10.UseVisualStyleBackColor = true;
            this.ckVen10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen9
            // 
            this.ckVen9.AutoSize = true;
            this.ckVen9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen9.Location = new System.Drawing.Point(182, 41);
            this.ckVen9.Name = "ckVen9";
            this.ckVen9.Size = new System.Drawing.Size(46, 17);
            this.ckVen9.TabIndex = 11;
            this.ckVen9.Text = "bit 9";
            this.ckVen9.UseVisualStyleBackColor = true;
            this.ckVen9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen8
            // 
            this.ckVen8.AutoSize = true;
            this.ckVen8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen8.Location = new System.Drawing.Point(182, 19);
            this.ckVen8.Name = "ckVen8";
            this.ckVen8.Size = new System.Drawing.Size(46, 17);
            this.ckVen8.TabIndex = 10;
            this.ckVen8.Text = "bit 8";
            this.ckVen8.UseVisualStyleBackColor = true;
            this.ckVen8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen7
            // 
            this.ckVen7.AutoSize = true;
            this.ckVen7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen7.Location = new System.Drawing.Point(6, 177);
            this.ckVen7.Name = "ckVen7";
            this.ckVen7.Size = new System.Drawing.Size(46, 17);
            this.ckVen7.TabIndex = 9;
            this.ckVen7.Text = "bit 7";
            this.ckVen7.UseVisualStyleBackColor = true;
            this.ckVen7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen6
            // 
            this.ckVen6.AutoSize = true;
            this.ckVen6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen6.Location = new System.Drawing.Point(6, 154);
            this.ckVen6.Name = "ckVen6";
            this.ckVen6.Size = new System.Drawing.Size(46, 17);
            this.ckVen6.TabIndex = 8;
            this.ckVen6.Text = "bit 6";
            this.ckVen6.UseVisualStyleBackColor = true;
            this.ckVen6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen5
            // 
            this.ckVen5.AutoSize = true;
            this.ckVen5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen5.Location = new System.Drawing.Point(6, 132);
            this.ckVen5.Name = "ckVen5";
            this.ckVen5.Size = new System.Drawing.Size(46, 17);
            this.ckVen5.TabIndex = 4;
            this.ckVen5.Text = "bit 5";
            this.ckVen5.UseVisualStyleBackColor = true;
            this.ckVen5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen4
            // 
            this.ckVen4.AutoSize = true;
            this.ckVen4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen4.Location = new System.Drawing.Point(6, 109);
            this.ckVen4.Name = "ckVen4";
            this.ckVen4.Size = new System.Drawing.Size(46, 17);
            this.ckVen4.TabIndex = 3;
            this.ckVen4.Text = "bit 4";
            this.ckVen4.UseVisualStyleBackColor = true;
            this.ckVen4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen3
            // 
            this.ckVen3.AutoSize = true;
            this.ckVen3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen3.Location = new System.Drawing.Point(6, 87);
            this.ckVen3.Name = "ckVen3";
            this.ckVen3.Size = new System.Drawing.Size(46, 17);
            this.ckVen3.TabIndex = 2;
            this.ckVen3.Text = "bit 3";
            this.ckVen3.UseVisualStyleBackColor = true;
            this.ckVen3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen2
            // 
            this.ckVen2.AutoSize = true;
            this.ckVen2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen2.Location = new System.Drawing.Point(6, 64);
            this.ckVen2.Name = "ckVen2";
            this.ckVen2.Size = new System.Drawing.Size(46, 17);
            this.ckVen2.TabIndex = 1;
            this.ckVen2.Text = "bit 2";
            this.ckVen2.UseVisualStyleBackColor = true;
            this.ckVen2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // ckVen1
            // 
            this.ckVen1.AutoSize = true;
            this.ckVen1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckVen1.Location = new System.Drawing.Point(6, 41);
            this.ckVen1.Name = "ckVen1";
            this.ckVen1.Size = new System.Drawing.Size(46, 17);
            this.ckVen1.TabIndex = 0;
            this.ckVen1.Tag = "1";
            this.ckVen1.Text = "bit 1";
            this.ckVen1.UseVisualStyleBackColor = true;
            this.ckVen1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckVen_MouseUp);
            // 
            // btnReadTcmzMonitors
            // 
            this.btnReadTcmzMonitors.Location = new System.Drawing.Point(648, 399);
            this.btnReadTcmzMonitors.Name = "btnReadTcmzMonitors";
            this.btnReadTcmzMonitors.Size = new System.Drawing.Size(61, 30);
            this.btnReadTcmzMonitors.TabIndex = 6;
            this.btnReadTcmzMonitors.Text = "Read";
            this.btnReadTcmzMonitors.UseVisualStyleBackColor = true;
            this.btnReadTcmzMonitors.Click += new System.EventHandler(this.btnReadTcmzMonitors_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lblLaserTemp);
            this.groupBox7.Controls.Add(this.txtLaserTemp);
            this.groupBox7.Controls.Add(this.lblMzTemp);
            this.groupBox7.Controls.Add(this.lblPcbTemp);
            this.groupBox7.Controls.Add(this.txtMzTemp);
            this.groupBox7.Controls.Add(this.txtPcbTempSense);
            this.groupBox7.Controls.Add(this.lbl5v2Sense);
            this.groupBox7.Controls.Add(this.txt5v2Sense);
            this.groupBox7.Location = new System.Drawing.Point(372, 237);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(336, 117);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "PCB Monitors";
            // 
            // lblLaserTemp
            // 
            this.lblLaserTemp.AutoSize = true;
            this.lblLaserTemp.Location = new System.Drawing.Point(6, 69);
            this.lblLaserTemp.Name = "lblLaserTemp";
            this.lblLaserTemp.Size = new System.Drawing.Size(63, 13);
            this.lblLaserTemp.TabIndex = 20;
            this.lblLaserTemp.Text = "Laser Temp";
            this.lblLaserTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLaserTemp
            // 
            this.txtLaserTemp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLaserTemp.Location = new System.Drawing.Point(87, 69);
            this.txtLaserTemp.Name = "txtLaserTemp";
            this.txtLaserTemp.ReadOnly = true;
            this.txtLaserTemp.Size = new System.Drawing.Size(43, 20);
            this.txtLaserTemp.TabIndex = 19;
            // 
            // lblMzTemp
            // 
            this.lblMzTemp.AutoSize = true;
            this.lblMzTemp.Location = new System.Drawing.Point(157, 69);
            this.lblMzTemp.Name = "lblMzTemp";
            this.lblMzTemp.Size = new System.Drawing.Size(51, 13);
            this.lblMzTemp.TabIndex = 12;
            this.lblMzTemp.Text = "Mz Temp";
            this.lblMzTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPcbTemp
            // 
            this.lblPcbTemp.AutoSize = true;
            this.lblPcbTemp.Location = new System.Drawing.Point(157, 40);
            this.lblPcbTemp.Name = "lblPcbTemp";
            this.lblPcbTemp.Size = new System.Drawing.Size(58, 13);
            this.lblPcbTemp.TabIndex = 18;
            this.lblPcbTemp.Text = "PCB Temp";
            this.lblPcbTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMzTemp
            // 
            this.txtMzTemp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtMzTemp.Location = new System.Drawing.Point(223, 69);
            this.txtMzTemp.Name = "txtMzTemp";
            this.txtMzTemp.ReadOnly = true;
            this.txtMzTemp.Size = new System.Drawing.Size(41, 20);
            this.txtMzTemp.TabIndex = 9;
            // 
            // txtPcbTempSense
            // 
            this.txtPcbTempSense.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPcbTempSense.Location = new System.Drawing.Point(223, 37);
            this.txtPcbTempSense.Name = "txtPcbTempSense";
            this.txtPcbTempSense.ReadOnly = true;
            this.txtPcbTempSense.Size = new System.Drawing.Size(41, 20);
            this.txtPcbTempSense.TabIndex = 17;
            // 
            // lbl5v2Sense
            // 
            this.lbl5v2Sense.AutoSize = true;
            this.lbl5v2Sense.Location = new System.Drawing.Point(6, 40);
            this.lbl5v2Sense.Name = "lbl5v2Sense";
            this.lbl5v2Sense.Size = new System.Drawing.Size(56, 13);
            this.lbl5v2Sense.TabIndex = 16;
            this.lbl5v2Sense.Text = "5v2 sense";
            this.lbl5v2Sense.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt5v2Sense
            // 
            this.txt5v2Sense.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txt5v2Sense.Location = new System.Drawing.Point(87, 37);
            this.txt5v2Sense.Name = "txt5v2Sense";
            this.txt5v2Sense.ReadOnly = true;
            this.txt5v2Sense.Size = new System.Drawing.Size(43, 20);
            this.txt5v2Sense.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(276, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 24);
            this.label5.TabIndex = 3;
            this.label5.Text = "Monitors";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.RxTimerEnabled);
            this.tabPage7.Controls.Add(this.groupBox19);
            this.tabPage7.Controls.Add(this.label31);
            this.tabPage7.Controls.Add(this.ReadRx);
            this.tabPage7.Controls.Add(this.groupBox16);
            this.tabPage7.Controls.Add(this.groupBox18);
            this.tabPage7.Controls.Add(this.groupBox15);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(736, 468);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Eval Board";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // RxTimerEnabled
            // 
            this.RxTimerEnabled.AutoSize = true;
            this.RxTimerEnabled.Location = new System.Drawing.Point(541, 419);
            this.RxTimerEnabled.Name = "RxTimerEnabled";
            this.RxTimerEnabled.Size = new System.Drawing.Size(86, 17);
            this.RxTimerEnabled.TabIndex = 40;
            this.RxTimerEnabled.Text = "Auto Update";
            this.RxTimerEnabled.UseVisualStyleBackColor = true;
            this.RxTimerEnabled.CheckedChanged += new System.EventHandler(this.RxTimerEnabled_CheckedChanged);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.edfaPowerControl);
            this.groupBox19.Controls.Add(this.groupBox20);
            this.groupBox19.Controls.Add(this.label37);
            this.groupBox19.Controls.Add(this.evalPcbVref);
            this.groupBox19.Controls.Add(this.label36);
            this.groupBox19.Controls.Add(this.evalPcbStm32Temp);
            this.groupBox19.Controls.Add(this.label35);
            this.groupBox19.Controls.Add(this.evalEdfaPower);
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.evalPcbTemp);
            this.groupBox19.Location = new System.Drawing.Point(303, 252);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(324, 161);
            this.groupBox19.TabIndex = 8;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Evaluation Board";
            // 
            // edfaPowerControl
            // 
            this.edfaPowerControl.Location = new System.Drawing.Point(6, 123);
            this.edfaPowerControl.Name = "edfaPowerControl";
            this.edfaPowerControl.Size = new System.Drawing.Size(223, 28);
            this.edfaPowerControl.TabIndex = 19;
            this.edfaPowerControl.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.edfaPowerControl.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.edfaPowerControl_ValueChanged);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.evalDigiOut3);
            this.groupBox20.Controls.Add(this.evalDigiOut2);
            this.groupBox20.Controls.Add(this.evalDigiOut1);
            this.groupBox20.Controls.Add(this.evalDigiOut0);
            this.groupBox20.Location = new System.Drawing.Point(189, 18);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(123, 97);
            this.groupBox20.TabIndex = 18;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Digital Outputs";
            // 
            // evalDigiOut3
            // 
            this.evalDigiOut3.AutoSize = true;
            this.evalDigiOut3.Location = new System.Drawing.Point(6, 74);
            this.evalDigiOut3.Name = "evalDigiOut3";
            this.evalDigiOut3.Size = new System.Drawing.Size(54, 17);
            this.evalDigiOut3.TabIndex = 21;
            this.evalDigiOut3.Text = "M3V3";
            this.evalDigiOut3.UseVisualStyleBackColor = true;
            this.evalDigiOut3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.evalDigiOut_MouseUp);
            // 
            // evalDigiOut2
            // 
            this.evalDigiOut2.AutoSize = true;
            this.evalDigiOut2.Location = new System.Drawing.Point(6, 55);
            this.evalDigiOut2.Name = "evalDigiOut2";
            this.evalDigiOut2.Size = new System.Drawing.Size(45, 17);
            this.evalDigiOut2.TabIndex = 20;
            this.evalDigiOut2.Text = "15V";
            this.evalDigiOut2.UseVisualStyleBackColor = true;
            this.evalDigiOut2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.evalDigiOut_MouseUp);
            // 
            // evalDigiOut1
            // 
            this.evalDigiOut1.AutoSize = true;
            this.evalDigiOut1.Location = new System.Drawing.Point(6, 37);
            this.evalDigiOut1.Name = "evalDigiOut1";
            this.evalDigiOut1.Size = new System.Drawing.Size(54, 17);
            this.evalDigiOut1.TabIndex = 19;
            this.evalDigiOut1.Text = "EDFA";
            this.evalDigiOut1.UseVisualStyleBackColor = true;
            this.evalDigiOut1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.evalDigiOut_MouseUp);
            // 
            // evalDigiOut0
            // 
            this.evalDigiOut0.AutoSize = true;
            this.evalDigiOut0.Location = new System.Drawing.Point(6, 19);
            this.evalDigiOut0.Name = "evalDigiOut0";
            this.evalDigiOut0.Size = new System.Drawing.Size(72, 17);
            this.evalDigiOut0.TabIndex = 18;
            this.evalDigiOut0.Text = "DEC TEC";
            this.evalDigiOut0.UseVisualStyleBackColor = true;
            this.evalDigiOut0.CheckedChanged += new System.EventHandler(this.evalDigiOut0_CheckedChanged);
            this.evalDigiOut0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.evalDigiOut_MouseUp);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(4, 101);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(65, 12);
            this.label37.TabIndex = 17;
            this.label37.Text = "Eval PCB Vref";
            // 
            // evalPcbVref
            // 
            this.evalPcbVref.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evalPcbVref.Location = new System.Drawing.Point(110, 97);
            this.evalPcbVref.Name = "evalPcbVref";
            this.evalPcbVref.Size = new System.Drawing.Size(66, 20);
            this.evalPcbVref.TabIndex = 16;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(4, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 12);
            this.label36.TabIndex = 15;
            this.label36.Text = "STM32 Temperature";
            // 
            // evalPcbStm32Temp
            // 
            this.evalPcbStm32Temp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evalPcbStm32Temp.Location = new System.Drawing.Point(110, 71);
            this.evalPcbStm32Temp.Name = "evalPcbStm32Temp";
            this.evalPcbStm32Temp.Size = new System.Drawing.Size(66, 20);
            this.evalPcbStm32Temp.TabIndex = 14;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(5, 49);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 12);
            this.label35.TabIndex = 13;
            this.label35.Text = "EDFA Power";
            // 
            // evalEdfaPower
            // 
            this.evalEdfaPower.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evalEdfaPower.Location = new System.Drawing.Point(111, 45);
            this.evalEdfaPower.Name = "evalEdfaPower";
            this.evalEdfaPower.Size = new System.Drawing.Size(66, 20);
            this.evalEdfaPower.TabIndex = 12;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(5, 23);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(78, 12);
            this.label34.TabIndex = 11;
            this.label34.Text = "PCB Temperature";
            // 
            // evalPcbTemp
            // 
            this.evalPcbTemp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.evalPcbTemp.Location = new System.Drawing.Point(111, 19);
            this.evalPcbTemp.Name = "evalPcbTemp";
            this.evalPcbTemp.Size = new System.Drawing.Size(66, 20);
            this.evalPcbTemp.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(202, 5);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(241, 24);
            this.label31.TabIndex = 7;
            this.label31.Text = "Evaluation Board Control";
            // 
            // ReadRx
            // 
            this.ReadRx.Location = new System.Drawing.Point(470, 419);
            this.ReadRx.Name = "ReadRx";
            this.ReadRx.Size = new System.Drawing.Size(62, 31);
            this.ReadRx.TabIndex = 6;
            this.ReadRx.Text = "Read";
            this.ReadRx.UseVisualStyleBackColor = true;
            this.ReadRx.Click += new System.EventHandler(this.ReadRx_Click);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label25);
            this.groupBox16.Controls.Add(this.Rx2NegVout);
            this.groupBox16.Controls.Add(this.label26);
            this.groupBox16.Controls.Add(this.Rx2PosVout);
            this.groupBox16.Controls.Add(this.label27);
            this.groupBox16.Controls.Add(this.Rx2NegMon_I);
            this.groupBox16.Controls.Add(this.label28);
            this.groupBox16.Controls.Add(this.Rx2PosMon_I);
            this.groupBox16.Controls.Add(this.rx2PeakCtrlV);
            this.groupBox16.Controls.Add(this.rx2NegDcOffsetV);
            this.groupBox16.Controls.Add(this.rx2PosDcOffsetV);
            this.groupBox16.Location = new System.Drawing.Point(303, 32);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(245, 214);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Receiver 2";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(7, 187);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 12);
            this.label25.TabIndex = 17;
            this.label25.Text = "Neg DC o/p voltage";
            // 
            // Rx2NegVout
            // 
            this.Rx2NegVout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx2NegVout.Location = new System.Drawing.Point(110, 183);
            this.Rx2NegVout.Name = "Rx2NegVout";
            this.Rx2NegVout.Size = new System.Drawing.Size(66, 20);
            this.Rx2NegVout.TabIndex = 16;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(7, 161);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 12);
            this.label26.TabIndex = 15;
            this.label26.Text = "Pos DC o/p Voltage";
            // 
            // Rx2PosVout
            // 
            this.Rx2PosVout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx2PosVout.Location = new System.Drawing.Point(110, 157);
            this.Rx2PosVout.Name = "Rx2PosVout";
            this.Rx2PosVout.Size = new System.Drawing.Size(66, 20);
            this.Rx2PosVout.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(7, 135);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 12);
            this.label27.TabIndex = 13;
            this.label27.Text = "Neg Photodiode Current";
            // 
            // Rx2NegMon_I
            // 
            this.Rx2NegMon_I.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx2NegMon_I.Location = new System.Drawing.Point(110, 131);
            this.Rx2NegMon_I.Name = "Rx2NegMon_I";
            this.Rx2NegMon_I.Size = new System.Drawing.Size(66, 20);
            this.Rx2NegMon_I.TabIndex = 12;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(7, 109);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 12);
            this.label28.TabIndex = 11;
            this.label28.Text = "Pos Photodiode Current";
            // 
            // Rx2PosMon_I
            // 
            this.Rx2PosMon_I.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx2PosMon_I.Location = new System.Drawing.Point(110, 105);
            this.Rx2PosMon_I.Name = "Rx2PosMon_I";
            this.Rx2PosMon_I.Size = new System.Drawing.Size(66, 20);
            this.Rx2PosMon_I.TabIndex = 10;
            // 
            // rx2PeakCtrlV
            // 
            this.rx2PeakCtrlV.Location = new System.Drawing.Point(9, 71);
            this.rx2PeakCtrlV.Name = "rx2PeakCtrlV";
            this.rx2PeakCtrlV.Size = new System.Drawing.Size(223, 28);
            this.rx2PeakCtrlV.TabIndex = 9;
            this.rx2PeakCtrlV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx2PeakCtrlV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx2PeakCtrlV);
            // 
            // rx2NegDcOffsetV
            // 
            this.rx2NegDcOffsetV.Location = new System.Drawing.Point(9, 45);
            this.rx2NegDcOffsetV.Name = "rx2NegDcOffsetV";
            this.rx2NegDcOffsetV.Size = new System.Drawing.Size(223, 28);
            this.rx2NegDcOffsetV.TabIndex = 8;
            this.rx2NegDcOffsetV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx2NegDcOffsetV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx2NegDcOffsetV);
            // 
            // rx2PosDcOffsetV
            // 
            this.rx2PosDcOffsetV.Location = new System.Drawing.Point(9, 19);
            this.rx2PosDcOffsetV.Name = "rx2PosDcOffsetV";
            this.rx2PosDcOffsetV.Size = new System.Drawing.Size(223, 28);
            this.rx2PosDcOffsetV.TabIndex = 7;
            this.rx2PosDcOffsetV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx2PosDcOffsetV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx2PosDcOffsetV);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label33);
            this.groupBox18.Controls.Add(this.demodulatorThermistor);
            this.groupBox18.Controls.Add(this.tecCtrl);
            this.groupBox18.Controls.Add(this.arm2HeaterCtrl);
            this.groupBox18.Controls.Add(this.arm1HeaterCtrl);
            this.groupBox18.Controls.Add(this.mainHeaterCtrl);
            this.groupBox18.Location = new System.Drawing.Point(22, 252);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(245, 161);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Demodulator";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 134);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 12);
            this.label33.TabIndex = 9;
            this.label33.Text = "Thermistor";
            // 
            // demodulatorThermistor
            // 
            this.demodulatorThermistor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.demodulatorThermistor.Location = new System.Drawing.Point(110, 130);
            this.demodulatorThermistor.Name = "demodulatorThermistor";
            this.demodulatorThermistor.Size = new System.Drawing.Size(66, 20);
            this.demodulatorThermistor.TabIndex = 8;
            // 
            // tecCtrl
            // 
            this.tecCtrl.Location = new System.Drawing.Point(6, 96);
            this.tecCtrl.Name = "tecCtrl";
            this.tecCtrl.Size = new System.Drawing.Size(223, 28);
            this.tecCtrl.TabIndex = 7;
            this.tecCtrl.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.tecCtrl.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setTecCtrl);
            // 
            // arm2HeaterCtrl
            // 
            this.arm2HeaterCtrl.Location = new System.Drawing.Point(6, 70);
            this.arm2HeaterCtrl.Name = "arm2HeaterCtrl";
            this.arm2HeaterCtrl.Size = new System.Drawing.Size(223, 28);
            this.arm2HeaterCtrl.TabIndex = 6;
            this.arm2HeaterCtrl.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.arm2HeaterCtrl.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setArm2HeaterCtrl);
            // 
            // arm1HeaterCtrl
            // 
            this.arm1HeaterCtrl.Location = new System.Drawing.Point(6, 44);
            this.arm1HeaterCtrl.Name = "arm1HeaterCtrl";
            this.arm1HeaterCtrl.Size = new System.Drawing.Size(223, 28);
            this.arm1HeaterCtrl.TabIndex = 5;
            this.arm1HeaterCtrl.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.arm1HeaterCtrl.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setArm1HeaterCtrl);
            // 
            // mainHeaterCtrl
            // 
            this.mainHeaterCtrl.Location = new System.Drawing.Point(6, 18);
            this.mainHeaterCtrl.Name = "mainHeaterCtrl";
            this.mainHeaterCtrl.Size = new System.Drawing.Size(223, 28);
            this.mainHeaterCtrl.TabIndex = 4;
            this.mainHeaterCtrl.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.mainHeaterCtrl.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setMainHeaterCtrl);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this.Rx1NegVout);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this.Rx1PosVout);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this.Rx1NegMon_I);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Controls.Add(this.Rx1PosMon_I);
            this.groupBox15.Controls.Add(this.rx1PeakCtrlV);
            this.groupBox15.Controls.Add(this.rx1NegDcOffsetV);
            this.groupBox15.Controls.Add(this.rx1PosDcOffsetV);
            this.groupBox15.Location = new System.Drawing.Point(22, 32);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(245, 214);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Receiver 1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(7, 187);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 12);
            this.label24.TabIndex = 17;
            this.label24.Text = "Neg DC o/p Voltage";
            // 
            // Rx1NegVout
            // 
            this.Rx1NegVout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx1NegVout.Location = new System.Drawing.Point(110, 183);
            this.Rx1NegVout.Name = "Rx1NegVout";
            this.Rx1NegVout.Size = new System.Drawing.Size(66, 20);
            this.Rx1NegVout.TabIndex = 16;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(7, 161);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(86, 12);
            this.label23.TabIndex = 15;
            this.label23.Text = "Pos DC o/p Voltage";
            // 
            // Rx1PosVout
            // 
            this.Rx1PosVout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx1PosVout.Location = new System.Drawing.Point(110, 157);
            this.Rx1PosVout.Name = "Rx1PosVout";
            this.Rx1PosVout.Size = new System.Drawing.Size(66, 20);
            this.Rx1PosVout.TabIndex = 14;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(7, 135);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 12);
            this.label22.TabIndex = 13;
            this.label22.Text = "Neg Photodiode Current";
            // 
            // Rx1NegMon_I
            // 
            this.Rx1NegMon_I.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx1NegMon_I.Location = new System.Drawing.Point(110, 131);
            this.Rx1NegMon_I.Name = "Rx1NegMon_I";
            this.Rx1NegMon_I.Size = new System.Drawing.Size(66, 20);
            this.Rx1NegMon_I.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 109);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 12);
            this.label21.TabIndex = 11;
            this.label21.Text = "Pos Photodiode Current";
            // 
            // Rx1PosMon_I
            // 
            this.Rx1PosMon_I.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Rx1PosMon_I.Location = new System.Drawing.Point(110, 105);
            this.Rx1PosMon_I.Name = "Rx1PosMon_I";
            this.Rx1PosMon_I.Size = new System.Drawing.Size(66, 20);
            this.Rx1PosMon_I.TabIndex = 10;
            // 
            // rx1PeakCtrlV
            // 
            this.rx1PeakCtrlV.Location = new System.Drawing.Point(9, 71);
            this.rx1PeakCtrlV.Name = "rx1PeakCtrlV";
            this.rx1PeakCtrlV.Size = new System.Drawing.Size(223, 28);
            this.rx1PeakCtrlV.TabIndex = 9;
            this.rx1PeakCtrlV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx1PeakCtrlV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx1PeakCtrlV);
            // 
            // rx1NegDcOffsetV
            // 
            this.rx1NegDcOffsetV.Location = new System.Drawing.Point(9, 45);
            this.rx1NegDcOffsetV.Name = "rx1NegDcOffsetV";
            this.rx1NegDcOffsetV.Size = new System.Drawing.Size(223, 28);
            this.rx1NegDcOffsetV.TabIndex = 8;
            this.rx1NegDcOffsetV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx1NegDcOffsetV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx1NegDcOffsetV);
            // 
            // rx1PosDcOffsetV
            // 
            this.rx1PosDcOffsetV.Location = new System.Drawing.Point(9, 19);
            this.rx1PosDcOffsetV.Name = "rx1PosDcOffsetV";
            this.rx1PosDcOffsetV.Size = new System.Drawing.Size(223, 28);
            this.rx1PosDcOffsetV.TabIndex = 7;
            this.rx1PosDcOffsetV.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rx1PosDcOffsetV.ValueChanged += new ModuleController.UserControls.scrollboxAndSlider.ValueChangedHandler(this.setRx1PosDcOffsetV);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox21);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(736, 468);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Debug";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox21.Controls.Add(this.groupBox17);
            this.groupBox21.Controls.Add(this.radioButton2);
            this.groupBox21.Controls.Add(this.radioButton1);
            this.groupBox21.Controls.Add(this.groupBox4);
            this.groupBox21.Location = new System.Drawing.Point(6, 38);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(724, 342);
            this.groupBox21.TabIndex = 6;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Instrument";
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this.messageLog);
            this.groupBox17.Controls.Add(this.ckLoggingEnabled);
            this.groupBox17.Controls.Add(this.btnClearLog);
            this.groupBox17.Location = new System.Drawing.Point(259, 19);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(459, 317);
            this.groupBox17.TabIndex = 5;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Communications Log";
            // 
            // messageLog
            // 
            this.messageLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.messageLog.Location = new System.Drawing.Point(6, 19);
            this.messageLog.Multiline = true;
            this.messageLog.Name = "messageLog";
            this.messageLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messageLog.Size = new System.Drawing.Size(447, 246);
            this.messageLog.TabIndex = 5;
            this.messageLog.TextChanged += new System.EventHandler(this.messageLog_TextChanged);
            // 
            // ckLoggingEnabled
            // 
            this.ckLoggingEnabled.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ckLoggingEnabled.AutoSize = true;
            this.ckLoggingEnabled.Location = new System.Drawing.Point(6, 283);
            this.ckLoggingEnabled.Name = "ckLoggingEnabled";
            this.ckLoggingEnabled.Size = new System.Drawing.Size(105, 17);
            this.ckLoggingEnabled.TabIndex = 1;
            this.ckLoggingEnabled.Text = "Logging enabled";
            this.ckLoggingEnabled.UseVisualStyleBackColor = true;
            this.ckLoggingEnabled.CheckedChanged += new System.EventHandler(this.ckLoggingEnabled_CheckedChanged);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearLog.Location = new System.Drawing.Point(390, 283);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(63, 26);
            this.btnClearLog.TabIndex = 0;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 38);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(77, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Eval Board";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(60, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Module";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.setupDebug);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtReadStr);
            this.groupBox4.Controls.Add(this.txtWriteStr);
            this.groupBox4.Controls.Add(this.btnReadStr);
            this.groupBox4.Controls.Add(this.btnWriteStr);
            this.groupBox4.Controls.Add(this.btnReadVal);
            this.groupBox4.Controls.Add(this.btnWriteByte);
            this.groupBox4.Controls.Add(this.txtReadByte);
            this.groupBox4.Controls.Add(this.txtByteVal);
            this.groupBox4.Controls.Add(this.lblRegisterNo);
            this.groupBox4.Controls.Add(this.txtRegisterNumber);
            this.groupBox4.Location = new System.Drawing.Point(5, 75);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(248, 172);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register Access";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "0x";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "0x";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "0x";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtReadStr
            // 
            this.txtReadStr.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtReadStr.Location = new System.Drawing.Point(98, 128);
            this.txtReadStr.Name = "txtReadStr";
            this.txtReadStr.ReadOnly = true;
            this.txtReadStr.Size = new System.Drawing.Size(130, 20);
            this.txtReadStr.TabIndex = 10;
            // 
            // txtWriteStr
            // 
            this.txtWriteStr.Location = new System.Drawing.Point(98, 100);
            this.txtWriteStr.Name = "txtWriteStr";
            this.txtWriteStr.Size = new System.Drawing.Size(130, 20);
            this.txtWriteStr.TabIndex = 9;
            // 
            // btnReadStr
            // 
            this.btnReadStr.Location = new System.Drawing.Point(9, 126);
            this.btnReadStr.Name = "btnReadStr";
            this.btnReadStr.Size = new System.Drawing.Size(79, 22);
            this.btnReadStr.TabIndex = 8;
            this.btnReadStr.Text = "Read String";
            this.btnReadStr.UseVisualStyleBackColor = true;
            this.btnReadStr.Click += new System.EventHandler(this.btnReadStr_Click);
            // 
            // btnWriteStr
            // 
            this.btnWriteStr.Location = new System.Drawing.Point(9, 98);
            this.btnWriteStr.Name = "btnWriteStr";
            this.btnWriteStr.Size = new System.Drawing.Size(79, 22);
            this.btnWriteStr.TabIndex = 7;
            this.btnWriteStr.Text = "Write String";
            this.btnWriteStr.UseVisualStyleBackColor = true;
            this.btnWriteStr.Click += new System.EventHandler(this.btnWriteStr_Click);
            // 
            // btnReadVal
            // 
            this.btnReadVal.Location = new System.Drawing.Point(11, 71);
            this.btnReadVal.Name = "btnReadVal";
            this.btnReadVal.Size = new System.Drawing.Size(77, 22);
            this.btnReadVal.TabIndex = 6;
            this.btnReadVal.Text = "Read Val";
            this.btnReadVal.UseVisualStyleBackColor = true;
            this.btnReadVal.Click += new System.EventHandler(this.btnReadVal_Click);
            // 
            // btnWriteByte
            // 
            this.btnWriteByte.Location = new System.Drawing.Point(11, 43);
            this.btnWriteByte.Name = "btnWriteByte";
            this.btnWriteByte.Size = new System.Drawing.Size(77, 22);
            this.btnWriteByte.TabIndex = 5;
            this.btnWriteByte.Text = "Write Val";
            this.btnWriteByte.UseVisualStyleBackColor = true;
            this.btnWriteByte.Click += new System.EventHandler(this.btnWriteByte_Click);
            // 
            // txtReadByte
            // 
            this.txtReadByte.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtReadByte.Location = new System.Drawing.Point(116, 73);
            this.txtReadByte.Name = "txtReadByte";
            this.txtReadByte.ReadOnly = true;
            this.txtReadByte.Size = new System.Drawing.Size(33, 20);
            this.txtReadByte.TabIndex = 4;
            // 
            // txtByteVal
            // 
            this.txtByteVal.Location = new System.Drawing.Point(116, 45);
            this.txtByteVal.Name = "txtByteVal";
            this.txtByteVal.Size = new System.Drawing.Size(33, 20);
            this.txtByteVal.TabIndex = 3;
            // 
            // lblRegisterNo
            // 
            this.lblRegisterNo.AutoSize = true;
            this.lblRegisterNo.Location = new System.Drawing.Point(6, 22);
            this.lblRegisterNo.Name = "lblRegisterNo";
            this.lblRegisterNo.Size = new System.Drawing.Size(60, 13);
            this.lblRegisterNo.TabIndex = 1;
            this.lblRegisterNo.Text = "Register 0x";
            // 
            // txtRegisterNumber
            // 
            this.txtRegisterNumber.Location = new System.Drawing.Point(116, 19);
            this.txtRegisterNumber.Name = "txtRegisterNumber";
            this.txtRegisterNumber.Size = new System.Drawing.Size(33, 20);
            this.txtRegisterNumber.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(278, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 24);
            this.label6.TabIndex = 3;
            this.label6.Text = "Engineering Debug";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.txtUnlockStatus);
            this.groupBox3.Controls.Add(this.btnCheckLockStatus);
            this.groupBox3.Controls.Add(this.txtPassword);
            this.groupBox3.Controls.Add(this.btnSendPassword);
            this.groupBox3.Location = new System.Drawing.Point(12, 386);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 69);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "TTA Module Password";
            // 
            // txtUnlockStatus
            // 
            this.txtUnlockStatus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtUnlockStatus.Location = new System.Drawing.Point(112, 42);
            this.txtUnlockStatus.Name = "txtUnlockStatus";
            this.txtUnlockStatus.ReadOnly = true;
            this.txtUnlockStatus.Size = new System.Drawing.Size(37, 20);
            this.txtUnlockStatus.TabIndex = 3;
            // 
            // btnCheckLockStatus
            // 
            this.btnCheckLockStatus.Location = new System.Drawing.Point(155, 42);
            this.btnCheckLockStatus.Name = "btnCheckLockStatus";
            this.btnCheckLockStatus.Size = new System.Drawing.Size(65, 21);
            this.btnCheckLockStatus.TabIndex = 2;
            this.btnCheckLockStatus.Text = "Status";
            this.btnCheckLockStatus.UseVisualStyleBackColor = true;
            this.btnCheckLockStatus.Click += new System.EventHandler(this.btnCheckLockStatus_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(6, 19);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(143, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // btnSendPassword
            // 
            this.btnSendPassword.Location = new System.Drawing.Point(155, 19);
            this.btnSendPassword.Name = "btnSendPassword";
            this.btnSendPassword.Size = new System.Drawing.Size(66, 20);
            this.btnSendPassword.TabIndex = 0;
            this.btnSendPassword.Text = "Send";
            this.btnSendPassword.UseVisualStyleBackColor = true;
            this.btnSendPassword.Click += new System.EventHandler(this.btnSendPassword_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.calibrationTabs);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(736, 468);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Calibration";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // calibrationTabs
            // 
            this.calibrationTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.calibrationTabs.Controls.Add(this.calTabDAC);
            this.calibrationTabs.Controls.Add(this.calTabGoldBox);
            this.calibrationTabs.Controls.Add(this.calTabPlots);
            this.calibrationTabs.Location = new System.Drawing.Point(14, 14);
            this.calibrationTabs.Name = "calibrationTabs";
            this.calibrationTabs.SelectedIndex = 0;
            this.calibrationTabs.Size = new System.Drawing.Size(709, 441);
            this.calibrationTabs.TabIndex = 1;
            // 
            // calTabDAC
            // 
            this.calTabDAC.Controls.Add(this.dacCalStartCal);
            this.calTabDAC.Controls.Add(this.dacCalSaveFile);
            this.calTabDAC.Controls.Add(this.dacCalLoadFile);
            this.calTabDAC.Controls.Add(this.dacCalDataGrid);
            this.calTabDAC.Controls.Add(this.dacCalOverallProgressBar);
            this.calTabDAC.Controls.Add(this.dacCalChannelProgressBar);
            this.calTabDAC.Location = new System.Drawing.Point(4, 22);
            this.calTabDAC.Name = "calTabDAC";
            this.calTabDAC.Padding = new System.Windows.Forms.Padding(3);
            this.calTabDAC.Size = new System.Drawing.Size(701, 415);
            this.calTabDAC.TabIndex = 0;
            this.calTabDAC.Text = "DAC Cal";
            this.calTabDAC.UseVisualStyleBackColor = true;
            // 
            // dacCalStartCal
            // 
            this.dacCalStartCal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dacCalStartCal.Location = new System.Drawing.Point(608, 387);
            this.dacCalStartCal.Name = "dacCalStartCal";
            this.dacCalStartCal.Size = new System.Drawing.Size(84, 23);
            this.dacCalStartCal.TabIndex = 5;
            this.dacCalStartCal.Text = "Calibrate DAC";
            this.dacCalStartCal.UseVisualStyleBackColor = true;
            this.dacCalStartCal.Click += new System.EventHandler(this.dacCalStartCal_Click);
            // 
            // dacCalSaveFile
            // 
            this.dacCalSaveFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dacCalSaveFile.Location = new System.Drawing.Point(79, 387);
            this.dacCalSaveFile.Name = "dacCalSaveFile";
            this.dacCalSaveFile.Size = new System.Drawing.Size(61, 22);
            this.dacCalSaveFile.TabIndex = 4;
            this.dacCalSaveFile.Text = "Save File";
            this.dacCalSaveFile.UseVisualStyleBackColor = true;
            // 
            // dacCalLoadFile
            // 
            this.dacCalLoadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dacCalLoadFile.Location = new System.Drawing.Point(6, 387);
            this.dacCalLoadFile.Name = "dacCalLoadFile";
            this.dacCalLoadFile.Size = new System.Drawing.Size(67, 22);
            this.dacCalLoadFile.TabIndex = 3;
            this.dacCalLoadFile.Text = "Load File";
            this.dacCalLoadFile.UseVisualStyleBackColor = true;
            this.dacCalLoadFile.Click += new System.EventHandler(this.dacCalLoadFile_Click);
            // 
            // dacCalDataGrid
            // 
            this.dacCalDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dacCalDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dacCalDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dacCalDataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.dacCalDataGrid.Location = new System.Drawing.Point(0, 0);
            this.dacCalDataGrid.Name = "dacCalDataGrid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dacCalDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dacCalDataGrid.Size = new System.Drawing.Size(692, 352);
            this.dacCalDataGrid.TabIndex = 2;
            // 
            // dacCalOverallProgressBar
            // 
            this.dacCalOverallProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dacCalOverallProgressBar.Location = new System.Drawing.Point(3, 375);
            this.dacCalOverallProgressBar.Name = "dacCalOverallProgressBar";
            this.dacCalOverallProgressBar.Size = new System.Drawing.Size(691, 10);
            this.dacCalOverallProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.dacCalOverallProgressBar.TabIndex = 1;
            // 
            // dacCalChannelProgressBar
            // 
            this.dacCalChannelProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dacCalChannelProgressBar.Location = new System.Drawing.Point(2, 358);
            this.dacCalChannelProgressBar.Name = "dacCalChannelProgressBar";
            this.dacCalChannelProgressBar.Size = new System.Drawing.Size(692, 11);
            this.dacCalChannelProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.dacCalChannelProgressBar.TabIndex = 0;
            // 
            // calTabGoldBox
            // 
            this.calTabGoldBox.Controls.Add(this.loadGBData);
            this.calTabGoldBox.Controls.Add(this.dataGridView1);
            this.calTabGoldBox.Location = new System.Drawing.Point(4, 22);
            this.calTabGoldBox.Name = "calTabGoldBox";
            this.calTabGoldBox.Padding = new System.Windows.Forms.Padding(3);
            this.calTabGoldBox.Size = new System.Drawing.Size(701, 415);
            this.calTabGoldBox.TabIndex = 1;
            this.calTabGoldBox.Text = "Gold Box Data";
            this.calTabGoldBox.UseVisualStyleBackColor = true;
            // 
            // loadGBData
            // 
            this.loadGBData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.loadGBData.Location = new System.Drawing.Point(588, 387);
            this.loadGBData.Name = "loadGBData";
            this.loadGBData.Size = new System.Drawing.Size(97, 22);
            this.loadGBData.TabIndex = 1;
            this.loadGBData.Text = "Load File";
            this.loadGBData.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.Size = new System.Drawing.Size(673, 369);
            this.dataGridView1.TabIndex = 0;
            // 
            // calTabPlots
            // 
            this.calTabPlots.Controls.Add(this.btnNextPlot);
            this.calTabPlots.Controls.Add(this.btnPreviousPlot);
            this.calTabPlots.Controls.Add(this.plotSurface2D1);
            this.calTabPlots.Location = new System.Drawing.Point(4, 22);
            this.calTabPlots.Name = "calTabPlots";
            this.calTabPlots.Size = new System.Drawing.Size(701, 415);
            this.calTabPlots.TabIndex = 2;
            this.calTabPlots.Text = "DAC Plot Data";
            this.calTabPlots.UseVisualStyleBackColor = true;
            // 
            // btnNextPlot
            // 
            this.btnNextPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextPlot.Location = new System.Drawing.Point(621, 375);
            this.btnNextPlot.Name = "btnNextPlot";
            this.btnNextPlot.Size = new System.Drawing.Size(77, 32);
            this.btnNextPlot.TabIndex = 2;
            this.btnNextPlot.Text = "Next";
            this.btnNextPlot.UseVisualStyleBackColor = true;
            this.btnNextPlot.Click += new System.EventHandler(this.btnNextPlot_Click);
            // 
            // btnPreviousPlot
            // 
            this.btnPreviousPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreviousPlot.Location = new System.Drawing.Point(3, 375);
            this.btnPreviousPlot.Name = "btnPreviousPlot";
            this.btnPreviousPlot.Size = new System.Drawing.Size(75, 32);
            this.btnPreviousPlot.TabIndex = 1;
            this.btnPreviousPlot.Text = "Previous";
            this.btnPreviousPlot.UseVisualStyleBackColor = true;
            // 
            // plotSurface2D1
            // 
            this.plotSurface2D1.AutoScaleAutoGeneratedAxes = false;
            this.plotSurface2D1.AutoScaleTitle = false;
            this.plotSurface2D1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.plotSurface2D1.DateTimeToolTip = false;
            this.plotSurface2D1.Legend = null;
            this.plotSurface2D1.LegendZOrder = -1;
            this.plotSurface2D1.Location = new System.Drawing.Point(3, 3);
            this.plotSurface2D1.Name = "plotSurface2D1";
            this.plotSurface2D1.RightMenu = null;
            this.plotSurface2D1.ShowCoordinates = true;
            this.plotSurface2D1.Size = new System.Drawing.Size(695, 366);
            this.plotSurface2D1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.plotSurface2D1.TabIndex = 0;
            this.plotSurface2D1.Text = "plotSurface2D1";
            this.plotSurface2D1.Title = "";
            this.plotSurface2D1.TitleFont = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.plotSurface2D1.XAxis1 = null;
            this.plotSurface2D1.XAxis2 = null;
            this.plotSurface2D1.YAxis1 = null;
            this.plotSurface2D1.YAxis2 = null;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel2});
            this.statusStrip.Location = new System.Drawing.Point(0, 531);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(772, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(109, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(772, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadSetupToolStripMenuItem,
            this.saveSetupToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadSetupToolStripMenuItem
            // 
            this.loadSetupToolStripMenuItem.Name = "loadSetupToolStripMenuItem";
            this.loadSetupToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.loadSetupToolStripMenuItem.Text = "Load Setup";
            this.loadSetupToolStripMenuItem.Click += new System.EventHandler(this.loadSetupToolStripMenuItem_Click);
            // 
            // saveSetupToolStripMenuItem
            // 
            this.saveSetupToolStripMenuItem.Name = "saveSetupToolStripMenuItem";
            this.saveSetupToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.saveSetupToolStripMenuItem.Text = "Save Setup";
            this.saveSetupToolStripMenuItem.Click += new System.EventHandler(this.saveSetupToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // mzTabTimer
            // 
            this.mzTabTimer.Interval = 1000;
            this.mzTabTimer.Tick += new System.EventHandler(this.mzTabTimer_Tick);
            // 
            // laserTabTimer
            // 
            this.laserTabTimer.Interval = 1000;
            this.laserTabTimer.Tick += new System.EventHandler(this.laserTabTimer_Tick);
            // 
            // cmbReadingMode
            // 
            this.cmbReadingMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbReadingMode.Enabled = false;
            this.cmbReadingMode.FormattingEnabled = true;
            this.cmbReadingMode.Items.AddRange(new object[] {
            "Decimal",
            "Hex",
            "Real"});
            this.cmbReadingMode.Location = new System.Drawing.Point(684, 532);
            this.cmbReadingMode.Name = "cmbReadingMode";
            this.cmbReadingMode.Size = new System.Drawing.Size(72, 21);
            this.cmbReadingMode.TabIndex = 3;
            this.cmbReadingMode.Text = "Decimal";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(644, 535);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "Mode";
            // 
            // debugLogTimer
            // 
            this.debugLogTimer.Interval = 250;
            this.debugLogTimer.Tick += new System.EventHandler(this.debugLogTimer_Tick);
            // 
            // rxTabTimer
            // 
            this.rxTabTimer.Interval = 1000;
            this.rxTabTimer.Tick += new System.EventHandler(this.rxTabTimerTick);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "Enable EDFA";
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 553);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.cmbReadingMode);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Module Control";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.channelSelect)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBottomDataModCtrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkBottomDataModCtrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tkTopDataModCtrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTopDataModCtrl)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.calibrationTabs.ResumeLayout(false);
            this.calTabDAC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dacCalDataGrid)).EndInit();
            this.calTabGoldBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.calTabPlots.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton absoluteMode;
        private System.Windows.Forms.RadioButton dacMode;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bookham.Toolkit.ModuleController.DSDBRSectionsCtrl dsdbrSectionsCtrl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnSendPassword;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabControl calibrationTabs;
        private System.Windows.Forms.TabPage calTabDAC;
        private System.Windows.Forms.DataGridView dacCalDataGrid;
        private System.Windows.Forms.ProgressBar dacCalOverallProgressBar;
        private System.Windows.Forms.ProgressBar dacCalChannelProgressBar;
        private System.Windows.Forms.TabPage calTabGoldBox;
        private System.Windows.Forms.TabPage calTabPlots;
        private System.Windows.Forms.Button btnNextPlot;
        private System.Windows.Forms.Button btnPreviousPlot;
        private NPlot.Windows.PlotSurface2D plotSurface2D1;
        private System.Windows.Forms.Button dacCalStartCal;
        private System.Windows.Forms.Button dacCalSaveFile;
        private System.Windows.Forms.Button dacCalLoadFile;
        private System.Windows.Forms.Button loadGBData;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblRegisterNo;
        private System.Windows.Forms.TextBox txtRegisterNumber;
        private System.Windows.Forms.TextBox txtByteVal;
        private System.Windows.Forms.Button btnWriteByte;
        private System.Windows.Forms.TextBox txtReadByte;
        private System.Windows.Forms.TextBox txtReadStr;
        private System.Windows.Forms.TextBox txtWriteStr;
        private System.Windows.Forms.Button btnReadStr;
        private System.Windows.Forms.Button btnWriteStr;
        private System.Windows.Forms.Button btnReadVal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUnlockStatus;
        private System.Windows.Forms.Button btnCheckLockStatus;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnSelectChannel;
        private System.Windows.Forms.Button btnRecallChannel;
        private System.Windows.Forms.NumericUpDown channelSelect;
        private System.Windows.Forms.Label lblMzTemp;
        private System.Windows.Forms.TextBox txtMzTemp;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox ckVen1;
        private System.Windows.Forms.Button btnReadTcmzMonitors;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lblPcbTemp;
        private System.Windows.Forms.TextBox txtPcbTempSense;
        private System.Windows.Forms.Label lbl5v2Sense;
        private System.Windows.Forms.TextBox txt5v2Sense;
        private System.Windows.Forms.CheckBox ckVen15;
        private System.Windows.Forms.CheckBox ckVen14;
        private System.Windows.Forms.CheckBox ckVen13;
        private System.Windows.Forms.CheckBox ckVen12;
        private System.Windows.Forms.CheckBox ckVen11;
        private System.Windows.Forms.CheckBox ckVen10;
        private System.Windows.Forms.CheckBox ckVen9;
        private System.Windows.Forms.CheckBox ckVen8;
        private System.Windows.Forms.CheckBox ckVen7;
        private System.Windows.Forms.CheckBox ckVen6;
        private System.Windows.Forms.CheckBox ckVen5;
        private System.Windows.Forms.CheckBox ckVen4;
        private System.Windows.Forms.CheckBox ckVen3;
        private System.Windows.Forms.CheckBox ckVen2;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox ckCtrlReg0;
        private System.Windows.Forms.CheckBox ckCtrlReg15;
        private System.Windows.Forms.CheckBox ckCtrlReg14;
        private System.Windows.Forms.CheckBox ckCtrlReg13;
        private System.Windows.Forms.CheckBox ckCtrlReg12;
        private System.Windows.Forms.CheckBox ckCtrlReg11;
        private System.Windows.Forms.CheckBox ckCtrlReg10;
        private System.Windows.Forms.CheckBox ckCtrlReg9;
        private System.Windows.Forms.CheckBox ckCtrlReg8;
        private System.Windows.Forms.CheckBox ckCtrlReg7;
        private System.Windows.Forms.CheckBox ckCtrlReg6;
        private System.Windows.Forms.CheckBox ckCtrlReg5;
        private System.Windows.Forms.CheckBox ckCtrlReg4;
        private System.Windows.Forms.CheckBox ckCtrlReg3;
        private System.Windows.Forms.CheckBox ckCtrlReg2;
        private System.Windows.Forms.CheckBox ckCtrlReg1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox ckCtrlStatus0;
        private System.Windows.Forms.CheckBox ckCtrlStatus15;
        private System.Windows.Forms.CheckBox ckCtrlStatus14;
        private System.Windows.Forms.CheckBox ckCtrlStatus13;
        private System.Windows.Forms.CheckBox ckCtrlStatus12;
        private System.Windows.Forms.CheckBox ckCtrlStatus11;
        private System.Windows.Forms.CheckBox ckCtrlStatus10;
        private System.Windows.Forms.CheckBox ckCtrlStatus9;
        private System.Windows.Forms.CheckBox ckCtrlStatus8;
        private System.Windows.Forms.CheckBox ckCtrlStatus7;
        private System.Windows.Forms.CheckBox ckCtrlStatus6;
        private System.Windows.Forms.CheckBox ckCtrlStatus5;
        private System.Windows.Forms.CheckBox ckCtrlStatus4;
        private System.Windows.Forms.CheckBox ckCtrlStatus3;
        private System.Windows.Forms.CheckBox ckCtrlStatus2;
        private System.Windows.Forms.CheckBox ckCtrlStatus1;
        private System.Windows.Forms.CheckBox ckVen0;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnReadModuleIdentity;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtFwBackRO;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtFwRelRO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMfgDateRO;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtModelRO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtManufacturerRO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDevTypeRO;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnWriteModuleIdentity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFwVersionWrite;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBuildDateWrite;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPcbSerialWrite;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tstLaserSerialWrite;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtModSerialWrite;
        private System.Windows.Forms.Button btnSaveSetup;
        private System.Windows.Forms.Button btnReadUpperModuleIdRegisters;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MZSectionsCtrl mzSectionsCtrl_L;
        private MZSectionsCtrl mzSectionsCtrl_R;
        private MZSectionsCtrl mzSectionsCtrl_O;
        private MZSectionsCtrl mzSectionsCtrl_PC;
        private System.Windows.Forms.Label lblCarverTapRf;
        private System.Windows.Forms.TextBox txtCarverTapRf;
        private System.Windows.Forms.Label lblOutputRfDc;
        private System.Windows.Forms.TextBox txtOutputRfDc;
        private System.Windows.Forms.Label lblOutInlineDc;
        private System.Windows.Forms.TextBox txtOutInlineDc;
        private System.Windows.Forms.Label lblLmmiDc;
        private System.Windows.Forms.TextBox txtLmmiDc;
        private System.Windows.Forms.Label lblUmmiDc;
        private System.Windows.Forms.TextBox txtUmmiDc;
        private System.Windows.Forms.Label lblCarverTap;
        private System.Windows.Forms.TextBox txtCarverTap;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Timer mzTabTimer;
        private System.Windows.Forms.CheckBox checkAutoUpdate;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TrackBar tkTopDataModCtrl;
        private System.Windows.Forms.NumericUpDown numTopDataModCtrl;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown numBottomDataModCtrl;
        private System.Windows.Forms.TrackBar tkBottomDataModCtrl;
        private System.Windows.Forms.CheckBox ckLaserAutoUpdate;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtLsPhaseSense;
        private System.Windows.Forms.Label lblLsPhaseSense;
        private System.Windows.Forms.Label lblLaserMon;
        private System.Windows.Forms.TextBox txtLaserMon;
        private System.Windows.Forms.Label lblRxMon;
        private System.Windows.Forms.TextBox txtRxMon;
        private System.Windows.Forms.Label lblTxMon;
        private System.Windows.Forms.TextBox txtTxMon;
        private System.Windows.Forms.Timer laserTabTimer;
        private System.Windows.Forms.Label lblLaserTemp;
        private System.Windows.Forms.TextBox txtLaserTemp;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox18;
        private ModuleController.UserControls.scrollboxAndSlider tecCtrl;
        private ModuleController.UserControls.scrollboxAndSlider arm2HeaterCtrl;
        private ModuleController.UserControls.scrollboxAndSlider arm1HeaterCtrl;
        private ModuleController.UserControls.scrollboxAndSlider mainHeaterCtrl;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox demodulatorThermistor;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Rx1NegVout;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Rx1PosVout;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Rx1NegMon_I;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Rx1PosMon_I;
        private ModuleController.UserControls.scrollboxAndSlider rx1PeakCtrlV;
        private ModuleController.UserControls.scrollboxAndSlider rx1NegDcOffsetV;
        private ModuleController.UserControls.scrollboxAndSlider rx1PosDcOffsetV;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Rx2NegVout;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Rx2PosVout;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Rx2NegMon_I;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Rx2PosMon_I;
        private ModuleController.UserControls.scrollboxAndSlider rx2PeakCtrlV;
        private ModuleController.UserControls.scrollboxAndSlider rx2NegDcOffsetV;
        private ModuleController.UserControls.scrollboxAndSlider rx2PosDcOffsetV;
        private System.Windows.Forms.Button ReadRx;
        private System.Windows.Forms.ComboBox cmbReadingMode;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox messageLog;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.CheckBox ckLoggingEnabled;
        private System.Windows.Forms.Timer debugLogTimer;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox evalEdfaPower;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox evalPcbTemp;
        private System.Windows.Forms.Timer rxTabTimer;
        private System.Windows.Forms.CheckBox RxTimerEnabled;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox evalPcbStm32Temp;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.CheckBox evalDigiOut3;
        private System.Windows.Forms.CheckBox evalDigiOut2;
        private System.Windows.Forms.CheckBox evalDigiOut1;
        private System.Windows.Forms.CheckBox evalDigiOut0;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox evalPcbVref;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private ModuleController.UserControls.scrollboxAndSlider edfaPowerControl;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem loadSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSetupToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog setupFileDialog;
        private System.Windows.Forms.CheckBox cbFastUpdate;
    }
}


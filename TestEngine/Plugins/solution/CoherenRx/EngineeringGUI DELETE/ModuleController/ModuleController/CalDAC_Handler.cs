using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestSolution.TestModules;
using Bookham.TestLibrary.Utilities;
using System.IO;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestSolution.TestPrograms.TunableModules;


namespace ModuleController
{
    /// <summary>
    /// Apart from the Run() method, this was lifted straight from the production TestProgram.
    /// It provides the necessary inputs required to run the test module.
    /// </summary>
    public class CalDAC_Handler
    {

        public CalDAC_Handler()
        {

        }

        /// <summary>
        /// Performs initialisation on the module, then runs it.
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        public DatumList Run(LocalEngine engine, DUTObject dutObject)
        {
            tsffPcbCalParamsConfig = new TestParamConfigAccessor(dutObject,
                "Configuration/TSFF/tsffPcbCalTestParams.xml", "ALL", "TsffPcbCalTestParams");

            this.dutModule = InstrumentLoader.InstrumentList["BlackBox"];
            this.switchChassis = InstrumentLoader.ChassisList["Chassis_Ag34970A"];
            this.dauDMM_0 = InstrumentLoader.InstrumentList["dauDMM_1"];
            this.dauDMM_1 = InstrumentLoader.InstrumentList["dauDMM_2"];
            this.dauDMM_2 = InstrumentLoader.InstrumentList["dauDMM_3"];

            ITestEngineInit engineInit = (ITestEngineInit)engine;
            DacTest_InitModule(engineInit);

            ITestEngineRun engineRun = (ITestEngineRun)engine;
            return DacTest(engineRun);
        }



        /// <summary>
        /// The Dac test init (or set up of test params / data etc)
        /// </summary>
        private void DacTest_InitModule(ITestEngineInit engine)
        {
            foreach (int myChannel in Enum.GetValues(typeof(Tsff_CalDAC_Test_Config.CalDacChannel)))
            {
                ModuleRun modRun;
                Tsff_CalDAC_Test_Config.CalDacChannel myParticularChannel = (Tsff_CalDAC_Test_Config.CalDacChannel)myChannel;


                string myChannelName = myParticularChannel.ToString();
                Tsff_CalDAC_Test_Config myDacTest;

                if (tsffPcbCalParamsConfig.GetBoolParam(myChannelName + "_CalDacTestRqd"))
                {
                    modRun = engine.AddModuleRun((myChannelName + "_CalDAC_Test"), "Tsff_CalDAC_Test", "");

                    //I need to decide which of my 3 multimeter channels 120, 121, 122 that i am connected to
                    if ((myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.MZLEFTBIAS) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.MZRIGHTBIAS))
                    {
                        modRun.Instrs.Add("dauDMM", this.dauDMM_0);
                    }
                    else if ((myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.FRONTODD) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.REAR) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.SOA))
                    {
                        modRun.Instrs.Add("dauDMM", this.dauDMM_1);
                    }
                    else if ((myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.PHASE) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.GAIN) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.FRONTEVEN) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.LEFTIMB) ||
                        (myParticularChannel == Tsff_CalDAC_Test_Config.CalDacChannel.RIGHTIMB))
                    {
                        modRun.Instrs.Add("dauDMM", this.dauDMM_2);
                    }
                    else
                    {
                        engine.ErrorInProgram("The Enum tells us the dau switch channel, but i don't recognise which muiltimeter channel to use 120, 121, 122?, is this a newly added enum channel?");
                    }


                    modRun.Instrs.Add("CalModule", this.dutModule);
                    modRun.Chassis.Add("Chassis_Ag34970A", this.switchChassis);


                    myDacTest.DacChannel = myParticularChannel;
                    myDacTest.DacStartValue = (tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_CalDacStartValue"));
                    myDacTest.DacStopValue = (tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_CalDacStopValue"));

                    // These params will be wildcarded:  There will be default All All values for calc Grad1, and calc Grad2 values (ie false, false)
                    // and default values for the start / stop dac values 0, 0, and also DsdbrFrontSection,
                    // For each device / channel that really 'is' required to do this test there will be valid entries.
                    myDacTest.CalculateGrad1Value = tsffPcbCalParamsConfig.GetBoolParam(myChannelName + "_CalculateGrad1Rqd");
                    myDacTest.CalculateGrad2Value = tsffPcbCalParamsConfig.GetBoolParam(myChannelName + "_CalculateGrad2Rqd");
                    myDacTest.DacStartValue_Grad1Calc = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_Grad1DacStartValue");
                    myDacTest.DacStopValue_Grad1Calc = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_Grad1DacStopValue");
                    myDacTest.DacStartValue_Grad2Calc = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_Grad2DacStartValue");
                    myDacTest.DacStopValue_Grad2Calc = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_Grad2DacStopValue");
                    myDacTest.DacStepSize = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_DacStepSize");
                    myDacTest.DsdbrFrontSection = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_DsdbrFrontSection");
                    myDacTest.DacValForMinOutput = tsffPcbCalParamsConfig.GetIntParam(myChannelName + "_DacValForMinOutput");

                    modRun.ConfigData.AddListItems(myDacTest.GetDatumList());

                    //
                    // COMMENTED OUT
                    //

                    //Need to provide some test limits (only if necessary)
                    //if (myDacTest.CalculateGrad1Value)
                    //{
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_SLOPE_GRADIENT1", "SlopeGradient1");
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_Y_INTERCEPT1", "YIntercept1");
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_CORRELATION_COEFF1", "CorrelationCoefficient1");
                    //}

                    //if (myDacTest.CalculateGrad2Value)
                    //{
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_SLOPE_GRADIENT2", "SlopeGradient2");
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_Y_INTERCEPT2", "YIntercept2");
                    //    modRun.Limits.AddParameter(this.testSpec, myChannelName + "_CORRELATION_COEFF2", "CorrelationCoefficient2");
                    //}
                }
            }
        }


        /// <summary>
        /// Does the Dac test multiple times as reqd,
        /// </summary>
        /// <param name="engine">our test engine</param>
        /// <returns>a datumlist containing all the plots</returns>
        private DatumList DacTest(ITestEngineRun engine)
        {
            DatumList myCompleteSetOfPlots = new DatumList();
            foreach (int myChannel in Enum.GetValues(typeof(Tsff_CalDAC_Test_Config.CalDacChannel)))
            {

                Tsff_CalDAC_Test_Config.CalDacChannel myParticularChannel = (Tsff_CalDAC_Test_Config.CalDacChannel)myChannel;

                string myChannelName = myParticularChannel.ToString();

                if (tsffPcbCalParamsConfig.GetBoolParam(myChannelName + "_CalDacTestRqd"))
                {
                    ModuleRunReturn modRunRtn;

                    modRunRtn = engine.RunModule((myChannelName + "_CalDAC_Test"));
                    DatumList myDatumResults = modRunRtn.ModuleRunData.ModuleData;

                    DatumPlot mylocalPlot = myDatumResults.ReadPlotDatum(myChannelName);

                    myCompleteSetOfPlots.Add(mylocalPlot);

                }
            }

            //Need to add characterised MZ Modulation DAC data. This is stored in file
            //Configuration\TSFF\MzModData.csv

            string mzDataFile = Path.GetFullPath(@"Configuration\TSFF\MzModData.csv");
            //Make sure that the MzModDac Data file exists. Throw a fatal exception if not.
            if (!File.Exists(mzDataFile))
            {
                engine.RaiseNonParamFail(0, "TSFF PCB Test Program: Unable to read MZ Modulation characterised data file Configuration\\TSFF\\MzModData.csv");
            }
            List<string[]> fileElements = null;
            // Read the delimited file
            using (DelimitedFileReader reader = new DelimitedFileReader(DelimitedFileDelimiter.Comma, true))
            {
                fileElements = reader.ReadFile(mzDataFile);
            }

            //First, find out how many lines there are in the file           
            if (fileElements.Count != 130)
            {
                // Invalid File Format. Must be 1 line of header, followed by 129 lines of data.
                engine.RaiseNonParamFail(0, "TSFF PCB Test Program: Mz Modulation Dac Charaterisation data file has in correct format: " + mzDataFile);
            }

            //Extract the data into two arrays of doubles, representing points on a graph plot.
            //The X-Axis shall be used for the Mz Modulation DAC settings (0 - 4095) and the 
            //Y-Axis will hold the resultant Vpi Voltage. Note that the DAC values are really 
            //integers, but we are storing them as doubles to make life easier.
            double[] xValuesArray = new double[129];
            double[] yValuesArray = new double[129];

            for (int i = 0; i < 129; i++)
            {
                xValuesArray[i] = Convert.ToDouble(fileElements[i + 1][0]);
                yValuesArray[i] = Convert.ToDouble(fileElements[i + 1][1]);
            }

            // Add the read data to DatumPlot objects.
            PlotAxis plotXAxis = new PlotAxis("DacValue", "CamoflagedInt32", xValuesArray);
            PlotAxis plotYAxis = new PlotAxis("Readings", "V", yValuesArray);
            DatumPlot dacDataPlot = new DatumPlot("MZMOD", plotXAxis, plotYAxis);

            //Add the MZ Mod Dac Data to the list of plots.
            myCompleteSetOfPlots.Add(dacDataPlot);


            return (myCompleteSetOfPlots);
        }

        /// <summary>
        /// Does array collaton and writing out CalDac data to csv
        /// </summary>
        /// <param name="engine">our test engine</param>
        /// <param name="resultsFilePath">The file path you want csv stored to</param>>
        /// <param name="previousTestData">needs to be passed the datumplots recorded previously</param>
        /// <returns>DatumList containing a Datum</returns>
        internal DatumFileLink calDacSaveResults(ITestEngineRun engine, string resultsFilePath, DatumList previousTestData)
        {
            //DatumList myTestModuleData = new DatumList();

            int TotalColumnCount = previousTestData.Count * 2;
            int maxRowCount = 0;


            //Simple loop to find the biggest array size in my DatumPlot list
            foreach (DatumPlot myPlot in previousTestData)
            {

                PlotAxis dacValueAxis = myPlot.GetAxisHandle("DacValue");

                if (dacValueAxis.Count > maxRowCount)
                {
                    maxRowCount = dacValueAxis.Count + 1; //Include 1 extra row for our columnNames
                }
            }

            //Now we create a rectangular array as this will be easier to use for storing
            //my csv data
            string[,] myDataArray = new string[maxRowCount, TotalColumnCount];

            int colId = 0;


            foreach (DatumPlot myPlot in previousTestData)
            {
                // Each DatumPlot will contain 2 axis.  
                // A dacValue, and a Readings axis 
                // So the foreach block needs to handle both of these axis

                //Put the columnNames details in place
                string PlotName = myPlot.Name;
                myDataArray[0, colId] = PlotName + "Dac";

                if (myPlot.Name.Contains("BIAS") || myPlot.Name.Contains("Bias"))
                {
                    myDataArray[0, colId + 1] = PlotName + "Voltage_V";
                }
                else
                {
                    myDataArray[0, colId + 1] = PlotName + "Current_mA";
                }
                PlotAxis dacValueAxis = myPlot.GetAxisHandle("DacValue");
                PlotAxis readingsAxis = myPlot.GetAxisHandle("Readings");

                for (int i = 0; i < (dacValueAxis.Count); i++)
                {
                    //The Dac value is an integer, lets ensure we save it as this
                    Int32 dacValue = (Int32)dacValueAxis[i];
                    myDataArray[i + 1, colId] = dacValue.ToString();
                    //readings are double, so no need to cast it
                    myDataArray[i + 1, colId + 1] = readingsAxis[i].ToString();
                }

                //populate any missing elements with empty spaces..
                for (int i = dacValueAxis.Count + 1; i < myDataArray.GetUpperBound(1); i++)
                {
                    myDataArray[i + 1, colId] = "";
                    myDataArray[i + 1, colId + 1] = "";
                }

                //move the colId on 2 places
                colId = colId + 2;
            }

            //Ok, so now we have a nice rectangular array, all ready for writing out to our csv File
            string mytimeDateFilePath = (generateUniqueFilename() + ".csv");
            DatumFileLink ResultsBlobFilePath = new DatumFileLink("ResultsBlobFilePath", resultsFilePath, mytimeDateFilePath);

            if (!Directory.Exists(Path.GetFullPath(resultsFilePath)))
            {
                Directory.CreateDirectory(Path.GetFullPath(resultsFilePath));
            }

            CalDacWriter myCalWriter = new CalDacWriter(engine);
            string pathCombination = Path.Combine(resultsFilePath, mytimeDateFilePath);


            myCalWriter.CreateFile(pathCombination);
            myCalWriter.WriteCsvTable(myDataArray);
            myCalWriter.CloseFile();
            return (ResultsBlobFilePath);
        }



        /// <summary>
        /// Generate a unique filename to store some blob data in. 
        /// Filename shall be derived from the current system date and time, in the following
        /// format:
        ///       YYYYMMDDHHNNSSUUU
        ///
        /// where YYYY is the four digit year
        ///       MM is the month
        ///       DD is the day
        ///       HH is the hour
        ///       NN is the minute
        ///      SS is the second
        ///      UUU is the milliseconds
        /// 
        /// The caller must append a file extension to the generated filename.
        /// </summary>
        /// <returns>The filename</returns>
        internal string generateUniqueFilename()
        {
            System.DateTime now = DateTime.UtcNow;
            //Do the next line to guarantee we can't have same filename in same ms on this clock
            System.Threading.Thread.Sleep(1);
            return now.ToString("yyyyMMddHHmmssfff");
        }


        #region Private data
        /// <summary>
        /// The test boards +1.8V supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc1pt8Supply;
        /// <summary>
        /// The test boards +3V supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc3Supply;
        /// <summary>
        /// The test board +5V  supply
        /// </summary>
        InstType_ElectricalSource testBoardVcc5Supply;
        /// <summary>
        /// The testt board -5.2V supply
        /// </summary>
        InstType_ElectricalSource testBoardVee5pt2Supply;
        ///// <summary>
        ///// Our TSFF chassis
        ///// </summary>
        //Chassis TsffChassis;
        /// <summary>
        /// Our DUT 
        /// </summary>
        Instrument dutModule;
        /// <summary>
        /// The switch chassis we are using
        /// </summary>
        Chassis switchChassis;
        /// <summary>
        /// Multimeter 0 (Channel 120)
        /// </summary>
        Instrument dauDMM_0;
        /// <summary>
        /// Multimeter 1 (Channel 121)
        /// </summary>
        Instrument dauDMM_1;
        /// <summary>
        /// Multimeter 2 (Channel 122)
        /// </summary>
        Instrument dauDMM_2;

        /// <summary>
        /// Our Line Wide test parameters
        /// </summary>
        TestParamConfigAccessor tsffPcbCalParamsConfig;

        /// <summary>
        /// Our Node Specific  test parameters
        /// </summary>
        TestParamConfigAccessor nodeSpecificTestParamsConfig;
        /// <summary>
        /// Our test specification
        /// </summary>
        Specification testSpec;
        /// <summary>
        /// Our test conditions
        /// </summary>
        DatumList testConditions;

        /// <summary>
        /// Our Blob File Link
        /// </summary>
        DatumFileLink blobFileData;
        #endregion
    
    }
}

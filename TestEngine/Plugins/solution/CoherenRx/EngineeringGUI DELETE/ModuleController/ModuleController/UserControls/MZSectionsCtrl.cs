using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ModuleController.Data;

namespace ModuleController
{
    public partial class MZSectionsCtrl : UserControl
    {
        public MZSectionsCtrl()
        {
            InitializeComponent();
            SetUpperLimits(true);
        }

        public MZBiasSettings Value
        {
            get
            {
                MZBiasSettings mzBiasSettings = new MZBiasSettings();

                mzBiasSettings.leftPhase.Current_mA = (double)leftImbalanceCurrent.Value;
                mzBiasSettings.leftPhase.Enabled = leftImbalanceEnabled.Checked;
                mzBiasSettings.leftPhase.ValueChanged = Convert.ToBoolean(leftImbalanceCurrent.Tag);

                mzBiasSettings.rightPhase.Current_mA = (double)rightImbalanceCurrent.Value;
                mzBiasSettings.rightPhase.Enabled = rightImbalanceEnabled.Checked;
                mzBiasSettings.rightPhase.ValueChanged = Convert.ToBoolean(rightImbalanceCurrent.Tag);

                mzBiasSettings.leftModulation.Voltage_V = (double)leftModulationVoltage.Value;
                mzBiasSettings.leftModulation.Enabled = leftModulationEnabled.Checked;
                mzBiasSettings.leftModulation.ValueChanged = Convert.ToBoolean(leftModulationVoltage.Tag);

                mzBiasSettings.rightModulation.Voltage_V = (double)rightModulationVoltage.Value;
                mzBiasSettings.rightModulation.Enabled = rightModulationEnabled.Checked;
                mzBiasSettings.rightModulation.ValueChanged = Convert.ToBoolean(rightModulationVoltage.Tag);

                return mzBiasSettings;
            }
            set
            {
                leftImbalanceEnabled.Checked = value.leftPhase.Enabled;
                leftImbalanceCurrent.Value = (decimal)value.leftPhase.Current_mA;
                leftImbalanceCurrent.Tag = value.leftPhase.ValueChanged;

                rightImbalanceEnabled.Checked = value.rightPhase.Enabled;
                rightImbalanceCurrent.Value = (decimal)value.rightPhase.Current_mA;
                rightImbalanceCurrent.Tag = value.rightPhase.ValueChanged;

                leftModulationEnabled.Checked = value.leftModulation.Enabled;
                leftModulationVoltage.Value = (decimal)value.leftModulation.Voltage_V;
                leftModulationVoltage.Tag = value.leftModulation.ValueChanged;

                rightModulationEnabled.Checked = value.rightModulation.Enabled;
                rightModulationVoltage.Value = (decimal)value.rightModulation.Voltage_V;
                rightModulationVoltage.Tag = value.rightModulation.ValueChanged;
            }
        }

        public void SetTitle(string sectionName)
        {
            this.sectionEnabled.Text = sectionName;
        }


        public bool HideRfControls
        {
            get { return hideRfControls; }
            set 
            {
                hideRfControls = value;

                leftModulationVoltage.Visible = !value;
                leftModSlider.Visible = !value;
                rightModulationVoltage.Visible = !value;
                rightModSlider.Visible = !value;
                leftModulationEnabled.Visible = !value;
                rightModulationEnabled.Visible = !value;
            }
        }

        public void ResetColours()
        {
            leftImbalanceCurrent.ForeColor = Color.Black;
            leftModulationVoltage.ForeColor = Color.Black;
            rightImbalanceCurrent.ForeColor = Color.Black;
            rightModulationVoltage.ForeColor = Color.Black;
        }

        public bool EnableSection
        {
            get { return sectionEnabled.Checked; }
            set { sectionEnabled.Checked = value; }
        }

        const int MaxImbDacDefault = 1023;
        const int MaxBiasDacDefault = 4095;
        const int MaxImbValDefault = 8;
        const int MaxBiasValDefault = 8;

        public void SetUpperLimits(bool dacMode)
        {
            if (dacMode)
            {
                SetUpperLimits(dacMode, MaxImbDacDefault, MaxBiasDacDefault);
            }
            else
            {
                SetUpperLimits(dacMode, MaxImbValDefault, MaxBiasValDefault);
            }

        }

        public void SetUpperLimits(bool dacMode, int maxImb, int maxBias)
        {
            if (dacMode)
            {
                leftImbalanceCurrent.Maximum = maxImb;
                leftImbalanceSlider.Maximum = maxImb;
                leftImbalanceSlider.LargeChange = 50;

                rightImbalanceCurrent.Maximum = maxImb;
                rightImbalanceSlider.Maximum = maxImb;
                rightImbalanceSlider.LargeChange = 50;


                leftModulationVoltage.Maximum = maxBias;
                leftModulationVoltage.Minimum = 0;

                leftModSlider.Maximum = maxBias;
                leftModSlider.Minimum = 0;
                leftModSlider.LargeChange = 50;


                rightModulationVoltage.Maximum = maxBias;
                rightModulationVoltage.Minimum = 0;

                rightModSlider.Maximum = maxBias;
                rightModSlider.Minimum = 0;
                rightModSlider.LargeChange = 50;

            }
            else
            {
                leftImbalanceCurrent.Maximum = maxImb;
                leftImbalanceSlider.Maximum = maxImb;

                rightImbalanceCurrent.Maximum = maxImb;
                rightImbalanceSlider.Maximum = maxImb;

                leftModulationVoltage.Minimum = -maxBias;
                leftModulationVoltage.Maximum = 0;
                leftModSlider.Maximum = 0;
                leftModSlider.Minimum = -8;

                rightModulationVoltage.Maximum = 0;
                rightModulationVoltage.Minimum = -maxBias;
                rightModSlider.Maximum = 0;
                rightModSlider.Minimum = -maxBias;
            }
            leftModSlider.TickFrequency = 1 + (int)(leftModulationVoltage.Maximum - leftModulationVoltage.Minimum) / 16;
            rightModSlider.TickFrequency = 1 + (int)(rightModulationVoltage.Maximum - rightModulationVoltage.Minimum) / 16;
            leftImbalanceSlider.TickFrequency = 1 + (int)(leftImbalanceCurrent.Maximum - leftImbalanceCurrent.Minimum) / 16;
            rightImbalanceSlider.TickFrequency = 1 +  (int)(rightImbalanceCurrent.Maximum - rightImbalanceCurrent.Minimum) / 16;
        }


        private void sectionEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (!hideRfControls)
            {            
                leftModulationVoltage.Enabled = sectionEnabled.Checked;
                rightModulationVoltage.Enabled = sectionEnabled.Checked;
                leftModulationEnabled.Enabled = sectionEnabled.Checked;
                rightModulationEnabled.Enabled = sectionEnabled.Checked;
                leftModSlider.Enabled = sectionEnabled.Checked;
                rightModSlider.Enabled = sectionEnabled.Checked;
            }
            
            leftImbalanceCurrent.Enabled = sectionEnabled.Checked;
            rightImbalanceCurrent.Enabled = sectionEnabled.Checked;
            leftImbalanceEnabled.Enabled = sectionEnabled.Checked;
            rightImbalanceEnabled.Enabled = sectionEnabled.Checked;
            leftImbalanceSlider.Enabled = sectionEnabled.Checked;
            rightImbalanceSlider.Enabled = sectionEnabled.Checked;
            if (sectionEnabled.Checked)
            {
                leftImbalanceEnabled.Checked = true;
                rightImbalanceEnabled.Checked = true;
                leftModulationEnabled.Checked = true;
                rightModulationEnabled.Checked = true;
            }
        }

        private bool hideRfControls;

        private void leftImbalanceCurrent_ValueChanged(object sender, EventArgs e)
        {
            leftImbalanceSlider.Value = (int)leftImbalanceCurrent.Value;
            leftImbalanceCurrent.ForeColor = Color.IndianRed;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            leftImbalanceCurrent.Value = leftImbalanceSlider.Value;
        }

        private void rightImbalanceCurrent_ValueChanged(object sender, EventArgs e)
        {
            rightImbalanceSlider.Value = (int)rightImbalanceCurrent.Value;
            rightImbalanceCurrent.ForeColor = Color.IndianRed;
        }

        private void rightImbalanceSlider_Scroll(object sender, EventArgs e)
        {
            rightImbalanceCurrent.Value = rightImbalanceSlider.Value;
        }

        private void leftModulationCurrent_ValueChanged(object sender, EventArgs e)
        {
            leftModSlider.Value = (int)(leftModulationVoltage.Value);
            leftModulationVoltage.ForeColor = Color.IndianRed;
        }

        private void leftModSlider_Scroll(object sender, EventArgs e)
        {
            leftModulationVoltage.Value = leftModSlider.Value;
        }

        private void rightModulationCurrent_ValueChanged(object sender, EventArgs e)
        {
            rightModSlider.Value = (int)rightModulationVoltage.Value;
            rightModulationVoltage.ForeColor = Color.IndianRed;
        }

        private void rightModSlider_Scroll(object sender, EventArgs e)
        {
            rightModulationVoltage.Value = rightModSlider.Value;
        }

        public bool SendImmediately
        {
            get { return sendImmediately; }
            set { sendImmediately = value; }
        }

        private bool sendImmediately;


        internal Worker.MzSectionName SectionName
        {
            get { return sectionName; }
            set { sectionName = value; }
        }

        private Worker.MzSectionName sectionName;

        private void leftImbalanceSlider_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SendImmediately || !leftImbalanceEnabled.Checked) return;
            Worker.SetMzLeftPhaseDac(SectionName, (int)leftImbalanceCurrent.Value);
            leftImbalanceCurrent.ForeColor = Color.Black;
        }

        private void rightImbalanceSlider_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SendImmediately || !rightImbalanceEnabled.Checked) return;
            Worker.SetMzRightPhaseDac(SectionName, (int)rightImbalanceCurrent.Value);
            rightImbalanceCurrent.ForeColor = Color.Black;
        }

        private void leftModSlider_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SendImmediately || ! leftModulationEnabled.Checked) return;
            Worker.SetMzLeftRfBiasDac(SectionName, (int)leftModSlider.Value);
            leftModulationVoltage.ForeColor = Color.Black;
        }

        private void rightModSlider_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SendImmediately || ! rightModulationEnabled.Checked) return;
            Worker.SetMzRightRfBiasDac(SectionName, (int)rightModSlider.Value);
            rightModulationVoltage.ForeColor = Color.Black;
        }

        private void leftImbalanceSlider_KeyUp(object sender, KeyEventArgs e)
        {
            leftImbalanceSlider_MouseUp(null, null);
        }

        private void rightImbalanceSlider_KeyUp(object sender, KeyEventArgs e)
        {
            rightImbalanceSlider_MouseUp(null, null);
        }

        private void leftModSlider_KeyUp(object sender, KeyEventArgs e)
        {
            leftModSlider_MouseUp(null, null);
        }

        private void rightModSlider_KeyUp(object sender, KeyEventArgs e)
        {
            rightModSlider_MouseUp(null, null);
        }

        private void leftImbalanceEnabled_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}

namespace ModuleController
{
    partial class MZSectionsCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leftImbalanceCurrent = new System.Windows.Forms.NumericUpDown();
            this.leftImbalanceEnabled = new System.Windows.Forms.CheckBox();
            this.rightImbalanceEnabled = new System.Windows.Forms.CheckBox();
            this.rightImbalanceCurrent = new System.Windows.Forms.NumericUpDown();
            this.leftModulationEnabled = new System.Windows.Forms.CheckBox();
            this.leftModulationVoltage = new System.Windows.Forms.NumericUpDown();
            this.rightModulationEnabled = new System.Windows.Forms.CheckBox();
            this.rightModulationVoltage = new System.Windows.Forms.NumericUpDown();
            this.sectionEnabled = new System.Windows.Forms.CheckBox();
            this.leftImbalanceSlider = new System.Windows.Forms.TrackBar();
            this.rightImbalanceSlider = new System.Windows.Forms.TrackBar();
            this.leftModSlider = new System.Windows.Forms.TrackBar();
            this.rightModSlider = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.leftImbalanceCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightImbalanceCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftModulationVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightModulationVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftImbalanceSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightImbalanceSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftModSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightModSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // leftImbalanceCurrent
            // 
            this.leftImbalanceCurrent.Location = new System.Drawing.Point(159, 22);
            this.leftImbalanceCurrent.Margin = new System.Windows.Forms.Padding(1);
            this.leftImbalanceCurrent.Name = "leftImbalanceCurrent";
            this.leftImbalanceCurrent.Size = new System.Drawing.Size(55, 20);
            this.leftImbalanceCurrent.TabIndex = 0;
            this.leftImbalanceCurrent.ValueChanged += new System.EventHandler(this.leftImbalanceCurrent_ValueChanged);
            this.leftImbalanceCurrent.KeyUp += new System.Windows.Forms.KeyEventHandler(this.leftImbalanceSlider_KeyUp);
            this.leftImbalanceCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.leftImbalanceSlider_MouseUp);
            // 
            // leftImbalanceEnabled
            // 
            this.leftImbalanceEnabled.AutoSize = true;
            this.leftImbalanceEnabled.Checked = true;
            this.leftImbalanceEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.leftImbalanceEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftImbalanceEnabled.Location = new System.Drawing.Point(0, 26);
            this.leftImbalanceEnabled.Name = "leftImbalanceEnabled";
            this.leftImbalanceEnabled.Size = new System.Drawing.Size(68, 16);
            this.leftImbalanceEnabled.TabIndex = 1;
            this.leftImbalanceEnabled.Text = "Left Phase";
            this.leftImbalanceEnabled.UseVisualStyleBackColor = true;
            this.leftImbalanceEnabled.CheckedChanged += new System.EventHandler(this.leftImbalanceEnabled_CheckedChanged);
            // 
            // rightImbalanceEnabled
            // 
            this.rightImbalanceEnabled.AutoSize = true;
            this.rightImbalanceEnabled.Checked = true;
            this.rightImbalanceEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rightImbalanceEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightImbalanceEnabled.Location = new System.Drawing.Point(0, 49);
            this.rightImbalanceEnabled.Name = "rightImbalanceEnabled";
            this.rightImbalanceEnabled.Size = new System.Drawing.Size(74, 16);
            this.rightImbalanceEnabled.TabIndex = 3;
            this.rightImbalanceEnabled.Text = "Right Phase";
            this.rightImbalanceEnabled.UseVisualStyleBackColor = true;
            // 
            // rightImbalanceCurrent
            // 
            this.rightImbalanceCurrent.Location = new System.Drawing.Point(159, 45);
            this.rightImbalanceCurrent.Margin = new System.Windows.Forms.Padding(1);
            this.rightImbalanceCurrent.Name = "rightImbalanceCurrent";
            this.rightImbalanceCurrent.Size = new System.Drawing.Size(55, 20);
            this.rightImbalanceCurrent.TabIndex = 2;
            this.rightImbalanceCurrent.ValueChanged += new System.EventHandler(this.rightImbalanceCurrent_ValueChanged);
            this.rightImbalanceCurrent.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rightImbalanceSlider_KeyUp);
            this.rightImbalanceCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rightImbalanceSlider_MouseUp);
            // 
            // leftModulationEnabled
            // 
            this.leftModulationEnabled.AutoSize = true;
            this.leftModulationEnabled.Checked = true;
            this.leftModulationEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.leftModulationEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftModulationEnabled.Location = new System.Drawing.Point(0, 72);
            this.leftModulationEnabled.Name = "leftModulationEnabled";
            this.leftModulationEnabled.Size = new System.Drawing.Size(88, 16);
            this.leftModulationEnabled.TabIndex = 5;
            this.leftModulationEnabled.Text = "Left Modulation";
            this.leftModulationEnabled.UseVisualStyleBackColor = true;
            // 
            // leftModulationVoltage
            // 
            this.leftModulationVoltage.Location = new System.Drawing.Point(159, 68);
            this.leftModulationVoltage.Margin = new System.Windows.Forms.Padding(1);
            this.leftModulationVoltage.Name = "leftModulationVoltage";
            this.leftModulationVoltage.Size = new System.Drawing.Size(55, 20);
            this.leftModulationVoltage.TabIndex = 4;
            this.leftModulationVoltage.ValueChanged += new System.EventHandler(this.leftModulationCurrent_ValueChanged);
            this.leftModulationVoltage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.leftModSlider_KeyUp);
            this.leftModulationVoltage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.leftModSlider_MouseUp);
            // 
            // rightModulationEnabled
            // 
            this.rightModulationEnabled.AutoSize = true;
            this.rightModulationEnabled.Checked = true;
            this.rightModulationEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rightModulationEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightModulationEnabled.Location = new System.Drawing.Point(0, 95);
            this.rightModulationEnabled.Name = "rightModulationEnabled";
            this.rightModulationEnabled.Size = new System.Drawing.Size(94, 16);
            this.rightModulationEnabled.TabIndex = 7;
            this.rightModulationEnabled.Text = "Right Modulation";
            this.rightModulationEnabled.UseVisualStyleBackColor = true;
            // 
            // rightModulationVoltage
            // 
            this.rightModulationVoltage.Location = new System.Drawing.Point(159, 91);
            this.rightModulationVoltage.Margin = new System.Windows.Forms.Padding(1);
            this.rightModulationVoltage.Name = "rightModulationVoltage";
            this.rightModulationVoltage.Size = new System.Drawing.Size(55, 20);
            this.rightModulationVoltage.TabIndex = 6;
            this.rightModulationVoltage.ValueChanged += new System.EventHandler(this.rightModulationCurrent_ValueChanged);
            this.rightModulationVoltage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rightModSlider_KeyUp);
            this.rightModulationVoltage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rightModSlider_MouseUp);
            // 
            // sectionEnabled
            // 
            this.sectionEnabled.AutoSize = true;
            this.sectionEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sectionEnabled.Location = new System.Drawing.Point(0, 3);
            this.sectionEnabled.Name = "sectionEnabled";
            this.sectionEnabled.Size = new System.Drawing.Size(105, 17);
            this.sectionEnabled.TabIndex = 8;
            this.sectionEnabled.Text = "Section Name";
            this.sectionEnabled.UseVisualStyleBackColor = true;
            this.sectionEnabled.CheckedChanged += new System.EventHandler(this.sectionEnabled_CheckedChanged);
            // 
            // leftImbalanceSlider
            // 
            this.leftImbalanceSlider.Location = new System.Drawing.Point(87, 22);
            this.leftImbalanceSlider.Margin = new System.Windows.Forms.Padding(1);
            this.leftImbalanceSlider.Name = "leftImbalanceSlider";
            this.leftImbalanceSlider.Size = new System.Drawing.Size(70, 34);
            this.leftImbalanceSlider.TabIndex = 9;
            this.leftImbalanceSlider.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.leftImbalanceSlider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.leftImbalanceSlider_MouseUp);
            // 
            // rightImbalanceSlider
            // 
            this.rightImbalanceSlider.Location = new System.Drawing.Point(88, 45);
            this.rightImbalanceSlider.Margin = new System.Windows.Forms.Padding(1);
            this.rightImbalanceSlider.Name = "rightImbalanceSlider";
            this.rightImbalanceSlider.Size = new System.Drawing.Size(70, 34);
            this.rightImbalanceSlider.TabIndex = 10;
            this.rightImbalanceSlider.Scroll += new System.EventHandler(this.rightImbalanceSlider_Scroll);
            this.rightImbalanceSlider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rightImbalanceSlider_MouseUp);
            // 
            // leftModSlider
            // 
            this.leftModSlider.Location = new System.Drawing.Point(88, 68);
            this.leftModSlider.Margin = new System.Windows.Forms.Padding(1);
            this.leftModSlider.Name = "leftModSlider";
            this.leftModSlider.Size = new System.Drawing.Size(70, 34);
            this.leftModSlider.TabIndex = 11;
            this.leftModSlider.Scroll += new System.EventHandler(this.leftModSlider_Scroll);
            this.leftModSlider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.leftModSlider_MouseUp);
            // 
            // rightModSlider
            // 
            this.rightModSlider.Location = new System.Drawing.Point(88, 91);
            this.rightModSlider.Margin = new System.Windows.Forms.Padding(1);
            this.rightModSlider.Name = "rightModSlider";
            this.rightModSlider.Size = new System.Drawing.Size(69, 34);
            this.rightModSlider.TabIndex = 12;
            this.rightModSlider.Scroll += new System.EventHandler(this.rightModSlider_Scroll);
            this.rightModSlider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rightModSlider_MouseUp);
            // 
            // MZSectionsCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rightModSlider);
            this.Controls.Add(this.leftModSlider);
            this.Controls.Add(this.rightImbalanceSlider);
            this.Controls.Add(this.leftImbalanceSlider);
            this.Controls.Add(this.sectionEnabled);
            this.Controls.Add(this.rightModulationEnabled);
            this.Controls.Add(this.rightModulationVoltage);
            this.Controls.Add(this.leftModulationEnabled);
            this.Controls.Add(this.leftModulationVoltage);
            this.Controls.Add(this.rightImbalanceEnabled);
            this.Controls.Add(this.rightImbalanceCurrent);
            this.Controls.Add(this.leftImbalanceEnabled);
            this.Controls.Add(this.leftImbalanceCurrent);
            this.Name = "MZSectionsCtrl";
            this.Size = new System.Drawing.Size(215, 118);
            ((System.ComponentModel.ISupportInitialize)(this.leftImbalanceCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightImbalanceCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftModulationVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightModulationVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftImbalanceSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightImbalanceSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftModSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightModSlider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown leftImbalanceCurrent;
        private System.Windows.Forms.CheckBox leftImbalanceEnabled;
        private System.Windows.Forms.CheckBox rightImbalanceEnabled;
        private System.Windows.Forms.NumericUpDown rightImbalanceCurrent;
        private System.Windows.Forms.CheckBox leftModulationEnabled;
        private System.Windows.Forms.NumericUpDown leftModulationVoltage;
        private System.Windows.Forms.CheckBox rightModulationEnabled;
        private System.Windows.Forms.NumericUpDown rightModulationVoltage;
        private System.Windows.Forms.CheckBox sectionEnabled;
        private System.Windows.Forms.TrackBar leftImbalanceSlider;
        private System.Windows.Forms.TrackBar rightImbalanceSlider;
        private System.Windows.Forms.TrackBar leftModSlider;
        private System.Windows.Forms.TrackBar rightModSlider;
    }
}

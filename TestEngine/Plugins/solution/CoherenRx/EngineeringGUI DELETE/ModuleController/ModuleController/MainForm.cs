using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestLibrary.Utilities;
using NPlot;
using ModuleController.Data;
using Bookham.TestLibrary.BlackBoxes;
using System.Collections;

namespace ModuleController
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            //AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            InitializeComponent();

#if ! DEBUG
            tabControl.TabPages.Remove(this.tabPage2);
            tabControl.TabPages.Remove(this.tabPage3);
            tabControl.TabPages.Remove(this.tabPage4);
#endif

            mzSectionsCtrl_PC.SetTitle("Pulse Carver");
            mzSectionsCtrl_PC.SectionName = ModuleController.Worker.MzSectionName.PulseCarver;
            mzSectionsCtrl_PC.EnableSection = true;

            mzSectionsCtrl_O.SetTitle("Outer");
            mzSectionsCtrl_O.SectionName = ModuleController.Worker.MzSectionName.Outer;
            mzSectionsCtrl_O.HideRfControls = true;
            mzSectionsCtrl_O.SetUpperLimits(true, 4095, 1);         // Special case for non standard IMB DAC. No MOD DAC.
            mzSectionsCtrl_O.EnableSection = true;

            mzSectionsCtrl_L.SetTitle("Top");
            mzSectionsCtrl_L.SectionName = ModuleController.Worker.MzSectionName.Top;
            mzSectionsCtrl_L.EnableSection = true;

            mzSectionsCtrl_R.SetTitle("Bottom");
            mzSectionsCtrl_R.SectionName = ModuleController.Worker.MzSectionName.Bottom;
            mzSectionsCtrl_R.EnableSection = true;

            numBottomDataModCtrl.Maximum = 4095;
            numTopDataModCtrl.Maximum = numBottomDataModCtrl.Maximum;
            tkBottomDataModCtrl.Maximum = (int)numBottomDataModCtrl.Maximum;
            tkTopDataModCtrl.Maximum = (int)numBottomDataModCtrl.Maximum;
            tkBottomDataModCtrl.TickFrequency = 1 + (int)(tkBottomDataModCtrl.Maximum - tkBottomDataModCtrl.Minimum) / 16;
            tkTopDataModCtrl.TickFrequency = tkBottomDataModCtrl.TickFrequency;

            toolStripProgressBar1.Visible = false;
            toolStripStatusLabel1.Text = "Ready";
            toolStripStatusLabel2.Text = "";

            rx1PosDcOffsetV.Setup("Pos DC i/p offset", 4095, 0);
            rx2PosDcOffsetV.Setup("Pos DC i/p offset", 4095, 0);
            rx1NegDcOffsetV.Setup("Neg DC i/p offset", 4095, 0);
            rx2NegDcOffsetV.Setup("Neg DC i/p offset", 4095, 0);
            rx1PeakCtrlV.Setup("Peak Control V", 4095, 0);
            rx2PeakCtrlV.Setup("Peak Control V", 4095, 0);
            mainHeaterCtrl.Setup("Main Heater Ctrl", 4095, 0);
            arm1HeaterCtrl.Setup("Arm 1 Heater Ctrl", 4095, 0);
            arm2HeaterCtrl.Setup("Arm 2 Heater Ctrl", 4095, 0);
            tecCtrl.Setup("TEC Control", 4095, 0);
            edfaPowerControl.Setup("Edfa Power", 4095, 0);

            toolTip1.SetToolTip(edfaPowerControl, "EDFA Power control");

            # region Register Status text
          
            // Vendor
            ckVen0.Text = "EOL Warn (L)";
            ckVen1.Text = "EOL Fatal (L)";
            ckVen2.Text = "unused";
            ckVen3.Text = "unused";
            ckVen4.Text = "Laser Temp (L)";
            ckVen5.Text = "Mz Temp(L)";
            ckVen6.Text = "-5.2 Warn(L)";
            ckVen7.Text = "-5.2 Fatal(L)";
            ckVen8.Text = "EOL Warn";
            ckVen9.Text = "EOL Fatal";
            ckVen10.Text = "unused";
            ckVen11.Text = "unused";
            ckVen12.Text = "Laser Temp";
            ckVen13.Text = "MZ Temp";
            ckVen14.Text = "-5.2 Warn";
            ckVen15.Text = "-5.2 Fatal";

            // Control
            ckCtrlReg0.Text = "Freq crtl (L)";
            ckCtrlReg1.Text = "Pwr ctrl (L)";
            ckCtrlReg2.Text = "TopMZ (L)";
            ckCtrlReg3.Text = "BottomMZ (L)";
            ckCtrlReg4.Text = "PC MZ (L)";
            ckCtrlReg5.Text = "OuterMZ (L)";
            ckCtrlReg6.Text = "unused";
            ckCtrlReg7.Text = "unused";
            ckCtrlReg8.Text = "Freq ctrl";
            ckCtrlReg9.Text = "Pwr ctrl";
            ckCtrlReg10.Text = "TopMZ";
            ckCtrlReg11.Text = "BottomMZ";
            ckCtrlReg12.Text = "PC MZ";
            ckCtrlReg13.Text = "Outer MZ";
            ckCtrlReg14.Text = "unused";
            ckCtrlReg15.Text = "unused";

            // Control Status
            ckCtrlStatus0.Text = "Power ctrl";
            ckCtrlStatus1.Text = "Frequency";
            ckCtrlStatus2.Text = "Dither";
            ckCtrlStatus3.Text = "Top MZ";
            ckCtrlStatus4.Text = "Bottom MZ";
            ckCtrlStatus5.Text = "Carver MZ ";
            ckCtrlStatus6.Text = "Outer MZ";
            ckCtrlStatus7.Text = "unused";
            ckCtrlStatus8.Text = "Laser TEC enable";
            ckCtrlStatus9.Text = "Laser TEC ctrl";
            ckCtrlStatus10.Text = "MZ TEC enable";
            ckCtrlStatus11.Text = "MZ TEC ctrl";
            ckCtrlStatus12.Text = "unused";
            ckCtrlStatus13.Text = "unused";
            ckCtrlStatus14.Text = "unused";
            ckCtrlStatus15.Text = "Autolock";
#endregion

            Worker.Initialise();
            oifDebug = Worker.RegisterDebug;

            // Set up controls
            ckImmediateSend_CheckedChanged(null, null);
        }


        //void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        //{
        //    throw;
        //}

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("An engineering GUI to control a BlackBox module\n\n(c) Bookham 2008","About " + Application.ProductName);
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            ApplyLaserAndMzSettings();
        }

        private void ApplyLaserAndMzSettings()
        {
            Worker.setLaserAndMz(dsdbrSectionsCtrl1.Value, mzSectionsCtrl_L.Value, mzSectionsCtrl_R.Value, mzSectionsCtrl_O.Value, mzSectionsCtrl_PC.Value);
            dsdbrSectionsCtrl1.ResetColours();
            mzSectionsCtrl_L.ResetColours();
            mzSectionsCtrl_R.ResetColours();
            mzSectionsCtrl_O.ResetColours();
            mzSectionsCtrl_PC.ResetColours();
        }

        private void dacCalStartCal_Click(object sender, EventArgs e)
        {
            TestModuleLoader.Initialise();
            //ITestModule modRun = TestModuleLoader.LoadModule("Tsff_CalDAC_Test", "");
            
            //// Run a single module
            //DatumList configData = new DatumList();
            //DatumList previousTestData = new DatumList();
            //DatumList calData = new DatumList();
            //modRun.DoTest(null, ModulePrivilegeLevel.Technician, configData, InstrumentLoader.InstrumentList, InstrumentLoader.ChassisList, calData, previousTestData);


            // Run all modules
            LocalEngine engine = new LocalEngine();
            DUTObject dutObject = new DUTObject();
            dutObject.TestStage = "PcbCal";
            dutObject.PartCode = "ALL";

            CalDAC_Handler moduleHandler = new CalDAC_Handler();
            DatumList calData = moduleHandler.Run(engine, dutObject);
            this.plotList = calData;

            // Save raw data
            DatumFileLink calDacResults = moduleHandler.calDacSaveResults(engine, "results", calData);
            // Populate grid
            Worker.PopulateGridFromFile(calDacResults.Value, dacCalDataGrid);

            double[] xData = null;
            double[] yData = null;
            foreach (DatumPlot myPlot in calData)
            {
                PlotAxis dacValueAxis = myPlot.GetAxisHandle("DacValue");
                PlotAxis readingsAxis = myPlot.GetAxisHandle("Readings");

                xData = new double[dacValueAxis.Count];
                yData = new double[dacValueAxis.Count];

                for (int i = 0; i < (dacValueAxis.Count); i++)
                {
                    xData[i] = dacValueAxis[i];
                    yData[i] = readingsAxis[i];
                }
                LinePlot plotData = new LinePlot(xData, yData);
                plotSurface2D1.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                plotSurface2D1.XAxis1.Label = myPlot.Name;
                plotData.Color = Color.DarkBlue;
                plotData.Label = readingsAxis.Units;
                plotSurface2D1.Visible = true;
                break;
            }
        }

        private void absoluteMode_CheckedChanged(object sender, EventArgs e)
        {
            dsdbrSectionsCtrl1.SetUpperLimits(!absoluteMode.Checked);
            mzSectionsCtrl_L.SetUpperLimits(!absoluteMode.Checked);
            mzSectionsCtrl_R.SetUpperLimits(!absoluteMode.Checked);
            mzSectionsCtrl_O.SetUpperLimits(!absoluteMode.Checked);
            mzSectionsCtrl_PC.SetUpperLimits(!absoluteMode.Checked);
        }

        private void dacCalLoadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = "results";
            openFileDialog1.ShowDialog();
            Worker.PopulateGridFromFile(openFileDialog1.FileName, dacCalDataGrid);
        }

        private DatumList plotList;
        private string plotOnDisplay;

        private void btnNextPlot_Click(object sender, EventArgs e)
        {
            if (plotList == null)
                return;

            double[] xData = null;
            double[] yData = null;
            foreach (DatumPlot myPlot in plotList)
            {
                this.plotOnDisplay = myPlot.Name;

                PlotAxis dacValueAxis = myPlot.GetAxisHandle("DacValue");
                PlotAxis readingsAxis = myPlot.GetAxisHandle("Readings");

                xData = new double[dacValueAxis.Count];
                yData = new double[dacValueAxis.Count];

                for (int i = 0; i < (dacValueAxis.Count); i++)
                {
                    yData[i] = dacValueAxis[i];
                    xData[i] = readingsAxis[i];
                }
                LinePlot plotData = new LinePlot(xData, yData);
                plotSurface2D1.Clear();
                plotSurface2D1.Add(plotData, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Right, 1);
                plotSurface2D1.XAxis1.Label = myPlot.Name;
                plotData.Color = Color.DarkBlue;
                plotData.Label = readingsAxis.Units;
                plotSurface2D1.Visible = true;
                plotSurface2D1.Refresh();
                Application.DoEvents();
                System.Threading.Thread.Sleep(5000);
            }
        }

        private void btnSendPassword_Click(object sender, EventArgs e)
        {
            Worker.RegisterDebug.SetOifRegisterAsString(0xFE, txtPassword.Text);
        }

        private void ckImmediateSend_CheckedChanged(object sender, EventArgs e)
        {
            mzSectionsCtrl_L.SendImmediately = true;
            mzSectionsCtrl_R.SendImmediately = true;
            mzSectionsCtrl_PC.SendImmediately = true;
            mzSectionsCtrl_O.SendImmediately = true;
        }
        
        private void btnWriteByte_Click(object sender, EventArgs e)
        {
            short registerNo;
            ushort byteToSet;

            try
            {
                registerNo = Convert.ToInt16(txtRegisterNumber.Text, 16);
            }
            catch (ArgumentOutOfRangeException)
            {
                txtRegisterNumber.ForeColor = Color.Red;
                return;
            }

            try
            {
                byteToSet = Convert.ToUInt16(txtByteVal.Text, 16);
            }
            catch (ArgumentOutOfRangeException)
            {
                txtByteVal.ForeColor = Color.Red;
                return;
            }

            txtRegisterNumber.ForeColor = Color.Black;
            txtRegisterNumber.Text = txtRegisterNumber.Text.PadLeft(2, '0').ToUpper();
            txtByteVal.ForeColor = Color.Black;
            txtByteVal.Text = txtByteVal.Text.PadLeft(2, '0').ToUpper();

            oifDebug.SetOifRegisterAsInt(registerNo, byteToSet);
        }

        private void btnReadVal_Click(object sender, EventArgs e)
        {
            short registerNo;
            try
            {
                registerNo = Convert.ToInt16(txtRegisterNumber.Text, 16);
            }
            catch (ArgumentOutOfRangeException)
            {
                txtRegisterNumber.ForeColor = Color.Red;
                return;
            }
            txtRegisterNumber.ForeColor = Color.Black;
            txtRegisterNumber.Text = txtRegisterNumber.Text.PadLeft(2, '0').ToUpper();

            short val = oifDebug.GetOifRegisterAsInt(registerNo);
            txtReadByte.Text = val.ToString("x").PadLeft(2, '0').ToUpper(); ;
        }

         private void btnWriteStr_Click(object sender, EventArgs e)
        {
            short registerNo;
            try
            {
                registerNo = Convert.ToInt16(txtRegisterNumber.Text, 16);
            }
            catch (ArgumentOutOfRangeException)
            {
                txtRegisterNumber.ForeColor = Color.Red;
                return;
            }
            txtRegisterNumber.ForeColor = Color.Black;
            txtRegisterNumber.Text = txtRegisterNumber.Text.PadLeft(2, '0').ToUpper();
            oifDebug.SetOifRegisterAsString(registerNo, txtWriteStr.Text);
        }

        private void btnReadStr_Click(object sender, EventArgs e)
        {
            short registerNo;
            try
            {
                registerNo = Convert.ToInt16(txtRegisterNumber.Text, 16);
            }
            catch (ArgumentOutOfRangeException)
            {
                txtRegisterNumber.ForeColor = Color.Red;
                return;
            }
            txtRegisterNumber.ForeColor = Color.Black;
            txtRegisterNumber.Text = txtRegisterNumber.Text.PadLeft(2, '0').ToUpper();
            txtReadStr.Text = oifDebug.GetOifRegisterAsString(registerNo);
        }

        private delegate short DoSomething();
        private string CallMethod(DoSomething action)
        {
            action.Invoke();
            List<string> outgoingMessageList = oifDebug.ReadRawOutboundMessages();
            // Send outgoing messages to real DUT

            // Collect incoming messages from real DUT

            // Poke incoming messages into driver for processing
            List<string> messageList = new List<string>();
            messageList.Add("64 FE 00 03");
            oifDebug.SetRawInboundMessages(messageList);
            
            // Process incoming messages
            return action().ToString();
        }

        private void btnCheckLockStatus_Click(object sender, EventArgs e)
        {
            //txtUnlockStatus.Text = CallMethod(Worker.DqpskSetup.GetUnlockStatus);

            txtUnlockStatus.Text = Worker.DqpskSetup.GetUnlockStatus().ToString();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                DSDBRSectionCurrents laserCurrents = dsdbrSectionsCtrl1.Value;
                MZBiasSettings pulseCarverMz = mzSectionsCtrl_PC.Value;
                MZBiasSettings topMz = mzSectionsCtrl_L.Value;
                MZBiasSettings bottomMz = mzSectionsCtrl_R.Value;
                MZBiasSettings outerMz = mzSectionsCtrl_O.Value;

                Worker.getLaserAndMz(ref laserCurrents, ref pulseCarverMz, ref topMz, ref bottomMz, ref outerMz);
                mzSectionsCtrl_L.Value = topMz;
                mzSectionsCtrl_R.Value = bottomMz;
                mzSectionsCtrl_O.Value = outerMz;
                mzSectionsCtrl_PC.Value = pulseCarverMz;

                dsdbrSectionsCtrl1.Value = laserCurrents;
                channelSelect.Value = Worker.Itla.GetChannel();

                // Update monitors
                txtTxMon.Text = Worker.DsdbrLaser.GetLockerTxMon_ADC().ToString();
                txtRxMon.Text = Worker.DsdbrLaser.GetLockerRefMon_ADC().ToString();
                txtLaserMon.Text = Worker.DsdbrLaser.GetLaserPowerMon_ADC(0).ToString();
                txtLsPhaseSense.Text = Worker.DsdbrLaser.GetLsPhaseSense().ToString();
                
                dsdbrSectionsCtrl1.ResetColours();
                mzSectionsCtrl_L.ResetColours();
                mzSectionsCtrl_R.ResetColours();
                mzSectionsCtrl_O.ResetColours();
                mzSectionsCtrl_PC.ResetColours();
            }
            catch (TunableModuleException tunableException)
            {
                //toolStripStatusLabel2.Text = tunableException.Message;
            }
        }

        private void btnRecallChannel_Click(object sender, EventArgs e)
        {
            // Set to channel and recall settings
            Worker.Itla.SelectChannel((int)channelSelect.Value);
            btnRead_Click(null, null);
        }

        private void btnSelectChannel_Click(object sender, EventArgs e)
        {
            // Set Channel
            Worker.Itla.SelectChannel((int)channelSelect.Value);
        }

        private void btnSaveSetup_Click(object sender, EventArgs e)
        {
            //Worker.itla.SaveConfig();
            
            // Write channel number to register 0x89
            Worker.DsdbrLaser.SaveChannel((int)channelSelect.Value);
        }

        private void btnReadModuleIdentity_Click(object sender, EventArgs e)
        {
            txtDevTypeRO.Text = Worker.Itla.GetDeviceType();
            txtManufacturerRO.Text = Worker.Itla.GetManufacturer();
            txtModelRO.Text = Worker.Itla.GetModelNumber();
            txtMfgDateRO.Text = Worker.Itla.GetManufacturingDate();
            txtFwRelRO.Text = Worker.Itla.GetReleaseCode();
            txtFwBackRO.Text = Worker.Itla.GetReleaseBackwards();
        }

        private void btnReadUpperModuleIdRegisters_Click(object sender, EventArgs e)
        {
            txtModSerialWrite.Text = Worker.BlackBoxIdentity.GetUnitSerialNumber();

            tstLaserSerialWrite.Text = Worker.BlackBoxIdentity.GetLaserSerialNumber();

            txtPcbSerialWrite.Text = Worker.BlackBoxIdentity.GetBoardSerialNumber();

#warning Date not working ?
            txtBuildDateWrite.Text = Worker.RegisterDebug.GetOifRegisterAsString(0xF9);
            //txtBuildDateWrite.Text = Worker.BlackBoxIdentity.GetUnitBuildDate().ToString();
#warning Register F8 not working
            //txtFwVersionWrite.Text = Worker.RegisterDebug.GetOifRegisterAsString(0xF8);
        }

        private void btnWriteModuleIdentity_Click(object sender, EventArgs e)
        {
            Worker.BlackBoxIdentity.SetUnitSerialNumber(txtModSerialWrite.Text);
            Worker.BlackBoxIdentity.SetLaserSerialNumber(tstLaserSerialWrite.Text);
            Worker.BlackBoxIdentity.SetBoardSerialNumber(txtPcbSerialWrite.Text);
            DateTime newDate;
            if (DateTime.TryParse(txtBuildDateWrite.Text, out newDate))
            {
                Worker.BlackBoxIdentity.SetUnitBuildDate(newDate);
                txtBuildDateWrite.ForeColor = Color.Black;

            }
            else
            {
                txtBuildDateWrite.ForeColor = Color.Red;
            }

        }

        private void btnReadTcmzMonitors_Click(object sender, EventArgs e)
        {

#warning implement this in driver
            txtLaserTemp.Text = Worker.RegisterDebug.GetOifRegisterAsInt(0xA2).ToString();

#warning implement this in driver
            txtMzTemp.Text = Worker.RegisterDebug.GetOifRegisterAsInt(0xAA).ToString();
            
            // TODO - AEA monitor registers
            string ByteList = Worker.RegisterDebug.GetOifRegisterAsString(0xEE);
            // split string into characters
            char[] charList = ByteList.ToCharArray();
            int[] monitorValue = new int[charList.Length / 2];
            for (int i = 0; i < charList.Length; i += 2)
            {
                int upper = Convert.ToInt16(charList[i]);
                int lower = Convert.ToInt16(charList[i + 1]);
                monitorValue[i / 2] = (int)(upper * 0xff + lower);
            }
            //1.	Inline monitor
            //2.	Pulse Carver monitor
            //3.	Top MZ monitor
            //4.	Bottom MZ monitor
            //5.	Output RF monitor
            //6.	Pulse Carver RF monitor
            txtOutInlineDc.Text = monitorValue[0].ToString();
            txtCarverTap.Text = monitorValue[1].ToString();
            txtUmmiDc.Text = monitorValue[2].ToString();
            txtLmmiDc.Text = monitorValue[3].ToString();
            txtOutputRfDc.Text = monitorValue[4].ToString();
            txtCarverTapRf.Text = monitorValue[5].ToString();

#warning implement this in driver
            txt5v2Sense.Text = Worker.RegisterDebug.GetOifRegisterAsInt(0x9F).ToString();
#warning implement this in driver
            txtPcbTempSense.Text = Worker.RegisterDebug.GetOifRegisterAsInt(0x90).ToString();

            // Control Reg
            byte upperControlByte, lowerControlByte;
            Worker.RegisterDebug.GetRawControlRegister(out upperControlByte, out lowerControlByte);
            ckCtrlReg0.Checked = (upperControlByte & 0x0001) != 0;
            ckCtrlReg1.Checked = (upperControlByte & 0x0002) != 0;
            ckCtrlReg2.Checked = (upperControlByte & 0x0004) != 0;
            ckCtrlReg3.Checked = (upperControlByte & 0x0008) != 0;
            ckCtrlReg4.Checked = (upperControlByte & 0x0010) != 0;
            ckCtrlReg5.Checked = (upperControlByte & 0x0020) != 0;
            ckCtrlReg6.Checked = (upperControlByte & 0x0040) != 0;
            ckCtrlReg7.Checked = (upperControlByte & 0x0080) != 0;
            ckCtrlReg8.Checked = (upperControlByte & 0x0100) != 0;
            ckCtrlReg9.Checked = (upperControlByte & 0x0200) != 0;
            ckCtrlReg10.Checked = (upperControlByte & 0x0400) != 0;
            ckCtrlReg11.Checked = (upperControlByte & 0x0800) != 0;
            ckCtrlReg12.Checked = (upperControlByte & 0x1000) != 0;
            ckCtrlReg13.Checked = (upperControlByte & 0x2000) != 0;
            ckCtrlReg14.Checked = (upperControlByte & 0x4000) != 0;
            ckCtrlReg15.Checked = (upperControlByte & 0x8000) != 0;

            int vendorRegister = Worker.RegisterDebug.GetOifRegisterAsUint(0x80);
            ckVen0.Checked = (vendorRegister & 0x0001) != 0;
            ckVen1.Checked = (vendorRegister & 0x0002) != 0;
            ckVen2.Checked = (vendorRegister & 0x0004) != 0;
            ckVen3.Checked = (vendorRegister & 0x0008) != 0;
            ckVen4.Checked = (vendorRegister & 0x0010) != 0;
            ckVen5.Checked = (vendorRegister & 0x0020) != 0;
            ckVen6.Checked = (vendorRegister & 0x0040) != 0;
            ckVen7.Checked = (vendorRegister & 0x0080) != 0;
            ckVen8.Checked = (vendorRegister & 0x0100) != 0;
            ckVen9.Checked = (vendorRegister & 0x0200) != 0;
            ckVen10.Checked = (vendorRegister & 0x0400) != 0;
            ckVen11.Checked = (vendorRegister & 0x0800) != 0;
            ckVen12.Checked = (vendorRegister & 0x1000) != 0;
            ckVen13.Checked = (vendorRegister & 0x2000) != 0;
            ckVen14.Checked = (vendorRegister & 0x4000) != 0;
            ckVen15.Checked = (vendorRegister & 0x8000) != 0;

            int ctrlStatusReg = Worker.RegisterDebug.GetOifRegisterAsUint(0x81);
            ckCtrlStatus0.Checked = (ctrlStatusReg & 0x0001) != 0;
            ckCtrlStatus1.Checked = (ctrlStatusReg & 0x0002) != 0;
            ckCtrlStatus2.Checked = (ctrlStatusReg & 0x0004) != 0;
            ckCtrlStatus3.Checked = (ctrlStatusReg & 0x0008) != 0;
            ckCtrlStatus4.Checked = (ctrlStatusReg & 0x0010) != 0;
            ckCtrlStatus5.Checked = (ctrlStatusReg & 0x0020) != 0;
            ckCtrlStatus6.Checked = (ctrlStatusReg & 0x0040) != 0;
            ckCtrlStatus7.Checked = (ctrlStatusReg & 0x0080) != 0;
            ckCtrlStatus8.Checked = (ctrlStatusReg & 0x0100) != 0;
            ckCtrlStatus9.Checked = (ctrlStatusReg & 0x0200) != 0;
            ckCtrlStatus10.Checked = (ctrlStatusReg & 0x0400) != 0;
            ckCtrlStatus11.Checked = (ctrlStatusReg & 0x0800) != 0;
            ckCtrlStatus12.Checked = (ctrlStatusReg & 0x1000) != 0;
            ckCtrlStatus13.Checked = (ctrlStatusReg & 0x2000) != 0;
            ckCtrlStatus14.Checked = (ctrlStatusReg & 0x4000) != 0;
            ckCtrlStatus15.Checked = (ctrlStatusReg & 0x8000) != 0;
        }


#region MZ Page
        // Added by TF 

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DSDBRSectionCurrents laserCurrents = dsdbrSectionsCtrl1.Value;
            MZBiasSettings pulseCarverMz = mzSectionsCtrl_PC.Value;
            MZBiasSettings topMz = mzSectionsCtrl_L.Value;
            MZBiasSettings bottomMz = mzSectionsCtrl_R.Value;
            MZBiasSettings outerMz = mzSectionsCtrl_O.Value;
            
            Worker.getLaserAndMz(ref laserCurrents, ref pulseCarverMz, ref topMz, ref bottomMz, ref outerMz);
            mzSectionsCtrl_L.Value = topMz;
            mzSectionsCtrl_R.Value = bottomMz;
            mzSectionsCtrl_O.Value = outerMz;
            mzSectionsCtrl_PC.Value = pulseCarverMz;

            updateMzMonitors();
        }

        private void mzTabTimer_Tick(object sender, EventArgs e)
        {
            mzTabTimer.Enabled = false;
            updateMzMonitors();
            if (cbFastUpdate.Checked)
            {
                double rxSum = 0;
                rxData.RemoveAt(0);
                rxData.Add(Convert.ToDouble(txtOutputRfDc.Text));
                
                for (int i = 0; i < 10; i++)
                {
                    rxSum += rxData[i];
                }
                // Round to nearest digit.
                txtOutputRfDc.Text = Convert.ToString((int)((rxSum + 5) / 10));
            }
            mzTabTimer.Enabled = true;
        }

        private void updateMzMonitors()
        {
            // AEA monitor registers
            byte[] byteList = new byte[12];
            int[] monitorValue = new int[6];
            string status = Worker.RegisterDebug.GetOifRegisterAsByteArray(0xEE, ref byteList);
            for (int i = 0; i < byteList.Length-2; i += 2)
            {
                int upper = Convert.ToInt16(byteList[i]);
                int lower = Convert.ToInt16(byteList[i + 1]);
                monitorValue[i / 2] = (int)(upper * 0xff + lower);
            }
            //1.	Inline monitor
            //2.	Pulse Carver monitor
            //3.	Top MZ monitor
            //4.	Bottom MZ monitor
            //5.	Output RF monitor
            //6.	Pulse Carver RF monitor
            txtOutInlineDc.Text = monitorValue[0].ToString();
            txtCarverTap.Text = monitorValue[1].ToString();
            txtUmmiDc.Text = monitorValue[2].ToString();
            txtLmmiDc.Text = monitorValue[3].ToString();
            txtOutputRfDc.Text = monitorValue[4].ToString();
            txtCarverTapRf.Text = monitorValue[5].ToString();          
            
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Message box doesn't seem to scroll if it's not displayed
            if (tabControl.SelectedTab.Text == "Debug")
            {
                this.messageLog.SelectionStart = this.messageLog.TextLength;
                this.messageLog.ScrollToCaret();
            }
        }

        private void checkAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            mzTabTimer.Enabled = checkAutoUpdate.Checked;
        }

        private void numTopDataModCtrl_ValueChanged(object sender, EventArgs e)
        {
            tkTopDataModCtrl.Value = (int)numTopDataModCtrl.Value;
            Worker.TestBoard.SetTopMzModV(Convert.ToUInt16(numTopDataModCtrl.Value));
        }

        private void numBottomDataModCtrl_ValueChanged(object sender, EventArgs e)
        {
            tkBottomDataModCtrl.Value = (int)numBottomDataModCtrl.Value;
            Worker.TestBoard.SetBottomMzModV(Convert.ToUInt16(numBottomDataModCtrl.Value));
        }

        private void tkTopDataModCtrl_Scroll(object sender, EventArgs e)
        {
            numTopDataModCtrl.Value = tkTopDataModCtrl.Value;
        }

        private void tkBottomDataModCtrl_Scroll(object sender, EventArgs e)
        {
            numBottomDataModCtrl.Value = tkBottomDataModCtrl.Value;
        }

        private void dacMode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void laserTabTimer_Tick(object sender, EventArgs e)
        {
            laserTabTimer.Enabled = false;
            
            btnRead_Click(null, null);

            laserTabTimer.Enabled = true;
        }

        private void ckLaserAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            laserTabTimer.Enabled = ckLaserAutoUpdate.Checked;
        }

  

        private void setRx1NegDcOffsetV(decimal newValue)
        {
            Worker.TestBoard.SetRx1NegDcOffset(Convert.ToUInt16(newValue));
        }

        private void setRx1PosDcOffsetV(decimal newValue)
        {
            Worker.TestBoard.SetRx1PosDcOffset(Convert.ToUInt16(newValue));
        }

        private void setRx1PeakCtrlV(decimal newValue)
        {
            Worker.TestBoard.SetRx1PeakCtrl(Convert.ToUInt16(newValue));
        }

        private void setRx2PosDcOffsetV(decimal newValue)
        {
            Worker.TestBoard.SetRx2PosDcOffset(Convert.ToUInt16(newValue));
        }

        private void setRx2NegDcOffsetV(decimal newValue)
        {
            Worker.TestBoard.SetRx2NegDcOffset(Convert.ToUInt16(newValue));
        }

        private void setRx2PeakCtrlV(decimal newValue)
        {
            Worker.TestBoard.SetRx2PeakCtrl(Convert.ToUInt16(newValue));
        }

        private void setMainHeaterCtrl(decimal newValue)
        {
            Worker.TestBoard.SetMainHeater(Convert.ToUInt16(newValue));
        }

        private void setArm1HeaterCtrl(decimal newValue)
        {
            Worker.TestBoard.SetRx1Heater(Convert.ToUInt16(newValue));
        }

        private void setArm2HeaterCtrl(decimal newValue)
        {
            Worker.TestBoard.SetRx2Heater(Convert.ToUInt16(newValue));
        }

        private void setTecCtrl(decimal newValue)
        {
            Worker.TestBoard.SetRxTEC(Convert.ToUInt16(newValue));
        }

        private void debugLogTimer_Tick(object sender, EventArgs e)
        {
            int startPos = this.messageLog.TextLength;
            this.messageLog.Text += oifDebug.GetRawMessageTrace();
            int newPos = this.messageLog.TextLength;
            if (newPos != startPos)
            {
                // Scroll down to the last line
                this.messageLog.SelectionStart = newPos;
                this.messageLog.ScrollToCaret();
            }
        }


        private void btnClearLog_Click(object sender, EventArgs e)
        {
            messageLog.Clear();
        }

        private void ckLoggingEnabled_CheckedChanged(object sender, EventArgs e)
        {
            oifDebug.DebugLogEnabled = ckLoggingEnabled.Checked;
            debugLogTimer.Enabled = ckLoggingEnabled.Checked;
        }

        private void ckVen_MouseUp(object sender, MouseEventArgs e)
        {
            int mask = 0;
            mask |= ckVen0.Checked ? 1 : 0;
            mask |= ckVen1.Checked ? 1 << 1 : 0;
            mask |= ckVen2.Checked ? 1 << 2 : 0;
            mask |= ckVen3.Checked ? 1 << 3 : 0;
            mask |= ckVen4.Checked ? 1 << 4 : 0;
            mask |= ckVen5.Checked ? 1 << 5 : 0;
            mask |= ckVen6.Checked ? 1 << 6 : 0;
            mask |= ckVen7.Checked ? 1 << 7 : 0;
            mask |= ckVen8.Checked ? 1 << 8 : 0;
            mask |= ckVen9.Checked ? 1 << 9 : 0;
            mask |= ckVen10.Checked ? 1 << 10 : 0;
            mask |= ckVen11.Checked ? 1 << 11 : 0;
            mask |= ckVen12.Checked ? 1 << 12 : 0;
            mask |= ckVen13.Checked ? 1 << 13 : 0;
            mask |= ckVen14.Checked ? 1 << 14 : 0;
            mask |= ckVen15.Checked ? 1 << 15 : 0;

            Worker.RegisterDebug.SetOifRegisterAsUInt(0x80, (ushort)mask);
        }

        private void ckCtrlStatus_MouseUp(object sender, MouseEventArgs e)
        {
            int mask = 0;
            mask |= ckCtrlStatus0.Checked ? 1 : 0;
            mask |= ckCtrlStatus1.Checked ? 1 << 1 : 0;
            mask |= ckCtrlStatus2.Checked ? 1 << 2 : 0;
            mask |= ckCtrlStatus3.Checked ? 1 << 3 : 0;
            mask |= ckCtrlStatus4.Checked ? 1 << 4 : 0;
            mask |= ckCtrlStatus5.Checked ? 1 << 5 : 0;
            mask |= ckCtrlStatus6.Checked ? 1 << 6 : 0;
            mask |= ckCtrlStatus7.Checked ? 1 << 7 : 0;
            mask |= ckCtrlStatus8.Checked ? 1 << 8 : 0;
            mask |= ckCtrlStatus9.Checked ? 1 << 9 : 0;
            mask |= ckCtrlStatus10.Checked ? 1 << 10 : 0;
            mask |= ckCtrlStatus11.Checked ? 1 << 11 : 0;
            mask |= ckCtrlStatus12.Checked ? 1 << 12 : 0;
            mask |= ckCtrlStatus13.Checked ? 1 << 13 : 0;
            mask |= ckCtrlStatus14.Checked ? 1 << 14 : 0;
            mask |= ckCtrlStatus15.Checked ? 1 << 15 : 0;

            Worker.RegisterDebug.SetOifRegisterAsUInt(0x81, (ushort)mask);
        }

        private void ckCtrlReg_MouseUp(object sender, MouseEventArgs e)
        {
            int mask = 0;
            mask |= ckCtrlReg0.Checked ? 1 : 0;
            mask |= ckCtrlReg1.Checked ? 1 << 1 : 0;
            mask |= ckCtrlReg2.Checked ? 1 << 2 : 0;
            mask |= ckCtrlReg3.Checked ? 1 << 3 : 0;
            mask |= ckCtrlReg4.Checked ? 1 << 4 : 0;
            mask |= ckCtrlReg5.Checked ? 1 << 5 : 0;
            mask |= ckCtrlReg6.Checked ? 1 << 6 : 0;
            mask |= ckCtrlReg7.Checked ? 1 << 7 : 0;
            mask |= ckCtrlReg8.Checked ? 1 << 8 : 0;
            mask |= ckCtrlReg9.Checked ? 1 << 9 : 0;
            mask |= ckCtrlReg10.Checked ? 1 << 10 : 0;
            mask |= ckCtrlReg11.Checked ? 1 << 11 : 0;
            mask |= ckCtrlReg12.Checked ? 1 << 12 : 0;
            mask |= ckCtrlReg13.Checked ? 1 << 13 : 0;
            mask |= ckCtrlReg14.Checked ? 1 << 14 : 0;
            mask |= ckCtrlReg15.Checked ? 1 << 15 : 0;

            Worker.RegisterDebug.SetOifRegisterAsUInt(0x82, (ushort)mask);
        }

        private void ReadRx_Click(object sender, EventArgs e)
        {
            UpdateRxTabReadings();
        }

        private void UpdateRxTabReadings()
        {
            demodulatorThermistor.Text = Worker.TestBoard.GetRxThermistor().ToString();

            Rx1PosMon_I.Text = Worker.TestBoard.GetRx1PosPhotocurrent().ToString();
            Rx1PosVout.Text = Worker.TestBoard.GetRx1PositiveOutputV().ToString();
            Rx1NegMon_I.Text = Worker.TestBoard.GetRx1NegPhotocurrent().ToString();
            Rx1NegVout.Text = Worker.TestBoard.GetRx1NegativeOutputV().ToString();

            Rx2PosMon_I.Text = Worker.TestBoard.GetRx2PosPhotocurrent().ToString();
            Rx2PosVout.Text = Worker.TestBoard.GetRx2PositiveOutputV().ToString();
            Rx2NegMon_I.Text = Worker.TestBoard.GetRx2NegPhotocurrent().ToString();
            Rx2NegVout.Text = Worker.TestBoard.GetRx2NegativeOutputV().ToString();

            evalPcbTemp.Text = Worker.TestBoard.GetTestPCBTemperature().ToString();
            evalEdfaPower.Text = Worker.TestBoard.GetTestBoardEDFAPower().ToString();

            evalPcbStm32Temp.Text = Worker.TestBoard.GetTestBoardStm32Temperature().ToString();
            evalPcbVref.Text = Worker.TestBoard.GetTestBoardVref().ToString();

            UpdateDioGui();
        }

        private void UpdateDioGui()
        {
            byte upper = 0, lower = 0;
            Worker.TestBoard.GetTestBoardDigiOutRegister(ref upper, ref lower);
            evalDigiOut0.Checked = (lower & 0x0001) != 0;
            evalDigiOut1.Checked = (lower & 0x0002) != 0;
            evalDigiOut2.Checked = (lower & 0x0004) != 0;
            evalDigiOut3.Checked = (lower & 0x0008) != 0;
        }

        private void rxTabTimerTick(object sender, EventArgs e)
        {
            UpdateRxTabReadings();
        }

        private void RxTimerEnabled_CheckedChanged(object sender, EventArgs e)
        {
            rxTabTimer.Enabled = RxTimerEnabled.Checked;
        }


        private void evalDigiOut_MouseUp(object sender, MouseEventArgs e)
        {
            int mask = 0;
            mask |= evalDigiOut0.Checked ? 1 : 0;
            mask |= evalDigiOut1.Checked ? 1 << 1 : 0;
            mask |= evalDigiOut2.Checked ? 1 << 2 : 0;
            mask |= evalDigiOut3.Checked ? 1 << 3 : 0;

            Worker.TestBoard.SetTestBoardDigiOutRegister(0, Convert.ToByte(mask));
        }

        private IOifRegisterDebug oifDebug;

        private void setupDebug(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                oifDebug = Worker.RegisterDebug;
                Worker.EvalBoardRegisterDebug.DebugLogEnabled = false;
            }
            else
            {
                oifDebug = Worker.EvalBoardRegisterDebug;
                Worker.RegisterDebug.DebugLogEnabled = false;
            }
            messageLog.Text = "";
            oifDebug.DebugLogEnabled = ckLoggingEnabled.Checked;
        }

        private void edfaPowerControl_ValueChanged(decimal newValue)
        {
            Worker.TestBoard.SetEdfaPower(Convert.ToUInt16(newValue));
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void loadSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Disable timers to stop logging. App directory is changed by file dialog, and so relative paths to logs become invalid.
            laserTabTimer.Enabled = false;
            mzTabTimer.Enabled = false;
            rxTabTimer.Enabled = false;

            setupFileDialog.RestoreDirectory = true;    // Without this the application's working directory will be set to wherever the user selected. This is very bad !
            setupFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            setupFileDialog.Title = "Please select setup data to load";
            setupFileDialog.CheckFileExists = true;
            setupFileDialog.CheckPathExists = true;
            setupFileDialog.DefaultExt = "sav";
            setupFileDialog.Filter = "Saved setup (*.sav)|*.sav|All files (*.*)|*.*";
            setupFileDialog.Multiselect = false;
            setupFileDialog.ShowDialog(this);
            if (setupFileDialog.FileName.Length == 0)
                return;
    
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream reader = null; ;
            
            try
            {
                reader = System.IO.File.Open(setupFileDialog.FileName, System.IO.FileMode.Open);
                SavedSettings savedSettings = (SavedSettings)bf.Deserialize(reader);
                mzSectionsCtrl_R.Value = savedSettings.bottomMz;
                mzSectionsCtrl_L.Value = savedSettings.topMz;
                mzSectionsCtrl_O.Value = savedSettings.outerMz;
                mzSectionsCtrl_PC.Value = savedSettings.pulseCarverMz;
                dsdbrSectionsCtrl1.Value = savedSettings.dsdbrSetup;
                // Eval board
                numBottomDataModCtrl.Value = savedSettings.evalBoardSetup.bottomModCtrlData;
                numTopDataModCtrl.Value = savedSettings.evalBoardSetup.topModCtrlData;
                Worker.TestBoard.SetTestBoardDigiOutRegister(0,savedSettings.evalBoardSetup.dioState);
                UpdateDioGui();
                edfaPowerControl.Value = savedSettings.evalBoardSetup.edfaPower;
                mainHeaterCtrl.Value = savedSettings.evalBoardSetup.mainHeaterCtrl;
                arm1HeaterCtrl.Value = savedSettings.evalBoardSetup.rx1HeaterCtrl;
                arm2HeaterCtrl.Value = savedSettings.evalBoardSetup.rx2HeaterCtrl;
                tecCtrl.Value = savedSettings.evalBoardSetup.tecControl;
                rx1NegDcOffsetV.Value = savedSettings.evalBoardSetup.rx1NegDcOffset;
                rx2NegDcOffsetV.Value = savedSettings.evalBoardSetup.rx2NegDcOffset;
                rx1PosDcOffsetV.Value = savedSettings.evalBoardSetup.rx1PosDcOffset;
                rx2PosDcOffsetV.Value = savedSettings.evalBoardSetup.rx2PosDcOffset;
                rx1PeakCtrlV.Value = savedSettings.evalBoardSetup.rx1PeakControl;
                rx2PeakCtrlV.Value = savedSettings.evalBoardSetup.rx2PeakControl;
            }
            catch ( System.Exception )
            {
            }
            finally
            {
                if ( reader != null )
                    reader.Close();
            }
            ApplyLaserAndMzSettings();
        }

        private void saveSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Disable timers to stop logging. App directory is changed by file dialog, and so relative paths to logs become invalid.
            laserTabTimer.Enabled = false;
            mzTabTimer.Enabled = false;
            rxTabTimer.Enabled = false;

            setupFileDialog.RestoreDirectory = true;    // Without this the application's working directory will be set to wherever the user selected. This is very bad !
            setupFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            setupFileDialog.Title = "Please select the save file";
            setupFileDialog.CheckFileExists = false;
            setupFileDialog.CheckPathExists = true;
            setupFileDialog.DefaultExt = "sav";
            setupFileDialog.Filter = "Saved setup (*.sav)|*.sav|All files (*.*)|*.*";
            setupFileDialog.Multiselect = false;
            setupFileDialog.ShowDialog(this);
            if (setupFileDialog.FileName.Length == 0)
                return;

            EvalBoardSettings evalBoardSetup = new EvalBoardSettings();
            evalBoardSetup.bottomModCtrlData = numBottomDataModCtrl.Value;
            evalBoardSetup.topModCtrlData = numTopDataModCtrl.Value;
            byte upperByte = 0, lowerByte = 0;
            Worker.TestBoard.GetTestBoardDigiOutRegister(ref upperByte, ref lowerByte);
            evalBoardSetup.dioState = lowerByte;
            evalBoardSetup.edfaPower = edfaPowerControl.Value;
            evalBoardSetup.mainHeaterCtrl = mainHeaterCtrl.Value;
            evalBoardSetup.rx1HeaterCtrl = arm1HeaterCtrl.Value;
            evalBoardSetup.rx2HeaterCtrl = arm2HeaterCtrl.Value;
            evalBoardSetup.tecControl = tecCtrl.Value;
            evalBoardSetup.rx1NegDcOffset = rx1NegDcOffsetV.Value;
            evalBoardSetup.rx2NegDcOffset = rx2NegDcOffsetV.Value;
            evalBoardSetup.rx1PosDcOffset = rx1PosDcOffsetV.Value;
            evalBoardSetup.rx2PosDcOffset = rx2PosDcOffsetV.Value;
            evalBoardSetup.rx1PeakControl = rx1PeakCtrlV.Value;
            evalBoardSetup.rx2PeakControl = rx2PeakCtrlV.Value;

            SavedSettings savedSettings = new SavedSettings(mzSectionsCtrl_L.Value, mzSectionsCtrl_R.Value, mzSectionsCtrl_PC.Value, mzSectionsCtrl_O.Value, dsdbrSectionsCtrl1.Value, evalBoardSetup);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream writer = null;

            try
            {
                writer = System.IO.File.Open(setupFileDialog.FileName, System.IO.FileMode.Create);
                bf.Serialize(writer, savedSettings);
            }
            catch (System.Exception)
            {
            }
            finally
            {
                if ( writer != null )
                    writer.Close();
            }
        }

        private void dsdbrSectionsCtrl1_Load(object sender, EventArgs e)
        {

        }

        private void messageLog_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void evalDigiOut0_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void mzSectionsCtrl_R_Load(object sender, EventArgs e)
        {

        }

        private void cbFastUpdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFastUpdate.Checked == true)
            {
                mzTabTimer.Interval = 100;
                rxData = new List<double>(10);
                for (int i = 0; i < 10; i++)
                {
                    rxData.Add(Convert.ToDouble(txtOutputRfDc.Text));
                }
            }
            else
            {
                mzTabTimer.Interval = 1000;
            }       
        }

        private List<double> rxData;

        private void tabPage6_Click(object sender, EventArgs e)
        {

        }
    }

#endregion
}

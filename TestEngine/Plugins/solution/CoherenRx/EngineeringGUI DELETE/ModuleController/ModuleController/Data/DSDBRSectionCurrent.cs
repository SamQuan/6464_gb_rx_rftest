using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleController.Data
{
    /// <summary>
    /// DSDBR Section Current Status
    /// </summary>
    public struct DSDBRSectionCurrent
    {
        /// <summary>
        /// Convenience constructor
        /// </summary>
        /// <param name="enabled">Current source enabled</param>
        /// <param name="current_mA">Current</param>
        public DSDBRSectionCurrent(bool enabled, double current_mA)
        {
            this.Enabled = enabled;
            this.Current_mA = current_mA;
            this.ValueChanged = true;
        }

        /// <summary>
        /// Section Current Enabled
        /// </summary>
        public bool Enabled;

        /// <summary>
        /// Section current in mA
        /// </summary>
        public double Current_mA;

        /// <summary>
        /// True if data has been updated since last write.
        /// </summary>
        public bool ValueChanged;

        /// <summary>
        /// Override of ToString() - aids Debug
        /// </summary>
        /// <returns>String representation</returns>
        public override string ToString()
        {
            if (!Enabled) return "Disabled";
            else
            {
                string str = string.Format("{1} mA", Current_mA);
                return str;
            }
        }
    }
}

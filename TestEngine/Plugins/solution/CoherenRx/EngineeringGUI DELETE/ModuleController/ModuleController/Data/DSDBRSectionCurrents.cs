using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleController.Data
{
    /// <summary>
    /// DSDBR Section currents
    /// </summary>
    public struct DSDBRSectionCurrents
    {
        /// <summary>Gain section</summary>
        public DSDBRSectionCurrent Gain;
        /// <summary>SOA section</summary>
        public DSDBRSectionCurrent SOA;
        /// <summary>Rear section</summary>
        public DSDBRSectionCurrent Rear;
        /// <summary>Phase section</summary>
        public DSDBRSectionCurrent Phase;
        /// <summary>Front1 section</summary>
        public DSDBRSectionCurrent FSPair;
        /// <summary>Front2 section</summary>
        public DSDBRSectionCurrent FsConstant;
        /// <summary>Front3 section</summary>
        public DSDBRSectionCurrent FsNonConstant;
    }
}

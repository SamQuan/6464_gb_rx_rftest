using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleController.Data
{
    public class EvalBoardSettings
    {
        public decimal topModCtrlData;
        public decimal bottomModCtrlData;
        public decimal rx1PosDcOffset;
        public decimal rx1NegDcOffset;
        public decimal rx1PeakControl;
        public decimal rx2PosDcOffset;
        public decimal rx2NegDcOffset;
        public decimal rx2PeakControl;
        public decimal mainHeaterCtrl;
        public decimal rx1HeaterCtrl;
        public decimal rx2HeaterCtrl;
        public decimal tecControl;
        public decimal edfaPower;
        public byte dioState;
    }
}

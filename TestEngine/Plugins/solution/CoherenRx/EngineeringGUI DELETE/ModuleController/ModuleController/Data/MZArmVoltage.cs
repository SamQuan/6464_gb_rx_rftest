using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleController.Data
{
    public struct MZArmVoltage
    {
        /// <summary>
        /// Convenience constructor
        /// </summary>
        /// <param name="enabled">Current source enabled</param>
        /// <param name="current_mA">Current</param>
        public MZArmVoltage(bool enabled, double voltage_V)
        {
            this.Enabled = enabled;
            this.Voltage_V = voltage_V;
            this.ValueChanged = true;
        }

        /// <summary>
        /// Arm Current Enabled
        /// </summary>
        public bool Enabled;

        /// <summary>
        /// Arm current in Voltage_V
        /// </summary>
        public double Voltage_V;

        /// <summary>
        /// True if data has been updated since last write.
        /// </summary>
        public bool ValueChanged;

        /// <summary>
        /// Override of ToString() - aids Debug
        /// </summary>
        /// <returns>String representation</returns>
        public override string ToString()
        {
            if (!Enabled) return "Disabled";
            else
            {
                string str = string.Format("{1} V", Voltage_V);
                return str;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleController.Data
{
    public class MZBiasSettings
    {
        public MZArmCurrent leftPhase;
        public MZArmCurrent rightPhase;
        public MZArmVoltage leftModulation;
        public MZArmVoltage rightModulation;
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ModuleController.Data
{
    [Serializable()]
    public class SavedSettings : ISerializable
    {
        public SavedSettings()
        {
        }

        public SavedSettings(MZBiasSettings topMz, MZBiasSettings bottomMz, MZBiasSettings pulseCarverMz, MZBiasSettings outerMz, DSDBRSectionCurrents dsdbrSetup, EvalBoardSettings evalBoardSetup)
        {
            this.topMz = topMz;
            this.bottomMz = bottomMz;
            this.outerMz = outerMz;
            this.pulseCarverMz = pulseCarverMz;
            this.dsdbrSetup = dsdbrSetup;
            this.evalBoardSetup = evalBoardSetup;
        }

        public SavedSettings(SerializationInfo info, StreamingContext context)
        {
            this.topMz = new MZBiasSettings();
            MarkAllMzValuesChanged(this.topMz);
            this.topMz.leftModulation.Voltage_V = info.GetDouble("TopMzLeftData");
            this.topMz.rightModulation.Voltage_V = info.GetDouble("TopMzRightData");
            this.topMz.leftPhase.Current_mA = info.GetDouble("TopMzLeftPhase");
            this.topMz.rightPhase.Current_mA = info.GetDouble("TopMzRightPhase");
            this.topMz.leftModulation.Enabled = true;
            this.topMz.rightModulation.Enabled = true;
            this.topMz.leftPhase.Enabled = true;
            this.topMz.rightPhase.Enabled = true;
            //
            this.bottomMz = new MZBiasSettings();
            MarkAllMzValuesChanged(this.bottomMz);
            this.bottomMz.leftModulation.Voltage_V = info.GetDouble("BottomMzLeftData");
            this.bottomMz.rightModulation.Voltage_V = info.GetDouble("BottomMzRightData");
            this.bottomMz.leftPhase.Current_mA = info.GetDouble("BottomMzLeftPhase");
            this.bottomMz.rightPhase.Current_mA = info.GetDouble("BottomMzRightPhase");
            this.bottomMz.leftModulation.Enabled = true;
            this.bottomMz.rightModulation.Enabled = true;
            this.bottomMz.leftPhase.Enabled = true;
            this.bottomMz.rightPhase.Enabled = true;
            //
            this.outerMz = new MZBiasSettings();
            MarkAllMzValuesChanged(this.outerMz);
            this.outerMz.leftModulation.Voltage_V = info.GetDouble("OuterMzLeftData");
            this.outerMz.rightModulation.Voltage_V = info.GetDouble("OuterMzRightData");
            this.outerMz.leftPhase.Current_mA = info.GetDouble("OuterMzLeftPhase");
            this.outerMz.rightPhase.Current_mA = info.GetDouble("OuterMzRightPhase");
            this.outerMz.leftPhase.Enabled = true;
            this.outerMz.rightPhase.Enabled = true;
            //            
            this.pulseCarverMz = new MZBiasSettings();
            MarkAllMzValuesChanged(this.pulseCarverMz);
            this.pulseCarverMz.leftModulation.Voltage_V = info.GetDouble("PCMzLeftData");
            this.pulseCarverMz.rightModulation.Voltage_V = info.GetDouble("PCMzRightData");
            this.pulseCarverMz.leftPhase.Current_mA = info.GetDouble("PCMzLeftPhase");
            this.pulseCarverMz.rightPhase.Current_mA = info.GetDouble("PCMzRightPhase");
            this.pulseCarverMz.leftModulation.Enabled = true;
            this.pulseCarverMz.rightModulation.Enabled = true;
            this.pulseCarverMz.leftPhase.Enabled = true;
            this.pulseCarverMz.rightPhase.Enabled = true;
            //
            this.dsdbrSetup = new DSDBRSectionCurrents();
            MarkAllLaserValuesChanged(this.dsdbrSetup);
            this.dsdbrSetup.Gain.Current_mA = info.GetDouble("DsdbrGain");
            this.dsdbrSetup.Rear.Current_mA = info.GetDouble("DsdbrRear");
            this.dsdbrSetup.Phase.Current_mA = info.GetDouble("DsdbrPhase");
            this.dsdbrSetup.SOA.Current_mA = info.GetDouble("DsdbrSoa");
            this.dsdbrSetup.FSPair.Current_mA = info.GetDouble("DsdbrFS1");
            this.dsdbrSetup.FsConstant.Current_mA = info.GetDouble("DsdbrFS2");
            this.dsdbrSetup.FsNonConstant.Current_mA = info.GetDouble("DsdbrFS3");
            //
            this.evalBoardSetup = new EvalBoardSettings();
            this.evalBoardSetup.topModCtrlData = info.GetDecimal("topModCtrlData");
            this.evalBoardSetup.bottomModCtrlData = info.GetDecimal("bottomModCtrlData");
            this.evalBoardSetup.rx1PosDcOffset = info.GetDecimal("rx1PosDcOffset");
            this.evalBoardSetup.rx1NegDcOffset = info.GetDecimal("rx1NegDcOffset");
            this.evalBoardSetup.rx1PeakControl = info.GetDecimal("rx1PeakControl");
            this.evalBoardSetup.rx2PosDcOffset = info.GetDecimal("rx2PosDcOffset");
            this.evalBoardSetup.rx2NegDcOffset = info.GetDecimal("rx2NegDcOffset");
            this.evalBoardSetup.rx2PeakControl = info.GetDecimal("rx2PeakControl");
            this.evalBoardSetup.mainHeaterCtrl = info.GetDecimal("mainHeaterCtrl");
            this.evalBoardSetup.rx1HeaterCtrl = info.GetDecimal("rx1HeaterCtrl");
            this.evalBoardSetup.rx2HeaterCtrl = info.GetDecimal("rx2HeaterCtrl");
            this.evalBoardSetup.tecControl = info.GetDecimal("tecControl");
            this.evalBoardSetup.edfaPower = info.GetDecimal("edfaPower");
            this.evalBoardSetup.dioState = info.GetByte("dioState");
        }

        internal MZBiasSettings topMz;
        internal MZBiasSettings bottomMz;
        internal MZBiasSettings pulseCarverMz;
        internal MZBiasSettings outerMz;
        internal DSDBRSectionCurrents dsdbrSetup;
        internal EvalBoardSettings evalBoardSetup;
        
        private void MarkAllMzValuesChanged(MZBiasSettings mz)
        {
            mz.leftModulation.ValueChanged = true;
            mz.rightModulation.ValueChanged = true;
            mz.leftPhase.ValueChanged = true;
            mz.rightPhase.ValueChanged = true;
        }

        private void MarkAllLaserValuesChanged(DSDBRSectionCurrents laser)
        {
            laser.FsConstant.ValueChanged = true;
            laser.FsNonConstant.ValueChanged = true;
            laser.FSPair.ValueChanged = true;
            laser.Phase.ValueChanged = true;
            laser.Rear.ValueChanged = true;
            laser.SOA.ValueChanged = true;
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("TopMzLeftData", topMz.leftModulation.Voltage_V);
            info.AddValue("TopMzRightData", topMz.rightModulation.Voltage_V);
            info.AddValue("TopMzLeftPhase", topMz.leftPhase.Current_mA);
            info.AddValue("TopMzRightPhase", topMz.rightPhase.Current_mA);
            //
            info.AddValue("BottomMzLeftData", bottomMz.leftModulation.Voltage_V);
            info.AddValue("BottomMzRightData", bottomMz.rightModulation.Voltage_V);
            info.AddValue("BottomMzLeftPhase", bottomMz.leftPhase.Current_mA);
            info.AddValue("BottomMzRightPhase", bottomMz.rightPhase.Current_mA);
            //
            info.AddValue("OuterMzLeftData", outerMz.leftModulation.Voltage_V);
            info.AddValue("OuterMzRightData", outerMz.rightModulation.Voltage_V);
            info.AddValue("OuterMzLeftPhase", outerMz.leftPhase.Current_mA);
            info.AddValue("OuterMzRightPhase", outerMz.rightPhase.Current_mA);
            //
            info.AddValue("PCMzLeftData", pulseCarverMz.leftModulation.Voltage_V);
            info.AddValue("PCMzRightData", pulseCarverMz.rightModulation.Voltage_V);
            info.AddValue("PCMzLeftPhase", pulseCarverMz.leftPhase.Current_mA);
            info.AddValue("PCMzRightPhase", pulseCarverMz.rightPhase.Current_mA);
            //
            info.AddValue("DsdbrGain", dsdbrSetup.Gain.Current_mA);
            info.AddValue("DsdbrRear", dsdbrSetup.Rear.Current_mA);
            info.AddValue("DsdbrPhase", dsdbrSetup.Phase.Current_mA);
            info.AddValue("DsdbrSoa", dsdbrSetup.SOA.Current_mA);
            info.AddValue("DsdbrFS1", dsdbrSetup.FSPair.Current_mA);
            info.AddValue("DsdbrFS2", dsdbrSetup.FsConstant.Current_mA);
            info.AddValue("DsdbrFS3", dsdbrSetup.FsNonConstant.Current_mA);
            //
            info.AddValue("topModCtrlData", evalBoardSetup.topModCtrlData);
            info.AddValue("bottomModCtrlData", evalBoardSetup.bottomModCtrlData);
            info.AddValue("rx1PosDcOffset", evalBoardSetup.rx1PosDcOffset);
            info.AddValue("rx1NegDcOffset", evalBoardSetup.rx1NegDcOffset);
            info.AddValue("rx1PeakControl", evalBoardSetup.rx1PeakControl);
            info.AddValue("rx2PosDcOffset", evalBoardSetup.rx2PosDcOffset);
            info.AddValue("rx2NegDcOffset", evalBoardSetup.rx2NegDcOffset);
            info.AddValue("rx2PeakControl", evalBoardSetup.rx2PeakControl);
            info.AddValue("mainHeaterCtrl", evalBoardSetup.mainHeaterCtrl);
            info.AddValue("rx1HeaterCtrl", evalBoardSetup.rx1HeaterCtrl);
            info.AddValue("rx2HeaterCtrl", evalBoardSetup.rx2HeaterCtrl);
            info.AddValue("tecControl", evalBoardSetup.tecControl);
            info.AddValue("edfaPower", evalBoardSetup.edfaPower);
            info.AddValue("dioState", evalBoardSetup.dioState);
        }

        #endregion
    }
}

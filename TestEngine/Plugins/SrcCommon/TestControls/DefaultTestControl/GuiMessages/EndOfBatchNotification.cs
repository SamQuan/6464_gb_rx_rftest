// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Control Plug-in
//
// EndOfBatchNotification.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.TestLibrary.TestControl.GuiMessages
{
	/// <summary>
	/// Batch View notifies the Test Control Plug-in that the end of batch has been reached.
	/// </summary>
	internal sealed class EndOfBatchNotification
	{
		internal EndOfBatchNotification()
		{
		}
	}
}

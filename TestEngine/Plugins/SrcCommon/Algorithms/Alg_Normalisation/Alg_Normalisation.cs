// Author: Cypress
// Design: based on Receiver Final test

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Provide methods for normalisation
    /// </summary>
    public class Alg_Normalisation
    {
        delegate double Method(double value1, double valu2);

        /// <summary>
        /// Normalise an array.
        /// each array element is subtracted by the meaning of the array
        /// </summary>
        /// <param name="array">input array</param>
        /// <returns>output array which is normalised</returns>
        public static double[] Normalise(double[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            double meaning = Alg_ArrayFunctions.GetMeaningOfArray(array);
            
            return Alg_ArrayFunctions.SubtractFromEachArrayElement(array, meaning);
        }

        /// <summary>
        /// Log Normalise an array
        /// each array element is substracted by the array element which the index is specified.
        /// </summary>
        /// <param name="array">input array</param>
        /// <param name="index">the array element index</param>
        /// <returns></returns>
        public static double[] LogNormalise(double[] array, int index)
        {
            return Normalise(array, index, NormalisationType.Log);
        }

        /// <summary>
        /// Linear Normalise an array
        /// each array element is divided by the array element which the index is specified.
        /// </summary>
        /// <param name="array">input array</param>
        /// <param name="index">the array element index</param>
        /// <returns></returns>
        public static double[] LinearNormalise(double[] array, int index)
        {
            return Normalise(array, index, NormalisationType.Linear);
        }

        /// <summary>
        /// Normalise an array.
        /// each array element is substracted(Linear) or divided(Log) by the array element which the index is specified.
        /// </summary>
        /// <param name="array">input array</param>
        /// <param name="index">the array element index</param>
        /// <param name="normalisationType">normalise type</param>
        /// <returns>output array which is normalised</returns>
        private static double[] Normalise(double[] array, int index,NormalisationType normalisationType)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            if (index < 0 || index >= array.Length)
            {
                throw new ArgumentOutOfRangeException("index", index, "Index out of range.");
            }

            double elementValue = array[index];
            Method method = null;
            double[] result = new double[array.Length];

            switch (normalisationType)
            {
                case NormalisationType.Log:
                    method = Substract;
                    break;
                case NormalisationType.Linear:
                    method = Divide;
                    break;
                default:
                    break;
            }


            for(int i=0;i<array.Length;i++)
            {
                result[i] = method(array[i], elementValue);
            }

            return result;

        }

        private static double Substract(double value1, double value2)
        {
            return (value1 - value2);
        }

        private static double Divide(double value1, double value2)
        {            
            return (value1 / value2);
        }


    }
}

// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// FindTurningPointUsing2ndOrderFit_TEST.cs
//
// Author: Keith Pillar

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Test harness for FindTurningPointUsing2ndOrderFit
    /// </summary>
    [TestFixture]
    public class FindTurningPointUsing2ndOrderFit_TEST
    {
        [Test]
        public void Test2ndDiff1()
        {
            //y = 3x squared + 0x + 2, therefore polyfit coefficients A= 3, B= 0, C = +2
            double[] xData = { 1,  2,  3,  4,  5,   6 };
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData);
            Assert.AreEqual(0, Math.Round(fitData.TurningPointXvalue, 1));
            Assert.AreEqual(2, Math.Round(fitData.TurningPointYvalue, 1));
        }

        [Test]
        public void Test2ndDiff2()
        {
            //y = 3x squared + 0x + 2, therefore polyfit coefficients A= 3, B= 0, C = +2
            //this test using from and to values so it will look at subset of full array.
            double[] xData = { 1, 2, 3, 4, 5, 6 };
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData,1,4);
            Assert.AreEqual(0, Math.Round(fitData.TurningPointXvalue, 1));
            Assert.AreEqual(2, Math.Round(fitData.TurningPointYvalue, 1));
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void Test2ndDiff3()
        {
            //null array
           
            double[] xData = null;
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData, 1, 4);
            
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void Test2ndDiff4()
        {
            //from index incorrect

            double[] xData = { 5, 14, 29, 50, 77, 110 };
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData,-1, 4);

        }
        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void Test2ndDiff5()
        {
            //to index incorrect

            double[] xData = { 5, 14, 29, 50, 77, 110 };
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData, 0, 6);

        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void Test2ndDiff6()
        {
            //arrays mismatched

            double[] xData = { 5, 14, 29, 50, 110 };
            double[] yData = { 5, 14, 29, 50, 77, 110 };
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData fitData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xData, yData, 0, 6);

        }
    
    }

}

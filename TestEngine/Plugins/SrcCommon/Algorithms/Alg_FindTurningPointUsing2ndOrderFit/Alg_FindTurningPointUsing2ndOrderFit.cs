// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
//
// Alg_FindTurningPointUsing2ndOrderFit.cs
//
// Author: kpillar, 2007
// Design: [Reference design documentation]

using System;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// An implementation of the algorithm for finding the turning points of an array of data
    /// using the polyfit.
    /// </summary>
    public sealed class Alg_FindTurningPointUsing2ndOrderFit
    {
        /// <summary>
        /// Empty private constructor.
        /// </summary>
        private Alg_FindTurningPointUsing2ndOrderFit()
        {
        }


        /// <summary>
        /// Function for finding the turning point data, uses all data within the array's
        /// </summary>
        /// <param name="xArray">array holding the x values data</param>
        /// <param name="yArray">array holding the x values data</param>
        /// <returns>
        /// A TurningPointData structure, containing a bool for success,
        /// a double turningPointXvalue, and a double turningPointYvalue 
        /// </returns>
        public static TurningPointData FindTurningPointUsing2ndOrderFit(double[] xArray, double[] yArray)
        {
            if (xArray == null || yArray == null)
            {
                throw new AlgorithmException("FindTurningPointUsing2ndOrderFit - input array is null");
            }

            double xlength = xArray.Length;
            double ylength = yArray.Length;

            if (xlength != ylength)
            {
                throw new AlgorithmException("findTurningPointUsing2ndOrderFit - input arrays must be the same size");
            }

            if (xlength < 3)
            {
                throw new AlgorithmException("findTurningPointUsing2ndOrderFit - input arrays must have at least 3 elements");
            }


            PolyFit myresults = Alg_PolyFit.PolynomialFit(xArray, yArray, 2);

            TurningPointData myreturnData = new TurningPointData();
            myreturnData.Success = false;
            myreturnData.TurningPointXvalue = double.NaN;
            myreturnData.TurningPointYvalue = double.NaN;

            double A = myresults.Coeffs[2];
            double B = myresults.Coeffs[1];
            double C = myresults.Coeffs[0];

            if (A != 0)
            {
                try
                {
                    myreturnData.TurningPointXvalue = -B / (2 * A);

                    myreturnData.TurningPointYvalue = C - ((B * B) / (4 * A));

                    myreturnData.Success = true;

                }
                catch (Exception)
                {
                    throw new AlgorithmException("findTurningPointUsing2ndOrderFit - data returned from polyfit is upsetting calcs");
                }
            }
            
            return myreturnData;
        }

       
        /// <summary>
        /// Function for finding the turning point data, only uses a sub Array,
        /// as defined by the fromIndex, and toIndex params
        /// </summary>
        /// <param name="xArray">array holding the x values data</param>
        /// <param name="yArray">array holding the x values data</param>
        /// <param name="fromIndex">Index point to start from</param>
        /// <param name="toIndex">Index point to finish at</param>
        /// <returns>
        /// A TurningPointData structure, containing a bool for success,
        /// a double turningPointXvalue, and a double turningPointYvalue 
        /// </returns>
        public static TurningPointData FindTurningPointUsing2ndOrderFit(double[] xArray, double[] yArray, int fromIndex, int toIndex)
        {
            if (xArray == null || yArray == null)
            {
                throw new AlgorithmException("FindTurningPointUsing2ndOrderFit - input array is null");
            }

            if (toIndex >= xArray.Length)
            {
                throw new AlgorithmException("DevFromLinearFit - cannot have toIndex bigger than array size");
            }

            if (fromIndex >= toIndex)
            {
                throw new AlgorithmException("DevFromLinearFit - cannot have fromIndex bigger than toIndex");
            }
            double[] mySubXArray = Alg_ArrayFunctions.ExtractSubArray(xArray, fromIndex, toIndex);
            double[] mySubYArray = Alg_ArrayFunctions.ExtractSubArray(yArray, fromIndex, toIndex);
            return (Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(mySubXArray, mySubYArray));
        }

        /// <summary>
        /// The turning point data structure
        /// </summary>
        public struct TurningPointData
        {
            /// <summary>
            /// Found turning point successfully
            /// </summary>
            public bool Success;
            /// <summary>
            /// X-value of turning point
            /// </summary>
            public double TurningPointXvalue;
            /// <summary>
            /// Y-value of turning point
            /// </summary>
            public double TurningPointYvalue;
        }

    }
}

// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZAnalysis_TEST.cs
//
// Author: Mark Fullalove

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Test harness for Alg_MZAnalysis_TEST
    /// </summary>
    [TestFixture]
    public class Alg_MZAnalysis_TEST
    {
        [Test]
        public void Valid_AnalysisNegVQuad()
        {
            double[] xData = xData101();
            double[] yData = yData_A(12,50);

            Alg_MZLVMinMax.LVMinMax dataOut = Alg_MZLVMinMax.MZLVMinMax(xData, yData, 4, true);
            Alg_MZAnalysis.MZAnalysis analysis = Alg_MZAnalysis.ZeroChirpAnalysis(xData, yData, dataOut, true, 0);

            #region Checks
            // By inspection of data :
            //   ER = 28
            //   VPI = 38
            //   VQuad @ 3dB = 2.5

            double deltaER = Math.Abs(analysis.ExtinctionRatio_dB - 28);
            Assert.IsTrue(deltaER < 1);
            double deltaVPi = Math.Abs(38 - analysis.Vpi);
            Assert.IsTrue(deltaVPi < 0.5);
            double deltaQuad = Math.Abs(2.5 - analysis.VQuad);
            Assert.IsTrue(deltaQuad < 0.1);
            #endregion
        }

        [Test]
        public void Valid_AnalysisPosVQuad()
        {
            // (3, 2.5) produces data points spaced equally either side of the null.
            // This is very hard to fit correctly.
            double[] xData = xData101();
            double[] yData = yData_A(3, 2.5);

            Alg_MZLVMinMax.LVMinMax dataOut = Alg_MZLVMinMax.MZLVMinMax(xData, yData, 5, true);
            Alg_MZAnalysis.MZAnalysis analysis = Alg_MZAnalysis.ZeroChirpAnalysis(xData, yData, dataOut, true, 0);

            #region Checks
            // By inspection of data :
            //   ER = 27
            //   VPI = 9.5
            //   VQuad = 9.5

            double deltaER = Math.Abs(27 - analysis.ExtinctionRatio_dB);
            Assert.IsTrue(deltaER < 3);
            double deltaVPi = Math.Abs(9.5 - analysis.Vpi);
            Assert.IsTrue(deltaVPi < 0.2);
            double deltaQuad = Math.Abs(9.5 - analysis.VImb);
            Assert.IsTrue(deltaQuad < 0.7);
            #endregion
        }
        
        [Test]
        public void Valid_AnalysisVQuadWithoutPeak()
        {
            // Data has 1 peak and 2 troughs.
            // Quad point is between a trough and the end of the dataset. 
            // There is not enough data at the end of the dataset to detect a peak.

            double[] xData =  Alg_ArrayFunctions.SubtractFromEachArrayElement(xData101(), 5); // Shift data left / or shift X axis to the right
            double[] yData = yData_A(10, 19);

            Alg_MZLVMinMax.LVMinMax dataOut = Alg_MZLVMinMax.MZLVMinMax(xData, yData, true);
            Alg_MZAnalysis.MZAnalysis analysis = Alg_MZAnalysis.ZeroChirpAnalysis(xData, yData, dataOut, true, 0);

            #region Checks
            // By inspection of data :
            //   VPI = 31.5
            //   VQuad = 29 ( note this is positive, even though there is no valid turning point detected beyond it )
            //
            double deltaVPi = Math.Abs(31.5 - analysis.Vpi);
            Assert.IsTrue(deltaVPi < 0.3,"VPi Calculation is incorrect");
            double deltaQuad = Math.Abs(28.5 - analysis.VImb);
            Assert.IsTrue(deltaQuad < 0.3,"VQuad calculation is incorrect");
            #endregion
        }

        [Test]
        public void DQPSK_BasicAnalysis()
        {
            double[] xData = Alg_ArrayFunctions.SubtractFromEachArrayElement(xData101(), 5); // Shift data left / or shift X axis to the right
            double[] yData = yData_A(10, 19);

            Alg_MZAnalysis.DqpskAnalysis results =  Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, 5, true);
        }

        #region testData
        /// <summary>
        /// Integers between -50 to +50
        /// </summary>
        /// <returns>A list of integers</returns>
        private double[] xData101()
        {
            double[] xData = new double[101];
            for (int i = 0; i < 101; i++)
            {
                xData[i] = i - 50;
            }
            return xData;
        }

        /// <summary>
        /// A set of data whose differential has a positive going turning point
        ///   101 points , sinusoidal, log scale
        ///   2 max, 2 min
        /// </summary>
        private double[] yData_A(double freq, double offset)
        {
            double quality = 1.02;      // 1 gives a very sharp null because power min will be at 0mW
            double scalingFactor = 1.4; // to scale the ER
            double powerOffset = 6;     // to add an insertion loss and scale the power to negative dBm

            double[] yData = new double[101];
            for (int i = 0; i < 101; i++)
            {
                double powermW = Math.Sin(((double)i - offset) / freq);
                double scaledPowerMw = quality + powermW;
                yData[i] = Alg_PowConvert_dB.Convert_mWtodBm(scaledPowerMw);
                yData[i] *= scalingFactor;
                yData[i] -= powerOffset;
            }
            return yData;
        }
        #endregion
    }
}

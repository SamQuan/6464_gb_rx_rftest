// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZAnalysis.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Analysis class for MZ data
    /// </summary> 
    /// 
    /// <remarks>
    /// Rules for zero chirp :
    ///   VQuad is located on the positive slope closest to 0v
    ///   VPI and ER are taken from the min/max pair closest to 
    ///   0v, regardless of the sign of the slope.
    /// In some instances the data will not contain data at either
    /// end of the array to allow detection of turning points. 
    /// If no MAX or MIN points are found the calculation should 
    /// use data from the ends of the array.
    /// 
    /// Rules for negative chirp :
    ///   VQuad, VPI and ER are located on the negative slope closest to 0v
    ///</remarks>
    public sealed partial class Alg_MZAnalysis
    {
        /// <summary>
        /// Calculates ER, VPI and VQuad from a ZERO CHIRP MZ characteristic.
        /// </summary>
        /// <remarks>
        /// Use this method if you wish to calculate the MIN and MAX turning points
        /// outside this algorithm.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="turning_Points">Structure containing the turning points within the Y data</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <param name="modBiasOffset_V">Modulator bias voltage offset</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] voltageArray, double[] powerArray, Alg_MZLVMinMax.LVMinMax turning_Points, bool power_in_dBm, double modBiasOffset_V)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }


            // max or a min at the ends of the array are not true turning points, but
            // if the scan has not collected sufficient data it is valid to use one of them for VQuad
            // however it is a bad idea to add these points when 0v is the end point of the scan
            Alg_MZLVMinMax.LVMinMax turningPoints;
            //if (voltageArray[0] != 0 && voltageArray[voltageArray.Length - 1] != 0)
            //{
            turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turning_Points);
            //}
            //else
            //{
            //    turningPoints = turning_Points;
            //}


            MZAnalysis results = new MZAnalysis();

            double[] peakTPs = turningPoints.VoltagesForAllPeaks();
            double[] valleyTPs = turningPoints.VoltagesForAllValleys();
            if (peakTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No peaks found in data.");
            }
            if (valleyTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No valleys found in data.");
            }



            // Calculate all potential VQuad points
            ArrayList quadPointsbyInflection = new ArrayList();
            ArrayList quadPointsByPower = new ArrayList();
            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                bool found = false;
                int maxIndex = 0;
                double maxPower = Double.MinValue;

                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    // Search for a positive slope
                    if (maxPoint.Voltage > minPoint.Voltage)
                    {
                        maxIndex = maxPoint.Index;
                        maxPower = maxPoint.Power_dBm;
                        found = true;
                        break;
                    }
                }

                // If we found a positive slope that uses this MIN we can analyse it.
                if (found)
                {
                    // METHOD 1
                    // Use point of inflection to find the point at which the gradient is at a MAX
                    // Power must be in mW
                    double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                    Alg_FindPointOfInflection.PointOfInflection potentialQuadPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, minPoint.Index, maxIndex);
                    if (potentialQuadPoint.Found)
                    {
                        quadPointsbyInflection.Add(potentialQuadPoint);
                    }

                    // METHOD 2
                    // Use half power point, 3dB down from Peak
                    Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                    QuadPoint3dB.Power_dBm = maxPower - 3;
                    double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, minPoint.Index, maxIndex);
                    double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, minPoint.Index, maxIndex);
                    // False peaks at the extremes of the sweep may not have a 3dB point
                    double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                    double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
                    if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
                    {
                        // Not found within data
                        QuadPoint3dB.Voltage = Double.NaN;
                    }
                    else
                    {
                        QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                    }
                    quadPointsByPower.Add(QuadPoint3dB);
                }
            }

            // Check all method 1 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltages = new double[quadPointsbyInflection.Count];
            for (int i = 0; i < quadPointsbyInflection.Count; i++)
            {
                Alg_FindPointOfInflection.PointOfInflection poi = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[i];
                quadPointVoltages[i] = poi.XValue;
            }
            int closestQUADToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltages, modBiasOffset_V);

            // Get details of the closest one to 0v
            Alg_FindPointOfInflection.PointOfInflection quadPoint = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[closestQUADToOffsetV];
            results.VImb = quadPoint.XValue;



            // Repeat for method 2 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltagesByPower = new double[quadPointsByPower.Count];
            for (int i = 0; i < quadPointsByPower.Count; i++)
            {
                Alg_MZLVMinMax.DataPoint poi = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[i];
                quadPointVoltagesByPower[i] = poi.Voltage;
            }
            int closestQUADToOffsetV_byPower = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltagesByPower, modBiasOffset_V);

            // Get details of the closest one to 0v
            Alg_MZLVMinMax.DataPoint quadPointByPower = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[closestQUADToOffsetV_byPower];
            results.VQuad = quadPointByPower.Voltage;

            #region exceptions to the rule
            if (Double.IsNaN(results.VImb))
            {
                // if not found try another way !
                Alg_MZLVMinMax.DataPoint negSlopeMaxPoint = new Alg_MZLVMinMax.DataPoint();
                Alg_MZLVMinMax.DataPoint negSlopeMinPoint = new Alg_MZLVMinMax.DataPoint();
                Alg_MZLVMinMax.DataPoint posSlopeMaxPoint = new Alg_MZLVMinMax.DataPoint();
                double maxPower = Double.MinValue;
                bool found = false;
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                    {
                        // Search for a negative slope
                        if (maxPoint.Voltage < minPoint.Voltage)
                        {
                            maxPower = maxPoint.Power_dBm;
                            found = true;
                            negSlopeMinPoint = minPoint;
                            negSlopeMaxPoint = maxPoint;
                            break;
                        }

                    }
                    if (found)
                        break;
                }
                // find max after this min
                if (found)
                {
                    found = false;
                    foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                    {
                        // Search for a next max ( positive slope )
                        if (maxPoint.Voltage > negSlopeMinPoint.Voltage)
                        {
                            found = true;
                            posSlopeMaxPoint = maxPoint;
                            break;
                        }
                    }
                }
                if (found)
                {
                    Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                    QuadPoint3dB.Power_dBm = maxPower - 3;
                    double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, negSlopeMinPoint.Index, posSlopeMaxPoint.Index);
                    double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, negSlopeMinPoint.Index, posSlopeMaxPoint.Index);
                    // Make sure there's enough power to give a 3dB point
                    double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                    double maxPwr = Alg_PointSearch.FindMaxValueInArray(subArray_Pwr);
                    if (QuadPoint3dB.Power_dBm < minPwr || QuadPoint3dB.Power_dBm > maxPwr)
                    {
                        // Not found within data
                        QuadPoint3dB.Voltage = Double.NaN;
                    }
                    else
                    {
                        results.VQuad = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                        results.VImb = results.VQuad;
                        // The distance Max->Min = Vpi
                        // The distance Min-> 3dB = Vpi / 2
                        // Max -> 3dB = 3/2 Vpi , so we can calculate Vpi by multiplying by 2/3
                        // Waveform may stretch as we move away from 3dB, so don't just use Max - Min
                        results.Vpi = Math.Abs(results.VQuad - negSlopeMaxPoint.Voltage) * 2 / 3;
                    }

                }
            }
            #endregion

            // Find the MAX and MIN closest to Quadrature ( Point of inflection )
            if (results.VImb != Double.NaN)
            {
                Alg_MZLVMinMax.DataPoint realMax, realMin;
                realMax.Power_dBm = Double.NaN;
                realMax.Voltage = Double.NaN;
                realMin.Power_dBm = Double.NaN;
                realMin.Voltage = Double.NaN;

                double deltaV = Double.MaxValue;
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    if (maxPoint.Voltage > results.VImb && Math.Abs(maxPoint.Voltage - results.VImb) < deltaV)
                    {
                        // Pos gradient
                        deltaV = Math.Abs(maxPoint.Voltage - results.VImb);
                        realMax = maxPoint;
                    }
                }
                deltaV = Double.MaxValue;
                foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                {
                    if (minPoint.Voltage < results.VImb && Math.Abs(results.VImb - minPoint.Voltage) < deltaV)
                    {
                        // Pos gradient
                        deltaV = Math.Abs(minPoint.Voltage - results.VImb);
                        realMin = minPoint;
                    }
                }

                // ER & VPi
                results.ExtinctionRatio_dB = realMax.Power_dBm - realMin.Power_dBm;
                results.Vpi = Math.Abs(realMax.Voltage - realMin.Voltage);
                results.VoltageAtMax = realMax.Voltage;
                results.PowerAtMax_dBm = realMax.Power_dBm;
                results.VoltageAtMin = realMin.Voltage;
                results.PowerAtMin_dBm = realMin.Power_dBm;
            }
            else
            {
                // Just in case !
                int closestMAXToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(peakTPs, modBiasOffset_V);
                int closestMINToOffsetV = Alg_ArrayFunctions.FindIndexOfNearestElement(valleyTPs, modBiasOffset_V);

                // ER & VPi
                results.ExtinctionRatio_dB = turningPoints.PeakData[closestMAXToOffsetV].Power_dBm - turningPoints.ValleyData[closestMINToOffsetV].Power_dBm;
                results.Vpi = Math.Abs(turningPoints.PeakData[closestMAXToOffsetV].Voltage - turningPoints.ValleyData[closestMINToOffsetV].Voltage);
                results.VoltageAtMax = turningPoints.PeakData[closestMAXToOffsetV].Voltage;
                results.PowerAtMax_dBm = turningPoints.PeakData[closestMAXToOffsetV].Power_dBm;
                results.VoltageAtMin = turningPoints.ValleyData[closestMINToOffsetV].Voltage;
                results.PowerAtMin_dBm = turningPoints.ValleyData[closestMINToOffsetV].Power_dBm;
            }


            return results;
        }

        /// <summary>
        /// Zero chirp analysis.
        /// Quad point, min and max are on the slope closest to 0v
        /// Imbalance point is at the point of inflection on the same slope. 
        /// </summary>
        /// <param name="voltageArray">Voltage</param>
        /// <param name="powerArray">Power</param>
        /// <param name="power_in_dBm">true if power is in dBm</param>
        /// <param name="positiveSlope">Whether to perform analysis on the positive slope</param>
        /// <param name="modBiasOffset_V">Modulator bias voltage offset</param>
        /// <returns>Analysis results</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm, bool positiveSlope, double modBiasOffset_V)
        {
            if (positiveSlope)
            {
                Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray, 5, power_in_dBm);
                return ZeroChirpAnalysis(voltageArray, powerArray, turningPoints, power_in_dBm, modBiasOffset_V);
            }
            else
            {
                // Analysis works on the positive slope by default
                double[] mirrored_V = Alg_ArrayFunctions.ReverseArray(voltageArray);
                mirrored_V = Alg_ArrayFunctions.MultiplyEachArrayElement(mirrored_V, -1);
                double[] reversed_P = Alg_ArrayFunctions.ReverseArray(powerArray);

                Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(mirrored_V, reversed_P, 5, power_in_dBm);
                MZAnalysis result = ZeroChirpAnalysis(mirrored_V, reversed_P, turningPoints, power_in_dBm, -modBiasOffset_V);
                // Undo the mirroring of the voltage results
                result.VImb *= -1;
                result.VoltageAtMax *= -1;
                result.VoltageAtMin *= -1;
                result.VQuad *= -1;
                return result;
            }
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <param name="positiveSlope">True if points of interest lie on the positive slope</param>
        /// <param name="modBiasOffset_V">Modulator bias voltage offset</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm, bool positiveSlope, double modBiasOffset_V)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return ZeroChirpAnalysis(bothArmsVoltage, bothArmsPower, power_in_dBm, positiveSlope, modBiasOffset_V);
        }


        /// <summary>
        /// Calculates ER, VPI and VQuad from ZERO CHIRP MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpTapAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return ZeroChirpTapAnalysis(bothArmsVoltage, bothArmsPower, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from ZERO CHIRP MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpTapAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            // Tap analysis works on current rather than power.
            // Shift power array so that there are no negative numbers.
            double min = Alg_PointSearch.FindMinValueInArray(powerArray);
            double[] normalisedPower = Alg_ArrayFunctions.SubtractFromEachArrayElement(powerArray, min);
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, normalisedPower, 5, power_in_dBm);
            return ZeroChirpAnalysis(voltageArray, normalisedPower, turningPoints, power_in_dBm, 0);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ imbalance control sweep.
        /// </summary>
        /// <remarks>
        /// The only data actually used at final test is the imbalance point, which should be 
        /// the closest minima to 0v
        /// 
        /// Use this method if you wish to calculate the MIN and MAX turning points
        /// outside this algorithm.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="turning_Points">Structure containing the turning points within the Y data</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpImbalanceCtrlAnalysis(double[] voltageArray, double[] powerArray, Alg_MZLVMinMax.LVMinMax turning_Points, bool power_in_dBm)
        {
            // Ipi can be hard to find if there is insufficient data.
            // RULES ( generally in accordange with D00001S Issue 02 ) :
            //
            // (1)
            // MIN + MAX on positive slope
            //  Ipi = Max - Min
            //
            // (2)
            // MAX with false minima
            //  Ipi = 2 x ( Max - Quad )
            //
            // (3)
            // MIN + MAX on neg slope
            //   Quad point = Max - 3dB, but on POS slope.
            //   Ipi = 3/2 x ( Max - Quad )
            //
            // (4)
            // MIN + MAX on neg slope
            //   As case 2
            //
            // In all cases, imbalance point is the MIN closest to 0

            #region PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }
            #endregion

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            double[] powerArray_mW = null;
            if (power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
                powerArray_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray);
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
                powerArray_mW = (double[])powerArray.Clone();
            }

            // copy the turning point collection
            Alg_MZLVMinMax.LVMinMax turningPoints = turning_Points;

            // Closest true MIN to 0
            bool foundMin = false;
            bool foundMax = false;
            Alg_MZLVMinMax.DataPoint closestMinTo0 = new Alg_MZLVMinMax.DataPoint();
            closestMinTo0.Voltage = double.NaN;

            double minV = Double.MaxValue;
            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                if (Math.Abs(minPoint.Voltage) < minV)
                {
                    closestMinTo0 = minPoint;
                    minV = Math.Abs(minPoint.Voltage);
                    foundMin = true;
                }
            }

            // MAX points
            Alg_MZLVMinMax.DataPoint maxToAnalyse;
            maxToAnalyse.Power_dBm = double.NaN;
            maxToAnalyse.Voltage = double.NaN;
            maxToAnalyse.Index = int.MinValue;


            double maxV = Double.MinValue;
            double posSlopeDelta = double.MaxValue;
            foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
            {
                // Closest MAX point with a positive slope
                if (foundMin && maxPoint.Voltage > closestMinTo0.Voltage && maxPoint.Voltage - closestMinTo0.Voltage < posSlopeDelta)
                {
                    maxToAnalyse = maxPoint;
                    posSlopeDelta = maxPoint.Voltage - closestMinTo0.Voltage;
                    foundMax = true;
                }
            }

            // Add array end points
            turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turning_Points);

            if (!foundMin)
            {
                // No true MIN found. Need to consider the end points.
                foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                {
                    if (Math.Abs(minPoint.Voltage) < minV)
                    {
                        closestMinTo0 = minPoint;
                        minV = Math.Abs(minPoint.Voltage);
                        foundMin = true;
                    }
                }
            }

            MZAnalysis results = new MZAnalysis();
            Alg_FindPointOfInflection.PointOfInflection inflectionPoint;

            bool foundIpi = false;
            if (foundMax)
            {
                // preferred option - true MIN + MAX on pos slope
                results.Vpi = maxToAnalyse.Voltage - closestMinTo0.Voltage;
                inflectionPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, powerArray_mW, closestMinTo0.Index, maxToAnalyse.Index);
                if (!inflectionPoint.XValue.Equals(double.NaN))
                {
                    results.VQuad = inflectionPoint.XValue;
                    foundIpi = true;
                }
            }
            else
            {
                // Need to look for a MAX again including the endpoints
                maxV = double.MaxValue;
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    // Closest MAX to 0
                    if (Math.Abs(maxPoint.Voltage) < maxV)
                    {
                        maxToAnalyse = maxPoint;
                        maxV = Math.Abs(maxPoint.Voltage);
                        foundMax = true;
                    }
                }
            }

            results.ExtinctionRatio_dB = maxToAnalyse.Power_dBm - closestMinTo0.Power_dBm;
            results.PowerAtMax_dBm = maxToAnalyse.Power_dBm;
            results.VoltageAtMax = maxToAnalyse.Voltage;
            results.PowerAtMin_dBm = closestMinTo0.Power_dBm;
            results.VImb = closestMinTo0.Voltage;
            results.VoltageAtMin = closestMinTo0.Voltage;

            #region Ipi & Iquad
            if (!foundIpi)
            {
                // Find inflection point on slope AFTER min ( pos slope )
                Alg_MZLVMinMax.DataPoint localMax;
                localMax.Index = int.MinValue;
                double maxPointDelta = Double.MaxValue;
                foundMax = false;
                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    if (closestMinTo0.Voltage - maxPoint.Voltage < maxPointDelta)
                    {
                        localMax = maxPoint;
                        maxPointDelta = closestMinTo0.Voltage - maxPoint.Voltage;
                        foundMax = true;
                    }
                }
                if (foundMax)
                {
                    inflectionPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, powerArray_mW, closestMinTo0.Index, localMax.Index);
                    if (!inflectionPoint.XValue.Equals(double.NaN))
                    {
                        results.Vpi = (inflectionPoint.XValue - maxToAnalyse.Voltage) * 2 / 3;
                        results.VQuad = inflectionPoint.XValue;
                        foundIpi = true;
                    }
                }
            }

            if (!foundIpi)  // Still not found
            {
                // Find 3dB / POI on slope BEFORE max
                Alg_MZLVMinMax.DataPoint localMin;
                localMin.Index = int.MinValue;
                double minPointDelta = Double.MaxValue;
                foundMin = false;
                foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                {
                    if (minPoint.Voltage - maxToAnalyse.Voltage < minPointDelta)
                    {
                        localMin = minPoint;
                        minPointDelta = minPoint.Voltage - maxToAnalyse.Voltage;
                        foundMin = true;
                    }
                }
                if (foundMin)
                {
                    inflectionPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, powerArray_mW, localMin.Index, maxToAnalyse.Index);
                    if (!inflectionPoint.XValue.Equals(double.NaN))
                    {
                        results.Vpi = (maxToAnalyse.Voltage - inflectionPoint.XValue) * 2;
                        results.VQuad = inflectionPoint.XValue;
                        foundIpi = true;
                    }
                }
            }

            if (!foundIpi)  // Still not found
            {
                // Find 3dB / POI on slope AFTER max
                Alg_MZLVMinMax.DataPoint localMin;
                double minPointDelta = Double.MaxValue;
                foundMin = false;
                foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
                {
                    if (minPoint.Voltage - maxToAnalyse.Voltage < minPointDelta)
                    {
                        localMin = minPoint;
                        minPointDelta = minPoint.Voltage - maxToAnalyse.Voltage;
                        foundMin = true;
                    }
                }
                if (foundMin)
                {
                    inflectionPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, powerArray_mW, maxToAnalyse.Index, closestMinTo0.Index);
                    results.Vpi = (inflectionPoint.XValue - maxToAnalyse.Voltage) * 2;
                    results.VQuad = inflectionPoint.XValue;
                }
            }
            #endregion

            return results;
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// The only data actually used at final test is the imbalance point, which should be 
        /// the closest minima to 0v.
        /// 
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpImbalanceCtrlAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray, 5, power_in_dBm);
            return NegChirpImbalanceCtrlAnalysis(voltageArray, powerArray, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ imbalance control sweep.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// 
        /// The only data actually used at final test is the imbalance point, which should be 
        /// the closest minima to 0v
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpImbalanceCtrlAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return NegChirpImbalanceCtrlAnalysis(bothArmsVoltage, bothArmsPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from negative chirp MZ tap data.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpTapAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            // Tap analysis works on current rather than power.
            // Shift power array so that there are no negative numbers.
            double min = Alg_PointSearch.FindMinValueInArray(powerArray);
            double[] normalisedPower = Alg_ArrayFunctions.SubtractFromEachArrayElement(powerArray, min);
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, normalisedPower, 5, power_in_dBm);
            return NegChirpImbalanceCtrlAnalysis(voltageArray, normalisedPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from negative chirp MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpTapAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return NegChirpTapAnalysis(bothArmsVoltage, bothArmsPower, power_in_dBm);
        }


    }
}
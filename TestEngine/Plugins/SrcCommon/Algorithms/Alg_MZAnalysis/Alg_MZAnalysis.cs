// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZAnalysis.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Analysis class for MZ data.
    /// 
    /// This file contains common methods and data definitions.
    /// 
    /// Gen4 and DQPSK analysis is split into seperate files to ease readability
    /// </summary>
    

    public sealed partial class Alg_MZAnalysis
    {
        /// <summary>
        /// Empty private constructor
        /// </summary>
        private Alg_MZAnalysis()
        {
        }

        
        /// <summary>
        /// Adds false turning points at either end of the array to enable the analysis to work
        /// within the end regions of the data.
        /// </summary>
        /// <param name="voltageArray">Voltage data</param>
        /// <param name="powerArray_dBm">Power data in dBm</param>
        /// <param name="turningPoints">A structure holding turning points</param>
        /// <returns>A new structure containing the original turning points plus a maxima or a mimima as appropriate at each end if the dataset</returns>
        /// 
        private static Alg_MZLVMinMax.LVMinMax addArrayEndPoints(double[] voltageArray, double[] powerArray_dBm, Alg_MZLVMinMax.LVMinMax turningPoints)
        {
            ArrayList peaks = new ArrayList(turningPoints.PeakData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.PeakData)
            {
                peaks.Add(dataPoint);
            }

            ArrayList valleys = new ArrayList(turningPoints.ValleyData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.ValleyData)
            {
                valleys.Add(dataPoint);
            }

            int lastElement = voltageArray.Length - 1;

            // End point
            Alg_MZLVMinMax.DataPoint endPoint = new Alg_MZLVMinMax.DataPoint();
            endPoint.Index = lastElement;
            endPoint.Power_dBm = powerArray_dBm[lastElement];
            endPoint.Voltage = voltageArray[lastElement];

            // Start point
            Alg_MZLVMinMax.DataPoint startPoint = new Alg_MZLVMinMax.DataPoint();
            startPoint.Index = 0;
            startPoint.Power_dBm = powerArray_dBm[0];
            startPoint.Voltage = voltageArray[0];

            // Determine gradient at each end of the data
            bool positiveSlopeAtStart = false; ;
            bool positiveSlopeAtEnd = false;
            LinearLeastSquaresFit endFitCoeffs = null;
            LinearLeastSquaresFit startFitCoeffs = null;

            // Check whether there are sufficient turning points detected
            if (turningPoints.PeakData.Length < 1 && turningPoints.ValleyData.Length < 1)
            {   // No peaks or valleys
                if (endPoint.Power_dBm > startPoint.Power_dBm)
                {
                    positiveSlopeAtEnd = true;
                    positiveSlopeAtStart = true;
                }
            }
            else
            {
                if (turningPoints.PeakData.Length < 1)
                {   // No peaks with a valley between means that data is "U" shaped.
                    peaks.Add(endPoint);            // Positive gradient near end
                    peaks.Insert(0, startPoint);    // Negative gradient near start
                }
                if (turningPoints.ValleyData.Length < 1)
                {    // No valleys with a peak in the middle means that data is "^" shaped.
                    valleys.Insert(0, startPoint);   // Positive gradient near start
                    valleys.Add(endPoint);            // Negative gradient near end
                }
                // This should be the "usual" case
                if (turningPoints.PeakData.Length >= 1 && turningPoints.ValleyData.Length >= 1)
                {   // To find the sign of the gradient we can draw a straight line from the end of the array to the nearest turning point.
                    double firstTurningPoint_V = turningPoints.PeakData[0].Voltage < turningPoints.ValleyData[0].Voltage ? turningPoints.PeakData[0].Voltage : turningPoints.ValleyData[0].Voltage;
                    double lastTurningPoint_V = turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage > turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage ? turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage : turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage;

                    // Due to the way that the LeastSquaresFit works we need be sure that the data point above 
                    // the fit start and stop points will result in a different point.
                    if (firstTurningPoint_V > voltageArray[2])        
                        startFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, voltageArray[0], firstTurningPoint_V);
                    if (lastTurningPoint_V < voltageArray[lastElement - 3])
                        endFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, lastTurningPoint_V, voltageArray[lastElement]);
                    // Check gradient
                    if (endFitCoeffs != null && endFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtEnd = true;
                    }
                    if (startFitCoeffs != null && startFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtStart = true;
                    }

                    // Deal with end point if analysis was ok
                    if (endFitCoeffs != null)
                    {
                        if (positiveSlopeAtEnd)
                        {
                            peaks.Add(endPoint);            // Positive gradient near end (peak)
                        }
                        else
                        {
                            valleys.Add(endPoint);          // Negative gradient near end (valley)
                        }
                    }

                    // Deal with start point
                    if (startFitCoeffs != null)
                    {
                        if (positiveSlopeAtStart)
                        {
                            valleys.Insert(0, startPoint);     // Positive gradient near start. (valley)
                        }
                        else
                        {
                            peaks.Insert(0, startPoint);   // Negative gradient near start (peak)
                        }
                    }
                }
            }

            // Create return structure
            Alg_MZLVMinMax.DataPoint[] peakData = (Alg_MZLVMinMax.DataPoint[])peaks.ToArray(typeof(Alg_MZLVMinMax.DataPoint));
            Alg_MZLVMinMax.DataPoint[] valleyData = (Alg_MZLVMinMax.DataPoint[])valleys.ToArray(typeof(Alg_MZLVMinMax.DataPoint));

            Alg_MZLVMinMax.LVMinMax returnData = new Alg_MZLVMinMax.LVMinMax();
            returnData.PeakData = peakData;
            returnData.ValleyData = valleyData;

            return returnData;
        }
        /// <summary>
        /// Structure to hold the MZ Analysis return values
        /// </summary>
        public struct MZAnalysis
        {
            /// <summary>
            /// VPi
            /// </summary>
            public double Vpi;
            /// <summary>
            /// Imbalance correction voltage. 
            /// This will be similar to the quadrature voltage for some device families.
            /// </summary>
            public double VImb;
            /// <summary>
            /// Quadrature voltage calculated by using 3dB power level.
            /// </summary>
            public double VQuad;
            /// <summary>
            /// Extinction Ratio
            /// </summary>
            public double ExtinctionRatio_dB;
            /// <summary>
            /// Voltage for max power
            /// </summary>
            public double VoltageAtMax;
            /// <summary>
            /// Voltage for min power
            /// </summary>
            public double VoltageAtMin;
            /// <summary>
            /// Peak power value
            /// </summary>
            public double PowerAtMax_dBm;
            /// <summary>
            /// Minimum power value
            /// </summary>
            public double PowerAtMin_dBm;
        }

        /// <summary>
        /// Structure to hold the MZ Analysis return values
        /// </summary>
        public struct DqpskAnalysis
        {
            /// <summary>
            /// Positive slope Vpi. Measured at the closest peak to 0v
            /// </summary>
            public double Vpi1;
            /// <summary>
            /// Negative slope Vpi. Measured at the second closest peak to 0v
            /// </summary>
            public double Vpi2;
            /// <summary>
            /// Modulation voltage. Peak closest to 0v to the next peak.
            /// </summary>
            public double Vmod;
            /// <summary>
            /// Extinction Ratio measured at the peak closest to 0v
            /// </summary>
            public double ExtinctionRatio1_dB;
            /// <summary>
            /// Extinction Ratio measured at second closest peak to 0v
            /// </summary>
            public double ExtinctionRatio2_dB;
            /// <summary>
            /// Voltage for the max power point closest to 0v
            /// </summary>
            public double VoltageAtMax1;
            /// <summary>
            /// Voltage for the second closest max power point to 0v
            /// </summary>
            public double VoltageAtMax2;
            /// <summary>
            /// Voltage for min power
            /// </summary>
            public double VoltageAtMin;
            /// <summary>
            /// Power value at the closest peak to 0v
            /// </summary>
            public double PowerAtMax1_dBm;
            /// <summary>
            /// Power value at the second closest peak to 0v
            /// </summary>
            public double PowerAtMax2_dBm;
            /// <summary>
            /// Minimum power value
            /// </summary>
            public double PowerAtMin_dBm;
        }

        /// <summary>
        /// Structure to specify the methodology used to analyse MZ data
        /// </summary>
        public struct MzAnalysisRules
        {
            /// <summary>
            /// True if the power is in dBm
            /// </summary>
            public bool power_in_dBm;
            /// <summary>
            /// Define the order of the polynomial fit which may be used to smooth the data before estimating the location of turning points
            /// </summary>
            public int orderOfPolyFit;
            /// <summary>
            /// Number of adjacent data points to look at before confirming a point as a real turning point
            /// </summary>
            public int numberOfPointsForTurningPointDetection;
            /// <summary>
            /// Number of points used for the second order fit around a turning point
            /// </summary>
            public int numberOfPointsForTurningPointFit;
            /// <summary>
            /// Number of points to be used if the data is smoothed
            /// </summary>
            public int numberOfPointsForSmoothing;
            /// <summary>
            /// Method of used to process that data before estimating the locations of turning points
            /// </summary>
            public SmoothingMethod smoothingMethod;
            /// <summary>
            /// Used to specify the preferred location of a quadrature point
            /// </summary>
            public QuadPointSlope quadPointSlope;
            /// <summary>
            /// Additional selection criteria if multiple quadrature points exist
            /// </summary>
            public QuadPointSelectionCriteria quadPointSelectionCriteria;
            /// <summary>
            /// Method used to locate the quadrature point
            /// </summary>
            public QuadPointLocationMethod quadPointLocationMethod;
            /// <summary>
            /// If set this may be used to select a point of interest other than the one closest to x=0
            /// </summary>
            public double targetLocation;
        }

        /// <summary>
        /// Method of used to process that data before estimating the locations of turning points
        /// </summary>
        public enum SmoothingMethod
        {
            /// <summary>
            /// Apply a polynomial fit
            /// </summary>
            PolyFit,
            /// <summary>
            /// Use pascal triangle smoothing
            /// </summary>
            PascalTriangleSmoothing
        }
        /// <summary>
        /// Used to specify the preferred location of a quadrature point
        /// </summary>
        public enum QuadPointSlope
        {
            /// <summary>
            /// Quad point must be located on a slope of positive gradient
            /// </summary>
            PositiveSlope,
            /// <summary>
            /// Quadrature point must be located on a point of negative gradient
            /// </summary>
            NegativeSlope,
            /// <summary>
            /// Just find me a quadrature point. I don't care where it is !
            /// </summary>
            EitherSlope
        }
        /// <summary>
        /// Additional selection criteria if multiple quadrature points exist
        /// </summary>
        public enum QuadPointSelectionCriteria
        {
            /// <summary>
            /// Find the quadrature point closest to zero
            /// </summary>
            ClosestToZero,
            /// <summary>
            /// Find the quadrature point closest to the value of x=targetLocation
            /// </summary>
            ClosestToTarget,
            /// <summary>
            /// Find the quadrature point on the slope that has the highest extinction ratio
            /// </summary>
            HighestER
        }
        /// <summary>
        /// Method used to locate the quadrature point
        /// </summary>
        public enum QuadPointLocationMethod
        {
            /// <summary>
            /// The 3dB point. Located 3dB below than the peak power
            /// </summary>
            HalfPower,
            /// <summary>
            /// Half way in terms of bias between the peak and the null
            /// </summary>
            HalfBias,
            /// <summary>
            /// The point at which the gradient reaches a maximum or a minimum.
            /// This is the preferred method where turning points do not exist.
            /// </summary>
            Inflection
        }
    }
}

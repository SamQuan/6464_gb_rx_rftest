// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZAnalysis.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Analysis class for MZ data
    /// </summary> 
    public sealed partial class Alg_MZAnalysis
    {
        /// <summary>
        /// General DQPSK MZ analysis.
        /// Locate the minumum point closest to 0v and return data from the peaks on either side of it.
        /// </summary>
        /// <param name="leftArmVoltageArray">leftArmVoltageArray. Must be in ascending order</param>
        /// <param name="rightArmVoltageArray">rightArmVoltageArray. Must be in ascending order</param>
        /// <param name="leftArmPowerArray">leftArmPowerArray</param>
        /// <param name="rightArmPowerArray">rightArmPowerArray</param>
        /// <param name="numberOfPointsForFit">Number of points for turning point fit</param>
        /// <param name="power_in_dBm">True if power is in dBm</param>
        /// <returns>Analysis data</returns>
        public static DqpskAnalysis DqpskMzAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, int numberOfPointsForFit, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] bothArmsVoltage = JoinSeData(leftArmVoltageArray, rightArmVoltageArray, true);
            double[] bothArmsPower = JoinSeData(leftArmPowerArray, rightArmPowerArray, false);

            return DqpskMzAnalysis(bothArmsVoltage, bothArmsPower, numberOfPointsForFit, power_in_dBm);
        }

        /// <summary>
        /// General DQPSK MZ analysis.
        /// Locate the minumum point closest to 0v and return data from the peaks on either side of it.
        /// </summary>
        /// <param name="leftArmVoltageArray">leftArmVoltageArray. Must be in ascending order</param>
        /// <param name="rightArmVoltageArray">rightArmVoltageArray. Must be in ascending order</param>
        /// <param name="leftArmPowerArray">leftArmPowerArray</param>
        /// <param name="rightArmPowerArray">rightArmPowerArray</param>
        /// <param name="mzAnalysisRules">Rules controlling data analysis methodology. Anything related to quadrature is ignored</param>
        /// <returns>Analysis data</returns>
        public static DqpskAnalysis DqpskMzAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, MzAnalysisRules mzAnalysisRules)
        {
            // Stitch the single ended sweeps together
            double[] bothArmsVoltage = JoinSeData(leftArmVoltageArray, rightArmVoltageArray, true);
            double[] bothArmsPower = JoinSeData(leftArmPowerArray, rightArmPowerArray, false);

            return DqpskMzAnalysis(bothArmsVoltage, bothArmsPower, mzAnalysisRules);
        }

        /// <summary>
        /// Joins data collected from single ended sweeps, removes the duplicate point at x=0
        /// </summary>
        /// <param name="leftArmArray"></param>
        /// <param name="rightArmArray"></param>
        /// <param name="IsVoltageData"></param>
        /// <returns></returns>
        public static double[] JoinSeData(double[] leftArmArray, double[] rightArmArray, bool IsVoltageData)
        {
            double[] bothArms;
            double[] reversedRightArm = Alg_ArrayFunctions.ReverseArray(rightArmArray);
            reversedRightArm = Alg_ArrayFunctions.ExtractSubArray(reversedRightArm, 1, reversedRightArm.Length - 1);
            if (IsVoltageData)
            {
                double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArm, -1);
                bothArms = Alg_ArrayFunctions.JoinArrays(leftArmArray, positiveReversedRightArmVoltage);
            }
            else
                bothArms = Alg_ArrayFunctions.JoinArrays(leftArmArray, reversedRightArm);

            return bothArms;
        }

        /// <summary>
        /// General DQPSK MZ analysis.
        /// Locate the minumum point closest to 0v and return data from the peaks on either side of it.
        /// </summary>
        /// <param name="voltageArray">Voltage data. Must be in ascending order</param>
        /// <param name="powerArray">power data</param>
        /// <param name="numberOfPointsForFit">Number of points for turning point fit</param>
        /// <param name="power_in_dBm">True if power is in dBm</param>
        /// <returns>Analysis data</returns>
        public static DqpskAnalysis DqpskMzAnalysis(double[] voltageArray, double[] powerArray, int numberOfPointsForFit, bool power_in_dBm)
        {
            // Set the defaults as used by old methods
            MzAnalysisRules mzAnalysisRules = new MzAnalysisRules();
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 5;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.power_in_dBm = power_in_dBm;
            mzAnalysisRules.smoothingMethod = SmoothingMethod.PolyFit;            
            mzAnalysisRules.orderOfPolyFit = 20;

            return DqpskMzAnalysis(voltageArray, powerArray, mzAnalysisRules);
        }

        /// <summary>
        /// General DQPSK MZ analysis.
        /// Locate the minumum point closest to 0v and return data from the peaks on either side of it.
        /// </summary>
        /// <param name="voltageArray">Voltage data. Must be in ascending order</param>
        /// <param name="powerArray">Power arrray</param>
        /// <param name="mzAnalysisRules">Rules controlling data analysis methodology. Anything related to quadrature is ignored</param>
        /// <returns>Analysis data</returns>
        public static DqpskAnalysis DqpskMzAnalysis(double[] voltageArray, double[] powerArray, MzAnalysisRules mzAnalysisRules)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }


            // Locate the turning points using an appropriate method
            Alg_MZLVMinMax.LVMinMax turningPoints;
            if (mzAnalysisRules.smoothingMethod == SmoothingMethod.PolyFit)
            {
                // Smooth and poly fit if required
                if (mzAnalysisRules.numberOfPointsForSmoothing > 0)
                    powerArray = Alg_Smoothing.PascalTriangleSmooth(powerArray, mzAnalysisRules.numberOfPointsForSmoothing);
                turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.orderOfPolyFit, mzAnalysisRules.numberOfPointsForTurningPointDetection, mzAnalysisRules.power_in_dBm);
            }
            else
            {
                // Just smooth
               // turningPoints = Alg_MZLVMinMax.MZLVMinMax_Smoothed(voltageArray, powerArray, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.numberOfPointsForTurningPointDetection, mzAnalysisRules.numberOfPointsForSmoothing, mzAnalysisRules.power_in_dBm);

                turningPoints = Alg_MZLVMinMax.MZLVMinMax_Smoothed(voltageArray, powerArray, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.numberOfPointsForSmoothing, mzAnalysisRules.power_in_dBm);
            }
            
            // Deal with partial sweeps by including the endpoints of the array
            double[] powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turningPoints);


            // Fill in the return data
            DqpskAnalysis returnData;

            // Closest MIN to target
            int closestIndexToTarget = int.MinValue;
            double closestVoltageToTarget = double.MaxValue;
            double powerAtTurningPoint_dBm = double.MinValue;
            bool found = false;
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.ValleyData)
            {
                if (Math.Abs(dataPoint.Voltage - mzAnalysisRules.targetLocation) < Math.Abs(closestVoltageToTarget - mzAnalysisRules.targetLocation))
                {
                    closestVoltageToTarget = dataPoint.Voltage;
                    closestIndexToTarget = dataPoint.Index;
                    powerAtTurningPoint_dBm = dataPoint.Power_dBm;
                    found = true;
                }
            }
            // Add MIN to results
            if (found)
            {
                returnData.PowerAtMin_dBm = powerAtTurningPoint_dBm;
                returnData.VoltageAtMin = closestVoltageToTarget;
            }
            else
            {
                returnData.PowerAtMin_dBm = double.NaN;
                returnData.VoltageAtMin = double.NaN;
            }

            // Locate the closest MAX to 0v
            closestIndexToTarget = int.MinValue;
            closestVoltageToTarget = double.MaxValue;
            powerAtTurningPoint_dBm = double.MinValue;
            found = false;
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.PeakData)
            {
                if (Math.Abs(dataPoint.Voltage - mzAnalysisRules.targetLocation) < Math.Abs(closestVoltageToTarget - mzAnalysisRules.targetLocation))
                {
                    closestVoltageToTarget = dataPoint.Voltage;
                    closestIndexToTarget = dataPoint.Index;
                    powerAtTurningPoint_dBm = dataPoint.Power_dBm;
                    found = true;
                }
            }
            // Add data relating to this max to the results as "MAX1"
            if (found)
            {
                returnData.PowerAtMax1_dBm = powerAtTurningPoint_dBm;
                returnData.VoltageAtMax1 = closestVoltageToTarget;
                // Derived data
                returnData.ExtinctionRatio1_dB = returnData.PowerAtMax1_dBm - returnData.PowerAtMin_dBm;
                returnData.Vpi1 = Math.Abs(returnData.VoltageAtMax1 - returnData.VoltageAtMin);
            }
            else
            {
                returnData.PowerAtMax1_dBm = double.NaN;
                returnData.VoltageAtMax1 = double.NaN;
                returnData.ExtinctionRatio1_dB = double.NaN;
                returnData.Vpi1 = double.NaN;
            }

            // Locate the second closest MZ to 0v
            int secondClosestIndexToZero = int.MinValue;
            double secondClosestVoltageToZero = double.MaxValue;
            powerAtTurningPoint_dBm = double.MinValue;
            found = false;
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.PeakData)
            {
                if (dataPoint.Index == closestIndexToTarget)
                    continue;

                if (Math.Abs(dataPoint.Voltage - mzAnalysisRules.targetLocation) < Math.Abs(secondClosestVoltageToZero - mzAnalysisRules.targetLocation))
                {
                    secondClosestVoltageToZero = dataPoint.Voltage;
                    secondClosestIndexToZero = dataPoint.Index;
                    powerAtTurningPoint_dBm = dataPoint.Power_dBm;
                    found = true;
                }
            }

            // Add data relating to this max to the results as "MAX2"
            if (found)
            {
                returnData.PowerAtMax2_dBm = powerAtTurningPoint_dBm;
                returnData.VoltageAtMax2 = secondClosestVoltageToZero;
                // Derived data
                returnData.ExtinctionRatio2_dB = returnData.PowerAtMax2_dBm - returnData.PowerAtMin_dBm;
                returnData.Vpi2 = Math.Abs(returnData.VoltageAtMin - returnData.VoltageAtMax2);

                // Overall Vmod
                returnData.Vmod = returnData.VoltageAtMax1 - returnData.VoltageAtMax2;
            }
            else
            {
                returnData.PowerAtMax2_dBm = double.NaN;
                returnData.VoltageAtMax2 = double.NaN;
                returnData.ExtinctionRatio2_dB = double.NaN;
                returnData.Vpi2 = double.NaN;
                returnData.Vmod = double.NaN;
            }

            return returnData;
        }



        #region OuterMZAnalysis

        /// <summary>
        /// Analyses data at the closest quad point to 0v.
        /// Quad point is defined as the point of inflection.        /// </summary>
        /// <param name="leftArmVoltageArray"></param>
        /// <param name="rightArmVoltageArray"></param>
        /// <param name="leftArmPowerArray"></param>
        /// <param name="rightArmPowerArray"></param>
        /// <param name="mzAnalysisRules"></param>
        /// <returns></returns>
        public static MZAnalysis DqpskOuterMzAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, MzAnalysisRules mzAnalysisRules)
        {
            // Stitch the single ended sweeps together
            double[] bothArmsVoltage = JoinSeData(leftArmVoltageArray, rightArmVoltageArray, true);
            double[] bothArmsPower = JoinSeData(leftArmPowerArray, rightArmPowerArray, false);

            return DqpskOuterMzAnalysis(bothArmsVoltage, bothArmsPower, mzAnalysisRules);
        }


        /// <summary>
        /// Analyses data at the closest quad point to 0v.
        /// Quad point is defined as the point of inflection.
        /// </summary>
        /// <param name="leftArmVoltageArray"></param>
        /// <param name="rightArmVoltageArray"></param>
        /// <param name="leftArmPowerArray"></param>
        /// <param name="rightArmPowerArray"></param>
        /// <param name="numberOfPointsForFit">numberOfPointsForFit</param>
        /// <param name="power_in_dBm">Is power_in_dBm ?</param>
        /// <param name="negativeSlope">Is the quadrature point to be found on a negative slope ?</param>
        /// <returns>MZAnalysis data</returns>
        public static MZAnalysis DqpskOuterMzAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, int numberOfPointsForFit, bool power_in_dBm, bool negativeSlope)
        {
            // Stitch the single ended sweeps together
            double[] bothArmsVoltage = JoinSeData(leftArmVoltageArray, rightArmVoltageArray, true);
            double[] bothArmsPower = JoinSeData(leftArmPowerArray, rightArmPowerArray, false);

            return DqpskOuterMzAnalysis(bothArmsVoltage, bothArmsPower, numberOfPointsForFit, power_in_dBm, negativeSlope);
        }


        /// <summary>
        /// Analyses data at the closest quad point to 0v.
        /// Quad point is defined as the point of inflection.
        /// </summary>
        /// <param name="voltageArray">voltageArray</param>
        /// <param name="powerArray">powerArray</param>
        /// <param name="numberOfPointsForFit">numberOfPointsForFit</param>
        /// <param name="power_in_dBm">Is power_in_dBm ?</param>
        /// <param name="negativeSlope">Is the quadrature point to be found on a negative slope ?</param>
        /// <returns>MZAnalysis data</returns>
        public static MZAnalysis DqpskOuterMzAnalysis(double[] voltageArray, double[] powerArray, int numberOfPointsForFit, bool power_in_dBm, bool negativeSlope)
        {
            MzAnalysisRules mzAnalysisRules = new MzAnalysisRules();
            mzAnalysisRules.power_in_dBm = power_in_dBm;
            mzAnalysisRules.numberOfPointsForTurningPointFit = numberOfPointsForFit;
            mzAnalysisRules.orderOfPolyFit = 20;
            mzAnalysisRules.smoothingMethod = SmoothingMethod.PolyFit;
            mzAnalysisRules.quadPointLocationMethod = QuadPointLocationMethod.Inflection;
            if (negativeSlope)
            {
                mzAnalysisRules.quadPointSlope = QuadPointSlope.NegativeSlope;
            }
            else
            {
                mzAnalysisRules.quadPointSlope = QuadPointSlope.PositiveSlope;
            }

            return DqpskOuterMzAnalysis(voltageArray, powerArray, mzAnalysisRules);
        }


        /// <summary>
        /// Locate quadrature point as defined by the MzAnalysisRules
        /// </summary>
        /// <param name="voltageArray">voltageArray</param>
        /// <param name="powerArray">powerArray</param>
        /// <param name="mzAnalysisRules">Rules to specify which on the ever-changing methods we should apply</param>
        /// <returns></returns>
        public static MZAnalysis DqpskOuterMzAnalysis(double[] voltageArray, double[] powerArray, MzAnalysisRules mzAnalysisRules)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (mzAnalysisRules.power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }

            Alg_MZLVMinMax.LVMinMax turningPoints;
            if (mzAnalysisRules.smoothingMethod == SmoothingMethod.PolyFit)
            {
                turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray_dBm, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.orderOfPolyFit, 0, true);    // power is now in dBm
            }
            else
            {
                turningPoints = Alg_MZLVMinMax.MZLVMinMax_Smoothed(voltageArray, powerArray_dBm, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.numberOfPointsForSmoothing, true);    // power is now in dBm
            }

            // The sweep may not have captured turning points at the very end of the range
            try
            {
                turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turningPoints);
            }
            catch (System.InvalidOperationException)
            {
                // Ignore errors where the turning point is at the end point
            }          

            // Closest quad point to 0v on appropriate slope
            double bestMatchingQuadPoint = double.MaxValue;
            double bestQuadPointER = double.MinValue;
            bool foundQuadPoint = false;

            Alg_MZLVMinMax.DataPoint selectedMinPoint = new Alg_MZLVMinMax.DataPoint();
            Alg_MZLVMinMax.DataPoint selectedMaxPoint = new Alg_MZLVMinMax.DataPoint();

            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                double closestMaxDistance = double.MaxValue;
                Alg_MZLVMinMax.DataPoint closestMaxPoint = new Alg_MZLVMinMax.DataPoint();
                closestMaxPoint.Voltage = double.NaN;
                closestMaxPoint.Power_dBm = double.NaN;
                closestMaxPoint.Index = int.MaxValue;

                // If we can use either quad point, then both must be analysed.
                if (mzAnalysisRules.quadPointSlope == QuadPointSlope.EitherSlope)
                {
                    MzAnalysisRules localRules = mzAnalysisRules;
                    localRules.quadPointSlope = QuadPointSlope.NegativeSlope;
                    AnalyseNeighbouringMaxPoint(voltageArray, powerArray_dBm, minPoint, localRules, turningPoints,
                        ref bestMatchingQuadPoint, ref bestQuadPointER, ref foundQuadPoint, ref selectedMinPoint, ref selectedMaxPoint, ref closestMaxDistance, ref closestMaxPoint);

                    localRules.quadPointSlope = QuadPointSlope.PositiveSlope;
                    closestMaxDistance = double.MaxValue;
                    AnalyseNeighbouringMaxPoint(voltageArray, powerArray_dBm, minPoint, localRules, turningPoints,
                        ref bestMatchingQuadPoint, ref bestQuadPointER, ref foundQuadPoint, ref selectedMinPoint, ref selectedMaxPoint, ref closestMaxDistance, ref closestMaxPoint);
                }
                else
                {
                    AnalyseNeighbouringMaxPoint(voltageArray, powerArray_dBm, minPoint, mzAnalysisRules, turningPoints,
                        ref bestMatchingQuadPoint, ref bestQuadPointER, ref foundQuadPoint, ref selectedMinPoint, ref selectedMaxPoint, ref closestMaxDistance, ref closestMaxPoint);
                }
            }

            MZAnalysis returnData = PopulateMzAnalysisData(foundQuadPoint, true, bestMatchingQuadPoint, selectedMinPoint, selectedMaxPoint);
            return returnData;
        }


        /// <summary>
        /// Search for quadrature points between a min and max power point
        /// </summary>
        /// <param name="voltageArray"></param>
        /// <param name="powerArray_dBm"></param>
        /// <param name="minPoint"></param>
        /// <param name="mzAnalysisRules"></param>
        /// <param name="turningPoints"></param>
        /// <param name="bestMatchingQuadPoint"></param>
        /// <param name="bestQuadPointER"></param>
        /// <param name="foundQuadPoint"></param>
        /// <param name="selectedMinPoint"></param>
        /// <param name="selectedMaxPoint"></param>
        /// <param name="closestMaxDistance"></param>
        /// <param name="closestMaxPoint"></param>
        private static void AnalyseNeighbouringMaxPoint(double[] voltageArray, double[] powerArray_dBm, Alg_MZLVMinMax.DataPoint minPoint, MzAnalysisRules mzAnalysisRules, Alg_MZLVMinMax.LVMinMax turningPoints, ref double bestMatchingQuadPoint, ref double bestQuadPointER, ref bool foundQuadPoint, ref Alg_MZLVMinMax.DataPoint selectedMinPoint, ref Alg_MZLVMinMax.DataPoint selectedMaxPoint, ref double closestMaxDistance, ref Alg_MZLVMinMax.DataPoint closestMaxPoint)
        {
            bool foundMax = false;

            // Locate the closest MAX to this MIN
            foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
            {
                // Negative slope means that the Max occurs at a lower voltage than the Min
                if (mzAnalysisRules.quadPointSlope == QuadPointSlope.NegativeSlope && maxPoint.Voltage > minPoint.Voltage)
                    continue;
                // Positive slope means that the Max occurs at a higher voltage than the Min
                if (mzAnalysisRules.quadPointSlope == QuadPointSlope.PositiveSlope && maxPoint.Voltage < minPoint.Voltage)
                    continue;

                // Locate the closest max point to the selected min point
                if (Math.Abs(minPoint.Voltage - maxPoint.Voltage) < closestMaxDistance)
                {
                    closestMaxPoint = maxPoint;
                    closestMaxDistance = Math.Abs(minPoint.Voltage - maxPoint.Voltage);
                    foundMax = true;
                }
            }

            //
            // Locate the quadrature point if we have a valid MAX and MIN
            //
            double quadPoint = double.NaN;
            if (foundMax)
            {
                switch (mzAnalysisRules.quadPointLocationMethod)
                {
                    case QuadPointLocationMethod.HalfPower:
                        double[] xSubset = Alg_ArrayFunctions.ExtractSubArray(voltageArray, Math.Min(closestMaxPoint.Index, minPoint.Index), Math.Max(closestMaxPoint.Index, minPoint.Index));
                        double[] ySubset = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, Math.Min(closestMaxPoint.Index, minPoint.Index), Math.Max(closestMaxPoint.Index, minPoint.Index));
                        try
                        {
                            quadPoint = XatYAlgorithm.Calculate(xSubset, ySubset, closestMaxPoint.Power_dBm - 3);
                        }
                        catch (AlgorithmException)
                        {
                            // No 3dB point found within this subset
                        }
                        break;

                    case QuadPointLocationMethod.HalfBias:
                        // Use this for quad point located using bias voltage instead of the 3dB point or inflection
                        quadPoint = (minPoint.Voltage + closestMaxPoint.Voltage) / 2;
                        break;

                    case QuadPointLocationMethod.Inflection:
                        // Use this for the quad point at the point of inflection

                        double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                        Alg_FindPointOfInflection.PointOfInflection quadPoint_byInflection = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, Math.Min(minPoint.Index, closestMaxPoint.Index), Math.Max(minPoint.Index, closestMaxPoint.Index));
                        quadPoint = quadPoint_byInflection.XValue;
                        break;

                    default:
                        throw new AlgorithmException("Unhandled quad point location method");
                }
            }

            //
            // Determine whether this quadrature point is the best match so far
            //
            if (!quadPoint.Equals(double.NaN))
            {
                switch (mzAnalysisRules.quadPointSelectionCriteria)
                {
                    case QuadPointSelectionCriteria.ClosestToZero:
                        // Now check whether this point has the closest quad point to 0
                        if (Math.Abs(quadPoint) < Math.Abs(bestMatchingQuadPoint))
                        {
                            foundQuadPoint = true;
                            selectedMinPoint = minPoint;
                            selectedMaxPoint = closestMaxPoint;
                            bestMatchingQuadPoint = quadPoint;
                        }
                        break;
                    case QuadPointSelectionCriteria.ClosestToTarget:
                        // Now check whether this point has the closest quad point to our target
                        if (Math.Abs(quadPoint - mzAnalysisRules.targetLocation) < Math.Abs(bestMatchingQuadPoint - mzAnalysisRules.targetLocation))
                        {
                            foundQuadPoint = true;
                            selectedMinPoint = minPoint;
                            selectedMaxPoint = closestMaxPoint;
                            bestMatchingQuadPoint = quadPoint;
                        }
                        break;
                    case QuadPointSelectionCriteria.HighestER:
                        double erAtThisQuadPoint = closestMaxPoint.Power_dBm - minPoint.Power_dBm;
                        if (erAtThisQuadPoint > bestQuadPointER)
                        {
                            foundQuadPoint = true;
                            selectedMinPoint = minPoint;
                            selectedMaxPoint = closestMaxPoint;
                            bestQuadPointER = erAtThisQuadPoint;
                            bestMatchingQuadPoint = quadPoint;
                        }
                        break;
                    default:
                        throw new AlgorithmException("Unhandled quad point selection method");
                }
            }
        }

        //##############################################################

        /// <summary>
        /// Analyses all slopes of the data set
        /// </summary>
        /// <param name="leftArmCurrentArray"></param>
        /// <param name="rightArmCurrentArray"></param>
        /// <param name="leftArmPowerArray"></param>
        /// <param name="rightArmPowerArray"></param>
        /// <param name="mzAnalysisRules"></param>
        /// <returns></returns>
        public static MZAnalysis[] DqpskOuterMzAnalysisOfAllSlopes(double[] leftArmCurrentArray, double[] rightArmCurrentArray, double[] leftArmPowerArray, double[] rightArmPowerArray, MzAnalysisRules mzAnalysisRules)
        {
            // Stitch the single ended sweeps together
            double[] bothArmsCurrent = JoinSeData(leftArmCurrentArray, rightArmCurrentArray, true);
            double[] bothArmsPower = JoinSeData(leftArmPowerArray, rightArmPowerArray, false);

            return DqpskOuterMzAnalysisOfAllSlopes(bothArmsCurrent, bothArmsPower, mzAnalysisRules);
        }

        /// <summary>
        /// Analyses all slopes of the data set
        /// </summary>
        /// <param name="currentArray">Current array in DESCENDING order</param>
        /// <param name="powerArray"></param>
        /// <param name="mzAnalysisRules"></param>
        /// <returns></returns>
        public static MZAnalysis[] DqpskOuterMzAnalysisOfAllSlopes(double[] currentArray, double[] powerArray, MzAnalysisRules mzAnalysisRules)
        {
            #region PRECONDITIONS
            if (currentArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + currentArray.Length + " and power data contains " + powerArray.Length);
            }
            if (currentArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (currentArray[0] < currentArray[currentArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in descending order");
            }
            #endregion

            // Change the sign of the data.
            currentArray = Alg_ArrayFunctions.MultiplyEachArrayElement(currentArray, -1);
            // Also reverse the power array to get L and R data the right way around
            //powerArray = Alg_ArrayFunctions.ReverseArray(powerArray);

            #region pre-process data to locate turning points
            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (mzAnalysisRules.power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }

            Alg_MZLVMinMax.LVMinMax turningPoints;
            if (mzAnalysisRules.smoothingMethod == SmoothingMethod.PolyFit)
            {
                turningPoints = Alg_MZLVMinMax.MZLVMinMax(currentArray, powerArray_dBm, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.orderOfPolyFit, 0, true);    // power is now in dBm
            }
            else
            {
                turningPoints = Alg_MZLVMinMax.MZLVMinMax_Smoothed(currentArray, powerArray_dBm, mzAnalysisRules.numberOfPointsForTurningPointFit, mzAnalysisRules.numberOfPointsForSmoothing, true);    // power is now in dBm
            }

            // The sweep may not have captured turning points at the very end of the range
            try
            {
                turningPoints = addArrayEndPoints(currentArray, powerArray_dBm, turningPoints);
            }
            catch (System.InvalidOperationException)
            {
                // Ignore errors where the turning point is at the end point
            }
            #endregion

            List<MZAnalysis> dataForAllSlopes = new List<MZAnalysis>();
            MZAnalysis dataForSingleSlope;

            bool foundQuadPoint = false;
            double bestMatchingQuadPoint = double.MaxValue;
            double bestQuadPointER = double.MinValue;
            double closestMaxDistance = double.MaxValue;
            Alg_MZLVMinMax.DataPoint selectedMinPoint = new Alg_MZLVMinMax.DataPoint();
            Alg_MZLVMinMax.DataPoint selectedMaxPoint = new Alg_MZLVMinMax.DataPoint();
            
            MzAnalysisRules localRules = mzAnalysisRules;

            // Analyse the peaks on either side of each min point
            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                Alg_MZLVMinMax.DataPoint closestMaxPoint = new Alg_MZLVMinMax.DataPoint();
                closestMaxPoint.Voltage = double.NaN;
                closestMaxPoint.Power_dBm = double.NaN;
                closestMaxPoint.Index = int.MaxValue;

                // Analyse the negative slope
                localRules.quadPointSlope = QuadPointSlope.NegativeSlope;
                // Reset the parameters
                bestMatchingQuadPoint = double.MaxValue;
                bestQuadPointER = double.MinValue;
                foundQuadPoint = false;
                closestMaxDistance = double.MaxValue;
                closestMaxPoint = new Alg_MZLVMinMax.DataPoint();
                // Analyse
                AnalyseNeighbouringMaxPoint(currentArray, powerArray_dBm, minPoint, localRules, turningPoints,
                    ref bestMatchingQuadPoint, ref bestQuadPointER, ref foundQuadPoint, ref selectedMinPoint, ref selectedMaxPoint, ref closestMaxDistance, ref closestMaxPoint);
                // Add results to output array if valid
                if (foundQuadPoint)
                {
                    dataForSingleSlope = PopulateMzAnalysisData(foundQuadPoint, true, bestMatchingQuadPoint, selectedMinPoint, selectedMaxPoint);
                    dataForAllSlopes.Add(dataForSingleSlope);
                }

                // Analyse the positive slope
                localRules.quadPointSlope = QuadPointSlope.PositiveSlope;
                // Reset the parameters
                bestMatchingQuadPoint = double.MaxValue;
                bestQuadPointER = double.MinValue;
                foundQuadPoint = false;
                closestMaxDistance = double.MaxValue;
                closestMaxPoint = new Alg_MZLVMinMax.DataPoint(); 
                // Analyse
                AnalyseNeighbouringMaxPoint(currentArray, powerArray_dBm, minPoint, localRules, turningPoints,
                    ref bestMatchingQuadPoint, ref bestQuadPointER, ref foundQuadPoint, ref selectedMinPoint, ref selectedMaxPoint, ref closestMaxDistance, ref closestMaxPoint);
                // Add results to output array if valid
                if (foundQuadPoint)
                {
                    dataForSingleSlope = PopulateMzAnalysisData(foundQuadPoint, true, bestMatchingQuadPoint, selectedMinPoint, selectedMaxPoint);
                    dataForAllSlopes.Add(dataForSingleSlope);
                }
            }

            return dataForAllSlopes.ToArray();
        }

        /// <summary>
        /// Creates a MZAnalysis structure using the data provided
        /// </summary>
        /// <param name="validQuadPoint"></param>
        /// <param name="quadraturePoint_V"></param>
        /// <param name="minPoint"></param>
        /// <param name="maxPoint"></param>
        /// <returns>A populated MZAnalysis structure</returns>
        private static MZAnalysis PopulateMzAnalysisData(bool validQuadPoint, bool currentData, double quadraturePoint_V, Alg_MZLVMinMax.DataPoint minPoint, Alg_MZLVMinMax.DataPoint maxPoint)
        {
            MZAnalysis dataForSingleSlope;
            //int signCorrectionFactor = currentData ? -1 : 1;
            int signCorrectionFactor = 1;

            if (validQuadPoint)
            {
                dataForSingleSlope.ExtinctionRatio_dB = maxPoint.Power_dBm - minPoint.Power_dBm;
                dataForSingleSlope.PowerAtMax_dBm = maxPoint.Power_dBm;
                dataForSingleSlope.PowerAtMin_dBm = minPoint.Power_dBm;
                dataForSingleSlope.VImb = quadraturePoint_V * signCorrectionFactor;
                dataForSingleSlope.VoltageAtMax = maxPoint.Voltage * signCorrectionFactor;
                dataForSingleSlope.VoltageAtMin = minPoint.Voltage * signCorrectionFactor;
                dataForSingleSlope.Vpi = Math.Abs(minPoint.Voltage - maxPoint.Voltage);
                dataForSingleSlope.VQuad = quadraturePoint_V * signCorrectionFactor;
            }
            else
            {
                dataForSingleSlope.ExtinctionRatio_dB = double.NaN;
                dataForSingleSlope.PowerAtMax_dBm = double.NaN;
                dataForSingleSlope.PowerAtMin_dBm = double.NaN;
                dataForSingleSlope.VImb = double.NaN;
                dataForSingleSlope.VoltageAtMax = double.NaN;
                dataForSingleSlope.VoltageAtMin = double.NaN;
                dataForSingleSlope.Vpi = double.NaN;
                dataForSingleSlope.VQuad = double.NaN;
            }
            return dataForSingleSlope;
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZLVMinMax_TEST.cs
//
// Author: Mark Fullalove

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Test harness for Alg_FindPointOfInflection
    /// </summary>
    [TestFixture]
    public class Alg_MZLVMinMax_TEST
    {
        [Test]
        public void Valid_MinMax()
        {
            double[] xData = xData101();
            double[] yData = yData_A();

            Alg_MZLVMinMax.LVMinMax dataOut = Alg_MZLVMinMax.MZLVMinMax(xData, yData, 5, true);

            #region ValidMinMaxChecks
            // 2 peaks
            Assert.AreEqual(2, dataOut.PeakData.Length);
            // Peak location
            Assert.AreEqual(17, dataOut.PeakData[0].Index);
            Assert.AreEqual(61, dataOut.PeakData[1].Index);
            // Peak Value
            // #1
            double deltaPower = yData[dataOut.PeakData[0].Index] - dataOut.PeakData[0].Power_dBm;
            Assert.IsTrue(deltaPower < 0.001);
            double deltaV = xData[dataOut.PeakData[0].Index] - dataOut.PeakData[0].Voltage;
            Assert.IsTrue(deltaV < 0.01);
            // #2
            deltaPower = yData[dataOut.PeakData[1].Index] - dataOut.PeakData[1].Power_dBm;
            Assert.IsTrue(deltaPower < 0.001);
            deltaV = xData[dataOut.PeakData[1].Index] - dataOut.PeakData[1].Voltage;
            Assert.IsTrue(deltaV < 0.01);

            // 2 troughs
            Assert.AreEqual(2, dataOut.ValleyData.Length);
            // Valley location
            Assert.AreEqual(39, dataOut.ValleyData[0].Index);
            Assert.AreEqual(83, dataOut.ValleyData[1].Index);
            // Valley Value
            // #1
            deltaPower = yData[dataOut.ValleyData[0].Index] - dataOut.ValleyData[0].Power_dBm;
            Assert.IsTrue(deltaPower < 0.1);
            deltaV = xData[dataOut.ValleyData[0].Index] - dataOut.ValleyData[0].Voltage;
            Assert.IsTrue(deltaV < 0.01);
            // #2
            deltaPower = yData[dataOut.ValleyData[1].Index] - dataOut.ValleyData[1].Power_dBm;
            Assert.IsTrue(deltaPower < 0.1);
            deltaV = xData[dataOut.ValleyData[1].Index] - dataOut.ValleyData[1].Voltage;
            Assert.IsTrue(deltaV < 0.01);
            #endregion
        }


        #region testData
        /// <summary>
        /// Integers between -50 to +50
        /// </summary>
        /// <returns>A list of integers</returns>
        private double[] xData101()
        {
            double[] xData = new double[101];
            for (int i = 0; i < 101; i++)
            {
                xData[i] = i-50;
            }
            return xData;
        }

        /// <summary>
        /// A set of data whose differential has a positive going turning point
        ///   101 points , sinusoidal, log scale
        ///   2 max, 2 min
        /// </summary>
        private double[] yData_A()
        {
            double[] yData = new double[101];
            for (int i = 0; i < 101; i++)
            {
                double powermW = Math.Sin(((double)i-50) / 7);
                double scaledPowerMw = 5 - 5 * powermW;
                yData[i] = Alg_PowConvert_dB.Convert_dBmtomW(5 - (5 * powermW)) * -1;
            }
            return yData;
        }
        #endregion
    }
}
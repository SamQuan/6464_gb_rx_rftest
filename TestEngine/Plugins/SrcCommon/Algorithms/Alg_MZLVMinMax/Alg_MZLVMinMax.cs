// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZLVMinMax.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Algorithm to locate turning points within a set of (X,Y) data.
    /// </summary>
    public sealed class Alg_MZLVMinMax
    {
        /// <summary>
        /// Empty private constructor.
        /// </summary>
        private Alg_MZLVMinMax()
        {
        }

        /// <summary>
        /// Locate maxima and minima with a dataset by looking for a turning point using a 10 point second order fit.
        /// </summary>
        /// <param name="voltageArray_V">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">Flag - true if power is in dBm, false if linear units</param>
        /// <returns>A structure containing data for all the peaks and troughs within the data</returns>
        public static LVMinMax MZLVMinMax(double[] voltageArray_V, double[] powerArray, bool power_in_dBm)
        {
            return MZLVMinMax(voltageArray_V, powerArray, 10, power_in_dBm);
        }

        /// <summary>
        /// Locate maxima and minima with a dataset by looking for a turning point using a second order fit.
        /// </summary>
        /// <param name="voltageArray_V">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="pointsForTurningPointFit">Apply second order fit to this number of points around each potential turning point.</param>
        /// <param name="power_in_dBm">Flag - true if power is in dBm, false if linear units</param>
        /// <returns>A structure containing data for all the peaks and troughs within the data</returns>
        public static LVMinMax MZLVMinMax(double[] voltageArray_V, double[] powerArray, int pointsForTurningPointFit, bool power_in_dBm)
        {
            return MZLVMinMax(voltageArray_V, powerArray, pointsForTurningPointFit, 20, 0, power_in_dBm);
        }

        /// <summary>
        /// Locate maxima and minima with a dataset by looking for a turning point using a second order fit.
        /// </summary>
        /// <param name="voltageArray_V"></param>
        /// <param name="powerArray"></param>
        /// <param name="pointsForTurningPointFit"></param>
        /// <param name="orderOfPolyFit"></param>
        /// <param name="pointsForPeakDetection"></param>
        /// <param name="power_in_dBm"></param>
        /// <returns></returns>
        public static LVMinMax MZLVMinMax(double[] voltageArray_V, double[] powerArray, int pointsForTurningPointFit, int orderOfPolyFit, int pointsForPeakDetection, bool power_in_dBm)
        {
            int arrayLength = voltageArray_V.Length;
            // PRECONDITIONS
            if (voltageArray_V.Length != powerArray.Length)
            {
                throw new AlgorithmException("Mismatched array sizes.");
            }

            if (arrayLength == 0)
            {
                throw new AlgorithmException("Empty data arrays");
            }

            if (pointsForTurningPointFit < 1)
            {
                throw new AlgorithmException("Number of points to fit < 1");
            }


            // Convert power to linear units if necessary
            double[] powerArray_mW = null;
            if (power_in_dBm)
            {
                powerArray_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray);
            }
            else
            {                
                powerArray_mW = (double[])powerArray.Clone();
            }

            // Apply a high order fit to help locate any potential turning points
            // A low wavelength MZ sweep may have up to 6 to 10 minima. A 20th order fit should be safe.
            PolyFit polyFit = Alg_PolyFit.PolynomialFit(voltageArray_V, powerArray_mW, orderOfPolyFit);

            // Locate any potential peaks and valleys
            ArrayList peakIndeces;
            ArrayList valleyIndeces;
            VerifyPeaksAndTroughs(voltageArray_V, powerArray_mW, polyFit.FittedYArray, pointsForTurningPointFit, pointsForPeakDetection, out peakIndeces, out valleyIndeces);

            // Create and populate the return structure
            LVMinMax minMaxData = new LVMinMax();
            minMaxData.PeakData = (DataPoint[])peakIndeces.ToArray(typeof(DataPoint));
            minMaxData.ValleyData = (DataPoint[])valleyIndeces.ToArray(typeof(DataPoint));

            return minMaxData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="voltageArray_V"></param>
        /// <param name="powerArray"></param>
        /// <param name="pointsForTurningPointFit"></param>
        /// <param name="pointsForSmoothing"></param>
        /// <param name="power_in_dBm"></param>
        /// <returns></returns>
        public static LVMinMax MZLVMinMax_Smoothed(double[] voltageArray_V, double[] powerArray, int pointsForTurningPointFit, int pointsForSmoothing, bool power_in_dBm)
        {
            return MZLVMinMax_Smoothed(voltageArray_V, powerArray, pointsForTurningPointFit, 0, pointsForSmoothing, power_in_dBm);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="voltageArray_V"></param>
        /// <param name="powerArray"></param>
        /// <param name="pointsForTurningPointFit"></param>
        /// <param name="pointsForPeakDetection"></param>
        /// <param name="pointsForSmoothing"></param>
        /// <param name="power_in_dBm"></param>
        /// <returns></returns>
        public static LVMinMax MZLVMinMax_Smoothed(double[] voltageArray_V, double[] powerArray, int pointsForTurningPointFit, int pointsForPeakDetection, int pointsForSmoothing, bool power_in_dBm)
        {
            int arrayLength = voltageArray_V.Length;
            // PRECONDITIONS
            if (voltageArray_V.Length != powerArray.Length)
            {
                throw new AlgorithmException("Mismatched array sizes.");
            }

            if (arrayLength == 0)
            {
                throw new AlgorithmException("Empty data arrays");
            }


            // Convert power to linear units if necessary
            double[] powerArray_mW = null;
            if (power_in_dBm)
            {
                powerArray_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray);
            }
            else
            {
                powerArray_mW = (double[])powerArray.Clone();
            }

            // Smooth the data before locating potential turning points
            double[] smoothedData = Alg_Smoothing.PascalTriangleSmooth(powerArray_mW, pointsForSmoothing);

            // Locate any potential peaks and valleys and verify
            ArrayList peakIndeces;
            ArrayList valleyIndeces;
            VerifyPeaksAndTroughs(voltageArray_V, powerArray_mW, smoothedData, pointsForTurningPointFit, pointsForPeakDetection, out peakIndeces, out valleyIndeces);

            // Create and populate the return structure
            LVMinMax minMaxData = new LVMinMax();
            minMaxData.PeakData = (DataPoint[])peakIndeces.ToArray(typeof(DataPoint));
            minMaxData.ValleyData = (DataPoint[])valleyIndeces.ToArray(typeof(DataPoint));

            return minMaxData;
        }




        private static void VerifyPeaksAndTroughs(double[] voltageArray_V, double[] powerArray_mW, double[] processedPowerArray_mW, int pointsForTurningPointFit, out ArrayList peakIndeces, out ArrayList valleyIndeces)
        {
            VerifyPeaksAndTroughs(voltageArray_V, powerArray_mW, processedPowerArray_mW, pointsForTurningPointFit, 0, out peakIndeces, out valleyIndeces);
        }

        private static void VerifyPeaksAndTroughs(double[] voltageArray_V, double[] powerArray_mW, double[] processedPowerArray_mW, int pointsForTurningPointFit, int pointsForPeakDetection, out ArrayList peakIndeces, out ArrayList valleyIndeces)
        {
            int[] potentialPeakIndeces;
            int[] potentialValleyIndeces;
            if (pointsForPeakDetection == 0)
            {
                potentialPeakIndeces = Alg_FindFeature.FindIndexesForAllPeaks(processedPowerArray_mW);
                potentialValleyIndeces = Alg_FindFeature.FindIndexesForAllValleys(processedPowerArray_mW);
            }
            else
            {
                potentialPeakIndeces = Alg_FindFeature.FindIndexesForAllPeaks(processedPowerArray_mW, pointsForPeakDetection);
                potentialValleyIndeces = Alg_FindFeature.FindIndexesForAllValleys(processedPowerArray_mW, pointsForPeakDetection);
            }
            int arrayLength = voltageArray_V.Length;

            // Now verify each potential turning point by performing a second order poly fit
            // to the raw (linear) data in the surrounding area

            // Verify the peaks
            peakIndeces = new ArrayList();
            foreach (int i in potentialPeakIndeces)
            {
                int startIndex = i - pointsForTurningPointFit < 0 ? 0 : i - pointsForTurningPointFit;
                int stopIndex = i + pointsForTurningPointFit >= arrayLength ? arrayLength - 1 : i + pointsForTurningPointFit;
                Alg_FindTurningPointUsing2ndOrderFit.TurningPointData tpData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(voltageArray_V, powerArray_mW, startIndex, stopIndex);
                //  Check that the turning point is within the dataset
                if (tpData.Success && tpData.TurningPointXvalue >= voltageArray_V[0] && tpData.TurningPointXvalue <= voltageArray_V[voltageArray_V.Length - 1])
                {
                    DataPoint turningPoint = new DataPoint();
                    turningPoint.Index = Alg_ArrayFunctions.FindIndexOfNearestElement(voltageArray_V, tpData.TurningPointXvalue);
                    turningPoint.Voltage = tpData.TurningPointXvalue;
                    turningPoint.Power_dBm = Alg_PowConvert_dB.Convert_mWtodBm(tpData.TurningPointYvalue);
                    // Only add if close to the range of the fit
                    if (turningPoint.Index + pointsForTurningPointFit >= startIndex && turningPoint.Index - pointsForTurningPointFit <= stopIndex)
                        peakIndeces.Add(turningPoint);
                }
            }

            // Verify the valleys
            valleyIndeces = new ArrayList();
            foreach (int i in potentialValleyIndeces)
            {
                int pointsToUse = pointsForTurningPointFit;    // reset after each attempt
                int startIndex = i - pointsToUse < 0 ? 0 : i - pointsToUse;
                int stopIndex = i + pointsToUse >= arrayLength ? arrayLength - 1 : i + pointsToUse;

                DataPoint turningPoint = new DataPoint();
                Alg_FindTurningPointUsing2ndOrderFit.TurningPointData tpData;

                // Power (in mW) cannot be negative.
                // This has been seen in artificial data with the X spacing too wide for the periodicity.
                // Using fewer points should produce a closer fit to the measured value.
                do
                {
                    tpData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(voltageArray_V, powerArray_mW, startIndex, stopIndex);
                    pointsToUse--;
                    startIndex = i - pointsToUse < 0 ? 0 : i - pointsToUse;
                    stopIndex = i + pointsToUse >= arrayLength ? arrayLength - 1 : i + pointsToUse;
                } while (tpData.Success && tpData.TurningPointYvalue < 0 && pointsToUse > 0);

                // We have a valid fit with a sensible power value.
                // Also check that it's within the dataset
                if (tpData.Success && tpData.TurningPointXvalue >= voltageArray_V[0] && tpData.TurningPointXvalue <= voltageArray_V[voltageArray_V.Length - 1])
                {
                    turningPoint.Index = Alg_ArrayFunctions.FindIndexOfNearestElement(voltageArray_V, tpData.TurningPointXvalue);
                    turningPoint.Voltage = tpData.TurningPointXvalue;
                    turningPoint.Power_dBm = Alg_PowConvert_dB.Convert_mWtodBm(tpData.TurningPointYvalue);
                    // Only add if close to the range of the fit
                    if (turningPoint.Index + pointsForTurningPointFit >= startIndex && turningPoint.Index - pointsForTurningPointFit <= stopIndex)
                        valleyIndeces.Add(turningPoint);
                }
            }
        }

        /// <summary>
        /// Contains data relating to peaks and troughs found within a dataset.
        /// </summary>
        public struct LVMinMax
        {
            /// <summary>
            /// The data for all peaks.
            /// </summary>
            public DataPoint[] PeakData;
            /// <summary>
            /// The data for all valleys.
            /// </summary>
            public DataPoint[] ValleyData;
            /// <summary>
            /// Find the voltages of all peaks
            /// </summary>
            /// <returns>Array voltage</returns>
            public double[] VoltagesForAllPeaks()
            {
                ArrayList peakVoltages = new ArrayList();
                foreach ( DataPoint point in PeakData )
                {
                    peakVoltages.Add(point.Voltage);
                }
                return (double[])peakVoltages.ToArray(typeof(double));
            }
            /// <summary>
            /// Find the voltages of all valleys
            /// </summary>
            /// <returns>Array voltage</returns>
            public double[] VoltagesForAllValleys()
            {
                ArrayList valleyVoltages = new ArrayList();
                foreach (DataPoint point in ValleyData)
                {
                    valleyVoltages.Add(point.Voltage);
                }
                return (double[])valleyVoltages.ToArray(typeof(double));
            }
        }

        /// <summary>
        /// Contains data relating to a single point of interest.
        /// </summary>
        public struct DataPoint
        {
            /// <summary>
            /// The index of the point within the original array.
            /// </summary>
            public int Index;
            /// <summary>
            /// The power value at this point.
            /// </summary>
            public double Power_dBm;
            /// <summary>
            /// The voltage value at this point.
            /// </summary>
            public double Voltage;
        }
    }
}

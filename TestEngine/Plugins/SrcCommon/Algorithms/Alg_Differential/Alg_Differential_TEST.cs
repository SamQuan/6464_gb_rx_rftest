// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_Differential_TEST.cs
//
// Author: Mark Fullalove
// Coverage : nCover reports 100% (excluding private ctor)

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
	/// Test harness for Alg_Differential
	/// </summary>
	[TestFixture]
    public class Alg_Differential_TEST
    {
        [Test]
        public void Valid_Diff()
        {
            double[] xData = { 0, 1, 2, 3, 4, 5, 6, 7 };
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
            Assert.AreEqual(-2, differential[5]);
        }

        [Test]
        public void Valid_SmoothedDiff()
        {
            double[] xData = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3, 4, 5,  6,  7, 11, 15, 17, 22, 24, 30 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData,3);
            Assert.AreEqual(-0.5357, Math.Round( differential[5],4));
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void MismatchXY_Diff()
        {
            double[] xData = { 0, 1, 2, 3, 4, 5, 6 };
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void MismatchXY_SmoothedDiff()
        {
            double[] xData = { 0, 1, 2, 3, 4, 5, 6 };
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoX_Diff()
        {
            double[] xData = new double[0];
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoY_Diff()
        {
            double[] yData = new double[0];
            double[] xData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoX_SmoothedDiff()
        {
            double[] xData = new double[0];
            double[] yData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NoY_SmoothedDiff()
        {
            double[] yData = new double[0];
            double[] xData = { 1, 3, 5, 7, 9, 7, 5, 3 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData,9);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void InsufficientData_Diff()
        {
            double[] xData = { 1, 2};
            double[] yData = { 1, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void InsufficientData_SmoothedDiff_Default3pt()
        {
            double[] xData = { 1, 2 };
            double[] yData = { 1, 3 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        public void MinimalDataPoints_Diff()
        {
            double[] xData = { 1, 2, 3 };
            double[] yData = { 1, 3, 4 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        public void MinimalPoints_SmoothedDiff_Default3Pt()
        {
            double[] xData = { 1, 2, 3, 4, 5, 6 };
            double[] yData = { 1, 3, 5, 5, 4, 1 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }


        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void InsufficientData_SmoothedDiff_5pt()
        {
            double[] xData = { 1, 2, 3, 4, 5, 6 };
            double[] yData = { 1, 3, 2, 3, 4, 5 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData, 5);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void NegativeSmoothing_SmoothedDiff()
        {
            double[] xData = { 1, 2, 3, 4, 5, 6 };
            double[] yData = { 1, 3, 2, 3, 4, 5 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData, -1);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void EqualX_Diff()
        {
            double[] xData = { 1, 2, 3, 6, 5, 6 };
            double[] yData = { 1, 3, 2, 3, 4, 5 };
            double[] differential = Alg_Differential.Differential(xData, yData);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void EqualX_SmoothedDiff()
        {
            double[] xData = { 1, 2, 3, 3, 5, 6 };
            double[] yData = { 1, 3, 2, 3, 4, 5 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
        }

        [Test]
        public void Dx0_SmoothedDiff_NegInf()
        {
            double[] xData = { 3, 2, 1, 0, 1, 2, 3 };
            double[] yData = { 1, 2, 3, 0, -3, -2, -1 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
            Assert.IsTrue(double.IsNegativeInfinity(differential[4]));
        }

        [Test]
        public void Dx0_SmoothedDiff_PosInf()
        {
            double[] xData = { 3, 2, 1, 0, 1, 2, 3 };
            double[] yData = { -3, -2, -1, 0, 1, 2, 3 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
            Assert.IsTrue(double.IsPositiveInfinity(differential[4]));
        }

        [Test]
        public void Dx0Dy0_SmoothedDiff()
        {
            double[] xData = { 3, 2, 1, 0, 1, 2, 3 };
            double[] yData = { 3, 2, 1, 0, 1, 2, 3 };
            double[] differential = Alg_Differential.SmoothedDifferential(xData, yData);
            Assert.IsTrue(double.IsNaN(differential[4]));
        }
    }
}

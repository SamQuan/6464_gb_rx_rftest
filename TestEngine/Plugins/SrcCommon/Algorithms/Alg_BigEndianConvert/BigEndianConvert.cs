// [Copyright]
//
// Bookham Test Engine Library
// Big-Endian conversion algorithms
//
// BigEndianConvert.cs
// 
// Author: Bill Godfrey
// Design: (taken from TSFF driver code and made more generically accessible)

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// BigEndian conversion exception
    /// </summary>
    public class BigEndianConvertException : Exception
    {
        /// <summary>
        /// Constructor taking an error message
        /// </summary>
        /// <param name="message">Message</param>
        public BigEndianConvertException(string message)
            : base(message) { }
    }

    /// <summary>
    /// Big Endian data conversion
    /// </summary>
    public sealed class Alg_BigEndianConvert
    {
        // private constructor
        private Alg_BigEndianConvert() { }

        /// <summary>
        /// Copy an array, reversing the order.
        /// </summary>
        /// <param name="src">Source array.</param>
        /// <param name="srcIndex">Index of src to start copying from.</param>
        /// <param name="dst">Destination array.</param>
        /// <param name="dstIndex">Index of dst to copy into.</param>
        /// <param name="count">Number of bytes.</param>
        private static void reverseArrayCopy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
        {
            for (int bytePos = 0; bytePos < count; ++bytePos)
            {
                dst[dstIndex + bytePos] = src[srcIndex + (count - 1 - bytePos)];
            }
        }

        /// <summary>
        /// Convert an array of bytes in big-endian order into a signed 32 bit integer. Will handle signed
        /// or unsigned values.
        /// </summary>
        /// <param name="beArray">Array of byte.</param>
        /// <param name="signExtend">True if array holds a signed value.</param>
        /// <returns>Converted signed integer.</returns>
        public static Int32 BigEndianBytesToInt32(byte[] beArray, bool signExtend)
        {
            if (BitConverter.IsLittleEndian)
            {
                /* Create an int-sized array of zero bytes. */
                byte[] leArray = BitConverter.GetBytes((Int32)0);

                /* If signExtend and value is -ve (bit7 of MSB is 1),
                 * fill the array with 0xff so the resulting value is
                 * negative. Two's complement requires the the unused MSBs
                 * by filled with zeros for +ve values and 0xff for
                 * -ve values.*/
                if (signExtend && (beArray[0] >= 0x80))
                {
                    for (int i = 0; i < leArray.Length; ++i)
                    {
                        leArray[i] = 0xff;
                    }
                }

                /* Collect array lengths. */
                int leLength = leArray.Length;
                int beLength = beArray.Length;

                /* Reverse array into left hand side of leArray. */
                reverseArrayCopy(beArray, 0, leArray, 0, beLength);

                /* Return LE array converted back to int. */
                return BitConverter.ToInt32(leArray, 0);
            }
            else
            {
                return BitConverter.ToInt32(beArray, 0);
            }
        }

        /// <summary>
        /// Convert a signed 32-bit integer into a big endian array of bytes. Will handle
        /// signed or unsigned values.
        /// </summary>
        /// <param name="value">Signed integer to convert.</param>
        /// <param name="beLength">Target length of array.</param>
        /// <param name="expectNegValue">True if -ve values should be expected.</param>
        /// <returns>Converted array.</returns>
        public static byte[] Int32ToBigEndianBytes(Int32 value, int beLength, bool expectNegValue)
        {
            /* Check if values are in range. */
            if (beLength > 4)
            {
                throw new BigEndianConvertException(
                    "Int32ToBigEndianBytes called with beLength of " + beLength + ". (Max 4)");
            }

            /* n bytes -> 2 ** (8*n) */
            int maxAllowed = (1 << ((beLength * 8)-1)) - 1;
            int minAllowed = -maxAllowed - 1;

            if ((value < minAllowed) || (value > maxAllowed))
            {
                throw new BigEndianConvertException(
                    "Int32ToBigEndianBytes - " + value + " cannot fit in " + beLength + " bytes.");
            }

            if (BitConverter.IsLittleEndian)
            {
                /* Collect bytes. Useful bytes will be to the left, LSB first. */
                byte[] leArray = BitConverter.GetBytes(value);
                int leLength = leArray.Length;

                /* Construct zero filled array for response. */
                byte[] beArray = new byte[beLength];

                /* Reverse left portion of leArray into beArray. */
                reverseArrayCopy(leArray, 0, beArray, 0, beLength);
                return beArray;
            }
            else
            {
                throw new BigEndianConvertException("Big Endian architures not supported.");
            }
        }

        /// <summary>
        /// Convert an array of bytes in big-endian order into a signed 64 bit integer. Will handle signed
        /// or unsigned values.
        /// </summary>
        /// <param name="beArray">Array of byte.</param>
        /// <param name="signExtend">True if array holds a signed value.</param>
        /// <returns>Converted signed integer.</returns>
        public static Int64 BigEndianBytesToInt64(byte[] beArray, bool signExtend)
        {
            if (BitConverter.IsLittleEndian)
            {
                /* Create an int-sized array of zero bytes. */
                byte[] leArray = BitConverter.GetBytes((Int64)0);

                /* If signExtend and value is -ve (bit7 of MSB is 1),
                 * fill the array with 0xff so the resulting value is
                 * negative. Two's complement requires the the unused MSBs
                 * by filled with zeros for +ve values and 0xff for
                 * -ve values.*/
                if (signExtend && (beArray[0] >= 0x80))
                {
                    for (int i = 0; i < leArray.Length; ++i)
                    {
                        leArray[i] = 0xff;
                    }
                }

                /* Collect array lengths. */
                int leLength = leArray.Length;
                int beLength = beArray.Length;

                /* Reverse array into left hand side of leArray. */
                reverseArrayCopy(beArray, 0, leArray, 0, beLength);

                /* Return LE array converted back to int. */
                return BitConverter.ToInt64(leArray, 0);
            }
            else
            {
                return BitConverter.ToInt64(beArray, 0);
            }
        }

        /// <summary>
        /// Convert a signed 64 bit integer into a big endian array of bytes. Will handle
        /// signed or unsigned values.
        /// </summary>
        /// <param name="value">Signed integer to convert.</param>
        /// <param name="beLength">Target length of array.</param>
        /// <param name="expectNegValue">True if -ve values should be expected.</param>
        /// <returns>Converted array.</returns>
        public static byte[] Int64ToBigEndianBytes(Int64 value, int beLength, bool expectNegValue)
        {
            /* Check if values are in range. */
            if (beLength > 8)
            {
                throw new BigEndianConvertException(
                    "Int64ToBigEndianBytes called with beLength of " + beLength + ". (Max 4)");
            }

            /* n bytes -> 2 ** (8*n) */
            int maxAllowed = (1 << ((beLength * 8) - 1)) - 1;
            int minAllowed = -maxAllowed - 1;

            if ((value < minAllowed) || (value > maxAllowed))
            {
                throw new BigEndianConvertException(
                    "Int64ToBigEndianBytes - " + value + " cannot fit in " + beLength + " bytes.");
            }

            if (BitConverter.IsLittleEndian)
            {
                /* Collect bytes. Useful bytes will be to the left, LSB first. */
                byte[] leArray = BitConverter.GetBytes(value);
                int leLength = leArray.Length;

                /* Construct zero filled array for response. */
                byte[] beArray = new byte[beLength];

                /* Reverse left portion of leArray into beArray. */
                reverseArrayCopy(leArray, 0, beArray, 0, beLength);
                return beArray;
            }
            else
            {
                throw new BigEndianConvertException("Big Endian architures not supported.");
            }
        }

        /// <summary>
        /// Convert an array of bytes in big-endian order into a double. 
        /// </summary>
        /// <param name="bytes">Big-endian bytes, 8 of</param>
        public static double BigEndianBytesToDouble(byte[] bytes)
        {
            double dummy = 0.0;
            byte[] adjustedBytes;
            if (BitConverter.IsLittleEndian)
            {
                int len = BitConverter.GetBytes(dummy).Length;
                adjustedBytes = new byte[len];
                reverseArrayCopy(bytes, 0, adjustedBytes, 0, len); 
            }
            else
            {
                adjustedBytes = bytes;
            }
            double dbl = BitConverter.ToDouble(bytes, 0);
            return dbl;
        }


        /// <summary>
        /// Convert an array of bytes in big-endian order into a single. 
        /// </summary>
        /// <param name="bytes">Big-endian bytes, 4 of</param>
        public static Single BigEndianBytesToSingle(byte[] bytes)
        {
            Single dummy = 0.0f;
            byte[] adjustedBytes;
            if (BitConverter.IsLittleEndian)
            {
                int len = BitConverter.GetBytes(dummy).Length;
                adjustedBytes = new byte[len];
                reverseArrayCopy(bytes, 0, adjustedBytes, 0, len);
            }
            else
            {
                adjustedBytes = bytes;
            }
            Single sng = BitConverter.ToSingle(bytes, 0);
            return sng;
        }


        /// <summary>
        /// Convert a double into a big endian array of bytes. 
        /// </summary>
        public static byte[] Int32ToBigEndianBytes(double val)
        {
            byte[] dblBytes = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
            {
                int len = dblBytes.Length;
                byte[] adjustedBytes = new byte[len];
                reverseArrayCopy(dblBytes, 0, adjustedBytes, 0, len);
                return adjustedBytes;
            }
            else return dblBytes;
        }

        
        /// <summary>
        /// Converts an array of bytes into a string represensation
        /// </summary>
        /// <param name="bArray">Byte array or List of bytes</param>
        /// <returns>string representation</returns>
        public static string ByteArrayToString(IEnumerable<byte> bArray)
        {
            StringBuilder s = new StringBuilder();
            s.Append("[ ");
            foreach (byte b in bArray)
            {
                string byteString = String.Format("{0:X2} ", b);
                s.Append(byteString);
            }
            s.Append("]");
            return s.ToString();
        }
    }
}

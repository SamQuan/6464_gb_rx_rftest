using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using System.Collections.Specialized;
using System.IO;
using System.Globalization;

namespace Bookham.TestLibrary.Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] xData;
            double[] yData;

            double[] unsmoothedData = new double[]{2,4,6,8,10,8,6,4,2};
            double[] smoothedData = Alg_Smoothing.PascalTriangleSmooth(unsmoothedData, 5);

            //ReadCSV(0, 2, out xData, out yData, "C:\\temp\\DebugData\\IncompleteSweep.csv");
            //ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\compressedPhase.csv");

            ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\CharMzLI_PulseCarver_Imb_DK100037.008_20081202032500983.csv");
            smoothedData = Alg_Smoothing.PascalTriangleSmooth(yData, 25);
            WriteData(xData, yData, smoothedData, "15pt_Smoothed.csv");
            // Center around zero
            xData = Alg_ArrayFunctions.AddToEachArrayElement(xData, (xData[xData.Length - 1] + xData[0])/-2);


            // Tap data
            yData = NormaliseYdata(yData);

            // Noisy CoC data
            Alg_MZAnalysis.MzAnalysisRules mzAnalysisRules = new Alg_MZAnalysis.MzAnalysisRules();
            mzAnalysisRules.numberOfPointsForSmoothing = 25;
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 15;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.power_in_dBm = false;
            mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;

            Alg_MZAnalysis.DqpskAnalysis dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            // Original method using poly fit
            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, 5, false );

            // Smoothed polyfit
            mzAnalysisRules.numberOfPointsForSmoothing = 25;
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 15;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.orderOfPolyFit = 20;
            mzAnalysisRules.power_in_dBm = false;
            mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PolyFit;
            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            #region SinusoidalFit
            double[] coeffs;
            double maxMse = double.MinValue;
            double minMse = double.MaxValue;
            foreach (string dbgFileName in Directory.GetFiles("C:\\temp\\DebugData\\SinusoidalFit"))
            {
                if ( dbgFileName.Contains("fitted" ) )
                    continue;
                //string dbgFileName = "C:\\temp\\DebugData\\InitMzLI_OuterMz_DK100037.008_20081202032137186.csv";
                ReadCSV(0, 4, out xData, out yData, dbgFileName);
                
                if (xData.Length == 0 || yData.Length == 0)
                    continue;

                coeffs = new double[4] { 0.0005, 400, 0, -0.0005 };

                //coeffs = new double[5] { -0.0005, 0.0005, 0.9, 0.005, 0.01 };
                //coeffs = new double[5] {-0.0005,-0.0005,0.7,0.02,0.05};
                double yAverage = 0;
                foreach (double element in yData)
                {
                    yAverage += element;
                }
                yAverage /= yData.Length;
                coeffs = new double[5] { yAverage, 0.001, 0.8, 0, 0.1 };

                double maxY = Alg_FindFeature.FindAbsoluteMaxWithSign(yData);
                double minY = Alg_FindFeature.FindAbsoluteMinWithSign(yData);
                double yRange = Math.Abs((maxY + minY ) / 2);

                coeffs = new double[5] { yAverage, yRange, 0.8, 0, 0.02 };

                double[] upperConstraints = new double[5] { double.MaxValue, double.MaxValue, 0.99, double.MaxValue, 0.1 };
                double[] lowerConstraints = new double[5] { double.MinValue, double.MinValue, 0.5, double.MinValue, 0.01 };

                SinusoidalFit sinusoidalFit = Alg_SinusoidalFit.PerformFit(xData, yData, coeffs);
                //SinusoidalFit sinusoidalFit = Alg_SinusoidalFit.PerformFit(xData, yData, coeffs, upperConstraints, lowerConstraints);

                string headerLine = String.Format("X,Raw Y,FittedY,MSE=,{0},Coeffs=,{1},{2},{3},{4},{5}", sinusoidalFit.MeanSquaredError, sinusoidalFit.Coeffs[0], sinusoidalFit.Coeffs[1], sinusoidalFit.Coeffs[2], sinusoidalFit.Coeffs[3], sinusoidalFit.Coeffs[4]);

                double mseOverYave = sinusoidalFit.MeanSquaredError / yAverage;

                if (coeffs[2] < 0.51 || coeffs[2] > 0.98)
                {
                    //Console.WriteLine("Retry " + dbgFileName + " on opposite phase");
                    coeffs[0] = yAverage;    
                    coeffs[1] = -yRange;  // Invert
                    coeffs[2] = 0.8;
                    coeffs[3] = 0;      
                    coeffs[4] = 0.02;
                    sinusoidalFit = Alg_SinusoidalFit.PerformFit(xData, yData, coeffs);
                    headerLine = String.Format("X,Raw Y,FittedY,MSE=,{0},Coeffs=,{1},{2},{3},{4},{5}", sinusoidalFit.MeanSquaredError, sinusoidalFit.Coeffs[0], sinusoidalFit.Coeffs[1], sinusoidalFit.Coeffs[2], sinusoidalFit.Coeffs[3], sinusoidalFit.Coeffs[4]);

                    if (coeffs[2] < 0.51 || coeffs[2] > 0.98)
                    {
                        bool eek = true;
                        Console.WriteLine("Problem fitting " + dbgFileName);
                    }
                }
                maxMse = Math.Max(maxMse, sinusoidalFit.MeanSquaredError);
                minMse = Math.Min(minMse, sinusoidalFit.MeanSquaredError);
                Console.Write(".");

                WriteData(xData, yData, sinusoidalFit.FittedYArray, dbgFileName + ".fitted.csv", headerLine);
            }
            #endregion

            #region DQPSK MZ Characterise
            string[] fileListPC = new string[2] { "ExtraMzLV_19.csv", "ExtraMzLV_20.csv" };
            foreach (string fileName in fileListPC)
            {
                double[] leftData;
                double[] rightData;
                ReadCSV(0, 1, out rightData, out leftData, "C:\\temp\\DebugData\\" + fileName);
                ReadCSV(0, 5, out xData, out yData, "C:\\temp\\DebugData\\" + fileName);
                xData = Alg_ArrayFunctions.SubtractArrays(leftData, rightData);

                mzAnalysisRules = new Alg_MZAnalysis.MzAnalysisRules();
                mzAnalysisRules.numberOfPointsForSmoothing = 15;
                mzAnalysisRules.numberOfPointsForTurningPointDetection = 15;
                mzAnalysisRules.numberOfPointsForTurningPointFit = 4;
                mzAnalysisRules.power_in_dBm = false;
                mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;
                Alg_MZAnalysis.DqpskAnalysis mzAnlyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            }


            //
            // Outer Analysis
            //

            //ReadCSV(0, 5, out xData, out yData, "C:\\temp\\DebugData\\outermz_pn.csv");
            //ReadCSV(0, 5, out xData, out yData, "C:\\temp\\DebugData\\cocOuter.csv");
            string[] fileList = new string[6]{
                "191900_OuterPP_1.csv", "191900_OuterPP_2.csv",
                "191250_OuterPP_1.csv", "191250_OuterPP_2.csv",
                "193150_OuterPP_1.csv", "193150_OuterPP_2.csv"};
            foreach (string fileName in fileList)
            {
                //
                // Electrical
                ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\elec\\" + fileName);
                yData = NormaliseYdata(yData);

                // Used in DQPSK MZ Characterise
                mzAnalysisRules = new Alg_MZAnalysis.MzAnalysisRules();
                mzAnalysisRules.numberOfPointsForSmoothing = 15;
                mzAnalysisRules.numberOfPointsForTurningPointDetection = 10;
                mzAnalysisRules.numberOfPointsForTurningPointFit = 3;
                mzAnalysisRules.power_in_dBm = false;
                mzAnalysisRules.quadPointLocationMethod = Alg_MZAnalysis.QuadPointLocationMethod.HalfPower;
                mzAnalysisRules.quadPointSelectionCriteria = Alg_MZAnalysis.QuadPointSelectionCriteria.HighestER;
                mzAnalysisRules.quadPointSlope = Alg_MZAnalysis.QuadPointSlope.EitherSlope;
                mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;

                Alg_MZAnalysis.MZAnalysis mzAnalyRaw =
                      Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);
                if (Double.IsNaN(mzAnalyRaw.ExtinctionRatio_dB))
                    throw new Exception("Analysis failed");

                // Optical
                ReadCSV(0, 5, out xData, out yData, "C:\\temp\\DebugData\\elec\\" + fileName);
                mzAnalyRaw = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);
                if (Double.IsNaN(mzAnalyRaw.ExtinctionRatio_dB))
                    throw new Exception("Analysis failed");

            }
            #endregion

            #region Phase Selection
            ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\phase_ch0.csv");
            yData = NormaliseYdata(yData);

            mzAnalysisRules = new Alg_MZAnalysis.MzAnalysisRules();
            mzAnalysisRules.numberOfPointsForSmoothing = 15;
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 15;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.power_in_dBm = false;
            mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;

            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);
            double firstMin = dqpskAnalyRaw.VoltageAtMin;

            ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\phase_ch46.csv");
            yData = NormaliseYdata(yData);


            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.targetLocation = firstMin;
            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            ReadCSV(0, 4, out xData, out yData, "C:\\temp\\DebugData\\phase_ch92.csv");
            yData = NormaliseYdata(yData);

            mzAnalysisRules.targetLocation = 0;
            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.targetLocation = firstMin;
            dqpskAnalyRaw = Alg_MZAnalysis.DqpskMzAnalysis(xData, yData, mzAnalysisRules);


            #endregion

            double[] xDataToAnalyse;
            double[] yDataToAnalyse;
            if (xData[0] > xData[xData.Length - 1])
            {
                xDataToAnalyse = Alg_ArrayFunctions.ReverseArray(xData);
                yDataToAnalyse = Alg_ArrayFunctions.ReverseArray(yData);
            }
            else
            {
                xDataToAnalyse = (double[])xData.Clone();
                yDataToAnalyse = (double[])yData.Clone();
            }
            // Center around zero
            //xDataToAnalyse = Alg_ArrayFunctions.AddToEachArrayElement(xDataToAnalyse, xDataToAnalyse[xDataToAnalyse.Length - 1] - xDataToAnalyse[0]);
            // Tap data
            //yData = NormaliseYdata(yData);



            mzAnalysisRules.numberOfPointsForSmoothing = 15;
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 15;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.power_in_dBm = false;
            mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;

            Alg_MZAnalysis.MZAnalysis mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);


            // Method using smoothing and 2nd order fit
            mzAnalysisRules = new Alg_MZAnalysis.MzAnalysisRules();
            mzAnalysisRules.numberOfPointsForSmoothing = 15;
            mzAnalysisRules.numberOfPointsForTurningPointDetection = 5;
            mzAnalysisRules.numberOfPointsForTurningPointFit = 5;
            mzAnalysisRules.power_in_dBm = false;
            mzAnalysisRules.quadPointLocationMethod = Alg_MZAnalysis.QuadPointLocationMethod.Inflection;
            mzAnalysisRules.quadPointSelectionCriteria = Alg_MZAnalysis.QuadPointSelectionCriteria.HighestER;
            mzAnalysisRules.quadPointSlope = Alg_MZAnalysis.QuadPointSlope.EitherSlope;
            mzAnalysisRules.smoothingMethod = Alg_MZAnalysis.SmoothingMethod.PascalTriangleSmoothing;

            mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.quadPointSlope = Alg_MZAnalysis.QuadPointSlope.PositiveSlope;
            mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.quadPointSelectionCriteria = Alg_MZAnalysis.QuadPointSelectionCriteria.HighestER;
            mzAnalysisRules.quadPointSlope = Alg_MZAnalysis.QuadPointSlope.EitherSlope;
            mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.quadPointLocationMethod = Alg_MZAnalysis.QuadPointLocationMethod.HalfPower;
            mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);

            mzAnalysisRules.quadPointLocationMethod = Alg_MZAnalysis.QuadPointLocationMethod.HalfBias;
            mzAnaly = Alg_MZAnalysis.DqpskOuterMzAnalysis(xData, yData, mzAnalysisRules);


            ReadCSV(0, 6, out xData, out yData, "C:\\temp\\DebugData\\DataIn.csv");

            double[] differential = Alg_Differential.Differential(xData, yData);
            WriteData(xData, yData, differential, "DifferentialTest.csv");

            differential = Alg_Differential.SmoothedDifferential(xData, yData, 4);
            WriteData(xData, yData, differential, "SmoothedDifferentialTest.csv");

            PolyFit fitData = Alg_PolyFit.PolynomialFit(xData, yData, 300);
            WriteData(xData, yData, fitData.FittedYArray, "PolyFit.csv");

            // MZ Analysis
            //ReadCSV(out xData, out yData, "C:\\temp\\DebugData\\RN37347_006_RA.csv");
            ReadCSV(0, 6, out xData, out yData, "C:\\temp\\DebugData\\RN37347_006_Diff.csv");
            
            if (xData[0] > xData[xData.Length-1])
            {
                xDataToAnalyse = Alg_ArrayFunctions.ReverseArray(xData);
                yDataToAnalyse = Alg_ArrayFunctions.ReverseArray(yData);
            } 
            else
            {
                xDataToAnalyse = (double[])xData.Clone();
                yDataToAnalyse = (double[])yData.Clone();
            }


            Alg_MZLVMinMax.LVMinMax tp = Alg_MZLVMinMax.MZLVMinMax(xDataToAnalyse, yDataToAnalyse, 5, true);
            Alg_MZAnalysis.MZAnalysis results = Alg_MZAnalysis.ZeroChirpAnalysis(xDataToAnalyse, yDataToAnalyse, tp, true, 0);

            // NEG Chirp
            double[] yDataToAnalyse_rev = Alg_ArrayFunctions.ReverseArray(yDataToAnalyse);
            tp = Alg_MZLVMinMax.MZLVMinMax(xDataToAnalyse, yDataToAnalyse, 5, true);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(xDataToAnalyse, yDataToAnalyse, tp, true, 0);


            #region setupTestEnginePluginLoader
            string[] searchDirs = {"."};
            Bookham.TestEngine.Framework.PluginLoader.Loader.AddPluginType("ReadDataPluginFamily",typeof(Bookham.TestEngine.PluginInterfaces.ExternalData.IDataRead), searchDirs);
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            ExtData.AddReadSource("Pcas_PAI", "PcasDataRead", "", "PCAS", true);
            #endregion

            // RN37393.015
            // Passes @ 1564
            // Create some key data
            StringDictionary keyData = new StringDictionary();
            keyData.Add("SERIAL_NO", "RN37389.015");
            keyData.Add("DEVICE_TYPE", "GEN4_MZ");
            keyData.Add("SCHEMA", "COC");
            //keyData.Add("TEST_STAGE", "DCTEST_156400");
            keyData.Add("TEST_STAGE", "DCTEST_152700");

            DatumList previousResults = ExtData.ReadData().GetLatestResults(keyData, true, "RETEST=0");
            FixBrokenTestEngineFilePaths(previousResults);


            #region pullGenericSweepData
            string leftArmDataFile = previousResults.ReadFileLinkFullPath("PLOT_LEFTARMSESWEEP");
            double[] leftArm_V;
            double[] leftArm_mW;            
            ReadCSV(0, 6, out leftArm_V, out leftArm_mW, leftArmDataFile);

            string rightArmDataFile = previousResults.ReadFileLinkFullPath("PLOT_RIGHTARMSESWEEP");
            double[] rightArm_V;
            double[] rightArm_mW;
            ReadCSV(0 ,6, out rightArm_V, out rightArm_mW, rightArmDataFile);
            #endregion

            #region GenericZeroChirpAnalysis
            // Stitch the single ended sweeps together
            double[] reversedLeftArm_V = Alg_ArrayFunctions.ReverseArray(leftArm_V);
            double[] reversedLeftArm_mW = Alg_ArrayFunctions.ReverseArray(leftArm_mW);
            
            double[] positiveRightArm_V = Alg_ArrayFunctions.MultiplyEachArrayElement(rightArm_V, -1);

            double[] leftArmRightArm_V = Alg_ArrayFunctions.JoinArrays(reversedLeftArm_V, positiveRightArm_V);
            double[] leftArmRightArm_mW = Alg_ArrayFunctions.JoinArrays(reversedLeftArm_mW, rightArm_mW);

            // Analyse
            fitData = Alg_PolyFit.PolynomialFit(leftArmRightArm_V, leftArmRightArm_mW, 20);
            WriteData(leftArmRightArm_V, leftArmRightArm_mW, fitData.FittedYArray, "RN37393.015_GENSWEEP_1527.csv");
            tp = Alg_MZLVMinMax.MZLVMinMax(leftArmRightArm_V, leftArmRightArm_mW, 5, true);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(leftArmRightArm_V, leftArmRightArm_mW, tp, true, 0);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(leftArm_V, rightArm_V, leftArm_mW, rightArm_mW, true, true, 0);
            #endregion

            #region compareGenSweepAnalysis
            Alg_MZAnalysis.MZAnalysis prevResults = new Alg_MZAnalysis.MZAnalysis();
            prevResults.ExtinctionRatio_dB = previousResults.ReadDouble("GEN_ER");
            prevResults.Vpi = previousResults.ReadDouble("GEN_VPI");
            prevResults.VImb = previousResults.ReadDouble("GEN_VIMB");
            prevResults.VQuad = previousResults.ReadDouble("GEN_VIMB");

            double deltaER = Math.Abs(prevResults.ExtinctionRatio_dB - results.ExtinctionRatio_dB);
            System.Diagnostics.Debug.Assert( deltaER < 0.1, "ER Analysis gives different results",deltaER.ToString() );
            double deltaVPi = Math.Abs(Math.Abs(prevResults.Vpi) - Math.Abs(results.Vpi));
            System.Diagnostics.Debug.Assert(deltaVPi < 0.1, "VPi Analysis gives different results", deltaVPi.ToString());
            double deltaQuad = Math.Abs(prevResults.VQuad - results.VQuad);
            System.Diagnostics.Debug.Assert(deltaQuad < 0.1, "QuadPoint Analysis gives different results", deltaQuad.ToString());
            // Save for debug / checking
            WriteData(leftArmRightArm_V, leftArmRightArm_mW, leftArmRightArm_mW, "RN37393.015_GENSWEEP.csv");
            #endregion



            #region pullVimbSweepData
            string imbDataFile = previousResults.ReadFileLinkFullPath("PLOT_VIMBCTRLDIFFERENTIALSWEEP");
            double[] imbSweep_V;
            double[] imbSweep_mW;
            ReadCSV(0, 6, out imbSweep_V, out imbSweep_mW, imbDataFile);
            double[] imbCTap_A;
            ReadCSV(0, 5, out imbSweep_V, out imbCTap_A, imbDataFile);
            #endregion

            #region vImbAnalysisAndComparison
            // Save for debug / checking
            WriteData(imbSweep_V, imbSweep_mW, imbSweep_mW, "RN37393.015_IMBSWEEP.csv");
            tp = Alg_MZLVMinMax.MZLVMinMax(imbSweep_V, imbSweep_mW, 5, true);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(imbSweep_V, imbSweep_mW, tp, true, 0);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(imbSweep_V, imbSweep_mW, true, true, 0);

            prevResults.ExtinctionRatio_dB = previousResults.ReadDouble("IMBC_ER");
            prevResults.Vpi = previousResults.ReadDouble("IMBC_DIPI");
            prevResults.VImb = previousResults.ReadDouble("IMBC_DIIMB");
            prevResults.VQuad = previousResults.ReadDouble("IMBC_DIIMB");

            deltaER = Math.Abs(prevResults.ExtinctionRatio_dB - results.ExtinctionRatio_dB);
            System.Diagnostics.Debug.Assert(deltaER < 0.1, "ER Analysis gives different results", deltaER.ToString());
            deltaVPi = Math.Abs(Math.Abs(prevResults.Vpi) - Math.Abs(results.Vpi));
            System.Diagnostics.Debug.Assert(deltaVPi < 0.1, "VPi Analysis gives different results", deltaVPi.ToString());
            deltaQuad = Math.Abs(prevResults.VQuad - results.VQuad);
            System.Diagnostics.Debug.Assert(deltaQuad < 0.1, "QuadPoint Analysis gives different results", deltaQuad.ToString());

            // TAP ANALYSIS
            double min = Alg_PointSearch.FindMinValueInArray(imbCTap_A);
            double[] normalisedCtapA = Alg_ArrayFunctions.SubtractFromEachArrayElement(imbCTap_A, min);

            tp = Alg_MZLVMinMax.MZLVMinMax(imbSweep_V, normalisedCtapA, 5, false);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(imbSweep_V, normalisedCtapA, tp, false, 0);
            results = Alg_MZAnalysis.ZeroChirpTapAnalysis(imbSweep_V, imbCTap_A, false);

            prevResults.VQuad = previousResults.ReadDouble("IMBC_DIIMB_CTAP");

            deltaQuad = Math.Abs(prevResults.VQuad - results.VQuad);
            System.Diagnostics.Debug.Assert(deltaQuad < 0.1, "CTap QuadPoint Analysis gives different results", deltaQuad.ToString());
            #endregion


            #region pullDiffSweepData
            string diffDataFile = previousResults.ReadFileLinkFullPath("PLOT_ARMDIFFERENTIALSWEEP");
            double[] diffSweep_V;
            double[] diffSweep_dB;
            ReadCSV(0, 6, out diffSweep_V, out diffSweep_dB, diffDataFile);
            double[] diffCTap_A;
            ReadCSV(0, 5, out diffSweep_V, out diffCTap_A, diffDataFile);
            #endregion

            #region diffAnalysisAndComparison
            tp = Alg_MZLVMinMax.MZLVMinMax(diffSweep_V, diffSweep_dB, 5, true);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(diffSweep_V, diffSweep_dB, tp, true, 0);

            prevResults.ExtinctionRatio_dB = previousResults.ReadDouble("DIFF_ER");
            prevResults.Vpi = previousResults.ReadDouble("DIFF_VPI");
            prevResults.VImb = previousResults.ReadDouble("DIFF_VIMB");
            prevResults.VQuad = previousResults.ReadDouble("DIFF_VIMB");

            deltaER = Math.Abs(prevResults.ExtinctionRatio_dB - results.ExtinctionRatio_dB);
            System.Diagnostics.Debug.Assert(deltaER < 0.1, "ER Analysis gives different results", deltaER.ToString());
            deltaVPi = Math.Abs(Math.Abs(prevResults.Vpi) - Math.Abs(results.Vpi));
            System.Diagnostics.Debug.Assert(deltaVPi < 0.1, "VPi Analysis gives different results", deltaVPi.ToString());
            deltaQuad = Math.Abs(prevResults.VQuad - results.VQuad);
            System.Diagnostics.Debug.Assert(deltaQuad < 0.1, "QuadPoint Analysis gives different results", deltaQuad.ToString());
            
            // Diff CTAP analysis
            min = Alg_PointSearch.FindMinValueInArray(diffCTap_A);
            normalisedCtapA = Alg_ArrayFunctions.SubtractFromEachArrayElement(diffCTap_A, min);

            tp = Alg_MZLVMinMax.MZLVMinMax(diffSweep_V, normalisedCtapA, 5, false);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(diffSweep_V, normalisedCtapA, tp, false, 0);
            results = Alg_MZAnalysis.ZeroChirpAnalysis(diffSweep_V, normalisedCtapA, false, true, 0);

            prevResults.VQuad = previousResults.ReadDouble("DIFF_VIMB_CTAP");
            deltaQuad = Math.Abs(prevResults.VQuad - results.VQuad);
            System.Diagnostics.Debug.Assert(deltaQuad < 0.1, "CTAP QuadPoint Analysis gives different results", deltaQuad.ToString());
            #endregion

        }

        private static double[] NormaliseYdata(double[] yData)
        {
            // Convert tap data
            // Tap analysis works on current rather than power.
            // Shift power array so that there are no negative numbers.
            // The result is that in either case the points at the top of the plot are of higher power
            double offset = Alg_PointSearch.FindMinValueInArray(yData);
            return Alg_ArrayFunctions.SubtractFromEachArrayElement(yData, offset);
        }

        private static void FixBrokenTestEngineFilePaths(DatumList previousResults)
        {
            string node;
            if (previousResults.GetDatumType("NODE") == DatumType.StringType)
            {
                node = previousResults.ReadString("NODE");
            }
            else
            {
                node = "NODE" + previousResults.ReadDouble("NODE").ToString();
            }

            DatumFileLink fl = (DatumFileLink)previousResults.GetDatum("PLOT_LEFTARMSESWEEP");
            if (fl.GetDirectoryPath().Length == 0)
            {
                resultsArchive = @"\\zpgty0x0\results\";
                UpdateFilePaths(node, previousResults);
            }
            fl = (DatumFileLink)previousResults.GetDatum("PLOT_LEFTARMSESWEEP");
            if (fl.GetDirectoryPath().Length == 0)
            {
                resultsArchive = @"\\Szn-sfl-clst-01\Results\";
                UpdateFilePaths(node, previousResults);
            }
        }

        private static void WriteData(double[] xData, double[] yData, double[] dataOut, string fileName)
        {
            WriteData(xData, yData, dataOut, fileName, "");
        }

        private static void WriteData(double[] xData, double[] yData, double[] dataOut, string fileName, string headerLine)
        {
            // Plot or write to CSV
            //string dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "plot");
            string dropArea = "C:\\temp\\DebugData\\";

            try
            {
                //dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "plot");
                if (!System.IO.Directory.Exists(dropArea))
                {
                    System.IO.Directory.CreateDirectory(dropArea);
                }

                System.IO.StreamWriter writer = new System.IO.StreamWriter(System.IO.Path.Combine(dropArea, fileName));
                if (headerLine.Length > 0)
                    writer.WriteLine(headerLine);

                for (int i = 0; i < xData.Length; i++)
                {
                    writer.WriteLine(xData[i].ToString() + "," + yData[i].ToString() + "," + dataOut[i].ToString());
                }
                writer.Close();
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore filesystem errors. Missing plot data should not be fatal.
            }
        }

        private static void ReadCSV(int xIndex, int yIndex, out double[] xData, out double[] yData, string fileName)
        {
            // Read CSV

            CsvReader reader = new CsvReader();
            string[] lineElems;
            List<double> colA = new List<double>();
            List<double> colB = new List<double>();

            reader.OpenFile(fileName);
            lineElems = reader.GetLine();

            while (lineElems != null)
            {
                // read the next line
                lineElems = reader.GetLine();
                // check for end of file
                if (lineElems == null) break;
                double result;
                if (Double.TryParse(lineElems[xIndex], out result) && Double.TryParse(lineElems[yIndex], out result))
                {
                    // Looks ok. Process the line
                    colA.Add(Convert.ToDouble(lineElems[xIndex]));
                    colB.Add(Convert.ToDouble(lineElems[yIndex]));
                }
            }

            reader.CloseFile();

            xData = colA.ToArray();
            yData = colB.ToArray();
        }

        /// <summary>
        /// Ensures that DatumFileLinks are populated with the full path to the raw data files.
        /// </summary>
        /// <param name="nodeNumber">PCAS node number</param>
        /// <param name="resultsData">The results data to update</param>
        private static void UpdateFilePaths(string nodeNumber, DatumList resultsData)
        {
            // If the path to a raw data file was not stored, we will need to create one.          
            if (nodeNumber.Length == 0)
            {
                // NODE cannot be found. This is a fatal error.				
                //logger.ErrorInExternalDataPlugin("PCAS Read Plug-in Fatal Error: NODE is not stored as part of test results. Not attempting to update DatumFileLink paths");
                return;
            }

            if (nodeNumber.IndexOf("NODE") == -1)
            {
                // Prepend "NODE" if not already there
                nodeNumber = "NODE" + nodeNumber;
            }

            string pcasResultsDate = resultsData.ReadString("TIME_DATE").Substring(0, 8);
            string shortPcasResultsDate = resultsData.ReadString("TIME_DATE").Substring(0, 6);

            //Find the path on the server to the blob data.
            bool foundBlobDataFile = false;

            string fullResultsPath;
            string directoryResultsPath;

            foreach (Datum resultDatum in resultsData)
            {
                // Update the path for each DatumFileLink
                if (resultDatum.Type.ToString() == "FileLinkType")
                {
                    DatumFileLink dfl = (DatumFileLink)resultDatum;
                    // Don't update entries that already have a full path.
                    if (dfl.GetDirectoryPath().Length == 0)
                    {
                        foundBlobDataFile = false;
                        foundBlobDataFile = SearchFileServerPath(nodeNumber, pcasResultsDate, foundBlobDataFile, dfl, out fullResultsPath, out directoryResultsPath);
                        // The original version looked in the legacy areas too
                        if (!foundBlobDataFile)
                        {
                            directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, shortPcasResultsDate);
                            fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());

                            if (File.Exists(fullResultsPath))
                            {
                                foundBlobDataFile = true;
                            }
                        }


                        if (!foundBlobDataFile)
                        {
                            //Can't find where the Blob data is stored!!! Write a warning log, so that this is captured.
                            //Put a dummy directory path. If the test program needs this data,  then the program will 
                            //Throw an exception when it triers to access the non-existant file.
                            string errorLogMessage = "PCAS Read Plug-in Error: Unable to find Blob Data File " + dfl.GetFileName() + " for NODE " + nodeNumber + ". Cannot update DatumFileLink " + dfl.Name + " path";
                            //logger.ErrorWrite(errorLogMessage);
                        }
                        else
                        {
                            dfl.Update(directoryResultsPath, dfl.GetFileName());
                        }
                    }
                }
            }
        }

        private static bool SearchFileServerPath(string nodeNumber, string pcasResultsDate, bool foundBlobDataFile, DatumFileLink dfl, out string fullResultsPath, out string directoryResultsPath)
        {
            directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, pcasResultsDate);
            fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());

            if (File.Exists(fullResultsPath))
            {
                foundBlobDataFile = true;
            }
            else
            {
                //The Blob file might have been stored the next day.
                directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, getNextDay(pcasResultsDate));
                fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());

                if (File.Exists(fullResultsPath))
                {
                    foundBlobDataFile = true;
                }
            }

            if (!foundBlobDataFile)
            {
                //The Blob Data wasn't stored the same day the PCAS results were stored, or the day after.
                //Try searching back up to 8 days.
                string tryDate = pcasResultsDate;
                for (int i = 0; i < 8; i++)
                {
                    //Try the previous day to the one just tried
                    tryDate = getPreviousDay(tryDate);

                    //Create the path
                    directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, tryDate);
                    fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());

                    if (File.Exists(fullResultsPath))
                    {
                        //Exit here if we've found the Blob File.
                        foundBlobDataFile = true;
                        break;
                    }
                }
            }
            return foundBlobDataFile;
        }


     /// <summary>
        /// Given a date in the format DDMMYYYY, return the next day in the same format
        /// </summary>
        /// <param name="date">The date in format DDMMYYYY</param>
        /// <returns>The next day in format DDMMYYYY </returns>
        private static string getNextDay(string date)
        {
            IFormatProvider culture = new CultureInfo("en-GB", true);
            DateTime today = DateTime.Parse(date.Substring(6, 2) + "/" +
                                            date.Substring(4, 2) + "/" +
                                            date.Substring(0, 4), culture, DateTimeStyles.None);

            DateTime nextDay = today.AddDays(1);

            return (nextDay.Year.ToString() +
                    convertToStringWithAtLeastTwoDigits(nextDay.Month) +
                    convertToStringWithAtLeastTwoDigits(nextDay.Day));
        }

        /// <summary>
        /// Given a date in the format DDMMYYYY, return the previous day in the same format
        /// </summary>
        /// <param name="date">The date in format DDMMYYYY</param>
        /// <returns>The previous day in format DDMMYYYY </returns>
        private static string getPreviousDay(string date)
        {
            IFormatProvider culture = new CultureInfo("en-GB", true);
            DateTime today = DateTime.Parse(date.Substring(6, 2) + "/" +
                                            date.Substring(4, 2) + "/" +
                                            date.Substring(0, 4), culture, DateTimeStyles.None);

            DateTime previousDay = today.AddDays(-1);

            return (previousDay.Year.ToString() + 
                    convertToStringWithAtLeastTwoDigits(previousDay.Month) +
                    convertToStringWithAtLeastTwoDigits(previousDay.Day));
        }

        /// <summary>
        /// Convert an integer to a string. Result must have at least two digits.
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>String representation</returns>
        private static string convertToStringWithAtLeastTwoDigits(int number)
        {
            if (number < 10)
            {
                return ("0" + number.ToString());
            }
            else
            {
                return (number.ToString());
            }
        }
        
        private static string resultsArchive;
    }
}

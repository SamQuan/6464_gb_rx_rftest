// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_FindFeature.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Algorithm to locate the point of inflection within a set of (X,Y) data.
    /// </summary>
    public sealed class Alg_FindPointOfInflection
    {
        /// <summary>
        /// Empty private constructor.
        /// </summary>
        private Alg_FindPointOfInflection()
        {
        }

        /// <summary>
        /// Locate the point at which the gradient reaches a maximum or a minimum.
        /// </summary>
        /// <param name="xArray">X data</param>
        /// <param name="yArray">Y data</param>
        /// <param name="startPos">Index at which to begin analysis</param>
        /// <param name="stopPos">Index at which to end analysis</param>
        /// <returns>A structure containing (X,Y) coordinates as well as an indicator of success.</returns>
        public static PointOfInflection FindPointOfInflection(double[] xArray, double[] yArray, int startPos, int stopPos )
        {
            // PRECONDITIONS
            if (xArray.Length != yArray.Length)
            {
                throw new AlgorithmException("Data arrays must be of equal length");
            }
            if (xArray.Length == 0)
            {
                throw new AlgorithmException("Data arrays cannot be empty");
            }

            PointOfInflection pointOfInflection = new PointOfInflection();
            double[] diffArray = Alg_Differential.Differential(xArray, yArray);
            Alg_FindTurningPointUsing2ndOrderFit.TurningPointData tpData = new Alg_FindTurningPointUsing2ndOrderFit.TurningPointData();

            if (stopPos - startPos < 3)
            {   // Insufficient data to apply a second order fit.
                tpData.Success = false;
            }
            else
            {
                tpData = Alg_FindTurningPointUsing2ndOrderFit.FindTurningPointUsing2ndOrderFit(xArray, diffArray, startPos, stopPos);
            }

            pointOfInflection.Found = tpData.Success;
            // if a turning point was found and is within the dataset
            if (pointOfInflection.Found && tpData.TurningPointXvalue > xArray[0] && tpData.TurningPointXvalue < xArray[xArray.Length -1] )
            {
                pointOfInflection.XValue = tpData.TurningPointXvalue;
                // Look within the subset covering the range that we just fitted to
                double[] xDataSubset = Alg_ArrayFunctions.ExtractSubArray(xArray, startPos, stopPos);
                double[] yDataSubset = Alg_ArrayFunctions.ExtractSubArray(yArray, startPos, stopPos);
                try
                {
                pointOfInflection.YValue = XatYAlgorithm.Calculate(yDataSubset, xDataSubset, tpData.TurningPointXvalue);
                }
                catch (AlgorithmException)
                {
                    pointOfInflection.XValue = double.NaN;
                    pointOfInflection.YValue = double.NaN;
                }
            }
            else
            {
                pointOfInflection.XValue = double.NaN;
                pointOfInflection.YValue = double.NaN;
            }
            return pointOfInflection;
        }


        /// <summary>
        /// Structure to return data from Alg_PointOfInflection
        /// </summary>
        public struct PointOfInflection
        {
            /// <summary>
            /// Indicates success or failure.
            /// </summary>
            public bool Found;
            /// <summary>
            /// X Coordinate of the point of inflection.
            /// </summary>
            public double XValue;
            /// <summary>
            /// Y Coordinate of the point of inflection.
            /// </summary>
            public double YValue;
        }
    }
}

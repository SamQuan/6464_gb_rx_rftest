Public Function OptimiseDelay(Optional ByRef Data, Optional ByRef Abort As Boolean) As Boolean

    'Function to optimise delay lines to achieve maximum bandwidth.
    
    Dim aDate As Date
    Dim aDates() As Date
    Dim b As Boolean
    Dim bFail As Boolean
    Dim dBandwidth As Double
    Dim dBandwidths() As Double
    Dim dBandwidthsSort() As Double
    Dim dBandwidthStart As Double
    Dim dDelta As Double
    Dim dNull As Double
    Dim dNulls() As Double
    Dim dRange As Double
    Dim dRanges() As Double
    Dim l As Long
    Dim lCount As Long
    Dim n As Single
    Dim nDelay As Single
    Dim nDelayLast As Single
    Dim nDelays() As Single
    Dim nDelaysSort() As Single
    Dim nSign As Single
    
    'Check for aborts.
    If Abort Then Exit Function
    
    'Check instruments are present.
    If Not cHP8703.Present Then Exit Function
    If Not cDelay1.Present Then Exit Function
    If Not cDelay2.Present Then Exit Function
    
    'Default sign.
    nSign = 1
    
    'Set fail flag.
    bFail = True
    
    Do
        'Get current date and time.
        aDate = Now
        
        'Read current delay (this is required as the calculated delay will be more accurate that the delay line can support).
        nDelay = ReadDelay()
        
        'Measure 3dB bandwidth, null frequency and dynamic range.
        dBandwidth = MeasureBandwidth(NullFrequency:=dNull, DynamicRange:=dRange)
        
        'Increment counter for valid data.
        lCount = lCount + 1
        
        'Redimension arrays.
        If lCount = 1 Then
            ReDim aDates(1 To lCount)
            ReDim nDelays(1 To lCount)
            ReDim nDelaysSort(1 To lCount)
            ReDim dNulls(1 To lCount)
            ReDim dBandwidths(1 To lCount)
            ReDim dRanges(1 To lCount)
            ReDim dBandwidthsSort(1 To lCount)
            
            'Remember first bandwidth.
            dBandwidthStart = dBandwidth
        Else
            ReDim Preserve aDates(1 To lCount)
            ReDim Preserve nDelays(1 To lCount)
            ReDim Preserve nDelaysSort(1 To lCount)
            ReDim Preserve dNulls(1 To lCount)
            ReDim Preserve dBandwidths(1 To lCount)
            ReDim Preserve dRanges(1 To lCount)
            ReDim Preserve dBandwidthsSort(1 To lCount)
        End If
        
        'Record data in arrays.
        aDates(lCount) = aDate
        nDelays(lCount) = nDelay
        dNulls(lCount) = dNull
        dBandwidths(lCount) = dBandwidth
        dRanges(lCount) = dRange
        
        nDelaysSort(lCount) = nDelay
        dBandwidthsSort(lCount) = dBandwidth
        
        'Check we have a valid range.
        If dRange = 0 Then
            'Abort this test.
            Debug.Assert 0
            Exit Do
        End If
        
        'Check if we have a valid range, but we can't get the bandwidth as there is no 3dB point.
        If (dNull = 0) And (dBandwidth = 0) And (dRange > 0) Then
            Debug.Assert 0
            OptimiseDelay = True
            Exit Do
        End If
        
        'Check if exceeded number of attempts.
        If (lCount >= 8) And (Not bFail) Then
            If dBandwidth > dBandwidthStart Then OptimiseDelay = True
            Exit Do
        End If
        
        If lCount > 1 Then
            'Check that we are going in the right direction.
            dDelta = dBandwidths(lCount) - dBandwidths(lCount - 1)
            
            If dDelta < 0 Then
                'Reverse the sign.
                nSign = -nSign
            End If
        End If
        
        'Check if we have a null.
        If (dNull > 0) Then
            'Calculate the predicted change in delay.
            n = 1000 / (2 * dNull)
            
        Else
            'Calculate the empirical change in delay.
            n = 2 + Abs(dDelta)
            
        End If
        
        'Change delay.
        nDelay = nDelay + nSign * n
        
        If lCount >= 5 Then
            'Sort the parameters so we can perform a curve fit.
            Call Math.Sort(nDelaysSort, dBandwidthsSort)
            
            'Find the delay corresponding to maximum interpolated bandwidth.
            Call Math.FindPeak(dBandwidthsSort, nDelaysSort, n, 1, bFail)
            
            'Change the delay to the interpolated delay.
            If Not bFail Then nDelay = n
            
            'See if we have arrived at the same point as last time.
            If nDelay = nDelayLast Then
                'Exit loop.
                If dBandwidth > dBandwidthStart Then OptimiseDelay = True
                Exit Do
            End If
        Else
            'Set fail flag as we don't have enough data to accurately determine optimum delay.
            bFail = True
        End If
        
        'Set new delay and check it has been set.
        b = SetDelay(nDelay)
        If Not b Then Exit Do
        
        'Loop is temporarily distablised by change in insertion loss of delay lines while changing.
        'Small wait before reset and then complete sweep.
        Wait 5, Abort:=Abort
        If Abort Then Exit Do
        
        'Set lock-in amplifier auto-offset.
        Call cLIA.SetAutoOffset
        
        'Wait for control loops.
        Wait 10, Abort:=Abort
        If Abort Then Exit Do
        
        'Remember delay.
        nDelayLast = nDelay
    Loop
    
    If Not IsMissing(Data) Then
        'Clear existing data.
        On Error Resume Next
        Erase Data
        Data = Empty
        On Error GoTo 0
        
        'Record data in Data object.
        For l = 1 To lCount
            Call CSV.SetItem(Data, l, "Date", Format$(aDates(l), "dd-mmm-yyyy hh:nn:ss"))
            Call CSV.SetItem(Data, l, "Delay", nDelays(l))
            Call CSV.SetItem(Data, l, "Null Frequency", dNulls(l))
            Call CSV.SetItem(Data, l, "Bandwidth", dBandwidths(l))
            Call CSV.SetItem(Data, l, "Dynamic Range", dRanges(l))
        Next l
    End If
    
    'OptimiseDelay returned parameter will have been set above.

End Function

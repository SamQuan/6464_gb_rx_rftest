VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Math"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'   � 1998, � 1999, � 2000, � 2001, � 2002 Nortel Networks

'IMPORTANT
'Only add functions to this file that have no external references.

'Update on 10/01/2001 to make all arrays variants.
'Update on 23/04/2001 to introduce gradient checks.
'Update on 22/05/2001 to change FindPeak() and FindMaximum() functions.
'Update on 12/03/2002 to add FindPeak2() function.

Option Explicit

Public Function Differentiate1(ByRef X, ByRef Y, ByRef dYdX) As Boolean
                                            '
    'Nortel prefered method of using 3 point
    'rolling average.
    
    'Please note that extra weighting is
    'given to points further away from the
    'point being differentiated, which gives
    'the smoothing.
    
    Const nMax As Single = 3.402823E+38
    
    Dim l As Long
    Dim lElement As Long
    Dim lError As Long
    Dim dX As Double
    Dim dY As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    If (VarType(dYdX) And vbArray) = 0 Then
        Err.Description = "dYdX parameter is not an array"
        Err.Raise vbObjectError + 3
    End If
    
    ReDim dYdX(LBound(Y, 1) + 3 To UBound(Y, 1) - 3)
    
    For l = LBound(dYdX, 1) To UBound(dYdX, 1)
        
        dX = 0
        dY = 0
        
        For lElement = -3 To 3
            dX = dX + lElement * X(l + lElement)
            dY = dY + lElement * Y(l + lElement)
        Next lElement
        
        dYdX(l) = Divide(dY, dX, lError)
        
        Select Case lError
        Case 0
        Case 11
            dYdX(l) = Sgn(dY) * nMax    'Maximum for a single
        Case Else
            Err.Raise lError
        End Select
        
    Next l
    
    On Error GoTo 0
    
    Differentiate1 = True
    Exit Function

ErrorRecover:
    Debug.Print "Differentiate1: Error " & Err.Description
    Debug.Assert 0
    
    ReDim dYdX(0 To 0)
    
    On Error GoTo 0
    Exit Function

End Function

Private Function Divide(ByRef Numerator, ByRef Demoninator, Optional ByRef Error As Long) As Double

    'Error trapped divison
    
    On Error Resume Next
    Divide = Numerator / Demoninator
    Error = Err.Number
    On Error GoTo 0

End Function

Public Function Differentiate2(ByRef X, ByRef Y, ByRef dYdX) As Boolean
                                            '
    'Standard mathematical differential.
    'This method does not provide any
    'smoothing.
    
    'Please note that the method will use
    'points either side of the point to be
    'differentiated, in order to not produce
    'an offset with the date when plotted
    'against the X axis.
    
    Const nMax As Single = 3.402823E+38
    
    Dim l As Long
    Dim lError As Long
    Dim dX As Double
    Dim dY As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(dYdX) And vbArray) = 0 Then
        Err.Description = "dYdX parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    ReDim dYdX(LBound(Y, 1) + 1 To UBound(Y, 1) - 1)
    
    For l = LBound(dYdX, 1) To UBound(dYdX, 1)
        
        dX = X(l + 1) - X(l - 1)
        dY = Y(l + 1) - Y(l - 1)
        
        dYdX(l) = Divide(dY, dX, lError)
        
        Select Case lError
        Case 0
        Case 11
            dYdX(l) = Sgn(dY) * nMax    'Maximum for a single
        Case Else
            Err.Raise lError
        End Select
        
    Next l
    
    On Error GoTo 0
    
    Differentiate2 = True
    Exit Function

ErrorRecover:
    Debug.Print "Differentiate2: Error " & Err.Description
    Debug.Assert 0
    
    ReDim dYdX(0 To 0)
    
    On Error GoTo 0
    Exit Function

End Function

Public Function DifferentiateSecond1(ByRef X, ByRef Y, ByRef dYdX) As Boolean
                                            '
    'Nortel method of using 3 point rolling
    'average.
    
    'Please note that extra weighting is
    'given to points further away from the
    'point being differentiated, which gives
    'the smoothing.
    
    Const nMax As Single = 3.402823E+38
    
    Dim l As Long
    Dim lError As Long
    Dim dX As Double
    Dim dY2 As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    If (VarType(dYdX) And vbArray) = 0 Then
        Err.Description = "dYdX parameter is not an array"
        Err.Raise vbObjectError + 3
    End If
    
    ReDim dYdX(LBound(Y, 1) + 3 To UBound(Y, 1) - 3)
    
    For l = LBound(dYdX, 1) To UBound(dYdX, 1)
        
        dX = 0
        dY2 = 0
        
'       dX = dX + 5 * X(l - 3)
'       dX = dX - 3 * X(l - 1)
'       dX = dX - 4 * X(l)
'       dX = dX - 3 * X(l + 1)
'       dX = dX + 5 * X(l + 3)
        
        dX = (X(l + 3) - X(l - 3)) / 6
        
        dY2 = dY2 + 5 * Y(l - 3)
        dY2 = dY2 - 3 * Y(l - 1)
        dY2 = dY2 - 4 * Y(l)
        dY2 = dY2 - 3 * Y(l + 1)
        dY2 = dY2 + 5 * Y(l + 3)
        
        dYdX(l) = Divide(dY2, dX ^ 2, lError)
        
        Select Case lError
        Case 0
        Case 11
            dYdX(l) = Sgn(dY2) * nMax    'Maximum for a single
        Case Else
            Err.Raise lError
        End Select
        
    Next l
    
    On Error GoTo 0
    
    DifferentiateSecond1 = True
    Exit Function

ErrorRecover:
    Debug.Print "DifferentiateSecond1: Error " & Err.Description
    Debug.Assert 0
    
    ReDim dYdX(0 To 0)
    
    On Error GoTo 0
    Exit Function

End Function

Public Function FindValueRef(ByRef X, ByVal Xref, Optional ByRef Fail As Boolean) As Long
                                            '
    'Function to return element number of X()
    'which is the closest to Xref.
    
    'This method was designed by Ian Gurney.
    
    Dim bSuccess As Boolean
    Dim l As Long
    Dim dError1 As Double
    Dim dError2 As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If LBound(X, 1) = UBound(X, 1) Then
        l = LBound(X, 1)
        
        If Xref = X(l) Then
            FindValueRef = l
            bSuccess = True
        End If
    Else
        For l = LBound(X, 1) To UBound(X, 1) - 1
            dError1 = Xref - X(l)
            dError2 = Xref - X(l + 1)
            
            'The next line looks at the sign of each
            'of the two errors.
            'If the sum of them are less than two
            'then we have found the point that we
            'are looking for.
            
            If Abs(Sgn(dError1) + Sgn(dError2)) < 2 Then
                If Abs(dError1) <= Abs(dError2) Then
                    FindValueRef = l         'This is closer (rounded down)
                Else
                    FindValueRef = l + 1     'The next point is closer
                End If
                
                bSuccess = True
                Exit For
            End If
        Next l
    End If
    
    On Error GoTo 0
    
    Fail = Not bSuccess
    Exit Function

ErrorRecover:
    Debug.Print "FindValueRef: Error " & Err.Description
    Debug.Assert 0
    On Error GoTo 0
    
    Fail = True
    Exit Function

End Function

Public Function FindValue(ByRef X, ByVal Xref, ByRef Y, Optional ByRef Fail As Boolean) As Double

    'Function to return the first (going up)
    'interpolated value of Y() which
    'corresponds to Xref.
    
    'The method uses a simple straight line
    'fit between the two closest points.
    
    'This method was designed by Ian Gurney.
    
    Dim lCount As Long
    Dim dY() As Double
    
    lCount = FindValues(X, Xref, Y, dY(), True)
    
    If lCount = 0 Then
        Fail = True
        Exit Function
    End If
    
    Fail = False
    FindValue = dY(1)

End Function

Public Function FindValues(ByRef X, ByVal Xref, ByRef Y, ByRef Yvalues, Optional ByVal FindFirstOnly As Boolean) As Long

    'Function to return an array of the
    'interpolated value of Y() which
    'corresponds to Xref.
    
    'The method uses a simple straight line
    'fit between the two closest points.
    
    'This method was designed by Ian Gurney.
    
    Dim l As Long
    Dim lValues As Long
    Dim dConst As Double
    Dim dError1 As Double
    Dim dError2 As Double
    Dim dGrad As Double
    Dim dx0 As Double
    Dim dy0 As Double
    
    'Check for arrays.
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    If LBound(X, 1) = UBound(X, 1) Then
        'Single element array.
        l = LBound(X, 1)
        
        If Xref = X(l) Then
            GoSub RedimArray
            
            On Error Resume Next
            Yvalues(lValues) = Y(l)
            On Error GoTo ErrorRecover
        End If
    Else
        For l = LBound(X, 1) To UBound(X, 1) - 1
            dError1 = Xref - X(l)
            dError2 = Xref - X(l + 1)
            
            'The next line looks at the sign of each
            'of the two errors.
            'If the sum of them are less than two
            'then we have found the point that we
            'are looking for.
            
            If (Abs(Sgn(dError1) + Sgn(dError2))) < 2 Then
                dx0 = X(l + 1) - X(l)
                
                If dx0 = 0 Then
                    'Vertical line.
                    
                    GoSub RedimArray
                    
                    On Error Resume Next
                    Yvalues(lValues) = Y(l)
                    On Error GoTo ErrorRecover
                Else
                    dy0 = Y(l + 1) - Y(l)
                    
                    dGrad = dy0 / dx0
                    dConst = Y(l) - dGrad * X(l)
                    
                    GoSub RedimArray
                    
                    On Error Resume Next
                    Yvalues(lValues) = dGrad * Xref + dConst
                    On Error GoTo ErrorRecover
                End If
                
                If FindFirstOnly Then Exit For
            End If
        Next l
    End If
    
    'Switch off error recovery.
    On Error GoTo 0
    
    If lValues = 0 Then
        On Error Resume Next
        ReDim Yvalues(0 To 0)
        On Error GoTo 0
    End If
    
    FindValues = lValues
    Exit Function

RedimArray:
    lValues = lValues + 1
    
    If lValues = 1 Then
        On Error Resume Next
        ReDim Yvalues(1 To 1)
        On Error GoTo ErrorRecover
    Else
        On Error Resume Next
        ReDim Preserve Yvalues(1 To lValues)
        On Error GoTo ErrorRecover
    End If
    Return

ErrorRecover:
    Debug.Print "FindValues: Error " & Err.Description
    Debug.Assert 0
    On Error GoTo 0
    
    On Error Resume Next
    ReDim Yvalues(0 To 0)
    On Error GoTo 0
    Exit Function

End Function

Public Function FindValueExtrapolated(ByRef X, ByVal Xref As Double, ByRef Y, Optional ByRef Fail As Boolean) As Double

    'This method was designed by Ian Gurney.
    
    Dim bSuccess As Boolean
    Dim i As Integer
    Dim dConst As Double
    Dim dError1 As Double
    Dim dError2 As Double
    Dim dGrad As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    If LBound(X, 1) = UBound(X, 1) Then
        FindValueExtrapolated = FindValue(X, Xref, Y, Fail)
        On Error GoTo 0
        Exit Function
    End If
    
    Select Case Xref
    Case Is < X(LBound(X, 1))
        i = LBound(X, 1)
    Case Is > X(UBound(X, 1))
        i = UBound(X, 1) - 1
    Case Else
        FindValueExtrapolated = FindValue(X, Xref, Y, Fail)
        On Error GoTo 0
        Exit Function
    End Select
    
    dGrad = (Y(i + 1) - Y(i)) / (X(i + 1) - X(i))
    dConst = Y(i) - dGrad * X(i)
            
    Fail = False
    FindValueExtrapolated = dGrad * Xref + dConst
    
    On Error GoTo 0
    Exit Function

ErrorRecover:
    Debug.Print "FindValueExtrapolated: Error " & Err.Description
    Debug.Assert 0
    On Error GoTo 0
    
    Fail = True
    Exit Function

End Function

Public Function FindPeak(ByRef Y, ByRef X, Optional ByRef Xpeak, Optional ByVal Sign As Integer, Optional ByRef Fail As Boolean, Optional ByVal DisableGradientCheck As Boolean) As Double
                                            '
    'Function to return value in Y() which
    'corresponds to the largest extrapolated
    'peak of Y().
    'The function will also return the
    'extrapolated value of X() which
    'corresponds to the Ypeak.
    
    'This method will fit a quadratic to
    'each three sets of points to work out
    'where the gradient is zero.
    'The peak value will be extrapolated
    'from the quadratic function fit.
    
    'Please note that this function will
    'only detect a peak, and not where the
    'value continually rises.
    
    'This method was designed by Ian Gurney.
    
    Dim bFound As Boolean
    Dim bValid As Boolean
    Dim dConst As Double
    Dim dError1 As Double
    Dim dError2 As Double
    Dim dPeak As Double
    Dim dPeakRef As Double
    Dim dProp As Double
    Dim dSquare As Double
    Dim l As Long
    Dim x0 As Double
    Dim x1 As Double
    Dim x12 As Double
    Dim x2 As Double
    Dim x23 As Double
    Dim x3 As Double
    Dim y0 As Double
    Dim y1 As Double
    Dim y2 As Double
    Dim y3 As Double
    
    Const nFraction As Single = 0
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    For l = LBound(Y, 1) + 1 To UBound(Y, 1) - 1
        
        'Read three points in array
        x1 = X(l - 1)
        x2 = X(l)
        x3 = X(l + 1)
        
        y1 = Y(l - 1)
        y2 = Y(l)
        y3 = Y(l + 1)
        
        If x1 <> x2 And x1 <> x3 And x2 <> x3 Then
            'Check to ensure that the gradient does change.
            
            dError1 = y2 - y1
            dError2 = y3 - y2
            
            If Abs(Sgn(dError1) + Sgn(dError2)) < 2 Or DisableGradientCheck Then
                'Solve the equation.
                
                Call QuadraticFit1(x1, y1, x2, y2, x3, y3, dSquare, dProp)
                
'               dSquare = (y1 * (x3 - x2) - y2 * (x3 - x1) + y3 * (x2 - x1)) / ((x2 - x1) * (x3 ^ 2 - x2 ^ 2) - (x3 - x2) * (x2 ^ 2 - x1 ^ 2))
'               dProp = (y1 * (x3 ^ 2 - x2 ^ 2) - y2 * (x3 ^ 2 - x1 ^ 2) + y3 * (x2 ^ 2 - x1 ^ 2)) / ((x1 * (x3 ^ 2 - x2 ^ 2) - x2 * (x3 ^ 2 - x1 ^ 2) + x3 * (x2 ^ 2 - x1 ^ 2)))
                
                'Check to make sure that there is a curve.
                
                If dSquare <> 0 Then
                    'Find the point where the gradient is zero.
                    'dy/dx = 0
                    
                    x0 = -dProp / (2 * dSquare)
                    
                    'Work out points greater than point 1
                    'and less than point 3.
                    
                    x12 = nFraction * X(l) + (1 - nFraction) * X(l - 1)
                    x23 = nFraction * X(l) + (1 - nFraction) * X(l + 1)
                    
                    dError1 = x0 - x12
                    dError2 = x0 - x23
                    
                    'The next line looks at the sign of each
                    'of the two errors.
                    'If the sum of them are less than two
                    'then we have found the point that we
                    'are looking for.
                    
                    If Abs(Sgn(dError1) + Sgn(dError2)) < 2 Then
'                       dConst = ((x2 * x1 ^ 2 - x1 * x2 ^ 2) * (x3 * y2 - x2 * y3) + (x3 * x2 ^ 2 - x2 * x3 ^ 2) * (x1 * y2 - x2 * y1)) / ((x2 * x1 ^ 2 - x1 * x2 ^ 2) * (x3 - x2) - (x3 * x2 ^ 2 - x2 * x3 ^ 2) * (x2 - x1))
'                       dConst = y2 - dSquare * x2 ^ 2 - dProp * x2
                        Call QuadraticFit1(x1, y1, x2, y2, x3, y3, , , dConst)
                        
                        y0 = dSquare * x0 ^ 2 + dProp * x0 + dConst
                        
'                       Debug.Print "FindPeak:", l, X(l), Format$(x0, "0.00E+00"), Format$(y0, "0.00E+00")
                        
                        Select Case Sgn(Sign)
                        Case 0
                            bValid = True
                        Case 1
                            bValid = (Sgn(dSquare) = -Sgn(Sign))
                        Case -1
                            bValid = (Sgn(dSquare) = -Sgn(Sign))
                        End Select
                        
                        If bValid Then
                            If bFound Then
                                Select Case Sgn(Sign)
                                Case 0
                                    bValid = Abs(y0) > Abs(dPeak)
                                Case 1
                                    bValid = y0 > dPeak
                                Case -1
                                    bValid = y0 < dPeak
                                End Select
                            End If
                            
                            If bValid Then
                                bFound = True             'Found a peak
                                dPeak = y0
                                dPeakRef = x0
                            End If
                        End If
                        
'                   Else
'                       Debug.Print "FindPeak:", l, X(l), Format$(x0, "0.00E+00"), "X"
                    End If
                End If
            End If
        End If
    Next l
    
    If bFound Then
        Xpeak = dPeakRef
        Fail = False
        FindPeak = dPeak
    Else
        Xpeak = 0
        Fail = True
    End If
    
    On Error GoTo 0
    Exit Function

ErrorRecover:
    Debug.Print "FindValue: Error " & Err.Description
    Debug.Assert 0
    
    Xpeak = 0
    On Error GoTo 0
    
    Fail = True
    Exit Function

End Function

Public Function FindPeak2(ByRef Y, ByRef X, Optional ByRef Xpeak, Optional ByVal Sign As Integer, Optional ByRef Fail As Boolean) As Double

    'This method was designed by Ian Gurney.
    
    Dim bFound As Boolean
    Dim bValid As Boolean
    Dim dMax As Double
    Dim dMaxRef As Double
    Dim l As Long
    Dim x0 As Double
    Dim y0 As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    'Search for an individual point.
    For l = LBound(Y, 1) To UBound(Y, 1)
        
        x0 = X(l)
        y0 = Y(l)
        
        'Check data point.
        If bFound Then
            Select Case Sgn(Sign)
            Case 0
                bValid = Abs(y0) > Abs(dMax)
            Case 1
                bValid = y0 > dMax
            Case -1
                bValid = y0 < dMax
            End Select
        Else
            bValid = True
        End If
        
        If bValid Then
            bFound = True             'Found a peak
            dMax = y0
            dMaxRef = x0
        End If
        
    Next l
    
    Xpeak = dMaxRef
    Fail = False
    On Error GoTo 0
    
    FindPeak2 = dMax
    Exit Function

ErrorRecover:
    Debug.Print "FindPeak2: Error " & Err.Description
    Debug.Assert 0
    
    Xpeak = 0
    On Error GoTo 0
    
    Fail = True
    Exit Function

End Function

Public Function FindMaximum(ByRef Y, ByRef X, Optional ByRef Xmax, Optional ByVal Sign As Integer, Optional ByVal DisableGradientCheck As Boolean) As Double

    'Function to find the maximum point in Y() array.
    
    'This can be the extrapolated peak, or actual point.
    
    'This method was designed by Ian Gurney.
    
    Dim bFail1 As Boolean
    Dim bFail2 As Boolean
    Dim bValid2 As Boolean
    Dim dMax1 As Double
    Dim dMax1Ref As Double
    Dim dMax2 As Double
    Dim dMax2Ref As Double
    
    'Execute FindPeak() function.
    dMax1 = FindPeak(Y, X, dMax1Ref, Sign, bFail1, DisableGradientCheck)
    
    'Execute FindPeak2() function.
    dMax2 = FindPeak2(Y, X, dMax2Ref, Sign, bFail2)
    
    'Check for both function errors.
    If bFail1 And bFail2 Then
        Xmax = 0
        Exit Function
    End If
    
    'If function 2 has errored use result for function 1.
    If bFail2 Then
        Xmax = dMax1Ref
        FindMaximum = dMax1
        Exit Function
    End If
    
    'If function 1 has errored use result for function 2.
    If bFail1 Then
        Xmax = dMax2Ref
        FindMaximum = dMax2
        Exit Function
    End If
    
    'Check if function 2 value is greater.
    Select Case Sgn(Sign)
    Case 0
        bValid2 = Abs(dMax2) > Abs(dMax1)
    Case 1
        bValid2 = dMax2 > dMax1
    Case -1
        bValid2 = dMax2 < dMax1
    End Select
    
    'If function 2 value is greater then use it.
    If bValid2 Then
        Xmax = dMax2Ref
        FindMaximum = dMax2
        Exit Function
    End If
    
    'Return value from function 1
    Xmax = dMax1Ref
    FindMaximum = dMax1

End Function

Public Function Smooth(ByRef Y, Optional Points As Integer) As Boolean
                                            '
    'Nortel test method.
    'This function will smooth Y() with
    'adjactent points in Y().
    
    'Please note that if you specify an even
    'number of points to smooth, then the
    'point itself in Y() will not be included
    'in the smoothed data.
    
    Dim bOdd As Boolean
    Dim dY() As Double
    Dim dSum As Double
    Dim l As Long
    Dim lOffset As Long
    Dim lSmooth As Long
    
    On Error GoTo ErrorRecover
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    'Default to 7 point smoothing if not set.
    If Points <= 0 Then Points = 7
    
    If (Points / 2) <> Int(Points / 2) Then bOdd = True
    lOffset = Int(Points / 2)
    
    'Make a copy of the Y() array
    ReDim dY(LBound(Y, 1) To UBound(Y, 1))
    
    For l = LBound(Y, 1) To UBound(Y, 1)
        dY(l) = Y(l)
    Next l
    
    'Redim the Y() array to the new size
    ReDim Y(LBound(Y, 1) + lOffset To UBound(Y, 1) - lOffset)
    
    For l = LBound(Y, 1) To UBound(Y, 1)
        
        dSum = 0
        
        For lSmooth = -lOffset To lOffset
            If lSmooth <> 0 Or bOdd Then
                dSum = dSum + dY(l + lSmooth)
            End If
        Next lSmooth
        
        Y(l) = dSum / Points
        
    Next l
    
    On Error GoTo 0
    
    Smooth = True
    Exit Function

ErrorRecover:
    Debug.Print "Smooth: Error " & Err.Description
    Debug.Assert 0
    
    'Don't redim Y() incase it is needed by calling context
    On Error GoTo 0
    Exit Function

End Function

Public Function StraightLineFit1(ByRef X, ByRef Y, ByRef y0) As Double
                                            '
    'Standard straight line fit, using the
    'method of least squares.
    'This function will return the gradient
    'of the straight line fit, and can also
    'return the Y intersect, Y0.
    
    'Please note that this method gives extra
    'weighting to points further away from
    'the line, so should not be consideded
    'very accurate.
    
    'Reference:
    'http://www.bjmath.com/bjmath/least/lsquare.htm
    
    Dim l As Long
    Dim lLBound As Long
    Dim lPoints As Long
    Dim lUBound As Long
    Dim dGradient As Double
    Dim dSumX As Double
    Dim dSumY As Double
    Dim dSumXX As Double
    Dim dSumXY As Double
    
    On Error GoTo ErrorRecover
    
    If (VarType(X) And vbArray) = 0 Then
        Err.Description = "X parameter is not an array"
        Err.Raise vbObjectError + 1
    End If
    
    If (VarType(Y) And vbArray) = 0 Then
        Err.Description = "Y parameter is not an array"
        Err.Raise vbObjectError + 2
    End If
    
    'Pick points where data coincides
    lLBound = LBound(X, 1)
    If LBound(Y, 1) > lLBound Then lLBound = LBound(Y, 1)
    
    lUBound = UBound(X, 1)
    If UBound(Y, 1) < lUBound Then lUBound = UBound(Y, 1)
    
    'Perform calculation
    For l = lLBound To lUBound
        dSumX = dSumX + X(l)
        dSumY = dSumY + Y(l)
        dSumXX = dSumXX + X(l) * X(l)
        dSumXY = dSumXY + X(l) * Y(l)
    Next l
    
    lPoints = lUBound - lLBound + 1
    
    'Calculate gradient and intercept
    dGradient = ((lPoints * dSumXY) - (dSumX * dSumY)) / ((lPoints * dSumXX) - (dSumX * dSumX))
    y0 = (dSumY - (dGradient * dSumX)) / lPoints
    
    On Error GoTo 0
    
    StraightLineFit1 = dGradient
    Exit Function

ErrorRecover:
    Debug.Print "StraightLineFit1: Error " & Err.Description
    Debug.Assert 0
    
    y0 = 0
    On Error GoTo 0
    
    StraightLineFit1 = 0
    Exit Function

End Function

Public Function Log10(ByRef Number, Optional ByRef Error As Long) As Double

    'Number can only be any value or 1D array
    'Use static to record Log(10) to save of time
    
    Static dLog10 As Double
    Dim l As Long
    
    If dLog10 = 0 Then dLog10 = Log(10)
    
    On Error GoTo ErrorRecover
    
    If (VarType(Number) And vbArray) = 0 Then
        Log10 = Log(Number) / dLog10
        On Error GoTo 0
        
        Error = 0
        Exit Function
    End If
    
    For l = LBound(Number, 1) To UBound(Number, 1)
        Number(l) = Log(Number) / dLog10
    Next l
    
    On Error GoTo 0
    
    Error = 0
    Log10 = True
    Exit Function

ErrorRecover:
    Error = Err.Number
    
    Debug.Print "Log10: Error " & Err.Description
'   Debug.Assert 0
    
    On Error GoTo 0
    Exit Function

End Function

Public Function Sort(ByRef X, ByRef Y) As Boolean

    'Function to sort data.
    
    'Code supports 1D or 2D Y array data.
    
    'Code originated from the IJ Gurney Lottery program.
    'http://www.iangurney.co.uk/go.htm?to=lottery.htm&name=Nortel%20Networks%20Software%20Engineer
    
    'This method was designed by Ian Gurney.
    
    Dim i As Integer
    Dim iX As Integer
    Dim iXmin As Integer
    Dim iY As Integer
    Dim iY2D As Integer
    Dim dXmin As Double
    Dim vY() As Variant
    
    On Error Resume Next
    iY2D = UBound(Y, 2) - LBound(Y, 2) + 1
    On Error GoTo 0
    
    If iY2D = 0 Then
        ReDim vY(1)
    Else
        ReDim vY(LBound(Y, 2) To UBound(Y, 2))
    End If
    
    On Error GoTo ErrorRecover
    
    For iX = LBound(X, 1) To UBound(X, 1)
        For i = iX To UBound(X, 1)
            If i = iX Then
                dXmin = X(i)
                iXmin = i
            Else
                If X(i) < dXmin Then
                    dXmin = X(i)
                    iXmin = i
                End If
            End If
        Next i
        
        If iY2D Then
            For iY = LBound(Y, 2) To UBound(Y, 2)
                vY(iY) = Y(iXmin, iY)
            Next iY
        Else
            vY(1) = Y(iXmin)
        End If
        
        For i = iXmin To iX + 1 Step -1
            X(i) = X(i - 1)
            
            If iY2D Then
                For iY = LBound(Y, 2) To UBound(Y, 2)
                    Y(i, iY) = Y(i - 1, iY)
                Next iY
            Else
                Y(i) = Y(i - 1)
            End If
        Next i
        
        X(iX) = dXmin
        
        If iY2D Then
            For iY = LBound(Y, 2) To UBound(Y, 2)
                Y(iX, iY) = vY(iY)
            Next iY
        Else
            Y(iX) = vY(1)
        End If
    Next iX
    
    On Error GoTo 0
    
    Sort = True
    Exit Function

ErrorRecover:
    Debug.Print "Sort: Error " & Err.Description
    Debug.Assert 0
    
    On Error GoTo 0
    Exit Function
    
    Resume

End Function

Public Function QuadraticFit1(ByVal x1, ByVal y1, ByVal x2, ByVal y2, ByVal x3, ByVal y3, Optional ByRef Quadratic, Optional ByRef Proportional, Optional ByRef Constant) As Boolean

    'Solves the equation: y = mx + nx2 + c
    
    On Error GoTo ErrorRecover
    
    If Not IsMissing(Quadratic) Or Not IsMissing(Constant) Then
        Quadratic = (y1 * (x3 - x2) - y2 * (x3 - x1) + y3 * (x2 - x1)) / ((x2 - x1) * (x3 ^ 2 - x2 ^ 2) - (x3 - x2) * (x2 ^ 2 - x1 ^ 2))
    End If
    
    If Not IsMissing(Proportional) Or Not IsMissing(Constant) Then
        Proportional = (y1 * (x3 ^ 2 - x2 ^ 2) - y2 * (x3 ^ 2 - x1 ^ 2) + y3 * (x2 ^ 2 - x1 ^ 2)) / ((x1 * (x3 ^ 2 - x2 ^ 2) - x2 * (x3 ^ 2 - x1 ^ 2) + x3 * (x2 ^ 2 - x1 ^ 2)))
    End If
    
    If Not IsMissing(Constant) Then
'       Constant = ((x2 * x1 ^ 2 - x1 * x2 ^ 2) * (x3 * y2 - x2 * y3) + (x3 * x2 ^ 2 - x2 * x3 ^ 2) * (x1 * y2 - x2 * y1)) / ((x2 * x1 ^ 2 - x1 * x2 ^ 2) * (x3 - x2) - (x3 * x2 ^ 2 - x2 * x3 ^ 2) * (x2 - x1))
        
        'The above equation sometimes gives the value 0 / 0 which will cause an overflow.
        'Instead rearrange the equation to give: c = y - mx - nx2
        Constant = y2 - Proportional * x2 - Quadratic * x2 ^ 2
    End If
    
    On Error GoTo 0
    
    QuadraticFit1 = True
    Exit Function

ErrorRecover:
    Debug.Print "QuadraticFit1: Error " & Err.Description
    Debug.Assert 0
    On Error GoTo 0

End Function


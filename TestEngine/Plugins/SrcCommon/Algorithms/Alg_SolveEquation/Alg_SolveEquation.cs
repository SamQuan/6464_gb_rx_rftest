// Author: Cypress
// Design: basic theory

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// 
    /// </summary>
    public class Alg_SolveEquation
    {

        /// <summary>
        /// get x value from the equation y=a*x+b
        /// where a,b,y values are known.
        /// </summary>
        /// <param name="a">a value</param>
        /// <param name="b">b value</param>
        /// <param name="y">y value</param>
        /// <returns>x value</returns>
        public static double SolveSimpleEquation(double a, double b, double y)
        {
            if (a == 0)
            {
                throw new ArgumentOutOfRangeException("a", a, "a cannot be 0.");
            }
            double x = (y - b) / a;

            return x;
        }

        /// <summary>
        /// get x values from the equation y=a*x^2+b*x+c
        /// where a,b,c,y values are known
        /// </summary>
        /// <param name="a">a value</param>
        /// <param name="b">b value</param>
        /// <param name="c">c value</param>
        /// <param name="y">y value</param>
        /// <param name="x1LessOne">x1 value which is the less one</param>
        /// <param name="x2LargerOne">x2 value which is the larger one</param>
        public static void SolveQuadraticEquation(double a, double b, double c, double y, out double x1LessOne, out double x2LargerOne)
        {

            //it's a simple equation
            if (a == 0)
            {
                x1LessOne = SolveSimpleEquation(b, c, y);
                x2LargerOne = x1LessOne;
                return;
            }

            double root = b * b - 4 * a * (c - y);

            //there is no real number solving
            if (root < 0)
            {
                throw new AlgorithmException("There is no real number solving for the equation.");
            }

            x1LessOne = (-b - Math.Sqrt(root)) / (2 * a);
            x2LargerOne = (-b + Math.Sqrt(root)) / (2 * a);


        }



    }
}

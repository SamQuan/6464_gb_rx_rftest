// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// SteinhartHart.cs
//
// Author: Joseph Olajubu
// Design:  

using System;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// This class contains static methods for implementing conversion 
    /// of Thermistor Resistance to Temperature and Vice versa. 
    /// </summary>
    public class SteinhartHart
    {
        #region Private Constructor
        private SteinhartHart() 
        {        
        }
        #endregion
        
        #region Public Methods
        
        
        /// <summary>
        /// Convert a temperature in Degrees Celsius to a Thermistor Resistance (in ohms)
        /// using the Steinhart-Hart equation, stated in terms of resistance:
        /// R = e^(((-X/2 + Z)^1/3 + ((-X/2 - Z)^1/3) 
        /// 
        /// where X =  (A - 1/T)/C, Z = Sqrt((X^2)/4 + (Y^3)/27)), and Y = B/C.
        /// 
        /// Note that T in the above equation is in Kelvins.
        /// </summary>
        /// <param name="temperature">The Temperature in Celsius.</param>
        /// <param name="a">Steinhart-Hart Constant A</param>
        /// <param name="b">Steinhart-Hart Constant B</param>
        /// <param name="c">Steinhart-Hart Constant C</param>
        /// <returns>Calculated Resistance (ohms)</returns>
        public static double TemperatureCelsiusToResistanceOhms(double temperature, double a, double b, double c)
        {
            double tempKelvins = temperature + 273.15;
            double x = (a - (1 / tempKelvins)) / c;
            double y = b / c;
            double z = Math.Sqrt((Math.Pow(x, 2) / 4) + (Math.Pow(y, 3) / 27));
            double resistance = Math.Exp(CalcCubeRoot((-x / 2) + z) + CalcCubeRoot((-x / 2) - z));

            return (resistance);
        }

        /// <summary>
        /// Convert a thermistor resistnce Value in ohms into a Temperature in 
        /// Degrees Celsius, using the Steinhart-Hart equation: 1/T = A + B.lnR + C.(lnR)^3
        /// 
        /// Note that T in the above equation is in Kelvins.
        /// </summary>
        /// <param name="resistance">Resistance (ohms) to be converted.</param>
        /// <param name="a">Steinhart-Hart Constant A</param>
        /// <param name="b">Steinhart-Hart Constant B</param>
        /// <param name="c">Steinhart-Hart Constant C</param>
        /// <returns>The Temperature in Celsius.</returns>
        public static double ResistanceOhmsToTemperatureCelsius(double resistance, double a, double b, double c)
        {
            //Get lnR. 
            double lnR = Math.Log(resistance);

            // Do 1/T = A + B.lnR + C.(lnR)^3
            double inverseTemperature_Kelvin = a + (b * lnR) + (c * Math.Pow(lnR, 3));

            //Return the temperature in Degrees Celsius.
            return ((1 / inverseTemperature_Kelvin) - 273.15);
        }

        /// <summary>
        /// Calculates the Cube root according to the sign of the input.
        /// </summary>
        /// <param name="x">Number to take the Cube root of</param>
        /// <returns>The result.</returns>
        private static double CalcCubeRoot(double x)
        {
            double dtmpX = 0;
            double dtmpY = 0;

            dtmpY = 1.0/3.0;

            if (x < 0)
            {
                dtmpX = x * -1;
                double _res = Math.Pow(dtmpX, dtmpY);
                return (_res * -1);
            }
            return (Math.Pow(x, dtmpY));
        }

        #endregion
    }
}

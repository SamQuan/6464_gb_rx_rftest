// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// CallendarVanDusen.cs
//
// Author: Joseph Olajubu
// Design:  

using System;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// This class contains static methods for implementing conversion 
    /// of Platinum RTD Sensor Resistance to Temperature and Vice versa. 
    /// 
    /// See http://content.honeywell.com/sensing/prodinfo/temperature/technical/c15_136.pdf 
    /// for details of the Callendar-Van Dusen equation used in this class.
    /// 
    /// </summary>
    public sealed class CallendarVanDusen
    {
        #region Private Constructor
        private CallendarVanDusen() 
        {    
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Convert a temperature in Degrees Celsius to a RTD Sensor Resistance (in ohms)
        /// using the Callendar-Van Dusen  equation.
        /// 
        /// 
        /// This is stated in terms of resistance:
        /// R = R0(1 + AT + BT^2 - 100CT^3 + CT^4)
        /// 
        /// where R0 = Resistance at 0 degrees Celsius (ohms)
        ///       A = alpha + (alpha * delta)/100
        ///       B = (-alpha * delta)/100^2
        ///       C (if  T less than 0)  = (-alpha * beta)/100^4, else C = 0
        /// 
        ///       and R0, alpha, beta and delta are Constants.
        /// 
        /// </summary>
        /// <param name="temperature">The Temperature in Celsius.</param>
        /// <param name="r0">Callendar-Van Dusen Constant R0</param>
        /// <param name="alpha">Callendar-Van Dusen Constant alpha</param>
        /// <param name="beta">Callendar-Van Dusen Constant beta. Only used when Temperature less than 0.</param>
        /// <param name="delta">Callendar-Van Dusen Constant delta</param>
        /// <returns>Calculated Resistance (ohms)</returns>        
        public static double TemperatureCelsiusToResistanceOhms(double temperature, double r0, double alpha, double beta, double delta)
        {
             
            //Calculate the constants for the equation using input parameters
            double a, b, c;
            
            a = calculateConstantA (alpha, delta);
            b = calculateConstantB (alpha, delta);

            if (temperature >= 0)
            {
                c = 0;
            }
            else
            {
                c = (-alpha * beta) / Math.Pow(100, 4);
            }
            
             // Do R = R0(1 + AT + BT^2 - 100CT^3 + CT^4) and return the result
            double resistance = r0 * (1 + (a * temperature)
                                        + (b * Math.Pow(temperature, 2))
                                        - (100 * c * Math.Pow(temperature, 3))
                                        + (c * Math.Pow(temperature, 4))); 
           
            return (resistance);
        }

        /// <summary>
        /// WARNING!!! this algorithm can only be used where Temperature > 0.
        /// 
        /// Convert a RTD resistance Value in ohms into a Temperature in 
        /// Degrees Celsius, using the Callendar-Van Dusen equation: 
        /// T = ((-R0 * A) + sqrt ((R0^2 * A^2) - (4*R0*B *(R0 - RT ))))/(2* R0 * B)
        /// 
        /// where RT = resistance at required temperature (ohms)
        ///       R0 = Resistance of sensor at 0 degrees Celsius (ohms)
        ///       A = alpha + (alpha * delta)/100
        ///       B = (-alpha * delta)/100^2
        /// 
        ///       and alpha, beta and delta are Constants.
        /// 
        /// </summary>
        /// <param name="resistance">Resistance (ohms) to be converted.</param>
        /// <param name="r0">Callendar-Van Dusen Constant R0</param>
        /// <param name="alpha">Callendar-Van Dusen Constant alpha</param>
        /// <param name="beta">Callendar-Van Dusen Constant beta</param>
        /// <param name="delta">Callendar-Van Dusen Constant delta</param>
        /// <returns>The Temperature in Celsius.</returns>
        public static double ResistanceOhmsToTemperatureCelsius(double resistance, double r0, double alpha, double beta, double delta)
        {
            //Calculate the constants for the equation using input parameters
            double a = calculateConstantA(alpha, delta);
            double b = calculateConstantB(alpha, delta);

            // Do T = ((-R0 * A) + sqrt ((R0^2 * A^2) - (4*R0*B *(R0 - RT ))))/(2* R0 * B)
            double temperature = ((-r0 * a) + Math.Sqrt((Math.Pow(r0, 2) * Math.Pow(a, 2)) - (4 * r0 * b *(r0 - resistance))))/(2*r0*b);

            if (temperature < 0)
            {
                throw new Algorithms.AlgorithmException("Unable to convert resistance to a Temperature below 0 degreess celsius accurately.");
            }

            // Return the result in degrees Celsius.
            return (temperature);
        }

        #endregion 

        #region Private Methods
        /// <summary>
        /// Calculates the "A" constant for the callendar-Van Dusen Equation using the 
        /// following formula:
        /// A = alpha + (alpha * delta)/100
        /// </summary>
        /// <param name="alpha">alpha</param>
        /// <param name="delta">delta</param>
        /// <returns>A -  the constant</returns>
        private static double calculateConstantA(double alpha, double delta)
        {
            return (alpha + ((alpha * delta) / 100));
        }


        /// <summary>
        /// Calculates the "B" constant for the callendar-Van Dusen Equation using the 
        /// following formula:
        /// B = (-alpha * delta)/100^2
        /// </summary>
        /// <param name="alpha">alpha</param>
        /// <param name="delta">delta</param>
        /// <returns>B -  the constant</returns>
        private static double calculateConstantB(double alpha, double delta)
        {
            return ((-alpha * delta)/10000);
        }

        #endregion
    }
}

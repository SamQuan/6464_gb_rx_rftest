// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_FindFeature.cs
//
// Author: Mark Fullalove
// Design: From Bookham "PcLeslie" software

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Class to find the absolute maximum value within a one dimensional array
    /// </summary>
    public sealed class Alg_FindFeature
    {
        /// <summary>
        /// Empty private constructor
        /// </summary>
        private Alg_FindFeature()
        {
        }


        /// <summary>
        /// Function to find the absolute maximum value within a one dimensional array
        /// </summary>
        /// <param name="oneDArray">The one dimensional array we wish to search</param>
        /// <returns>The value furthest from 0 in either a positive or a negative direction.</returns>
        public static double FindAbsoluteMaxWithSign(double[] oneDArray)
        {
            if (oneDArray.Length == 0)
            {
                throw new AlgorithmException("FindMaxValueInArray:  Empty array");
            }

            double maxValue = Alg_PointSearch.FindMaxValueInArray(oneDArray);
            double minValue = Alg_PointSearch.FindMinValueInArray(oneDArray);

            return Math.Abs(maxValue) > Math.Abs(minValue) ? maxValue : minValue;
        }

        /// <summary>
        /// Function to find the absolute minimum value within a one dimensional array
        /// </summary>
        /// <param name="oneDArray">The one dimensional array we wish to search</param>
        /// <returns>The value closest to 0 in either a positive or a negative direction.</returns>
        public static double FindAbsoluteMinWithSign(double[] oneDArray)
        {
            if (oneDArray.Length == 0)
            {
                throw new AlgorithmException("FindMinValueInArray:  Empty array");
            }

            double minValue = double.MaxValue;
            double absMinValue = double.MaxValue;

            foreach (double thisValue in oneDArray)
            {
                if ( Math.Abs(thisValue) < absMinValue)
                {
                    absMinValue = Math.Abs(thisValue);
                    minValue = thisValue;
                }
            }

            return minValue;
        }

        /// <summary>
        /// Returns the number with the greatest magnitude.
        /// </summary>
        /// <param name="a">A number</param>
        /// <param name="b">Another number</param>
        /// <returns>The number with the greatest deviation from 0</returns>
        public static double FindAbsoluteMaxWithSign(double a, double b)
        {
            return Math.Abs(a) > Math.Abs(b) ? a : b;
        }

        /// <summary>
        /// Determine the location of peaks by comparing each point to its adjacent values.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <param name="numberOfPointsToUse">The number of points to check either side of each potential peak.</param>
        /// <returns>An array containing the indexes of peaks</returns>
        public static int[] FindIndexesForAllPeaks(double[] dataSet, int numberOfPointsToUse)
        {
            return findIndexesForAllExtrema(dataSet, numberOfPointsToUse, PeakOrValley.Peak );
        }

        /// <summary>
        /// Determine the location of peaks by comparing each point to three neighbouring points either side.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <returns>An array containing the indexes of peaks</returns>
        public static int[] FindIndexesForAllPeaks(double[] dataSet)
        {
            return findIndexesForAllExtrema(dataSet, 3, PeakOrValley.Peak );
        }

        /// <summary>
        /// Determine the location of valleys by comparing each point to its adjacent values.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <param name="numberOfPointsToUse">The number of points to check either side of each potential valley.</param>
        /// <returns>An array containing the indexes of the valleys</returns>
        public static int[] FindIndexesForAllValleys(double[] dataSet, int numberOfPointsToUse)
        {
            return findIndexesForAllExtrema(dataSet, numberOfPointsToUse, PeakOrValley.Valley );
        }

        /// <summary>
        /// Determine the location of valleys by comparing each point to three neighbouring points either side.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <returns>An array containing the indexes of the valleys</returns>
        public static int[] FindIndexesForAllValleys(double[] dataSet)
        {
            return findIndexesForAllExtrema(dataSet, 3, PeakOrValley.Valley );
        }

        /// <summary>
        /// Finds the index of the peak having the maximum value.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <param name="numberOfPointsToUse">The number of points to check either side of each potential peak.</param>
        /// <returns>The index of the highest peak</returns>
        public static int FindIndexForMaxPeak(double[] dataSet, int numberOfPointsToUse)
        {
            int[] pointsOfInterest = findIndexesForAllExtrema(dataSet, numberOfPointsToUse, PeakOrValley.Peak);
            
            int maxPeakIndex = pointsOfInterest[0];
            double maxValue = double.MinValue;

            foreach (int index in pointsOfInterest)
            {
                if (dataSet[index] > maxValue)
                {
                    maxValue = dataSet[index];
                    maxPeakIndex = index;
                }
            }
            return maxPeakIndex;
        }

        /// <summary>
        /// Finds the index of the peak having the maximum value by comparing each point to three adjacent points.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <returns>The index of the highest peak</returns>
        public static int FindIndexForMaxPeak(double[] dataSet)
        {
            return FindIndexForMaxPeak(dataSet, 3);
        }


        /// <summary>
        /// Finds the index of the valley having the minimum value.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <param name="numberOfPointsToUse">The number of points to check either side of each potential valley.</param>
        /// <returns>The index of the lowest valley</returns>
        public static int FindIndexForMinValley(double[] dataSet, int numberOfPointsToUse)
        {
            int[] pointsOfInterest = findIndexesForAllExtrema(dataSet, numberOfPointsToUse, PeakOrValley.Valley);

            int maxPeakIndex = pointsOfInterest[0];
            double maxValue = double.MaxValue;

            foreach (int index in pointsOfInterest)
            {
                if (dataSet[index] < maxValue)
                {
                    maxValue = dataSet[index];
                    maxPeakIndex = index;
                }
            }
            return maxPeakIndex;
        }

        /// <summary>
        /// Finds the index of the valley having the minimum value by comparing each point to three naighbouring values.
        /// </summary>
        /// <param name="dataSet">Array of data to be searched</param>
        /// <returns>The index of the lowest valley</returns>
        public static int FindIndexForMinValley(double[] dataSet)
        {
            return FindIndexForMinValley(dataSet, 3);
        }

        #region Private Methods.

        /// <summary>
        /// Determine whether a value is a peak by comparing it against neighbouring values
        /// </summary>
        /// <param name="indexToCheck">The index containing the value to be checked</param>
        /// <param name="pointsToUse">The number of data points either side to check</param>
        /// <param name="dataSet">The array of data</param>
        /// <returns>TRUE if the point has a higher value than its neighbours</returns>
        private static bool IsPeak(int indexToCheck, int pointsToUse, double[] dataSet)
        {
            bool peak = true;

            for (int i = indexToCheck - pointsToUse; i <= indexToCheck + pointsToUse; i++)
            {
                if (i >= 0 && i <= dataSet.Length)              // Within bounds
                {
                    if (dataSet[i] > dataSet[indexToCheck])     // Value at indexToCheck must be the bigger number if it is a peak.
                    {
                        peak = false;
                    }
                }
            }

            return peak;
        }

        /// <summary>
        /// Determine whether a value is a valley by comparing it against neighbouring values
        /// </summary>
        /// <param name="indexToCheck">The index containing the value to be checked</param>
        /// <param name="pointsToUse">The number of data points either side to check</param>
        /// <param name="dataSet">The array of data</param>
        /// <returns>TRUE if the point has a lower value than its neighbours</returns>
        private static bool IsValley(int indexToCheck, int pointsToUse, double[] dataSet)
        {
            bool valley = true;

            for (int i = indexToCheck - pointsToUse; i <= indexToCheck + pointsToUse; i++)
            {
                if (i >= 0 && i <= dataSet.Length)              // Within bounds
                {
                    if (dataSet[i] < dataSet[indexToCheck])     // Value at indexToCheck must be the smaller number if it is a peak.
                    {
                        valley = false;
                    }
                }
            }

            return valley;
        }

        /// <summary>
        /// Finds all indexes for all peaks or valleys
        /// </summary>
        /// <param name="dataSet">Data to be analysed</param>
        /// <param name="pointsToUse">Number of points to check either side of each potential extrema</param>
        /// <param name="mode">Peak or Valley</param>
        /// <returns>An array of peaks or valleys.</returns>
        private static int[] findIndexesForAllExtrema(double[] dataSet, int pointsToUse, PeakOrValley mode)
        {
            if (dataSet.Length == 0)
            {
                throw new AlgorithmException("Alg_FindFeature: Empty array");
            }

            ArrayList pointsOfInterest = new ArrayList();
            for (int i = pointsToUse; i < dataSet.Length - pointsToUse; i++)
            {
                if (mode == PeakOrValley.Peak)
                {
                    if (IsPeak(i, pointsToUse, dataSet))
                    {
                        pointsOfInterest.Add(i);
                    }
                }
                else
                {
                    if (IsValley(i, pointsToUse, dataSet))
                    {
                        pointsOfInterest.Add(i);
                    }
                }
            }

            int[] indexList = (int[])pointsOfInterest.ToArray(typeof(int));

            return indexList;
        }


        /// <summary>
        /// Enumeration for search mode.
        /// </summary>
        private enum PeakOrValley
        {
            /// <summary>
            /// A maximum turning point
            /// </summary>
            Peak,
            /// <summary>
            /// A minumum turning point
            /// </summary>
            Valley
        }
        #endregion

    }
}

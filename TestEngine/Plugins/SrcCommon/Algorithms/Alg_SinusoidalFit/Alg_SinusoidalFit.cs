// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_SinusoidalFit.cs
//
// Author: Mark Fullalove
// Design: 
//
// Notes : This project supplies a delegate function which uses pointers.
// In order to compile the "Allow unsafe code" option must be selected in the build settings for this project.


using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Algorithms
{
    public class Alg_SinusoidalFit
    {
        Alg_SinusoidalFit()
        {
        }

        /// <summary>
        /// Performs a non-linear fit using a sinusoidal model function.
        /// </summary>
        /// <param name="xData">X axis data to be fitted</param>
        /// <param name="yData">Y axis data to be fitted</param>
        /// <param name="startingCoeffs">An array of coefficients to control the following parameters in the model function : offset, amplitude, power, phase, period</param>
        /// <returns>An structure containing the results of the fit.</returns>
        public static SinusoidalFit PerformFit(double[] xData, double[] yData, double[] startingCoeffs)
        {
            applyConstraints = false;
            return performLMFit(xData, yData, startingCoeffs);
        }

        /// <summary>
        /// Performs a non-linear fit using a sinusoidal model function.
        /// </summary>
        /// <param name="xData">X axis data to be fitted</param>
        /// <param name="yData">Y axis data to be fitted</param>
        /// <param name="startingCoeffs">An array of coefficients to control the following parameters in the model function : offset, amplitude, power, phase, period</param>
        /// <param name="coefficientUpperConstraints">An array containing the same number of elements as startingCoeffs. Use Double.MaxValue for elements that have no constraint.</param>
        /// <param name="coefficientLowerConstraints">An array containing the same number of elements as startingCoeffs. Use Double.MinValue for elements that have no constraint.</param>
        /// <returns>An structure containing the results of the fit.</returns>
        public static SinusoidalFit PerformFit(double[] xData, double[] yData, double[] startingCoeffs, double[] coefficientUpperConstraints, double[] coefficientLowerConstraints)
        {
            applyConstraints = true;

            if ( startingCoeffs == null || coefficientLowerConstraints == null || coefficientUpperConstraints == null )
                throw new System.ArgumentException("Neither the coefficient array nor the constraint array may be null.");

            if (coefficientLowerConstraints.Length != coefficientUpperConstraints.Length || coefficientLowerConstraints.Length != startingCoeffs.Length)
                throw new System.ArgumentException("The number of upper and lower constraints must match the number of coefficients.");

            upperConstraints = coefficientUpperConstraints;
            lowerConstraints = coefficientLowerConstraints;

            return performLMFit(xData, yData, startingCoeffs);
        }

        unsafe static private SinusoidalFit performLMFit(double[] xData, double[] yData, double[] coeffs)
        {
            if ( xData == null || yData == null )
                throw new System.ArgumentException("The X and Y data arrays cannot be null.");

            if ( xData.Length != yData.Length )
                throw new System.ArgumentException("The X and Y data arrays must be of the same size.");

            SinusoidalFit sinusoidalFit = new SinusoidalFit();

            sinusoidalFit.MeanSquaredError = Alg_LMFit.PerformFit(xData, yData, ref coeffs, model);
            sinusoidalFit.Coeffs = coeffs;
            sinusoidalFit.FittedYArray = new double[xData.Length];

            fixed (double* coeffPtr = coeffs)
            {
                for (int i = 0; i < coeffs.Length; i++)
                {
                    coeffPtr[i] = coeffs[i];
                }

                for (int i = 0; i < xData.Length; i++)
                {
                    sinusoidalFit.FittedYArray[i] = model(xData[i], coeffPtr);
                }
            }

            return sinusoidalFit;
        }

        unsafe static private double model(double xPoint, double* coeffs)
        {           
            // attempt to apply constraints
            if (applyConstraints)
            {
                for (int i = 0; i < upperConstraints.Length - 1; i++)
                {
                    coeffs[i] = Math.Min(coeffs[i], upperConstraints[i]);
                    coeffs[i] = Math.Max(coeffs[i], lowerConstraints[i]);
                }
            }

            double scaledCurrent = Math.Sign(xPoint) * Math.Pow(Math.Abs(xPoint), coeffs[2]);
            double sinTerm = (scaledCurrent - coeffs[3]) * Math.PI * 2 / coeffs[4];
            return coeffs[0] + coeffs[1] * Math.Sin(sinTerm);
        }

        private static bool applyConstraints;
        private static double[] upperConstraints;
        private static double[] lowerConstraints;
    }

    public struct SinusoidalFit
    {
        /// <summary>
        /// Array of Y points generated by the fit routine
        /// </summary>
        public double[] FittedYArray;
        /// <summary>
        /// Coefficients for the model function
        /// </summary>
        public double[] Coeffs;
        /// <summary>
        /// MeanSquaredError
        /// </summary>
        public double MeanSquaredError;
    }
}

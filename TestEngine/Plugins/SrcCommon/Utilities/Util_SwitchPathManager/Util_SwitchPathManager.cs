using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Equipment;
using System.Data;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Switch Path Manager utility
    /// </summary>
    public class Util_SwitchPathManager
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configFile">Switch path config file</param>
        /// <param name="instrColl">Instrument collection</param>
        public Util_SwitchPathManager(string configFile, InstrumentCollection instrColl)
        {
            // initialise the switch position matrix
            this.switchPositionMatrix = new Dictionary<string, int[]>();
            // load switch config
            DataTable dataTable = loadConfigFile(configFile, instrColl);
            // validate that all the positions are in bounds
            validatePosnBounds(dataTable);
        }

        /// <summary>
        /// The meta-value in switch position to ignore that switch for this position
        /// </summary>
        public const int IgnoreSwPosn = -1;

        #region Private Data
        /// <summary>
        /// Switch path data as a Data Table
        /// </summary>
        private Dictionary<string, int[]> switchPositionMatrix;
        /// <summary>
        /// Switch Names
        /// </summary>
        private string[] switchNames;
        /// <summary>
        /// List of switches - keep original order
        /// </summary>
        private IInstType_SimpleSwitch[] switches;
        #endregion

        #region Public Methods/Functions
        /// <summary>
        /// Switch names
        /// </summary>
        public string[] SwitchNames
        {
            get
            {
                return switchNames;
            }
        }

        /// <summary>
        /// Valid Switch Positions
        /// </summary>
        public string[] ValidPosnNames
        {
            get
            {
                List<string> posnNames = new List<string>(switchPositionMatrix.Count);
                foreach (KeyValuePair<string, int[]> posn in switchPositionMatrix)
                {
                    posnNames.Add(posn.Key);
                }
                return posnNames.ToArray();
            }
        }

        /// <summary>
        /// Get a switch by name
        /// </summary>
        /// <param name="switchName">Switch Name</param>
        /// <returns>Switch Definition object</returns>
        public IInstType_SimpleSwitch GetSwitch(string switchName)
        {
            foreach (IInstType_SimpleSwitch swDef in this.switches)
            {
                if (swDef.Name == switchName) return swDef;
            }
            // if got here didn't find it!
            throw new Util_SwitchPathManagerException("Couldn't find switch '" + switchName + "'!");
        }

        /// <summary>
        /// Set switches to named position
        /// </summary>
        /// <param name="posnName">Position name</param>
        public void SetToPosn(string posnName)
        {
            int[] swPosns = switchPositionMatrix[posnName];
            for (int ii = 0; ii < this.switches.Length; ii++)
            {
                // get the value of the switch position
                int val = swPosns[ii];
                // do we ignore this switch for this switch path
                if (val == IgnoreSwPosn) continue;

                // set the switch
                IInstType_SimpleSwitch swDef = switches[ii];
                swDef.SwitchState = val;
            }
        }

        /// <summary>
        /// Get the required switch position for the compound switch position name and a single named switch.
        /// </summary>
        /// <param name="posnName">Name of the switch position</param>
        /// <param name="switchName">Switch Name</param>
        /// <returns></returns>
        public int GetSwitchPosition(string posnName, string switchName)
        {
            // find which index (column) this switch is
            int counter = 0;
            int switchIndex = -1;
            foreach (IInstType_SimpleSwitch swDef in this.switches)
            {
                if (swDef.Name == posnName)
                {
                    switchIndex = counter;
                    break;
                }
                counter++;
            }
            if (switchIndex == -1)
            {
                throw new Util_SwitchPathManagerException("Couldn't find switch of name: " + switchName);
            }
            int[] swPosns = switchPositionMatrix["posnName"];
            // find the position
            int val = swPosns[switchIndex];
            return val;
        }

        #endregion

        #region Helper Fns
        /// <summary>
        /// Load the configuration file and validate it against the Instrument collection provided
        /// </summary>
        /// <param name="configFile">Config file</param>
        /// <param name="instrColl">Instrument collection</param>
        /// <returns>DataTable from XML file</returns>
        private DataTable loadConfigFile(string configFile, InstrumentCollection instrColl)
        {
            // Load the XML file into a DataSet object
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(configFile);
            // check we have the correct switch configuration table
            if (!dataSet.Tables.Contains("SwitchConfig"))
            {
                throw new Util_SwitchPathManagerException("Invalid switch configuration file - missing 'SwitchConfig' table");
            }
            DataTable dataTable = dataSet.Tables["SwitchConfig"];

            // find names of the columns - these are a header "PosnName" one column per switch
            int nbrCols = dataTable.Columns.Count;
            if (nbrCols == 0)
            {
                throw new Util_SwitchPathManagerException("No columns found in 'SwitchConfig' table");
            }
            // check 1st column is PosnName
            DataColumn firstCol = dataTable.Columns[0];
            if ((firstCol.ColumnName != "PosnName") || (firstCol.DataType != typeof(string)))
            {
                throw new Util_SwitchPathManagerException("Invalid switch configuration file - 'PosnName' column must be of type string");
            }
            // initialise the switch array
            this.switches = new IInstType_SimpleSwitch[nbrCols - 1];
            // look through the other columns    
            for (int ii=0; ii<nbrCols-1; ii++)
            {
                DataColumn aCol = dataTable.Columns[ii+1];
                // process the column name
                string[] columnInfo = aCol.ColumnName.Split(':');
                string instName = columnInfo[0];
                // find the instrument from the collection
                Instrument instr = instrColl[instName];
                // switch definition remembers what type we are
                IInstType_SimpleSwitch swDef = null;
                // check type of the switch column
                if (aCol.DataType == typeof(Int32))
                {
                    // is this a simple switch or Digital IO?
                    IInstType_SimpleSwitch simpleSw;
                    IInstType_DigitalIO digiIo;
                    IInstType_DigiIOCollection digiIoCollection;
                    if ((simpleSw=instr as IInstType_SimpleSwitch) != null)
                    {
                        swDef = simpleSw;
                    }
                    // is this a Digi IO line?                                        
                    else if ((digiIo = instr as IInstType_DigitalIO) != null)
                    {
                        // wrap it with a Inst_DigiIoAsSwitch class
                        simpleSw = new Inst_DigiIoAsSwitch(digiIo);
                        // add the wrapped class
                        swDef = simpleSw;
                    }
                    else if ((digiIoCollection = instr as IInstType_DigiIOCollection) != null)
                    {
                        // get the line number
                        int lineNbr;
                        try
                        {
                            lineNbr = int.Parse(columnInfo[1]);
                            digiIo = digiIoCollection.GetDigiIoLine(lineNbr);
                            // wrap it with a Inst_DigiIoAsSwitch class
                            simpleSw = new Inst_DigiIoAsSwitch(digiIo);
                            // add the wrapped class
                            swDef = simpleSw;
                        }
                        catch (Exception e)
                        {
                            // give a helpful error message if the above goes wrong for any reason
                            string errMsg = string.Format("Instrument '{0}' is an IO collection, but doesn't specify a valid line number!",
                                instName);
                            throw new InstrumentException(errMsg, e);
                        }
                    }
                    else
                    {
                        string errMsg = string.Format("Instrument '{0}' isn't a simple switch or a Digital IO.\n" +
                            "Actual instrument type is {1}", instName, instr.GetType().Name);
                        throw new Util_SwitchPathManagerException(errMsg);
                    }

                }
                else
                {
                    string errMsg = string.Format("Column type for instrument '{0}' was not Int32 (actual column type {1}).\n"+
                            "Actual instrument type is {2}", instName, aCol.DataType.Name, instr.GetType().Name);
                        throw new Util_SwitchPathManagerException(errMsg);
                }
                // it's a correct type and matches the data column: remember this switch definition
                this.switches[ii] = swDef;
            }            

            // remember the switch names
            this.switchNames = new string[this.switches.Length];
            for (int ii = 0; ii < this.switches.Length; ii++)
            {
                this.switchNames[ii] = this.switches[ii].Name;
            }
            // return our datatable for later validation
            return dataTable;
        }

        // for each switch position - check it's within the capability of the underlying switch
        private void validatePosnBounds(DataTable dataTable)
        {
            foreach (DataRow row in dataTable.Rows)
            {
                string posnName = (string) row.ItemArray[0];
                if (switchPositionMatrix.ContainsKey(posnName))
                {
                    throw new Util_SwitchPathManagerException("Switch position name occurs more than once: " + posnName);
                }
                
                // get and validate the positions
                int[] posns = new int[switches.Length];
                for(int ii=0; ii<this.switches.Length; ii++)
                {
                    IInstType_SimpleSwitch swDef = switches[ii];
                    // get the value
                    int val = (int)row.ItemArray[ii + 1];
                    // check it's in range (or is the IgnoreSwPosn meta-value) 
                    if ((val != IgnoreSwPosn) && (val < swDef.MinimumSwitchState) || (val > swDef.MaximumSwitchState))
                    {
                        string errMsg = string.Format("Switch Path Position out of bounds: PosnName='{0}', Switch='{1}'({2}, {3}), RequiredPosn={4}",
                            posnName, swDef.Name, swDef.MinimumSwitchState, swDef.MaximumSwitchState, val);
                        throw new Util_SwitchPathManagerException(errMsg);
                    }
                    posns[ii] = val;
                }    
        
                // add to our lookup
                switchPositionMatrix.Add(posnName, posns);
            }
        }        
        #endregion
    }
}

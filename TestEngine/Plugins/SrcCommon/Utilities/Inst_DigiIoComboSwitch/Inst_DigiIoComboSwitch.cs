using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.Equipment;
using System.Data;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Utility to make a group of Digital IO lines behave like a simple switch.
    /// Positions start at 1 and go to N. Config file position 0 is reserved
    /// as the position you go to on SetDefaultState call.
    /// </summary>
    /// <remarks>This instrument could talk to multiple chassis objects,
    /// so we are using the VirtualChassis singleton</remarks>
    public class Inst_DigiIoComboSwitch : UnmanagedInstrument, IInstType_SimpleSwitch
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="instrumentName">Instrument Name</param>
        /// <param name="configFile">Config File</param>
        /// <param name="instrColl">Instrument collection</param>
        public Inst_DigiIoComboSwitch(string instrumentName, string configFile, 
            InstrumentCollection instrColl)
            :base(instrumentName)
        {
            // initialise the switch array
            this.switchMatrix = new SortedDictionary<int, bool[]>();
            DataTable dataTable = loadConfigFile(configFile, instrColl);
            validatePosns(dataTable);
        }

        #region Private Data
        /// <summary>
        /// Switch matrix data as a Dictionary
        /// </summary>
        private SortedDictionary<int, bool[]> switchMatrix;
        /// <summary>
        /// Max Position
        /// </summary>
        private int maxPosition;
        
        /// <summary>
        /// List of switches - keep original order
        /// </summary>
        private IInstType_DigitalIO[] digiIoLines;

        #endregion   
        /// <summary>
        /// Set default switch state
        /// </summary>
        public override void SetDefaultState()
        {
            this.SwitchState = 0;
        }

        #region IInstType_SimpleSwitch Members
        /// <summary>
        /// Returns the maximum settable switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int MaximumSwitchState
        {
            get { return maxPosition; }
        }

        /// <summary>
        /// Returns the minimum settable switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int MinimumSwitchState
        {
            get { return 1; }
        }

        /// <summary>
        /// Reads/sets the current switch position for the output side of the switch
        /// channel.
        /// </summary>
        public int SwitchState
        {
            get
            {
                // get all the line states
                bool[] lineStates = new bool[digiIoLines.Length];
                for(int ii=0; ii<digiIoLines.Length; ii++)
                {
                    IInstType_DigitalIO digiIo = digiIoLines[ii];
                    lineStates[ii] = digiIo.LineState;
                }
                int posn = findSwitchPosition(lineStates);
                return posn;                
            }
            set
            {
                // do we need to change position?
                int currentState = SwitchState;
                if (currentState != value)
                {
                    // set to safe position first
                    setToPosition(0);
                    // set to real position next
                    setToPosition(value);
                }
            }
        }

        #endregion        

        #region Helper Fns
        /// <summary>
        /// Load the configuration file and validate the schema (columns) against the Instrument collection provided
        /// </summary>
        /// <param name="configFile">Config file</param>
        /// <param name="instrColl">Instrument collection</param>
        /// <returns>The DataTable containing all the switch positions</returns>
        private DataTable loadConfigFile(string configFile, InstrumentCollection instrColl)
        {
            // Load the XML file into a DataSet object
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(configFile);
            // check we have the correct switch configuration table
            if (!dataSet.Tables.Contains("SwitchMatrix"))
            {
                throw new InstrumentException("Invalid switch configuration file - missing 'SwitchConfig' table");
            }
            DataTable dataTable = dataSet.Tables["SwitchMatrix"];

            // find names of the columns - these are a header "PosnName" one column per switch
            int nbrCols = dataTable.Columns.Count;
            if (nbrCols == 0)
            {
                throw new InstrumentException("No columns found in 'InstrumentCollection instrColl' table");
            }
            // check 1st column is PosnName
            DataColumn firstCol = dataTable.Columns[0];
            if ((firstCol.ColumnName != "Position") || (firstCol.DataType != typeof(int)))
            {
                throw new InstrumentException("Invalid switch configuration file - 'Position' column must be of type integer");
            }

            // create our instruments array
            digiIoLines = new IInstType_DigitalIO[nbrCols - 1];
            // look through the other columns    
            for (int ii = 1; ii < nbrCols; ii++)
            {
                DataColumn aCol = dataTable.Columns[ii];
                string[] columnInfo = aCol.ColumnName.Split(':');
                string instName = columnInfo[0];
                // find the instrument from the collection
                Instrument instr = instrColl[instName];
                if (aCol.DataType == typeof(Boolean))
                {
                    IInstType_DigitalIO digiIo;
                    IInstType_DigiIOCollection digiIoCollection;
                    if ((digiIo = instr as IInstType_DigitalIO) != null)
                    {
                        // remember our instrument
                        digiIoLines[ii - 1] = digiIo;
                    }
                    else if ((digiIoCollection = instr as IInstType_DigiIOCollection) != null)
                    {
                        // get the line number
                        int lineNbr;
                        try
                        {
                            lineNbr = int.Parse(columnInfo[1]);
                            digiIo = digiIoCollection.GetDigiIoLine(lineNbr);
                            digiIoLines[ii - 1] = digiIo;
                        }
                        catch (Exception e)
                        {
                            // give a helpful error message if the above goes wrong for any reason
                            string errMsg = string.Format("Instrument '{0}' is an IO collection, but doesn't specify a valid line number!",                            
                                instName);
                            throw new InstrumentException(errMsg, e);
                        }
                    }
                    else
                    {
                        string errMsg = string.Format("Instrument '{0}' isn't a Digital IO(Boolean type column).\n" +
                            "Actual instrument type is {1}", instName, instr.GetType().Name);
                        throw new InstrumentException(errMsg);
                    }                    
                }
                else
                {
                    string errMsg = string.Format("Invalid column type for instrument '{0}' (column type {1}).\n" +
                            "Actual instrument type is {2}", instName, aCol.DataType.Name, instr.GetType().Name);
                    throw new InstrumentException(errMsg);
                }                
            }
            return dataTable;      
        }

        /// <summary>
        /// Validate all the switch positions - find them all and check that there are none missing
        /// or duplicated. Add the positions to the Dictionary.
        /// </summary>
        /// <param name="dataTable">DataTable of switch positions from our XML file</param>
        private void validatePosns(DataTable dataTable)
        {
            int nbrRows = dataTable.Rows.Count;
            // find all positions, starting at zero (default posn)
            // if any of the positions is missing findSwitchPosition will throw!
            // i.e. it will only work if you have positions from 0-(rowNbr-1) contiguously
            // However they can be in any order in the data set!
            for (int ii=0; ii<nbrRows; ii++)
            {
                addNewSwitchPosition(ii, dataTable);                
            }
            this.maxPosition = nbrRows-1;                                    
        }

        /// <summary>
        /// Find a position from our XML file and add it to the Dictionary
        /// </summary>
        /// <param name="posn">Position to find</param>
        /// <param name="dataTable">DataTable of switch positions from our XML file</param>
        void addNewSwitchPosition(int posn, DataTable dataTable)
        {
            // find row
            string sqlSearchStatement = "Position=" + posn + "";
            DataRow[] rows = dataTable.Select(sqlSearchStatement);
            // check only found one row
            if (rows.Length == 0)
            {
                throw new InstrumentException("No switch position " + posn + " found");
            }
            if (rows.Length > 1)
            {
                string errMsg = String.Format("Switch position {0} found {1} times!",
                    posn, rows.Length);
                throw new InstrumentException(errMsg);
            }
            // OK - there's only 1 row, now find the line states
            DataRow row = rows[0];
            int nbrLines = dataTable.Columns.Count - 1;
            bool[] lineStates = new bool[nbrLines];            
            for (int ii = 0; ii < nbrLines; ii++)
            {
                lineStates[ii] = (bool) row.ItemArray[ii + 1];
            }
            // add to our matrix
            switchMatrix.Add(posn, lineStates);
        }

        /// <summary>
        /// Find a switch position by comparing it with the current states of the Digital IO lines 
        /// </summary>
        /// <param name="lineStates">Line states</param>
        /// <returns>Which position it matches, or -1 if no match</returns>
        int findSwitchPosition(bool[] lineStates)
        {
            int nbrIoLines = lineStates.Length;
            foreach (KeyValuePair<int, bool[]> switchPosn in switchMatrix)
            {
                // make position 0 untraceable
                if (switchPosn.Key == 0) continue;
                // check all positions match
                bool[] posnStates = switchPosn.Value;
                bool matchFound = true;
                for (int ii = 0; ii < nbrIoLines; ii++)
                {
                    // if this IO line is wrong, set flag and abort
                    if (posnStates[ii] != lineStates[ii])
                    {
                        matchFound = false;
                        break;
                    }
                }
                if (matchFound) return switchPosn.Key;
            
            }
            // if got here, didn't find the states
            return -1;
        }

        /// <summary>
        /// Set lines to their position. Position 0 is our special safe mode - we have
        /// to set switches in reverse order to be safe.
        /// </summary>
        /// <param name="posn">Position number</param>
        void setToPosition(int posn)
        {
            int nbrDigiIoLines = digiIoLines.Length;
            bool[] lineStates = switchMatrix[posn];
            // going to safe mode? then do it backwards
            if (posn == 0)
            {
                for (int ii = nbrDigiIoLines - 1; ii >= 0; ii--)
                {
                    digiIoLines[ii].LineState = lineStates[ii];
                }
            }
            // do it forwards
            else
            {
                for (int ii = 0; ii < nbrDigiIoLines; ii++)
                {
                    digiIoLines[ii].LineState = lineStates[ii];
                }
            }
        }
        #endregion
    }
}

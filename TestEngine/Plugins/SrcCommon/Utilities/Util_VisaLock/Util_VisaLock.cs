// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Util_VisaLock.cs
//
// Author: mark.fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;                 // ChassisType_Visa
using Bookham.TestEngine.Equipment;                     // InstrumentCollection
//using Boookham.

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Helper class to lock VISA resource
    /// </summary>
    public class Util_VisaLock: IDisposable
    {
        /// <summary>
        /// Lock a VISA resource
        /// </summary>
        /// <param name="instrument">Instrument</param>
        /// <param name="timeout_ms">Chassis timeout</param>
        public Util_VisaLock(Instrument instrument, int timeout_ms)
        {
            SetVisaLock(instrument, true, timeout_ms);
            this.instrument = instrument;
        }

        /// <summary>
        /// Lock a collection of instruments
        /// </summary>
        /// <param name="instrumentCollection">Collection of instruments</param>
        /// <param name="timeout_ms">Chassis timeout</param>
        public Util_VisaLock(InstrumentCollection instrumentCollection, int timeout_ms)
        {
            if ( instrumentCollection == null )
            {
                throw new ArgumentException("Instrument collection is null");
            }
            this.instrumentCollection = new InstrumentCollection();
            foreach (Instrument instrument in instrumentCollection.Values)
            {
                SetVisaLock(instrument, true, timeout_ms);
                this.instrumentCollection.Add(instrument);
            }
        }

        #region private members

        private Instrument instrument;
        private InstrumentCollection instrumentCollection;
        private bool disposed = false;
        
        private static void SetVisaLock(Instrument instrument, bool locked)
        {
            ChassisType_Visa visaChassis = (ChassisType_Visa)instrument.InstrumentChassis;
            //visaChassis.Locked = locked;

            if (locked == false)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (visaChassis.Locked)
                    {
                        try
                        {
                            visaChassis.Locked = locked;
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        private static void SetVisaLock(Instrument instrument, bool locked, int Timeout_ms)
        {
            ChassisType_Visa visaChassis = (ChassisType_Visa)instrument.InstrumentChassis;
            SetChassisTimeout(visaChassis, Timeout_ms);

            //Cypress: comment it. set Locked in "SetChassisTimeout" method.
            //visaChassis.Locked = locked;
        }

        /// <summary>
        /// Set the timeout for the chassis.
        /// If the instrument is locked by another session we can read but not set the timeout.
        /// An error will be thrown if unable to set the timeout within the required period.
        /// </summary>
        /// <param name="chassis">The chassis to lock</param>
        /// <param name="Timeout_ms">The timeout to set</param>
        private static void SetChassisTimeout(ChassisType_Visa chassis, int Timeout_ms)
        {
            //Cypress:comment it
            ////if ( chassis.Timeout_ms == Timeout_ms )
            ////    return;

            DateTime startTime = DateTime.Now;
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 0, Timeout_ms);

            // Keep trying to set the chassis timeout
            bool chassisUnlocked = false;
            while (!chassisUnlocked)
            {
                if (DateTime.Now > startTime + timeSpan)
                    break;

                try
                {
                    //chassis.Timeout_ms = Timeout_ms;
                    chassis.Locked = true;//Cypress: set locked directly.
                    chassisUnlocked = true;
                }
                catch (Exception)
                {
                    // If the chassis is already locked then we can't set the timeout.
                    System.Threading.Thread.Sleep(200);
                }
            }

            chassis.Timeout_ms = Timeout_ms;//Cypress: set time out here.

            //// Allow error to be thrown if timeSpan is exceeded.
            //if (!chassisUnlocked)
            //    chassis.Timeout_ms = Timeout_ms;
        }
        
 

        #region IDisposable Members
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore we call GC.SupressFinalize to take this object off the finalization queue 
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly or indirectly by a user's code. 
        // Managed and unmanaged resources can be disposed.
        // If disposing equals false, the method has been called by the runtime from inside the finalizer
        // and we should not reference other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // So that we only unlock once we check to see if Dispose has already been called.
            if(!this.disposed)
            {   		 
                if (instrument != null)
                    SetVisaLock(instrument, false);

                if (instrumentCollection != null)
                {
                    foreach (Instrument inst in instrumentCollection.Values)
                    {
                        SetVisaLock(inst, false);
                    }
                }
            }
            disposed = true;         
        }

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method does not get called.
        /// <summary>
        /// 
        /// </summary>
        ~Util_VisaLock()      
        {
            Dispose(false);
        }
        #endregion // IDisposable

        #endregion // private members
    }
}

//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// FileLockedEventArgs.cs
//
// Author: Cypress Chen, 2009.03
// Design: [Reference design documentation]
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// The class contents data for FileLocked event.
    /// </summary>
    public class FileLockedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="fileName">The file name which is locked.</param>
        public FileLockedEventArgs(string fileName)
        {
            _fileName = fileName;
        }

        private string _fileName;
        /// <summary>
        /// Gets the file name which is locked.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
        }

    }
}

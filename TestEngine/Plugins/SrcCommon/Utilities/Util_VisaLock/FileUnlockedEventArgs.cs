//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// FileLockedEventArgs.cs
//
// Author: Cypress Chen, 2009.03
// Design: [Reference design documentation]
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// The class contents data for FileUnlocked event.
    /// </summary>
    public class FileUnlockedEventArgs:EventArgs
    {
        /// <summary>
        /// Initialize an instance of the class.
        /// </summary>
        /// <param name="fileName"></param>
        public FileUnlockedEventArgs(string fileName)
        {
            _fileName = fileName;
        }
        private string _fileName;
        /// <summary>
        /// Gets the file name which is unlocked.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
        }

    }
}

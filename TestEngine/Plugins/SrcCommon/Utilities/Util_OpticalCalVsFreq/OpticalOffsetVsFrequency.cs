// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// SplitterCalReader.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// OpticalOffsetVsFrequency - stores a list of optical power offsets in dB against optical frequency in GHz.
    /// Used in ITLA for calibrating the optical path between the bulkhead and the power meter (including inline attenuator).
    /// Also used for storing the optical power calibration correction of the reference power meter.
    /// </summary>
    public class OpticalOffsetVsFrequency
    {

        double[] freqGHzArray;
        double[] offset_dBArray;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="nbrPoints">How many points?</param>
        public OpticalOffsetVsFrequency(int nbrPoints)
        {
            freqGHzArray = new double[nbrPoints];
            offset_dBArray = new double[nbrPoints];
        }

        /// <summary>
        /// Frequency array (GHz)
        /// </summary>
        public double[] FreqGHz
        {
            get { return freqGHzArray; }
        }

        /// <summary>
        /// Offsets in dB
        /// </summary>
        public double[] Offset_dB
        {
            get { return offset_dBArray; }
        }

        /// <summary>
        /// Calculate the Offset at a given frequency
        /// </summary>
        /// <param name="freqGHz"></param>
        /// <returns></returns>
        public double GetOffset_dB(double freqGHz)
        {
            double offset = XatYAlgorithm.Calculate(offset_dBArray, freqGHzArray, freqGHz);
            return offset;
        }
    }
}

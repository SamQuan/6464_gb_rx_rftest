using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NUnit.Framework;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Generate File name (e.g. for a BLOB file)
    /// </summary>
    public sealed class Util_GenerateFileName
    {
        private Util_GenerateFileName() { }

        /// <summary>
        /// Generate file name with 17 digit time stamp in UTC.
        /// 
        /// Timestamp format:[y][M][d][h][min][s][ms]
        /// y=4-digit Year
        /// M=month number
        /// d=day of month
        /// h=hour
        /// min=minute
        /// s=second
        /// ms=millisecond
        /// </summary>
        /// <param name="directory">Directory to put results into</param>
        /// <param name="filenameStem">Filename stem</param>
        /// <param name="dutSerial">Serial number of DUT</param>
        /// <param name="extension">Filename extension</param>
        /// <returns></returns>
        public static string GenWithTimestamp(string directory, string filenameStem, 
            string dutSerial, string extension)
        {
            char pathSep = Path.DirectorySeparatorChar;

            string timestamp = generateUniqueTimeStamp();
            string filename = string.Format("{0}{1}{2}_{3}_{4}.{5}",
                directory, pathSep, filenameStem, dutSerial, timestamp, extension);

            return filename;
        }

        /// <summary>
        /// Generate unique 17 digit time stamp in UTC:
        /// [y][M][d][H][min][s][ms]
        /// y=4-digit Year
        /// M=month number
        /// d=day of month
        /// H=hour
        /// min=minute
        /// s=second
        /// ms=millisecond
        /// </summary>
        /// <returns>Time stamp</returns>
        private static string generateUniqueTimeStamp()
        {
            DateTime nowUtc = DateTime.Now.ToUniversalTime();
            string timestamp = nowUtc.ToString("yyyyMMddHHmmssfff");
            return timestamp;
        }
    }

    /// <exclude/>
    /// <summary>Test fixture class to test filename generator in nunit</summary>
    [TestFixture]
    public class Test_UtilGenFileName
    {
        /// <exclude/>
        [Test]
        public void Test1()
        {
            string filename = Util_GenerateFileName.GenWithTimestamp("a", "aFile", "1234", "csv");
        }
    }
}

// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// TempTableConfigAccessor.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Structure with a single Temperature Table point
    /// </summary>
    public struct TempTablePoint
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tecNbr">Which TEC number (integer)</param>
        /// <param name="offsetC">Temperature offset to use (deg C)</param>
        /// /// <param name="tolerance_degc">Tolerance of set temperature for TEC to achieve</param>
        /// <param name="delayTime_s">Delay time to wait for stabilisation (seconds)</param>
        public TempTablePoint(int tecNbr, double offsetC, double tolerance_degc,
            double delayTime_s)
        {
            TecNbr = tecNbr;
            OffsetC = offsetC;
            DelayTime_s = delayTime_s;
            Tolerance_degC = tolerance_degc;
        }

        /// <summary>
        /// Tec number
        /// </summary>
        public readonly int TecNbr;

        /// <summary>
        /// Temperature offset to use (deg C)
        /// </summary>
        public readonly double OffsetC;

        /// <summary>
        /// Tolerance on Set Temperature in Deg C
        /// </summary>
        public readonly double Tolerance_degC; 

        /// <summary>
        /// Delay time to wait for stabilisation (seconds)
        /// </summary>
        public readonly double DelayTime_s;        
    }

    /// <summary>
    /// Class to access temperature config files
    /// </summary>
    public class TempTableConfigAccessor
    {
        /// <summary>
        /// Constructor - loads the config file.
        /// </summary>
        /// <param name="dutObject">DUT Object with Test Jig data</param>
        /// <param name="nbrTecs">Number of Tec stages to get data for</param>
        /// <param name="configFile">The config file to load</param>
        public TempTableConfigAccessor(DUTObject dutObject, int nbrTecs, string configFile)
        {
            this.nbrTecs = nbrTecs;
            configKeys = new StringDictionary();
            configKeys["JigID"] = dutObject.TestJigID;
            
            config =
                new ConfigDataAccessor(configFile, "TempTable");            
        }

        /// <summary>
        /// Get temperature cal point
        /// </summary>
        /// <param name="fromTempC">Temperature going from</param>
        /// <param name="toTempC">Temperature going to</param>
        /// <returns>An array of temperature calibration points (one per tec)</returns>
        public TempTablePoint[] GetTemperatureCal(double fromTempC, double toTempC)
        {            
            try
            {
                // trim to 2 decimal places
                double fromTrim = AccuracyTrimmer.TrimDouble(fromTempC, 2);
                double toTrim = AccuracyTrimmer.TrimDouble(toTempC, 2);
                configKeys["FromTempC"] = fromTrim.ToString();
                configKeys["ToTempC"] = toTrim.ToString();

                TempTablePoint[] tempPoints = new TempTablePoint[nbrTecs];

                // read each Tec's settings
                for (int ii = 1; ii <= nbrTecs; ii++)
                {
                    // set the Tec number
                    configKeys["TecNbr"] = ii.ToString();
                    // Get the config row
                    DatumList configRow = config.GetData(configKeys, true);
                    // Read the values.
                    tempPoints[ii - 1] = new TempTablePoint(ii,
                        configRow.ReadDouble("OffsetC"),
                        configRow.ReadDouble("ToleranceDegC"),
                        configRow.ReadDouble("DelayTime_s"));
                       
                }

                return tempPoints;
            }
            catch (Exception e)
            {
                string errMsg = String.Format
                    ("Couldn't read config for from temp {0} to temp {1}",
                    fromTempC, toTempC);
                throw new Exception(errMsg, e);
            }            
        }

        #region Private data
        StringDictionary configKeys;
        ConfigDataAccessor config;

        int nbrTecs;
        #endregion
    }
}

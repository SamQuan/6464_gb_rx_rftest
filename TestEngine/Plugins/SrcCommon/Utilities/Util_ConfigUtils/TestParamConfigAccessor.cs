// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// TestParamConfigAccessor.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to access test parameter config file
    /// </summary>
    public class TestParamConfigAccessor
    {
        /// <summary>
        /// Constructor - loads the config file.
        /// </summary>
        /// <param name="dutObject">DUT Object with Device ID data</param>
        /// <param name="configFile">Which file to load</param>
        /// <param name="configTableName">Table name in configuration file</param>
        /// <param name="connectorVariant">Connector variant string</param>
        public TestParamConfigAccessor(DUTObject dutObject, string configFile,
            string connectorVariant, string configTableName)
        {
            configKeys = new StringDictionary();
            configKeys["DeviceType"] = dutObject.PartCode;
            configKeys["TestStage"] = dutObject.TestStage;
            configKeys["ConnectorVariant"] = connectorVariant;

            config =
                new ConfigDataAccessor(configFile, configTableName);            
        }

        /// <summary>
        /// Get a string parameter
        /// </summary>
        /// <param name="paramName">Parameter name</param>
        /// <returns>The string</returns>
        public string GetStringParam(string paramName)
        {
            // adjust parameter name
            configKeys["ParamName"] = paramName;

            try
            {
                DatumList configRow = config.GetData(configKeys, true);
                string value = configRow.ReadString("ParamValue");
                return value;
            }
            catch (Exception e)
            {
                throw new Exception("Couldn't find TestParam: '" + paramName + "'", e);
            }
            
        }

        /// <summary>
        /// Get a boolean parameter
        /// </summary>
        /// <param name="paramName">Parameter Name</param>
        /// <returns>The boolean</returns>
        public bool GetBoolParam(string paramName)
        {
            string paramValue = GetStringParam(paramName);
            return bool.Parse(paramValue);
        }

        /// <summary>
        /// Get an integer parameter
        /// </summary>
        /// <param name="paramName">Parameter Name</param>
        /// <returns>The integer</returns>
        public int GetIntParam(string paramName)
        {
            string paramValue = GetStringParam(paramName);
            return int.Parse(paramValue);
        }

        /// <summary>
        /// Get a double parameter
        /// </summary>
        /// <param name="paramName">Parameter Name</param>
        /// <returns>The double</returns>
        public double GetDoubleParam(string paramName)
        {
            string paramValue = GetStringParam(paramName);
            return double.Parse(paramValue);
        }

        #region Private data
        StringDictionary configKeys;
        ConfigDataAccessor config;
            
        #endregion
    }
}

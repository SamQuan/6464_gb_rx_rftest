// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// CalStdValidity.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Calibration or Standard Validity class. This class is constructed with
    /// the details of the calibration / standard validity period, when this was last performed.
    /// 
    /// Optionally you can also use this to track a maximum count a run counter, 
    /// and the current value of the run counter.
    /// 
    /// To ignore this counter capability, just enter "0" into the countMax field.
    /// 
    /// From this you can then query each of the values that were inputted, and also
    /// call the IsValid property to determine whether the cal/std is valid.
    /// </summary>
    public class CalStdValidity
    {
        /// <summary>
        /// Constructor - times are contained in strings, which this constructor will 
        /// parse them to the .NET time/date types.
        /// </summary>
        /// <param name="period">Calibration Period</param>
        /// <param name="lastDone">When last calibration was done</param>
        /// <param name="countMax">Maximum counter when calibration is due</param>
        /// <param name="count">Current calibration counter</param>
        public CalStdValidity(string period, string lastDone, int countMax,
            int count)
        {
            try
            {
                Period = TimeSpan.Parse(period);
            }
            catch (Exception e)
            {
                throw new ArgumentException("Cal/Std period string was incorrectly formatted: "
                    + period, e);
            }
            try
            {
                LastDone = DateTime.Parse(lastDone);
            }
            catch (Exception e)
            {
                throw new ArgumentException("Cal/Std lastDone string was incorrectly formatted: "
                    + lastDone, e);
            }
            
            CountMax = countMax;
            Count = count;
        }

        /// <summary>
        /// Constructor - times use the .NET time types, so no parsing necessary.
        /// </summary>
        /// <param name="period">Calibration Period</param>
        /// <param name="lastDone">When last calibration was done</param>
        /// <param name="countMax">Maximum counter when calibration is due</param>
        /// <param name="count">Current calibration counter</param>
        public CalStdValidity(TimeSpan period, DateTime lastDone, int countMax,
            int count)
        {
            Period = period;
            LastDone = lastDone;
            CountMax = countMax;
            Count = count;            
        }

        /// <summary>
        /// Is the calibration valid?
        /// </summary>
        public bool CalStdValid
        {
            get
            {
                if (LastDone + Period < DateTime.Now)
                    return false;

                if (CountMax == 0) return true;
                if (Count >= CountMax) return false;
                else return true;
            }
        }

        /// <summary>
        /// Calibration/Standard period
        /// </summary>
        public readonly TimeSpan Period;

        /// <summary>
        /// Last time Calibration/Standard done
        /// </summary>
        public readonly DateTime LastDone;

        /// <summary>
        /// Maximum count before Calibration/Standard required
        /// </summary>
        public readonly int CountMax;

        /// <summary>
        /// Current counter
        /// </summary>
        public readonly int Count;        
    }    
}

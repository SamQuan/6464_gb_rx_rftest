// [Copyright]
//
// Bookham Test Engine Library
// 
//
// TLBlackBox_DUTPowerOperations_Config.cs
// Design: PCBA Cal Test Modules DD 
// Author: Keith Pillar

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;



namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This structure encapsulates all the data requirements of the test module
    /// At test program level the user declares an instance of the structure, but not a new instance, 
    /// ie mystruct myInstance;
    /// then they can make populate the members of the struct
    /// ie
    /// myInstance.param1 = 20;
    /// myInstance.param2 = 20;
    /// 
    /// Only when the structure is fully populated can they call
    /// DatumList myD = myInstance.GetDatumList();
    /// 
    /// Else they will get a compile time error, as the object myInstance
    /// only exists at the point where all members are populated!
    /// 
    /// In the test module level the user simply calls
    /// mystruct myInstance = new mystruct(configData);
    /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
    /// </summary>
    public struct TLBlackBox_DUTPowerOperations_Config
    {
        /// <summary>
        /// Method populates a datumlist based on this structure data
        /// </summary>
        /// <returns>datumlist</returns>
        public DatumList GetDatumList()
        {
            DatumList myLocalDatumList = new DatumList();

            DatumDouble Supply1_V = new DatumDouble("Supply1_V", this.Supply1_V);
            DatumDouble Supply1_I = new DatumDouble("Supply1_I", this.Supply1_I);
            DatumDouble Supply2_V = new DatumDouble("Supply2_V", this.Supply2_V);
            DatumDouble Supply2_I = new DatumDouble("Supply2_I", this.Supply2_I);
            DatumBool PoweredUp = new DatumBool("PoweredUp", this.PoweredUp);
            
            myLocalDatumList.Add(Supply1_V);
            myLocalDatumList.Add(Supply1_I);
            myLocalDatumList.Add(Supply2_V);
            myLocalDatumList.Add(Supply2_I);
            myLocalDatumList.Add(PoweredUp);

            return (myLocalDatumList);
        }




        /// <summary>
        /// Constructor which takes a datumList and populates our StackedTecControlType
        /// </summary>
        /// <param name="mydata"></param>
        public TLBlackBox_DUTPowerOperations_Config(DatumList mydata)
        { 
            this.Supply1_V = mydata.ReadDouble("Supply1_V");
            this.Supply1_I = mydata.ReadDouble("Supply1_I");
            this.Supply2_V = mydata.ReadDouble("Supply2_V");
            this.Supply2_I = mydata.ReadDouble("Supply2_I");
            this.PoweredUp = mydata.ReadBool("PoweredUp");
        }

        //List of all my parameters i will use
        /// <summary>
        /// Our supply 1 voltage 
        /// </summary>
        public double Supply1_V;
        /// <summary>
        /// Our supply 1 current
        /// </summary>
        public double Supply1_I;
        /// <summary>
        /// Our supply 2 voltage
        /// </summary>
        public double Supply2_V;
        /// <summary>
        /// Our supply 2 current
        /// </summary>
        public double Supply2_I;
        /// <summary>
        /// Whether the device is to be powered up or not
        /// </summary>
        public bool PoweredUp;
    }
}

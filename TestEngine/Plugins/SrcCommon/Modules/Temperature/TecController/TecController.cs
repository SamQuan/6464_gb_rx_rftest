using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using System.Timers;


namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// This is the class that all tec controllers make use of
    /// </summary>
    public class TecController 
    {
        
        /// <summary>
        /// The constructor for the tec class
        /// </summary>
        /// <param name="engine">needs a copy of the test engine otherwise it cannt send module errors or talk to gui</param>
        public TecController(ITestEngine engine)
        {
            // Hook up the Elapsed events for the timers.
            this.timeoutTimer = new System.Timers.Timer();
            this.timeoutTimer.Elapsed += new ElapsedEventHandler(TimeoutTimerElapsed);
            this.stabilisationTimer = new System.Timers.Timer();
            this.stabilisationTimer.Elapsed += new ElapsedEventHandler(StabilisationTimerElapsed);
            this.withinToleranceWindowTimer = new System.Timers.Timer();
            this.withinToleranceWindowTimer.Elapsed += new ElapsedEventHandler(WithinToleranceWindowTimerElapsed);
            
            moduleResults = new DatumList();
            myLocalEngine = engine;
            SetupDefaultState();
        }

        /// <summary>
        /// Sets up default state for timers etc
        /// </summary>
        public void SetupDefaultState()
        {
            this.timeoutTimer.Enabled=false;
            this.stabilisationTimer.Enabled=false;
            this.withinToleranceWindowTimer.Enabled=false;
            setPt1 = new DatumDouble("SetPointTemp1");
            setPt2 = new DatumDouble("SetPointTemp2");
            actVal1= new DatumDouble("ActualTemp1");
            actVal2= new DatumDouble("ActualTemp1");
        }
       
        /// <summary>
        /// The timer interval for the timeout timer
        /// </summary>
        public double TimeoutTimerInterval_s
        {
            get
            {
                return (timeoutTimer.Interval/1000);
            }
            set
            {
                timeoutTimer.Interval = value * 1000;
            }
        }

        /// <summary>
        /// the timer interval for the WithinToleranceWindow Timer
        /// </summary>
        public double WithinToleranceWindowTimer_s
        {
            get
            {
                return (withinToleranceWindowTimer.Interval/1000);
            }
            set
            {
                withinToleranceWindowTimer.Interval = value * 1000;
            }
        }

        /// <summary>
        /// the timer interval for the StabilisationTimer Timer
        /// </summary>
        public double StabilisationTimer_s
        {
            get
            {
                return (stabilisationTimer.Interval/1000);
            }
            set
            {
                stabilisationTimer.Interval = value * 1000;
            }
        }

        /// <summary>
        /// the timeout timer has completed its time period
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void TimeoutTimerElapsed(object source, ElapsedEventArgs e)
        {
           //Error In Module due to timeout.
            timedOut = true;
        }

        /// <summary>
        /// the stabilisation timer has completed its time period
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void StabilisationTimerElapsed(object source, ElapsedEventArgs e)
        {
            stabilisationTimerCompleted= true;
        }
        
        /// <summary>
        /// the within tolerance timer has completed its time period
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void WithinToleranceWindowTimerElapsed(object source, ElapsedEventArgs e)
        {
            withinTolWindowTimerCompleted = true;
        }


        /// <summary>
        /// function to allow us to do delays
        /// </summary>
        /// <param name="delay">delay time in seconds</param>
        public void Delay(Int32 delay)
        {
            // sleep 
            System.Threading.Thread.Sleep(delay);
        }

        /// <summary>
        /// function to evaluate if we are WithinToleranceWindow
        /// </summary>
        /// <param name="actualReading">actualReading</param>
        /// <param name="targetReading">targetReading</param>
        /// <param name="targetTolerance">+\- allowed variance from target (defines the tolerance window)</param>
        /// <returns>boolean flag to indicate if within targetPercentage</returns>
        private bool WithinToleranceWindow(double actualReading, double targetReading, double targetTolerance)
        {
            double upperValue = (targetReading + targetTolerance);
            double lowerValue = (targetReading - targetTolerance);

            if (actualReading > lowerValue && actualReading < upperValue)
            {
                return (true);
            }
            else
            {
                return (false);
            }
            
        }

        /// <summary>
        /// generic function used to tell us if one or both sensors within tolerances
        /// </summary>
        /// <returns></returns>
        private bool WithinTolerance()
        {
            actualReading1_C=tecController1.SensorTemperatureActual_C;
            if (twoTecsInParallel == true)
            {
                actualReading2_C = tecController2.SensorTemperatureActual_C;
                bool myResultTec1 = WithinToleranceWindow(this.actualReading1_C, this.targetReading1_C, this.targetTolerance_C);
                bool myResultTec2 = WithinToleranceWindow(this.actualReading2_C, this.targetReading2_C, this.targetTolerance_C);
                return (myResultTec1 & myResultTec2);
            }
            else
            {
                return (WithinToleranceWindow(this.actualReading1_C, this.targetReading1_C, this.targetTolerance_C));
            }
        }
        
       /// <summary>
       /// Flag to get set the TimeoutTimerStarted
       /// </summary>
        public bool TimeoutTimerStarted 
        {
            get
            {
                return(timeoutTimer.Enabled);
            }
            set
            {
                timeoutTimer.Enabled=value;
            }
        }

        /// Flag to get set the WithinToleranceWindowTimerStarted
        public bool WithinToleranceWindowTimerStarted 
        { 
            get
            {
                return(withinToleranceWindowTimer.Enabled);
            }
            set
            {
                withinToleranceWindowTimer.Enabled= value;
            }
        }

        /// <summary>
        /// Flag to get set the StabilisationTimerTimerStarted
        /// </summary>
        public bool StabilisationTimerTimerStarted
        {
            get
            {
                return (stabilisationTimer.Enabled);
            }
            set
            {
                stabilisationTimer.Enabled = value;
            }
        }

        /// <summary>
        /// Flag to get set the WithinTolWindowTimerCompleted
        /// </summary>
        public bool WithinTolWindowTimerCompleted
        {
            get
            {
                return(withinTolWindowTimerCompleted);
            
            }
            set
            {
                withinTolWindowTimerCompleted=value;
            }
        }
 
        
        /// <summary>
        /// the heart of the class, the function that does the work
        /// </summary>
        public void TecControlRoutine()
        {
            WithinTolWindowTimerCompleted = false;
            timedOut = false;
            
            do
            {
                if (WithinTolerance())
                {
                    if (!WithinToleranceWindowTimerStarted)
                    {
                        WithinToleranceWindowTimerStarted = true;
                    }
                }
                else
                {
                    WithinToleranceWindowTimerStarted = false;
                }

                Delay(delayTimeBetweenReadings_ms);

                if (timedOut)
                {
                    //Timed out. Raise Error in Module.
                    myLocalEngine.ErrorInModule("TEC Error: Timeout timer has elapsed, failed to acheive temperature within max time specified");
                }
            } 
            while (!WithinTolWindowTimerCompleted);
            
            StabilisationTimerTimerStarted = true;
            
            do
            {
                Delay(delayTimeBetweenReadings_ms);

                if (timedOut)
                {
                    //Timed out. Raise Error in Module.
                    myLocalEngine.ErrorInModule("TEC Error: Timeout timer has elapsed, failed to acheive temperature within max time specified");
                }
            }
            while (!stabilisationTimerCompleted);

        } 
        
  

        private ITestEngine myLocalEngine;
        
        private System.Timers.Timer timeoutTimer;
        private System.Timers.Timer withinToleranceWindowTimer;
        private System.Timers.Timer stabilisationTimer;
        private bool timedOut;
       
        /// <summary> Actual reading 1.</summary>
        public double actualReading1_C;
        /// <summary> Actual reading 2</summary>
        public double actualReading2_C;

        /// <summary> Delay time between readings. </summary>
        public int delayTimeBetweenReadings_ms;

        /// <summary> Two TECs in parallel flag. </summary>
        public bool twoTecsInParallel;

        /// <summary> Within window timer completed flag </summary>
        public bool withinTolWindowTimerCompleted;

        /// <summary> Stabilisation timer completed flag. </summary>
        public bool stabilisationTimerCompleted;

        /// <summary> Target tolerance </summary>
        public double targetTolerance_C;

        /// <summary> Target reading 1. </summary>
        public double targetReading1_C;

        /// <summary> Target reading 2. </summary>
        public double targetReading2_C;

        /// <summary> TEC Controller Instrument 1. </summary>
        public InstType_TecController tecController1;
        /// <summary> TEC Controller Instrument 2. </summary>
        public InstType_TecController tecController2;

        /// <summary> Set point 1. </summary>
        public DatumDouble setPt1;
        /// <summary> Set point 2. </summary>
        public DatumDouble setPt2;
        /// <summary> actual value 1. </summary>
        public DatumDouble actVal1;
        /// <summary> actual value 2. </summary>
        public DatumDouble actVal2;

        /// <summary> Module results. </summary>
        public DatumList moduleResults;

    }
}

       
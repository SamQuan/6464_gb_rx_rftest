using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.TestModules;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.TestPrograms
{
    /// <summary>
    /// Tec Control Trial Test Program
    /// </summary>
    public class TecControlTrial : ITestProgram
    {
        #region ITestProgram Members

        /// <summary>
        /// GUI Type
        /// </summary>
        public Type UserControl
        {
            get { return typeof(ProgramGui); }
        }

        /// <summary>
        /// Init code
        /// </summary>
        /// <param name="engine">Test Engine reference</param>
        /// <param name="dutObject">The DUT object</param>
        /// <param name="instrs">Instruments</param>
        /// <param name="chassis">Chassis</param>
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            // this is trial code
            // when using structures if you do not use the word 'new' it provides an extra level
            // of functonality which may be useful.
            // Because there was no 'new' word, you have to 'explicitly' provide every member of the structure
            // with a value before it will compile.

            /// <summary>
            /// This structure encapsulates all the data requirements of the test module
            /// At test program level the user declares an instance of the structure, but now a new instance, 
            /// ie mystruct myInstance;
            /// then they can make populate the members of the struct
            /// ie
            /// myInstance.param1 = 20;
            /// myInstance.param2 = 20;
            /// 
            /// Only when the structure is fully populated can they call
            /// DatumList myD = myInstance.GetDatumList();
            /// 
            /// Else they will get a compile time error, as the object myInstance
            /// only exists at the point where all members are populated!
            /// 
            /// In the test module level the user simply calls
            /// mystruct myInstance = new mystruct(configData);
            /// and they can read the myInstance.param1, myInstance.param2 = 20 values;
            /// </summary>


            Nt10_StackedTecInitialise_Config myConfigData;
            myConfigData.TopTec_ControlMode = InstType_TecController.ControlMode.Temperature;
            myConfigData.TopTec_IntegralGain = 50;
            myConfigData.TopTec_ProportionalGain = 50;
            myConfigData.TopTec_MaxI = 3;
            myConfigData.TopTecSteinHartData_A = 0;
            myConfigData.TopTecSteinHartData_B = 22;
            myConfigData.TopTecSteinHartData_C = 33;

            myConfigData.BtmTec_ControlMode = InstType_TecController.ControlMode.Temperature;
            myConfigData.BtmTec_IntegralGain = 50;
            myConfigData.BtmTec_ProportionalGain = 50;
            myConfigData.BtmTec_MaxI = 3;
            myConfigData.BtmTecSteinHartData_A = 0;
            myConfigData.BtmTecSteinHartData_B = 22;

            //in this case because i have explicitly given values to every element of the structure i get compile
            //time errors.  If you uncomment this next line it will not compile.
            myConfigData.BtmTecSteinHartData_C = 33;

            //this line only compiles if all the structure is already populated
            DatumList configData = myConfigData.GetDatumList();



        }

        /// <summary>
        /// Run the program
        /// </summary>
        /// <param name="engine">Test Engine</param>
        /// <param name="dutObject">DUT Object</param>
        /// <param name="instrs">Instruments</param>
        /// <param name="chassis">Chassis</param>
        public void Run(ITestEngineRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            //Using the StackedTecControl_Config to run the temperature setting
            StackedTecControl_Config myConfigData;
            myConfigData.BtmTec_RqdStabilisationTime_s = 0.022;
            myConfigData.BtmTec_RqdTimeInToleranceWindow_s = 0.033;
            //and so on...

            //this line only compiles if all the structure is already populated
            //DatumList configData = myConfigData.GetDatumList();


        }

        /// <summary>
        /// Post Run Phase
        /// </summary>
        /// <param name="engine">Test Engine</param>
        /// <param name="dutObject">DUT Object</param>
        /// <param name="instrs">Instruments</param>
        /// <param name="chassis">Chassis</param>
        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, InstrumentCollection instrs, ChassisCollection chassis)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Data Write Phase
        /// </summary>
        /// <param name="engine">Test Engine</param>
        /// <param name="dutObject">DUT Object</param>
        /// <param name="dutOutcome">DUT Outcome</param>
        /// <param name="results">Results</param>
        /// <param name="userList">User list</param>
        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Generic Stacked TEC Controller Module
    /// </summary>
    public class StackedTecControl : ITestModule
    {
        /// <summary>
        /// This does the test for us, this class is assuming a stack of 2 tecs for doing temeprature controller
        /// </summary>
        /// <param name="engine">The itest engine engine</param>
        /// <param name="userType">operator engineer or technician</param>
        /// <param name="configData">a datum list of all required config data, 
        /// uses StackedTecControlType for list of data and StackedTecControl_Config 
        /// to get to and from datumlist)</param>
        /// <param name="instruments">Should only contain a single tec controller unit</param>
        /// <param name="chassis">the chassis</param>
        /// <param name="calData">any cal data (none reqd)</param>
        /// <param name="previousTestData">any previous data (none reqd)</param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            TecController tecClass = new TecController(engine);

            //change the config datumlist into a structure i can use easily
            StackedTecControl_Config myDataStructure = new StackedTecControl_Config(configData);
            
            // Interrogate the config data and get a list of all the values we need
            // set up all the values in the tecClass class ready for our use
            tecClass.delayTimeBetweenReadings_ms = myDataStructure.TimeBetweenReadings_ms;
            tecClass.TimeoutTimerInterval_s = myDataStructure.MaxExpectedTimeForOperation_s;

            //start the clock ticking
            tecClass.TimeoutTimerStarted = true;

            //*************** Do Bottom Tec control bits *****************************
            // Interrogate the config data and get a list of all the values we need
            // set up all the values in the tecClass class ready for our use

            tecClass.WithinToleranceWindowTimer_s = myDataStructure.BtmTec_RqdTimeInToleranceWindow_s;
            tecClass.StabilisationTimer_s = myDataStructure.BtmTec_RqdStabilisationTime_s;
            tecClass.targetReading1_C = myDataStructure.BtmTec_SetPointTemperature_C;
            tecClass.targetTolerance_C = myDataStructure.BtmTec_TemperatureTolerance_C;

            tecClass.tecController1 = (InstType_TecController)instruments["BtmTec_Controller"];

            //Need to ensure Top tec is switched off before setting bottom tec
            tecClass.tecController2 = (InstType_TecController)instruments["TopTec_Controller"];
            tecClass.tecController2.OutputEnabled = false;

            // set our bottom tec to value, and go for it..
            tecClass.tecController1.SensorTemperatureSetPoint_C = myDataStructure.BtmTec_SetPointTemperature_C;
            tecClass.tecController1.OutputEnabled = true;
            engine.SendStatusMsg("Setting Temperature of Bottom TEC Controller to: " + myDataStructure.BtmTec_SetPointTemperature_C.ToString() + " degC");

            tecClass.TecControlRoutine();

            //*************** Completed Bottom Tec control bits *************************


            //*************** Do the Top Tec control bits *************************
            
            tecClass.WithinToleranceWindowTimer_s = myDataStructure.TopTec_RqdTimeInToleranceWindow_s;
            tecClass.StabilisationTimer_s = myDataStructure.TopTec_RqdStabilisationTime_s;
            tecClass.targetReading1_C = myDataStructure.TopTec_SetPointTemperature_C;
            tecClass.targetTolerance_C = myDataStructure.TopTec_TemperatureTolerance_C;

            tecClass.tecController1 = (InstType_TecController)instruments["TopTec_Controller"];

            //set our top tec value as reqd and go for it
            tecClass.tecController1.SensorTemperatureSetPoint_C = myDataStructure.TopTec_SetPointTemperature_C;
            tecClass.tecController1.OutputEnabled = true;
            engine.SendStatusMsg("Setting Temperature of Top TEC Controller to: " + myDataStructure.TopTec_SetPointTemperature_C.ToString() + " degC");
           
            tecClass.TecControlRoutine();
            //*************** Completed Top Tec control bits *************************

            // stop the TimeoutException timer
            tecClass.TimeoutTimerStarted = false;

            //we are completed successfully.
            return (tecClass.moduleResults);
        }


        /// <summary>
        /// The user control we are using for display
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

    }
}

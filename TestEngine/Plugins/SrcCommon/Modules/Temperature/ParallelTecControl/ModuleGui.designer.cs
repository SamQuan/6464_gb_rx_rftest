// [Copyright]
//
// Bookham Test Engine
// TecControl
//
// Bookham.TestSolution.TestModules/ModuleGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// 
    /// </summary>
    partial class ModuleGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSetPointTemperature11 = new System.Windows.Forms.Label();
            this.labelSetPointTemperature12 = new System.Windows.Forms.Label();
            this.labelActTemperature2 = new System.Windows.Forms.Label();
            this.labelActTemperature1 = new System.Windows.Forms.Label();
            this.Sp1Value = new System.Windows.Forms.Label();
            this.SP2Value = new System.Windows.Forms.Label();
            this.ActualValue1 = new System.Windows.Forms.Label();
            this.ActualValue2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelSetPointTemperature11
            // 
            this.labelSetPointTemperature11.AutoSize = true;
            this.labelSetPointTemperature11.Location = new System.Drawing.Point(16, 16);
            this.labelSetPointTemperature11.Name = "labelSetPointTemperature11";
            this.labelSetPointTemperature11.Size = new System.Drawing.Size(113, 13);
            this.labelSetPointTemperature11.TabIndex = 0;
            this.labelSetPointTemperature11.Text = "SetPointTemperature1";
            // 
            // labelSetPointTemperature12
            // 
            this.labelSetPointTemperature12.AutoSize = true;
            this.labelSetPointTemperature12.Location = new System.Drawing.Point(158, 16);
            this.labelSetPointTemperature12.Name = "labelSetPointTemperature12";
            this.labelSetPointTemperature12.Size = new System.Drawing.Size(113, 13);
            this.labelSetPointTemperature12.TabIndex = 1;
            this.labelSetPointTemperature12.Text = "SetPointTemperature2";
            // 
            // labelActTemperature2
            // 
            this.labelActTemperature2.AutoSize = true;
            this.labelActTemperature2.Location = new System.Drawing.Point(162, 110);
            this.labelActTemperature2.Name = "labelActTemperature2";
            this.labelActTemperature2.Size = new System.Drawing.Size(106, 13);
            this.labelActTemperature2.TabIndex = 2;
            this.labelActTemperature2.Text = "Actual Temperature2";
            // 
            // labelActTemperature1
            // 
            this.labelActTemperature1.AutoSize = true;
            this.labelActTemperature1.Location = new System.Drawing.Point(23, 110);
            this.labelActTemperature1.Name = "labelActTemperature1";
            this.labelActTemperature1.Size = new System.Drawing.Size(106, 13);
            this.labelActTemperature1.TabIndex = 3;
            this.labelActTemperature1.Text = "Actual Temperature1";
            // 
            // Sp1Value
            // 
            this.Sp1Value.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Sp1Value.Location = new System.Drawing.Point(19, 50);
            this.Sp1Value.Name = "Sp1Value";
            this.Sp1Value.Size = new System.Drawing.Size(110, 45);
            this.Sp1Value.TabIndex = 4;
            // 
            // SP2Value
            // 
            this.SP2Value.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.SP2Value.Location = new System.Drawing.Point(158, 50);
            this.SP2Value.Name = "SP2Value";
            this.SP2Value.Size = new System.Drawing.Size(110, 45);
            this.SP2Value.TabIndex = 5;
            // 
            // ActualValue1
            // 
            this.ActualValue1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ActualValue1.Location = new System.Drawing.Point(19, 134);
            this.ActualValue1.Name = "ActualValue1";
            this.ActualValue1.Size = new System.Drawing.Size(110, 45);
            this.ActualValue1.TabIndex = 6;
            // 
            // ActualValue2
            // 
            this.ActualValue2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ActualValue2.Location = new System.Drawing.Point(158, 134);
            this.ActualValue2.Name = "ActualValue2";
            this.ActualValue2.Size = new System.Drawing.Size(110, 45);
            this.ActualValue2.TabIndex = 7;
            // 
            // ModuleGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ActualValue2);
            this.Controls.Add(this.ActualValue1);
            this.Controls.Add(this.SP2Value);
            this.Controls.Add(this.Sp1Value);
            this.Controls.Add(this.labelActTemperature1);
            this.Controls.Add(this.labelActTemperature2);
            this.Controls.Add(this.labelSetPointTemperature12);
            this.Controls.Add(this.labelSetPointTemperature11);
            this.Name = "ModuleGui";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSetPointTemperature11;
        private System.Windows.Forms.Label labelSetPointTemperature12;
        private System.Windows.Forms.Label labelActTemperature2;
        private System.Windows.Forms.Label labelActTemperature1;
        private System.Windows.Forms.Label Sp1Value;
        private System.Windows.Forms.Label SP2Value;
        private System.Windows.Forms.Label ActualValue1;
        private System.Windows.Forms.Label ActualValue2;

    }
}

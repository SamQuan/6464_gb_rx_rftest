// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataRead\PcasSchema.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.


namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// A list of schemas currently supported by PCAS.
	/// </summary>
	internal enum PCASschema
	{
		amco,
		catvdb,
		chipdb,
		coc,
		hiberdb,
		hbr10db,
		oal,
		oc_proddb,
		train
	}
}

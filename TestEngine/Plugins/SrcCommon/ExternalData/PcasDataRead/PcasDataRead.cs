// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataRead\PcasDataRead.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Collections.Specialized;	// StringDictionary
using System.Globalization;				// CultureInfo
using System.Text;						// StringBuilder
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.InternalData;




namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// Implementation of IDataRead for PCAS
	/// </summary>
	public class PcasDataRead : IDataRead
	{
		/// <summary>
		/// Constructor that allows a data source to be specified.
		/// </summary>
		/// <param name="logger">A component providing an ILogger interface. This will be used to access TestEngine.Framework.Logging</param>
		/// <param name="dataSource">Data source name ( e.g. 'PCAS' )</param>
		public PcasDataRead( ILogging logger, string dataSource )
		{
			if ( logger == null )
			{
				throw new BookhamExternalDataException("'logger' is null");
			}
			this.logger = logger;

			if ( dataSource == null || dataSource.Length == 0) 
			{
				this.dataSource = "PCAS";	// Default to "PCAS"
			}
			else
			{
				this.dataSource = dataSource;
			}
		}



		/// <summary>
		/// Extracts key parameters from the keyTable
		/// </summary>
		/// <param name="keyTable">Name/value pairs of keys</param>
		private void ProcessKeyData(StringDictionary keyTable)
		{
			// Clear any existing data
			inputKeyData = new deviceInfo();
			keyDataWhereClause = new StringBuilder();

			foreach ( string key in keyTable.Keys )
			{
				string keyName = key.ToUpper(CultureInfo.InvariantCulture);
				switch ( keyName )
				{	
					case "DEVICE_TYPE":
						inputKeyData.deviceType = keyTable[key].ToLower(CultureInfo.InvariantCulture);		// Always Lowercase in PCAS
						break;
					case "TEST_STAGE":
						inputKeyData.testStage = keyTable[key].ToLower(CultureInfo.InvariantCulture);		// Always Lowercase in PCAS
						break;
					case "SPECIFICATION":
						inputKeyData.specification = keyTable[key].ToLower(CultureInfo.InvariantCulture);	// Always Lowercase in PCAS
						break;
					case "SERIAL_NO":
						inputKeyData.serialNo = keyTable[key].ToUpper(CultureInfo.InvariantCulture);		// Always Uppercase in PCAS
						break;
					case "SCHEMA":
						inputKeyData.schema = keyTable[key].ToLower(CultureInfo.InvariantCulture);			// Always Lowercase in PCAS
						break;

					default:
						// Use any extra key data for a where clause
						if ( keyDataWhereClause.Length > 0 )
							keyDataWhereClause.Append(" and ");

						keyDataWhereClause.Append( keyName + "=" + keyTable[key] );
						logger.DebugOut("PcasRead","Extra key information : " + keyDataWhereClause.ToString());
						break;
				}
			}
			// Check that the required keys were present
			CheckKeyData();
		}



		/// <summary>
		///  Checks for existence of keys.
		/// SERIAL_NO and SCHEMA must be present. Others are optional.
		/// </summary>
		private void CheckKeyData()
		{

			
			StringBuilder missingKeys = new StringBuilder();

			if ( inputKeyData.serialNo.Length == 0 )
				missingKeys.Append("SERIAL_NO ");

			if ( inputKeyData.schema.Length == 0 ) 
				missingKeys.Append("SCHEMA ");

			if ( missingKeys.Length > 0 )
			{
				throw new BookhamExternalDataException("Missing key data for '" + missingKeys.ToString() );
			}
			
			// Check that the schema is one of our permitted values.
			CheckSchema(inputKeyData.schema);
		}

		/// <summary>
		/// Checks that the schema exists in the PCASschema enum.
		/// </summary>
		/// <param name="schemaName">A string containing the schema name</param>
		/// <returns>TRUE if the schema exists.</returns>
		private Boolean CheckSchema(string schemaName)
		{
			foreach(string name in Enum.GetNames(typeof(PCASschema)))
			{
				if ( String.Compare(name, inputKeyData.schema, true, CultureInfo.InvariantCulture) == 0 )	// FxCop suggests this method
					return true;
			}
			throw new BookhamExternalDataException("Invalid schema '" + schemaName +  "'");
		}

		
		/// <summary>
		/// Updates the key data collection to correspond with the data that was retrieved.
		/// </summary>
		/// <param name="keyObject">Name of keyObject to update</param>
		/// <param name="paramName">Name of param within key object.</param>
		/// <param name="newValue">A new value to assign to the specified paramName</param>
		internal void UpdateKeyData(StringDictionary keyObject, string paramName, string newValue)
		{
			// Only update the key if we have a value for the parameter.
			// Log the event if not. We may be able to do better.
			if ( newValue != null )
			{
				// update key data to pass back to caller
				if ( keyObject[paramName] == null )
				{
					// Add
					keyObject.Add(paramName,newValue);
				} 
				else
				{
					// Update
					keyObject[paramName] = newValue;
				}
			}
			else
			{
				this.logger.DatabaseTransactionWrite("LOG_LABEL","Refusing to update key '" + paramName + " ' with null value");
			}
		}

		

		#region IDataRead Members

		/// <summary>
		/// Retrieves latest results for the device specified by keyObject
		/// </summary>
		/// <param name="keyObject">Must contain SCHEMA and SERIAL_NUMBER. May contain one or more of DEVICE_TYPE , TEST_STAGE , SPECIFICATION. Additional value pairs will be treated as string data and appended to the where clause. Quotes must be included if appropriate to the data type ( e.g. "USER_NAME","'Fred Bloggs'".</param>
		/// <param name="retrieveRawData">Not implemented in this release..</param>
		/// <param name="whereClause">A clause to append to the SQL SELECT statement. Should not contain the word 'WHERE'.</param>
		/// <returns>DatumList containing a single set of results.</returns>
		public DatumList GetLatestResults(StringDictionary keyObject, bool retrieveRawData, string whereClause)
		{
			// KeyData= SCHEMA + SERIAL_NO
			//			SCHEMA + SERIAL_NO + DEVICE_TYPE
			//			SCHEMA + SERIAL_NO + TEST_STAGE
			//			SCHEMA + SERIAL_NO + DEVICE_TYPE + TEST_STAGE
			//			SCHEMA + SERIAL_NO + SPECIFICATION

			// NB. In pcas SERIAL NUMBER is upper case, other keys are lower case
			
			ProcessKeyData(keyObject);
			
			PCASread pcasRead = null;
			DatumList resultsData = null;
			string fullWhereClause = whereClause;

			try
			{
				pcasRead = new PCASread(this.logger, dataSource);
				pcasRead.DeviceInfo = inputKeyData;
						
				if ( keyDataWhereClause.Length > 0 )
				{
					// use any extra key data to form a where clause
					if ( whereClause == null || whereClause.Length == 0 )
					{
						fullWhereClause = keyDataWhereClause.ToString();
					}
					else
					{
						fullWhereClause = keyDataWhereClause.ToString() + " and " + whereClause;
					}
				}

				if ( fullWhereClause.Length == 0 )
				{
					// No WHERE clause.

					// Find latest results from test_link using type / stage / spec if present
					logger.DatabaseTransactionWrite(LOG_LABEL,"getLatestResultsForDevice : SERIAL_NO='"+inputKeyData.serialNo+"',TEST_STAGE='"+inputKeyData.testStage+"',DEVICE_TYPE='"+inputKeyData.deviceType+"',SPECIFICATION='"+inputKeyData.specification+"'");
					resultsData = pcasRead.getLatestResultsForDevice(inputKeyData.serialNo, inputKeyData.testStage, inputKeyData.deviceType, inputKeyData.specification, retrieveRawData);
				} 
				else
				{
					// WHERE clause available.

					if ( inputKeyData.specification != null && inputKeyData.specification.Length != 0 )	
					{
						// Use the spec. This allows us to restrict our search to one table.
						logger.DatabaseTransactionWrite(LOG_LABEL,"getResultsForDevice : SERIAL_NO='"+inputKeyData.serialNo+"',SPECIFICATION='"+keyObject["SPECIFICATION"]+"',WHERE '"+whereClause+"'");
						resultsData = pcasRead.getResultsForDevice(inputKeyData.serialNo, inputKeyData.specification, fullWhereClause, retrieveRawData);
					}
					else
					{
						// Find the latest table that matches the SQL by using device type , serial number & stage
						logger.DatabaseTransactionWrite(LOG_LABEL,"getResultsForDevice : SERIAL_NO='"+inputKeyData.serialNo+"',DEVICE_TYPE='"+inputKeyData.deviceType+"',TEST_STAGE='"+inputKeyData.testStage+"',WHERE '"+whereClause+"'");
						resultsData = pcasRead.getResultsForDevice( inputKeyData.serialNo, inputKeyData.deviceType, inputKeyData.testStage, fullWhereClause, retrieveRawData );
					}

				}
				
				// Removed. An AccuRev issue addresses this ...
				//
				// update key data to reflect what we actually found
				//UpdateKeyData(keyObject,"DEVICE_TYPE",pcasRead.DeviceInfo.deviceType);
				//UpdateKeyData(keyObject,"TEST_STAGE",pcasRead.DeviceInfo.testStage);
				//UpdateKeyData(keyObject,"SPECIFICATION",pcasRead.DeviceInfo.specification);
			}
			finally
			{
				// Always call Disconnect
				if ( pcasRead != null )
					pcasRead.Disconnect();
			}
			
			return resultsData;
		}

		/// <summary>
		/// Retrieves latest results for the device specified by keyObject
		/// </summary>
		/// <param name="keyObject">Must contain SCHEMA and SERIAL_NUMBER. May contain one or more of DEVICE_TYPE , TEST_STAGE , SPECIFICATION. Additional value pairs will be treated as strings data and appended to the where clause.</param>
		/// <param name="retrieveRawData">Not implemented in this release..</param>
		/// <returns>DatumList containing a single set of results.</returns>
		public DatumList GetLatestResults(StringDictionary keyObject, bool retrieveRawData)
		{
			DatumList datumList = this.GetLatestResults(keyObject,retrieveRawData,"");
			return datumList;
		}

		/// <summary>
		/// Retrieves all test results for the device specified by keyObject.
		/// </summary>
		/// <param name="keyObject">Must contain SCHEMA and SERIAL_NUMBER. May contain one or more of DEVICE_TYPE , TEST_STAGE , SPECIFICATION. Additional value pairs will be treated as strings data and appended to the where clause.</param>
		/// <param name="retrieveRawData">Not implemented in this release.</param>
		/// <param name="whereClause">'Where' clause to append to SQL SELECT statement. Should not include the word 'WHERE'</param>
		/// <returns>Array of DatumLists containing all matching results sets.</returns>
		public DatumList[] GetResults(StringDictionary keyObject, bool retrieveRawData, string whereClause)
		{
			ProcessKeyData(keyObject);
			
			PCASread pcasRead = null;
			DatumList[] resultsData = null;
			
			pcasRead = new PCASread(this.logger, dataSource);

			try
			{
				pcasRead.DeviceInfo = inputKeyData;
									
				if ( keyDataWhereClause.Length > 0 )
				{
					// use any extra key data to form a where clause
					if ( whereClause == null || whereClause.Length == 0 )
					{
						whereClause = keyDataWhereClause.ToString();
					}
					else
					{
						whereClause = keyDataWhereClause.ToString() + " and " + whereClause;
					}
				}

				if ( inputKeyData.specification != null && inputKeyData.specification.Length != 0 )	
				{
					// Use the spec if we have one. Just look in this one table.
					logger.DatabaseTransactionWrite(LOG_LABEL,"getMultipleResultsForDevice : SERIAL_NO='"+inputKeyData.serialNo+"',SPECIFICATION='"+keyObject["SPECIFICATION"]+"',WHERE '"+whereClause+"'");
					resultsData = pcasRead.getMultipleResultsForDevice(inputKeyData.serialNo, inputKeyData.specification, whereClause, retrieveRawData);
				}
				else
				{
					// Find all tables that matching the SQL by using serial number + device type & stage if present
					logger.DatabaseTransactionWrite(LOG_LABEL,"getMultipleResultsForDevice : SERIAL_NO='"+inputKeyData.serialNo+"',DEVICE_TYPE='"+inputKeyData.deviceType+"',TEST_STAGE='"+inputKeyData.testStage+"',WHERE '"+whereClause+"'");					
					resultsData = pcasRead.getMultipleResultsForDevice( inputKeyData.serialNo, inputKeyData.deviceType, inputKeyData.testStage, whereClause, retrieveRawData );
				}

			}
			finally
			{
				// Call Disconnect
				pcasRead.Disconnect();
			}
			
			return resultsData;
		}

		/// <summary>
		/// Holds the name of the data source.
		/// </summary>
		internal string dataSource;
		/// <summary>
		/// This is used to pass logging messages back to the core Log component.
		/// </summary>
		internal ILogging logger;
		/// <summary>
		/// This is constructed from any extra key/value pairs passed in via the key data collection.
		/// </summary>
		internal StringBuilder keyDataWhereClause;
		/// <summary>
		/// Used by the Logging component as the 'searchLabel' 
		/// </summary>
		const string LOG_LABEL = "ExtData_PcasDataRead";
		/// <summary>
		/// A structure containing key data relating to one device.
		/// </summary>
		internal deviceInfo inputKeyData;

		#endregion
	}

	/// <summary>
	/// A structure containing key data relating to one device.
	/// </summary>
	internal struct deviceInfo
	{
		/// <summary>
		/// Serial number of the device
		/// </summary>
		internal string serialNo;
		/// <summary>
		/// PCAS device type
		/// </summary>
		internal string deviceType;
		/// <summary>
		/// PCAS test stage
		/// </summary>
		internal string testStage;
		/// <summary>
		/// PCAS spec_id
		/// </summary>
		internal string specification;
		/// <summary>
		/// PCAS schema name
		/// </summary>
		internal string schema;
	}
}

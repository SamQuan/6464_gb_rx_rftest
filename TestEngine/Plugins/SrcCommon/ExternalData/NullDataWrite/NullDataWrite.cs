using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;

namespace Bookham.TestLibrary.ExternalData
{
    public class NullDataWrite : IDataWrite
    {
        /// <summary>
        /// This constructor used by the TestEngine to instantiate the component.
        /// </summary>
        /// <param name="logger">Unused</param>
        /// <param name="dropDirectory">Unused</param>
        public NullDataWrite(ILogging logger, string dropDirectory)
        {
            // Do nothing
        }

        #region IDataWrite Members
        /// <summary>
        /// This method does nothing at all, any data passed to it is ignored. 
        /// It exists simply to keep give the TestEngine something to 'write' to at the end of the sequence.
        /// </summary>
        /// <param name="testData">Unused</param>
        /// <param name="keyData">Unused</param>
        /// <param name="specName">Unused</param>
        /// <param name="ignoreMissing">Unused</param>
        public bool Write(TestData testData, System.Collections.Specialized.StringDictionary keyData, string specName, bool ignoreMissing, bool usingUtcDateTime)
        {
            return true;
            // Do nothing.
        }

        #endregion

       
    }
}

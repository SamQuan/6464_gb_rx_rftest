// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\PcasLimitConvertUtils.cs
// 
// Author: Mark Fullalove - refactored by Paul Annetts from PcasLimit class
// Design: External Data Library Design Document.

using System;
using System.Globalization;							// CultureInfo
using Bookham.TestEngine.PluginInterfaces.ExternalData; // Exception
using Bookham.TestEngine.Framework.InternalData;	// AccuracyTrimmer

namespace Bookham.TestLibrary.ExternalData
{
    /// <summary>
    /// Helper class to give access to PCAS data accuracy and FileLink type
    /// conversion rules
    /// </summary>
    sealed public class PcasLimitConvertUtils
    {
        /// <summary>
        /// private constructor - only have static methods
        /// </summary>
        private PcasLimitConvertUtils() { }

        #region Public Static Functions
        
        /// <summary>
        /// Returns the maximum length of a specified data type.
        /// </summary>
        /// <returns>Maximum length in number of characters</returns>
        public static int GetAccuracy(PCASdataType dataType, string format)
        {
            switch (dataType)
            {
                // Attempt to convert the PCAS format string into something sensible.
                // Do not do this for string types.

                case PCASdataType.PCAS_NUMBER_38:
                    return interpretFormat(format);

                case PCASdataType.PCAS_FLOAT_126:
                    return interpretFormat(format);

                case PCASdataType.PCAS_VARCHAR2_256:
                    return 256;

                case PCASdataType.PCAS_VARCHAR2_80:
                    return 80;

                case PCASdataType.PCAS_VARCHAR2_20:
                    return 20;

                case PCASdataType.PCAS_VARCHAR2_14:
                    return 14;

                default:
                    // Never get here
                    throw new BookhamExternalDataException("PCASlimit::maxLengthForDataType - corrupt dataType");
            }
        }

        /// <summary>
        /// Get the DatumType from the PCAS short descriptor
        /// </summary>
        /// <param name="shortDesc">PCAS short descriptor</param>
        /// <returns>DatumType</returns>
        public static DatumType GetDatumTypeFromStringShortDesc(string shortDesc)
        {
            // The remaining types are treated as strings apart from the following special case :
            //
            // Special case : "PLOT", "IMAGE", "RAW", "FILE", "MATRIX" strings are dealt with as DatumFileLinks
            if ((shortDesc.StartsWith("FILE_")
                && (!shortDesc.EndsWith("_TIMESTAMP"))) ||
                shortDesc.EndsWith("_FILE")             ||
                (shortDesc.Contains("_FILE_")
                && (!shortDesc.EndsWith("_TIMESTAMP"))) ||
                shortDesc.StartsWith("PLOT_")           ||
                shortDesc.EndsWith("_PLOT")             ||
                shortDesc.Contains("_PLOT_")            ||
                shortDesc.StartsWith("IMAGE_")          ||
                shortDesc.EndsWith("_IMAGE")            ||
                shortDesc.Contains("_IMAGE_")           ||
                (shortDesc.StartsWith("MATRIX_") &&
                (!shortDesc.Equals("MATRIX_NUMBER")))   ||
                shortDesc.EndsWith("_MATRIX")           ||
                shortDesc.Contains("_MATRIX_")          ||
                shortDesc.StartsWith("RAW_")            ||
                shortDesc.EndsWith("_RAW")              ||
                shortDesc.Contains("_RAW_")) 
            {
                return DatumType.FileLinkType;
            }
            else
            {
                return DatumType.StringType;
            }

        }

        #endregion

        #region Private static functions
                
        /// <summary>
        /// This attempts to convert the information from the 'format' field in a PCAS limits file into something meaningful.
        /// For numeric data types it returns a number of decimal places.
        /// For string data it returns the maximum allowed length of the string.
        /// </summary>
        /// <returns>An integer number of decimal places or maximum string length.</returns>
        private static int interpretFormat(string format)
        {
            // "none" or "&" means 'no rounding'
            if ((format.IndexOf("none") > 0) || (format == "&"))
            {
                return AccuracyTrimmer.AccuracyFactorDefault; // This value means "No Rounding"
            }

            // S4D.3D or SDD.DDDD.
            if (format.IndexOf("D") > 0)
            {
                int digit = format.IndexOf(".");
                // Check that the "." is contained within the string, and is not the last character.
                if (digit == format.Length || digit < 0)
                    throw new BookhamExternalDataException("Badly formatted pcas limit :'" + format + "'");
                // Look at the character after the "."
                string formatNo = format[digit + 1].ToString();
                if (formatNo == "D")
                {
                    // SDD.DDDD = return 4
                    return Convert.ToInt32(format.Length - digit - 1, CultureInfo.InvariantCulture);
                }
                else
                {
                    // S4D.3D = return 3
                    return Convert.ToInt32(formatNo, CultureInfo.InvariantCulture);
                }
            }

            // 80A. Return the value preceeding the "A"
            if (format.EndsWith("A"))
            {
                return Convert.ToInt32(format.Substring(0, format.Length - 1), CultureInfo.InvariantCulture);
            }

            //  ##0.00## or 0.0000. Return number of characters following the decimal point.
            int position = format.IndexOf(".");
            if (position > 0)
            {
                return Convert.ToInt32(format.Length - position - 1);
            }

            // '0' itself is allowed as it is just rounding to an integer.
            // PJA Addition 23/03/06.
            if (format == "0")
            {
                return 0;
            }

            // should not get here
            throw new BookhamExternalDataException("ExtData_PcasLimit: Unhandled format string '" + format + "'");
        }

        #endregion

    }
}

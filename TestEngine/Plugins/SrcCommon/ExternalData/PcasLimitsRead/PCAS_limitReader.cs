// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\Pcas_LimitReader.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Data.OleDb;				// OleDbConnection
using System.Globalization;				// CultureInfo
using System.Text;						// StringBuilder
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.ExternalData;
using Bookham.TestEngine.PluginInterfaces.ExternalData;	// ILogging

namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// This class creates a connection to the PCAS database and provides a 'getSpec' method to retrieve specification data.
	/// </summary>
	internal class PCAS_limitReader
	{
		/// <summary>
		/// Create a PCAS_limitReader object and open connection to the PCAS database.
		/// </summary>
		/// <param name="DataSource">Data source name. Defaults to 'PCAS' if empty.</param>
		/// <param name="logger">A reference to the component providing logging functionality.</param>
		internal PCAS_limitReader(ILogging logger, string DataSource)
		{
			if ( DataSource == null || DataSource.Length == 0 )
			{
				throw new BookhamExternalDataException("Missing DataSource");
			}
			if ( logger == null )
			{
				throw new BookhamExternalDataException("'logger' is null");
			}
			this.logger = logger;
			Connect("Provider=MSDAORA; Data Source=" + DataSource + "; User ID=pcas; Password=test");
		}

		/// <summary>
		/// Connects to the data source
		/// </summary>
		/// <param name="ConnectionString">The data source connection string</param>
		private void Connect(string ConnectionString)
		{
			dbConnection = new OleDbConnection(ConnectionString);
			this.logger.DatabaseTransactionWrite(LOG_LABEL,"Opening connection to PCAS");
			dbConnection.Open();
		}

		/// <summary>
		/// Disconnects from the data source
		/// </summary>
		internal void Disconnect()
		{
			this.logger.DatabaseTransactionWrite(LOG_LABEL,"Closing connection to PCAS");
			dbConnection.Close();		
		}

		/// <summary>
		/// Returns a spec_id from PCAS that matches the key values passed in.
		/// </summary>
		/// <param name="schema">The PCAS schema</param>
		/// <param name="deviceType">The PCAS device_type</param>
		/// <param name="testStage">The PCAS test_stage</param>
		/// <param name="specificationId">The PCAS spec_id</param>
		/// <returns>The matching PCAS spec_id, or an empty string if none found.</returns>
		private string getLatestSpec( string schema, string deviceType, string testStage, string specificationId )
		{
			StringBuilder sqlStmt = new StringBuilder();
			StringBuilder whereClause = new StringBuilder();

			sqlStmt.Append("select SPEC_ID from "+schema+".test_master where ");
			
			if ( deviceType != null && deviceType.Length > 0 )
			{
				whereClause.Append("device_type='"+deviceType+"'");
			}
			
			if ( testStage != null && testStage.Length > 0 )
			{
				if ( whereClause.Length > 0 ) 
					whereClause.Append(" and ");

				whereClause.Append("test_stage='"+testStage+"'");
			}

			if ( specificationId != null && specificationId.Length > 0 )
			{
				if ( whereClause.Length > 0 ) 
					whereClause.Append(" and ");

				whereClause.Append("spec_id='"+specificationId+"'");
			}
			
			// Ensure that we get the latest spec first in the list
            whereClause.Append(" order by results_table_id desc,spec_id desc");

			sqlStmt.Append(whereClause.ToString());

			OleDbCommand myCommand = new OleDbCommand(sqlStmt.ToString(),dbConnection);
			OleDbDataReader myReader;

			logger.DatabaseTransactionWrite(LOG_LABEL,sqlStmt.ToString());
			myReader = myCommand.ExecuteReader();
			
			string specId = "";
			
			try
			{
				
				if ( myReader.Read() )
				{
					specId = myReader.GetString(0);
				}
				else
				{
					logger.DatabaseTransactionWrite(LOG_LABEL,"No data found for '" + sqlStmt.ToString() + "'");
				}
			}
			finally
			{
				myReader.Close();
			}
			return specId;
		}


		/// <summary>
		/// Retrieves the PCAS specification as a PCASspec object.
		/// </summary>
		/// <param name="schema">The PCAS schema</param>
		/// <param name="deviceType">The PCAS device_type</param>
		/// <param name="testStage">The PCAS test_stage</param>
		/// <param name="specificationId">The PCAS spec_id</param>
		/// <returns>The PCAS specification as a PCASspec object</returns>
		internal PCASspec getSpec( string schema, string deviceType, string testStage, string specificationId )
		{
			string specId = getLatestSpec( schema, deviceType, testStage, specificationId );
            logger.DatabaseTransactionWrite(LOG_LABEL, "Latest specId is '" + specId + "'");
			if ( specId == null || specId.Length == 0 )
			{
				logger.ErrorInExternalDataPlugin("No Specification found for DeviceType='" + deviceType + "' , TestStage = '" + testStage + "' , Schema = '" + schema + "'");
			}
			return getSpec( schema, specId );
		}


		/// <summary>
		/// Get the named PCAS spec object
		/// </summary>
		/// <param name="schema">Schema</param>
		/// <param name="name">Spec name</param>
		/// <returns>The PCAS spec object</returns>
		internal PCASspec getSpec( string schema, string name )
		{
			// Note. We do not retrieve ONFAIL or REGRADE. They are not used by the TestEngine.
			string sqlStmt = "select SHORT_DESC,MINIMUM,MAXIMUM,FORMAT,STORE,PRIORITY,UNITS from "+schema+"." + name + " order by short_desc";
			OleDbCommand dbCommand = new OleDbCommand(sqlStmt,dbConnection);
			OleDbDataReader dbReader;

			logger.DeveloperWrite(LOG_LABEL,sqlStmt);
			dbReader = dbCommand.ExecuteReader();
			PCASspec spec = new PCASspec(name);
			try
			{
				
				while( dbReader.Read() )
				{
					PCASlimit limit = new PCASlimit(logger);
					object valueAsObject;

					limit.shortDesc = dbReader.GetString(0);		// VARCHAR2(30)
					limit.min = dbReader.GetDouble(1);				// FLOAT(126)
					limit.max =  dbReader.GetDouble(2);				// FLOAT(126)

					limit.format = dbReader.GetString(3);			// VARCHAR2(12)
				
					// Due to inconsistencies in PCAS the following fields can be either DOUBLE or DECIMAL
					// This means that we need to read them as 'object' types and convert.
					valueAsObject = dbReader.GetValue(4);
					limit.setDataType( Convert.ToInt32(valueAsObject,CultureInfo.InvariantCulture) );
								
					valueAsObject = dbReader.GetValue(5);
					limit.priority = Convert.ToInt32(valueAsObject,CultureInfo.InvariantCulture);
                    
                    limit.units = (dbReader.GetString(6)).Trim();
			
					//
					//  Workaround
					//
					//  OleDbDataReader.GetDouble() appears to have a precision bug.
					//
					// Example :-
					//		'select MINIMUM from hbr10db.tm10n_dyn_01_00_18 where short_desc='AC_VPI_25C' returns 3.3
					//		Whereas GetDouble() returns 3.3000000000000003
					//
					if ( limit.dataType == PCASdataType.PCAS_FLOAT_126 || limit.dataType == PCASdataType.PCAS_NUMBER_38 )
					{
						limit.min =  AccuracyTrimmer.TrimDouble(limit.min,limit.accuracy());	// FLOAT(126)
						limit.max =  AccuracyTrimmer.TrimDouble(limit.max,limit.accuracy());	// FLOAT(126)
					}

					spec.Add( limit );
				}
			}
			finally
			{
				dbReader.Close();
			}
			return spec;
		}

		/// <summary>
		/// Reference to the component providing logging functions
		/// </summary>
		ILogging logger;

		/// <summary>
		/// Data source connection.
		/// </summary>
		OleDbConnection dbConnection; 

		/// <summary>
		/// A label used to identify our messages with the logging component.
		/// </summary>
		private const string LOG_LABEL = "PCASlimitReader";

	}
}

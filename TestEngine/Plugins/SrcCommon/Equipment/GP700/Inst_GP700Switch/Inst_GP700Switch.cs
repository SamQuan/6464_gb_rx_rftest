// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_GP700Switch.cs
//
// Author: Bill P. Godfrey, 2006
// Design: GP700 Operation Manual

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument to control a DiCon Fiberoptics Inc GP700, M and S switches.
    /// </summary>
    public class Inst_GP700Switch : InstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID. "Sn" or "Mn". Switch type 'M' or 'S' followed by index.</param>
        /// <param name="subSlotInit">Unused.</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_GP700Switch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "DiCon Fiberoptics Inc  GP700",		// hardware name 
                "0",  			                    // minimum valid firmware version 
                "Version 1.02");        			// maximum valid firmware version 
            ValidHardwareData.Add("GP700", instrVariant1);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_GP700",	// chassis driver name  
                "0",				// minimum valid chassis driver version  
                "2.0.0.0");			// maximum valid chassis driver version
            ValidChassisDrivers.Add("GP700", chassisInfo);

            // initialise this instrument's chassis
            this.gp700Chassis = (Chassis_GP700)chassisInit;

            /* Validate slot id */
            if (Slot.Length < 2)
            {
                throw new InstrumentException("Slot \"" + Slot + "\" is too short.");
            }

            bool firstChar = true;
            foreach (char slotChar in Slot)
            {
                /* If firstChar, check M/S prefix. */
                if (firstChar && (slotChar != 'M') && (slotChar != 'S'))
                {
                    throw new InstrumentException("Slot \"" + Slot + "\" does not begining with 'M' or 'S'.");
                }
                    /* If not first char, check it is a digit. */
                else if (!firstChar && !char.IsDigit(slotChar))
                {
                    throw new InstrumentException("Slot \"" + Slot + "\" contains a non-numeric after the first character.");
                }

                /* Won't be the first character next time around. */
                firstChar = false;
            }
            

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return gp700Chassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return gp700Chassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            /* Send a reset command. */
            gp700Chassis.Write("*RST", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    /* Nothing to do. */               
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_GP700 gp700Chassis;
        #endregion

        #region Private switch type classification helper functions.
        /// <summary>
        /// Gets true if instrument is an Sn switch.
        /// </summary>
        private bool isSSwitch
        {
            get { return (Slot[0] == 'S'); }
        }

        /// <summary>
        /// Gets true if instrument is an Mn switch.
        /// </summary>
        private bool isMSwitch
        {
            get { return !isSSwitch; }
        }
        #endregion

        #region Limits fetch.
        /// <summary>
        /// Call "SYST:CONF?" and parse response for current switch's maximum limit.
        /// </summary>
        /// <returns>Extracted max limit level.</returns>
        private int getMaxLimit()
        {
            /* S switches are all 1:2 switches, so return 2. */
            if (isSSwitch) { return 2; }

            /* Else, M switch. */

            /* Perform a "SYST:CONF?" query and parse the result looking for the current M switch
                 * and return limit. */
            string resp = gp700Chassis.Query("SYST:CONF?", this);
            string[] respByComma = resp.Split(',');

            /* Evaluate each part. */
            foreach (string item in respByComma)
            {
                /* Trim item before parsing. */
                string trimmedItem = item.Trim();

                /* Ignore empty parts. */
                if (trimmedItem.Length == 0) { continue; }

                /* Ignore items not begining with an 'M'. */
                if (trimmedItem[0] != 'M') { continue; }

                /* Split item by spaces, ignore if not in 3 parts. */
                string[] subItems = trimmedItem.Split(' ');
                if (subItems.Length != 3) { continue; }

                /* Check if item is for this slot id. */
                if (!matchingSlotId(subItems[0])) { continue; }

                /* Look for subitem with 'A' prefix. */
                foreach (string subItem in subItems)
                {
                    /* Ignore empty subitems. */
                    if (subItem.Length == 0) { continue; }

                    /* Check first character. We are only interesting in values with an initial 'A'. */
                    if (subItem[0] == 'A')
                    {
                        /* Remove prefix, convert to int and return. */
                        try
                        {
                            return int.Parse(subItem.Remove(0, 1));
                        }
                        /* Ignore expected exceptions thrown by int.Parse, just ignore them. */
                        catch (System.FormatException) { continue; }
                        catch (System.OverflowException) { continue; }
                    }

                    /* It didn't start with an 'A', try next one. */
                }
                /* If we get here, the required item was not found. Ignore and loop again. */

            } /* END foreach item. */

            /* If we get here, the requested item was not returned. Throw exception. */
            throw new InstrumentException(
                "Cound not find requested information in SYST:CONF response. \"" + resp + "\".");
        }

        /// <summary>
        /// Check if candidate slotid liberally matches actual Slot ID. 
        /// </summary>
        /// <param name="candSlot">Candidate slot id.</param>
        /// <returns>True if matches. False on failed match.</returns>
        private bool matchingSlotId(string candSlot)
        {
            /* Reject too short strings. */
            if (candSlot.Length < 2) { return false; }

            /* Check that second char is a digit. */
            if (!Char.IsDigit(candSlot[1])) { return false; }

            /* Remove 'M' prefix. */
            candSlot = candSlot.Remove(0, 1);
            string actSlot = Slot.Remove(0, 1);

            /* Parse into integer. */
            int cand, act;
            try
            {
                cand = int.Parse(candSlot);
                act = int.Parse(actSlot);
            }
            catch (Exception)
            {
                /* If it failed int.Parse, ignore it. */
                return false;
            }

            return (cand == act);           
        }
        #endregion

        #region Public limits query properties.
        /// <summary>
        /// Gets max limit for output.
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                return getMaxLimit(); 
            }
        }
 
        /// <summary>
        /// Gets min limit for output. S=1 M=0.
        /// </summary>
        public override int MinimumSwitchState
        {
            get { return isSSwitch ? 1 : 0; }
        }
        #endregion

        #region Switch query helper functions.
        /// <summary>
        /// Fetch a switch position.
        /// </summary>
        /// <returns>Input or output position.</returns>
        private int querySwitchPos()
        {
            /* Perform command. */
            string resp = gp700Chassis.Query(Slot + "?", this);

            if (isSSwitch)
            {
                /* Handle S response. */
                try
                {
                    return int.Parse(resp);
                }
                catch (Exception)
                {
                    throw new InstrumentException("Bad response. \"" + resp + "\".");
                }                
            }
            else
            {
                /* Handle M response. */
                string[] respByComma = resp.Split(',');

                /* Check length. */
                if (respByComma.Length != 2)
                {
                    throw new InstrumentException(
                        "GP700 returned \"" +
                        resp +
                        "\" to \"" +
                        Slot +
                        "?\" command. (Expected 2 parts.)");
                }

                /* Attempt convertion of first part to an integer. */
                try
                {
                    return int.Parse(respByComma[0]);
                }
                catch (Exception)
                {
                    throw new InstrumentException("Bad response. \"" + resp + "\".");
                }
            }
        }
        #endregion

        #region Public switch sets/gets
        /// <summary>
        /// Gets or sets current output position. (When setting, will use pre-existing input
        /// position.)
        /// </summary>
        public override int SwitchState
        {
            get
            {
                return querySwitchPos();
            }
            set
            {
                /* Build cmd string, "Sn x" or "Mn x". */
                string gpibCmd = Slot + " " + value;

                /* Send command. */
                gp700Chassis.Write(gpibCmd, this);
            }
        }
        #endregion
    }
}

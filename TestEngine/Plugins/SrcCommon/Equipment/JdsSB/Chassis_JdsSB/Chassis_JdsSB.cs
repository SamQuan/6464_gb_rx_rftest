// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_JdsSB.cs
//
// Author: mark.fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    public class Chassis_JdsSB : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_JdsSB(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with real values, create multiple copies as required.
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "JDS FITEL Inc., SB",			// hardware name 
                "0",			// minimum valid firmware version 
                "3.13");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("IDN?", null).Split(',');

                // Return fields 1 & 2, the manufacturer name and eqipment name.
                return idn[0].Trim() + ", " + idn[1].Trim();
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // Nothing to do here before we go online
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // 1 = parameter error , 32 = syntax error , 128 = self test error
                    // this.StandardEventRegisterMask = 1 + 32 + 128;
                    // clear the status registers
                    this.Write_Unchecked("CLR", null);                    
                }
            }
        }
        #endregion
    }
}

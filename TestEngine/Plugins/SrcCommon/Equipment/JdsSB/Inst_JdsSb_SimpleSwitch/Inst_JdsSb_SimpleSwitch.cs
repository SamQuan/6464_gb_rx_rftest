// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_JdsSb_SimpleSwitch.cs
//
// Author: mark.fullalove, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    public class Inst_JdsSb_SimpleSwitch : InstType_SimpleSwitch, IInstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JdsSb_SimpleSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "JDS FITEL Inc., SB",				// hardware name 
                "0",  			// minimum valid firmware version 
                "3.13");			// maximum valid firmware version 
            ValidHardwareData.Add("JdsSB", instrVariant1);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JdsSB",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "3.13");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("JdsSB", chassisInfo);

            // initialise this instrument's chassis
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JdsSB)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write_Unchecked("RESET", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // No custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JdsSB instrumentChassis;
        /// <summary>
        /// Maximum switch position. This is checked every time, so the value is cached.
        /// </summary>
        private int maximumSwitchState;
        #endregion


        public override int MaximumSwitchState
        {
            get
            {
                if (maximumSwitchState == 0)
                {
                    maximumSwitchState = Convert.ToInt32(instrumentChassis.Query_Unchecked("CLOSE? MAX", this));
                }
                return maximumSwitchState;
            }
        }

        public override int MinimumSwitchState
        {
            get { return 1; }
        }

        public override int SwitchState
        {
            get
            {
                return Convert.ToInt32(instrumentChassis.Query_Unchecked("CLOSE?", this));
            }
            set
            {
                if ( value < MinimumSwitchState || value > MaximumSwitchState )
                    throw new InstrumentException("Invalid switch Position Specified: " + value);

                instrumentChassis.Write_Unchecked("CLOSE " + value.ToString(), this);
            }
        }
    }
}

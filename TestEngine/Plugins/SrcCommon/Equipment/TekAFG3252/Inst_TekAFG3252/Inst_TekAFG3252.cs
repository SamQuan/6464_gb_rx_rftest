// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: Feifeng.zhang, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Tek AFG3252
    /// /// Instrument Driver
    /// This is a SCPI Driver for the AFG;
    /// </summary>
    
    public class Inst_TekAFG3252: InstType_AcousticOptoModulator
    //public class Inst_TekAFG3252: InstType_functionGenerator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_TekAFG3252(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord TekAFG3252Data = new InstrumentDataRecord(
                "AFG3252",	    // hardware name 
                "0.0.1",			// minimum valid firmware version 
                "2.1.0");		// maximum valid firmware version
            ValidHardwareData.Add("TekAFG3252", TekAFG3252Data);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_TekAFG3252",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_TekAFG3252", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_TekAFG3252)chassisInit;
            // put it into a local so it is esier to debug
            //theSlot = base.Slot;
        }
        #endregion

        #region Instrument overrides
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string hard = instrumentChassis.Query("*IDN?", null);
                string[] info = hard.Split(':');
                return info[2].ToString();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string hard = instrumentChassis.Query("*IDN?", null);
                string[] info = hard.Split(',');
                return info[1].ToString();
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        /// 
        public override void SetDefaultState()
        {
            // using interface
            // using scpi

           
            string Header = "SOUR" + "1";
            // continues waveform
            InstWrite("MODE" + "1" + ":TYPE CONT");
            // Square wave this enfores a default 50% duty cycle
            InstWrite(Header + ":FUNC:SHAP SIN");
            // voltages
            InstWrite(Header + ":VOLT:AMPL 0.12V");
            InstWrite(Header + ":VOLT:OFFS 1V");
            // set default duty cycle for each channel

            // frequency
            this.Frequency_Hz = 130;
            // switch it on
            InstWrite("OUTP" + theSlot + ":STAT OFF");
            this.PhaseOffset_degrees = 0;
            // this a trick which forces a phase couple, it was found that the INST:COUP:PHAS command would not work if the channel was not set manually so this was employed instead
            InstWrite("*SAV 4");
            InstWrite("*RCL 4");
             
        }

      
        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
                //this.SetDefaultState();

                if (value) // if setting online                
                {
                  
                         this.SetDefaultState();
                         //changed by feifeng to prevent the equipment on when started.
                         //InstWrite("OUTP" + "1" + ":STAT ON");
                 }
                else
                         InstWrite("OUTP" + "1" + ":STAT OFF");
            }
        }
        public void SetCondition1State()
        {
            // using interface   

           
            string Header = "SOUR" + "1";
            // continues waveform
            InstWrite("MODE" + "1" + ":TYPE CONT");
            // Square wave this enfores a default 50% duty cycle
            InstWrite(Header + ":FUNC:SHAP SIN");
            // voltages
            //InstWrite(Header + ":VOLT:AMPL 0.05V");
            InstWrite(Header + ":VOLT:AMPL 0.05VPP");
            InstWrite(Header + ":VOLT:OFFS 1V");
            // set default duty cycle for each channel
          
            // frequency
            this.Frequency_Hz = 1400;
            // switch it on
            //InstWrite("OUTP" + theSlot + ":STAT ON");
            this.PhaseOffset_degrees = 0;
            // this a trick which forces a phase couple, it was found that the INST:COUP:PHAS command would not work if the channel was not set manually so this was employed instead
            InstWrite("*SAV 4");
            InstWrite("*RCL 4");


        }
        public void SetCondition2State()
        {
            // using interface        
            
            string Header = "SOUR" + "1";
            // continues waveform
            InstWrite("MODE" + "1" + ":TYPE CONT");
            // Square wave this enfores a default 50% duty cycle
            InstWrite(Header + ":FUNC:SHAP SIN");
            // voltages
            //InstWrite(Header + ":VOLT:AMPL 0.05V");
            InstWrite(Header + ":VOLT:AMPL 0.12VPP");
            InstWrite(Header + ":VOLT:OFFS 1V");
            
            // frequency
            this.Frequency_Hz = 130;
            // switch it on
            //InstWrite("OUTP" + theSlot + ":STAT ON");
            this.PhaseOffset_degrees = 0;
            // this a trick which forces a phase couple, it was found that the INST:COUP:PHAS command would not work if the channel was not set manually so this was employed instead
            InstWrite("*SAV 5");
            InstWrite("*RCL 5");


        }
        /// <summary>
        /// Set the instrument enable or disable
        /// </summary>
        public bool Output_Enable
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
               if (value) // if setting online                
                {                   
                    InstWrite("OUTP" + "1" + ":STAT ON");
                }
                else
                    InstWrite("OUTP" + "1" + ":STAT OFF");
            }
        }
        #endregion

        public float Amplitude
        {

            get
            {
                float ampl = Convert.ToSingle(InstQuery("SOUR" + "1" + ":VOLT:AMPL?"));
                return ampl;
            }
            set
            {
                float ampl = Convert.ToSingle(value);
                //string flush = instrumentChassis.Query("*OPC?"); // needs to be done for this driver
                InstWrite("SOUR" + "1" + ":VOLT:AMPL: " + ampl.ToString()+"VPP");


            }
        }

        #region AOM InstrumentType property overrides

        /// <summary>
        /// Duty Cycle as %
        /// </summary>
        public override float DutyCycle_percent
        {
        
            get
            {
                //float DutCy = Convert.ToSingle(InstQuery("SOUR" + theSlot + ":PULS:DCYC?"));
                //double DutCy = 50;
                return 50;
            }
            set
            {
                if (value == 50)  // if 50 then that is the default setting
                {
                    InstWrite(string.Format("SOUR{0}:FUNC:SHAP SQU",theSlot));
                    
                }
                else
                {
                    // we have added an adjustment to acount for the unsquare shape of the FG in this mode
                    value += 5; // because we normally would specifiy 50 or 25 the 25 will be bumped up to 30 with this particuliar FG
                    // define wavelength to be 20 points max with a freq of 701kHz
                    // we can only have a resolution of 5% duty cycle
                    int maxAllowedPoint = 20;
                    
                    InstWrite(string.Format("DATA:DEF EMEM,{0}",maxAllowedPoint)); // prepare the memory size
                    int onCount = Convert.ToInt16(Math.Round(((value * maxAllowedPoint) / 100))); // determine the number of high points, this should round up
                    // construct the shape and put it into the AFGs working memory
                    for (int pointIdx = 1; pointIdx <= maxAllowedPoint; pointIdx++)
                            InstWrite(string.Format("TRAC:VAL EMEM,{0},{1}", pointIdx,((pointIdx <= onCount) ? 2047 : 0))); // point on or off depending on whether pointIdx id less or equal to on
                   
                    
                    // assinged to the channel Function ( this will not allow phase shift unlike normal square wave )
                    InstWrite(string.Format("SOUR{0}:FUNC EMEM",theSlot));
                    // re phase lock the two channels
                    //InstWrite("INST:COUP:PHAS ALL");
                    this.PhaseOffset_degrees = 0;
                    InstWrite("*SAV 5");
                    InstWrite("*RCL 5");
                }

                //float DutCy = Convert.ToSingle(value);
                //InstWrite(string.Format("SOUR{0}:PULS:DCYC {1}",theSlot,DutCy);
            }
        }
        /// <summary>
        /// property get/set Frequnecy in Hertz
        /// </summary>
        public override float Frequency_Hz
        {
           
            get
            {
                float Freq = Convert.ToSingle(InstQuery("SOUR" + theSlot + ":FREQ?"));
                return Freq;
            }
            set
            {
                float Freq = Convert.ToSingle(value);
                //string flush = instrumentChassis.Query("*OPC?"); // needs to be done for this driver
                InstWrite("SOUR" + theSlot + ":FREQ " + Freq.ToString());
                
                
            }
        }
        /// <summary>
        /// get/set property Phase Offset of waveform in degress -360 to 360 allowed
        /// </summary>
        public override float PhaseOffset_degrees
        {
           
            get
            {
                float Phs = Convert.ToSingle(InstQuery("SOUR" + theSlot + ":PHAS:ADJ?"));
                // return in radians so convert to degrees
                Phs = Convert.ToSingle (Math.Round((Phs * 360) / (2 * Math.PI)));
                
                return Phs;
            }
            set
            {
       
                float Phs = Convert.ToSingle(value);
                InstWrite("SOUR" + theSlot + ":PHAS:ADJ " + Phs.ToString() + "DEG");
            }
        }
        #endregion
        #region Private methods
        /// <summary>
        /// InstWrite encapsulates the visa write allowing external OPC operation complete, need because this returns 11 instead of 1 sometimes
        /// </summary>
        /// <param name="Command"></param>
        private void InstWrite(string Command)
        {
            instrumentChassis.Write_Unchecked( Command, this);
            WaitComplete(5);
        }
        private void InstWrite(string Command,int TimeOutSeconds)
        {
            instrumentChassis.Write_Unchecked(Command, this);
            WaitComplete(TimeOutSeconds);
        }
        /// <summary>
        /// InstQuery encapsulates the visa write allowing external OPC operation complete, need because this returns 11 instead of 1 sometimes
        /// </summary>
        /// <param name="Question"></param>
        /// <returns></returns>
        private string InstQuery(string Question)
        {
            string resp = instrumentChassis.Query_Unchecked( Question , this);
            WaitComplete(2);
            return resp;
        }
        private void WaitComplete(double timeout)
        {
            string resp;
            DateTime StartTime;
            TimeSpan duration;
            StartTime = DateTime.Now;
            instrumentChassis.Timeout_ms = (int)timeout * 1000;  // use for blocking time out too
            do
            {
                resp = instrumentChassis.Query_Unchecked("*OPC?",this);
                duration = DateTime.Now - StartTime;
            } while ((resp[0] == '0') && (duration.TotalSeconds < timeout));  // polling time out test
        }
        #endregion
        #region Private data
        private Chassis_TekAFG3252 instrumentChassis;
        private string theSlot;
        #endregion
    }
}

// [Copyright]
//
// Bookham Chassis_TekAFG3252
// Bookham.TestSolution.Chassis
//
// Chassis1.cs
//
// Author: Feifeng.Zhang, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// Tektronix AFG3252 function generator Chassis
    /// </summary> 
    public class Chassis_TekAFG3252 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_TekAFG3252(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord AFG3252_FGData = new ChassisDataRecord(
                "AFG3252",			// hardware name 
                "0.0.1",			// minimum valid firmware version 
                "2.1.0");		// maximum valid firmware version 
            ValidHardwareData.Add("AFG3252", AFG3252_FGData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string hard = Query("*IDN?", null);
                string[] info = hard.Split(':');
                return info[2].ToString();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string hard = Query("*IDN?", null);
                string[] info = hard.Split(',');
                return info[1].ToString();
            }
        }
        /// <summary>
        /// Get the error string
        /// </summary>
        /// <returns></returns>
        public override string GetErrorString()
        {
            return base.GetErrorString();
        }
        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    ////this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    ////this.Write("*CLS", null);                    
                }
            }
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_Ar9710.cs
//
// Author: HEIKO.FETTIG, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
   /// <summary>
   /// Anritsu MS9710B/C OSA Instrument Driver
   /// </summary>
   public class Inst_Ar9710_OSA : InstType_OSA
   {
      #region Constructor
      /// <summary>
      /// Instrument Constructor.
      /// </summary>
      /// <param name="instrumentNameInit">Instrument name</param>
      /// <param name="driverNameInit">Instrument driver name</param>
      /// <param name="slotInit">Slot ID for the instrument</param>
      /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
      /// <param name="chassisInit">Chassis through which the instrument communicates</param>
      public Inst_Ar9710_OSA(string instrumentNameInit, string driverNameInit,
          string slotInit, string subSlotInit, Chassis chassisInit)
         : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
          chassisInit)
      {
         // Setup expected valid hardware variants 
         InstrumentDataRecord ar9710BData = new InstrumentDataRecord(
             "ANRITSU MS9710B",			// hardware name 
             "0",			// minimum valid firmware version 
             "V3.5&V3.1");		// maximum valid firmware version 
         ValidHardwareData.Add("AR9710B", ar9710BData);
         InstrumentDataRecord ar9710CData = new InstrumentDataRecord(
             "ANRITSU MS9710C",			// hardware name 
             "0",			// minimum valid firmware version 
             "V3.71&V3.3");		// maximum valid firmware version 
         ValidHardwareData.Add("AR9710C", ar9710CData);

         // Configure valid chassis driver information
         InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
             "Chassis_Ar9710",								// chassis driver name  
             "0.0.0.0",									// minimum valid chassis driver version  
             "2.0.0.0");									// maximum valid chassis driver version
         ValidChassisDrivers.Add("MyChassis", chassisInfo);

         // initialise this instrument's chassis
         this.instrumentChassis = (Chassis_Ar9710) chassisInit;
      }
      #endregion

      #region Instrument overrides

      /// <summary>
      /// Firmware version of this instrument.
      /// </summary>
      public override string FirmwareVersion
      {
         get
         {
            // Read the chassis ID string
            string idn = instrumentChassis.Query("*IDN?", null);

            // Return the firmware version in the 4th comma seperated field
            string fwv = idn.Split(',')[3].Trim();

            // Log event 
            LogEvent("'FirmwareVersion' returned : " + fwv);

            return fwv;
         }
      }

      /// <summary>
      /// Hardware Identity of this instrument.
      /// </summary>
      public override string HardwareIdentity
      {
         get
         {
            // Read the chassis ID string and split the comma seperated fields
            string[] idn = instrumentChassis.Query("*IDN?", null).Split(',');

            // Return field1, the manufacturer name and field 2, the model number
            return idn[0] + " " + idn[1];
         }
      }

      /// <summary>
      /// Set instrument to default state
      /// </summary>
      /// <remarks>
      /// Resets the instrument, sets up sweep range 1525-1565nm and starts single sweep.
      /// </remarks>
      public override void SetDefaultState()
      {
         // Send reset command to instrument
         this.Reset();
         // set scan area to C-Band
         this.WavelengthStart_nm = 1525;
         this.WavelengthStop_nm = 1565;
         // start single sweep
         this.SweepMode = SweepModes.Single;
      }

      /// <summary>
      /// Setup the instrument as it goes online
      /// </summary>
      /// <remarks>If set to true, sets the instrument into the default state.</remarks>
      public override bool IsOnline
      {
         get
         {
            return base.IsOnline;
         }
         set
         {
            // setup base class
            base.IsOnline = value;

            if (value) // if setting online                
            {
               this.SetDefaultState();
            }
         }
      }
      #endregion

      #region OSA InstrumentType property overrides
      /// <summary>
      /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
      /// </summary>
      /// <value>
      /// Set value has to be between 0.1 and 10 db/Div.
      /// </value>
      public override double Amplitude_dBperDiv
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("LOG?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 0.1 || value > 10)
            {
               throw new InstrumentException("Amplitude scale must be be between 0.1 and 10 dB/Div. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("LOG " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the state of the internal Attenuator. 
      /// </summary>
      public override bool Attenuator
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("ATT?", this);
            // Convert response to bool and return
            return (response.Trim() == "ON");
         }
         set
         {
            // Write the value to the instrument
            instrumentChassis.Write("ATT " + (value ? "ON" : "OFF"), this);
         }
      }

      /// <summary>
      /// This property is not available on this instrument.
      /// </summary>
      public override bool AutoRange
      {
         get
         {
            throw new InstrumentException("'AutoRange' is not available.");
         }
         set
         {
            throw new InstrumentException("'AutoRange' is not available.");
         }
      }

      /// <summary>
      /// Returns the display trace, as an array of numeric values. 
      /// </summary>
      public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
      {
         get
         {
            // read amplitude values from OSA   
            byte[] response = instrumentChassis.QueryByteArray("DBA?", this);
            // convert byte array response into amplitude array
            int numPoints = this.TracePoints;
            InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
            double waveLength = this.WavelengthStart_nm;
            double waveLengthStep = this.WavelengthSpan_nm / (numPoints - 1);

            for (int index = 0; index < numPoints; index++)
            {
               // BitConverter uses little endian, we have big endian, so we have to swap the two bytes
               byte[] ampByte = new byte[2] { response[2 * index + 1], response[2 * index] };
               short ampInt = BitConverter.ToInt16(ampByte, 0);

               // values are transferred as 16bit integers and 100 * float value
               trace[index].power_dB = (double) ampInt * 0.01;
               trace[index].wavelength_nm = Math.Round(waveLength, 2);
               waveLength += waveLengthStep;
            }

            return trace;
         }
      }

      /// <summary>
      /// Reads the present marker amplitude for the OSA channel.
      /// </summary>
      /// <remarks>
      /// Throws an exception if the instrument does not return the level in dBm.
      /// </remarks>
      /// <seealso cref="MarkerToPeak"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerToMinimum"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerWavelength_nm"/>
      /// <seealso cref="MarkerOn"/>
      public override double MarkerAmplitude_dBm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("TMK?", this);
            markerOn = true;  // remember that marker is now on
            // extract amplitude from response (wavelength, amplitude)
            string amplitude = response.Split(',')[1];
            // remove and check unit
            string unit = amplitude.Substring(amplitude.Length - 3);
            if (unit != "DBM")
            {
               throw new InstrumentException("Instrument returned amplitude with incorrect unit. Amplitude = "
                                   + amplitude);
            }
            else
            {
               amplitude = amplitude.Substring(0, amplitude.Length - 3);
            }
            // Convert response to double and return
            return Convert.ToDouble(amplitude);
         }
      }

      /// <summary>
      /// Shows/hides the marker for the OSA channel. 
      /// </summary>
      /// <remarks>
      /// Since the instrument does not have way to read whether a marker is active or not,
      /// this is handled using a private variable that is set to true every time the marker
      /// activated and false when all markes are erased.
      /// </remarks>
      /// <seealso cref="MarkerToPeak"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerToMinimum"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerWavelength_nm"/>
      /// <seealso cref="MarkerAmplitude_dBm"/>
      public override bool MarkerOn
      {
         get
         {
            // return soft buffer
            return markerOn;
         }
         set
         {
            if (value)
            {
               // Query the instrument to turn on trace marker at last position   
               string response = instrumentChassis.Query("TMK?", this);
               markerOn = true;  // remember that marker is now on
            }
            else
            {
               // Erase all markers
               instrumentChassis.Write("EMK", this);
               markerOn = false;  // remember that marker is now off
            }
         }
      }

      /// <summary>
      /// Reads/sets the marker wavelength for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between the current start and stop wavelengths.
      /// </value>
      /// <seealso cref="MarkerToPeak"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerToMinimum"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerAmplitude_dBm"/>
      /// <seealso cref="MarkerOn"/>
      public override double MarkerWavelength_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("TMK?", this);
            markerOn = true;  // remember that marker is now on
            // extract wavelength from response (wavelength, wavelength)
            string wavelength = response.Split(',')[0];
            // Convert response to double and return
            return Convert.ToDouble(wavelength);
         }
         set
         {
            // Check whether value is in correct range
            if (value < this.WavelengthStart_nm || value > this.WavelengthStop_nm)
            {
               throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                    " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("TMK " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the point averaging for the OSA channel. 
      /// </summary>
      /// <value>
      /// The set value has to be between 2 and 1000 or 0.
      /// </value>
      public override int PointAverage
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("AVT?", this);
            // check if OFF was returned
            if (response == "OFF") response = "0";
            // Convert response to double and return
            return Convert.ToInt32(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 0 || value == 1 || value > 1000)
            {
               throw new InstrumentException("Point Average must be between 2 and 1000 or 0. Set value = "
                                          + value);
            }
            else
            {
               // convert 0 to OFF
               string outValue = Convert.ToString(value);
               if (value == 0) outValue = "OFF";
               // Write the value to the instrument
               instrumentChassis.Write("AVT " + outValue, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the reference level for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between -90 and +30 dBm.
      /// </value>
      public override double ReferenceLevel_dBm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("RLV?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < -90 || value > 30)
            {
               throw new InstrumentException("Reference level must be between -90 and +30 dBm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("RLV " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the resolution bandwidth for the OSA channel.  
      /// </summary>
      /// <value>
      /// If an exact match is not possible the closest value ABOVE that supplied will be used.
      /// Possible set values are: 0.05, 0.07, 0.1, 0.2, 0.5, and 1 nm.
      /// </value>
      public override double ResolutionBandwidth
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("RES?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            double res = 0;
            // Check whether value is in correct range
            if (value <= 0.05) res = 0.05;
            else if (value <= 0.07) res = 0.07;
            else if (value <= 0.1) res = 0.1;
            else if (value <= 0.2) res = 0.2;
            else if (value <= 0.5) res = 0.5;
            else res = 1;
            // Write the value to the instrument
            instrumentChassis.Write("RES " + res, this);
         }
      }

      /// <summary>
      /// Returns the executional status for the OSA channel. 
      /// </summary>
      /// <value>
      /// Busy is returned when the OSA is in the process of measureing a spectrum.
      /// Idle is returned when no spectrum is measured. Unspecified is returned when OSA is in power
      /// monitor mode. DataReady is not supported.
      /// </value>
      /// <seealso cref="SweepMode"/>
      /// <seealso cref="SweepStart"/>
      /// <seealso cref="SweepStop"/>
      public override InstType_OSA.ChannelState Status
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("MOD?", this);
            // Convert response and return
            InstType_OSA.ChannelState status = ChannelState.Unspecified;
            switch (Convert.ToInt32(response))
            {
               case 0: status = ChannelState.Idle;         // Not sweeping
                  break;
               case 1:                                      // Single sweep in progress
               case 2: status = ChannelState.Busy;         // Continuous sweeo in progress
                  break;
               case 3: status = ChannelState.Unspecified;  // Power monitor
                  break;
            }
            return status;
         }
      }

      /// <summary>
      /// Reads/sets the sweep capture mode for the OSA channel.
      /// </summary>
      /// <value>
      /// Triggered mode is not available.
      /// Unspecified mode cannot be set.
      /// </value>
      /// <seealso cref="Status"/>
      /// <seealso cref="SweepStart"/>
      /// <seealso cref="SweepStop"/>
      public override InstType_OSA.SweepModes SweepMode
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("MOD?", this);
            // Convert response and return
            InstType_OSA.SweepModes mode = SweepModes.Unspecified;
            switch (Convert.ToInt32(response))
            {
               case 0:                                   // Not sweeping
               case 1: mode = SweepModes.Single;        // Single sweep in progress
                  break;
               case 2: mode = SweepModes.Continuous;    // Continuous sweeo in progress
                  break;
               case 3: mode = SweepModes.Unspecified;   // Power monitor
                  break;
            }
            return mode;
         }
         set
         {
            // Select command
            string command = "";
            switch (value)
            {
               case SweepModes.Single: command = "SSI";  // Start single sweep
                  break;
               case SweepModes.Continuous: command = "SRT";  // Start continuous sweep
                  break;
               case SweepModes.Unspecified: throw new InstrumentException("Cannot set to 'Unspecified' mode.");
               case SweepModes.Triggered: throw new InstrumentException("'Triggered' mode not available.");
            }
            // Write the command to the instrument
            instrumentChassis.Write(command, this);
         }
      }

      /// <summary>
      /// Reads/sets the number of trace points for the OSA channel.
      /// </summary>
      /// <value>
      /// If an exact match is not possible the closest value ABOVE that supplied will be used.
      /// Possible set values are: 51, 101, 201, 501, 1001, 2001, and 5001.
      /// </value>
      public override int TracePoints
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("MPT?", this);
            // Convert response to double and return
            return Convert.ToInt32(response);
         }
         set
         {
            int mpt = 0;

            // Check whether value is in correct range
            if (value <= 51) mpt = 51;
            else if (value <= 101) mpt = 101;
            else if (value <= 251) mpt = 251;
            else if (value <= 501) mpt = 501;
            else if (value <= 1001) mpt = 1001;
            else if (value <= 2001) mpt = 2001;
            else mpt = 5001;

            // Write the value to the instrument
            instrumentChassis.Write("MPT " + mpt, this);
         }
      }

      /// <summary>
      /// Reads/sets the video averaging value for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between 2 and 1000 or 0.
      /// </value>
      public override int VideoAverage
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("AVS?", this);
            // check if OFF was returned
            if (response == "OFF") response = "0";
            // Convert response to double and return
            return Convert.ToInt32(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 0 || value == 1 || value > 1000)
            {
               throw new InstrumentException("Video Average must be between 2 and 1000 or 0. Set value = "
                                          + value);
            }
            else
            {
               // convert 0 to OFF
               string outValue = Convert.ToString(value);
               if (value == 0) outValue = "OFF";
               // Write the value to the instrument
               instrumentChassis.Write("AVS " + outValue, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the video bandwidth for the OSA channel.
      /// </summary>
      /// <value>
      /// If an exact match is not possible the closest value ABOVE that supplied will be used.
      /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.
      /// </value>
      public override double VideoBandwidth
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("VBW?", this);
            // extract value
            double vbw = 0;
            if (response.IndexOf("MHZ") >= 0)
               vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e6;
            else if (response.IndexOf("KHZ") >= 0)
               vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e3;
            else if (response.IndexOf("HZ") >= 0)
               vbw = Convert.ToDouble(response.Substring(0, response.Length - 2));

            // Convert response to double and return
            return vbw;
         }
         set
         {
            double vbw = 0;
            // Check whether value is in correct range
            if (value <= 10) vbw = 10;
            else if (value <= 100) vbw = 100;
            else if (value <= 1000) vbw = 1000;
            else if (value <= 10000) vbw = 10000;
            else if (value <= 100000) vbw = 100000;
            else vbw = 1000000;
            // Write the value to the instrument
            instrumentChassis.Write("VBW " + vbw, this);
         }
      }

      /// <summary>
      /// Reads/sets the centre wavelength for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between 600 and 1750nm.
      /// </value>
      public override double WavelengthCentre_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("CNT?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 600 || value > 1750)
            {
               throw new InstrumentException("Wavelength Centre must be between 600 and 1750 nm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("CNT " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the wavelength offset for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between -1 and +1nm.
      /// </value>
      public override double WavelengthOffset_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("WOFS?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < -1 || value > 1)
            {
               throw new InstrumentException("Wavelength Offset must be between -1.0 and +1.0 nm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("WOFS " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the wavelength span for the OSA channel.
      /// </summary>
      /// <value>
      /// The set value has to be between 0.2 and 1200nm.
      /// </value>
      public override double WavelengthSpan_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("SPN?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 0.2 || value > 1200)
            {
               throw new InstrumentException("Wavelength Span must be between 0.2 and 1200 nm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("SPN " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
      /// </summary>
      /// <value>
      /// The set value has to be between 600 and 1750nm.
      /// </value>
      public override double WavelengthStart_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("STA?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 600 || value > 1750)
            {
               throw new InstrumentException("Wavelength Start must be between 600 and 1750 nm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("STA " + value, this);
            }
         }
      }

      /// <summary>
      /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
      /// </summary>
      /// <value>
      /// The set value has to be between 600 and 1800nm.
      /// </value>
      public override double WavelengthStop_nm
      {
         get
         {
            // Query the instrument   
            string response = instrumentChassis.Query("STO?", this);
            // Convert response to double and return
            return Convert.ToDouble(response);
         }
         set
         {
            // Check whether value is in correct range
            if (value < 600 || value > 1800)
            {
               throw new InstrumentException("Wavelength Stop must be between 600 and 1800 nm. Set value = "
                                          + value);
            }
            else
            {
               // Write the value to the instrument
               instrumentChassis.Write("STO " + value, this);
            }
         }
      }

      #endregion

      #region OSA InstrumentType function overrides
        /// <summary>
        /// Autoalign OSA.
        /// </summary>
        public override void AutoAlign()
        {
           throw new InstrumentException("The method or operation is not implemented.");
        }

      /// <summary>
      /// Moves the marker to the minimum signal point on the display. 
      /// </summary>
      /// <seealso cref="MarkerToPeak"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerWavelength_nm"/>
      /// <seealso cref="MarkerAmplitude_dBm"/>
      /// <seealso cref="MarkerOn"/>
      public override void MarkerToMinimum()
      {
         // Retrieve the trace
         InstType_OSA.OptPowerPoint[] trace = this.GetDisplayTrace;
         // find min in trace
         InstType_OSA.OptPowerPoint min = trace[0];
         for (int index = 1; index < trace.Length; index++)
         {
            if (trace[index].power_dB < min.power_dB)
               min = trace[index];
         }
         // set marker to minimum
         this.MarkerWavelength_nm = min.wavelength_nm;
      }

      /// <summary>
      /// Moves the marker to the next peak to the left/right on the display.
      /// </summary>
      /// <param name="directionToMove">
      /// Enumeration representing the direction in which to search for the next peak.
      /// If unspecified is selected, the next peak is searched.
      /// </param>
      /// <remarks>
      /// Throws an exception if the peak search is unsuccessful.
      /// </remarks>
      /// <seealso cref="MarkerToPeak"/>
      /// <seealso cref="MarkerToMinimum"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerWavelength_nm"/>
      /// <seealso cref="MarkerAmplitude_dBm"/>
      /// <seealso cref="MarkerOn"/>
      public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
      {
         string direction = "";

         switch (directionToMove)
         {
            case Direction.Left: direction = "LEFT";
               break;
            case Direction.Right: direction = "RIGHT";
               break;
            case Direction.Unspecified: direction = "NEXT";
               break;
         }

         // Move the marker
         instrumentChassis.Write_Unchecked("PKS " + direction, this);
         markerOn = true;  // remember that marker is now on
         this.WaitForEsrBitSet(2, 0);
         // check for measurement error
         string response = instrumentChassis.Query_Unchecked("ESR3?", this);
         if ((Convert.ToInt32(response) & (1 << 1)) == (1 << 1))
         {
            // remove error from queue
            response = instrumentChassis.Query_Unchecked("ERR?", this);
            // clear registers
            instrumentChassis.Write_Unchecked("*CLS", this);
            throw new InstrumentException("Peak not found.");
         }
      }

      /// <summary>
      /// Moves the marker to the maximum signal point on the display.
      /// </summary>
      /// <remarks>
      /// Throws anexception if the peak search is unsuccessful.
      /// </remarks>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerToMinimum"/>
      /// <seealso cref="MarkerToNextPeak"/>
      /// <seealso cref="MarkerWavelength_nm"/>
      /// <seealso cref="MarkerAmplitude_dBm"/>
      /// <seealso cref="MarkerOn"/>
      public override void MarkerToPeak()
      {
         // Move the marker
         instrumentChassis.Write_Unchecked("PKS PEAK", this);
         markerOn = true;  // remember that marker is now on
         this.WaitForEsrBitSet(2, 0);
         // check for measurement error
         string response = instrumentChassis.Query_Unchecked("ESR3?", this);
         if ((Convert.ToInt32(response) & (1 << 1)) == (1 << 1))
         {
            // remove error from queue
            response = instrumentChassis.Query_Unchecked("ERR?", this);
            // clear registers
            instrumentChassis.Write_Unchecked("*CLS", this);
            throw new InstrumentException("Peak not found.");
         }
      }

      /// <summary>
      /// Starts a measurement sweep, and returns immediately.
      /// </summary>
      /// <remarks>
      /// Status() property should be used to determine sweep progress/completion. 
      /// </remarks>
      /// <seealso cref="Status"/>
      /// <seealso cref="SweepMode"/>
      /// <seealso cref="SweepStop"/>
      public override void SweepStart()
      {
         // Start the sweep
         instrumentChassis.Write("SSI", this);
      }

      /// <summary>
      /// Stops an active measurement sweep. 
      /// </summary>
      /// <seealso cref="Status"/>
      /// <seealso cref="SweepMode"/>
      /// <seealso cref="SweepStart"/>
      public override void SweepStop()
      {
         // Stop the sweep
         instrumentChassis.Write("SST", this);
      }

      #endregion

      #region Instrument specific commands

      /// <summary>
      /// Performs a reset of the instrument and waits for it to finish. 
      /// </summary>
      public void Reset()
      {
         // Start the reset
         instrumentChassis.Write_Unchecked("*RST", this);
         DateTime startTime = DateTime.Now;
         TimeSpan timeOut = new TimeSpan(0, 1, 0); // reset should not take longer than 60 seconds
         // Check if reset is finished
         this.WaitForEsrBitSet(2, 4, timeOut);
         markerOn = false; // remember that marker is now off
      }

      /// <summary>
      /// Waits until the specified bit in the specified ESR register is set.
      /// Times out after 60s.
      /// </summary>
      /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
      /// <param name="esrBit">Number of the bit to be checked.</param>
      private void WaitForEsrBitSet(byte esrRegister, byte esrBit)
      {
         // call WaitForEsrBitSet with default timeOut of 60s
         TimeSpan timeOut = new TimeSpan(0, 1, 0);
         this.WaitForEsrBitSet(esrRegister, esrBit, timeOut);
      }

      /// <summary>
      /// Waits until the specified bit in the specified ESR register is set.
      /// </summary>
      /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
      /// <param name="esrBit">Number of the bit to be checked.</param>
      /// <param name="timeOut">TimeSpan after which the function times out and throws an exception.</param>
      private void WaitForEsrBitSet(byte esrRegister, byte esrBit, TimeSpan timeOut)
      {
         if (esrRegister < 1 || esrRegister > 3)
            throw new InstrumentException("Number of ESR Register has to be between 1 and 3. ESR# = " + esrRegister);
         else if (esrBit < 0 || esrRegister > 7)
            throw new InstrumentException("Number of Bit to be checked has to be between 0 and 7. Bit# = " + esrBit);
         else
         {
            DateTime startTime = DateTime.Now;
            Boolean bitSet = false;
            string response = "";
            // Check if bit is set
            while (DateTime.Now.Subtract(startTime) < timeOut && !bitSet)
            {
               try
               {
                  // check if bit is set
                  response = instrumentChassis.Query_Unchecked("ESR" + esrRegister + "?", this);
                  bitSet = ((Convert.ToInt32(response) & (1 << esrBit)) != (1 << esrBit));
               }
               catch (Bookham.TestEngine.PluginInterfaces.Chassis.ChassisException)
               {
                  // Caught ChassisException
                  // Communication mit chassis interrupted. This happens during reset.
               }
               System.Threading.Thread.Sleep(100);
            }
            // check if bitSet or timed out
            if (!bitSet)
               throw new InstrumentException("ESR Bit Check timed out.");
         }
      }

      #endregion

      #region Private data
      /// <summary>
      /// Instrument's chassis
      /// </summary>
      private Chassis_Ar9710 instrumentChassis;
      /// <summary>
      /// Soft marker on/off buffer
      /// </summary>
      private Boolean markerOn;
      #endregion

      
  }
}

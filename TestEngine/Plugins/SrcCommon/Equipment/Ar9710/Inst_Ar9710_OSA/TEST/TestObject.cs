using System;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;

namespace Equip_Test_InstAr9710
{
   /// <exclude />	
   [TestFixture]
   public class Test_Object
   {
      #region References to the objects to test - Chassis and Instruments
      private Chassis_Ar9710 testChassis;
      private Inst_Ar9710_OSA testOSA;
      #endregion


      #region Constants for use during test.
      // VISA Chassis for where to find the instrument
      const string visaResource = "GPIB0::1::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
      const string chassisName = "Chassis";
      const string osaName = "Instrument";
      #endregion

      /// <exclude />
      [TestFixtureSetUp]
      public void Setup()
      {
         // initialise Test Engine logging domain
         Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
         Bookham.TestEngine.Framework.Logging.Initializer.Init();
         Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
         TestOutput("*** Test Harness Initialising ***");

         // create chassis objects
         testChassis = new Chassis_Ar9710(chassisName, "Chassis_Ar9710", visaResource);
         TestOutput(chassisName, "Created OK");

         // create instrument objects            
         testOSA = new Inst_Ar9710_OSA(osaName, "Inst_Ar9710", "", "", testChassis);
         TestOutput(osaName, "Created OK");

         // put them online
         testChassis.IsOnline = true;
         TestOutput(chassisName, "IsOnline set true OK");
         testOSA.IsOnline = true;
         TestOutput(osaName, "IsOnline set true OK");
      }

      /// <exclude />
      [TestFixtureTearDown]
      public void ShutDown()
      {
         testOSA.IsOnline = false;
         testChassis.IsOnline = false;

         // Test end
         TestOutput("Test Finished!");
      }

      /// <exclude />
      [Test]
      public void T01_Versions()
      {
         TestOutput("\n\n*** T01_Versions ***");
         // Check all the version properties
         TestOutput("Chassis Hardware ID = " + testChassis.HardwareIdentity);
         TestOutput("Chassis Firmware Version = " + testChassis.FirmwareVersion);
         TestOutput("Instrument Hardware ID = " + testOSA.HardwareIdentity);
         TestOutput("Instrument Firmware Version = " + testOSA.FirmwareVersion);
      }

      /// <exclude />
      [Test]
      public void T02_Check_Defaults()
      {
         TestOutput("\n\n*** T02_Check_Defaults***");
         // Check the wavelength start
         TestOutput("Reading Wavelength Start [nm] = " + testOSA.WavelengthStart_nm);
         Assert.AreEqual(1525, testOSA.WavelengthStart_nm);
         // Check the wavelength stop
         TestOutput("Reading Wavelength Stop [nm] = " + testOSA.WavelengthStop_nm);
         Assert.AreEqual(1565, testOSA.WavelengthStop_nm);
      }

      /// <exclude />
      [Test]
      public void T03_Amplitude_dBperDiv()
      {
         TestOutput("\n\n*** T03_Amplitude_dBperDiv***");
         // Check the get property
         TestOutput("Reading Amplitude Scale [dB/Div] = " + testOSA.Amplitude_dBperDiv);
         // Check the set property
         TestOutput("Setting Amplitude Scale to 5dB/Div]");
         testOSA.Amplitude_dBperDiv = 5;
         // Check the get property
         TestOutput("Reading Amplitude Scale [dB/Div] = " + testOSA.Amplitude_dBperDiv);
         // Check if set correctly
         Assert.AreEqual(5, testOSA.Amplitude_dBperDiv);
         try
         {
            testOSA.Amplitude_dBperDiv = 20;
            Assert.Fail("Amplitude Scale should not be settable to 20.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting Amplitude Scale [dB/Div]: " + ex1.Message);
         }
         TestOutput("Setting Amplitude Scale to 10dB/Div]");
         testOSA.Amplitude_dBperDiv = 10;
         // Check the get property
         TestOutput("Reading Amplitude Scale [dB/Div] = " + testOSA.Amplitude_dBperDiv);
         // Check if set correctly
         Assert.AreEqual(10, testOSA.Amplitude_dBperDiv);
      }

      /// <exclude />
      [Test]
      public void T04_AutoRange()
      {
         TestOutput("\n\n*** T04_AutoRange***");
         Boolean autoRange = false;
         // Check the get property
         try
         {
            autoRange = testOSA.AutoRange;
            TestOutput("Reading AutoRange property = " + autoRange);
            Assert.Fail("AutoRange should always cause error.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error reading AutoRange property: " + ex1.Message);
         }
         // Check the set property
         TestOutput("Setting AutoRange property to false");
         try
         {
            testOSA.AutoRange = true;
            Assert.Fail("AutoRange should always cause error.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting AutoRange property: " + ex1.Message);
         }
      }

      /// <exclude />
      [Test]
      public void T05_GetDisplayTrace()
      {
         TestOutput("\n\n*** T05_GetDisplayTrace ***");
         // set to 500 points
         TestOutput("Setting number of trace points to 500 ...");
         testOSA.TracePoints = 500;
         TestOutput("Number of trace points = " + testOSA.TracePoints);
         Assert.AreEqual(501, testOSA.TracePoints);
         // run an acquisition
         testOSA.SweepStart();
         TestOutput("Starting acquisition");
         while (testOSA.Status == InstType_OSA.ChannelState.Busy)
            System.Threading.Thread.Sleep(500);
         TestOutput("Acquisition finished OK");
         // Retrieve a demo trace
         InstType_OSA.OptPowerPoint[] result = testOSA.GetDisplayTrace;
         TestOutput("Demo Trace retrieved OK");
         TestOutput(string.Format("Point {2,4}: {0,10} nm, {1,10} dB", result[0].wavelength_nm,
                                                                           result[0].power_dB, 1));
         TestOutput("...");
         TestOutput(string.Format("Point {2,4}: {0,10} nm, {1,10} dB", result[result.Length - 1].wavelength_nm,
                                                   result[result.Length - 1].power_dB, result.Length));
         // checked if correct number of points
         Assert.AreEqual(501, result.Length);
         // set number of points to 2000
         TestOutput("Setting number of trace points to 2000 ...");
         testOSA.TracePoints = 2000;
         TestOutput("Number of trace points = " + testOSA.TracePoints);
         Assert.AreEqual(2001, testOSA.TracePoints);
         // run an acquisition
         testOSA.SweepStart();
         TestOutput("Starting acquisition");
         while (testOSA.Status == InstType_OSA.ChannelState.Busy)
            System.Threading.Thread.Sleep(500);
         TestOutput("Acquisition finished OK");
         // Retrieve the trace
         result = testOSA.GetDisplayTrace;
         TestOutput("Demo Trace retrieved OK");
         TestOutput(string.Format("Point {2,4}: {0,10} nm, {1,10} dB", result[0].wavelength_nm,
                                                                           result[0].power_dB, 1));
         TestOutput("...");
         TestOutput(string.Format("Point {2,4}: {0,10} nm, {1,10} dB", result[result.Length - 1].wavelength_nm,
                                                   result[result.Length - 1].power_dB, result.Length));
         Assert.AreEqual(2001, result.Length);
         // reset to 500 points
         TestOutput("Setting number of trace points to 500 ...");
         testOSA.TracePoints = 500;
         TestOutput("Number of trace points = " + testOSA.TracePoints);
         Assert.AreEqual(501, testOSA.TracePoints);
      }

      /// <exclude />
      [Test]
      public void T06_TestMarkers()
      {
         TestOutput("\n\n*** T06_TestMarkers ***");
         // run an acquisition
         testOSA.SweepStart();
         TestOutput("Starting acquisition");
         while (testOSA.Status == InstType_OSA.ChannelState.Busy)
            System.Threading.Thread.Sleep(500);
         TestOutput("Acquisition finished OK");
         // Retrieve the trace
         InstType_OSA.OptPowerPoint[] trace = testOSA.GetDisplayTrace;
         TestOutput("Demo Trace retrieved OK");
         // find min, max in trace
         InstType_OSA.OptPowerPoint min = trace[0];
         InstType_OSA.OptPowerPoint max = trace[0];
         for (int index = 1; index < trace.Length; index++)
         {
            if (trace[index].power_dB < min.power_dB)
               min = trace[index];
            if (trace[index].power_dB > max.power_dB)
               max = trace[index];
         }
         TestOutput("Minimum = " + min.power_dB + "dB @ " + min.wavelength_nm + "nm");
         TestOutput("Maximum = " + max.power_dB + "dB @ " + max.wavelength_nm + "nm");
         // perform peak search
         TestOutput("Performing Peak Search ...");
         try
         {
            testOSA.MarkerToPeak();
            TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
            Assert.IsTrue(testOSA.MarkerOn);
            TestOutput("Peak = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
            Assert.AreEqual(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(max.power_dB, 2));
            Assert.AreEqual(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(max.wavelength_nm, 2));
         }
         catch
         {
            TestOutput("No Peak found");
         }
         // perform next peak search left
         TestOutput("Performing Next Peak Left Search ...");
         try
         {
            testOSA.MarkerToPeak();
            testOSA.MarkerToNextPeak(InstType_OSA.Direction.Left);
            TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
            Assert.IsTrue(testOSA.MarkerOn);
            TestOutput("Peak = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
            Assert.Less(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(max.power_dB, 2));
            Assert.Less(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(max.wavelength_nm, 2));
         }
         catch
         {
            TestOutput("No Peak found");
         }
         // perform next peak search right
         TestOutput("Performing Next Peak Right Search ...");
         try
         {
            testOSA.MarkerToPeak();
            testOSA.MarkerToNextPeak(InstType_OSA.Direction.Right);
            TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
            Assert.IsTrue(testOSA.MarkerOn);
            TestOutput("Peak = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
            Assert.Less(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(max.power_dB, 2));
            Assert.Greater(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(max.wavelength_nm, 2));
         }
         catch
         {
            TestOutput("No Peak found");
         }
         // perform next peak search
         TestOutput("Performing Next Peak Search ...");
         try
         {
            testOSA.MarkerToPeak();
            testOSA.MarkerToNextPeak(InstType_OSA.Direction.Unspecified);
            TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
            Assert.IsTrue(testOSA.MarkerOn);
            TestOutput("Peak = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
            Assert.Less(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(max.power_dB, 2));
            Assert.AreNotEqual(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(max.wavelength_nm, 2));
         }
         catch
         {
            TestOutput("No Peak found");
         }
         // perform minimum search
         TestOutput("Performing Minimum Search ...");
         testOSA.MarkerToMinimum();
         TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
         Assert.IsTrue(testOSA.MarkerOn);
         TestOutput("Minimum = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
         Assert.AreEqual(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(min.power_dB, 2));
         Assert.AreEqual(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(min.wavelength_nm, 2));
         // set marker to maximum
         TestOutput("Setting Marker to " + max.wavelength_nm + "nm");
         testOSA.MarkerWavelength_nm = max.wavelength_nm;
         TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
         Assert.IsTrue(testOSA.MarkerOn);
         TestOutput("Marker location = " + testOSA.MarkerAmplitude_dBm + "dB @ " + testOSA.MarkerWavelength_nm + "nm");
         Assert.AreEqual(Math.Round(testOSA.MarkerAmplitude_dBm, 2), Math.Round(max.power_dB, 2));
         Assert.AreEqual(Math.Round(testOSA.MarkerWavelength_nm, 2), Math.Round(max.wavelength_nm, 2));
         // turn marker off
         TestOutput("Turning Marker off ...");
         testOSA.MarkerOn = false;
         TestOutput("Marker is " + (testOSA.MarkerOn ? "on." : "off."));
         Assert.IsFalse(testOSA.MarkerOn);
      }

      /// <exclude />
      [Test]
      public void T07_TestSweepModes()
      {
         TestOutput("\n\n*** T07_TestSweepModes ***");
         // check current state
         TestOutput("Current Sweep Mode = " + testOSA.SweepMode.ToString());
         TestOutput("Current Sweep Status = " + testOSA.Status.ToString());
         // set continuous mode, start acquisition, check status, stop acquisition, check status
         TestOutput("Set mode to continuous sweep ...");
         testOSA.SweepMode = InstType_OSA.SweepModes.Continuous;
         TestOutput("Current Sweep Mode = " + testOSA.SweepMode.ToString());
         Assert.AreEqual(InstType_OSA.SweepModes.Continuous, testOSA.SweepMode);
         TestOutput("Starting acquisition ...");
         testOSA.SweepStart();
         TestOutput("Current Sweep Status = " + testOSA.Status.ToString());
         Assert.AreEqual(InstType_OSA.ChannelState.Busy, testOSA.Status);
         TestOutput("Stopping acquisition ...");
         testOSA.SweepStop();
         TestOutput("Current Sweep Status = " + testOSA.Status.ToString());
         Assert.AreEqual(InstType_OSA.ChannelState.Idle, testOSA.Status);
         // set single mode, start acquisition, check status, stop acquisition, check status
         TestOutput("Set mode to single sweep ...");
         testOSA.SweepMode = InstType_OSA.SweepModes.Single;
         TestOutput("Current Sweep Mode = " + testOSA.SweepMode.ToString());
         Assert.AreEqual(InstType_OSA.SweepModes.Single, testOSA.SweepMode);
         TestOutput("Starting acquisition ...");
         testOSA.SweepStart();
         TestOutput("Current Sweep Status = " + testOSA.Status.ToString());
         Assert.AreEqual(InstType_OSA.ChannelState.Busy, testOSA.Status);
         TestOutput("Stopping acquisition ...");
         testOSA.SweepStop();
         TestOutput("Current Sweep Status = " + testOSA.Status.ToString());
         Assert.AreEqual(InstType_OSA.ChannelState.Idle, testOSA.Status);
         // try to set triggered mode
         TestOutput("Set mode to triggerd sweep ...");
         try
         {
            testOSA.SweepMode = InstType_OSA.SweepModes.Triggered;
            Assert.Fail("Triggered sweep mode should not be available.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting sweep mode: " + ex1.Message);
         }
         // try to set unspecified mode
         TestOutput("Set mode to unspecified sweep ...");
         try
         {
            testOSA.SweepMode = InstType_OSA.SweepModes.Unspecified;
            Assert.Fail("Unspecified sweep mode should not be available.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting sweep mode: " + ex1.Message);
         }
      }

      /// <exclude />
      [Test]
      public void T08_TestWavelengthOffset()
      {
         TestOutput("\n\n*** T08_TestWavelegthOffset ***");
         // check current settings
         TestOutput("Wavelength Offset = " + testOSA.WavelengthOffset_nm + "nm");
         // change offset to 1nm
         TestOutput("Setting Wavelength Offset to 1nm ...");
         testOSA.WavelengthOffset_nm = 1;
         TestOutput("Wavelength Offset = " + testOSA.WavelengthOffset_nm + "nm");
         Assert.AreEqual(1, testOSA.WavelengthOffset_nm);
         // change offset to 0nm
         TestOutput("Setting Wavelength Offset to 0nm ...");
         testOSA.WavelengthOffset_nm = 0;
         TestOutput("Wavelength Offset = " + testOSA.WavelengthOffset_nm + "nm");
         Assert.AreEqual(0, testOSA.WavelengthOffset_nm);
         // run an acquisition
         testOSA.SweepStart();
         TestOutput("Starting acquisition");
         while (testOSA.Status == InstType_OSA.ChannelState.Busy)
            System.Threading.Thread.Sleep(500);
         TestOutput("Acquisition finished OK");
      }
 
      /// <exclude />
      [Test]
      public void T09_TestSweepBoundaries()
      {
         TestOutput("\n\n*** T09_TestSweepBoundaries ***");
         // set to defaults
         testOSA.SetDefaultState();
         // check current settings
         TestOutput("Wavelength Start = " + testOSA.WavelengthStart_nm + "nm");
         TestOutput("Wavelength Stop = " + testOSA.WavelengthStop_nm + "nm");
         TestOutput("Wavelength Centre = " + testOSA.WavelengthCentre_nm + "nm");
         TestOutput("Wavelength Span = " + testOSA.WavelengthSpan_nm + "nm");
         Assert.AreEqual(1545, testOSA.WavelengthCentre_nm);
         Assert.AreEqual(1525, testOSA.WavelengthStart_nm);
         Assert.AreEqual(1565, testOSA.WavelengthStop_nm);
         Assert.AreEqual(40, testOSA.WavelengthSpan_nm);
         // change centre wavelength
         TestOutput("Setting Wavelength Centre to 1550nm ...");
         testOSA.WavelengthCentre_nm = 1550;
         TestOutput("Wavelength Start = " + testOSA.WavelengthStart_nm + "nm");
         TestOutput("Wavelength Stop = " + testOSA.WavelengthStop_nm + "nm");
         TestOutput("Wavelength Centre = " + testOSA.WavelengthCentre_nm + "nm");
         TestOutput("Wavelength Span = " + testOSA.WavelengthSpan_nm + "nm");
         Assert.AreEqual(1550, testOSA.WavelengthCentre_nm);
         Assert.AreEqual(1530, testOSA.WavelengthStart_nm);
         Assert.AreEqual(1570, testOSA.WavelengthStop_nm);
         Assert.AreEqual(40, testOSA.WavelengthSpan_nm);
         // change wavelength span
         TestOutput("Setting Wavelength Span to 60nm ...");
         testOSA.WavelengthSpan_nm = 60;
         TestOutput("Wavelength Start = " + testOSA.WavelengthStart_nm + "nm");
         TestOutput("Wavelength Stop = " + testOSA.WavelengthStop_nm + "nm");
         TestOutput("Wavelength Centre = " + testOSA.WavelengthCentre_nm + "nm");
         TestOutput("Wavelength Span = " + testOSA.WavelengthSpan_nm + "nm");
         Assert.AreEqual(1550, testOSA.WavelengthCentre_nm);
         Assert.AreEqual(1520, testOSA.WavelengthStart_nm);
         Assert.AreEqual(1580, testOSA.WavelengthStop_nm);
         Assert.AreEqual(60, testOSA.WavelengthSpan_nm);
         // change start wavelength
         TestOutput("Setting Wavelength Start to 1525nm ...");
         testOSA.WavelengthStart_nm = 1525;
         TestOutput("Wavelength Start = " + testOSA.WavelengthStart_nm + "nm");
         TestOutput("Wavelength Stop = " + testOSA.WavelengthStop_nm + "nm");
         TestOutput("Wavelength Centre = " + testOSA.WavelengthCentre_nm + "nm");
         TestOutput("Wavelength Span = " + testOSA.WavelengthSpan_nm + "nm");
         Assert.AreEqual(1552.5, testOSA.WavelengthCentre_nm);
         Assert.AreEqual(1525, testOSA.WavelengthStart_nm);
         Assert.AreEqual(1580, testOSA.WavelengthStop_nm);
         Assert.AreEqual(55, testOSA.WavelengthSpan_nm);
         // change stop wavelength
         TestOutput("Setting Wavelength Stop to 1565nm ...");
         testOSA.WavelengthStop_nm = 1565;
         TestOutput("Wavelength Start = " + testOSA.WavelengthStart_nm + "nm");
         TestOutput("Wavelength Stop = " + testOSA.WavelengthStop_nm + "nm");
         TestOutput("Wavelength Centre = " + testOSA.WavelengthCentre_nm + "nm");
         TestOutput("Wavelength Span = " + testOSA.WavelengthSpan_nm + "nm");
         Assert.AreEqual(1545, testOSA.WavelengthCentre_nm);
         Assert.AreEqual(1525, testOSA.WavelengthStart_nm);
         Assert.AreEqual(1565, testOSA.WavelengthStop_nm);
         Assert.AreEqual(40, testOSA.WavelengthSpan_nm);
      }

      /// <exclude />
      [Test]
      public void T10_PointAverage()
      {
         TestOutput("\n\n*** T03_PointAverage***");
         // Check the get property
         TestOutput("Reading Point Average = " + testOSA.PointAverage);
         // Check the set property
         TestOutput("Setting Point Average to 5");
         testOSA.PointAverage = 5;
         // Check the get property
         TestOutput("Reading Point Average = " + testOSA.PointAverage);
         // Check if set correctly
         Assert.AreEqual(5, testOSA.PointAverage);
         TestOutput("Setting Point Average to 1");
         try
         {
            testOSA.PointAverage = 1;
            Assert.Fail("PointAverage should not be settable to 1.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting Point Average: " + ex1.Message);
         }
         TestOutput("Setting Point Average to 0");
         testOSA.PointAverage = 0;
         // Check the get property
         TestOutput("Reading Point Average = " + testOSA.PointAverage);
         // Check if set correctly
         Assert.AreEqual(0, testOSA.PointAverage);
      }

      /// <exclude />
      [Test]
      public void T11_VideoAverage()
      {
         TestOutput("\n\n*** T11_VideoAverage***");
         // Check the get property
         TestOutput("Reading Video Average = " + testOSA.VideoAverage);
         // Check the set property
         TestOutput("Setting Video Average to 5");
         testOSA.VideoAverage = 5;
         // Check the get property
         TestOutput("Reading Video Average = " + testOSA.VideoAverage);
         // Check if set correctly
         Assert.AreEqual(5, testOSA.VideoAverage);
         TestOutput("Setting Video Average to 1");
         try
         {
            testOSA.VideoAverage = 1;
            Assert.Fail("VideoAverage should not be settable to 1.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting Video Average: " + ex1.Message);
         }
         TestOutput("Setting Video Average to 0");
         testOSA.VideoAverage = 0;
         // Check the get property
         TestOutput("Reading Video Average = " + testOSA.VideoAverage);
         // Check if set correctly
         Assert.AreEqual(0, testOSA.VideoAverage);
      }

      /// <exclude />
      [Test]
      public void T12_VideoBandwidth()
      {
         TestOutput("\n\n*** T12_VideoBandwidth***");
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 5");
         testOSA.VideoBandwidth = 5;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(10, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 50");
         testOSA.VideoBandwidth = 50;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(100, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 5000");
         testOSA.VideoBandwidth = 5000;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(10000, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 50000");
         testOSA.VideoBandwidth = 50000;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(100000, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 500000");
         testOSA.VideoBandwidth = 500000;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(1000000, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 5000000");
         testOSA.VideoBandwidth = 5000000;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(1000000, testOSA.VideoBandwidth);
         // Check the set property
         TestOutput("Setting Video Bandwidth to 500");
         testOSA.VideoBandwidth = 500;
         // Check the get property
         TestOutput("Reading Video Bandwidth = " + testOSA.VideoBandwidth);
         // Check if set correctly
         Assert.AreEqual(1000, testOSA.VideoBandwidth);
      }

      /// <exclude />
      [Test]
      public void T13_ResolutionBandwidth()
      {
         TestOutput("\n\n*** T13_ResolutionBandwidth***");
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.04");
         testOSA.ResolutionBandwidth = 0.04;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(0.05, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.06");
         testOSA.ResolutionBandwidth = 0.06;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(0.07, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.08");
         testOSA.ResolutionBandwidth = 0.08;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(0.1, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.15");
         testOSA.ResolutionBandwidth = 0.15;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(0.2, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.25");
         testOSA.ResolutionBandwidth = 0.25;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(0.5, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 0.75");
         testOSA.ResolutionBandwidth = 0.75;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(1, testOSA.ResolutionBandwidth);
         // Check the set property
         TestOutput("Setting Resolution Bandwidth to 1.5");
         testOSA.ResolutionBandwidth = 1.5;
         // Check the get property
         TestOutput("Reading Resolution Bandwidth = " + testOSA.ResolutionBandwidth);
         // Check if set correctly
         Assert.AreEqual(1, testOSA.ResolutionBandwidth);
      }

      /// <exclude />
      [Test]
      public void T14_ReferenceLevel_dBm()
      {
         TestOutput("\n\n*** T14_ReferenceLevel_dBm***");
         // Check the get property
         TestOutput("Reading Reference Level [dB] = " + testOSA.ReferenceLevel_dBm);
         // Check the set property
         TestOutput("Setting Reference Level to -5dB");
         testOSA.ReferenceLevel_dBm = -5;
         // Check the get property
         TestOutput("Reading Reference Level [dB] = " + testOSA.ReferenceLevel_dBm);
         // Check if set correctly
         Assert.AreEqual(-5, testOSA.ReferenceLevel_dBm);
         TestOutput("Setting Reference Level [dB] to 300");
         try
         {
            testOSA.ReferenceLevel_dBm = 300;
            Assert.Fail("ReferenceLevel_dBm should not be settable to 300.");
         }
         catch (Exception ex1)
         {
            TestOutput("Error setting Reference Level [dB]: " + ex1.Message);
         }
         TestOutput("Setting Reference Level to 20 dB");
         testOSA.ReferenceLevel_dBm = 20;
         // Check the get property
         TestOutput("Reading Reference Level [dB] = " + testOSA.ReferenceLevel_dBm);
         // Check if set correctly
         Assert.AreEqual(20, testOSA.ReferenceLevel_dBm);
      }

      /// <exclude />
      [Test]
      public void T15_Attenuator()
      {
         TestOutput("\n\n*** T14_Attenuator***");
         // Check the get property
         TestOutput("Reading Attenuator State = " + testOSA.Attenuator.ToString());
         // Check the set property
         TestOutput("Setting Attenuator State to True");
         testOSA.Attenuator = true;
         // Check the get property
         TestOutput("Reading Attenuator State = " + testOSA.Attenuator.ToString());
         // Check if set correctly
         Assert.AreEqual(true, testOSA.Attenuator);
         // Check the set property
         TestOutput("Setting Attenuator State to False");
         testOSA.Attenuator = false;
         // Check the get property
         TestOutput("Reading Attenuator State = " + testOSA.Attenuator.ToString());
         // Check if set correctly
         Assert.AreEqual(false, testOSA.Attenuator);
      }

      #region Private helper fns
      private void TestOutput(string output)
      {
         BugOut.WriteLine(BugOut.WildcardLabel, output);
      }

      private void TestOutput(string objStr, string output)
      {
         string outputStr = String.Format("{0}: {1}", objStr, output);
         BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
      }
      #endregion
   }

}

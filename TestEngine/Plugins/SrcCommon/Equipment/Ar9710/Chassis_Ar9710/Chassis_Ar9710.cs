// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ar9710.cs
//
// Author: HEIKO.FETTIG, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
   /// <summary>
   /// Anritsu MS9710B/C OSA Chassis Driver
   /// </summary>
   public class Chassis_Ar9710 : ChassisType_Visa488_2
   {
      #region Constructor
      /// <summary>
      /// Chassis Constructor.
      /// </summary>
      /// <param name="chassisNameInit">Chassis name</param>
      /// <param name="driverNameInit">Chassis driver name</param>
      /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
      public Chassis_Ar9710(string chassisNameInit, string driverNameInit,
          string resourceString)
         : base(chassisNameInit, driverNameInit, resourceString)
      {
         // Setup expected valid hardware variants 
         ChassisDataRecord ar9710BData = new ChassisDataRecord(
             "ANRITSU MS9710B",			// hardware name 
             "0",			// minimum valid firmware version 
             "V3.5&V3.1");		// maximum valid firmware version 
         ValidHardwareData.Add("AR9710B", ar9710BData);
         ChassisDataRecord ar9710CData = new ChassisDataRecord(
             "ANRITSU MS9710C",			// hardware name 
             "0",			// minimum valid firmware version 
             "V3.71&V3.3");		// maximum valid firmware version 
         ValidHardwareData.Add("AR9710C", ar9710CData);
      }
      #endregion

      #region Chassis overrides
      /// <summary>
      /// Firmware version of this chassis.
      /// </summary>
      public override string FirmwareVersion
      {
         get
         {
            // Read the chassis ID string
            string idn = Query_Unchecked("*IDN?", null);

            // Return the firmware version in the 4th comma seperated field
            return idn.Split(',')[3].Trim();
         }
      }

      /// <summary>
      /// Hardware Identity of this chassis.
      /// </summary>
      public override string HardwareIdentity
      {
         get
         {
            // Read the chassis ID string and split the comma seperated fields
            string[] idn = Query_Unchecked("*IDN?", null).Split(',');

            // Return field1, the manufacturer name and field 2, the model number
            return idn[0] + " " + idn[1];
         }
      }

      /// <summary>
      /// Sends the correct string to the chassis to retrieve the error code/name.
      /// </summary>
      /// <returns>Error code/name</returns>
      public override string  GetErrorString()
      {
         // Read the Error string
         string error = Query_Unchecked("ERR?", null);

         // Return the Error string
         return error;
      }

      /// <summary>
      /// Setup the chassis as it goes online
      /// </summary>
      public override bool IsOnline
      {
         get
         {
            return base.IsOnline;
         }
         set
         {
            // setup base class
            base.IsOnline = value;

            if (value) // if setting online                
            {
               //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
               this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
               //// clear the status registers
               this.Write("*CLS", null);                    
            }
         }
      }
      #endregion
   }
}

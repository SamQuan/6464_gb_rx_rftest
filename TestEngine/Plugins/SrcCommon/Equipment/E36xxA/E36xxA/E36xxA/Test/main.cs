using System;
using System.Collections.Generic;
using System.Text;


namespace EquipmentTest_Inst_E36xX
{
     public class TestWrapper
    {
        public  static int Main(string[] args)
        {
            //Init tester
            Inst_E3632A_Test test = new Inst_E3632A_Test();
            
            //Run tests
            test.Setup();
            //creat chassis
            test.T01_CreatInst("GPIB0::5::INSTR");
            test.T03_SetOnline();
            
            //test.T02_tryCommsNotOnLine();
            test.T04_DriverVersion();
            test.T05_FirmwareVersion();

            test.T06_HardwareID();

            test.T07_SetDefaultState();

            test.T08_EnableOutput();

            test.T09_Current_amp();

            test.T10_Voltage_volt();
            test.ShutDown();
            return 0;

        }
    }
}

﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_E36xxA.cs
//
// Author: mark.norman, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    /// <summary>
    /// Agilent E36xxA psus
    /// </summary>
    public class Chassis_E36xxA : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_E36xxA(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord E3632AData = new ChassisDataRecord(
            "HEWLETT-PACKARD E3632A", // hardware name HEWLETT-PACKARD E3632A
                "0",										// minimum valid firmware version 
                "6");										// maximum valid firmware version 
            ValidHardwareData.Add("E3632A", E3632AData);

            ChassisDataRecord E3631AData = new ChassisDataRecord(
           "HEWLETT-PACKARD E3631A", // hardware name HEWLETT-PACKARD E3631A
               "0",										// minimum valid firmware version 
               "6");										// maximum valid firmware version 
            ValidHardwareData.Add("E3631A", E3631AData);

            ChassisDataRecord E3644AData = new ChassisDataRecord(
           "Agilent Technologies E3644A", // hardware name Agilent Technologies E3644A
               "0",										// minimum valid firmware version 
               "6");										// maximum valid firmware version 
            ValidHardwareData.Add("E3644A", E3644AData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                string fv = idn[3].Substring(0, 11);
                return idn[3].Substring(4, 3);

            }
        }

        /// <summary>
        /// Hardware identity string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                //string hi = idn[0] + " " + idn[1];
                return idn[0] + " " + idn[1];

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // needed for error detection routines
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                }
            }
        }

        #endregion
        public List<InstType_ElectricalSource> Children
        {
            get
            { return (mChildren); }
            set
            { mChildren = value; }
        }
        private List<InstType_ElectricalSource>mChildren = new List<InstType_ElectricalSource>();
    }
}

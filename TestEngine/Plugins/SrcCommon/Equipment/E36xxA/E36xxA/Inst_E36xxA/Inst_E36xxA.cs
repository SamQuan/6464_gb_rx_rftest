﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_E36xxA.cs
//
// Author: mark.norman, 2010
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    /// <summary>
    /// /
    /// </summary>
    public class Inst_E36xxA : InstType_ElectricalSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_E36xxA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord E3632AData = new InstrumentDataRecord(
            "HEWLETT-PACKARD E3632A", // hardware name HEWLETT-PACKARD E3632A
                "0",										// minimum valid firmware version 
                "6");										// maximum valid firmware version 
            E3632AData.Add("MultiChannelChassis", "false");
            ValidHardwareData.Add("E3632A", E3632AData);

            InstrumentDataRecord E3631AData = new InstrumentDataRecord(
            "HEWLETT-PACKARD E3631A", // hardware name HEWLETT-PACKARD E3631A
                "0",										// minimum valid firmware version 
                "6");										// maximum valid firmware version 
            E3631AData.Add("MultiChannelChassis", "true");
            ValidHardwareData.Add("E3631A", E3631AData);

           
             InstrumentDataRecord E3644AData = new InstrumentDataRecord(
             "Agilent Technologies E3644A", // hardware name Agilent Technologies E3644A
                 "0",										// minimum valid firmware version 
                 "6");										// maximum valid firmware version 
                E3644AData.Add("MultiChannelChassis", "false");
                ValidHardwareData.Add("E3644A", E3644AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
             InstrumentDataRecord E36xxAchassisData = new InstrumentDataRecord(
                "Chassis_E36xxA",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "6");									// maximum valid chassis driver version
             ValidChassisDrivers.Add("E36xxA", E36xxAchassisData);


            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
             this.instrumentChassis = (Chassis_E36xxA)chassisInit; // base.InstrumentChassis;
           
            }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Sets/returns the output state
        /// This does not use the output enable for the whole chassis
        /// it sets and clears the voltage on each channel
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                if (HardwareData["MultiChannelChassis"].ToLower().Contains("false"))
                    return (instrumentChassis.Query_Unchecked("OUTP:STAT?", this).Contains("1"));
                else
                    // Return bool value
                    return (this.mOutputEnabled);
            }
            set
            {
                if (HardwareData["MultiChannelChassis"].ToLower().Contains("false"))
                    instrumentChassis.Write_Unchecked("OUTP:STAT " + (value ? "ON" : "OFF"), this);
                else
                {
                    SelectChannel();
                    this.mOutputEnabled = value;

                    if (value)
                    {
                        // take up the voltage
                        instrumentChassis.Write_Unchecked("VOLT " + this.VoltsSet.ToString().Trim(), this);
                        // make sure the overall output enable of the chassis in on
                        instrumentChassis.Write_Unchecked("OUTP:STAT ON", this);
                    }
                    else
                    {

                        instrumentChassis.Write_Unchecked("VOLT 0.0", this);
                        // only disable the whole instruments output if all of the channels supported by the
                        // chassis are disabled
                        int driverCount = 0;
                        int disabledCount = 0;

                        foreach (InstType_ElectricalSource inst in this.instrumentChassis.Children)
                        {
                            driverCount++;
                            if (!inst.OutputEnabled) disabledCount++;

                        }
                        if (driverCount == disabledCount) instrumentChassis.Write_Unchecked("OUTP:STAT OFF", this);


                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                SelectChannel();
                // Read current in volts
                string rtn = instrumentChassis.Query_Unchecked("volt?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                SelectChannel();
                // Set voltage
                instrumentChassis.Write_Unchecked("VOLT " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        ///  Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                SelectChannel();
                // Read voltage in volts
                string[] meas = instrumentChassis.Query_Unchecked(":MEAS:VOLT?", this).Split(',');

                // Get the 1st value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the voltage setpoint level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                if (!this.mOutputEnabled && HardwareData["MultiChannelChassis"].ToLower().Contains("true"))
                    return(VoltsSet); // for when it is enabled.
                SelectChannel();
                // Get voltage source set level in volts
                string rtn = instrumentChassis.Query_Unchecked("VOLT?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {

                VoltsSet = value; // for when it is enabled.
                if (this.mOutputEnabled || HardwareData["MultiChannelChassis"].ToLower().Contains("false"))
                {
                    SelectChannel();
                    // Set voltage level
                    instrumentChassis.Write_Unchecked("VOLT " + value.ToString().Trim(), this);
                }
            }
        }


        /// <summary>
        /// Sets/returns the compliance current setpoint
        /// Compliance current being the maximum positive or negative current the instrument will supply
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                SelectChannel();
                // Read compliance current in amps
                string rtn = instrumentChassis.Query_Unchecked("CURR?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                SelectChannel();
                // Set current in amps
                instrumentChassis.Write_Unchecked("CURR " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        ///  Sets/returns the current level set point
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                SelectChannel();
                // Get current source set level in amps
                string rtn = instrumentChassis.Query_Unchecked("CURR?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                SelectChannel();
                // Set current source level in amps
                instrumentChassis.Write_Unchecked("CURR " + value.ToString().Trim(), this);
            }
        }


        /// <summary>
        /// Returns the actual measured current level 
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                SelectChannel();
                // Read current in amps
                string[] meas = instrumentChassis.Query_Unchecked(":MEAS:CURR?", this).Split(',');

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            if (!Initialised && HardwareData["MultiChannelChassis"].ToLower().Contains("true"))
            {
                // all this gubbins is required if we have a multichannel driver
                // so outputenable property will work for each channel indipendently
                this.mOutputEnabled = true;                // make true so we can get the current value
                this.VoltsSet = this.VoltageSetPoint_Volt; // need to know this asod
                // use the whole chassis enable to determine if this channel is enabled
                this.mOutputEnabled = instrumentChassis.Query_Unchecked("OUTP:STAT?", null).Contains("ON");
                
                instrumentChassis.Children.Add(this);
                Initialised = true;
            }

        }
        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        /// <remarks>If set to true, sets the instrument into the default state.</remarks>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                  
                        this.SetDefaultState();
                }
            }
        }
        #endregion

        #region Private methods
        private void SelectChannel()
        {
            if (HardwareData["MultiChannelChassis"] == "true")
            {
                string SlotId = base.Slot;
                if (base.Slot == "")
                    SlotId = "P6V"; // use this as the default if not set in configuration
                instrumentChassis.Write_Unchecked("INST:SEL " + SlotId, this);
            }
        }
        #endregion=-[

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        /// // Chassis reference
        private Chassis_E36xxA instrumentChassis;
        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";
        private bool mOutputEnabled;
        private double VoltsSet;
        private bool Initialised = false;
        #endregion
    }
}

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using System.Collections.Generic;

namespace EquipmentTest_Inst_E363X
{
    [TestFixture]
    public class Inst_SR830_Test
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Inst_SR830_Test()
        { }
       
        /// <summary>
        /// 
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
        }
        [Test]
        public void T01_CreatInst(string visaResource)
        {
            testChassis = new Chassis_SR830("TestChassis", "Chassis_SR830", visaResource);
            Console.WriteLine("Chassis Created OK!");
            testInst = new Inst_SR830("TestInst", "Inst_SR830", "", "", testChassis);
         
            Console.WriteLine("Instrument Created OK!");
          

        }

        [Test]
        public void T03_SetOnline()
        {
            testChassis.IsOnline = true;
                Console.WriteLine("Chassis IsOnline set true OK");
            testInst.IsOnline = true;
            Console.WriteLine("Instrument IsOnline set true OK");
        }

        [Test]
        public void T04_DriverVersion()
        {
            Console.WriteLine(testInst.DriverVersion);
        }

        [Test]
        public void T05_FirmwareVersion()
        {
            Console.WriteLine(testInst.FirmwareVersion);
        }

        [Test]
        public void T06_HardwareID()
        {
            Console.WriteLine(testInst.HardwareIdentity);
        }

        [Test]
        public void T07_SetDefaultState()
        {
            testInst.SetDefaultState();
            Console.WriteLine("Default state set OK");


            List<double> outputList = new List<double>();
            do
            {
                double output = testInst.ReadOutput(Inst_SR830.OutputTypeEnum.Phase);
                outputList.Add(output);
                Thread.Sleep(10);
            } while (true);

            double adfa = 00;
        }

        [Test]
        public void T08_TimeConstantSetting()
        {
            testInst.TimeConstant = Inst_SR830.TimeConstantEnum._100ms;
            testInst.LowerPassFilterSloper = Inst_SR830.FilterSlopeEnum.six_dBperOct;
            testInst.SynchronousFilter = true;
        }

        [Test]
        public void T09_SnesitivitySetting()
        {
            testInst.Sensitivity = Inst_SR830.SensitivityEnum._100mVperNA;
        }

        [Test]
        public void T10_SignalInputSetting()
        {
            testInst.InputSignalConfiguration = Inst_SR830.InputSignalConfigEnum.A;
            testInst.InputCouple = Inst_SR830.InputCoupleTypeEnum.AC;
            testInst.ShieldGround = Inst_SR830.ShieldGroundEnum.Ground;
        }

        [Test]
        public void T11_ReserveSetting()
        {
            testInst.ReserveMode = Inst_SR830.ReserveModeEnum.Normal;
        }

        [Test]
        public void T12_NotchFiltersSelect()
        {
            testInst.NotchFilter = Inst_SR830.NotchFilterEnum.LineFilter;
        }

        [Test]
        public void T13_DisplaySetting()
        {
            testInst.SetDisplayAndRatio(Inst_SR830.ChanNumEnum.Ch1, Inst_SR830.DisplyChanOneEnum.X, Inst_SR830.RatioChanOneEnum.AuxIn1);
            testInst.SetDisplayAndRatio(Inst_SR830.ChanNumEnum.Ch2, Inst_SR830.DisplayChanTowEnum.Y, Inst_SR830.RatioChanTwoEnum.AuxIn3);
            testInst.SetOffsetAndExpands(Inst_SR830.signalTypeEnum.X, Inst_SR830.ExpandEnum.x10, 100);
            testInst.SetOffsetAndExpands(Inst_SR830.signalTypeEnum.Y, Inst_SR830.ExpandEnum.x100, 90);
            testInst.AutoOffset(Inst_SR830.signalTypeEnum.R);
        }

        [Test]
        public void T14_AutoSetting()
        {
            testInst.AutoGain();
            testInst.AutoPhase();
            testInst.AutoReserve();
        }

        [Test]
        public void T15_InterfaceSelect()
        {
            testInst.CommunicateInterface = Inst_SR830.InterfaceCommunicateEnum.GPIB;
        }

        [Test]
        public void T16_ReferenceSourceSetting()
        {
            testInst.ReferenceSource = Inst_SR830.ReferenceSourceEnum.Internal;
            testInst.ReferenceFreq_Hz = 1050;
            testInst.ReferencePhase = 180;
            testInst.ReferenceTrigger = Inst_SR830.ReferTrigEnum.FallingEdge;
        }

        [Test]
        public void T17_SaveLockInSetting()
        {
            testInst.SaveSettingInBuff(1);
        }

        [Test]
        public void T18_RecallLockInSetting()
        {
            testInst.RecallSettingInBuff(1);
        }



        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_SR830 testChassis;
        private Inst_SR830 testInst;
       
    }
}

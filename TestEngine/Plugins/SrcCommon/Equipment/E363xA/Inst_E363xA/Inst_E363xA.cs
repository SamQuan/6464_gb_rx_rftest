﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_E363xA.cs
//
// Author: jack.guo, 2008
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    /// <summary>
    /// /
    /// </summary>
    public class Inst_E363xA : InstType_ElectricalSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_E363xA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord E3632AData = new InstrumentDataRecord(
            "HEWLETT-PACKARD E3632A", // hardware name HEWLETT-PACKARD E3632A
                "0",										// minimum valid firmware version 
                "6");										// maximum valid firmware version 
            ValidHardwareData.Add("E3632A", E3632AData);

            InstrumentDataRecord E3631AData = new InstrumentDataRecord(
            "HEWLETT-PACKARD E3631A", // hardware name HEWLETT-PACKARD E3631A
                "0",										// minimum valid firmware version 
                "6");										// maximum valid firmware version 
            ValidHardwareData.Add("E3631A", E3631AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
             InstrumentDataRecord E363xAchassisData = new InstrumentDataRecord(
                "Chassis_E363xA",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "6");									// maximum valid chassis driver version
             ValidChassisDrivers.Add("Chassis_E363xA", E363xAchassisData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
             this.instrumentChassis = (Chassis_E363xA)base.InstrumentChassis;
            }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Sets/returns the output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Query the output state
                string rtn = instrumentChassis.Query_Unchecked(":OUTP:STAT?", this);

                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set output
                instrumentChassis.Write_Unchecked(":OUTP:STAT " + boolVal, this);
            }
        }

/// <summary>
/// /
/// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                // Read current in volts
                string rtn = instrumentChassis.Query_Unchecked("volt?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Set voltage
                instrumentChassis.Write_Unchecked("VOLT " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        ///  Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Read voltage in volts
                string[] meas = instrumentChassis.Query_Unchecked(":MEAS:VOLT?", this).Split(',');

                // Get the 1st value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the voltage setpoint level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                // Get voltage source set level in volts
                string rtn = instrumentChassis.Query_Unchecked("VOLT?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Set voltage level
                instrumentChassis.Write_Unchecked("VOLT " + value.ToString().Trim(), this);
            }
        }


        /// <summary>
        /// Sets/returns the compliance current setpoint
        /// Compliance current being the maximum positive or negative current the instrument will supply
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                // Read compliance current in amps
                string rtn = instrumentChassis.Query_Unchecked("CURR?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Set current in amps
                instrumentChassis.Write_Unchecked("CURR " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        ///  Sets/returns the current level set point
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                // Get current source set level in amps
                string rtn = instrumentChassis.Query_Unchecked("CURR?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Set current source level in amps
                instrumentChassis.Write_Unchecked("CURR " + value.ToString().Trim(), this);
            }
        }


        /// <summary>
        /// Returns the actual measured current level 
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                // Read current in amps
                string[] meas = instrumentChassis.Query_Unchecked(":MEAS:CURR?", this).Split(',');

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
          
            // Set factory default
            instrumentChassis.Write_Unchecked("*RST", this);

        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        /// // Chassis reference
        private Chassis_E363xA instrumentChassis;
        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";
        #endregion
    }
}

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace EquipmentTest_Inst_E363X
{
    [TestFixture]
    public class Inst_E3632A_Test
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Inst_E3632A_Test()
        { }
        /// <summary>
        /// 
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
        }
        [TestFixtureSetUp]
        public void ShutDown()
        {
            testInst.OutputEnabled = false;
            testChassis.IsOnline = false;
            //Test end
            Console.WriteLine("Test Finished!");
        }
        [Test]
        public void T01_CreatInst(string visaResource)
        {
            testChassis = new Chassis_E363xA("TestChassis", "Chassis_E363xA", visaResource);
            Console.WriteLine("Chassis Created OK!");
            testInst = new Inst_E363xA("TestInst", "Inst_E363xA", "1", "", testChassis);
         
            Console.WriteLine("Instrument Created OK!");
          

        }
        [Test]
        [ExpectedException(typeof(System.Exception))]
        public void T02_tryCommsNotOnLine()
        {
            //Cannot try this as logging shuts down the app after the exception
            try
            {
             
                Console.WriteLine(testInst.HardwareIdentity);
            }
            catch(ChassisException e)
            {               
                Console.WriteLine("Expected exception :" + e.Message);
            }

        }
        [Test]
        public void T03_SetOnline()
        {
            testChassis.IsOnline = true;
                Console.WriteLine("Chassis IsOnline set true OK");
            testInst.IsOnline = true;
            Console.WriteLine("Instrument IsOnline set true OK");
        }

        [Test]
        public void T04_DriverVersion()
        {
            Console.WriteLine(testInst.DriverVersion);
        }

        [Test]
        public void T05_FirmwareVersion()
        {
            Console.WriteLine(testInst.FirmwareVersion);
        }

        [Test]
        public void T06_HardwareID()
        {
            Console.WriteLine(testInst.HardwareIdentity);
        }

        [Test]
        public void T07_SetDefaultState()
        {
            testInst.SetDefaultState();
            Console.WriteLine("Default state set OK");
            
        }

        [Test]
        public void T08_EnableOutput()
        {
            Console.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
            testInst.OutputEnabled = true;
            Debug.WriteLine("Output state set to 'true' OK");
            Console.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
        }

        [Test]
        public void T09_Current_amp()
        {
            testInst.CurrentComplianceSetPoint_Amp = 1;
            Console.WriteLine("Compl_Current_amp : " + testInst.CurrentComplianceSetPoint_Amp);
            Console.WriteLine("Current_amp : " + testInst.CurrentActual_amp);
            testInst.CurrentSetPoint_amp = 1;
            Console.WriteLine("Current_amp set to 1");
            Console.WriteLine("Current_amp : " + testInst.CurrentActual_amp);
            testInst.CurrentSetPoint_amp =1;
        }

        [Test]
        public void T10_Voltage_volt()
        {
            testInst.VoltageComplianceSetPoint_Volt = 5;
            Console.WriteLine("Compl_Voltage_volt : " + testInst.VoltageComplianceSetPoint_Volt);
            Console.WriteLine("Voltage_volt : " + testInst.VoltageActual_Volt);
            testInst.VoltageSetPoint_Volt = 5;
            Console.WriteLine("Voltage_volt set to 5");
            Console.WriteLine("Voltage_volt : " + testInst.VoltageActual_Volt);
            testInst.VoltageSetPoint_Volt = 5;
        }


        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_E363xA testChassis;
        private Inst_E363xA testInst;
       
    }
}

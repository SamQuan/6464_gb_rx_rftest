// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag816x_OpticalPowerMeter.cs
//
// Author: paul.annetts, 2006
// Design: As per 816x Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;
using System.Collections;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 816x mounted power meter. This driver aims to support all types of power meter, 
    /// including optical power heads (8162x), power sensor modules (8163x) and old series
    /// of power meters (815xx)
    /// </summary>
    public class Inst_Ag816x_OpticalPowerMeter : InstType_OpticalPowerMeter, IInstType_TriggeredOpticalPowerMeter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag816x_OpticalPowerMeter(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // Configure valid hardware information
            string maxPowMeterFwVersion = "V4.85(1126)";
            // Add 81623A optical head details
            InstrumentDataRecord ag81623AData = new InstrumentDataRecord(
                "81623A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81623AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81623A", ag81623AData);

            // Add 81624A optical head details
            InstrumentDataRecord ag81624AData = new InstrumentDataRecord(
                "81624A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81624AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81624A", ag81624AData);

            // Add 81625A optical head details
            InstrumentDataRecord ag81625AData = new InstrumentDataRecord(
                "81625A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81625AData.Add("MaximumPwrRangeDbm", "20");		// maximum valid power range 
            ValidHardwareData.Add("Ag81625A", ag81625AData);

            // Add 81625B optical head details
            InstrumentDataRecord ag81625BData = new InstrumentDataRecord(
                "81625B",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81625BData.Add("MaximumPwrRangeDbm", "27");		// maximum valid power range 
            ValidHardwareData.Add("Ag81625B", ag81625BData);

            // Add 81626B optical head details
            InstrumentDataRecord ag81626BData = new InstrumentDataRecord(
                "81626B",                 // hardware name 
                "V1.0",                                     // minimum valid firmware version 
                maxPowMeterFwVersion);                         // maximum valid firmware version 
            ag81626BData.Add("MaximumPwrRangeDbm", "27");      // maximum valid power range 
            ValidHardwareData.Add("Ag81626B", ag81626BData);

            // Add 81632A optical head details
            InstrumentDataRecord ag81632AData = new InstrumentDataRecord(
                "81632A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81632AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81632A", ag81632AData);

            // Add 81633A optical head details
            InstrumentDataRecord ag81633AData = new InstrumentDataRecord(
                "81633A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81633AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81633A", ag81633AData);

            // Add 81633B optical head details
            InstrumentDataRecord ag81633BData = new InstrumentDataRecord(
                "81633B",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81633BData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81633B", ag81633BData);

            // Add 81634A optical head details
            InstrumentDataRecord ag81634AData = new InstrumentDataRecord(
                "81634A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81634AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81634A", ag81634AData);

            // Add 81635A optical head details
            InstrumentDataRecord ag81635AData = new InstrumentDataRecord(
                "81635A",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81635AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81635A", ag81635AData);

            // Add 81636B optical head details
            InstrumentDataRecord ag81636BData = new InstrumentDataRecord(
                "81636B",					// hardware name 
                "V1.0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81636BData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81636B", ag81636BData);

            //add for Ag8163B..
            InstrumentDataRecord ag8163BData = new InstrumentDataRecord(
                "Agilent Technologies 8163B",
                "0",
                "V5.01(54851)");
            this.ValidHardwareData.Add("Ag8163B", ag8163BData);

            //add for Ag8163B..
            InstrumentDataRecord ag81533BData = new InstrumentDataRecord(
                "HEWLETT-PACKARD,HP81533B",
                "0",
                "19-May-00");
            this.ValidHardwareData.Add("Ag81533B", ag81533BData);

            //add for Ag81525A..
            InstrumentDataRecord ag81525AData = new InstrumentDataRecord(
                //"HEWLETT-PACKARD,HP81525A",
                "HP81525A",
                "0",
                "19-May-00");
            this.ValidHardwareData.Add("Ag81525A", ag81525AData);

            // Configure valid chassis information
            // Add 816x chassis details
            InstrumentDataRecord ag816xData = new InstrumentDataRecord(
                "Chassis_Ag816x",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag816x", ag816xData);

            InstrumentDataRecord ag81624BData = new InstrumentDataRecord(
             "81624B",					// hardware name 
             "V1.0",											// minimum valid firmware version 
             maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81624BData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81624B", ag81624BData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag816x)base.InstrumentChassis;

            // validate range for slot and subslot.
            int slot;
            int subSlot;
            try
            {
                slot = int.Parse(this.Slot);
                subSlot = int.Parse(this.SubSlot);
            }
            catch (SystemException e)
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot/subslot format",
                    this.Name);
                throw new InstrumentException(errStr, e);
            }

            // 8166 has 17 slots (maximum possible)
            if ((slot < 1) || (slot > 17))
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot {1}",
                    this.Name, this.Slot);
                throw new InstrumentException(errStr);
            }

            // A maximum of two channels in each slot (81635, 81619)
            if ((subSlot < 1) || (subSlot > 2))
            {
                string errStr = String.Format("Instrument '{0}': Invalid sub-slot {1}",
                    this.Name, this.SubSlot);
                throw new InstrumentException(errStr);
            }

            // Generate the common command stem that most commands use!
            this.commandStem = String.Format(":SENS{0}:CHAN{1}:",
                    this.Slot, this.SubSlot);
            this.commandPowStem = this.commandStem + "POW:";

            // default to safe mode
            this.safeMode = true;
        }
        #endregion

        #region Instrument overrides


        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // All the firmware is in the plugin slot, not the head - check if it is indeed present
                this.checkSlotNotEmpty();
                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format(":SLOT{0}:IDN?", this.Slot);
                string resp = instrumentChassis.Query_Unchecked(command, this);
                string pluginFwVer = resp.Split(',')[3];

                // Return the 4th substring containing the firmware version
                return pluginFwVer;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string command;
                string resp;
                string hwID;

                // check if the instrument channel specified is actually there!
                this.checkSlotNotEmpty();
                this.checkHeadNotEmpty();
                // Read the HEAD IDN? string which has 4 comma seperated substrings                
                command = String.Format(":SLOT{0}:HEAD{1}:IDN?", this.Slot, this.SubSlot);
                resp = instrumentChassis.Query_Unchecked(command, this);
                if (resp.Length > 0)
                {
                    // There IS a head... This isn't a power sensor module.
                    // Model Nbr after 1st comma
                    hwID = resp.Split(',')[1];
                }
                else
                {
                    // No head... This is a power sensor module.
                    // However the previous command will cause an error to be recorded, so clear the registers!
                    instrumentChassis.Write_Unchecked("*CLS", this);

                    // Read the SLOT IDN? string which has 4 comma seperated substrings                
                    command = String.Format(":SLOT{0}:IDN?", this.Slot);
                    resp = instrumentChassis.Query_Unchecked(command, this);
                    if (resp.Length == 0)
                    {
                        string errStr = String.Format("No 816x power meter available in '{0}' at {1}:{2}",
                            this.ChassisName, this.Slot, this.SubSlot);
                        throw new InstrumentException(errStr);
                    }
                    // Model Nbr after 1st comma
                    hwID = resp.Split(',')[1];
                }
                return hwID;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            SetContinuousModeOff();
            Mode = MeterMode.Absolute_dBm;
            Range = InstType_OpticalPowerMeter.AutoRange;
            Wavelength_nm = 1550;
            AveragingTime_s = 0.1;
            CalOffset_dB = 0.0;
            ReferencePower = 0.0;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // read hardware data to remember maximum power
                    this.maxPowerRange_dBm = Convert.ToDouble(HardwareData["MaximumPwrRangeDbm"]);

                    // check that the power meter is present
                    this.checkSlotNotEmpty();
                    this.checkHeadNotEmpty();

                    // turn off continuous measurement
                    string command = String.Format("INIT{0}:CONT OFF", this.Slot);
                    chassisWrite(command);

                    // get current mode
                    this.currMode = Mode;
                }
            }
        }
        #endregion

        #region Optical Power Meter functions
        /// <summary>
        /// Get / Set the mode of optical power meter. 
        /// If a set of a mode which is not supported by this instrument is attempted, 
        /// an exception should be thrown.
        /// </summary>
        public override InstType_OpticalPowerMeter.MeterMode Mode
        {
            get
            {
                string command;
                string resp;
                // are we in absolute or relative mode
                command = commandPowStem + "REF:STAT?";
                resp = chassisQuery(command);
                if (resp == "1")
                {
                    this.currMode = MeterMode.Relative_dB;
                }
                else if (resp == "0")
                {
                    // dBm or W
                    command = commandPowStem + "UNIT?";
                    resp = chassisQuery(command);
                    if (resp == powUnit_dBm) this.currMode = MeterMode.Absolute_dBm;
                    else if (resp == powUnit_W) this.currMode = MeterMode.Absolute_mW;
                    else
                    {
                        throw new InstrumentException("Invalid response to '" + command + "': " + resp);
                    }
                }
                else
                {
                    throw new InstrumentException("Invalid response to '" + command + "': " + resp);
                }
                return this.currMode;
            }

            set
            {
                // get current mode - refresh cached state
                this.currMode = this.Mode;
                if (value == this.currMode) return;

                switch (value)
                {
                    case MeterMode.Absolute_dBm:
                        if (currMode == MeterMode.Relative_dB)
                        {
                            chassisWrite(commandPowStem + "REF:STAT 0");
                        }
                        chassisWrite(commandPowStem + "UNIT " + powUnit_dBm);
                        break;
                    case MeterMode.Absolute_mW:
                        if (currMode == MeterMode.Relative_dB)
                        {
                            chassisWrite(commandPowStem + "REF:STAT 0");
                        }
                        chassisWrite(commandPowStem + "UNIT " + powUnit_W);
                        break;
                    case MeterMode.Relative_dB:
                        // force a reference mode of absolute power
                        chassisWrite(commandPowStem + "REF:STAT:RAT TOREF,1");
                        // turn on relative mode
                        chassisWrite(commandPowStem + "REF:STAT 1");
                        break;
                    default:
                        throw new InstrumentException("Unsupported Meter Mode: " + value.ToString());
                }
                // remember new mode
                this.currMode = value;
            }
        }

        /// <summary>
        /// Reads/sets the expected input signal wavelength in nanometres
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                // Read configured wavelength in metres, convert to nm and return
                string resp = chassisQuery(commandPowStem + "WAV?");
                return Convert.ToDouble(resp) * 1e9;
            }
            set
            {
                // The meter can understand NM, so let it deal with the conversion...
                string command = String.Format("{0}WAV {1}NM", commandPowStem, value);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Get / Set calibration offset in decibels.
        /// </summary>
        public override double CalOffset_dB
        {
            get
            {
                // Read configured offset
                string resp = chassisQuery(commandStem + "CORR?");
                return Convert.ToDouble(resp);
            }
            set
            {
                string command = String.Format("{0}CORR {1}DB", commandStem, value);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Get/Set reference power for relative measurements. Uses the 
        /// current MeterMode for units.
        /// MeterMode.Absolute_dBm -> dBm
        /// MeterMode.Absolute_mW  -> mW
        /// MeterMode.Relative_dB  -> dBm
        /// </summary>
        public override double ReferencePower
        {
            get
            {
                // read the power
                string resp = chassisQuery(commandPowStem + "REF? TOREF");
                double respValue = Convert.ToDouble(resp);
                if (this.currMode == MeterMode.Absolute_mW)
                {
                    // will have returned power in Watts, so transform
                    return respValue * 1e3;
                }
                // value is in dBm as expected
                else return respValue;
            }
            set
            {
                string unit;
                switch (this.currMode)
                {
                    case MeterMode.Absolute_dBm:
                    case MeterMode.Relative_dB:
                        unit = "DBM";
                        break;
                    case MeterMode.Absolute_mW:
                        unit = "MW";
                        break;
                    default:
                        throw new InstrumentException("Invalid power meter mode: " + this.currMode.ToString());
                }


                string command = String.Format("{0}REF TOREF,{1}{2}", commandPowStem, value, unit);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Sets/Returns the measurement range. Units depend on current mode.
        /// The range method guarantees to set a power at or *above* that specified in this function.
        /// I.e. the power level specified in the set is guaranteed to be measureable in this mode.        
        /// </summary>
        /// <remarks>For 816x DUAL head power meters (e.g. 81635, 81619) auto-ranging 
        /// talks to "master" channel (1)
        /// whatever the ACTUAL channel this object represents. 
        /// N.B.: The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences. 
        /// 
        /// N.B.: Manual ranging is independent of channel!</remarks>
        public override double Range
        {
            get
            {
                string resp;
                // check is autorange                
                if (inAutoRangeMode()) return AutoRange;

                // not auto-range - query actual range
                resp = chassisQuery(commandPowStem + "RANG?");
                double range_dBm = Convert.ToDouble(resp);
                // return dependent on current mode
                if (this.currMode == MeterMode.Absolute_mW)
                {
                    return Inst_Ag816x_OpticalPowerMeter.Convert_dBmtomW(range_dBm);
                }
                else return range_dBm;
            }
            set
            {
                string command;

                bool inAutoRange = inAutoRangeMode();
                // are we being asked to change into AutoRange mode?
                if (IsAutoRange(value))
                {
                    // only set to autorange if isn't already
                    if (!inAutoRange)
                    {
                        // auto-range only set on MASTER channel
                        command = String.Format(":SENS{0}:POW:RANG:AUTO ON",
                            this.Slot);
                        chassisWrite(command);
                    }
                }
                else
                {
                    // switch off autorange if necessary
                    if (inAutoRange)
                    {
                        // auto-range only set on MASTER channel
                        command = String.Format(":SENS{0}:POW:RANG:AUTO OFF",
                            this.Slot);
                        chassisWrite(command);
                    }
                    double setValue_dBm;
                    // set value must be sent in dBm, whatever the current mode
                    if (Mode == MeterMode.Absolute_mW)
                    {
                        setValue_dBm = Inst_Ag816x_OpticalPowerMeter.Convert_mWtodBm(value);
                    }
                    else setValue_dBm = value;

                    // 816x Power Meters ROUND the value, don't take the next highest,
                    // so have to "unround" it by adding half the range. Luckily the ranges
                    // are all 10 dB apart, so add 5 dB to force the next range higher up...

                    // Check in driver that range isn't too high
                    if (setValue_dBm > this.maxPowerRange_dBm)
                    {
                        string errStr = String.Format("Power range set out-of-range (too high): {0} ({1} dBm)",
                            value, setValue_dBm);
                        throw new InstrumentException(errStr);
                    }

                    // Do the rounding, checking for high end condition
                    if (setValue_dBm > this.maxPowerRange_dBm - 5)
                    {
                        setValue_dBm = this.maxPowerRange_dBm;
                    }
                    else setValue_dBm = setValue_dBm + 5;

                    // send new range - manual range independently set PER channel
                    command = String.Format("{0}RANG {1}", commandPowStem, setValue_dBm);
                    chassisWrite(command);
                }
            }
        }

        /// <summary>
        /// Sets/returns the measurement averaging value in seconds for the power meter channel.
        /// </summary>
        /// <remarks>For 816x DUAL head power meters (e.g. 81635, 81619) this 
        /// talks to "master" channel (1)
        /// whatever the ACTUAL channel this object represents. 
        /// N.B.: The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override double AveragingTime_s
        {
            get
            {
                // Read averaging time FROM CHANNEL 1 (MASTER)
                string command = String.Format(":SENS{0}:POW:ATIM?", this.Slot);
                string resp = chassisQuery(command);
                return Convert.ToDouble(resp);
            }
            set
            {
                // Sets the averaging time IN CHANNEL 1 (MASTER)
                string command = String.Format(":SENS{0}:POW:ATIM {1}", this.Slot
                    , value);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Initiate and return a power measurement. Returned units depend 
        /// on the current mode of the instrument (dBm, mW, dB). 
        /// </summary>
        public override double ReadPower()
        {
            // Ensure that continuous mode is off, otherwise the INIT will give error -213 ("Init Ignored")
            // ian.webb 24May2007: this can only be set on the master channel (slave channel follows it)
            //                     so always use CHAN1 for the channel identifier!
            string command = String.Format(":INIT{0}:CHAN1:CONT OFF", this.Slot);
            chassisWrite(command);
            // software trigger a reading
            chassisWrite("INIT" + this.Slot);
            // fetch the reading from this channel
            command = String.Format(":FETC{0}:CHAN{1}:POW?", this.Slot, this.SubSlot);

            // manual check of errors as "negative" powers seen at zero low light levels mess
            // up the error registers. Also overload condition causes errors - in
            // fact these two conditions are pretty much indistinguishable.

            string resp = this.instrumentChassis.Query_Unchecked(command, this);
            if (resp == "+3.40282300E+038")
            {
                // have a problem - underrange in dB mode or range overload.
                // clear the standard registers
                chassisWrite("*CLS");
                return OutOfRange;
            }

            if (safeMode)
            {
                // manually check for errors if the reading was "OK"...
                StatusByteFlags sbf = this.instrumentChassis.StatusByte488_2_Command;
                if (this.instrumentChassis.IsStandardEvent(sbf))
                {
                    this.instrumentChassis.RaiseChassisException(command, this);
                }
            }

            double dbl = Convert.ToDouble(resp);
            if (this.currMode == MeterMode.Absolute_mW)
            {
                return dbl * 1e3;
            }
            else return dbl;
        }

        /// <summary>
        /// Starts a calibration of power meter dark-current 
        /// value on the power meter channel. For this calibration to succeed the meter must 
        /// have zero light input. This function returns immediately once the command
        /// has been sent to the instrument (Async command).
        /// </summary>
        /// <remarks>For 816x DUAL head power meters (e.g. 81635, 81619) this 
        /// talks to "master" channel (1)
        /// whatever the ACTUAL channel this object represents. 
        /// N.B.: The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override void ZeroDarkCurrent_Start()
        {
            string command;

            // start the zeroing
            command = String.Format(":SENS{0}:CORR:COLL:ZERO", this.Slot);
            this.instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Ends a calibration of optical dark-current. Waits for a previously started
        /// dark current cal to complete.
        /// </summary>
        /// <remarks>For 816x DUAL head power meters (e.g. 81635, 81619) this 
        /// talks to "master" channel (1)
        /// whatever the ACTUAL channel this object represents. 
        /// N.B.: The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override void ZeroDarkCurrent_End()
        {
            // allow a timeout of 45 seconds for this very long operation
            int timeoutInit = instrumentChassis.Timeout_ms;
            instrumentChassis.Timeout_ms = 45000;
            // wait for pending operations
            instrumentChassis.WaitForOperationComplete();
            // reset timeout to normal
            instrumentChassis.Timeout_ms = timeoutInit;

            string command;
            string resp;

            // check the zero query
            command = String.Format(":SENS{0}:CORR:COLL:ZERO?", this.Slot);
            resp = chassisQuery(command);
            if (resp != "+0")
            {
                throw new InstrumentException("Zeroing operation failed, returned: " + resp);
            }
        }

        /// <summary>
        /// Get/Set Enable/Disable Min/Max hold function.
        /// </summary>
        public override bool MaxMinOn
        {
            // added by m q norman 2007/07/24
            get
            {
                // ask for result string
                string command = String.Format(":SENS{0}:FUNC:STAT?", this.Slot);
                string resp = chassisQuery(command);
                return (resp.Contains("MINMAX,PROGRESS"));
            }

            set
            {
                string command;
                if (value) // switch on
                {

                    // stop it just in case it has been left running
                    command = String.Format(":SENS{0}:FUNC:STAT MINM,STOP", this.Slot);
                    this.instrumentChassis.Write(command, this);
                    // configure it
                    command = String.Format(":SENS{0}:FUNC:PAR:MINM CONT,1", this.Slot); // the ,1 parameter is not used but the instrument expects it
                    this.instrumentChassis.Write(command, this);
                  
                    // start it
                    command = String.Format(":SENS{0}:FUNC:STAT MINM,STAR", this.Slot);
                    this.instrumentChassis.Write(command, this);
                }
                else     // switch off
                { 
                    // stop it
                    command = String.Format(":SENS{0}:FUNC:STAT MINM,STOP", this.Slot);
                    this.instrumentChassis.Write(command, this);
                }
            }
        }


        /// <summary>
        /// Return  the max and min power readings observed since the last activation 
        /// of Max/Min Hold Mode.
        /// </summary>
        public override InstType_OpticalPowerMeter.PowerMaxMin ReadPowerMaxMin()
        {
            // added by m q norman 2007/07/24
            // test in min max mode first
            if (!this.MaxMinOn)
                throw new InstrumentException("Can not return PowerMaxMin because PM is not in max min mode");
            // create an empty result
            InstType_OpticalPowerMeter.PowerMaxMin result = new InstType_OpticalPowerMeter.PowerMaxMin();
            // prepare the correct query string
            string command = String.Format(":SENS{0}:FUNC:RES?", this.Slot);
            string resp = "";
            try
            {
                // do the query
                resp = chassisQuery(command);
            }
            catch (ChassisException ce)
            {
                // if error -261 ( Math error ) is normally due to no signal as the detector sees zero mW causing log 0 error, translate into a better description
                string errStr = ce.Message;
                if(ce.Message.Contains("-261"))
                    errStr = String.Format("Instrument '{0}': No power seen while in min max mode",
                        this.Name);
                
                throw new InstrumentException(errStr, ce);
             }
            
            // expected and assumed format is "#255\nMin:n.nnnnnE+nn,Max:n.nnnnnE+nn,Act:n.nnnnnE+nn"
            if(!resp.Contains("Min:") || !resp.Contains("Max:"))
                throw new InstrumentException(string.Format("Not returning correct min max format, is PM in Min Max Mode? Return<{0}>",resp));
            // into parts, should be 3
            string[] parts = resp.Split(',');
            // part 1 is min
            result.Min = double.Parse(parts[0].Substring(9));
            // part 2 is max
            result.Max = double.Parse(parts[1].Substring(5));
           
            return result;
        }


        #endregion

        #region 816x Power Meter Specific functions
        /// <summary>
        /// SafeMode flag. If true, instrument will always wait for completion after every command and check
        /// the error registers. If false it will do neither.
        /// </summary>
        public bool SafeMode
        {
            get
            {
                return this.safeMode;
            }
            set
            {
                safeMode = value;
            }
        }



        #endregion

        #region ITriggeredOpticalPowerMeter Members
        /// <summary>
        /// Wait for input trigger
        /// </summary>
        /// <param name="enabled">TRUE to wait for a trigger, FALSE to ignore triggers</param>
        public void EnableInputTrigger(bool enabled)
        {
            string commandTriggerStem = String.Format("TRIG{0}:CHAN{1}:", this.Slot, this.SubSlot);
            if (enabled)
            {
                chassisWrite(commandTriggerStem + "INP SME");
            }
            else
            {
                chassisWrite(commandTriggerStem + "INP IGN");
            }
        }
        /// <summary>
        /// Produce an output trigger after taking a reading
        /// </summary>
        /// <param name="enabled">TRUE to produce a trigger pulse</param>
        public void EnableOutputTrigger(bool enabled)
        {
            string commandTriggerStem = String.Format("TRIG{0}:CHAN{1}:", this.Slot, this.SubSlot);
            if (enabled)
            {
                chassisWrite(commandTriggerStem + "OUTP AVG");
            }
            else
            {
                chassisWrite(commandTriggerStem + "OUTP DIS");
            }
        }
        /// <summary>
        /// Start data acquisition mode
        /// </summary>
        /// <remarks>
        /// When you enable a logging data acquisition function for a Agilent 8163A/B Series Power
        /// Meter with averaging time of less than 100 ms with input hardware triggering disabled, all
        /// GPIB commands will be ignored for the duration of the function.
        /// </remarks>
        public void StartDataLogging()
        {
            chassisWrite(commandStem + "FUNC:STAT LOGG,START");
        }
        /// <summary>
        /// Exit data acquisition mode
        /// </summary>
        /// <remarks>
        /// Exit data acquisition before you try to set up any more data acquisition.
        /// Some parameters cannot be set until you exit this mode. 
        /// </remarks>
        public void StopDataLogging()
        {
            chassisWrite(commandStem + "FUNC:STAT LOGG,STOP");
        }
        /// <summary>
        /// Set up data acquisition.
        /// </summary>
        /// <param name="numberOfPoints">The number of points to collect</param>
        /// <param name="averagingTime_s">The averaging time of each measurement</param>
        public void ConfigureDataLogging(int numberOfPoints, double averagingTime_s)
        {
            string command = commandStem + String.Format("FUNC:PAR:LOGG {0},{1}", numberOfPoints, averagingTime_s);
            chassisWrite(command);
        }
        /// <summary>
        /// Query whether data acquisition is in progress
        /// </summary>
        /// <returns>TRUE if busy</returns>
        public bool DataLoggingInProgress()
        {
            string response = chassisQuery(commandStem + "FUNC:STATE?");
            string[] words = response.Split(',');
            return words[1] != "COMPLETE";
        }
        /// <summary>
        /// Retrieve power data.
        /// </summary>
        /// <returns>An array of power data converted to the appropriate units in accordance with the MeterMode</returns>
        public double[] GetSweepData()
        {
            bool busy = true;
            while (busy)
            {
//#warning - Add a timeout here
                busy = DataLoggingInProgress();
            }
            byte[] buffer = this.instrumentChassis.QueryIEEEBinary_Unchecked(commandStem + "FUNC:RES?", this);
            // Convert byte array into an array of doubles.
            ArrayList dataList = new ArrayList();
            for (int i = 0; i < buffer.Length; i += 4)
            {
                Single dataPoint = BitConverter.ToSingle(buffer, i);
                dataList.Add((double)dataPoint);
            }
            double[] power_W = (Double[])dataList.ToArray(typeof(Double));
            double[] power_mW = Alg_ArrayFunctions.MultiplyEachArrayElement(power_W, 1000);
            // Convert units as appropriate
            switch (this.currMode)
            {
                case MeterMode.Absolute_dBm:
                    return Alg_PowConvert_dB.Convert_mWtodBm(power_mW);
                case MeterMode.Absolute_mW:
                    return power_mW;
                case MeterMode.Relative_dB:
                // Even in relative mode the sweep data is returned in Watts.
                // Relative mode could be relative to a fixed reference or relative to another channel.
                // Just don't do it !
                default:
                    throw new InstrumentException("Data collected in unhandled measurement mode : '" + this.currMode.ToString() + "'");
            }
        }
        #endregion

        #region Helper functions
        /// <summary>
        /// put this entire slot into non continuous mode
        /// </summary>
        private void SetContinuousModeOff()
        {
            string cmd = String.Format("INIT{0}:CONT OFF", this.Slot);
            chassisWrite(cmd);
        }

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            if (safeMode)
            {
                this.instrumentChassis.Write(command, this);
            }
            else
            {
                // if not in safe mode commands are async and no error checking
                this.instrumentChassis.Write(command, this, true, false);
            }
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            if (safeMode)
            {
                return this.instrumentChassis.Query(command, this);
            }
            else
            {
                // if not in safe mode, don't check for errors
                return this.instrumentChassis.Query(command, this, false);
            }
        }

        /// <summary>
        /// Is the instrument in autoranging mode
        /// </summary>
        /// <returns>true if autoranging, false otherwise</returns>
        private bool inAutoRangeMode()
        {
            string command = String.Format(":SENS{0}:POW:RANG:AUTO?", this.Slot);
            string resp = chassisQuery(command);
            if (resp == trueStr) return true;
            else if (resp == falseStr) return false;
            else throw new InstrumentException("Bad return from command '" +
                command + "' : " + resp);
        }

        /// <summary>
        /// Check that the slot isn't empty. This may happen before errors setup, so 
        /// DON'T called checked Chassis methods.
        /// </summary>
        private void checkSlotNotEmpty()
        {
            bool slotEmpty;

            // All the firmware is in the plugin slot, not the head
            string command = String.Format(":SLOT{0}:EMPTY?", this.Slot);
            string resp = this.instrumentChassis.Query_Unchecked(command, this);
            if (resp == "1") slotEmpty = true;
            if (resp == "0") slotEmpty = false;
            else
            {
                string errStr = String.Format("Command '{0}' returned bad value: '{1}'",
                    command, resp);
                throw new InstrumentException(errStr);
            }

            if (slotEmpty)
            {
                throw new InstrumentException("Slot " + this.Slot + " is empty!");
            }
        }

        /// <summary>
        /// Check that the power meter head isn't empty. This may happen before errors setup, so 
        /// DON'T called checked Chassis methods.
        /// </summary>
        private void checkHeadNotEmpty()
        {
            bool subSlotEmpty;

            // All the firmware is in the plugin slot, not the head
            string command = String.Format(":SLOT{0}:HEAD{1}:EMPTY?", this.Slot,
                this.SubSlot);
            string resp = this.instrumentChassis.Query_Unchecked(command, this);
            if (resp == "1") subSlotEmpty = true;
            if (resp == "0") subSlotEmpty = false;
            else
            {
                string errStr = String.Format("Command '{0}' returned bad value: '{1}'",
                    command, resp);
                throw new InstrumentException(errStr);
            }

            if (subSlotEmpty)
            {
                throw new InstrumentException("Slot " + this.Slot + " Head " + this.SubSlot + " is empty!");
            }
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag816x instrumentChassis;

        /// <summary>
        /// Current meter mode state cache
        /// </summary>
        private MeterMode currMode;

        private bool safeMode;

        /// <summary>
        /// Maximum power range supported by this Chassis
        /// </summary>
        private double maxPowerRange_dBm;

        // Power unit constants
        private const string powUnit_dBm = "+0";
        private const string powUnit_W = "+1";

        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";

        // Common command stem ":SENS{0}:CHAN{1}:" for most commands
        private string commandStem;
        // Common command stem ":SENS{0}:CHAN{1}:POW:" for most power commands
        private string commandPowStem;

        #endregion
    }

}

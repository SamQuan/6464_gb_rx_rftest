// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_816x.cs
//
// Author: paul.annetts, 2006
// Design: As per 816x Driver DD.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for Agilent 816x Lightwave Measurement System.
    /// </summary>
    public class Chassis_Ag816x : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="visaResourceString">VISA resource string for communicating with the chassis</param>
        public Chassis_Ag816x(string chassisNameInit, string driverNameInit,
            string visaResourceString)
            : base(chassisNameInit, driverNameInit, visaResourceString)
        {
            // maximum allowed chassis version - if we know of a newer good firmware version,
            // update the following line and all the chassis variants will be updated!

            string maxChassisVersion = "V6.85(1126)";
            // Setup expected valid hardware variants 
            // Add Ag8163A & B details
            ChassisDataRecord ag8163AData = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8163A",			// hardware name 
                "0",								// minimum valid firmware version 
                maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8163A", ag8163AData);

            ChassisDataRecord ag8163A2Data = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8163A",			// hardware name 
                "0",								// minimum valid firmware version 
                "V4.02(1291)");					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8163A2", ag8163A2Data);

            ChassisDataRecord ag8163BData = new ChassisDataRecord(
                "Agilent Technologies 8163B",			// hardware name 
                "0",								// minimum valid firmware version 
                "V5.25(72637)");					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8163B", ag8163BData);
            

            // Add Ag8164 details
            ChassisDataRecord ag8164AData = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8164A",			// hardware name 
                "0",								// minimum valid firmware version 
                maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8164A", ag8164AData);

            ChassisDataRecord ag8164BData = new ChassisDataRecord(
                "Agilent Technologies 8164B",			// hardware name 
                "0",								// minimum valid firmware version 
                maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8164B", ag8164BData);

            // Add Ag8166 details
            ChassisDataRecord ag8166AData = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8166A",			// hardware name 
                "0",								// minimum valid firmware version 
                maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8166A", ag8166AData);

            ChassisDataRecord ag81624BData = new ChassisDataRecord(
               "Agilent Technologies 81624B",			// hardware name 
               "0",								// minimum valid firmware version 
               maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("Ag81624B", ag81624BData);
            



            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // NOTHING TO DO
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // Set up Chassis standard event register mask
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write("*CLS", null);                    
                }
            }
        }
        /// <summary>
        /// Queries an IEEE standard binary block from an Agilent Chassis
        /// </summary>
        /// <param name="cmd">Command to send to equipment</param>
        /// <param name="i">Equipment reference</param>
        /// <returns>Reply byte array</returns>
        /// <remarks>Need new version to deal with Agilent extra termination character!</remarks>
        public new byte[] QueryIEEEBinary_Unchecked(string cmd, Instrument i)
        {
            // get the file data
            byte[] bytes = base.QueryIEEEBinary_Unchecked(cmd, i);
            // Agilent appends a linefeed character after this, so need to read this back too
            // so that it doesn't mess up any future comms. (ignore it)
            this.Read_Unchecked(null);
            return bytes;
        }
        #endregion        
    }
}

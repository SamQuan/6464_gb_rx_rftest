using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Chassis_816x_Test
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_Ag816x testChassis;

        private Inst_Ag816x_OpticalPowerMeter testOPM_1;

        private Inst_Ag816x_OpticalPowerMeter testOPM_2;     
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB1::10::INSTR";
        //"//cas-w-182/GPIB0::20::INSTR"; 
        //"//pai-tx-labj1/GPIB1::20::INSTR"; 
        const string chassisName = "816x Chassis";
        const string instr1Name = "OPM1";
        const string instr2Name = "OPM2";
        string instr1Slot = "2";
        string instr1SubSlot = "1";

        string instr2Slot = "1";
        string instr2SubSlot = "1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_Ag816x(chassisName, "Chassis_Ag816x", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testOPM_1 = new Inst_Ag816x_OpticalPowerMeter(instr1Name,
                "Inst_Ag816x_OpticalPowerMeter", instr1Slot, instr1SubSlot, testChassis);
            TestOutput(instr1Name, "Created OK");

            testOPM_2 = new Inst_Ag816x_OpticalPowerMeter(instr2Name,
                "Inst_Ag816x_OpticalPowerMeter", instr2Slot, instr2SubSlot, testChassis);
            TestOutput(instr2Name, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testOPM_1.IsOnline = true;
            TestOutput(instr1Name, "IsOnline set true OK");
            testOPM_2.IsOnline = true;
            TestOutput(instr2Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(chassisName, "HW: " + testChassis.HardwareIdentity);
            TestOutput(chassisName, "FW: " + testChassis.FirmwareVersion);
            TestOutput(instr1Name, "Driver: " + testOPM_1.DriverName + ":" + testOPM_1.DriverVersion);
            TestOutput(instr1Name, "HW: " + testOPM_1.HardwareIdentity);
            TestOutput(instr1Name, "FW: " + testOPM_1.FirmwareVersion);
            TestOutput(instr2Name, "Driver: " + testOPM_2.DriverName + ":" + testOPM_2.DriverVersion);
            TestOutput(instr2Name, "HW: " + testOPM_2.HardwareIdentity);
            TestOutput(instr2Name, "FW: " + testOPM_2.FirmwareVersion);
        }

        [Test]
        public void T02_MeterMode()
        {
            TestOutput("\n\n*** T02_MeterMode ***");
            TestOutput(instr1Name, "Current Mode: " + testOPM_1.Mode.ToString());
            TestOutput(instr2Name, "Current Mode: " + testOPM_2.Mode.ToString());

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            Assert.AreEqual(InstType_OpticalPowerMeter.MeterMode.Absolute_dBm, testOPM_1.Mode);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            Assert.AreEqual(InstType_OpticalPowerMeter.MeterMode.Absolute_mW, testOPM_1.Mode);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            Assert.AreEqual(InstType_OpticalPowerMeter.MeterMode.Relative_dB, testOPM_1.Mode);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            Assert.AreEqual(InstType_OpticalPowerMeter.MeterMode.Absolute_dBm, testOPM_1.Mode);
        }

        [Test]
        public void T03_MeterSetup()
        {
            TestOutput("\n\n*** T03_MeterSetup ***");
            TestOutput(instr1Name, "Meter Wavelength(nm): " + testOPM_1.Wavelength_nm.ToString());
            TestOutput(instr2Name, "Meter Wavelength(nm): " + testOPM_2.Wavelength_nm.ToString());
            testOPM_1.Wavelength_nm = 1560;
            Assert.AreEqual(1560, testOPM_1.Wavelength_nm, 0.1);
            testOPM_1.Wavelength_nm = 1550;
            Assert.AreEqual(1550, testOPM_1.Wavelength_nm, 0.1);
            
            TestOutput(instr1Name, "Cal Offset(dB): " + testOPM_1.CalOffset_dB.ToString());
            TestOutput(instr2Name, "Cal Offset(dB): " + testOPM_2.CalOffset_dB.ToString());
            testOPM_1.CalOffset_dB = 0.3;
            Assert.AreEqual(0.3, testOPM_1.CalOffset_dB, 0.01);
            testOPM_1.CalOffset_dB = 0.0;
            Assert.AreEqual(0.0, testOPM_1.CalOffset_dB, 0.01);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            TestOutput(instr1Name, "Ref Power (dBm): " + testOPM_1.ReferencePower.ToString());
            TestOutput(instr2Name, "Ref Power (dBm): " + testOPM_2.ReferencePower.ToString());
            testOPM_1.ReferencePower = -3;
            Assert.AreEqual(-3, testOPM_1.ReferencePower, 0.01);
            testOPM_1.ReferencePower = 0;
            Assert.AreEqual(0, testOPM_1.ReferencePower, 0.01);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            TestOutput(instr1Name, "Ref Power (dBm): " + testOPM_1.ReferencePower.ToString());
            TestOutput(instr2Name, "Ref Power (dBm): " + testOPM_2.ReferencePower.ToString());
            testOPM_1.ReferencePower = -3;
            Assert.AreEqual(-3, testOPM_1.ReferencePower, 0.01);
            testOPM_1.ReferencePower = 0;
            Assert.AreEqual(0, testOPM_1.ReferencePower, 0.01);

            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            TestOutput(instr1Name, "Ref Power (mW): " + testOPM_1.ReferencePower.ToString());
            TestOutput(instr2Name, "Ref Power (mW): " + testOPM_2.ReferencePower.ToString());
            testOPM_1.ReferencePower = 0.1;
            Assert.AreEqual(0.1, testOPM_1.ReferencePower, 0.001);
            testOPM_1.ReferencePower = 1;
            Assert.AreEqual(1, testOPM_1.ReferencePower, 0.001);

            // reset to default
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            testOPM_1.ReferencePower = 0;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            testOPM_2.ReferencePower = 0;

            TestOutput(instr1Name, "Averaging time (s): " + testOPM_1.AveragingTime_s.ToString());
            TestOutput(instr2Name, "Averaging time (s): " + testOPM_2.AveragingTime_s.ToString());
            testOPM_1.AveragingTime_s = 0.1;
            Assert.AreEqual(0.1, testOPM_1.AveragingTime_s, 0.001);
            testOPM_1.AveragingTime_s = 1;
            Assert.AreEqual(1, testOPM_1.AveragingTime_s, 0.001);            
        }

        [Test]
        public void T04_MeterRange()
        {

            TestOutput("\n\n*** T04_MeterRange ***");
            // read the maximum power range allowable - from Hardware data
            string maxPowerStr = testOPM_1.HardwareData["MaximumPwrRangeDbm"];
            double maxPower = Convert.ToDouble(maxPowerStr);
            double minPower = -60;

            TestOutput("*** ABSOLUTE_DBM ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            
            for (double val = minPower; val < maxPower + 1; val += 5)
            {
                powRange(val, false);
            }

            TestOutput("*** RELATIVE_DB ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            
            for (double val = minPower; val < maxPower + 1; val += 5)
            {
                powRange(val, false);
            }

            TestOutput("*** ABSOLUTE_mW ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
            TestOutput(instr1Name, "Meter Range: " + testOPM_1.Range.ToString());
            
            for (double val = minPower; val < maxPower + 1; val += 5)
            {
                powRange(val, true);
            }

            // reset
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
        }

        [Test]
        public void T05_TakeReadings()
        {
            TestOutput("\n\n*** T05_TakeReadings ***");
            // reset
            testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
            testOPM_2.Range = InstType_OpticalPowerMeter.AutoRange;
            int ii;
            int count = 10;

            TestOutput("*** ABSOLUTE_DBM ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            for (ii = 0; ii < count; ii++)
            {
                TestOutput(instr1Name, "Power: " + testOPM_1.ReadPower().ToString());
                TestOutput(instr2Name, "Power: " + testOPM_2.ReadPower().ToString());
            }

            TestOutput("*** ABSOLUTE_mW ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
            for (ii = 0; ii < count; ii++)
            {
                TestOutput(instr1Name, "Power: " + testOPM_1.ReadPower().ToString());
                TestOutput(instr2Name, "Power: " + testOPM_2.ReadPower().ToString());
            }

            TestOutput("*** RELATIVE_dB ***");
            testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Relative_dB;
            for (ii = 0; ii < count; ii++)
            {
                TestOutput(instr1Name, "Power: " + testOPM_1.ReadPower().ToString());
                TestOutput(instr2Name, "Power: " + testOPM_2.ReadPower().ToString());
            }
        }

        [Test]
        public void T06_Zeroing()
        {
            TestOutput("\n\n*** T06_Zeroing ***");
            TestOutput("Starting zeroing...");
            testOPM_1.ZeroDarkCurrent_Start();            
            testOPM_2.ZeroDarkCurrent_Start();
            TestOutput("Waiting...");
            testOPM_1.ZeroDarkCurrent_End();
            testOPM_2.ZeroDarkCurrent_End();
            TestOutput("Zeroing complete...");            
        }

        [Test]
        public void T07_DefaultState()
        {
            TestOutput("\n\n*** T07_DefaultState ***");
            testOPM_1.SetDefaultState();
            testOPM_2.SetDefaultState();
        }

        [Test]
        public void T08_TimingTest()
        {
            TestOutput("\n\n*** T08_TimingTest ***");
            int loopIterations = 3;
            int readsPerLoop = 10;
            TestOutput("Starting timing test in safe mode");
            testOPM_1.SafeMode = true;
            testOPM_2.SafeMode = true;
            DateTime startSafe = DateTime.Now;
            this.timingTest(loopIterations, readsPerLoop);
            TimeSpan safeTime = DateTime.Now - startSafe;
            TestOutput("Test took: " + safeTime.ToString());

            TestOutput("Starting timing test in unsafe mode");
            testOPM_1.SafeMode = false;
            testOPM_2.SafeMode = false;
            DateTime startUnSafe = DateTime.Now;
            this.timingTest(loopIterations, readsPerLoop);
            TimeSpan unSafeTime = DateTime.Now - startUnSafe;
            TestOutput("Test took: " + unSafeTime.ToString());
            
        }

        #region Private helper fns
        private void powRange(double value, bool convertToDbm)
        {
            double setValue_unitCorrected;
            if (convertToDbm)
            {
                setValue_unitCorrected = InstType_OpticalPowerMeter.Convert_dBmtomW(value);
            }
            else setValue_unitCorrected = value;

            TestOutput(instr1Name, "Setting range to: " + setValue_unitCorrected.ToString());
            testOPM_1.Range = setValue_unitCorrected;
            
            TestOutput(instr1Name, "Meter Range is: " + testOPM_1.Range.ToString());            
        }

        private void timingTest(int iterations, int nbrReads)
        {
            int ii, jj;
            for (ii = 0; ii<iterations; ii++)
            {
                TestOutput(String.Format("LOOP {0} OF {1}", ii+1, iterations));
                TestOutput("Setup operations");
                testOPM_1.SetDefaultState();
                testOPM_2.SetDefaultState();
                testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                testOPM_1.Wavelength_nm = 1520.0;
                testOPM_1.Range = -30;
                testOPM_1.Range = InstType_OpticalPowerMeter.AutoRange;
                testOPM_1.AveragingTime_s = 0.01;
                
                TestOutput("Reads in dBm");               
                for (jj = 0; jj < nbrReads / 2; jj++)
                {
                    double val1 = testOPM_1.ReadPower();
                    double val2 = testOPM_2.ReadPower();
                }

                testOPM_1.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_mW;
                testOPM_2.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                testOPM_1.Wavelength_nm = 1570.0;
                TestOutput("Reads in mW");
                for (jj = 0; jj < nbrReads / 2; jj++)
                {
                    double val1 = testOPM_1.ReadPower();
                    double val2 = testOPM_2.ReadPower();
                }
            }
            

        }

        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Chassis_816x_Test
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object_Atten
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_Ag816x testChassis;
        private Inst_Ag81561A_Attenuator testAttn;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::20::INSTR";
            //"//cas-w-182/GPIB0::20::INSTR"; 
            //"//pai-tx-labj1/GPIB1::20::INSTR"; 
        const string chassisName = "816x Chassis";
        const string instr1Name = "Attn1";
        Random rnd = new Random();
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_Ag816x(chassisName, "Chassis_Ag816x", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testAttn = new Inst_Ag81561A_Attenuator(instr1Name,
                "Inst_Ag81561A_Attenuator", "1", "", testChassis);
            TestOutput(instr1Name, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testAttn.IsOnline = true;
            TestOutput(instr1Name, "IsOnline set true OK");
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(chassisName, "HW: " + testChassis.HardwareIdentity);
            TestOutput(chassisName, "FW: " + testChassis.FirmwareVersion);
            TestOutput(instr1Name, "Driver: " + testAttn.DriverName + ":" + testAttn.DriverVersion);
            TestOutput(instr1Name, "HW: " + testAttn.HardwareIdentity);
            TestOutput(instr1Name, "FW: " + testAttn.FirmwareVersion);
        }

        [Test]
        public void T02_SetAttenuation()
        {
            TestOutput("\n\n**** T02_SetAttenuation ***");
            double initialAttn_dB = testAttn.Attenuation_dB;
            TestOutput(instr1Name, "InitialAttn: " + initialAttn_dB + " dB");
            double newAttn_dB = rnd.NextDouble() * 5;
            TestOutput(instr1Name, "Setting NewAttn: " + newAttn_dB + " dB");
            testAttn.Attenuation_dB = newAttn_dB;
            TestOutput(instr1Name, "Done.");
            double verifyAttn_dB = testAttn.Attenuation_dB;
            TestOutput(instr1Name, "VerifyAttn: " + verifyAttn_dB + " dB");
            TestOutput(instr1Name, "Setting back to zero.");
            testAttn.Attenuation_dB = 0.0;
            double zeroVerifyAttn_dB = testAttn.Attenuation_dB;
            TestOutput(instr1Name, "ZeroVerifyAttn: " + zeroVerifyAttn_dB + " dB");
        }

        [Test]
        public void T03_SetWaveLength()
        {
            TestOutput("\n\n**** T03_SetWaveLength ***");
            double initialWave_nm = testAttn.Wavelength_nm;
            TestOutput(instr1Name, "Initial Wavelength: " + initialWave_nm + " nm");
            double newWave_nm = rnd.Next(1500, 1600);
            TestOutput(instr1Name, "Setting new Wavelength: " + newWave_nm + " nm");
            testAttn.Wavelength_nm = newWave_nm;
            TestOutput(instr1Name, "Done.");
            double verifyWave_nm = testAttn.Wavelength_nm;
            TestOutput(instr1Name, "Verify Wavelength: " + verifyWave_nm + " nm");
            TestOutput(instr1Name, "Restoring initial: " + initialWave_nm + " nm");
            testAttn.Wavelength_nm = initialWave_nm;
            double verifyInital_nm = testAttn.Wavelength_nm;
            TestOutput(instr1Name, "VerifyInitial: " + verifyInital_nm + " nm");
        }

        [Test]
        public void T04_SetOutputEnabled()
        {
            TestOutput("\n\n**** T04_SetOutputEnabled ***");
            bool initialValue = testAttn.OutputEnabled;
            TestOutput(instr1Name, "Initial value: " + initialValue);
            bool newValue = !initialValue;
            TestOutput(instr1Name, "Setting new value: " + newValue);
            testAttn.OutputEnabled = newValue;
            TestOutput(instr1Name, "Done.");
            TestOutput(instr1Name, "Verifying: " + testAttn.OutputEnabled);
            TestOutput(instr1Name, "Reverting...");
            testAttn.OutputEnabled = initialValue;
            TestOutput(instr1Name, "Done. Confirming: " + testAttn.OutputEnabled);
        }

        [Test]
        public void T05_SetCalibrationFactor()
        {
            TestOutput("\n\n**** T05_SetCalibrationFactor ***");
            double iniVal_dB = testAttn.CalibrationFactor_dB;
            TestOutput(instr1Name, "Initial value: " + iniVal_dB);

            double newVal_dB = rnd.NextDouble() * 5 + 1;
            TestOutput(instr1Name, "Setting new value: " + newVal_dB);
            testAttn.CalibrationFactor_dB = newVal_dB;
            TestOutput(instr1Name, "Verifying: " + testAttn.CalibrationFactor_dB);

            TestOutput(instr1Name, "Restoring.");
            testAttn.CalibrationFactor_dB = iniVal_dB;
            TestOutput(instr1Name, "Confirm: " + testAttn.CalibrationFactor_dB);
            
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

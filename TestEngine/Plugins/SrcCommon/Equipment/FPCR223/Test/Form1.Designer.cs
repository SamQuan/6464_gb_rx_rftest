namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSet = new System.Windows.Forms.Button();
            this.labelGPIBAdd = new System.Windows.Forms.Label();
            this.labelChannel = new System.Windows.Forms.Label();
            this.labelVoltage = new System.Windows.Forms.Label();
            this.numericUpDownAddress = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownChannel = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownVoltage = new System.Windows.Forms.NumericUpDown();
            this.buttonInit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVoltage)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSet
            // 
            this.buttonSet.Location = new System.Drawing.Point(149, 208);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(87, 32);
            this.buttonSet.TabIndex = 0;
            this.buttonSet.Text = "Set Voltage";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // labelGPIBAdd
            // 
            this.labelGPIBAdd.Location = new System.Drawing.Point(12, 60);
            this.labelGPIBAdd.Name = "labelGPIBAdd";
            this.labelGPIBAdd.Size = new System.Drawing.Size(88, 19);
            this.labelGPIBAdd.TabIndex = 1;
            this.labelGPIBAdd.Text = "GPIB Address";
            // 
            // labelChannel
            // 
            this.labelChannel.Location = new System.Drawing.Point(12, 104);
            this.labelChannel.Name = "labelChannel";
            this.labelChannel.Size = new System.Drawing.Size(88, 19);
            this.labelChannel.TabIndex = 2;
            this.labelChannel.Text = "Channel Num";
            // 
            // labelVoltage
            // 
            this.labelVoltage.Location = new System.Drawing.Point(12, 148);
            this.labelVoltage.Name = "labelVoltage";
            this.labelVoltage.Size = new System.Drawing.Size(88, 19);
            this.labelVoltage.TabIndex = 3;
            this.labelVoltage.Text = "Voltage Value";
            // 
            // numericUpDownAddress
            // 
            this.numericUpDownAddress.Location = new System.Drawing.Point(116, 58);
            this.numericUpDownAddress.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownAddress.Name = "numericUpDownAddress";
            this.numericUpDownAddress.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownAddress.TabIndex = 4;
            this.numericUpDownAddress.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // numericUpDownChannel
            // 
            this.numericUpDownChannel.Location = new System.Drawing.Point(116, 102);
            this.numericUpDownChannel.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownChannel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownChannel.Name = "numericUpDownChannel";
            this.numericUpDownChannel.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownChannel.TabIndex = 5;
            this.numericUpDownChannel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownVoltage
            // 
            this.numericUpDownVoltage.Location = new System.Drawing.Point(116, 146);
            this.numericUpDownVoltage.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownVoltage.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownVoltage.Name = "numericUpDownVoltage";
            this.numericUpDownVoltage.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownVoltage.TabIndex = 6;
            this.numericUpDownVoltage.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // buttonInit
            // 
            this.buttonInit.Location = new System.Drawing.Point(15, 208);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(87, 32);
            this.buttonInit.TabIndex = 7;
            this.buttonInit.Text = "Initialise";
            this.buttonInit.UseVisualStyleBackColor = true;
            this.buttonInit.Click += new System.EventHandler(this.buttonInit_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(266, 285);
            this.Controls.Add(this.buttonInit);
            this.Controls.Add(this.numericUpDownVoltage);
            this.Controls.Add(this.numericUpDownChannel);
            this.Controls.Add(this.numericUpDownAddress);
            this.Controls.Add(this.labelVoltage);
            this.Controls.Add(this.labelChannel);
            this.Controls.Add(this.labelGPIBAdd);
            this.Controls.Add(this.buttonSet);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVoltage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Label labelGPIBAdd;
        private System.Windows.Forms.Label labelChannel;
        private System.Windows.Forms.Label labelVoltage;
        private System.Windows.Forms.NumericUpDown numericUpDownAddress;
        private System.Windows.Forms.NumericUpDown numericUpDownChannel;
        private System.Windows.Forms.NumericUpDown numericUpDownVoltage;
        private System.Windows.Forms.Button buttonInit;
    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonInit_Click(object sender, EventArgs e)
        {
            string resources = "GPIB0::"+ numericUpDownAddress.Value.ToString() + "::INSTR";
            chassisFPCR223 = new Chassis_FPCR223("ChassisTest", "Chassis_FPCR223", resources);
            instFPCR223 = new Inst_FPCR223("InstTest", "Inst_FPCR223", "", "", chassisFPCR223);
            try
            {
                chassisFPCR223.IsOnline = true;
                instFPCR223.IsOnline = true;
                instFPCR223.SetDefaultState();
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Initialise Instrument Error! \n" + ex.ToString());
            }
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            int channelNo = (int)numericUpDownChannel.Value;
            double voltageValue = (double)numericUpDownVoltage.Value;

            try
            {
                instFPCR223.SetVoltage(channelNo, voltageValue);
                //System.Threading.Thread.Sleep(1000);
                //instFPCR223.SetVoltage(channelNo, voltageValue);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Set Voltage Error! \n" + ex.ToString());
            }
        }

        private Chassis_FPCR223 chassisFPCR223;
        private Inst_FPCR223 instFPCR223;
    }
}
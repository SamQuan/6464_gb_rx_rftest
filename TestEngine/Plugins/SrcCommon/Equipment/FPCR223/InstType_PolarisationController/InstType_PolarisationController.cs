// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// InstType_PolarisationController.cs
//
// Author: Alexander Wu
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.InstrTypes
{
    /// <summary>
    /// Digital IO instrument type 
    /// </summary>
    public abstract class InstType_PolarisationController : Instrument
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public InstType_PolarisationController(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
        }

        /// <summary>
        /// Set voltage for the specific cell
        /// </summary>
        /// <param name="channelNum"></param>
        public abstract void ReadVoltage(int channelNum);

        /// <summary>
        /// Read voltage of the specific cell
        /// </summary>
        /// <param name="channelNum"></param>
        /// <param name="voltageVal_V"></param>
        public abstract void SetVoltage(int channelNum, double voltageVal_V);
    }

}


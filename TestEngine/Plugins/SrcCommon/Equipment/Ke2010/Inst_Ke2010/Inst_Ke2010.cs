// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ke2010.cs
//
// Author: alice.huang, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
    ///<summary >
    /// Ke2010 instrument
    /// </summary>
    public class Inst_Ke2010 : InstType_MultiMeter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slot">Slot ID for the instrument</param>
        /// <param name="subSlot">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ke2010(string instrumentName, string driverName,
            string slot, string subSlot, Chassis chassis)
            : base(instrumentName, driverName, slot, subSlot,
            chassis)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord Ke2010Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2010",				// hardware name 
                "0",  			// minimum valid firmware version 
                "A20");			// maximum valid firmware version 
            ValidHardwareData.Add("Ke2010", Ke2010Data);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord Ke2010ChassisData = new InstrumentDataRecord(
                "Chassis_Ke2010",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ke2010", Ke2010ChassisData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (ChassisType_Visa)base.InstrumentChassis;
        }
        #endregion

        #region Define multimeter interface for inst_Ke2010

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                return idn[3].Substring(0, 3);
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the full IDN? string which has 4 comma seperated substrings
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');

                // Build the string from 2 substrings, the manufacturer name & the model number
                string hid = idn[0].Trim() + " " + idn[1].Trim();

                // Log event 
                LogEvent("'HardwareIdentity' returned : " + hid);

                return hid;

            }
        }

        /// <summary>
        /// Set instrument to default state by sending command ":SYSTEM:PRESet"
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write_Unchecked("*RST", this);
            instrumentChassis.Write_Unchecked("*WAI", this);
            this.Write(":SYSTEM:PRESet");
        }

        /// <summary>
        /// get tha AC measurement type
        /// </summary>
        public override MeterAcType AcType
        {
            get { return MeterAcType.True_RMS; }
        }

        /// <summary>
        /// get/set the multimeter fuction range. "0" for autorange
        /// Parameters  = 0 to 3.1 Expected reading is amps (ACI and DCI)
        /// 0 to 757.5 Expected reading is AC volts (ACV)
        /// 0 to 1010 Expected reading in DC volts (DCV)
        /// 0 to 10.1 Expected reading in DC volts (DCV) for sense terminals
        /// 0 to 120e6 Expected reading is ohms (W2 and W4)
        /// DEFault 3.03 (ACI and DCI)
        /// 757.5 (ACV)
        /// 1010 (DCV)
        /// 101e6 (W2 and W4)
        /// MINimum 0 (All functions)
        /// MAXimum Same as DEFault
        /// </summary>
        public override double MeterRange
        {
            get
            {
                string queryResult = "";
                string commandString = ":VOLT:AC:RANG?";
                switch (this.MultiMeterMode)
                {
                    case MeterMode.Current_AC_A_rms:
                        queryResult = instrumentChassis.Query_Unchecked(":CURR:AC:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString=":CURR:AC:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":CURR:AC:RANG?";
                        }
                        break;
                    case MeterMode.Current_DC_A:
                        queryResult = instrumentChassis.Query_Unchecked(":CURR:DC:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString = ":CURR:DC:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":CURR:DC:RANG?";
                        }
                        break;
                    case MeterMode.Resistance2wire_ohms:
                        queryResult = instrumentChassis.Query_Unchecked(":RES:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString = ":RES:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":RES:RANG?";
                        }
                        break;
                    case MeterMode.Resistance4wire_ohms:
                        queryResult = instrumentChassis.Query_Unchecked(":FRES:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString = ":FRES:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":FRES:RANG?";
                        }
                        break;
                    case MeterMode.Voltage_AC_V_rms:
                        queryResult = instrumentChassis.Query_Unchecked(":VOLT:AC:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString = ":VOLT:AC:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":VOLT:AC:RANG?";
                        }
                        break;
                    case MeterMode.Voltage_DC_V:
                        queryResult = instrumentChassis.Query_Unchecked(":VOLT:DC:RANG:AUTO?", null);
                        if (queryResult == "1")
                        {
                            //commandString = ":VOLT:DC:RANG:AUTO:ULIM?";
                            return InstType_MultiMeter.AutoRange;
                        }
                        else
                        {
                            commandString = ":VOLT:DC:RANG?";
                        }
                        break;
                }
                return double.Parse(instrumentChassis.Query_Unchecked(commandString, null));
            }
            set
            {

                string commandString = ":VOLT:AC:RANG:AUTO ON";
                switch (this.MultiMeterMode)
                {
                    case MeterMode.Current_AC_A_rms:
                        if (value == 0)
                        {
                            commandString = ":CURR:AC:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":CURR:AC:RANG " + value.ToString();
                        }
                        break;
                    case MeterMode.Current_DC_A:
                        if (value == 0)
                        {
                            commandString = ":CURR:DC:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":CURR:DC:RANG " + value.ToString();
                        }
                        break;
                    case MeterMode.Resistance2wire_ohms:
                        if (value == 0)
                        {
                            commandString = ":RES:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":RES:RANG " + value.ToString();
                        }
                        break;
                    case MeterMode.Resistance4wire_ohms:
                        if (value == 0)
                        {
                            commandString = ":FRES:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":FRES:RANG " + value.ToString();
                        }
                        break;
                    case MeterMode.Voltage_AC_V_rms:
                        if (value == 0)
                        {
                            commandString = ":VOLT:AC:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":VOLT:AC:RANG " + value.ToString();
                        }
                        break;
                    case MeterMode.Voltage_DC_V:
                        if (value == 0)
                        {
                            //":VOLT:DC:RANG:AUTO "
                            commandString = ":VOLT:DC:RANG:AUTO ON";
                        }
                        else
                        {
                            commandString = ":VOLT:DC:RANG " + value.ToString();
                        }
                        break;
                }
                instrumentChassis.Write_Unchecked(commandString, null);
            }
        }

        /// <summary>
        /// get/set multimeter function type;InstType_MultiMeter.MeterMode available only
        /// </summary>
        public override InstType_MultiMeter.MeterMode MultiMeterMode
        {
            get
            {
                MeterMode meterMode = MeterMode.Voltage_DC_V;
                string currentFunction = instrumentChassis.Query_Unchecked(":SENS:FUNC?", null).ToUpper();
                switch (currentFunction)
                {
                    case "\"VOLT:DC\"":
                        meterMode = MeterMode.Voltage_DC_V;
                        break;
                    case "\"VOLT:AC\"":
                        meterMode = MeterMode.Voltage_AC_V_rms;
                        break;
                    case "\"CURR:DC\"":
                        meterMode = MeterMode.Current_DC_A;
                        break;
                    case "\"CURR:AC\"":
                        meterMode = MeterMode.Current_AC_A_rms;
                        break;
                    case "\"RES\"":
                        meterMode = MeterMode.Resistance2wire_ohms;
                        break;
                    case "\"FRES\"":
                        meterMode = MeterMode.Resistance4wire_ohms;
                        break;

                    default:
                        throw new Exception("Unknown MultiMeter Mode");
                }
                return meterMode;
            }
            set
            {
                string commandString = "";
                switch (value)
                {
                    case MeterMode.Current_AC_A_rms:
                        commandString = ":SENS:FUNC 'CURR:AC'";
                        break;
                    case MeterMode.Current_DC_A:
                        commandString = ":SENS:FUNC 'CURR:DC'";
                        break;
                    case MeterMode.Resistance2wire_ohms:
                        commandString = ":SENS:FUNC 'RES'";
                        break;
                    case MeterMode.Resistance4wire_ohms:
                        commandString = ":SENS:FUNC 'FRES'";
                        break;
                    case MeterMode.Voltage_AC_V_rms:
                        commandString = ":SENS:FUNC 'VOLT:AC'";
                        break;
                    case MeterMode.Voltage_DC_V:
                        //strCommand = ":VOLT:DC:RANG " + strRangeSetting;
                        commandString = ":SENS:FUNC 'VOLT:DC'";
                        break;
                    default:
                        return;
                }
                
                instrumentChassis.Write_Unchecked(commandString, null);
            }
        }

        /// <summary>
        /// initiate a measure
        /// </summary>
        /// <returns>return a measurement reading </returns>
        public override double GetReading()
        {
            string commandString = "READ?";
            string reply = instrumentChassis.Query_Unchecked(commandString, null);
            //reply = reply.Substring(0, reply.IndexOf(",") - 4);
            while (!char.IsDigit(reply[reply.Length - 1]))
            {
                reply = reply.Substring(0, reply.Length - 1);
            }
            return double.Parse(reply);
        }

        /// <summary>
        /// the instrument does not contain this function
        /// </summary>
        public override double IntegrationTime_s
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

        #region public functions

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetMultiElementReading()
        {
            string commandString = "READ?";
            string reply = instrumentChassis.Query_Unchecked(commandString, null);
            //reply = reply.Substring(0, reply.IndexOf(",") - 4);
            //while (!char.IsDigit(reply[reply.Length - 1]))
            //{
            //    reply = reply.Substring(0, reply.Length - 1);
            //}
            return reply;
        }

        /// <summary>
        /// Clears all event registers and Error Queue
        /// </summary>
        public void Clear()
        {
            instrumentChassis.Write_Unchecked("*CLS", this);
        }

        /// <summary>
        /// Take 2010 out of idle state and initiate a trigger
        /// </summary>
        public void InitTrigger()
        {
            this.Write(":INIT");
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ContinuousInitiation
        {
            get
            {
                string results = instrumentChassis.Query_Unchecked(":INITiate:CONTinuous?", this);

                if (results == "1")
                {
                    return true;
                }
                else if (results == "0")
                {
                    return false;
                }
                else
                {
                    throw new InstrumentException("Trigger Continuous Initiation Status Error!!!");
                }
            }
            set
            {
                string command = ":INIT:CONT " + (value ? "ON" : "OFF");
                //send(":INIT:CONT ON");
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// clear buffer 
        /// </summary>
        public void BufferClearing()
        {
            this.Write(":TRAC:CLEAR");
        }

        /// <summary>
        /// Set/Get Buffer size . the value might set between 2~1024
        /// </summary>
        public double BufferSize
        {
            get
            {
                //string commandString = ":TRAC:POIN:AUTO?";

                //string rtn = this.Query(commandString);

                //if (int.Parse(rtn) == 1)
                //    return InstType_MultiMeter.AutoRange ;
                //else
                //{
                string commandString = ":TRAC:POIN?";
                string rtn = this.Query(commandString);

                return double.Parse(rtn);
                //}
            }
            set
            {
                string commandString = "";
                //if (value == 0)
                //    commandString = ":TRAC:POIN:AUTO ON";
                //else
                //{
                if (value < 2) value = 2;
                else if (value > 1024) value = 1024;
                //strCommand = ":TRAC:POIN " + utils::lexical_cast<std::string>(nNumberOfPoints);
                commandString = ":TRAC:POIN " + value.ToString();
                this.Write(commandString);
                //}
            }
        }

        /// <summary>
        /// get/set buffer control type, "NEV" , "NEXT" AVAILABLE ONLY
        /// </summary>
        public BufferControlMode BufferControl
        {
            get
            {
                string rtn = this.Query(":TRAC:FEED:CONT?");

                if (rtn.Equals("NEV"))
                    return BufferControlMode.NEVER;
                if (rtn.Equals("NEXT"))
                    return BufferControlMode.NEXT;
                //if (rtn.Equals("ALW"))
                //    return BufferControlMode.ALWAYS;
                //if (rtn.Equals("PRET"))
                //    return BufferControlMode.PRETRIGGER;
                throw new Exception("Unknow buffer control mode!");
            }
            set
            {
                string commandString = ":TRAC:FEED:CONT ";
                //send(":TRAC:FEED:CONT NEV");
                switch (value)
                {
                    case BufferControlMode.NEVER:
                        commandString += "NEV";
                        break;
                    case BufferControlMode.NEXT:
                        commandString += "NEXT";
                        break;
                    default:
                        break;
                }
                this.Write(commandString);
            }
        }

        /// <summary>
        /// select the source of readings to be placed in the buffer
        /// </summary>
        public BufferFeedMode BufferFeed
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRACe:FEED?", this);

                rtn = rtn.ToUpper();

                BufferFeedMode results = BufferFeedMode.NONE;

                switch (rtn.Substring(0, 4))
                {
                    case "SENS":
                        results = BufferFeedMode.SENS;
                        break;
                    case "CALC":
                        results = BufferFeedMode.CALC;
                        break;
                    case "NONE":
                        results = BufferFeedMode.NONE;
                        break;
                }

                return results;
            }
            set
            {
                instrumentChassis.Write_Unchecked(":TRACe:FEED " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Read status of memory
        /// </summary>
        public string BufferStatus
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRACe:FREE?", this);

                string[] results = rtn.Split(',');

                return results[0] + "bytes of memory are available; " + results[1] + "bytes are reserved to store readings.";
            }
        }

        /// <summary>
        /// Maxchan=10
        /// </summary>
        public int ChanMax = 10;

        /// <summary>
        /// MinChan=1
        /// </summary>
        public int ChanMin = 1;

        private int chanToBeHandle;

        /// <summary>
        /// Set/Get the channel to/under handle
        /// </summary>
        public int Channel
        {
            get
            {
                return chanToBeHandle;
            }
            set
            {
                if (value < ChanMin) value = ChanMin;
                if (value > ChanMax) value = ChanMax;

                chanToBeHandle = value;

            }
        }

        /// <summary>
        /// 1       for close
        /// 0       for open
        /// other   for ERROR
        /// </summary>
        public int ChannelStatus
        {
            get
            {
                int status = 0;
                string rtn = this.Query(":ROUT:MULT:CLOS:STAT?");// (@" + Channel.ToString() + ")");
                int Ind_start = rtn.IndexOf('@') + 1;
                int Ind_end = rtn.IndexOf(')');
                rtn = rtn.Substring(Ind_start, Ind_end - Ind_start);

                string[] Chans = rtn.Split(',');
                if (Chans.Length == 1)// if only one channel is closed check if the channel is the specified or not
                {
                    if (int.Parse(Chans[0]) != Channel) return status;
                    else { status = 1; return status; }
                }
                else // if more than one channels are closed, look up the list for the specified channel
                {
                    for (int i = Chans.GetLowerBound(0); i <= Chans.GetUpperBound(0); i++)
                    {
                        if (int.Parse(Chans[i]) == Channel) { status = 1; return status; }
                    }
                }
                return status;
            }
            set
            {
                string commandString = "";
                if (value == 1)
                {
                    commandString = ":ROUT:CLOS (@" + Channel.ToString() + ")";
                }
                if (value == 0)
                {
                    commandString = "ROUT:MULT:OPEN (@" + Channel.ToString() + ")";
                }

                this.Write(commandString);
            }
        }

        /// <summary>
        /// ???
        /// </summary>
        public string PreStoredChannelStatus
        {
            set
            {
                instrumentChassis.Write_Unchecked(":ROUT:MULT:CLOS " + "(@" + value + ")", this);
            }
        }

        /// <summary>
        /// Define internal scan list and enable scan
        /// The format of the list is "firstChannel:secondChannel"
        /// </summary>
        /// <param name="firstChannel"></param>
        /// <param name="secondChannel"></param>
        public void SetRounteScanChannel(int firstChannel, int secondChannel)
        {
            if ((firstChannel > secondChannel) || (firstChannel == secondChannel))
            {
                throw new InstrumentException("Set_Route_Scan_Channel - Invalid Channel Assignment");
            }
            if ((firstChannel < 1) || (firstChannel > 9))
            {
                throw new InstrumentException("Set_Route_Scan_Channel - First Channel Out Of Range");
            }
            if ((secondChannel < 2) || (secondChannel > 10))
            {
                throw new InstrumentException("Set_Route_Scan_Channel - Second Channel Out Of Range");
            }
            //":ROUT:SCAN (@" +
            string command = ":ROUT:SCAN (@" + firstChannel.ToString() + ":" + secondChannel.ToString() + ")";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Perform specified scan operation
        /// </summary>
        public ScanOperationType ScanOperation
        {
            get
            {
                string results = instrumentChassis.Query_Unchecked(":ROUTe:SCAN:LSELect?", this);
                results = results.ToUpper();

                ScanOperationType op;

                switch (results)
                {
                    case "INTERNAL":
                        op = ScanOperationType.INTERNAL;
                        break;
                    case "ENTERNAL":
                        op = ScanOperationType.EXTERNAL;
                        break;
                    case "NONE":
                        op = ScanOperationType.NONE;
                        break;
                    default:
                        op = ScanOperationType.NONE;
                        break;
                }

                return op;
            }
            set
            {
                string command = "ROUT:SCAN:LSEL ";
                //strCommand("ROUT:SCAN:LSEL ");
                switch (value)
                {
                    case ScanOperationType.INTERNAL:
                        command += "INT";
                        break;
                    case ScanOperationType.EXTERNAL:
                        command += "EXT";
                        break;
                    case ScanOperationType.NONE:
                        command += "NONE";
                        break;
                    default:
                        break;
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="measState"></param>
        /// <param name="state"></param>
        public void SetSenseFilter(MeasurementType measState, bool state)
        {
            string command = "";

            switch (measState)
            {
                case MeasurementType.CurrentAC:
                    //":CURR:AC:AVER:STAT ";
                    command = ":CURR:AC:AVER:STAT ";
                    break;
                case MeasurementType.CurrentDC:
                    command = ":CURR:DC:AVER:STAT ";
                    break;
                case MeasurementType.VoltageAC:
                    command = ":VOLT:AC:AVER:STAT ";
                    break;
                case MeasurementType.VoltageDC:
                    command = ":VOLT:DC:AVER:STAT ";
                    break;
                case MeasurementType.Resistance_2Wire:
                    command = "RES:AVER:STAT ";
                    break;
                case MeasurementType.Resistance_4Wire:
                    command = "FRES:AVER:STAT ";
                    break;
                case MeasurementType.Temperature:
                    command = "TEMP:AVER:STAT ";
                    break;
                case MeasurementType.Period:
                case MeasurementType.Frequency:
                case MeasurementType.Diode:
                case MeasurementType.Continuity:
                default:
                    throw new InstrumentException("Set_Sense_Filter - Invalid Measurement Type Specified");
            }
            command += state ? "1" : "0";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// 
        /// </summary>
        public MeasurementType SenseFunction
        {
            set
            {
                string command = "";

                switch (value)
                {
                    case MeasurementType.CurrentAC:
                        command = ":FUNC 'CURR:AC'";
                        break;
                    case MeasurementType.CurrentDC:
                        command = ":FUNC 'CURR:DC'";
                        break;
                    case MeasurementType.VoltageAC:
                        command = ":FUNC 'VOLT:AC'";
                        break;
                    case MeasurementType.VoltageDC:
                        command = ":FUNC 'VOLT:DC'";
                        break;
                    case MeasurementType.Resistance_2Wire:
                        command = ":FUNC 'RES'";
                        break;
                    case MeasurementType.Resistance_4Wire:
                        command = ":FUNC 'FRES'";
                        break;
                    case MeasurementType.Period:
                        command = ":FUNC 'PER'";
                        break;
                    case MeasurementType.Frequency:
                        command = ":FUNC 'FREQ'";
                        break;
                    case MeasurementType.Temperature:
                        command = ":FUNC 'TEMP'";
                        break;
                    case MeasurementType.Diode:
                        command = ":FUNC 'DIOD'";
                        break;
                    case MeasurementType.Continuity:
                        command = ":FUNC 'CONT'";
                        break;
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number">
        /// Service Request Register
        /// 0	Clear Enable register
        /// 1	MSB				B0
        /// 2	NOT USED		B1
        /// 4	EAV				B2
        /// 8	QSB				B3
        /// 16	MAV				B4
        /// 32	ESB				B5
        /// 64	NOT USED		B6
        /// 128	OSB				B7
        /// 255	ALL BITS SET
        ///</param>
        public void SetServiceRequestRegisters(int number)
        {
            if ((number >= 0) && (number <= 255))
            {
                // Valid Register Value Supplied
                string command;

                command = ":*SRE " + number.ToString();

                instrumentChassis.Write_Unchecked(command, this);
            }
            else
            {
                throw new InstrumentException("Set_ServiceRequestRegisters - Invalid Register Value Supplied");
            }
        }



        /// <summary>
        /// close chan and make a meassure
        /// </summary>
        /// <param name="chan">specify a channel to close  and make a measurement</param>
        /// <returns>measurement reading </returns>
        public double ReadChannel(int chan)
        {
            double reading = 0.0f;
            if ((chan >= ChanMin) && (chan <= ChanMax))
            {
                Channel = chan;
                ChannelStatus = 1;
                reading = GetReading();
            }
            else throw new Exception("no channel " + "\"" + chan.ToString() + "\"" + " available in multimeter ke2010");
            this.Write("ROUT:OPEN:ALL");

            return reading;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chan"></param>
        /// <param name="Reading"></param>
        /// <param name="unit"></param>
        public void ReadChannelInMultiDataElement(int chan, out double Reading, out string unit)
        {
            string reading = "";
            int ind;
            DataElement = DataElementType.ReadUnit;
            if ((chan >= ChanMin) && (chan <= ChanMax))
            {
                Channel = chan;
                ChannelStatus = 1;
                reading = GetMultiElementReading();
                unit = "";
                for (ind = reading.Length - 1; ind > 0; ind--)
                {
                    if (!Char.IsDigit(reading[ind]))
                    {
                        unit = reading[ind] + unit;
                        continue;
                    }
                    break;
                }

                reading = reading.Substring(1, ind);
                Reading = double.Parse(reading);

            }
            else throw new Exception("no channel " + "\"" + chan.ToString() + "\"" + " available in multimeter ke2010");
            this.Write("ROUT:OPEN:ALL");

            // return reading;
        }

        /// <summary>
        /// Set/Get data elements; Inst_Ke2010.DataElementType
        /// </summary>
        public DataElementType DataElement
        {
            get
            {
                String rtn = this.Query("FORM:ELEM?");

                switch (rtn)
                {
                    case "READ,,":
                        return DataElementType.ReadValue;

                    case "READ,CHAN,":
                        return DataElementType.ReadChannel;
                    case "READ,,UNIT":
                        return DataElementType.ReadUnit;
                    case "READ,CHAN,UNIT":
                        return DataElementType.ReadChnannelUnit;

                    default:
                        throw new Exception("Unknown data element Type!");
                }
            }
            set
            {
                String commandStr = "";
                switch (value)
                {
                    case DataElementType.ReadValue:
                        commandStr = "FORM:ELEM READ";
                        break;
                    case DataElementType.ReadChannel:
                        commandStr = "FORM:ELEM READ,CHAN";
                        break;
                    case DataElementType.ReadUnit:
                        commandStr = "FORM:ELEM READ,UNIT";
                        break;
                    case DataElementType.ReadChnannelUnit:
                        commandStr = "FORM:ELEM READ,CHAN,UNIT";
                        break;
                    default:
                        throw new Exception("Unknown data element Type!");
                }
                this.Write(commandStr);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataSourceType DataSource
        {
            set
            {
                //send(":TRAC:FEED SENSE");
                switch (value)
                {
                    case DataSourceType.SENSE:
                        instrumentChassis.Write_Unchecked(":TRAC:FEED SENSE", this);
                        break;
                    case DataSourceType.CALC:
                        instrumentChassis.Write_Unchecked(":TRAC:FEED CALC", this);
                        break;
                    case DataSourceType.NONE:
                        instrumentChassis.Write_Unchecked(":TRAC FEED NONE", this);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 1     for autoZero
        /// 0     for manualZero
        /// other for ERROR
        /// </summary>
        public Boolean IsAutoZero
        {
            get
            {
                string rtn = this.Query(":SYST:AZER:STAT?");

                if (int.Parse(rtn) == 1)
                    return true;
                if (int.Parse(rtn) == 0)
                    return false;

                throw new Exception("Unknow Auto Zero status!");
            }
            set
            {
                string commandString = "";
                if (value)
                    commandString = ":SYST:AZER:STAT ON";
                else
                    commandString = ":SYST:AZER:STAT OFF";

                this.Write(commandString);
            }
        }

        /// <summary>
        /// SET Mesurement resolution digits
        /// 4 3?digits
        /// 5 4?digits
        /// 6 5?digits
        /// 7 6?digits
        /// 8 7?digits
        /// DEFault 5?digits for ACI and ACV
        /// 6?digits for TEMP, FREQ, PER
        /// 7?digits for DCI, DCV, W2, W4
        /// MINimum 3?digits for ACI, ACV, DCI, DCV, W2, W4, TEMP,
        /// FREQ, PER
        /// MAXIMUM 6?digits for ACI, ACV, TEMP, FREQ, PER, W4 if
        /// dryckt is on
        /// 7?digits for DCI, DCV, W2, W4
        /// </summary>
        public double MeterResolution
        {
            get
            {
                string commandString = CommandHead + ":DIG?"; //":DIG:AUTO?";

                string rtn = this.Query(commandString);
                //if (int.Parse(rtn) == 1)
                //{
                //    return InstType_MultiMeter.AutoRange;
                //}
                //else
                //{
                //    commandString = CommandHead + ":DIG?";
                //}

                //rtn = this.Query(commandString);
                return double.Parse(rtn);
            }
            set
            {
                string commandString;
                //if (value.ToString() == InstType_MultiMeter.AutoRange.ToString())
                //{
                //    commandString = CommandHead + ":DIG:AUTO ON";
                //}
                //else
                //{
                commandString = CommandHead + ":DIG " + value.ToString();
                //}

                this.Write(commandString);
            }
        }

        /// <summary>
        /// set/get if to display the measurement reading on the front panel or not
        /// </summary>
        public Boolean IsDisplayFrontPanel
        {
            get
            {
                string rtn = this.Query(":DISP:ENABLE?");

                if (int.Parse(rtn) == 1)
                    return true;
                else
                    return false;
            }
            set
            {
                if (value)
                    this.Write(":DISP:ENABLE ON");
                else
                    this.Write(":DISP:ENABLE OFF");
            }
        }

        /// <summary>
        /// set or et the digital averaging filter status for the specified function.
        /// </summary>
        public Boolean AverageFilterStatus
        {
            get
            {
                string commandString = CommandHead + ":AVER:STATE?";

                string rtn = this.Query(commandString);

                if (int.Parse(rtn) == 1)
                    return true;
                if (int.Parse(rtn) == 0)
                    return false;

                throw new Exception("Unknow filter status!");
            }
            set
            {
                string commandString = "";
                if (value)
                {
                    commandString = CommandHead + ":AVER:STATE ON";
                }
                else
                {
                    commandString = CommandHead + ":AVER:STATE OFF";
                }

                this.Write(commandString);
            }
        }

        /// <summary>
        /// set or get the filter average count.
        /// </summary>
        public int FilterCount
        {
            get
            {
                string commandString = CommandHead + ":AVER:COUNT?";

                string rtn = this.Query(commandString);
                return int.Parse(rtn);
            }
            set
            {
                string commandString = CommandHead + ":AVER:COUNT " + value.ToString();

                this.Write(commandString);
            }
        }

        /// <summary>
        /// get or set the type of averaging filter
        /// </summary>
        public MeterFilterType Filter
        {
            get
            {
                string commandString = CommandHead + ":AVER:TCON?";

                string rtn = this.Query(commandString);

                if (rtn.Equals("MOV"))
                    return MeterFilterType.Moving;
                if (rtn.Equals("REP"))
                    return MeterFilterType.Repeat;

                throw new Exception("Unknow filter type!");
            }
            set
            {
                string commandString = CommandHead + ":AVER:TCON ";

                if (value.Equals(MeterFilterType.Moving))
                    commandString = commandString + "MOV";
                else
                    commandString = commandString + "REP";

                this.Write(commandString);
            }
        }

        /// <summary>
        /// Set/Get DMM Trigger continous status
        /// </summary>
        public bool IsTriggerContinuous
        {
            get
            {
                string rtn = this.Query(":INIT:CONT?");
                if (int.Parse(rtn) == 1) return true;
                if (int.Parse(rtn) == 0) return false;
                throw new Exception(" Unknown trigger continuous status");
            }
            set
            {
                string CommandStr = "";

                if (value == true) CommandStr = ":INIT:CONT ON";
                else CommandStr = ":INIT:CONT OFF";
                this.Write(CommandStr);
            }
        }

        /// <summary>
        /// 1     for Using Reference
        /// 0     for not
        /// other for ERROR
        /// </summary>        
        public Boolean IsUsingReference
        {
            get
            {
                string commandString = CommandHead + ":REF:STAT?";
                string rtn = this.Query(commandString);

                if (int.Parse(rtn) == 1)
                    return true;
                if (int.Parse(rtn) == 0)
                    return false;

                throw new Exception("Unknow using reference status!");
            }
            set
            {
                string commandString = "";
                if (value)
                {
                    commandString = CommandHead + ":REF:STAT ON";
                }
                else
                {
                    commandString = CommandHead + ":REF:STAT OFF";
                }

                this.Write(commandString);
            }
        }

        /// <summary>
        /// Get/Set event enable register value
        ///Bit0, Reading Overflow (ROF)  Set bit indicates that the reading exceeds the measurement range of the instrument.
        ///Bit1, Low Limit1 (LL1) Set bit indicates that the reading is less than the Low Limit 1 setting.
        ///Bit B2, High Limit1 (HL1) Set bit indicates that the reading is greater than the High Limit 1 setting.
        ///Bit B3, Low Limit 2 (LL2) Set bit indicates that the reading is less than the Low Limit 2 setting.
        ///Bit B4, High Limit 2 (HL2) Set bit indicates that the reading is greater than the High Limit 2 setting.
        ///Bit B5, Reading Available (RAV) Set bit indicates that a reading was taken and processed.
        ///Bit B6 Not used.
        ///Bit B7, Buffer Available (BAV) Set bit indicates that there are at least two
        ///readings in the trace buffer.
        ///Bit B8, Buffer Half Full (BHF) Set bit indicates that the trace buffer ishalf full.
        ///Bit B9, Buffer Full (BFL) Set bit indicates that the trace buffer is full.
        /// </summary>
        public int RegisterEventEnable
        {
            get
            {
                string rtn = this.Query(":STAT:MEAS:ENAB?");
                return int.Parse(rtn);
            }
            set
            {
                if (value < 0 || value > 1024)
                    throw new Exception(" invalid   satus enable register value");
                //strCommand = ":STAT:MEAS:ENAB " + utils::lexical_cast<std::string>(nValue);
                this.Write(":STAT:MEAS:ENAB " + value.ToString());
            }
        }

        /// <summary>
        /// Set Service request register
        /// 0 Clears enable register
        ///Bit0  MSB bit (Bit 0)
        ///Bit2  EAV bit (Bit 2)
        ///Bit3  Set QSB bit (Bit 3)
        ///Bit4  Set MAV bit (Bit 4)
        ///Bit5  Set ESB (Bit 5)
        ///Bit6  Set OSB (Bit 7)
        ///255 Set all bits
        /// </summary>
        public int RegisterServiceRequest
        {
            set
            {
                if ((value < 0) || (value > 255))
                    throw new Exception("Invalid service request value");
                else
                    this.Write(":*SRE " + value.ToString());
                //strCommand = ":*SRE " + utils::lexical_cast<std::string>(nValue);
            }
        }

        /// <summary>
        /// set/get reference value
        /// Parameters = -3.1 to 3.1 Reference for ACI and DCI,
        ///-757.5 to 757.5 Reference for ACV,
        ///-1010 to 1010 Reference for DCV,
        ///-10.1 to 10.1 Reference for DCV for sense terminals,
        ///0 to 120e6 Reference for W2 and W4,
        ///0 to 1.5e7 Reference for FREQ,
        ///0 to 1 Reference for PER,
        ///-200 to 1372 Reference for TEMP,
        ///DEFault 0 (all functions),
        ///MINimum Minimum value for specified function,
        ///MAXimum Maximum value for specified function,
        /// </summary>
        public double ReferenceValue
        {
            get
            {
                string commandString = CommandHead + ":REF?";
                string rtn = this.Query(commandString);
                return double.Parse(rtn);
            }
            set
            {
                string commandString = CommandHead + ":REF " + value.ToString();

                this.Write(commandString);
            }
        }

        /// <summary>
        /// set/get tigger control source
        /// </summary>
        public ControlSourceType TriggerSource
        {
            get
            {
                string rtn = this.Query(":TRIG:SOUR?");

                if (rtn.Equals("IMM"))
                    return ControlSourceType.IMMEDIATE;
                //if (rtn.Equals("HOLD"))
                //    return ControlSourceType.HOLD;
                if (rtn.Equals("MAN"))
                    return ControlSourceType.MANUAL;
                if (rtn.Equals("BUS"))
                    return ControlSourceType.BUS;
                if (rtn.Equals("TIM"))
                    return ControlSourceType.TIMER;
                if (rtn.Equals("EXT"))
                    return ControlSourceType.EXTERNAL;

                //throw new Exception("Unknown Trigger Source Type");
                return ControlSourceType.IMMEDIATE;
            }
            set
            {
                string commandString = ":TRIG:SOUR ";
                //send(":TRIG:SOUR IMM");
                switch (value)
                {
                    case ControlSourceType.IMMEDIATE:
                        commandString += "IMM";
                        break;
                    case ControlSourceType.EXTERNAL:
                        commandString += "EXT";
                        break;
                    case ControlSourceType.TIMER:
                        commandString += "TIM";
                        break;
                    case ControlSourceType.MANUAL:
                        commandString += "MAN";
                        break;
                    case ControlSourceType.BUS:
                        commandString += "BUS";
                        break;
                    default:
                        commandString += "IMM";
                        break;
                }
                this.Write(commandString);
            }
        }

        /// <summary>
        /// set/get the times thet operation loops around in the trigger operation
        /// The times will be 1 to 9999
        /// </summary>
        public int TriggerCount
        {
            get
            {
                string rtn = this.Query(":TRIG:COUN?");
                double results = double.Parse(rtn);

                if (results <= 9999)
                {
                    return int.Parse(rtn);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if ((value >= 2) && (value <= 1024))
                {
                    //strCommand = ":TRIG:COUN " + utils::lexical_cast<std::string>(nTriggerCount);
                    string commandString = ":TRIG:COUN " + value.ToString();
                    this.Write(commandString);
                }
                else
                    throw new Exception("K2010::TriggerCount - Invalid Trigger Count Specified(2~1024)");
            }
        }

        /// <summary>
        /// Set/Get the Tigger Count to the Status listed below:
        /// INF Sets count to infinite
        /// DEFault Sets count to 1
        /// MINimum Sets count to 1
        /// MAXimum Sets count to 9999
        /// </summary>
        public string TriggerCount2
        {
            set
            {
                string command = "";

                switch (value.ToUpper())
                {
                    case "INF":
                        command = ":TRIG:COUN INF";
                        break;
                    case "DEFAULT":
                        command = ":TRIG:COUN DEFAULT";
                        break;
                    case "MINIMUM":
                        command = ":TRIG:COUN MINIMUM";
                        break;
                    case "MAXIMUM":
                        command = ":TRIG:COUN MAXIMUM";
                        break;
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
            get
            {
                string rtn = this.Query(":TRIG:COUN?");
                double results = double.Parse(rtn);

                if (results <= 9999)
                {
                    return rtn;
                }
                else
                {
                    return "INF";
                }
            }
        }

        /// <summary>
        /// Set trigger model delay
        /// The parameters will be 
        ///     0 to 999999.999         Specify delay in seconds
        ///     DEFault                 0 second delay
        ///     MINimum                 0 second delay
        ///     MAXimum                 999999.999 second delay
        /// </summary>
        public string TriggerDelay
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:DEL?", this);

                return rtn;
            }
            set
            {
                instrumentChassis.Write_Unchecked(":TRIG:DEL " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Set/Get interval for measure layer timer
        /// The parameter will be 0.001 to 999999.999
        /// </summary>
        public double TriggerTimer
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:TIMer?", this);
                return double.Parse(rtn);
            }
            set
            {
                instrumentChassis.Write_Unchecked(":TRIG:TIMer " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Bypass measure control source
        /// </summary>
        public void SetTriggerSignal()
        {
            instrumentChassis.Write_Unchecked(":TRIG:SIGNal", this);
        }

        /// <summary>
        /// Set sample count
        /// The value will be 1 to 1024
        /// </summary>
        public int TriggerSampleCount
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":SAMPle:COUNt?", this);
                return int.Parse(rtn);
            }
            set
            {
                if ((value >= 1) && (value <= 1024))
                {
                    //strCommand = ":SAMP:COUN " + utils::lexical_cast<std::string>(nSampleCount);
                    instrumentChassis.Write_Unchecked(":SAMP:COUN " + value.ToString(), this);
                }
                else
                {
                    throw new InstrumentException("Set Trigger Sample Count Error -- Sample Count Error!!!");
                }
            }
        }

        /// <summary>
        /// set all channel open
        /// </summary>
        public void OpenAllChannel()
        {
            this.Write("ROUT:OPEN:ALL");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetSingleMeasurement(MeasurementType type)
        {
            string results = "";

            switch (type)
            {
                case MeasurementType.CurrentAC:
                    results = instrumentChassis.Query_Unchecked("MEAS:CURR:AC?", this);
                    break;
                case MeasurementType.CurrentDC:
                    results = instrumentChassis.Query_Unchecked("MEAS:CURR:DC?", this);
                    break;
                case MeasurementType.VoltageAC:
                    results = instrumentChassis.Query_Unchecked("MEAS:VOLT:AC?", this);
                    break;
                case MeasurementType.VoltageDC:
                    results = instrumentChassis.Query_Unchecked("MEAS:VOLT:DC?", this);
                    break;
                case MeasurementType.Resistance_2Wire:
                    results = instrumentChassis.Query_Unchecked("MEAS:RES?", this);
                    break;
                case MeasurementType.Resistance_4Wire:
                    results = instrumentChassis.Query_Unchecked("MEAS:FRES?", this);
                    break;
                case MeasurementType.Period:
                    results = instrumentChassis.Query_Unchecked("MEAS:PER?", this);
                    break;
                case MeasurementType.Frequency:
                    results = instrumentChassis.Query_Unchecked("MEAS:FREQ?", this);
                    break;
                case MeasurementType.Temperature:
                    results = instrumentChassis.Query_Unchecked("MEAS:TEMP?", this);
                    break;
                case MeasurementType.Diode:
                    results = instrumentChassis.Query_Unchecked("MEAS:DIOD?", this);
                    break;
                case MeasurementType.Continuity:
                    results = instrumentChassis.Query_Unchecked("MEAS:CONT?", this);
                    break;
            }

            return Convert.ToDouble(results);
        }

        /// <summary>
        /// Read Sweep Data from buffer 
        /// </summary>
        /// <param name="values">Sweep Data</param>
        public void ReadSweepData(out double[] values)
        {
            Write("FORM:BORD SWAP"); // Big Endian byte order
            this.DataFormat = DataFormatType.REAL32;
            //send(":TRAC:DATA?");
            byte[] buffer = instrumentChassis.QueryByteArray_Unchecked(":TRAC:DATA?", this);

            float[] data = ParseBinaryData(buffer);
            values = new double[data.Length];
            int i = 0;
            foreach (float value in data)
            {
                values[i] = (double)value;
                i++;
            }
        }
        public double[] Read_Sweep(int nNumberOfChannels, int nNumberOfPoints)
        {
            // Assuming that FORMAT has been pre configured for
            // correct resultant data format
            List<double> dRawData_Vec = new List<double>();
            byte[] res = new byte[nNumberOfChannels * nNumberOfPoints * 20];
            //send(":TRAC:DATA?");
            //ReceiveBinary(res);
            res = instrumentChassis.QueryByteArray_Unchecked(":TRAC:DATA?", this);

            // Tokenise
            //StringList list;  
            //List<string> list = new List<string>();
            //std::istringstream sslist( res);
            //sslist >> StringListReader( list, ',' );
            string strRes = UnicodeEncoding.ASCII.GetString(res);
            string[] list = strRes.Split(',');

            // Determine List size
            int nSize = list.Length;
            int elem = 0;
            double dVal = 0;

            for (elem = 0; elem < nSize; elem++)
            {
                dVal = Convert.ToSingle(list[elem]);
                dRawData_Vec.Add(dVal);
            }
            // Convert to VARIANT
            //updateVariantFromArray<double,1>(*dRawData,dRawData_Vec);
            return dRawData_Vec.ToArray();


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nChannelNumber"></param>
        public void Set_Channel(int nChannelNumber)
        {
            if ((nChannelNumber > 0) && (nChannelNumber < 11))
            {
                string strCommand;
                strCommand = ":ROUT:CLOS (@ " + nChannelNumber.ToString() + ")";
                this.Write(strCommand);
            }
            else
            {
                throw new Exception("K2010::Close_Channel - Invalid Channel Value Supplied");
            }
        }
        /// <summary>
        /// Reset the instrument
        /// </summary>
        public void Reset()
        {
            instrumentChassis.Write_Unchecked("*RST", this);
        }

        /// <summary>
        /// Reset trigger system
        /// </summary>
        public void ResetTrigger()
        {
            //send("ABOR");
            instrumentChassis.Write_Unchecked("ABOR", this);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetTrigger()
        {
            //send(":INIT:IMM");
            instrumentChassis.Write_Unchecked(":INIT:IMM", this);
        }

        #endregion

        #region private functions

        /// <summary>
        /// Write command to instrument
        /// </summary>
        /// <param name="cmd">the command we wish to write to chassis</param>
        public void Write(string cmd)
        {
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Query command to instrument 
        /// </summary>
        /// <param name="cmd">the query string</param>
        /// <returns></returns>
        public string Query(string cmd)
        {
            return (instrumentChassis.Query_Unchecked(cmd, this));
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, double response)
        {
            try
            {
                return Double.Parse(command);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "', was expecting a double: " + response,
                    e);
            }
        }

        /// <summary>
        /// Takes the binary data (byte array), ignores the first 2 bytes, then
        /// converts groups of 4 bytes into singles and eventually returns an array of singles
        /// </summary>
        /// <param name="buffer">the bute array</param>
        /// <returns>an array of singles</returns>
        private Single[] ParseBinaryData(byte[] buffer)
        {
            Single[] valueList;
            int buffSize = buffer.Length;
            int index = 0;
            valueList = new Single[(buffSize - 2) / 4];
            // Ignore the first 2 bytes as they contain header information
            for (int i = 2; i + 3 < buffer.Length; i += 4)
            {
                byte[] subArray = Alg_ArrayFunctions.ExtractSubArray(buffer, i, i + 4);
                valueList[index] = Alg_BigEndianConvert.BigEndianBytesToSingle(subArray);
                index++;
            }
            return valueList;
        }

        #endregion

        #region Applications

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enabledScan"></param>
        /// <param name="numberOfPoints"></param>
        /// <param name="firstChan"></param>
        /// <param name="secondChan"></param>
        public void InitialiseVoltageSweep(bool enabledScan, int numberOfPoints, int firstChan, int secondChan)
        {

            ushort numberOfChannels = 0;

            if (enabledScan == false)
                numberOfChannels = 1;
            else
            {
                numberOfChannels = (ushort)(Math.Abs(secondChan - firstChan) + 1);
                if (firstChan > secondChan)
                {
                    throw new Exception("Inst_Ke2010 InitialiseVoltageSweep _ First Channel Cannot Occur Before Last Channel");
                }
                if (firstChan == secondChan)
                {
                    throw new Exception("Inst_Ke2010 InitialiseVoltageSweep - Channel Allocation Not Set Up");
                }
            }

            //---
            Reset();
            SenseFunction = MeasurementType.VoltageDC;
            //SetDefaultState();
            //Set_StatusPreset();":STAT:PRES"
            instrumentChassis.Write_Unchecked(":STAT:PRES", this);
            Clear();
            RegisterEventEnable = 512;
            RegisterServiceRequest = 1;
            ContinuousInitiation = false;
            ResetTrigger();
            TriggerSampleCount = numberOfChannels;
            TriggerSource = ControlSourceType.EXTERNAL;
            SetSenseFilter(MeasurementType.CurrentDC, false);
            TriggerCount = numberOfPoints;
            MultiMeterMode = MeterMode.Voltage_DC_V;
            MeterRange = 10;
            BufferSize = numberOfPoints * numberOfChannels;
            //Set_Buffer_Clear();send(":TRAC:CLE");
            instrumentChassis.Write_Unchecked(":TRAC:CLE", this);
            DataSource = DataSourceType.SENSE;
            BufferControl = BufferControlMode.NEXT;
            if (enabledScan)
            {
                SetRounteScanChannel(firstChan, secondChan);
                ScanOperation = ScanOperationType.INTERNAL;
            }
        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;

        /// <summary>
        /// the corresponding command head of the meterMode
        /// </summary>
        private string CommandHead
        {
            get
            {
                string commandString = "";
                switch (MultiMeterMode)
                {
                    case MeterMode.Current_AC_A_rms:
                        commandString = ":CURR:AC";
                        break;
                    case MeterMode.Current_DC_A:
                        commandString = ":CURR:DC";
                        break;
                    case MeterMode.Resistance2wire_ohms:
                        commandString = ":RES";
                        break;
                    case MeterMode.Resistance4wire_ohms:
                        commandString = ":FRES";
                        break;
                    case MeterMode.Voltage_AC_V_rms:
                        commandString = ":VOLT:AC";
                        break;
                    case MeterMode.Voltage_DC_V:
                        commandString = ":VOLT:DC";
                        break;
                }

                return commandString;
            }
        }

        #endregion

        #region enum Data

        /// <summary>
        /// the type of averaging filter
        /// </summary>
        public enum MeterFilterType
        {
            /// <summary>
            /// Moving filter type
            /// </summary>
            Moving = 0,
            /// <summary>
            /// Repeat Filter Type
            /// </summary>
            Repeat = 1,
        }

        /// <summary>
        /// event control source.
        /// </summary>
        public enum ControlSourceType
        {
            /// <summary>
            /// Pass operation through immediately
            /// </summary>
            IMMEDIATE = 0,
            /// <summary>
            /// Select External Triggering as event
            /// </summary>
            EXTERNAL = 1,
            /// <summary>
            /// Select timer as event
            /// </summary>
            TIMER = 2,
            /// <summary>
            /// Select manual event
            /// </summary>
            MANUAL = 3,
            /// <summary>
            /// Select bus trigger as event
            /// </summary>
            BUS = 4,
        }

        /// <summary>
        /// source of readings to be placed in the buffer
        /// </summary>
        public enum BufferControlMode
        {
            /// <summary>
            /// Disables buffer storage
            /// </summary>
            NEVER = 0,
            /// <summary>
            /// Fills buffer and stops
            /// </summary>
            NEXT = 1,
            //ALWAYS = 2,
            // PRETRIGGER = 3,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum BufferFeedMode
        {
            /// <summary>
            /// Put raw readings in buffer
            /// </summary>
            SENS,

            /// <summary>
            /// Put calculated readings in buffer
            /// </summary>
            CALC,

            /// <summary>
            /// Put no readings in buffer
            /// </summary>
            NONE
        }

        /// <summary>
        /// Reading components
        /// </summary>
        public enum DataElementType
        {
            /// <summary>
            /// measurement  reading value only
            /// </summary>
            ReadValue = 0,
            /// <summary>
            /// reading + channel
            /// </summary>
            ReadChannel = 1,
            /// <summary>
            /// reading + units
            /// </summary>
            ReadUnit = 2,
            /// <summary>
            /// reading + channel + unit
            /// </summary>
            ReadChnannelUnit = 3,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum MeasurementType
        {
            /// <summary>
            /// 
            /// </summary>
            CurrentAC,

            /// <summary>
            /// 
            /// </summary>
            CurrentDC,

            /// <summary>
            /// 
            /// </summary>
            VoltageAC,

            /// <summary>
            /// 
            /// </summary>
            VoltageDC,

            /// <summary>
            /// 
            /// </summary>
            Resistance_2Wire,

            /// <summary>
            /// 
            /// </summary>
            Resistance_4Wire,

            /// <summary>
            /// 
            /// </summary>
            Period,

            /// <summary>
            /// 
            /// </summary>
            Frequency,

            /// <summary>
            /// 
            /// </summary>
            Temperature,

            /// <summary>
            /// 
            /// </summary>
            Diode,

            /// <summary>
            /// 
            /// </summary>
            Continuity
        }

        /// <summary>
        /// 
        /// </summary>
        public enum DataSourceType
        {
            /// <summary>
            /// 
            /// </summary>
            SENSE,

            /// <summary>
            /// 
            /// </summary>
            CALC,

            /// <summary>
            /// 
            /// </summary>
            NONE
        }

        /// <summary>
        /// Operation Type of Scan
        /// </summary>
        public enum ScanOperationType
        {
            /// <summary>
            /// Enable scan for internal scanner card
            /// </summary>
            INTERNAL,

            /// <summary>
            /// Enable scan for external scanner card
            /// </summary>
            EXTERNAL,

            /// <summary>
            /// Disable all scan operations
            /// </summary>
            NONE
        }

        /// <summary>
        /// Get/set the data format
        /// </summary>
        public DataFormatType DataFormat
        {
            get
            {
                string rtn = this.Query(":FORM:DATA?");

                if (rtn == "ASC")
                    return DataFormatType.ASCII;
                if (rtn == "REAL,32")
                    return DataFormatType.REAL32;
                if (rtn == "REAL,64")
                    return DataFormatType.REAL64;
                if (rtn == "SRE")
                    return DataFormatType.SREAL;
                if (rtn == "DRE")
                    return DataFormatType.DREAL;

                throw new InstrumentException("Unknown Format Type : " + rtn);
            }
            set
            {
                string commandString = ":FORM:DATA ";

                if (value.Equals(DataFormatType.ASCII))
                    commandString = commandString + "ASC";

                else if (value.Equals(DataFormatType.REAL32))
                    commandString = commandString + "REAL,32";

                else if (value.Equals(DataFormatType.REAL64))
                    commandString = commandString + "REAL,64";

                else if (value.Equals(DataFormatType.DREAL))
                    commandString = commandString + "DRE";

                else if (value.Equals(DataFormatType.SREAL))
                    commandString = commandString + "SRE";

                this.Write(commandString);
            }
        }

        /// <summary>
        /// The data format for transferring reading over the bus
        /// </summary>
        public enum DataFormatType
        {
            /// <summary>
            /// Direct readable form
            /// </summary>
            ASCII = 0,
            /// <summary>
            /// The binary IEEE754 single precision data format
            /// </summary>
            REAL32 = 1,
            /// <summary>
            /// The binary IEEE754 double precision data format
            /// </summary>
            REAL64 = 2,
            /// <summary>
            /// The same as REAL32
            /// </summary>
            SREAL = 3,
            /// <summary>
            /// The same as REAL64
            /// </summary>
            DREAL = 4,
        }

        #endregion

    }
}

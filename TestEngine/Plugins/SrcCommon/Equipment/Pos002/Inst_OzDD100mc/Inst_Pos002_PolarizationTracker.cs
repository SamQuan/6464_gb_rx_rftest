// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Pos002_PolarizationTracker.cs
//
// Author: 
// Design: As specified in Pos-002 driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;
using System.IO;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// OZ POS-002 optical Polarization Tracker
    /// </summary>
    public class Inst_Pos002_PolarizationTracker : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Pos002_PolarizationTracker(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all OzDd100mc details
            InstrumentDataRecord Pos002InstData = new InstrumentDataRecord(
                "Hardware_Unknown",      // hardware name 
                "Firmware_Unknown",  			                // minimum valid firmware version 
                "Firmware_Unknown");			            // maximum valid firmware version 
            
            Pos002InstData.Add("MinWavelength_nm", "1200");    // minimum wavelength
            Pos002InstData.Add("MaxWavelength_nm", "1650");    // maximum wavelength

            ValidHardwareData.Add("Pos002", Pos002InstData);

            // Configure valid chassis information
            // Add Chassis_OzDd100mc chassis details
            InstrumentDataRecord Pos002ChassisData = new InstrumentDataRecord(
                "Chassis_OzDd100mc",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "1.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("OzDd100mc", Pos002ChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Pos002) base.InstrumentChassis;
            
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
               return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            chassisWrite("*RST");       
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Pos-002 Optical Polarization Tracker Specific functions

        /// <summary>
        /// Enabled/Disabled polarization tracker 
        /// </summary>
        public bool EnabledPolarizationTracker
        {
            get 
            {
                string str = chassisQuery("*MOD?");

                if (str.Contains("*MOD ENA#"))
                {
                    return true;
                }
                else if (str.Contains("*MOD DIS#"))
                {
                    return false;
                }
                else
                {
                    throw new InstrumentException("Invalid status return!");
                }

            }
            set 
            {
                if (value == true)
                {
                    chassisWrite("*ENA#");
                }
                else
                {
                    chassisWrite("*DIS#");
                }
            }
        }

        #endregion

        #region Helper functions

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            this.instrumentChassis.WriteLine(command, this);
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// </summary>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        /// <summary>
        /// Get next line from file. Returns null if empty
        /// </summary>
        /// <returns>Array of strings for this line</returns>
        public string[] GetLine(StreamReader streamReader)
        {
            // read the next line
            string line = streamReader.ReadLine();
            // check for end of file (null).
            if (line == null) return null;
            string[] elemsInLine = line.Split(',');
            return elemsInLine;
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Pos002 instrumentChassis;

        private Dictionary<string, double> stepForAttenuation_dB = new Dictionary<string, double>();
        private Dictionary<string, double> stepForWavelength_nm = new Dictionary<string, double>();

        #endregion
    }
}

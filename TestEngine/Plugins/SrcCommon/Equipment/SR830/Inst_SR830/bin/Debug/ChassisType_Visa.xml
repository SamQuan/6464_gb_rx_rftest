<?xml version="1.0"?>
<doc>
    <assembly>
        <name>ChassisType_Visa</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa">
            <summary>
            A chassis which uses National Instruments VISA .NET software library to provide 
            instrument/chassis communications support.
            This provides a number of physical instrument interface options including 
            GPIB, RS232, USB and Ethernet among others.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.#ctor(System.String,System.String,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="chassisName">Chassis name</param>
            <param name="driverName">Chassis driver name</param>
            <param name="resourceInfo">Resource data</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Dispose">
            <summary>
            Destructor
            Closes the current Visa session if it exists
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.GetStatusByte">
            <summary>
            Read the status byte - VISA is clever enough to adapt what this does dependent on the 
            interface.
            </summary>
            <remarks>
            Remarks from VISA .NET HELP:
            This method reads the service request status from a service requester (the message-based device). 
            For example, on the IEEE 488.2 interface, polling devices read the message. For other types of interfaces, a message is sent in response to a service request to retrieve status information. 
            For a session to a Serial device or Ethernet socket, if IOProtocol is set to Protocol4882, the string, "*STB?\n", is sent to the device, and the device status byte is read. If the status information is only one byte long, the most significant byte is returned with the zero value. If the service requester does not respond in the actual timeout period, a VisaException is thrown with an error code of ErrorTimeout. For a session to a USB instrument, this function sends the READ_STATUS_BYTE command on the control pipe. 
            </remarks>
            <returns>StatusByteFlags - Partially defines the bits on a status byte bitfield. Other non-standard bits are available
            </returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Clear">
            <summary>
            Call a VISA clear on the chassis
            (e.g. for GPIB this is identical to DEVCLR low level command).
            </summary>
            <remarks>
            Quote from VISA .NET HELP:
            This method performs an IEEE 488.1-style clear of the device. 
            For VXI, this method uses the Word Serial Clear command. 
            For GPIB systems, this method uses the Selected Device Clear command. 
            For a session to a Serial device or Ethernet socket, if IOProtocol is set to Protocol4882, the device is sent the string "*CLS\n". For a Serial device of Ethernet socket, this method is invalid for any other setting of IOProtocol. 
            </remarks>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Write_Unchecked(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Write string to VISA
            Throws an exception if equipment is offline
            </summary>
            <param name="command">String to write to equipment</param>
            <param name="instrument">equipment reference</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.WriteByteArray_Unchecked(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Write byte array to VISA
            </summary>
            <param name="writeData">Byte array to write to equipment</param>
            <param name="instrument">Equipment reference</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.WriteIEEEBinary_Unchecked(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Writes an IEEE standard binary block from Chassis
            </summary>
            <param name="writeData">Byte array to write to equipment</param>
            <param name="instrument">Equipment reference</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Query_Unchecked(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            String query VISA  
            </summary>
            <param name="command">String to write to equipment</param>
            <param name="instrument">Equipment reference</param>
            <returns>Reply string</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.QueryByteArray_Unchecked(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Byte array query VISA  
            </summary>
            <param name="command">Command to send to equipment</param>
            <param name="instrument">Equipment reference</param>
            <returns>Reply byte array</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.QueryByteArray_Unchecked(System.String,System.Int32,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Byte array query VISA  
            </summary>
            <param name="command">Command to send to equipment</param>
            <param name="instrument">Equipment reference</param>
            <param name="nbrBytesToReceive">Number of bytes enabled</param>
            <returns>Reply byte array</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.QueryIEEEBinary_Unchecked(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Queries an IEEE standard binary block from Chassis
            </summary>
            <param name="command">Command to send to equipment</param>
            <param name="instrument">Equipment reference</param>
            <returns>Reply byte array</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.QueryByte_Unchecked(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
             Sends a query, and converts the response received back to a byte
            </summary>
            <param name="command">command to send</param>
            <param name="instrument">instrument reference</param>
            <returns>returned value from the query</returns>
            <exception cref="T:Bookham.TestEngine.PluginInterfaces.Chassis.ChassisException">If invalid return value or format</exception>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Read_Unchecked(Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Read string from VISA
            </summary>
            <param name="instrument">Equipment reference</param>
            <returns>Reply string</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.ReadByteArray_Unchecked(Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Int32)">
            <summary>
            Read byte array from VISA
            </summary>
            <param name="instrument">Equipment reference</param>
            <param name="nbrBytesToReceive">Number of bytes enabled</param>        
            <returns>Reply byte array</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.ReadByteArray_Unchecked(Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Read byte array from VISA
            </summary>
            <param name="instrument">Equipment reference</param>
            <returns>Reply byte array</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.checkEquipmentOnline(Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Check that the instrument and chassis is online. Raise an exception if not.
            </summary>
            <param name="instrument">Instrument reference</param>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.visaSession">
            <summary>
            Local reference to the VISA session
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.vcTimeout_ms">
            <summary>
            Local timeout 
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.vcLocked">
            <summary>
            Local lock. Holds the set timeout value when offline and visa session is not available
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.VisaSession">
            <summary>
            VisaSession property provides an instance of a message based Visa session.
            Refer to National Instruments VISA documentation for details.
            A session is created the first time communication is attempted with 'IsOnline' true.
            This session remains and is reused for subsequent communication until closed 
            (by calling the sessions 'Dispose' function) or the chassis object is destroyed.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.IsOnline">
            <summary>
            Online property. 
            If true, communication with instrument is attempted.
            False by default at startup. Overrides from base 'Chassis' class.
            
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Timeout_ms">
            <summary>
            Communications timeout in milliseconds
            Overriden from base 'Chassis' class.
            Visa timeout value may vary from set value as the Visa timout is roounded
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa.Locked">
            <summary>
            Chassis communication lock. 
            Exception if chassis is locked and communication is attempted.
            False by default at startup
            Overrides definition in 'Chassis'
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2">
            <summary>
            VISA Chassis type that implements IEEE.488.2 standard commands and registers.
            The instrument will typically be a GPIB instrument, but depending on the
            actual underlying interface the NI-VISA library will adapt its calls. 
            For example: to read the status byte,
            this is polled by a low level command over GPIB and USB, but read by a message over serial or Ethernet interface.
            See National Instrument .NET interface help file (part of NI-VISA full installation). 
            </summary>
            <remarks>Derives from ChassisType_Visa to provide basic VISA function</remarks>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.#ctor(System.String,System.String,System.String)">
            <summary>
            Constructor - pass it up the chain
            </summary>
            <param name="chassisName">Chassis name</param>
            <param name="driverName">Chassis driver name</param>
            <param name="resourceInfo">Resource data</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.IsStandardEvent(NationalInstruments.VisaNS.StatusByteFlags)">
            <summary>
            Does the status byte (flags) have the "Standard Event" bit set?
            </summary>
            <param name="statusByte">Status byte (flags) from the Chassis</param>
            <returns>true if "Standard Event" bit is set</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.WaitForOperationComplete">
            <summary>
            Waits for all pending operations to complete.
            If anything else comes back an exception is thrown because it is obviously unexpected.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.WaitForOperationComplete(System.Int32,System.Int32)">
            <summary>
            Waits for all pending operations to complete, specifying delay between tries and
            maximum number of tries.
            </summary>
            <param name="maxTries">Maximum number of times to loop.</param>
            <param name="delay_ms">Delay between retries.</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Write(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Writes a command to the chassis and will wait for completion and check status
            byte and other registers for error conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Write(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Boolean,System.Boolean)">
            <summary>
            Writes a command to the chassis. Dependent on options given this will wait 
            for completion and check status
            byte and other registers for error conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
            <param name="errorCheck">If async set to false, then this parameter determines
            whether errors are checked for</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Write(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Boolean,System.Int32,System.Int32)">
            <summary>
            Writes a command to the chassis, waiting in a loop for completion. Intended
            for use with instruments where "*OPC?" may return "0" or "+0" meaning
            incomplete.
            </summary>
            <param name="command">Command to send.</param>
            <param name="i">Applicable instrument.</param>
            <param name="errorCheck">If true, check for errors.</param>
            <param name="maxWaitLoops">Maximum number of attempts to wait for completion.</param>
            <param name="waitDelay_ms">Delay between attempts.</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.WriteByteArray(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Writes a byte array to the chassis and will wait for completion and check status
            byte and other registers for error conditions.
            </summary>
            <param name="bytes">Bytes to send</param>
            <param name="i">Which instrument is this from?</param>
            <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.WriteByteArray(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Boolean,System.Boolean)">
            <summary>
            Writes a byte array to the chassis. Dependent on options given this will wait 
            for completion and check status
            byte and other registers for error conditions.
            </summary>
            <param name="bytes">Bytes to send</param>
            <param name="i">Which instrument is this from?</param>
            <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
            <param name="errorCheck">If async set to false, then this parameter determines
            whether errors are checked for</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.WriteIEEEBinary(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Writes an IEEE standard binary block from Chassis and will wait for completion and check status
            byte and other registers for error conditions.
            </summary>
            <param name="bytes">Byte array to write to equipment</param>
            <param name="i">Equipment reference</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Query(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Sends a command to the chassis and waits for a response. Checks status
            byte and other registers for error conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <returns>string response</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Query(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Boolean)">
            <summary>
            Sends a command to the chassis and waits for a response. Dependent on 
            arguments, will check the status byte and other registers for error 
            conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <param name="errorCheck">Don't check for errors once value returns</param>
            <returns>string response</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.QueryByteArray(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Sends a command to the chassis and waits for a byte array response. Checks status
            byte and other registers for error conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <returns>Byte array response</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.QueryByteArray(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument,System.Boolean)">
            <summary>
            Sends a command to the chassis and waits for a byte array response. Dependent on 
            arguments, will check the status byte and other registers for error 
            conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <param name="errorCheck">Don't check for errors once value returns</param>
            <returns>Byte array response</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.QueryByteArray(System.String,System.Int32,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Sends a command to the chassis and waits for a byte array response. Checks status
            byte and other registers for error conditions.
            </summary>
            <param name="command">Command to send</param>
            <param name="i">Which instrument is this from?</param>
            <param name="nbrBytesToReceive">Number of bytes enabled</param>
            <returns>Byte array response</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.QueryIEEEBinary(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Queries an IEEE standard binary block from Chassis
            </summary>
            <param name="command">Command to send to equipment</param>
            <param name="i">Equipment reference</param>
            <returns>Reply byte array (header stripped)</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.RaiseChassisException(System.String,Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Queries the chassis for the details of the error it has encountered,
            and raise a ChassisException from it.
            </summary>
            <remarks>Only call this function once you have detected a chassis error!</remarks>
            <param name="command">Last command that was sent</param>
            <param name="i">Instrument generating comms</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.RaiseChassisException(System.Byte[],Bookham.TestEngine.PluginInterfaces.Instrument.Instrument)">
            <summary>
            Queries the chassis for the details of the error it has encountered,
            and raise a ChassisException from it.
            </summary>
            <remarks>Only call this function once you have detected a chassis error!</remarks>
            <param name="bytes">Last bytes that were sent</param>
            <param name="i">Instrument generating comms</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.GetErrorString">
            <summary>
            Function used to get error information from Chassis, once an error condition has
            been detected. This is a separate virtual function, as most instruments (Agilent , Keithley) 
            implement this as SYST:ERR? (SCPI syntax). However this isn't strictly general to all 488.2 chassis, 
            so allow other chassis to override it.
            </summary>
            <returns>Error string from the chassis</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.isErrorConditionInStatusByte">
            <summary>
            Query the status byte register and return true if an error 
            condition is signalled.
            </summary>
            <returns>True if error, false if none</returns>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.byteArrayToHexStr(System.Byte[])">
            <summary>
            Generate a hex string from a byte array
            </summary>
            <param name="bytes">The byte array</param>
            <returns>Converted to string</returns>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.errorCheckUse_488_2_Command">
            <summary>
            For error checking functions, if true, use the "*STB?" normal command rather than
            ChassisType_Visa.GetStatusByte() command.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.Idn">
            <summary>
            Get IDN string
            </summary>
            <returns>IDN string</returns>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.StatusByte488_2_Command">
            <summary>
            Reads the status byte from the chassis, using the 488.2 command.
            This differs from ChassisType_Visa.GetStatusByte() in that this
            isn't a low level GPIB bus command.
            
            This is necessary as some chassis return different values to the two
            methods of reading the status byte (e.g. Agilent 816x)
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.StandardEventRegister">
            <summary>
            Get value of the standard event register
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.StandardEventRegisterMask">
            <summary>
            Get/set value of the mask on the standard event register which will 
            cause bit 5 of the status byte to be raised. This can be then detected 
            by reading the status byte with ChassisType_Visa.GetStatusByte() and checking
            for the value "EventStatusRegister" by calling the this.IsStandardEvent() method.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_Visa488_2.ErrorCheckUse_488_2_Command">
            <summary>
            For error checking functions, if true, use the "*STB?" normal command rather than
            ChassisType_Visa.GetStatusByte() command.
            
            This is necessary as some chassis return different values to the two
            methods of reading the status byte (e.g. Agilent 816x)
            </summary>        
        </member>
        <member name="T:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI">
            <summary>
            A chassis which uses National Instruments VISA .NET software library to provide 
            instrument/chassis communications support for PXI Instruments.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.#ctor(System.String,System.String,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="chassisName">Chassis name</param>
            <param name="driverName">Chassis driver name</param>
            <param name="resourceInfo">Resource data</param>
        </member>
        <member name="M:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.Dispose">
            <summary>
            Destructor
            Closes the current Visa session if it exists
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.pxiSession">
            <summary>
            Local reference to the VISA session
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.vcTimeout_ms">
            <summary>
            Local timeout 
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.vcLocked">
            <summary>
            Local lock. Holds the set timeout value when offline and visa session is not available
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.VisaSession">
            <summary>
            VisaSession property provides an instance of a PXI Visa session.
            Refer to National Instruments VISA documentation for details.
            A session is created the first time communication is attempted with 'IsOnline' true.
            This session remains and is reused for subsequent communication until closed 
            (by calling the sessions 'Dispose' function) or the chassis object is destroyed.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.VisaSessionHandle">
            <summary>
            Return the low-level Visa session handle that is needed
            when communicating with PXI driver DLLs via Interop
            (referred to as ViSession type in VB and C/C++)
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.IsOnline">
            <summary>
            Online property. 
            If true, communication with instrument is attempted.
            False by default at startup. Overrides from base 'Chassis' class.
            
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.Timeout_ms">
            <summary>
            Communications timeout in milliseconds
            Overriden from base 'Chassis' class.
            Visa timeout value may vary from set value as the Visa timout is roounded
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.ChassisTypes.ChassisType_PXI.Locked">
            <summary>
            Chassis communication lock. 
            Exception if chassis is locked and communication is attempted.
            False by default at startup
            Overrides definition in 'Chassis'
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.ChassisTypes.NamespaceDoc">
            <summary>
            Test Engine Chassis Types Code.
            </summary>
        </member>
    </members>
</doc>

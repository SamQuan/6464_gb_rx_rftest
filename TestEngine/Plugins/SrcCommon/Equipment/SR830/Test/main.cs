using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;


namespace EquipmentTest_Inst_E363X
{
     public class TestWrapper
    {
        public  static int Main(string[] args)
        {
            //Init tester
            Inst_SR830_Test test = new Inst_SR830_Test();
            
            //Run tests
            test.Setup();
            //creat chassis
            test.T01_CreatInst("GPIB0::8::INSTR");
            test.T03_SetOnline();
            test.T04_DriverVersion();
            test.T05_FirmwareVersion();
            test.T06_HardwareID();
            test.T07_SetDefaultState();




            Thread.Sleep(1000);
            test.T18_RecallLockInSetting();
            test.T08_TimeConstantSetting();
            test.T09_SnesitivitySetting();
            test.T10_SignalInputSetting();
            test.T11_ReserveSetting();
            test.T12_NotchFiltersSelect();
            test.T13_DisplaySetting();
            test.T14_AutoSetting();
            test.T15_InterfaceSelect();
            test.T16_ReferenceSourceSetting();
            test.T17_SaveLockInSetting();
            test.T18_RecallLockInSetting();

            Console.WriteLine("please inter a key to exit!");
            Console.ReadLine();
            return 0;
        }
    }
}

using System;
using System.Collections.Generic;

using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace JdsSaZ349TestHarness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // Chassis reference
        private Chassis_JDSSAZ349 testChassis;

        // Instrument References
        private Inst_JDSSAZ349_SimpleSwitch test1Instr;
        private Inst_JDSSAZ349_SimpleSwitch test2Instr;
        private Inst_JDSSAZ349_SimpleSwitch test3Instr;
        private Inst_JDSSAZ349_SimpleSwitch test4Instr;
        private Inst_JDSSAZ349_SimpleSwitch test5Instr;
        private Inst_JDSSAZ349_SimpleSwitch test6Instr;
        private Inst_JDSSAZ349_SimpleSwitch test7Instr;
        private Inst_JDSSAZ349_SimpleSwitch test8Instr;
        #endregion

        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::12::INSTR";
        const string chassisName = "JDS SA-Z349 Chassis";
        const string instrDriverName = "Inst_JDSSAZ349_SimpleSwitch";
        const string instr1Name = "SW1";
        const string instr2Name = "SW2";
        const string instr3Name = "SW3";
        const string instr4Name = "SW4";
        const string instr5Name = "SW5";
        const string instr6Name = "SW6";
        const string instr7Name = "SW7";
        const string instr8Name = "SW8";
        #endregion


        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis object
            testChassis = new Chassis_JDSSAZ349 (chassisName, "Chassis_JDSSAZ349", visaResource);
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            test1Instr = new Inst_JDSSAZ349_SimpleSwitch(instr1Name, instrDriverName, "1", "", testChassis);
            TestOutput(test1Instr, "Created OK");

            test2Instr = new Inst_JDSSAZ349_SimpleSwitch(instr2Name, instrDriverName, "2", "", testChassis);
            TestOutput(test2Instr, "Created OK");

            test3Instr = new Inst_JDSSAZ349_SimpleSwitch(instr3Name, instrDriverName, "3", "", testChassis);
            TestOutput(test3Instr, "Created OK");

            test4Instr = new Inst_JDSSAZ349_SimpleSwitch(instr4Name, instrDriverName, "4", "", testChassis);
            TestOutput(test4Instr, "Created OK");

            test5Instr = new Inst_JDSSAZ349_SimpleSwitch(instr5Name, instrDriverName, "5", "", testChassis);
            TestOutput(test5Instr, "Created OK");

            test6Instr = new Inst_JDSSAZ349_SimpleSwitch(instr6Name, instrDriverName, "6", "", testChassis);
            TestOutput(test6Instr, "Created OK");

            test7Instr = new Inst_JDSSAZ349_SimpleSwitch(instr7Name, instrDriverName, "7", "", testChassis);
            TestOutput(test7Instr, "Created OK");

            test8Instr = new Inst_JDSSAZ349_SimpleSwitch(instr8Name, instrDriverName, "8", "", testChassis);
            TestOutput(test8Instr, "Created OK");
            
            // put them online
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;            
            TestOutput(testChassis, "IsOnline set true OK");

            test1Instr.IsOnline = true;
            test1Instr.EnableLogging = true;
            TestOutput(test1Instr, "IsOnline set true OK");

            test2Instr.IsOnline = true;
            test2Instr.EnableLogging = true;
            TestOutput(test2Instr, "IsOnline set true OK");

            test3Instr.IsOnline = true;
            test3Instr.EnableLogging = true;
            TestOutput(test3Instr, "IsOnline set true OK");

            test4Instr.IsOnline = true;
            test4Instr.EnableLogging = true;
            TestOutput(test4Instr, "IsOnline set true OK");

            test5Instr.IsOnline = true;
            test5Instr.EnableLogging = true;
            TestOutput(test5Instr, "IsOnline set true OK");

            test6Instr.IsOnline = true;
            test6Instr.EnableLogging = true;
            TestOutput(test6Instr, "IsOnline set true OK");

            test7Instr.IsOnline = true;
            test7Instr.EnableLogging = true;
            TestOutput(test7Instr, "IsOnline set true OK");

            test8Instr.IsOnline = true;
            test8Instr.EnableLogging = true;
            TestOutput(test8Instr, "IsOnline set true OK"); 
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_SwitchTest()
        {
            TestOutput("\n\n*** T01_SwitchTest: Switch 1: Tests***");
            test1Instr.SetDefaultState();
            TestOutput("\n\n*** T01_FirstTest: Switch 1: Selected Output 1 O.K. ***");

            test1Instr.SwitchState = 1;
            Assert.AreEqual(test1Instr.SwitchState, 1);

            test1Instr.SwitchState = 2;
            Assert.AreEqual(test1Instr.SwitchState, 2);
            TestOutput("\n\n*** T01_FirstTest: Switch 1: Selected Output 2 O.K. ***");
        
        }

        [Test]
        public void T02_SwitchTest()
        {
            TestOutput("\n\n*** T02_SwitchTest: Switch 2 Tests ***");
            test2Instr.SetDefaultState();

            test2Instr.SwitchState = 1;
            Assert.AreEqual(test2Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 2: Selected Output 1 O.K. ***");

            test2Instr.SwitchState = 2;
            Assert.AreEqual(test2Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 2: Selected Output 2 O.K. ***");
        }

        public void T03_SwitchTest()
        {
            TestOutput("\n\n*** T03_SwitchTest: Switch 3 Tests ***");
            test3Instr.SetDefaultState();

            test3Instr.SwitchState = 1;
            Assert.AreEqual(test3Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 3: Selected Output 1 O.K. ***");

            test3Instr.SwitchState = 2;
            Assert.AreEqual(test3Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 3: Selected Output 2 O.K. ***");
        }

        public void T04_SwitchTest()
        {
            TestOutput("\n\n*** T04_SwitchTest: Switch 4 Tests ***");
            test4Instr.SetDefaultState();

            test4Instr.SwitchState = 1;
            Assert.AreEqual(test4Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 4: Selected Output 1 O.K. ***");

            test4Instr.SwitchState = 2;
            Assert.AreEqual(test4Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 4: Selected Output 2 O.K. ***");
        }


        public void T05_SwitchTest()
        {
            TestOutput("\n\n*** T05_SwitchTest: Switch 5 Tests ***");
            test5Instr.SetDefaultState();

            test5Instr.SwitchState = 1;
            Assert.AreEqual(test5Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 5: Selected Output 1 O.K. ***");

            test5Instr.SwitchState = 2;
            Assert.AreEqual(test5Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 5: Selected Output 2 O.K. ***");
        }

        public void T06_SwitchTest()
        {
            TestOutput("\n\n*** T06_SwitchTest: Switch 6 Tests ***");
            test6Instr.SetDefaultState();

            test6Instr.SwitchState = 1;
            Assert.AreEqual(test6Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 6: Selected Output 1 O.K. ***");

            test6Instr.SwitchState = 2;
            Assert.AreEqual(test6Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 6: Selected Output 2 O.K. ***");
        }

        public void T07_SwitchTest()
        {
            TestOutput("\n\n*** T07_SwitchTest: Switch 7 Tests ***");
            test7Instr.SetDefaultState();

            test7Instr.SwitchState = 1;
            Assert.AreEqual(test7Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 7: Selected Output 1 O.K. ***");

            test7Instr.SwitchState = 2;
            Assert.AreEqual(test7Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 7: Selected Output 2 O.K. ***");
        }


        public void T08_SwitchTest()
        {
            TestOutput("\n\n*** T08_SwitchTest: Switch 8 Tests ***");
            test8Instr.SetDefaultState();

            test8Instr.SwitchState = 1;
            Assert.AreEqual(test8Instr.SwitchState, 1);
            TestOutput("\n\n*** Switch 8: Selected Output 1 O.K. ***");

            test8Instr.SwitchState = 2;
            Assert.AreEqual(test8Instr.SwitchState, 2);
            TestOutput("\n\n*** Switch 8: Selected Output 2 O.K. ***");
        }

        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

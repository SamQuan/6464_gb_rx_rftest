// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SAZ349_SimpleSwitch.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// JDS SAZ349 8x1x2 Optical Switch Instrument Driver
    /// </summary>
    public class Inst_JDSSAZ349_SimpleSwitch : InstType_SimpleSwitch, IInstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JDSSAZ349_SimpleSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "JDS FITEL Inc., SA Switch",			// hardware name 
                "0",			                    // minimum valid firmware version 
                "1.10");		                    // maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JDSSAZ349",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JDSSAZ349)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                System.Threading.Thread.Sleep(1000);
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                System.Threading.Thread.Sleep(1000);
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.SwitchState = 1;
            state = 1;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JDSSAZ349 instrumentChassis;

        private int state;
        #endregion

        /// <summary>
        /// Gets the Maximum Switch State
        /// </summary>
        public override int MaximumSwitchState
        {
            get 
            { 
                return 2; 
            }
        }


        /// <summary>
        /// Gets the Minimum Switch State
        /// </summary>
        public override int MinimumSwitchState
        {
            get
            {
                return 1;
            }
        }


        /// <summary>
        /// Get/Set The Switch State
        /// </summary>
        public override int SwitchState
        {
            get
            {
                return state;
            }
            set
            {
                //Validate valid switch position.
                if ((value >= 1) && (value <= 2))
                {
                    //Build cmd string
                    string gpibCmd = "SWITCH " + this.Slot.ToString() + " 1 " + value;
                    state = value;
                    instrumentChassis.Write_Unchecked(gpibCmd, this);
                }
                else
                {
                    throw new InstrumentException("Invalid switch Position Specified: " + value);
                }
            }
        }
    }
}

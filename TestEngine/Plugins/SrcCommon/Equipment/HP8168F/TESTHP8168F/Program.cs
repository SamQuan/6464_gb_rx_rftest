
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.ChassisNS;

namespace TESTHP8168F
{
    class Program
    {
        static void Main(string[] args)
        {
            Chassis_HP8168F TestChassis;
            Inst_HP8168F_TunableLaserSource TestInstrument;
            string VisaResource = "visa://szn-submarpin01/GPIB0::8::INSTR";
            TestChassis = new Chassis_HP8168F("TestChassis", "Chassis_HP8168F", VisaResource);
            TestInstrument = new Inst_HP8168F_TunableLaserSource("TestInstrument", "Inst_HP8168F_TunableLaserSource", "1", "", TestChassis);
           
            TestChassis.IsOnline = true;
            TestInstrument.IsOnline = true;
            TestInstrument.BeamEnable = true;
            TestInstrument.SetDefaultState();
            TestInstrument.WavelengthINnm = 1520;
            TestInstrument.BeamEnable = true;
            TestInstrument.BeamEnable = false;
        }
    }
}

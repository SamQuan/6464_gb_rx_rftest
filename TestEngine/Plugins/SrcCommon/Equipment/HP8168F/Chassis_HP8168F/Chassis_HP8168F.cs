// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_HP8168F.cs
//
// Author: jerryxw.hu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_HP8168F : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_HP8168F(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // maximum allowed chassis version - if we know of a newer good firmware version,
            // update the following line and all the chassis variants will be updated!

            string maxChassisVersion = "V6.85(1126)";
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            // Add Ag8168F details
            //HEWLETT-PACKARD,HP8168F,3612G01208,6.02
            ChassisDataRecord hp8168FData = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8168F",			// hardware name 
                "0",								// minimum valid firmware version 
                maxChassisVersion);					// maximum valid firmware version 
            ValidHardwareData.Add("HP8168F", hp8168FData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {

                // setup base class
                base.IsOnline = value;
                if (value) // if setting online                
                {
                    // Set up Chassis standard event register mask
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write_Unchecked("*CLS", null);
                }
            }
        }
        /// <summary>
        /// Queries an IEEE standard binary block from an Agilent Chassis
        /// </summary>
        /// <param name="cmd">Command to send to equipment</param>
        /// <param name="i">Equipment reference</param>
        /// <returns>Reply byte array</returns>
        /// <remarks>Need new version to deal with Agilent extra termination character!</remarks>
        public new byte[] QueryIEEEBinary_Unchecked(string cmd, Instrument i)
        {
            // get the file data
            byte[] bytes = base.QueryIEEEBinary_Unchecked(cmd, i);
            // Agilent appends a linefeed character after this, so need to read this back too
            // so that it doesn't mess up any future comms. (ignore it)
            this.Read_Unchecked(null);
            return bytes;
        }
        #endregion        
    }
}

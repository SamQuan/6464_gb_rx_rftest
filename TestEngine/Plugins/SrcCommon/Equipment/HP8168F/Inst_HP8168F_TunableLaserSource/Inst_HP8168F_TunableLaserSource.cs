// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_HP8168F_TunableLaserSource.cs
//
// Author: edfaline, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_HP8168F_TunableLaserSource : InstType_TunableLaserSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_HP8168F_TunableLaserSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "HP8168F",				// hardware name 
                "0",  			// minimum valid firmware version 
                "8.00");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            InstrumentDataRecord hp8168FData = new InstrumentDataRecord(
                "Chassis_HP8168F",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("HP8168F", hp8168FData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instChassis = (Chassis_HP8168F)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string response = this.instChassis.Query_Unchecked("*IDN?", this);
                string[] idParts = response.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return idParts[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format("*IDN?", this.Slot);
                string resp = this.instChassis.Query_Unchecked(command, this);
                if (resp.Length == 0)
                {
                    string errStr = String.Format("No 8168F TLS available in '{0}'", this.ChassisName);
                    throw new InstrumentException(errStr);
                }
                // Model Nbr after 1st comma
                string hwID = resp.Split(',')[1];

                return hwID;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.Timeout_ms = 5000;
            this.WavelengthINnm = 1540.0;
            this.PowerUnits = EnPowerUnits.dBm;
            this.Power = 0;
            this.BeamEnable = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        /// <summary>
        /// Reads back the laser-on rise time (from the instrument) and waits this period to ensure the laser power 
        /// has been reached before continuing.
        /// </summary>
        private void WaitForLaserRiseTime()
        {
            System.Threading.Thread.Sleep(1000);
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool BeamEnable
        {
            get
            {
                bool beamState = false;
                string myQuery = string.Format("OUTP:STAT?");

                beamState = (Convert.ToInt32(this.instChassis.Query(myQuery, this)) > 0);

                return beamState;
            }
            set
            {
                /*
				 * CHECK SIGNAL-LOCK STATE OF THE TLS. ATTEMPT UNLOCK USING DEFAULT PASSWORD IF IT IS LOCKED
				 * -----------------------------------------------------------------------------------------
				 */

                if (Convert.ToInt32(this.instChassis.Query_Unchecked(":LOCK?", this)) == 1)
                {
                    /* signal lock engaged, attempt to unlock with the default password (1234) */
                    this.instChassis.Write(":LOCK 0, 1234", this, true, 10, 100);

                    /* if the instrument is still locked, throw a signal-lock exception */
                    if (Convert.ToInt32(this.instChassis.Query(":LOCK?", this)) != 0)
                    {
                        throw new InstrumentException("The signal-lock could not be disabled. The unlock process expects the instrument's " +
                            "signal-lock password to be set to the default (1234)");
                    }
                }

                /*
                 * CHANGE THE BEAM ENABLE STATE FOR THE SOURCE
                 * -------------------------------------------
                 */
                string myCommand = string.Format("OUTP:STAT {0}", ((value == true) ? "1" : "0"));
                this.instChassis.Write(myCommand, this, true, 10, 100);

                /*
                 * WAIT UNTIL THE LASER-ON RISE TIME CONDITION HAS BEEN SATISFIED
                 * --------------------------------------------------------------
                 */
                this.WaitForLaserRiseTime();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MaximumOuputPower
        {
            get
            {
                string myQuery = string.Format("SOUR:POW? MAX");
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MaximumWavelengthINnm
        {
            get
            {
                //read back the maximum settable wavelength (value returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR:WAVE? MAX");
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MinimumOutputPower
        {
            get
            {
                string myQuery = string.Format("SOUR:POW? MIN");
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MinimumWavelengthINnm
        {
            get
            {
                //read back the minimum settable wavelength (value returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR:WAVE? MIN");
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double Power
        {
            get
            {
                string myQuery = string.Format("SOUR:POW?");
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
            set
            {
                /*
				 * SET THE POWER LEVEL IN dBm
				 */
                this.instChassis.Write(string.Format("SOUR:POW {0}",value.ToString()), this, true, 100, 100);

                /*
                 * WAIT UNTIL THE LASER-ON RISE TIME CONDITION HAS BEEN SATISFIED
                 * --------------------------------------------------------------
                 */
                this.WaitForLaserRiseTime();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                EnPowerUnits units = EnPowerUnits.Unspecified;

                string myQuery = string.Format("SOUR:POW:UNIT?");
                string response = this.instChassis.Query(myQuery, this);

                int unitsId = int.MinValue;
                if (int.TryParse(response, out unitsId))
                {
                    if (unitsId == 0) units = EnPowerUnits.dBm;
                    else if (unitsId == 1) units = EnPowerUnits.mW;
                }

                return units;
            }
            set
            {
                if (value == EnPowerUnits.Unspecified) throw new Exception("unspecified is not a valid type of unit to be set!");
                else
                {
                    string myCommand = string.Format("SOUR:POW:UNIT {0}", ((value == EnPowerUnits.dBm) ? "DBM" : "W"));
                    this.instChassis.Write(myCommand, this, true, 10, 100);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double WavelengthINnm
        {
            get
            {
                //read back the output wavelength (returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR:WAVE?");
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
            set
            {
                this.instChassis.Write(
                    string.Format("SOUR:WAVE {0}NM", value.ToString()), this, true, 10, 100);
            }
        }

        //Add by jerry
        public bool ModulationEnable
        {
            get
            {
                // Query the Modulation state
                bool Modulationstate = false;
                string myQuery = string.Format("SOUR:AM:STAT?", this);
                Modulationstate = (Convert.ToInt32(this.instChassis.Query(myQuery, this)) > 0);
                // Return bool value
                return Modulationstate;
            }
            set
            {
                string myCommand = string.Format("SOUR:AM:STAT {0}",((value == true) ? "1" : "0"),this);
                this.instChassis.Write(myCommand, this, true, 10, 100);
            }
        }
        public double CoherenceControl
        {
            get
            {
                // Query the CoherenceControl state
                double coherenceControl=0;
                string myQuery = string.Format(":STAT:QUES:ENABLE?", this);
                coherenceControl =Convert.ToDouble( this.instChassis.Query(myQuery, this));
                // Return string value
                return coherenceControl;
            }


            set
            {
                string myCommand = string.Format(":STAT:QUES:ENABLE {0}", value,this);
                this.instChassis.Write(myCommand, this, true, 10, 100);
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_HP8168F instChassis = null;

        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_GPmdl002.cs
//
// Author: 
// Design: As specified in Ag8156 driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// GP  mdl002 optical attenuator module driver
    /// </summary>
    public class Inst_GPmdl002_Attenuator : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_GPmdl002_Attenuator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all OzDd100mc details
            InstrumentDataRecord GPmdl002InstData = new InstrumentDataRecord(
                "Hardware_Unknown",      // hardware name 
                "Firmware_Unknown",  			                // minimum valid firmware version 
                "Firmware_Unknown");			            // maximum valid firmware version 
            
            GPmdl002InstData.Add("MinWavelength_nm", "1200");    // minimum wavelength
            GPmdl002InstData.Add("MaxWavelength_nm", "1650");    // maximum wavelength
            GPmdl002InstData.Add("MaxAttenuation_dB", "60");     // maximum attenuation

            ValidHardwareData.Add("OzDd100mc", GPmdl002InstData);

            // Configure valid chassis information
            // Add Chassis_OzDd100mc chassis details
            InstrumentDataRecord GPmdl002ChassisData = new InstrumentDataRecord(
                "Chassis_GPmdl002",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "1.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("GPmdl002", GPmdl002ChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_GPmdl002) base.InstrumentChassis;
            
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
               return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            chassisWrite("*RST");       
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        #endregion

        #region MDL-002 function

        /// <summary>
        /// set the delay time in ps
        /// </summary>
        /// <param name="delayTime_ps">delay time in ps</param>
        public void SetDelayTime(double delayTime_ps)
        {
            string value = delayTime_ps.ToString("##.##");
            this.chassisWrite("_ABS_ " + value + "$");
        }

        /// <summary>
        /// relative zero position set
        /// </summary>
        /// <param name="zeroPosition_ps">zero position in ps</param>
        public void SetZeroPosition(double zeroPosition_ps)
        {
            string value = zeroPosition_ps.ToString("##.##");
            this.chassisWrite("_REL_ " + value + "$");
        }

        /// <summary>
        /// Scan Start Position
        /// </summary>
        /// <param name="startPosition_ps"></param>
        public void ScanStartPosition(double startPosition_ps)
        {
            string value = startPosition.ToString("##.##");
            this.chassisWrite("_SC1_ " + value + "$");
        }

        /// <summary>
        /// Scan end position
        /// </summary>
        /// <param name="endPosition_ps"></param>
        public void ScanEndPosition(double endPosition_ps)
        {
            string value = endPosition_ps.ToString("##.##");
            this.chassisWrite("_SC2_ " + value + "$");
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="speed"></param>
        public void ScanSpeedSetting(ScanSpeed speed)
        {
            string value = speed.ToString("##.##");
            this.chassisWrite("_SPD_" + value + "$");
        }

        /// <summary>
        /// reset to absolute zero position
        /// </summary>
        public void ResetToAbsoluteZeroPosition()
        {
            this.chassisWrite("_ORG_$");
        }

        /// <summary>
        /// start to scan
        /// </summary>
        public void StartSan()
        {
            this.chassisWrite("_SST_$");
        }

        /// <summary>
        /// stop motor motion
        /// </summary>
        public void StopMotorMotion()
        {
            this.chassisWrite("_STP_$");
        }

        /// <summary>
        /// set the delay unit in mm or ps
        /// </summary>
        /// <param name="unit">delay unit in mm or ps</param>
        public void SetDelayUnit(delayUnit unit)
        {
            switch (unit)
            {
                case delayUnit .mm:
                    this.chassisWrite("_MMU_$");
                case delayUnit .ps:
                    this.chassisWrite("_PSU_$");
            }
        }

        #endregion

        #region Helper functions

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            string returnData;
            int i = 0;
            do
            {
                this.instrumentChassis.WriteLine(command, this);
                returnData = this.instrumentChassis.ReadLine(this);
                i++;
            } while (returnData != "OK" || i < 3);

            if (returnData!="OK")
            {
                throw new InstrumentException("can not receive 'OK',error command response!");
            }
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// </summary>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_GPmdl002 instrumentChassis;

        enum ScanSpeed
        { 
            zero,
            one,
            two,
            three,
            four,
            five,
            six,
            seven,
            eight,
            nine,
        }

        enum delayUnit
        { 
            mm,
            ps
        }

        #endregion
    }
}

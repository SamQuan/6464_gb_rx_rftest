// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestSolution.Chassis
//
// Chassis_GPmdl002.cs
//
// Author: 
// As per GP MDL-002 driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for GP MDL-002 Optical Attenuator.
    /// </summary>
    public class Chassis_GPmdl002 : ChassisType_Serial
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="visaResourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_GPmdl002(string chassisNameInit, string driverNameInit,
            string comResourceString)
            : base(chassisNameInit, driverNameInit, comResourceString)
        {
            //Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "Hardware_Unknown",			// hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);

            Configure(9600, 8, System.IO.Ports.StopBits.One, System.IO.Ports.Parity.None,
                System.IO.Ports.Handshake.None, 512, 512);
            NewLine = "/r/n";
            
        }
        #endregion

        public override string FirmwareVersion
        {
            get 
            {
                return "Firmware_Unknown";
            }
        }

        public override string HardwareIdentity
        {
            get 
            {
                return "Hardware_Unknown";
            }
        }

    }
}

using System;
using System.Collections.Generic;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using System.Collections;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Microsoft.Win32;



namespace Equip_Test1
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_TekAFG320 testChassis;
        private InstType_AcousticOptoModulator testAOM1;
        private InstType_AcousticOptoModulator testAOM2;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB1::2::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string attName = "Instrument";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            ////// create chassis objects
            ////testChassis = new Chassis_TekAFG320(chassisName, "Chassis_TekAFG320", visaResource);
            ////TestOutput(chassisName, "Created OK");
            // create chassis objects
            string visa;
            // access value from registry
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\EDFAInstruments\\AFG320", true);
            if (key == null) key = Registry.LocalMachine.CreateSubKey("Software\\EDFAInstruments\\AFG320");
            visa = (string)key.GetValue("visaResource");
            if (visa == null)
            {
                visa = visaResource;
                key.SetValue("visaResource", visa); // populate with default

            }

            visa = Microsoft.VisualBasic.Interaction.InputBox("Enter the GPIB resource", "GPIB Resource", visa, 0, 0);
            // back into registry
            key.SetValue("visaResource", visa); // populate
            testChassis = new Chassis_TekAFG320(chassisName, "Chassis_TekAFG320", visa);
            
            TestOutput(chassisName, "Created OK");
            
            
            // create instrument objects            
            testAOM1 = new Inst_TekAFG320_NEOS_AOM(attName, "TekAFG320_NEOS", "1", "", testChassis);
            testAOM2 = new Inst_TekAFG320_NEOS_AOM(attName, "TekAFG320_NEOS", "2", "", testChassis);
            TestOutput(attName, "Created OK");

            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");

            testAOM1.IsOnline = true;
            testAOM2.IsOnline = true;
            TestOutput(attName, "IsOnline set true OK");


            // put them online
            // TODO...
            //TestOutput("Don't forget to put equipment objects online");
            //testChassis.IsOnline = true;
            //TestOutput(chassisName, "IsOnline set true OK");
            //testDMM_1.IsOnline = true;
            //TestOutput(instr1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            //TestOutput("Don't forget to take the chassis offline!");
            testAOM1.IsOnline = false;
            testAOM2.IsOnline = false;
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            //Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            //Assert.IsNaN(Double.NaN);
            testChassis.IsOnline = true;
            testAOM1.IsOnline = true;
            testAOM2.IsOnline = true;

           for(int i = 0;i < 10;i++)
            {
                testAOM2.DutyCycle_percent = 25;
            }


            for (int i = -360; i <=360; i = i + 10)
            {
                testAOM1.PhaseOffset_degrees = Convert.ToSingle(i);
                TestOutput("AMO1 Phase = " + Convert.ToString(i) + " " + testAOM1.PhaseOffset_degrees);
              
                testAOM2.PhaseOffset_degrees = Convert.ToSingle(i * -1);
                TestOutput("AOM2 Phase = " + Convert.ToString(i * -1) + " " + testAOM2.PhaseOffset_degrees);
               
            }
            testAOM1.PhaseOffset_degrees = 0;
            testAOM2.PhaseOffset_degrees = 0;
        }

        [Test]
        public void T02_SecondTest()
        {
            for (int i = 600; i <= 701; i = i + 1)
            {
                testAOM1.Frequency_Hz = Convert.ToSingle(i * 1000);
                TestOutput("AMO1 Freq = " + Convert.ToString(i * 1000) + " " + testAOM1.Frequency_Hz);

                testAOM2.Frequency_Hz = Convert.ToSingle(i * 1000);
                TestOutput("AOM2 Freq = " + Convert.ToString(i * 1000) + " " + testAOM2.Frequency_Hz);

            }
            testAOM1.Frequency_Hz = 701000;
            testAOM2.Frequency_Hz = 701000;
            TestOutput("\n\n*** T02_SecondTest***");
           
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
//using Bookham.TestEngine.PluginInterfaces.Instrument;
//using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;

namespace Ag8156A_Harness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_Ag8156A testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_Ag8156A_Attenuator Att1;        
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::28::INSTR";
        const string chassisName = "8156A Chassis";
        const string instr1Name = "ATT1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis object
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_Ag8156A(chassisName, "Chassis_Ag8156A", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            Att1 = new Inst_Ag8156A_Attenuator(instr1Name, "Inst_Ag8156A_Attenuator", "", "", testChassis);
            TestOutput(instr1Name, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            Att1.IsOnline = true;
            TestOutput(instr1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {            
            TestOutput("\n\n*** T01_FirstTest ***");
            
            Att1.SetDefaultState();

            Att1.OutputEnabled = false;
            Att1.OutputEnabled = true;

            Att1.Wavelength_nm = 1500;

            Att1.Attenuation_dB = 20;            
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            //Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

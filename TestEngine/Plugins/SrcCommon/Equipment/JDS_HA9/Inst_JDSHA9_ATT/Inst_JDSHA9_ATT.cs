// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// JDS HA9 Attenuator Instrument Driver
    /// This is a SCPI Driver for the HA9;
    /// Please ensure that the unit is in SCPI mode
    /// Turn off, hold the local button, turn on
    /// Select SCPI with the up/down buttons, press local
    /// Turn off / on and tis done.
    /// </summary>
    
    public class Inst_JDSHA9_ATT : InstType_OpticalAttenuator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JDSHA9_ATT(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord jdsha9Data = new InstrumentDataRecord(
                "HA9L",			// hardware name 
                "0",			// minimum valid firmware version 
                "02.423");		// maximum valid firmware version
            ValidHardwareData.Add("JDSHA9", jdsha9Data);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JDSHA9",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_JDSHA9", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JDSHA9)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string hard = instrumentChassis.Query("*IDN?", null);
                string[] info = hard.Split(',');
                return info[3].ToString();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string hard = instrumentChassis.Query("*IDN?", null);
                string[] info = hard.Split(',');
                return info[1].ToString();
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.CalibrationFactor_dB = 0F;
            this.Wavelength_nm = 1540F;
            this.Attenuation_dB = 50F;
            double att = this.Attenuation_dB;
            this.OutputEnabled= false;
            // change the timeout_ms to 20s
            this.Timeout_ms = 20000;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
                //this.SetDefaultState();

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region ATTENUATOR InstrumentType property overrides

        /// <summary>
        /// Reads / Set the attenuation in dB
        /// </summary>
        /// <value>
        /// The set value should be between 0 and 60
        /// </value>
        public override double Attenuation_dB
        {
            get
            {

                attenuationAtt = Convert.ToDouble(instrumentChassis.Query(":INPUT:ATT?", this, times));
                 

                return attenuationAtt;
            }
            set
            {
                attenuationAtt = Convert.ToDouble(value);

                instrumentChassis.Write(":INPUT:ATT " + attenuationAtt.ToString(), this, times);



            }
        }


        /// <summary>
        /// Enables / disables the output of the device
        /// </summary>
        /// <value>
        /// Boolean True=ON false =OFF
        /// </value>

        public override bool OutputEnabled
        {
            get
            {
                return outputstateAtt;
            }
            set
            {
                switch (value)
                {
                    case true:
                        outputstateAtt = true;
                        instrumentChassis.Write(":OUTP:STAT 1", this,times);
                        break;
                    case false:
                        outputstateAtt = false;
                        instrumentChassis.Write(":OUTP:STAT 0", this,times);
                        break;
                }

            }
        }

        /// <summary>
        /// Sets a calibration offset in dB
        /// </summary>
        /// <value>
        /// Value in dB in range 0 to 60
        /// </value>
        public override double CalibrationFactor_dB
        {
            get
            {
                return calloffsetAtt;
            }
            set
            {
                calloffsetAtt = Convert.ToDouble(value);
                instrumentChassis.Write(":INPUT:OFFS " + calloffsetAtt.ToString(),this,times);
            }
        }

        /// <summary>
        /// Sets the instruments wavelength in nm
        /// </summary>
        public override double Wavelength_nm 
        {
            get
            {
                wavelengthAtt = Convert.ToDouble(instrumentChassis.Query (":INP:WAV?", this,times));
                return wavelengthAtt * Math.Pow(10, 9);
            }
            set
            {
                wavelengthAtt = Convert.ToDouble (value);
                instrumentChassis.Write(":INP:WAV " + wavelengthAtt.ToString() + "NM",this,times);
            }
        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JDSHA9 instrumentChassis;
        private double wavelengthAtt;
        private double attenuationAtt;
        private double calloffsetAtt;
        private bool outputstateAtt;
        private int times = 10;
        #endregion

        #region public property
            
        /// <summary>
        /// For the reason that the HA9 always report an *OPC error, if you ues the method Query and Write with the override(string, int, int), the second
        /// int is the retry times. When query or write to HA9, if *OPC happen, it will retry. The default value is 10 tiems.
        /// </summary>
        public int retryTimes
        {
            get { return times; }
            set { times = value; }
        }
	
        #endregion
    }
}

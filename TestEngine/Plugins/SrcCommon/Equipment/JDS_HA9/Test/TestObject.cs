using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
//using Bookham.TestSolution.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using System.Collections;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Equip_Test1
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_JDSHA9 testChassis;
        private Inst_JDSHA9_ATT testATT;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::5::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string attName = "Instrument";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_JDSHA9(chassisName, "Chassis_JDSHA9", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testATT = new Inst_JDSHA9_ATT(attName, "JDSHA9", "", "", testChassis);
            TestOutput(attName, "Created OK");

            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");

            testATT.IsOnline = true;
            TestOutput(attName, "IsOnline set true OK");


            // put them online
            // TODO...
            //TestOutput("Don't forget to put equipment objects online");
            //testChassis.IsOnline = true;
            //TestOutput(chassisName, "IsOnline set true OK");
            //testDMM_1.IsOnline = true;
            //TestOutput(instr1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            //TestOutput("Don't forget to take the chassis offline!");
            testATT.IsOnline = false;
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            //Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            //Assert.IsNaN(Double.NaN);
            testChassis.IsOnline = true;
            testATT.IsOnline = true;

            testATT.OutputEnabled = true;

            decimal itemsInuse =0;
            IDictionary<string,decimal> DE= new Dictionary<string,decimal>();

            for (int i = 0; i <=50; i++)
            {
                testATT.Attenuation_dB = i;
                TestOutput("Attenuation = " + i.ToString());
                DE.Add("Att" + i, i);
            }
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

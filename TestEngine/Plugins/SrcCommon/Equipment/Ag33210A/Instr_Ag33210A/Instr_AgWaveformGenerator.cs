// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_Ag33120A.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Instr_AgWaveformGenerator : Instrument
    {
        public enum WaveType
        {   //sinusoid wave
            SIN,
            // square wave
            SQU,
            //ramp wave
            RAMP,
            //pulse wave
            PULS,
            //noise wave
            NOIS,
            //DC wave
            DC,
            //user define wave
            USER
        }

        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_AgWaveformGenerator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_Ag33220A = new InstrumentDataRecord(
                "Agilent Technologies 33220A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "2.00-2.00-22-2");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33220A", instr_Ag33220A);

            InstrumentDataRecord chassis_Ag33220A = new InstrumentDataRecord(
                "Chassis_AgWaveformGenerator",
                "0.0.0.0",
                "4.0.0.0");
            ValidChassisDrivers.Add("Ag33220A", chassis_Ag33220A);


            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_AgWaveformGenerator)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[3].Trim();
               
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[0] + " " + idnArray[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.OutputEnabled = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion


        #region signal generater

        public bool OutputEnableSysnc
        {
            get
            {
                bool outputState = false;
                string command = "OUTP:SYNC?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);

                if (str == "1")
                {
                    outputState = true;
                }
                return outputState;
            }
            set
            {
                string command = "OUTP:SYNC OFF";
                if (value)
                {
                    command = "OUTP:SYNC ON";
                }
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }



        public void SetTriggerToBus()
        {
            string command = "TRIG:SOUR BUS";
            this.instrumentChassis.Write_Unchecked(command, this);

        }
        
        
        /// <summary>
        /// set/get wave function
        /// </summary>
        public WaveType WaveFunction
        {
            get
            {
                string command = "FUNC?";
                string str= this.instrumentChassis.Query_Unchecked(command, this);
                waveFunction= (WaveType)Enum.Parse(typeof (WaveType),str);
                return waveFunction;
            }
            set 
            {
                string command = "FUNC " + Enum.GetName(typeof(WaveType), value);
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// set/get Frequency of waveform
        /// </summary>
        public double Frequency_Hz
        {
            get
            {
                string command = "frequency?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                frequency = double.Parse(str);
                return frequency;
            }
            set
            {
                string command = "frequency " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// set/get Amplitude of waveform
        /// </summary>
        public double Amplitude_Volt
        {
            get
            {
                string command = "VOLT?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                amplitude = double.Parse(str);
                return amplitude;
            }
            set
            {
                string command = "VOLT " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// set/get dc offset 
        /// </summary>
        public double DcOffset_Volt
        {
            get
            {
                string command = "VOLT:OFFS?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                dcOffset = double.Parse(str);
                return dcOffset;
            }
            set
            {
                string command = "VOLT:OFFS " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// set/get output enable
        /// </summary>
        public bool OutputEnabled
        {
            get
            {
                bool outputState = false;
                string command = "OUTP?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                
                if (str == "1")
                {
                    outputState = true;
                }
                return outputState;
            }
            set
            {
                string command = "OUTP OFF";
                if (value)
                {
                    command = "OUTP ON";
                }
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_AgWaveformGenerator instrumentChassis;

        private double frequency;
        private double amplitude;
        private WaveType waveFunction;
        private double dcOffset;
        
        #endregion

    }
}

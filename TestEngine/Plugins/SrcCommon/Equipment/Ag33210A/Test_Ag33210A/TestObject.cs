using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Test_Ag33120A
{
    class TestObject
    {
        public void Init()
        {
            address = "USB0::0x0957::0x0407::MY44017579::INSTR";

            chassis = new Chassis_AgWaveformGenerator("Ag33220A", "Chassis_AgWaveformGenerator", address);
            instrument = new Instr_AgWaveformGenerator("Instr_33220A", "Instr_AgWaveformGenerator", "", "", chassis);

            chassis.IsOnline = true;
            instrument.IsOnline = true;
        }

        public void Run()
        {
            instrument.WaveFunction = Instr_AgWaveformGenerator.WaveType.NOIS;

            instrument.Frequency_Hz = 500 * 1000;
            double freq = instrument.Frequency_Hz;

            instrument.Amplitude_Volt = 2;
            double amplitude = instrument.Amplitude_Volt;

            instrument.OutputEnabled = true;
            bool outputState = instrument.OutputEnabled;

            instrument.OutputEnableSysnc = true;
            outputState = instrument.OutputEnableSysnc;
        }

        public void shutDown()
        {
            chassis.IsOnline = false;
            instrument.IsOnline = false;

        }

        #region private parameter

        private Chassis_AgWaveformGenerator chassis;
        private Instr_AgWaveformGenerator instrument;

        private string address;

        #endregion
    }
}

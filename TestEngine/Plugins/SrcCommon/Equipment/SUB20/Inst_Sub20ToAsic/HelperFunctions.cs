using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    internal static class HelperFunctions
    {
        internal static string HexToString(string hexString)
        {
            if (hexString.Length % 2 != 0)
                throw new Exception("Response string does not contain 2 byte pairs");

            StringBuilder sb = new StringBuilder(hexString.Length / 2);
            for (int index = 0; index < hexString.Length; index += 2)
            {
                sb.Append((char)(Convert.ToByte(hexString.Substring(index, 2), 16)));
            }
            return sb.ToString();
        }

        internal static double HexToDouble(string hexString)
        {
            return Convert.ToDouble(hexString);
        }

        internal static byte HexToByte(string hexString)
        {
            byte a = Convert.ToByte(hexString.Substring(0, 2), 16);
            return a;
        }

        internal static byte[] StringToByteArray(string stringToConvert)
        {
            char[] charArray = stringToConvert.ToCharArray();

            byte[] returnArray = new byte[stringToConvert.Length];
            for (int index = 0; index < stringToConvert.Length; index++)
            {
                returnArray[index] = Convert.ToByte(charArray[index]);
            }

            return returnArray;
        }

        internal static string ByteArrayToString(byte[] byteArray)
        {
            byte[] localArray = (byte[])(byteArray.Clone());
            //Array.Reverse(localArray);

            StringBuilder sb = new StringBuilder(localArray.Length);
            //sb.Append(byteArray);
            foreach (byte character in localArray)
            {
                sb.Append(Convert.ToChar(character));
            }
            return sb.ToString();
        }

        internal static long HexToLong(string hexString)
        {
            return Convert.ToInt64(hexString, 16);
        }

        internal static int HexToInt(string hexString)
        {
            return Convert.ToInt16(hexString, 16);
        }
        internal static int HexToUInt(string hexString)
        {
            return Convert.ToUInt16(hexString, 16);
        }
        internal static String ToHex(byte byteToConvert)
        {
            string result = byteToConvert.ToString("x").PadLeft(2, '0');
            return result.Remove(0, result.Length - 2).ToUpper();
        }

        internal static String ToHex(int intToConvert)
        {
            string result = intToConvert.ToString("x").PadLeft(4, '0');
            return result.Remove(0, result.Length - 4).ToUpper();
        }

        internal static String ToHex(long lngToConvert)
        {
            string result = lngToConvert.ToString("x").PadLeft(8, '0');
            return result.Remove(0, result.Length - 8).ToUpper();
        }
        internal static String ToHex(string stringToConvert)
        {
            StringBuilder sb = new StringBuilder(stringToConvert.Length * 2);
            for (int i = 0; i < stringToConvert.Length; i++)
            {
                sb.Append(ToHex((byte)stringToConvert[i]));
            }
            return sb.ToString().ToUpper();
        }
    }
}

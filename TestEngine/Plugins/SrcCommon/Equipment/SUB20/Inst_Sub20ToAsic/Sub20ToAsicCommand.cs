using System;
using System.Collections.Generic;
using System.Text;
namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// List of commands
    /// </summary>

    public enum Titan_Command
    {
        /// <summary>User defined command</summary>

        UserDefined,
        #region Base Page

        /// <summary>ModuleIdentifier</summary>
        ModuleIdentifier,

        /// <summary>SignalConditionerControl</summary>

        SignalConditionerControl,
        /// <summary>TemperatureHighAlarmThreshold</summary>

        TemperatureHighAlarmThreshold,
        /// <summary>TemperatureLowAlarmThreshold</summary>

        TemperatureLowAlarmThreshold,
        /// <summary>TemperatureHighWarningThreshold</summary>

        TemperatureHighWarningThreshold,
        /// <summary>TemperatureLowWarningThreshold</summary>

        TemperatureLowWarningThreshold,
        /// <summary>PhaseCurrentHighAlarmThreshold</summary>

        SoaCurrentHighAlarmThreshold,
        /// <summary>PhaseCurrentLowAlarmThreshold</summary>

        SoaCurrentLowAlarmThreshold,
        /// <summary>PhaseCurrentWarningAlarmThreshold</summary>

        SoaCurrentHighWarningThreshold,
        /// <summary>PhaseCurrentWarningAlarmThreshold</summary>

        SoaCurrentLowWarningThreshold,
        /// <summary>TxPowerHighAlarmThreshold</summary>

        TxPowerHighAlarmThreshold,
        /// <summary>TxPowerLowAlarmThreshold</summary>

        TxPowerLowAlarmThreshold,
        /// <summary>TxPowerWarningAlarmThreshold</summary>

        TxPowerHighWarningThreshold,
        /// <summary>TxPowerWarningAlarmThreshold</summary>

        TxPowerLowWarningThreshold,
        /// <summary>RxPowerHighAlarmThreshold</summary>

        RxPowerHighAlarmThreshold,
        /// <summary>RxPowerLowAlarmThreshold</summary>

        RxPowerLowAlarmThreshold,
        
        /// <summary>RxPowerHighWarningThreshold</summary>
        RxPowerHighWarningThreshold,

        /// <summary>RxPowerLowWarningThreshold</summary>
        RxPowerLowWarningThreshold,

        /// <summary>AUX1HighAlarmThreshold</summary>

        AUX1HighAlarmThreshold,
        /// <summary>AUX1LowAlarmThreshold</summary>

        AUX1LowAlarmThreshold,
        /// <summary>AUX1WarningAlarmThreshold</summary>

        AUX1HighWarningThreshold,
        /// <summary>AUX1WarningAlarmThreshold</summary>

        AUX1LowWarningThreshold,
        /// <summary>AUX2HighAlarmThreshold</summary>

        AUX2HighAlarmThreshold,
        /// <summary>AUX2LowAlarmThreshold</summary>

        AUX2LowAlarmThreshold,
        /// <summary>AUX2WarningAlarmThreshold</summary>

        AUX2HighWarningThreshold,
        /// <summary>AUX2WarningAlarmThreshold</summary>

        AUX2LowWarningThreshold,
        /// <summary>LasersFirstFrequencyTHz</summary>

        LasersFirstFrequencyTHz,
        /// <summary>LasersFirstFrequency(0.1GHz)</summary>

        LasersFirstFrequency_0pt1GHz,
        /// <summary>LasersLastFrequencyTHz</summary>

        LasersLastFrequencyTHz,
        /// <summary>LasersLastFrequency(0.1GHz)</summary>

        LasersLastFrequency_0pt1GHz,
        /// <summary>LaserGridSpacing(0.1GHz)</summary>

        LaserGridSpacing_0pt1GHz,
        /// <summary>BERreportingfromhost.</summary>

        BERreportingfromhost,
        /// <summary>LaserChannel</summary>

        LaserChannel,
        /// <summary>Laserferquencyerror(0.1GHz)</summary>

        LaserFrequencyError_5pm,
        /// <summary>RatiooferroredonestoerroredzeroestoadjustRxDTV(writtenbyhost)</summary>

        RxDTV,
        /// <summary>Latchedinterruptbits(Temperature,TxPhasecurrentandTxpoweralarms)</summary>
        
        RxPhase,

        LatchedInterruptBits_Temperature_TxPhasecurrent_TxpowerAlarms,
        /// <summary>Latchedinterruptbits(RxPower,AUX1andAUX2alarms)</summary>

        Latchedinterruptbits_RxPower_AUX1_AUX2Alarms,
        /// <summary>Latchedinterruptbits(Temperature,TxPhasecurrentandTxpowerwarnings)</summary>

        Latchedinterruptbits_Temperature_TxPhaseCurrent_TxpowerWarnings,
        /// <summary>Latchedinterruptbits(RxPower,AUX1andAUX2warnings)</summary>

        Latchedinterruptbits_RxPower_AUX1_AUX2Warnings,
        /// <summary>Latchedinterruptbits(TX_NR,TX_Fault,TXLOL,RX_NR,RX_LOS,RXLOL,MOD_NR)</summary>

        Latchedinterruptbits_TX_NR_TX_Fault_TXLOL_RX_NR_RX_LOS_RXLOL_MOD_NR,
        /// <summary>Latchedinterruptbits(APD,TECandTXUnlockedalarms)</summary>

        Latchedinterruptbits_APD_TEC_TXUnlockedalarms,
        /// <summary>Latchedinterruptbits(supplyrailvoltagealarms)</summary>

        Latchedinterruptbits_SupplyRailVoltageAlarms,
        /// <summary>Latchedinterruptbits(supplyrailvoltagewarnings)</summary>

        Latchedinterruptbits_SupplyRailVoltageWarnings,
        /// <summary>Interruptmaskbits(Temperature,TxPhasecurrentandTxpoweralarms)</summary>

        Interruptmaskbits_Temperature_TxPhasecurrentandTxpoweralarms,
        /// <summary>Interruptmaskbits(RxPower,AUX1andAUX2alarms)</summary>

        InterruptmaskbitsRxPowerAUX1andAUX2alarms,
        /// <summary>Interruptmaskbits(Temperature,TxPhasecurrentandTxpowerwarnings)</summary>

        InterruptmaskbitsTemperatureTxPhasecurrentandTxpowerwarnings,
        /// <summary>Interruptmaskbits(RxPower,AUX1andAUX2warnings)</summary>

        InterruptmaskbitsRxPowerAUX1andAUX2warnings,
        /// <summary>Interruptmaskbits(TX_NR,TX_Fault,TXLOL,RX_NR,RX_LOS,RXLOL,MOD_NR)</summary>

        InterruptmaskbitsTX_NRTX_FaultTXLOL_RX_NR_RX_LOS_RXLOL_MOD_NR,
        /// <summary>Interruptmaskbits(APD,TECandTXUnlockedalarms)</summary>

        Interruptmaskbits_APD_TECandTXUnlockedalarms,
        /// <summary>Interruptmaskbits(Supplyrailvoltagealarms)</summary>

        Interruptmaskbits_Supplyrailvoltagealarms,
        /// <summary>Interruptmaskbits(Supplyrailvoltagewarnings)</summary>

        Interruptmaskbits_Supplyrailvoltagewarnings,
        /// <summary>TemperatureMonitor(1/256degreesC)</summary>

        TemperatureMonitor_1over256degreesC,
        /// <summary>SoaCurrentMonitor(4uA)</summary>

        SoaCurrentMonitor_4uA,
        /// <summary>TxPowerMonitor(0.1uW)</summary>

        TxPowerMonitor_0pt1uW,
        /// <summary>RxPowerMonitor(0.1uW)</summary>

        RxPowerMonitor_0pt1uW,
        /// <summary>AUX1Monitor</summary>

        AUX1Monitor,
        /// <summary>AUX2Monitor</summary>

        AUX2Monitor,
        /// <summary>TxDisable/Pdown/Interruptcontrol</summary>

        TxDisable_Pdown_Interruptcontrol,
        /// <summary>TX_NR,TX_Fault,TX_CDR,RX_NR,RX_CDR</summary>

        TX_NR_TX_Fault_TX_CDR_RX_NR_RX_CDR,

        /// <summary>Channel Selection by Channel number</summary>
        ChannelSelectionItu,

        /// <summary>FrequencyError_0pt1GHz</summary>
        LaserFrequencyError_0pt1GHz,

        /// <summary>Packeterrorchecking</summary>

        Packeterrorchecking,
        /// <summary>NewPassword</summary>

        NewPassword,
        /// <summary>Password</summary>

        Password,
        /// <summary>PageSelect</summary>

        PageSelect,
        #endregion

        #region Page 1
        /// <summary>Identifier Type of serial transceiver (see Table 32)</summary>
        IdentifierType,
        /// <summary>Ext. Identifier Extended identifier of type of serial transceiver (see Table 47)</summary>
        ExtendedIdentifier,
        /// <summary>Connector Code for connector type (see Table 48)</summary>
        ConnectorCode,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_10GEthernet,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_10GFibreChannel,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_10GCopper,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_LowerSpeed,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_Interconnect,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_ShortHaul,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_LongHual,
        /// <summary>Transceiver Code for electronic compatibility or optical compatibility (see Table 49)</summary>
        TransceiverCode_TuneableMethods,
        /// <summary>Encoding Code for serial encoding algorithm (see Table 50)</summary>
        EncodingCode,
        /// <summary>BR-Min Minimum bit rate, units of 100 MBits/s.</summary>
        BitRateMin_Mbps,
        /// <summary>BR-Max Maximum bit rate, units of 100 MBits/s.</summary>
        BitRateMax_Mbps,
        /// <summary>Length(SMF)-km Link length supported for SMF fiber in km</summary>
        LinkLengthSMF_km,
        /// <summary>Length (E-50�m) Link length supported for EBW 50/125 �m fiber, units of 2 m</summary>
        LinkLengthE50_2m,
        /// <summary>Length (50 �m) Link length supported for 50/125 �m fiber, units of 1 m</summary>
        LinkLength50_m,
        /// <summary>Length (62.5 �m) Link length supported for 62.5/125 �m fiber, units of 1 m</summary>
        LinkLength62pt5_m,
        /// <summary>Length (Copper) Link length supported for copper, units of 1m</summary>
        LinkLengthCu_m,
        /// <summary>Device Tech Device technology (see Table 51, Table 52)</summary>
        DeviceTechnology,
        /// <summary>Vendor name XFP vendor name (ASCII). String must be padded with spaces (0x20)</summary>
        VendorName,
        /// <summary>CDR Support CDR Rate Support (see Table 53)</summary>
        CDRRate,
        /// <summary>Vendor OUI</summary>
        VendorOUI,
        /// <summary>Vendor PN Part number provided by XFP vendor (ASCII). String must be padded with spaces (0x20)</summary>
        VendorPartNumber,
        /// <summary>Vendor rev Revision level for part number provided by vendor (ASCII)</summary>
        VendorRevision,
        /// <summary>Nominal laser wavelength</summary>
        NominalWavelength,
        /// <summary>Wavelength Tolerance</summary>
        WavelengthTolerance,
        /// <summary>Max Case Temp Maximum Case Temperature in Degrees C.</summary>
        MaxCaseTemperature,
        /// <summary>CC_BASE Checksum of registers 128 to 190 (Modulo 256 sum)</summary>
        Checksum_base,
        /// <summary>CC_BASE Checksum read back for calculating checksum</summary>
        Checksum_base_read,
        /// <summary>Max Power Dissipation (reg value * 40mW)</summary>
        MaxPowerDissipationDiv40mw,
        /// <summary>Max Power Dissipation in power down mode (reg value * 40mW)</summary>
        MaxPowerDissipationPoweredDownDiv40mw,
        /// <summary>Bits 7:4 = 5v supply current (*100ma)  Bits 3:0 = 3.3v supply current (*100mA)</summary>
        SupplyCurrent5v_3v3_div100mA,
        /// <summary>Bits 7:4 = 1.8v supply current (*100ma)  Bits 3:0 = -5.2v supply current (*100mA)</summary>
        SupplyCurrent1v8_m5v2_div100mA,
        /// <summary>Vendor SN Serial number provided by vendor (ASCII). String must be padded with spaces (0x20)</summary>
        VendorSerialNumber,
        /// <summary>Date code in BCD format.</summary>
        DateCode,
        /// <summary>Diagnostic Monitoring Type (see MSA Table 56)</summary>
        DiagnosticMonitoringType,
        /// <summary>Enhanced Options (see MSA Table 57)</summary>
        EnhancedOptions,
        /// <summary>Aux Monitoring (see MSA Table 58 AND 59)</summary>
        AuxMonitoring,
        /// <summary>CC_EXT Checksum of registers 192 to 222 (Modulo 256 sum)</summary>
        Checksum_ext,
        /// <summary>Vendor Specific EEPROM</summary>
        VendorSpecificEEPROM,
        #endregion

        #region Page 2
        #region Alactel-Lucent
        /// <summary>ALU Specific Vedor MemoryLocation</summary>
        ALUCLEICode,
        /// <summary>ALU IPN (Part Code)</summary>
        ALUIPN,
        /// <summary>ALU Name</summary>
        ALUName,
        /// <summary>ALU Compatibility</summary>
        ALUCompatibility,
        /// <summary>ALU Reserved area 1</summary>
        ALUReservered1,
        /// <summary>ALU Reserved area 2</summary>
        ALUReservered2,
        /// <summary>ALU WES Serial Number</summary>
        ALUWESSerial,
        /// <summary>ALU Acronym</summary>
        ALUAcronym,
        /// <summary>ALU Interchangeability Marking</summary>
        ALUInterchangeabilityMarking,
        /// <summary>ALU Memory Checksum</summary>
        ALUChecksum,
        #endregion
        #region ADVA
        /// <summary>ADVA Scheme Number Int Value Byte 1</summary>
        ADVASchemeNumberInt_1,
        /// <summary>ADVA Scheme Number Int Value Byte 2</summary>
        ADVASchemeNumberInt_2,
        /// <summary>ADVA Scheme Number ASCII Value</summary>
        ADVASchemeNumberASCII,
        /// <summary>ADVA Scheme Number Padding</summary>
        ADVASchemeNumberPadding,
        /// <summary>ADVA Telcordia Vendor ID</summary>
        ADVATelcordiaVendorID,
        /// <summary>ADVA Special Programming Byte 1</summary>
        ADVASpecialProgramming_1,
        /// <summary>ADVA Special Programming Byte 2</summary>
        ADVASpecialProgramming_2,
        /// <summary>ADVA Special Programming Byte 3</summary>
        ADVASpecialProgramming_3,
        /// <summary>ADVA Special Programming Byte 4</summary>
        ADVASpecialProgramming_4,
        /// <summary>ADVA Special Programming Byte 5</summary>
        ADVASpecialProgramming_5,
        /// <summary>ADVA Special Programming Byte 6</summary>
        ADVASpecialProgramming_6,
        /// <summary>ADVA Special Programming Byte 7</summary>
        ADVASpecialProgramming_7,
        /// <summary>ADVA Special Programming Byte 8</summary>
        ADVASpecialProgramming_8,
        /// <summary>ADVA Special Programming Byte 9</summary>
        ADVASpecialProgramming_9,
        /// <summary>ADVA Special Programming Byte 10</summary>
        ADVASpecialProgramming_10,
        /// <summary>ADVA Special Programming Byte 11</summary>
        ADVASpecialProgramming_11,
        /// <summary>ADVA Special Programming Byte 12</summary>
        ADVASpecialProgramming_12,
        /// <summary>ADVA Special Programming Byte 13</summary>
        ADVASpecialProgramming_13,
        /// <summary>ADVA Special Programming Byte 14</summary>
        ADVASpecialProgramming_14,
        /// <summary>ADVA Part Number</summary>
        ADVAPartNumber,
        /// <summary>ADVA Serial Number</summary>
        ADVASerialNumber,
        /// <summary>ADVA Type</summary>
        ADVAType,
        /// <summary>ADVA Reach</summary>
        ADVAReach,
        /// <summary>ADVA Bitrate</summary>
        ADVABitrate,
        /// <summary>ADVA Checksum 128-223</summary>
        ADVALowerChecksum,
        /// <summary>ADVA Hardware Revision</summary>
        ADVAHardwareRevision,
        /// <summary>ADVA CLEI Code</summary>
        ADVACLEICode,
        /// <summary>ADVA Zeros</summary>
        ADVAZeros,
        /// <summary>ADVA Checsum 225-246</summary>
        ADVAUpperChecksum,
        #endregion

        #region CYAN
        CyanCompanyName,
        CyanPartNumber,
        #endregion
        #region  Ericsson
        EricssonPrefix,
        EricssonCLEICode,
        EricssonProductNumber,
        EricssonPad,
        EricssonPartNumber,
        EricssonLogo,
        EricssonMisc,
        #endregion

        /// <summary>Frame Type (120 w/ CRC16)</summary>
        FrameType,
        /// <summary>FDU Type (Model ID Long)</summary>
        FDUType_ModelId,
        /// <summary>Model ID</summary>
        ModelId,
        /// <summary>FDU Type (CLEI)</summary>
        FduType_CLEI,
        /// <summary>CLEI Code</summary>
        CLEICode,
        /// <summary>ECI in BCD</summary>
        ECI_BCD,
        /// <summary>Laser Bias Current BOL, 1LSB=4uA. 60mA = 0x3A98</summary>
        LaserBiasBOL_4uA,
        /// <summary>TX Output power BOL, 1LSB=0.1uW, 4.2dBm=26302=0x66BE</summary>
        TxPowerBOL_0pt1uW,
        /// <summary>CN4200 required Market Code</summary>
        CN4200MarketCode,
        #endregion

        #region Page 3
        /// <summary>ALARM_CASE_TEMP_HIGH</summary>
        ALARM_CASE_TEMP_HIGH,
        /// <summary>ALARM_CASE_TEMP_LOW</summary>
        ALARM_CASE_TEMP_LOW,
        /// <summary>WARN_CASE_TEMP_HIGH</summary>
        WARN_CASE_TEMP_HIGH,
        /// <summary>WARN_CASE_TEMP_LOW</summary>
        WARN_CASE_TEMP_LOW,
        /// <summary>ALARM_TX_POWER_HIGH</summary>
        ALARM_TX_POWER_HIGH,
        /// <summary>ALARM_TX_POWER_LOW</summary>
        ALARM_TX_POWER_LOW,
        /// <summary>WARN_TX_POWER_HIGH</summary>
        WARN_TX_POWER_HIGH,
        /// <summary>WARN_TX_POWER_LOW</summary>
        WARN_TX_POWER_LOW,
        /// <summary>ALARM_RX_POWER_HIGH</summary>
        ALARM_RX_POWER_HIGH,
        /// <summary>ALARM_RX_POWER_LOW</summary>
        ALARM_RX_POWER_LOW,
        /// <summary>WARN_RX_POWER_HIGH</summary>
        WARN_RX_POWER_HIGH,
        /// <summary>WARN_RX_POWER_LOW</summary>
        WARN_RX_POWER_LOW,
        /// <summary>Auxilliary monitors selected by Serial ID reg 222</summary>
        ALARM_AUX1_HIGH,
        /// <summary>ALARM_AUX1_LOW</summary>
        ALARM_AUX1_LOW,
        /// <summary>WARN_AUX1_HIGH</summary>
        WARN_AUX1_HIGH,
        /// <summary>WARN_AUX1_LOW</summary>
        WARN_AUX1_LOW,
        /// <summary>ALARM_AUX2_HIGH</summary>
        ALARM_AUX2_HIGH,
        /// <summary>ALARM_AUX2_LOW</summary>
        ALARM_AUX2_LOW,
        /// <summary>WARN_AUX2_HIGH</summary>
        WARN_AUX2_HIGH,
        /// <summary>WARN_AUX2_LOW</summary>
        WARN_AUX2_LOW,
        /// <summary>RX power cal 0 uW (Dark)</summary>
        RX_PMON_CAL_0,
        /// <summary>RX power cal 1.0 uW (-30dBm)</summary>
        RX_PMON_CAL_1,
        /// <summary>RX power cal 1.58 uW (-28dBm)</summary>
        RX_PMON_CAL_2,
        /// <summary>RX power cal 2.51 uW (-26dBm)</summary>
        RX_PMON_CAL_3,
        /// <summary>RX power cal 3.98 uW (-24dBm)</summary>
        RX_PMON_CAL_4,
        /// <summary>RX power cal 6.31 uW (-22dBm)</summary>
        RX_PMON_CAL_5,
        /// <summary>RX power cal 10.0 uW (-20dBm)</summary>
        RX_PMON_CAL_6,
        /// <summary>RX power cal 15.85 uW (-18dBm)</summary>
        RX_PMON_CAL_7,
        /// <summary>RX power cal 25.12 uW (-16dBm)</summary>
        RX_PMON_CAL_8,
        /// <summary>RX power cal 39.81 uW (-14dBm)</summary>
        RX_PMON_CAL_9,
        /// <summary>RX power cal 50.12 uW (-13dBm)</summary>
        RX_PMON_CAL_10,
        /// <summary>RX power cal 63.10 uW (-12dBm)</summary>
        RX_PMON_CAL_11,
        /// <summary>RX power cal 79.43 uW (-11dBm)</summary>
        RX_PMON_CAL_12,
        /// <summary>RX power cal 100.00 uW (-10dBm)</summary>
        RX_PMON_CAL_13,
        /// <summary>RX power cal 158.49 uW (-8dBm)</summary>
        RX_PMON_CAL_14,
        /// <summary>RX power cal 316.23 uW (-5dBm)</summary>
        RX_PMON_CAL_15,
        /// <summary>APD Bias control loop gain</summary>
        RX_LOOP_GAIN,
        /// <summary>APD DAC value to give M=3</summary>
        RX_BIAS_M3_DAC,
        /// <summary>APD DAC value to give M=8</summary>
        RX_BIAS_M8_DAC,
        /// <summary>DAC Temperature compensation mV per degree C</summary>
        RX_TEMPCO_MV_C,
        /// <summary>DSDBR temperature setpoint</summary>
        LS_PID_SETPOINT_CELCIUS,
        /// <summary>DSDBR TEC control loop proportional gain</summary>
        LS_PID_P_GAIN,
        /// <summary>DSDBR TEC control loop integral gain</summary>
        LS_PID_I_GAIN,
        /// <summary>DSDBR TEC control loop derivative gain</summary>
        LS_PID_D_GAIN,
        /// <summary>DSDBR over-temperature threshold before shutdown</summary>
        LS_PID_OVERTEMP_MARGIN,
        /// <summary>DSDBR delay before over-temperature shutdown</summary>
        LS_PID_OVERTEMP_DELAY,
        /// <summary>Optical power output setpoint.</summary>
        LS_POWER_CTRL_SET,
        /// <summary>Power control loop gain, default = 16</summary>
        LS_POWER_CTRL_GAIN,
        #endregion

        #region Page 4
        /// <summary>Channel frequency in GHz - 180000</summary>
        CHAN_PARAM_FREQUENCY,
        /// <summary>Locker polarity switch setting</summary>
        CHAN_LOCKER_POLARITY,
        /// <summary>Front switch setting</summary>
        CHAN_PARAM_SWITCHES,
        /// <summary>Front odd DAC</summary>
        CHAN_PARAM_ODD,
        /// <summary>Front even DAC</summary>
        CHAN_PARAM_EVEN,
        /// <summary>Gain DAC</summary>
        CHAN_PARAM_GAIN,
        /// <summary>Rear DAC</summary>
        CHAN_PARAM_REAR,
        /// <summary>Phase DAC for mode acquisition</summary>
        CHAN_PARAM_PHASE_CENTER,
        /// <summary>Phase DAC for unlocked operation</summary>
        CHAN_PARAM_PHASE_BOL,
        /// <summary>Locker calibration value to achieve ITU</summary>
        CHAN_PARAM_LOCKER_CAL,
        /// <summary>Reflected BFM gain digipot</summary>
        CHAN_PARAM_BFM_REFL,
        /// <summary>Phase monitor EOL alarm value</summary>
        CHAN_PARAM_PHASE_ALARM_HIGH,
        /// <summary>Phase monitor EOL alarm value</summary>
        CHAN_PARAM_PHASE_ALARM_LOW,
        /// <summary>Phase monitor EOL warn value</summary>
        CHAN_PARAM_PHASE_WARN_HIGH,
        /// <summary>Phase monitor EOL warn value</summary>
        CHAN_PARAM_PHASE_WARN_LOW,
        /// <summary>Phase monitor at BOL</summary>
        CHAN_PARAM_PHASE_MON_BOL,
        /// <summary>Locker slope for wavelength error calculation</summary>
        CHAN_PARAM_LOCKER_SLOPE,
        /// <summary>SOA DAC value to give enough power to activate locker</summary>
        CHAN_PARAM_SOA_LOCKER_ACTIVATE,
        /// <summary>SOA DAC value to give rated power output at BOL</summary>
        CHAN_PARAM_SOA_CALIBRATED_PWR,
        /// <summary>Rear SOA Dac Value</summary>
        CHAN_PARAM_REAR_SOA,
        /// <summary>Power monitor slope</summary>
        CHAN_PARAM_POW_MON_SLOPE,
        /// <summary>Power monitor offset</summary>
        CHAN_PARAM_POW_MON_OFFSET,
        /// <summary>Left imbalance DAC</summary>
        CHAN_PARAM_MZ_IMB_LEFT,
        /// <summary>Right imbalance DAC</summary>
        CHAN_PARAM_MZ_IMB_RIGHT,
        /// <summary>Pin 14 bias DAC</summary>
        CHAN_PARAM_MZ_BIAS_14,
        /// <summary>Pin 15 bias DAC</summary>
        CHAN_PARAM_MZ_BIAS_15,
        /// <summary>Pulse width DAC</summary>
        CHAN_PARAM_MZ_PW,
        /// <summary>Modulation DAC</summary>
        CHAN_PARAM_MZ_MOD,
        /// <summary>Crossing control set point</summary>
        CHAN_PARAM_MZ_X_POINT,
        /// <summary>+ve or -ve slope operation</summary>
        CHAN_PARAM_MZ_SLOPE,
        #endregion

        #region Page 5
        /// <summary>RX_BIAS_M3_VOLTS Voltage for M=3 operation @ room temp</summary>
        RX_BIAS_M3_VOLTS,
        /// <summary>RX_BIAS_M8_VOLTS Voltage for M=8 operation @ room temp</summary>
        RX_BIAS_M8_VOLTS,
        /// <summary>RX_POWER_MON_RAW The value in this register should be written to the appropriate RX_PMON_CAL_x register during APD calibration</summary>
        RX_POWER_MON_RAW,
        /// <summary>RX_STATUS Bit 0: APD Bias generator enable, Bit 1: Manual bias control enable, Bit 2: Temperature compensation disable.</summary>
        RX_STATUS,
        /// <summary>RX_VOLTS Current APD voltage</summary>
        RX_VOLTS,
        /// <summary>RX_TEMP Current APD temperature</summary>
        RX_TEMP,
        /// <summary>RX_POWER Received power</summary>
        RX_POWER,
        /// <summary>DEV_DAC_REAR_SOA Rear SOA current source</summary>
        DEV_DAC_REAR_SOA,
        /// <summary>DEV_DAC_ODD Front odd current source DAC ( 0 - 10.46mA )</summary>
        DEV_DAC_ODD,
        /// <summary>DEV_DAC_PHASE Phase current source DAC</summary>
        DEV_DAC_PHASE,
        /// <summary>DEV_DAC_EVEN Front even current source DAC ( 0 - 10.46mA )</summary>
        DEV_DAC_EVEN,
        /// <summary>DEV_DAC_GAIN Gain current source DAC ( 0 - 245mA )</summary>
        DEV_DAC_GAIN,
        /// <summary>DEV_DAC_SOA SOA current source DAC ( 0 = -59mA, 32768 = 0mA, 65535 = 245mA )</summary>
        DEV_DAC_SOA,
        /// <summary>DEV_DAC_REAR Rear current source DAC( 0 - 245mA )</summary>
        DEV_DAC_REAR,
        /// <summary>DEV_DAC_LS_TEC DSDBR TEC current DAC ( 0 ~ +1.1A, 65535 ~ -1.1A)</summary>
        DEV_DAC_LS_TEC,
        /// <summary>DEV_DAC_APD_BIAS APD Bias control ( Volts ~ ( dac / 2.7076 ) + 14131 )</summary>
        DEV_DAC_APD_BIAS,
        /// <summary>DEV_DAC_MZ_PW MZ Pulse-width DAC</summary>
        DEV_DAC_MZ_PW,
        /// <summary>DEV_DAC_MZ_BIAS_14 MZ Bias voltage DAC</summary>
        DEV_DAC_MZ_BIAS_14,
        /// <summary>DEV_DAC_MZ_BIAS_15 MZ Bias voltage DAC</summary>
        DEV_DAC_MZ_BIAS_15,
        /// <summary>DEV_DAC_MZ_MOD MZ modulation depth DAC</summary>
        DEV_DAC_MZ_MOD,
        /// <summary>DEV_DAC_LS_PWC_REF RX LOS threshold</summary>
        DEV_DAC_LS_PWC_REF,
        /// <summary>DEV_DAC_UNUSED unused</summary>
        DEV_DAC_UNUSED,
        /// <summary>DEV_DAC_LS_CAL_TEC unused</summary>
        DEV_DAC_LS_CAL_TEC,
        /// <summary>SPARE_BFMT Transmitted BFM gain digipot</summary>
        SPARE_BFMT,
        /// <summary>SPARE_BFMR Reflected BFM gain digipot</summary>
        SPARE_BFMR,
        /// <summary>DEV_DAC_MZ_IMB_LEFT Imbalance control (0-8mA)</summary>
        DEV_DAC_MZ_IMB_LEFT,
        /// <summary>DEV_DAC_MZ_IMB_RIGHT Imbalance control (0-8mA)</summary>
        DEV_DAC_MZ_IMB_RIGHT,
        /// <summary>DEV_ADC_BFM_TRAN Transmit BFM current</summary>
        DEV_ADC_BFM_TRAN,
        /// <summary>DEV_ADC_BFM_REFL Reflected BFM current</summary>
        DEV_ADC_BFM_REFL,
        /// <summary>DEV_ADC_LS_TEMP Tosa Thermistor</summary>
        DEV_ADC_LS_TEMP,
        /// <summary>SPARE_LSP Sum of BFM currents (optical power into MZ)</summary>
        SPARE_LSP,
        /// <summary>SPARE_LSPHASE Phase current</summary>
        SPARE_LSPHASE,
        /// <summary>SPARE_MZT MZ thermistor</summary>
        SPARE_MZT,
        /// <summary>DEV_ADC_MZ_MMI MZ MMI tap</summary>
        DEV_ADC_MZ_MMI,
        /// <summary>DEV_ADC_RX_POWER APD photo current</summary>
        DEV_ADC_RX_POWER,
        /// <summary>DEV_ADC_1V2_REF 1V2 ref</summary>
        DEV_ADC_1V2_REF,
        /// <summary>DEV_ADC_CASE_TEMP Case thermistor</summary>
        DEV_ADC_CASE_TEMP,
        /// <summary>DEV_ADC_PSU_VCC5 5.0v rail</summary>
        DEV_ADC_PSU_VCC5,
        /// <summary>DEV_ADC_PSU_VCC3 3.3v rail</summary>
        DEV_ADC_PSU_VCC3,
        /// <summary>DEV_ADC_PSU_VCC2 1.8v rail</summary>
        DEV_ADC_PSU_VCC2,
        /// <summary>DEV_ADC_PSU_VEE5 -5.2v rail</summary>
        DEV_ADC_PSU_VEE5,
        /// <summary>DEV_ADC_RX_TEMP APD thermistor</summary>
        DEV_ADC_RX_TEMP,
        /// <summary>LS_PID_SETPOINT_ADC DSDBR temperature setpoint in ADC units</summary>
        LS_PID_SETPOINT_ADC,
        /// <summary>LS_PID_TEMP_CELCIUS DSDBR submount temperature</summary>
        LS_PID_TEMP_CELCIUS,
        /// <summary>LS_PID_ENABLE DSDBR TEC control enable</summary>
        LS_PID_ENABLE,
        /// <summary>MZ_PID_SETPOINT_ADC MZ temperature setpoint in ADC units</summary>
        MZ_PID_SETPOINT_ADC,
        /// <summary>MZ_PID_TEMP_CELCIUS MZ submount temperature</summary>
        MZ_PID_TEMP_CELCIUS,
        /// <summary>MZ_PID_ENABLE MZ TEC control enable</summary>
        MZ_PID_ENABLE,
        /// <summary>PID_STATUS PID control loops status</summary>
        PID_STATUS,
        /// <summary>MZ_AUTOX_SET_POINT Set point for auto eye crossing loop</summary>
        MZ_AUTOX_SET_POINT,
        /// <summary>MZ_AUTOX_ENABLE Enable for auto eye crossing loop</summary>
        MZ_AUTOX_ENABLE,
        /// <summary>MZ_AUTOX_SLOPE Determines which slope of MZ the loop uses</summary>
        MZ_AUTOX_SLOPE,
        /// <summary>CHAN_STORE_IDX Store current channel to the specified EEPROM channel index</summary>
        CHAN_STORE_IDX,
        /// <summary>BFM_LOCKER_ENABLE Wavelength locker enable</summary>
        BFM_LOCKER_ENABLE,
        /// <summary>BFM_LOCKER_SLOPE Locker slope select</summary>
        BFM_LOCKER_SLOPE,
        /// <summary>BFM_INIT_AUTOLOCK Initiate automatic setting of BFM gain digipots</summary>
        BFM_INIT_AUTOLOCK,
        /// <summary>DSDBR_FRONT_SWITCHES Front section switches</summary>
        DSDBR_FRONT_SWITCHES,
        /// <summary>EDC_REGISTER_ADDRESS Address of EDC register to read or write.</summary>
        EDC_REGISTER_ADDRESS,
        /// <summary>EDC_REGISTER_VALUE Value of EDC register to read or write.</summary>
        EDC_REGISTER_VALUE,
        /// <summary>Configures what to log  and reads back 4 * 1000 16 bit readings empty when 0xAAAA 43690 read 4 times on the trot</summary>
        DATALOG,
        /// <summary>LS_POWER_CTRL_ENABLE Set to 0x0001 to enable closed loop power control.</summary>
        LS_POWER_CTRL_ENABLE,
        /// <summary>LS_POWER_MON Optical power in uW</summary>
        LS_POWER_MON,
        /// <summary>LS_POWER_RAW Power control loop input value from MMI tamp ADC.</summary>
        LS_POWER_RAW,
        #endregion

        #region Page 6
        /// <summary>TosaSerialNumber</summary>
        TosaSerialNumber,
        /// <summary>RosaSerialNumber</summary>
        RosaSerialNumber,
        /// <summary>PcbaSerialNumber</summary>
        PcbaSerialNumber,
        /// <summary>HardwareRevision</summary>
        HardwareRevision,
        /// <summary>STM32MicroRevision</summary>
        STM32MicroRevision,
        /// <summary>FirmwarePartNumber</summary>
        FirmwarePartNumber,
        /// <summary>BuildDate</summary>
        BuildDate,
        /// <summary>BuildTime</summary>
        BuildTime,
        /// <summary>FirmwareRevision</summary>
        FirmwareRevision,
        #endregion

        #region Page 8
        /// <summary>Address of CDR register to configure</summary>
        CDRRegisterAddress1,
        /// <summary>Value to be written to CDR register</summary>
        CDRRegisterValue1,
        /// <summary></summary>
        CDRRegisterAddress2,
        /// <summary></summary>
        CDRRegisterValue2,
        /// <summary></summary>
        CDRRegisterAddress3,
        /// <summary></summary>
        CDRRegisterValue3,
        /// <summary></summary>
        CDRRegisterAddress4,
        /// <summary></summary>
        CDRRegisterValue4,
        /// <summary></summary>
        CDRRegisterAddress5,
        /// <summary></summary>
        CDRRegisterValue5,
        /// <summary></summary>
        CDRRegisterAddress6,
        /// <summary></summary>
        CDRRegisterValue6,
        /// <summary></summary>
        CDRRegisterAddress7,
        /// <summary></summary>
        CDRRegisterValue7,
        /// <summary></summary>
        CDRRegisterAddress8,
        /// <summary></summary>
        CDRRegisterValue8,
        /// <summary></summary>
        CDRRegisterAddress9,
        /// <summary></summary>
        CDRRegisterValue9,
        /// <summary></summary>
        CDRRegisterAddress10,
        /// <summary></summary>
        CDRRegisterValue10,
        /// <summary></summary>
        CDRRegisterAddress11,
        /// <summary></summary>
        CDRRegisterValue11,
        /// <summary></summary>
        CDRRegisterAddress12,
        /// <summary></summary>
        CDRRegisterValue12,
        /// <summary></summary>
        CDRRegisterAddress13,
        /// <summary></summary>
        CDRRegisterValue13,
        /// <summary></summary>
        CDRRegisterAddress14,
        /// <summary></summary>
        CDRRegisterValue14,
        /// <summary></summary>
        CDRRegisterAddress15,
        /// <summary></summary>
        CDRRegisterValue15,
        /// <summary></summary>
        CDRRegisterAddress16,
        /// <summary></summary>
        CDRRegisterValue16,
        /// <summary></summary>
        CDRRegisterAddress17,
        /// <summary></summary>
        CDRRegisterValue17,
        /// <summary></summary>
        CDRRegisterAddress18,
        /// <summary></summary>
        CDRRegisterValue18,
        /// <summary></summary>
        CDRRegisterAddress19,
        /// <summary></summary>
        CDRRegisterValue19,
        /// <summary></summary>
        CDRRegisterAddress20,
        /// <summary></summary>
        CDRRegisterValue20,
        /// <summary></summary>
        CDRRegisterAddress21,
        /// <summary></summary>
        CDRRegisterValue21,
        /// <summary></summary>
        CDRRegisterAddress22,
        /// <summary></summary>
        CDRRegisterValue22,
        #endregion

        #region Page 9
        /// <summary>Allows the loader to check for a valid file header if header wrong upgrade is aborted.</summary>
        StartFirmwareUpgrade,
        /// <summary>A maximum of 16 data words of data for the upgrade detailed below.</summary>
        UpgradeWriteData,
        /// <summary>Aborts the upgrade process.</summary>
        AbortFirmwareUpgrade,
        /// <summary>Informs the module to check the image transferred</summary>
        ChecksumVerify,
        /// <summary>Informs the module to restart and run the newly upgraded image</summary>
        InvokeFirmwareUpgrade,
        /// <summary>Informs the module to make the newly upgraded image the primary boot image</summary>
        CommitFirmwareUpgrade,
        /// <summary>Informs the module to run and make the new upgrade the primary boot image on reset</summary>
        CommitPending,
        /// <summary></summary>
        WriteData1,
        /// <summary></summary>
        WriteData2,
        /// <summary></summary>
        WriteData3,
        /// <summary></summary>
        WriteData4,
        /// <summary></summary>
        WriteData5,
        /// <summary></summary>
        WriteData6,
        /// <summary></summary>
        WriteData7,
        /// <summary></summary>
        WriteData8,
        /// <summary></summary>
        WriteData9,
        /// <summary></summary>
        WriteData10,
        /// <summary></summary>
        WriteData11,
        /// <summary></summary>
        WriteData12,
        /// <summary></summary>
        WriteData13,
        /// <summary></summary>
        WriteData14,
        /// <summary></summary>
        WriteData15,
        /// <summary></summary>
        WriteData16,
        #endregion
    }

    /// <summary>
    /// Titan memory map
    /// </summary>
    public class Titan_MemoryAddress
    {
        /// <summary>
        /// Defines a location in the titan memory map
        /// </summary>
        /// <param name="page">Paged address register number</param>
        /// <param name="address">Address offset</param>
        /// <param name="length">Number of bytes</param>
        public Titan_MemoryAddress(int page, int address, int length)
        {
            this.page = page;
            this.address = address;
            this.length = length;
            this.scalingFactor = 1;
        }

        /// <summary>
        /// Defines a location in the titan memory map
        /// </summary>
        /// <param name="page">Paged address register number</param>
        /// <param name="address">Address offset</param>
        /// <param name="length">Number of bytes</param>
        /// <param name="scalingFactor">DAC value  x  scaling factor = real world value</param>
        public Titan_MemoryAddress(int page, int address, int length, double scalingFactor)
        {
            this.page = page;
            this.address = address;
            this.length = length;
            this.scalingFactor = scalingFactor;
        }

        public readonly int page;
        public readonly int address;
        public readonly int length;
        public readonly double scalingFactor;
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Sub20ToAsic.cs
//
// Author: meirong.zhang, 2012
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestEngine.Framework.InternalData;
//using Bookham.TestLibrary.Algorithms;

// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_Sub20ToAsic : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Sub20ToAsic(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "SUB-20",				// hardware name 
                "0.1.9",               	// minimum valid firmware version 
                "0.5.3");              	// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            InstrumentDataRecord instrVariant2 = new InstrumentDataRecord(
                "Stm32Usb",				// hardware name 
                "Firmware_Unknown",   	// minimum valid firmware version 
                "Firmware_Unknown");   	// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant2", instrVariant2);

            // Configure valid chassis driver information
            InstrumentDataRecord sub20chassisInfo = new InstrumentDataRecord(
                "Chassis_Sub20",							// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Sub20Chassis", sub20chassisInfo);

            InstrumentDataRecord stm32chassisInfo = new InstrumentDataRecord(
                "Chassis_Stm32Usb",							// chassis driver name  
                "0.0.0.0",							// minimum valid chassis driver version  
                "2.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("Stm32Chassis", stm32chassisInfo);

            // initialise this instrument's chassis
            //this.instrumentChassis = (ChassisTypeI2C)chassisInit;
            this.instrumentChassis = (Chassis_Sub20)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }


        /// <summary>
        /// Set instrument to default state and carry out Polarisation initialisation
        /// </summary>
        public override void SetDefaultState()
        {
            this.instrumentChassis.GPIO_SetConfig(0xffffffff, 0xffffffff); //sets all IO to output
            //Carry out 0-1-0-1 sequence to initialise polarisation controller
            this.instrumentChassis.GPIO_Write(0x00000000, 0xffffffff); 
            this.instrumentChassis.GPIO_Write(0xffffffff, 0xffffffff);
            this.instrumentChassis.GPIO_Write(0x00000000, 0xffffffff);
            this.instrumentChassis.GPIO_Write(0xffffffff, 0xffffffff);
            
        }


        // Value between 0 and 4096 
        // channel between 0 and 3
        public bool pol_write(uint value, uint channel)
        {
            uint total = 24576;
            total = total + 32768 * channel;
            total = total + 131072 * value;
            this.instrumentChassis.GPIO_Write(total, 0xffffffff);
            bool result = this.instrumentChassis.GPIO_Write(total-8192, 0xffffffff);// sets CS low

            return result;

        }


        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        //public string ReadStringFromChassis(Titan_Command titanCommand)
        //{
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);
        //    return HelperFunctions.ByteArrayToString(responseData).TrimEnd('\0');
        //}

        //public string ReadStringFromChassisNoTrim(Titan_Command titanCommand)
        //{
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);
        //    return HelperFunctions.ByteArrayToString(responseData);
        //}

        //public UInt16 ReadUInt16FromChassis(Titan_Command titanCommand)
        //{
        //    // Data is returned [MSB] [LSB]
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);

        //    // Put MSB second
        //    Array.Reverse(responseData);

        //    // BitConverter.ToUInt16 requires 2 bytes
        //    Array.Resize(ref responseData, 2);

        //    Titan_MemoryAddress memoryAddress = MemoryMap_Titan.GetMemoryAddress(titanCommand);
        //    double unscaledValue = Convert.ToDouble(BitConverter.ToUInt16(responseData, 0));
        //    return Convert.ToUInt16(unscaledValue * memoryAddress.scalingFactor);
        //}

        //public Int16 ReadSInt16FromChassis(Titan_Command titanCommand)
        //{
        //    Titan_MemoryAddress memoryAddress = MemoryMap_Titan.GetMemoryAddress(titanCommand);

        //    // Data is returned [MSB] [LSB]
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);

        //    // Put MSB second
        //    Array.Reverse(responseData);

        //    // BitConverter.ToInt16 requires 2 bytes
        //    Array.Resize(ref responseData, 2);

        //    double unscaledValue = Convert.ToDouble(BitConverter.ToInt16(responseData, 0));
        //    return Convert.ToInt16(unscaledValue * memoryAddress.scalingFactor);
        //}

        /// <summary>
        /// Produce a 7 bit integer from a single byte where the MSB defines the sign (2's complement)
        /// </summary>
        /// <param name="titanCommand"></param>
        /// <returns>a signed int16 representation of 7 bit integer</returns>
        //public sbyte ReadSInt8FromChassis(Titan_Command titanCommand)
        //{
        //    Titan_MemoryAddress memoryAddress = MemoryMap_Titan.GetMemoryAddress(titanCommand);

        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);

        //    /*
        //     * 2's complement
        //     * 
        //     * 00000010	= 2     (2)
        //     * 00000001	= 1     (1)
        //     * 00000000	= 0     (0)
        //     * 11111111	= -1    (255)
        //     * 11111110	= -2    (254)
        //     */

        //    // Get LSB only
        //    byte lsb = responseData[responseData.Length - 1];

        //    // Perform 2's complement
        //    sbyte signedByte = unchecked((sbyte)lsb);

        //    return Convert.ToSByte(signedByte);
        //}

        //public double ReadDoubleFromChassis(Titan_Command titanCommand)
        //{
        //    Titan_MemoryAddress memoryAddress = MemoryMap_Titan.GetMemoryAddress(titanCommand);

        //    // Data is returned [MSB] [LSB]
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);

        //    // BitConverter expects the LSB first, so reverse the order
        //    Array.Reverse(responseData);

        //    double unscaledValue = 0;

        //    // Attempt to deal with negative values
        //    switch (memoryAddress.length)
        //    {
        //        case 2:
        //            unscaledValue = Convert.ToDouble(BitConverter.ToInt16(responseData, 0));
        //            break;
        //        case 4:
        //            unscaledValue = Convert.ToDouble(BitConverter.ToInt32(responseData, 0));
        //            break;
        //        default:
        //            // Pad data because BitConverter.ToInt64 needs 8 bits
        //            Array.Resize(ref responseData, 8);
        //            unscaledValue = Convert.ToDouble(BitConverter.ToInt64(responseData, 0));
        //            break;
        //    }

        //    return unscaledValue * memoryAddress.scalingFactor;
        //}

        public void WriteDoubleToChassis(int channel, double currentValue)
        {
            byte dacSinkRegister = 0x34;//52
            byte dacPolarityRegister = 0x3E;//62
            byte dacConfigRegiste;
            byte dacSourceRegister;

            int valueAsDacCounts = Convert.ToInt16(CurrentToDac(channel, currentValue)); //13652
            valueAsDacCounts = valueAsDacCounts * 4;
            byte[] byteDac = BitConverter.GetBytes(valueAsDacCounts);
            Array.Resize(ref byteDac, 2);
            Array.Reverse(byteDac);
            byte[] setdacConfigReg = setdacConfigRegister(channel);
            dacConfigRegiste = setdacConfigReg[0];//10
            dacSourceRegister = setdacConfigReg[1];//42

            // Data must be [MSB] ... [LSB]
            byte[] byteArray;
            if (channel == 6)
            {
               byteArray=new byte [9];
               if (valueAsDacCounts < 0)
               {
                   byteArray[0] = dacSourceRegister;
                   byteArray[1] = 0;
                   byteArray[2] = 0;
                   byteArray[3] = dacSinkRegister;
                   byteArray[4] = byteDac[0];
                   byteArray[5] = byteDac[1];
                   byteArray[6] = dacPolarityRegister;
                   byteArray[7] = 0x80;
                   byteArray[8] = 0;
               }
               else
               {
                   byteArray[0] = dacSinkRegister;
                   byteArray[1] = 0;
                   byteArray[2] = 0;
                   byteArray[3] = dacSourceRegister;
                   byteArray[4] = byteDac[0];
                   byteArray[5] = byteDac[1];
                   byteArray[6] = dacPolarityRegister;
                   byteArray[7] = 0;
                   byteArray[8] = 0;
               }
            }
            else
            {
                //if (channel == 3 || channel == 7 || channel == 8 || channel == 9)
                //{
                //    byteArray = new byte[3];
                //    byteArray[0] = dacConfigRegiste;
                //    byteArray[1] = 0x80;
                //    byteArray[2] = 0;
                //}
                byteArray = new byte[3];
                byteArray[0] = dacSourceRegister;
                byteArray[1] = byteDac[0];
                byteArray[2] = byteDac[1];
            }
            WriteToChassisByteArray(byteArray);
            //return result ;
        }


        public void setDacMultiplex(int channel,int subChannel, double currentValue)
        {
            byte dacSinkRegister = 0x34;//52
            byte dacPolarityRegister = 0x3E;//62
            byte dacConfigRegiste;
            byte dacSourceRegister;
            byte configMSB;
            byte configLSB;

            int valueAsDacCounts = Convert.ToInt16(CurrentToDac(1, currentValue)); //13652
            valueAsDacCounts = valueAsDacCounts * 4;
            byte[] byteDac = BitConverter.GetBytes(valueAsDacCounts);
            Array.Resize(ref byteDac, 2);
            Array.Reverse(byteDac);
            byte[] setdacConfigReg = setdacConfigRegister(channel);
            dacConfigRegiste = setdacConfigReg[0];//10
            dacSourceRegister = setdacConfigReg[1];//42
            setMsbLsbformuxchanel(subChannel, out configMSB,out configLSB);

            // Data must be [MSB] ... [LSB]
            byte[] byteArray= new byte[6];
            byteArray[0] = dacConfigRegiste;
            byteArray[1] = configMSB;
            byteArray[2] = configLSB;
            byteArray[3] = dacSourceRegister;
            byteArray[4] = byteDac[0];
            byteArray[5] = byteDac[1];

            WriteToChassisByteArray(byteArray);
        }

        //public Int32 ReadInt32FromChassis(Titan_Command titanCommand)
        //{
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);

        //    // Data is returned MSB LSB, but BitConverter needs LSB first
        //    Array.Reverse(responseData);

        //    // BitConverter.ToInt32 requires 4 bytes
        //    Array.Resize(ref responseData, 4);

        //    Titan_MemoryAddress memoryAddress = MemoryMap_Titan.GetMemoryAddress(titanCommand);
        //    double unscaledValue = Convert.ToDouble(BitConverter.ToInt32(responseData, 0));
        //    return Convert.ToInt32(unscaledValue * memoryAddress.scalingFactor);
        //}

        //public Int64 ReadInt64FromChassis()
        //{
        //    byte[] responseData = ReadByteArrayFromChassis(titanCommand);
        //    // BitConverter.ToInt64 requires 8 bytes
        //    Array.Resize(ref responseData, 8);
        //    return BitConverter.ToInt64(responseData, 0);
        //}

        public byte ReadByteFromChassis()
        {

            byte[] responseData = ReadByteArrayFromChassis(1);
            return responseData[0];
        }

        public byte[] ReadByteArrayFromChassis(int numberOfBytes)
        {
            return this.instrumentChassis.I2C_Read( numberOfBytes);
        }

        //public void WriteToChassis(int data)
        //{

        //    // GetBytes returns the data with LSB at bit[0]
        //    byte[] byteArray = BitConverter.GetBytes(data);

        //    //Resize affects the RHS of the array, leaving bit[0] untouched
        //    //Array.Resize(ref byteArray, titan_MemoryAddress.length);

        //    // WriteToChassis expects the MSB as bit [0]
        //    Array.Reverse(byteArray);

        //    WriteToChassis(byteArray);
        //}

        //public void WriteToChassis( short data)
        //{

        //    // GetBytes returns the data with LSB at bit[0]
        //    byte[] byteArray = BitConverter.GetBytes(data);

        //    // BitConverter expects the LSB first, so reverse the order
        //    Array.Reverse(byteArray);

        //    WriteToChassis( byteArray);
        //}

        //public void WriteToChassis(sbyte data)
        //{
        //    byte valueToWrite = unchecked((byte)data);
        //    WriteToChassis( new byte[] { valueToWrite });
        //}

        //public void WriteToChassis( string data)
        //{           
        //    byte[] byteArray = HelperFunctions.StringToByteArray(data);
            
        //    // Need to pad the right-hand end of string arrays. Resize does this for us.

        //    WriteToChassis(byteArray);
        //}

        /// <summary>
        /// Write an array of bytes to the transponder
        /// </summary>
        /// <param name="titanCommand">Command type</param>
        /// <param name="byteArray">MSB first</param>
        public void WriteToChassisByteArray(byte[] byteArray)
        {
            // Make sure that the array is the correct length

            this.instrumentChassis.I2C_Write(byteArray);
        }

        /// <summary>
        /// Write an array of bytes to the transponder
        /// </summary>
        /// <param name="titanCommand">Command type</param>
        /// <param name="byteArray">MSB first</param>
        public void WriteToChassis(byte[] byteArray, int numberOfByte, int memoryAddress)
        {
            // Make sure that the array is the correct length

            this.instrumentChassis.I2C_Write(byteArray, numberOfByte, memoryAddress);
        }
        public bool  SetFrequency(int freq)
        {
            bool result= this.instrumentChassis.I2C_SetFrequency(ref freq);
            return result;
        }
        public int CurrentToDac(int channel, double current)
        {
            int dac = 0;
            int range_ma = 0;
            switch (channel)
            {
                case 1:
                    range_ma = 8;
                    break;
                case 2:
                    range_ma = 10;
                    break;
                case 3:
                    range_ma = 20;
                    break;
                case 4:
                    range_ma = 90;
                    break;
                case 7:
                    range_ma = 90;
                    break;
                case 5:
                    range_ma = 180;
                    break;
                case 6:
                    range_ma = 300;
                    break;
                case 8:
                    range_ma = 15;
                    break;
                case 9:
                    range_ma = 15;
                    break;
                default:
                    throw new ArgumentException("Invalid state: " + channel.ToString());
                    break;
            }
            dac = Convert.ToInt16(current * 16383 / range_ma);
            return dac;
       }

        public bool gpio_config(uint value, uint mask)
        {
           bool result= this.instrumentChassis.GPIO_SetConfig ( value, mask) ;
            return result;

        }

        public bool gpio_write(uint value, uint mask)
        {
            bool result = this.instrumentChassis.GPIO_Write(value, mask);
            return result;

        }

        public void setMsbLsbformuxchanel(int subChannel, out byte configMSB, out  byte configLSB)
        {
            switch (subChannel)
            {
                case 1:
                    configMSB = 0x0;
                    configLSB = 0x0;
                    break;
                case 2:
                    configMSB = 0x0;
                    configLSB = 0x80;
                    break;
                case 3:
                    configMSB = 0x1;
                    configLSB = 0x80;
                    break;
                default:
                    throw new ArgumentException("Invalid state: " + subChannel.ToString());
                    break;
            }
        }

        public byte [] setdacConfigRegister(int channel)
        {
            
            byte dacConfigRegister;
            byte dacSourceRegister;
            switch (channel)
            {
                case 1:
                    dacConfigRegister = 0x2;
                    dacSourceRegister = 0x22;
                    break;
                case 2:
                    dacConfigRegister = 0x4;
                    dacSourceRegister = 0x24;
                    break;
                case 3:
                    dacConfigRegister = 0x6;
                    dacSourceRegister = 0x26;
                    break;
                case 4:
                    dacConfigRegister = 0x8;
                    dacSourceRegister = 0x28;
                    break;
                case 5:
                    dacConfigRegister = 0xA;
                    dacSourceRegister = 0x2A;
                    break;
                case 6:
                    dacConfigRegister = 0xC;
                    dacSourceRegister = 0x2C;
                    break;
                case 7:
                    dacConfigRegister = 0xE;
                    dacSourceRegister = 0x2E;
                    break;
                case 8:
                    dacConfigRegister = 0x10;
                    dacSourceRegister = 0x30;
                    break;
                case 9:
                    dacConfigRegister = 0x12;
                    dacSourceRegister = 0x32;
                    break;
                default:
                    throw new ArgumentException("Invalid state: " + channel.ToString());
                    break;
            }
            byte[] returnData = new byte[] { dacConfigRegister, dacSourceRegister };
            return  returnData;
        }

        public void getFrontpair(double Switches, out int channel, out int subChannel)
        {
            int Channel=0;
            int subchannel=0;

            if (Switches == 1)
            {
                Channel = 1;
                subchannel = 0;
            }

            if (Switches == 2)
            {
                Channel = 2;
                subchannel = 0;
            }

            if (Switches == 3)
            {
                Channel = 1;
                subchannel = 1;
            }
            if (Switches == 4)
            {
                Channel = 2;
                subchannel = 1;
            }
            if (Switches == 5)
            {
                Channel = 1;
                subchannel = 2;
            }
            if (Switches == 6)
            {
                Channel = 2;
                subchannel = 2;
            }

            if (Switches == 7)
            {
                Channel = 1;
                subchannel = 3;
            }
            if (Switches == 8)
            {
                Channel = 2;
                subchannel = 3;
            }

            channel = Channel;
            subChannel = subchannel;
        }

        //public bool SafeMode
        //{
        //    get { return this.instrumentChassis.SafeMode; }
        //    set { this.instrumentChassis.SafeMode = value; }
        //}
#region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Sub20 instrumentChassis;
        //private ChassisTypeI2C instrumentChassis;
#endregion
    }
}

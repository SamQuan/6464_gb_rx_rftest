// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Sub20.cs
//
// Author: Mark Fullalove, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Xdimax;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// SUB-20 chassis
    /// </summary>
    public class Chassis_Sub20 : ChassisTypeI2C, IDisposable
    {
        /// <summary>
        /// The handle to the sub20 board
        /// </summary>
        private int sub20Handle;

        /// <summary>
        /// An instance of the Sub20 class
        /// </summary>
        private Sub20 sub20;

        /// <summary>
        /// Maximum i2c rate in kHz
        /// </summary>
        private int maxI2CRate_kHz;

        /// <summary>
        /// The i2c address. Note that this is shifted up one bit and a R/W bit appended before sending to the device.
        /// </summary>
        private byte i2cAddr;

        /// <summary>
        /// True if already disposed
        /// </summary>
        private bool disposed;

        /// <summary>
        /// True if an exception should be thwrown upon communications failures
        /// </summary>
        private bool safeMode;

        /// <summary>
        /// Serial number of SUB-20 hardware
        /// </summary>
        private string deviceId;

        //public delegate void DebugLogHandler(string message);

        public event DebugLogHandler DebugLog;
        
        /// <summary>
        /// OnDebugLog event handler
        /// </summary>
        protected virtual void OnDebugLog(string message)
        {
            // If an event has no subscribers registered, it will evaluate to null.
            // Check this before calling the event itself.
            if (DebugLog != null)
            {
                DebugLog(message);  // Notify Subscribers
            }
        }
        

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the Chassis_Sub20 class
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis
        /// e.g "SUB20,58,200,01C7" where :
        ///     "58" is the address of the i2c device
        ///     "200" is the communication rate in kHz.
        ///     "01C7" is the serial number/module address of the SUB-20 PCB</param>
        public Chassis_Sub20(
            string chassisNameInit, 
            string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Set up expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "SUB-20",			// hardware name 
                "0.1.9",			// minimum valid firmware version 
                "0.9.9");		    // maximum valid firmware version 
            ValidHardwareData.Add("Chassis_Sub20", chassisData);

            string[] resByComma = resourceString.Split(',');

            if (resByComma[0] == "SUB20")
            {
                if (resByComma.Length != 4)
                    throw new ChassisException("Bad resource string. Use \"SUB20,nn[,nnn],[0xnnnn]\". eg \"SUB20,58,200,0x1C7\"");

                // Extract max I2C rate
                this.maxI2CRate_kHz = int.Parse(resByComma[2], System.Globalization.CultureInfo.InvariantCulture);

                // Extract I2C module address of the MAX5111
                this.i2cAddr = byte.Parse(resByComma[1], System.Globalization.NumberStyles.HexNumber);

                // Extract the address of the SUB-20 board
                this.deviceId = resByComma[3];
            }
        }
        #endregion

        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.Sub20Board.GetFwVersion();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.Sub20Board.GetName();
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }

            set
            {
                // if setting online
                if (value)
                {
                    // get a handle
                    Sub20Enum sub20EnumObj = new Sub20Enum();
                    this.sub20Handle = sub20EnumObj.GetNext(this.sub20Handle);

                    /* Sub20EnumObj.GetNext() returns the handle from the next 
                     * device that is not already open. Once all devices are 
                     * open an empty string is returned.
                     */
                    List<Sub20> availableDevices = new List<Sub20>();

                    // Check whether the serial number matches the one we're looking for
                    string serialNo = this.Sub20Board.GetSerialNumber();
                    while (!String.IsNullOrEmpty(serialNo) && !this.deviceId.Equals(serialNo))
                    {
                        availableDevices.Add(this.sub20);
                        this.sub20 = null;

                        this.sub20Handle = sub20EnumObj.GetNext(this.sub20Handle);
                        serialNo = this.Sub20Board.GetSerialNumber();
                    }

                    // Now we need to close the devices that we don't want to use.
                    foreach (Sub20 wrongSub20 in availableDevices)
                    {
                        string seri = wrongSub20.GetSerialNumber();
                        wrongSub20.Close();
                        wrongSub20.Dispose();
                    }

                    // Not found.
                    if (String.IsNullOrEmpty(serialNo))
                        throw new ChassisException("Unable to locate Sub-20 S/No " + this.deviceId);
                }
                else
                {
                    this.sub20.Close();
                    this.sub20.Dispose();
                }


                // setup base class
                base.IsOnline = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the status of all communications will be checked
        /// </summary>
        public override bool SafeMode
        {
            get { return this.safeMode; }
            set { this.safeMode = value; }
        }

        private enum SendOrReceive
        {
            Send,
            Receive
        }

        /// <summary>
        /// Gets an instance of the Sub20 class
        /// </summary>
        private Sub20 Sub20Board
        {
            get
            {
                if (this.sub20 == null)
                {
                    this.sub20 = new Sub20();
                    this.sub20.Open(this.sub20Handle);

                    int i2cFreq = (int)(this.maxI2CRate_kHz) * 1000;
                    this.sub20.I2C_SetFrequency(ref i2cFreq);

                    this.LCD_Write(0, this.Name);
                    this.LCD_Write(1, "Online");
                }

                return this.sub20;
            }
        }

        /// <summary>
        /// Write to i2c
        /// </summary>
        /// <param name="payload">Payload bytes</param>
        /// <param name="numberOfBytes">Number of bytes to send</param>
        /// <param name="memoryAddress">Target memory address</param>        
        public override void I2C_Write(byte[] payload, int numberOfBytes, int memoryAddress)
        {
            byte MAX_PACKET_SIZE = 32;

            int memoryAddressSize = 1 + memoryAddress / 256;
            int attempts = this.safeMode ? 3 : 1;
            bool success = true;
            byte[] resultingData = payload;

            do
            {
                success = true;
                byte numberOfBytesWritten = 0;
                byte bytesToWrite = Math.Min((byte)numberOfBytes, MAX_PACKET_SIZE);
                int startAddress = memoryAddress;

                while (numberOfBytesWritten < numberOfBytes)
                {
                    byte[] writeData = new byte[bytesToWrite];

                    Array.Copy(payload, numberOfBytesWritten, writeData, 0, bytesToWrite);

                    memoryAddress += numberOfBytesWritten;

                    this.Sub20Board.I2C_Write(this.i2cAddr, memoryAddress, memoryAddressSize, writeData);

                    SendDebugMessage(SendOrReceive.Send, writeData, memoryAddress + numberOfBytesWritten);

                    numberOfBytesWritten += bytesToWrite;

                    bytesToWrite = Math.Min(bytesToWrite, (byte)(Convert.ToByte(numberOfBytes) - numberOfBytesWritten));

                    System.Threading.Thread.Sleep(10);
                }
                //SendDebugMessage(SendOrReceive.Send, payload, memoryAddress);

                if (this.SafeMode)
                {
                    // reset the memoryaddress to the start address as we may have increased it for large data writes.
                    memoryAddress = startAddress;

                    this.CheckForCommsError();

                    // Verify that the data was written
                    resultingData = this.I2C_Read(numberOfBytes, memoryAddress);
                    if (resultingData.Length != payload.Length)
                    {
                        success = false;
                        LogEvent("Verify data read back incorrect number of bytes");
                    }

                    for (int i = 0; i < resultingData.Length; i++)
                    {
                        if (payload[i] != resultingData[i])
                        {
                            success = false;
                            LogEvent(String.Format("Data verification failed. Wrote {0} but read back {1}", BitConverter.ToString(payload), BitConverter.ToString(resultingData)));
                        }
                    }

                }
                else
                {
                    if (DebugLog != null)
                    {
                        int errorCode = this.Sub20Board.GetLastError();
                        if (errorCode != 0)
                        {
                            string errorDescription = this.Sub20Board.GetStrError(errorCode);
                            DebugLog(errorDescription);
                        }
                    }
                }
            } while (!success && attempts-- > 0);

            if (!success)
                throw new ChassisException(String.Format("Data verification failed. Wrote {0} but read back {1}", BitConverter.ToString(payload), BitConverter.ToString(resultingData)));

        }

        private void SendDebugMessage(SendOrReceive mode, byte[] payload)
        {
            SendDebugMessage(mode, payload, int.MinValue);
        }

        private void SendDebugMessage(SendOrReceive mode, byte[] payload, int memoryAddress)
        {
            if (DebugLog != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(mode == SendOrReceive.Send ? "Sent : " : "Read : ");
                sb.Append(ByteToString(this.i2cAddr));
                sb.Append(" ");
                if (memoryAddress != int.MinValue)
                {
                    sb.Append(IntToString(memoryAddress));
                    sb.Append(" ");
                }

                // Foreach runs from index 0 upwards. We need the data MSB first, so reverse it.
                byte[] debugPayload = (byte[])payload.Clone();
                //Array.Reverse(debugPayload);
                foreach (byte byteToConvert in debugPayload)
                {
                    sb.Append(ByteToString(byteToConvert));
                    sb.Append(" ");
                }
                DebugLog(sb.ToString());
            }
        }

        private string ByteToString(byte byteToConvert)
        {
            string result = byteToConvert.ToString("x").PadLeft(2, '0');
            return result.Remove(0, result.Length - 2).ToUpper();
        }

        private string IntToString(int intToConvert)
        {
            string result = intToConvert.ToString("x").PadLeft(2, '0');
            return result.Remove(0, result.Length - 2).ToUpper();
        }

        /// <summary>
        /// The address byte will be sent automatically.
        /// The payload will be appended.
        /// </summary>
        /// <param name="payload">Consisting of command byte and data bytes</param>
        //public override bool I2C_Write(byte[] payload)
        public override void I2C_Write(byte[] payload)
        {
            
            bool result= this.Sub20Board.I2C_Write(this.i2cAddr, 0, 0, payload);

            SendDebugMessage(SendOrReceive.Send, payload);

            if (this.SafeMode)
            {
                this.CheckForCommsError();
                // TODO ? - Verify that the data was written
            }
            //return result;
        }

        public bool  I2C_SetFrequency(ref int freq)
        {
            bool result = this.Sub20Board.I2C_SetFrequency(ref freq);

            //SendDebugMessage(SendOrReceive.Send, payload);

            if (this.SafeMode)
            {
                this.CheckForCommsError();
                // TODO ? - Verify that the data was written
            }
            return result;
        }

        /// <summary>
        /// Read over I2C
        /// </summary>
        /// <param name="numberOfBytes">Number of bytes to read</param>
        /// <returns>An array of bytes</returns>
        public override byte[] I2C_Read(int numberOfBytes)
        {
            byte[] buffer = new byte[numberOfBytes];

            this.Sub20Board.I2C_Read(this.i2cAddr, 0, 0, buffer);

            SendDebugMessage(SendOrReceive.Receive, buffer);

            if (this.SafeMode)
            {
                this.CheckForCommsError();
            }

            return buffer;
        }

        /// <summary>
        /// Read over I2C
        /// </summary>
        /// <param name="numberOfBytes">Number of bytes to read</param>
        /// <param name="memoryAddress">Memory address from which to read</param>        
        /// <returns>An array of bytes</returns>
        public override byte[] I2C_Read(int numberOfBytes, int memoryAddress)
        {
            byte[] buffer = new byte[numberOfBytes];

            int memoryAddressSize = 1 + memoryAddress / 256;

            this.Sub20Board.I2C_Read(this.i2cAddr, memoryAddress, memoryAddressSize, buffer);
            System.Threading.Thread.Sleep(10);

            SendDebugMessage(SendOrReceive.Receive, buffer, memoryAddress);

            if (this.SafeMode)
                this.CheckForCommsError();
            else
            {
                if (DebugLog != null)
                {
                    int errorCode = this.Sub20Board.GetLastError();
                    if (errorCode != 0)
                    {
                        string errorDescription = this.Sub20Board.GetStrError(errorCode);
                        DebugLog(errorDescription);
                    }
                }
            }
            return buffer;
        }

        public bool GPIO_SetConfig(uint value,uint mask)
        {
            return this.Sub20Board.GPIO_SetConfig(value, mask);
        }
        /// <summary>
        /// Read to I/O
        /// </summary>
        /// <param name="value">Value to set</param>
        /// <param name="mask">The bit will bet set if the corresponding mask bit is "1".</param>
        /// <returns>True on success</returns>
        public bool GPIO_Read(ref uint value)
        {
            return this.Sub20Board.GPIO_Read(ref value);
        }

        /// <summary>
        /// Write to I/O
        /// </summary>
        /// <param name="value">Value to set</param>
        /// <param name="mask">The bit will bet set if the corresponding mask bit is "1".</param>
        /// <returns>True on success</returns>
        public bool GPIO_Write(uint value, uint mask)
        {
            return this.Sub20Board.GPIO_Write(value, mask);
        }


        public bool POL_Init()
        {
            this.Sub20Board.GPIO_SetConfig(0xffffffff, 0xffffffff); // Sets all I/Os to outputs
            this.Sub20Board.GPIO_Write(0, 0xffffffff);
            this.Sub20Board.GPIO_Write(0xffffffff, 0xffffffff);
            this.Sub20Board.GPIO_Write(0, 0xffffffff);
            return this.Sub20Board.GPIO_Write(0xffffffff, 0xffffffff);
        }

        public bool POL_Write(uint value, uint Channel)
        {
            uint total = 16384 +(Channel *32768) ; // sets Reset high and sets to correct channel
            total = total + (value * 131072);   // add value to write to channel

            return this.Sub20Board.GPIO_Write(total, 0xffffffff);
        }

        /// <summary>
        /// Write to the SUB-20 LCD display.
        /// </summary>
        /// <param name="line">Line of display 0 or 1 </param>
        /// <param name="message">Message to write. Will be truncated to 8 characters.</param>
        public void LCD_Write(int line, string message)
        {
            if (line > 1 || line < 0)
                throw new ChassisException("Invalid line number. Must be 0 or 1");

            this.Sub20Board.LCD_Write(string.Format("\\e0{0}{1}", line, message.Substring(0, Math.Min(8, message.Length))));
        }

        /// <summary>
        /// Release resources owned by this class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Throws a ChassisException exception if a communication error has occurred.
        /// </summary>
        private void CheckForCommsError()
        {
            int errorCode = this.Sub20Board.GetLastError();
            if (errorCode != 0)
            {
                string errorDescription = this.Sub20Board.GetStrError(errorCode);
                throw new ChassisException("Error communicating with " + this.Name + " via Sub-20 board : " + errorDescription);
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Release resources owned by this class
        /// </summary>
        /// <param name="disposing">True if disposing</param>
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // Dispose of any managed resources here :
                    if (this.sub20 != null)
                    {
                        this.LCD_Write(1, "Offline");
                        this.sub20.Close();
                        this.sub20.Dispose();
                    }
                }

                // Dispose of any unmanaged resources here :
            }

            this.disposed = true;
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Inst_Ke2500.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Instrument driver for one channel of a Keithley 2500 dual bfm monitor
	/// Provides basic source functions only for 'ElectricalSource' instrument type
	/// </summary>
	public class Inst_Ke2500 : InstType_ElectricalSource
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
		public Inst_Ke2500(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information

			// Add Ke2500 details
			InstrumentDataRecord ke2500Data = new InstrumentDataRecord(	
				"KEITHLEY INSTRUMENTS INC. MODEL 2500",			// hardware name 
				"A00",											// minimum valid firmware version 
				"A02");											// maximum valid firmware version 
			ke2500Data.Add("MaxCurrentAmp", "0.02");			// maximum current 
			ke2500Data.Add("MaxVoltage", "100.0");				// maximum voltage 
			ke2500Data.Add("GroundConnect", "true");			// connect negative terminals to analogue 0V
			ke2500Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
			ValidHardwareData.Add("Ke2500", ke2500Data);

			// Configure valid chassis information
			// Add 2500 chassis details
			InstrumentDataRecord ke2500ChassisData = new InstrumentDataRecord(	
				"Chassis_Ke2500",								// chassis driver name  
				"0",											// minimum valid chassis driver version  
				"1.0.0.0");										// maximum valid chassis driver version
			ValidChassisDrivers.Add("Chassis_Ke2500", ke2500ChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (ChassisType_Visa) base.InstrumentChassis;
		}


		#region Public Instrument Methods and Properties

		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
				string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');

				// Build the string from 2 substrings, the manufacturer name & the model number
				string hid = idn[0] + " " + idn[1];

				// Log event 
				LogEvent("'HardwareIdentity' returned : " + hid);

				return hid;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
				string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');				

				// The 4th substring contains the firmware version
				// The first 3 characters are the version
				string fv = idn[3].Substring(0,3);

				// Log event 
				LogEvent("'FirmwareVersion' returned : " + fv);

				// Return 
				return fv;
			}
		}

		/// <summary>
		/// Configures the instrument into a default state.
		/// 
		/// </summary>
		public override void SetDefaultState()
		{
			// Setline frequency
			instrumentChassis.Write_Unchecked(":SYST:LFR " + HardwareData["LineFrequency_Hz"], this);

			// Set ground connect
			if (HardwareData["GroundConnect"].ToLower() == "true")
			{
				instrumentChassis.Write_Unchecked(":SOUR" + this.Slot + ":GCON 1", this);
			}
		}

		#endregion

		#region Public ElectricalSource InstrumentType Methods and Properties

		/// <summary>
		/// Sets/returns the output state
		/// </summary>
		public override bool OutputEnabled
		{
			get 
			{
				// Query the output state
				string rtn = instrumentChassis.Query_Unchecked(":OUTP" + this.Slot + ":STAT?", this);

				// Return bool value
				return (rtn == trueStr);
			}
			set 
			{
				// Convert bool value to string
				string boolVal = value ? trueStr : falseStr;

				// Set output
				instrumentChassis.Write_Unchecked(":OUTP" + this.Slot + ":STAT " + boolVal, this);
			}
		}

		/// <summary>
        /// Sets/returns the current level set point
		/// SET/GET CURRENT SETPOINT NOT SUPPORTED by this instrument
		/// </summary>
		public override double CurrentSetPoint_amp
		{
			get 
			{
                // Cannot get  current setpoint, voltage source only.
                throw new InstrumentException("Invalid operation. Cannot get current set point with this instrument");				
			}
			
			set
			{
				// Cannot set current, voltage source only.
				throw new InstrumentException("Invalid operation. Cannot set current with this instrument");
			}
		}

        /// <summary>
        /// Sets/returns the actual measured current level 
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                // Read current in amps. Returns data for 2 channels
                string[] meas = instrumentChassis.Query_Unchecked(":MEAS:CURR?", this).Split(',');

                // Select data to return
                string rtn;
                switch (this.Slot)
                {
                    case "1": rtn = meas[0]; break;
                    case "2": rtn = meas[1]; break;
                    default: throw new InstrumentException("Invalid slot ID '" + this.Slot + "'");
                }

                // Convert and return
                return Convert.ToDouble(rtn);
            }
        }

		/// <summary>
		/// Sets/returns the compliance current set point
        /// NOT SUPPORTED ON THIS INSTRUMENT
		/// </summary>
		public override double CurrentComplianceSetPoint_Amp
		{
			get 
			{
				// No current compliance function for this instrument
				throw new InstrumentException("Current compliance not supported on this instrument");
			}
			
			set
			{
				// No current compliance function for this instrument
				throw new InstrumentException("Current compliance not supported on this instrument");
			}
		}

		/// <summary>
        /// Sets/returns the voltage setpoint level
		/// </summary>
		public override double VoltageSetPoint_Volt
		{
			get 
			{
				// Read current in volts
				string rtn = instrumentChassis.Query_Unchecked(":SOUR" + this.Slot + ":VOLT:LEV?", this);				
				
				// Convert and return
				return Convert.ToDouble(rtn);								
			}
			
			set
			{
				// Check value with maximum value
				double compVolt = Convert.ToDouble(HardwareData["MaxVoltage"]);
				if (Math.Abs(value) > compVolt)
				{
					throw new InstrumentException("Set value level '" + value.ToString() +
						"' is greater than maximum available '" + compVolt.ToString() + "'");
				}
				// Set voltage
				instrumentChassis.Write_Unchecked(":SOUR" + this.Slot + ":VOLT:LEV " + value.ToString().Trim(), this);
			}
		}

        /// <summary>
        ///  Returns the measured voltage output level
        ///  NOT SUPPORTED ON THIS INSTRUMENT
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Cannot measure the output voltage on this instrument.
                throw new InstrumentException("Measurement of output Voltage not supported on this instrument");
            }
        }
		
		/// <summary>
		/// Sets/returns the voltage compliance level
		/// NOT SUPPORTED by this instrument
		/// </summary>
		public override double VoltageComplianceSetPoint_Volt
		{
			get 
			{
				// No voltage compliance function for this instrument
				throw new InstrumentException("Voltage compliance not supported on this instrument");
			}
			
			set
			{
				// No voltage compliance function for this instrument
				throw new InstrumentException("Voltage compliance set not supported on this instrument");
			}
		}


		#endregion


		#region Private Data

		// Chassis reference
		private ChassisType_Visa instrumentChassis;

		// Bool values
		private const string falseStr = "0";
		private const string trueStr = "1";

		#endregion
	}
}

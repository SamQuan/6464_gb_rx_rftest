// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Ke2500.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	#region Public Functions
	
	/// <summary>
	/// Keithley Instruments Ke2500 Chassis Driver
	/// </summary>
	public class Chassis_Ke2500 : ChassisType_Visa
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
		public Chassis_Ke2500(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information
			
			// Add details of Ke2500 chassis
			ChassisDataRecord ke2500Data = new ChassisDataRecord(
				"KEITHLEY INSTRUMENTS INC. MODEL 2500",		// hardware name 
				"0",										// minimum valid firmware version 
				"A02");										// maximum valid firmware version 
			ValidHardwareData.Add("Ke2500", ke2500Data);
		}


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
				
				// Return the firmware version in the 4th comma seperated field
				// The first 3 characters are the firmware version
				return idn[3].Substring(0, 3);
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
		
				// Return field1, the manufacturer name and field 2, the model number
				return idn[0] + " " + idn[1];
			}
		}


		#endregion
	}
	
}

using System;
using System.Collections.Generic;
using System.Text;
//using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
//using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using NUnit.Framework;

namespace Test_AgE4407x
{
    class TestObject
    {
        public void Init()
        {
            address = "GPIB0::18::INSTR";
            chassis = new Chassis_AgE4407x("Chassis_ESA", "Chassis_AgE4407x", address);

            instrument = new Instr_AgE4407x("Instr_ESA", "Instr_AgE4407x", "", "", chassis);

            chassis.IsOnline = true;

            instrument.IsOnline = true;
        }

        public void Run()
        {
            instrument.CenterFreq_Khz = 750;
            double centerFreq = instrument.CenterFreq_Khz;
            instrument.Span_Khz = 1;
            double span = instrument.Span_Khz;
            instrument.ResolutionBW_Khz = 1;
            double resBW = instrument.ResolutionBW_Khz;
            instrument.VedioBW_Khz = 30;
            double vedBW = instrument.VedioBW_Khz;
            instrument.AvgState = false;
            bool avgState = instrument.AvgState;
            instrument.MeasMode = Instr_AgE4407x.MeasureMode.SA;
            Instr_AgE4407x.MeasureMode sa = instrument.MeasMode;
            instrument.PeakSearchMode = Instr_AgE4407x.PKSearchMode.MAXimum;
            Instr_AgE4407x.PKSearchMode mode = instrument.PeakSearchMode;
            instrument.Unit = Instr_AgE4407x.UnitSystem.V;
            Instr_AgE4407x.UnitSystem unit = instrument.Unit;
            double xValue = instrument.GetXValue();
            double yValue = instrument.GetYValue();

            double att = 5;
            bool attAuto = false;
            instrument.AttentionAuto = attAuto;
            attAuto = instrument.AttentionAuto;
            instrument.Attention = 5;
            att = instrument.Attention;

            double freq = instrument.CenterFreq_Khz;
            //double voltage = instrument.Unit;
            
            


        }

        public void Shutdown()
        {
            instrument.IsOnline = false;
            chassis.IsOnline = false;
        }


        private static string address;
        private static Chassis_AgE4407x chassis;
        private static Instr_AgE4407x instrument;
    }
}

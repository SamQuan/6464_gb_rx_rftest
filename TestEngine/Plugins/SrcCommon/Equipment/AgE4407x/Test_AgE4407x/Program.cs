using System;
using System.Collections.Generic;
using System.Text;

namespace Test_AgE4407x
{
    class Program
    {
        static void Main(string[] args)
        {
            TestObject test = new TestObject();

            test.Init();

            test.Run();

            test.Shutdown();


        }
    }
}

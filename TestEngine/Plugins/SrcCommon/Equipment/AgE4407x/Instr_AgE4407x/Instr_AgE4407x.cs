// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_AgE4407x.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
//using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.ChassisTypes;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Instr_AgE4407x : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_AgE4407x(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_AgE4407B = new InstrumentDataRecord(
                "Hewlett-Packard  E4407B",				// hardware name 
                "0",  			// minimum valid firmware version 
                "A.04.07");			// maximum valid firmware version 
            ValidHardwareData.Add("AgE4407B", instr_AgE4407B);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassis_AgE4407B = new InstrumentDataRecord(
                "Chassis_AgE4407x",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("AgE440x", chassis_AgE4407B);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_AgE4407x)chassisInit;

            //init command
            this.cmdIdn = "*IDN?";
            this.cmdClear = "*CLS";
            this.cmdRst = "*RST";
            this.cmdFreqCenter = ":FREQ:CENT";
            this.cmdSpan = ":FREQ:SPAN";
            this.cmdAverageState = ":AVER";
            this.cmdResolutionBW = ":BAND";
            this.cmdVedioBW = ":BAND:VID";
            this.cmdMeasMode = ":INST:NSEL";
            this.cmdUnitSys = ":UNIT:POW";
            this.cmdPeakSearch = ":CALC:MARK:MAX";  //on page 252
            this.cmdPeakSearchMode = ":CALC:MARK:PEAK:SEAR:MODE";
            this.cmdResolutionBWAuto = ":BAND:AUTO";
            this.cmdMaxValueForX = ":CALC:MARK:X?";
            this.cmdMaxValueForY = ":CALC:MARK:Y?";
            this.cmdAttention = ":POWer:ATTenuation?";

            //this.cmdAttention = ":SOUR:POW:ATT";
            //this.cmdAttentionAuto = ":SOUR:POW:ATT:AUTO";

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                idn = this.instrumentChassis.Query_Unchecked(this.cmdIdn, this);
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                idn = this.instrumentChassis.Query_Unchecked(this.cmdIdn, this);
                string[] idnArray = idn.Split(',');
                return idnArray[0] + " " + idnArray[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            Clear();
            Reset();
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                try
                {
                    base.IsOnline = value;
                }
                catch (Exception ex)
                {
                    string m = ex.ToString();
                    throw;
                }

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion


        #region ESA special function


        /// <summary>
        /// Set or Get the center frequency
        /// </summary>
        public double CenterFreq_Khz
        {
            
            get
            {
                string command = cmdFreqCenter + "?";
                string rep = this.instrumentChassis.Query_Unchecked(command, this);

                //if (rep.Contains("+"))
                //{
                //    rep.Remove("+");
                //}
                //centerFreq = double.Parse(rep);

                return centerFreq;
            }
            set
            {
                centerFreq = value;
                string command = cmdFreqCenter + " " + centerFreq.ToString() + "khz";
                this.instrumentChassis.Write_Unchecked(command,this);
                
            }
        }

        /// <summary>
        /// Set or Get the Span
        /// </summary>
        public double Span_Khz
        {
            get
            {
                string command = cmdSpan + "?";
                string rep = this.instrumentChassis.Query_Unchecked(command, this);

                return span;
            }
            set
            {
                span = value;
                string command = cmdSpan + " " + span.ToString() + "khz";
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }


        /// <summary>
        /// Set or Get the average state
        /// </summary>
        public bool AvgState
        {
            get
            {
                string command = cmdAverageState + "?";
                string rep = this.instrumentChassis.Query_Unchecked(command, this);

                return averageState;
            }
            set
            {
                string com;
                if (value)
                {
                    averageState = value;
                    com = "1";
                }
                else
                { 
                    averageState = value;
                    com = "0";
                }

                string command = cmdAverageState + " " + com;
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }


        /// <summary>
        /// Set or Get the resolution bandwidth
        /// </summary>
        public double ResolutionBW_Khz
        {
            get
            {
                string command = cmdResolutionBW + "?";
                string rep = this.instrumentChassis.Query_Unchecked(command, this);
                if (rep.Contains("+"))
                {
                    rep.Replace("+", "");
                }
                resolutionBW = double.Parse(rep);
                resolutionBW = resolutionBW / 1000;
                return resolutionBW;
            }
            set
            {
                if (resoBWAuto)
                {
                    resoBWAuto = false; 
                }
                resolutionBW = value;
                string command = cmdResolutionBW + " " + resolutionBW.ToString() + "khz";
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }


        /// <summary>
        /// Set or Get the vedio bandwidth, and the vedio must 
        /// be 1,3,10,30,100,300
        /// </summary>
        public double VedioBW_Khz
        {
            get
            {
                string command = cmdVedioBW + "?";
                string rep = this.instrumentChassis.Query_Unchecked(command, this);

                if (rep.Contains("+"))
                {
                    rep.Replace("+", "");
                }
                vedioBW = double.Parse(rep);
                vedioBW = vedioBW / 1000;
                return vedioBW;
            }
            set
            {
                vedioBW = value;
                string command = cmdVedioBW + " " + vedioBW.ToString() + "khz";
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }


        /// <summary>
        /// Set or Get the measure mode
        /// </summary>
        public MeasureMode MeasMode
        {
            get
            {
                string command = cmdMeasMode + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);

                return measMode;
 
            }
            set
            {
                measMode = value;
                string command = cmdMeasMode + " " + measMode.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }

        /// <summary>
        /// Set or Get the Unit System
        /// </summary>
        public UnitSystem Unit
        {
            get
            {
                string command = cmdUnitSys + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return unit;
            }
            set
            {
                unit = value;
                string command = cmdUnitSys + " " + unit.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        /// <summary>
        /// Get or Set Peak Search Mode
        /// </summary>
        public PKSearchMode PeakSearchMode
        {
            get 
            {
                string command = this.cmdPeakSearchMode + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return peakSearchMode;
            }
            set
            {
                peakSearchMode = value;
                string command = this.cmdPeakSearchMode + " " + peakSearchMode.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }

        /// <summary>
        /// execute the peak search
        /// </summary>
        public void PeakSearch()
        {

            this.instrumentChassis.Write_Unchecked(cmdPeakSearch, this);
        }


        public bool ResolutionBWAuto
        {
            get
            {
                string command = this.cmdResolutionBWAuto + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res == "1")
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            set
            {
                resoBWAuto = value;
                string command;
                if (resoBWAuto)
                {
                    command = this.cmdResolutionBWAuto + " " + "1";
                }
                else
                {
                    command = this.cmdResolutionBWAuto + " " + "0";
                }
                this.instrumentChassis.Write_Unchecked(command, this);
                
            }


        }

        /// <summary>
        /// Get the value from X-axis
        /// </summary>
        /// <returns>x-axis value</returns>
        public double GetXValue()
        {
            string res = this.instrumentChassis.Query_Unchecked(this.cmdMaxValueForX, this);
            if (res.Contains("+"))
            {
                res =res.Replace("+", "");
            }
            double value = double.Parse(res);
            return value;

        }

        /// <summary>
        /// Get the value from Y-axis
        /// </summary>
        /// <returns>y-axis value</returns>
        public double GetYValue()
        {
            string res;
            double value = 0;
            while(!WaittingForOperationComplete())
            {
                 System.Threading.Thread.Sleep(200);
                
            }
            res = this.instrumentChassis.Query_Unchecked(this.cmdMaxValueForY, this);
            if (res.Contains("+"))
            {
                res = res.Replace("+", "");
                //res.Substring(1);
                //sres.Remove(0);
            }
            value = double.Parse(res.ToString());
           
            return value;

        }

        public bool AttentionAuto
        {
            get
            {
                string command = this.cmdAttentionAuto + "?";
                string res = this.instrumentChassis.Query_Unchecked(command,this);
                if (res.Contains("+"))
                {
                    res = res.Replace("+", "");
                }
                if (res.Trim() == "1")
                {
                    attentionAuto = true;
                }
                else
                {
                    attentionAuto = false;
                }
                return attentionAuto;
            }
            set
            {
                attentionAuto = value;
                string command;
                if (attentionAuto == true)
                {
                    command = this.cmdAttentionAuto + " 1";

                }
                else
                {
                    command = this.cmdAttentionAuto + " 0";
                }
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        public double Attention
        {
            get
            {
                this.cmdAttention = ":POWer:ATTenuation";
                string command = this.cmdAttention + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                   res = res.Replace("+", "");
                }
                attention = double.Parse(res.Trim());
                return attention;

 
            }
            set
            {
                //this.AttentionAuto = false;
                attention = value;
                this.cmdAttention = ":POWer:ATTenuation";
                string command = this.cmdAttention + " " + attention.ToString() + "db";
                this.instrumentChassis.Write_Unchecked(command, this);
 
            }
        }

        /// <summary>
        /// Get or Set the enable State
        /// </summary>
        public bool OutputEnable
        {
            get
            {
                string command = ":OUTP?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res = res.Replace("+", "");
                }
                if (res.Trim() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                int temp;
                if (value)
                {
                    temp = 1;
                }
                else
                {
                    temp = 0;
                }
                string command = ":OUTP " + temp.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Get or Set the coupling mode, for example AC, DC
        /// </summary>
        public CouplingMode CoupMode
        {
            get
            {
                string command = ":INPut:COUPling?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res = res.Replace("+", "");
                }
                if (res.Trim() == CouplingMode.AC.ToString())
                {
                    return CouplingMode.AC;
                }
                else
                {
                    return CouplingMode.DC;
                }

            }
            set
            {
                string command = ":INPut:COUPling " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }

        }


        public bool ContinueMode
        {
            get
            {
                string command = ":INIT:CONT?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res =res.Replace("+", "");
                }
                if (res.Trim() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

                
            }
            set
            {
                int temp;
                if (value)
                {
                    temp = 1;

                }
                else
                {
                    temp = 0;
                }
                string command = ":INIT:CONT " + temp.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Get or Set the sweep time.
        /// </summary>
        public double SweepTime_ms
        {
            get
            {
                double result;
                if (!SweepTimeAuto)
                {
                    string command = ":SWEep:TIME?";
                    string res = this.instrumentChassis.Query_Unchecked(command, this);
                    if (res.Contains("+"))
                    {
                        res =res.Replace("+", "");
                    }
                    result = double.Parse(res.Trim());
                    result = result * 1000;
                }
                else
                {
                    result = double.NaN;
                }
                return result;
            }
            set
            {
                SweepTimeAuto = false;
                string command = ":SWEep:TIME " + value.ToString() + "ms";
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        /// <summary>
        /// Get or Set the Sweep time to auto mode or not.
        /// </summary>
        public bool SweepTimeAuto
        {
            get
            {
                string command = ":SWEep:TIME:AUTO?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Trim() == "1")
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            set
            {
                string command;
                if (value)
                {
                    command = ":SWEep:TIME:AUTO 1";

                }
                else
                {
                    command = ":SWEep:TIME:AUTO 0";
                }
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }


        /// <summary>
        /// Get or Set the display status
        /// </summary>
        public bool EnableDisplay
        {
            get 
            {
                string command = ":DISP:ENABLe?";
                string result = this.instrumentChassis.Query_Unchecked(command, this);
                if (result.Trim() == "1")
                {
                    enableDisplay = true;
                }
                else
                {
                    enableDisplay = false;
                }
                return enableDisplay;
            }
            set 
            { 
                enableDisplay = value;
                int a = 0;
                if (enableDisplay)
                {
                    a = 1;
                }
                string command = ":DISP:ENABLE " + a.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }
	

        public void Sweep()
        {
            string command = ":INIT:IMM";
            this.instrumentChassis.Write_Unchecked(command, this);
        }

        public enum MeasureMode
        {
            SA = 1,
            GSM = 3,
            CDMA = 4,
            PNOISE = 14,
            EDGE = 252,
            NFIGURE = 219,
            BLUETOOTH = 228,
            CATV = 227,
            MAN = 229,
            LINK = 231

        }

        public enum UnitSystem
        {
            DBM,
            DBMV,
            DBUV,
            DBUA,
            V,
            W,
            A
        }

        public enum PKSearchMode
        {
            PARameter,
            MAXimum
        }

        public enum CouplingMode
        {
            AC,
            DC
        }

        #endregion


        #region additional method

        /// <summary>
        /// Get the identity from instrument
        /// </summary>
        public string Idn
        {
            get
            {
                idn = this.instrumentChassis.Query_Unchecked(cmdIdn, this);
                return idn;
            }
            
        }


        /// <summary>
        /// Reset the instrument
        /// </summary>
        public void Reset()
        {
            this.instrumentChassis.Write_Unchecked(cmdRst, this);
        }


        /// <summary>
        /// Clear the instrument
        /// </summary>
        public void Clear()
        {
            this.instrumentChassis.Write_Unchecked(cmdClear, this);
        }

        /// <summary>
        /// Set or Get the average count
        /// </summary>
        public int AverageCount
        {
            get
            {
                string command = ":AVERage:COUNt?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res =res.Replace("+", "");
                }
                int result = int.Parse(res.Trim());
                return result;
            }
            set
            {
                string command = ":AVERage:COUNt " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }

        /// <summary>
        /// Set or Get the average type
        /// </summary>
        public AverageType AvgType
        {
            get
            {
                AverageType type = AverageType.NAN;
                if (averageState)
                {
                    string command = ":AVERage:TYPE?";
                    string res = this.instrumentChassis.Query_Unchecked(command, this);
                    if (res.Contains("+"))
                    {
                        res = res.Replace("+", "");
                    }
                    if (res.Trim().ToLower() == "vedio")
                    {
                        type = AverageType.VIDeo;
                    }
                    else if (res.Trim().ToLower() == "rms")
                    {
                        type = AverageType.RMS;
                    }
                }
                else
                {
                    type = AverageType.NAN;
                }
                return type;
            }
            set
            {
                averageState = true;
                string command = ":AVERage:TYPE " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }

        /// <summary>
        /// average type list
        /// </summary>
        public enum AverageType
        {
            VIDeo,
            RMS,
            NAN
        }
        

        /// <summary>
        /// wait for operation complete
        /// </summary>
        /// <returns></returns>
        private bool WaittingForOperationComplete()
        {
            string command = "*OPC?";
            string res = this.instrumentChassis.Query_Unchecked(command,this);
            if (res.Contains("+"))
            {
                res = res.Replace("+", "");
            }
            if (res.Trim() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_AgE4407x instrumentChassis;

        private CouplingMode curMode;
        private string idn;
        private double centerFreq;
        private double span;
        private bool averageState;
        private double resolutionBW;
        private double vedioBW;
        private MeasureMode measMode;
        private UnitSystem unit;
        private PKSearchMode peakSearchMode;
        private bool resoBWAuto;
        private double xValueRead;
        private double yValueRead;
        private double attention;
        private bool attentionAuto;
        private bool enableDisplay;
        #endregion


        #region command area

        private string cmdIdn;
        private string cmdClear;
        private string cmdRst;
        private string cmdFreqCenter;
        private string cmdSpan;
        private string cmdAverageState;
        private string cmdResolutionBW;
        private string cmdVedioBW;
        private string cmdMeasMode;
        private string cmdUnitSys;
        private string cmdPeakSearch;
        private string cmdPeakSearchMode;
        private string cmdResolutionBWAuto;
        private string cmdMaxValueForX;
        private string cmdMaxValueForY;
        private string cmdAttention;
        private string cmdAttentionAuto;
        #endregion
    }
}

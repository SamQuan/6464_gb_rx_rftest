// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_ILX79800E_OpticSource.cs
//
// Author: peter.lin, 2007 
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_ILX79800E_OpticSource : InstType_TunableLaserSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_ILX79800E_OpticSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord ILX79800E_Data = new InstrumentDataRecord(
                "79800E",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("ILX79800E_Data", ILX79800E_Data);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_ILX7900B",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "10.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_ILX7900B", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_ILX7900B)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the Channel IDN?                
                string resp = instrumentChassis.Query_Unchecked("CHAN " + base.Slot + ";IDN?", null);
                string HwVer = resp.Substring(1, 6);

                return HwVer;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //this.PowerUnits = InstType_TunableLaserSource.EnPowerUnits.dBm;

            // defaults state = 1510nm wavelength, power -10dBm, units dBm, power on
            //this.WavelengthINnm = 1510;
            this.Power = -10;
            this.BeamEnable = true;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region TLS Instrument overrides

        public override bool BeamEnable
        {
            get
            {
                m_BeamEnable = (Beam_Enable)Convert.ToInt16(instrumentChassis.Query_Unchecked("CHAN " + base.Slot + ";OUT?", this));
                if (m_BeamEnable == Beam_Enable.Beam_On)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (value)
                {
                    m_BeamEnable = Beam_Enable.Beam_On;
                }
                else
                {
                    m_BeamEnable = Beam_Enable.Beam_Off;
                }
                instrumentChassis.Write_Unchecked("CHAN " + base.Slot + ";OUT " + ((int)m_BeamEnable).ToString(), this);
                opComplete();
            }
        }

        public override double MaximumOuputPower
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override double MaximumWavelengthINnm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override double MinimumOutputPower
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override double MinimumWavelengthINnm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override double Power
        {
            get
            {
                m_Power = Convert.ToDouble(instrumentChassis.Query_Unchecked("CHAN " + base.Slot + ";LEVEL?", this));
                // power is returned in watts
                return m_Power;

            }
            set
            {
                instrumentChassis.Write_Unchecked("CHAN " + base.Slot + ";LEVEL " + value.ToString(), this);
                opComplete();
            }
        }

        public override InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override double WavelengthINnm
        {
            get
            {
                //returns value in nm
                m_Wavelength = Convert.ToDouble(instrumentChassis.Query_Unchecked("CHAN " + base.Slot + ";WAVE?", this));
                return m_Wavelength;
            }
            set
            {
                instrumentChassis.Write_Unchecked("CHAN " + base.Slot + ";WAVE " + value.ToString(), this);
                opComplete();
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_ILX7900B instrumentChassis;

        /// <summary>
        ///  valid laser status  ie. either on or off
        /// </summary>
        private enum Beam_Enable : uint
        {
            Beam_On = 1,
            Beam_Off = 0
        }
        /// <summary>
        /// enable the beam/laser
        /// </summary>
        private Beam_Enable m_BeamEnable;

        /// <summary>
        /// last operation complete status
        /// </summary>
        private void opComplete()
        {
            int comp = 0, count = 0;
            while (comp != 1)
            {
                comp = Convert.ToInt16(instrumentChassis.Query_Unchecked("*OPC?", this));
                count++;
            }
        }
        /// <summary>
        /// enable the Power Units
        /// </summary>
        private InstType_TunableLaserSource.EnPowerUnits m_PowerUnits;
        /// <summary>
        /// laser wavelength
        /// </summary>
        private double m_Wavelength;
        /// <summary>
        /// laser Power
        /// </summary>
        private double m_Power;
        /// <summary>
        /// laser Power Units
        /// </summary>
        private int m_intPowerUnits;
        #endregion

    }
}

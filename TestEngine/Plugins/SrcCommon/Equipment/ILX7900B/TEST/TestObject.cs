using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace ILX7900B_Harness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_ILX7900B testChassis;
        
        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_ILX79800E_OpticSource LS1;        
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::1::INSTR";
        const string chassisName = "ILX7900B Chassis";
        const string instr1Name = "LS1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis object
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_ILX7900B(chassisName, "Chassis_ILX7900B", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            LS1 = new Inst_ILX79800E_OpticSource(instr1Name, "Inst_ILX79800E_OpticSource", "1", "1", testChassis);
            TestOutput(instr1Name, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            LS1.IsOnline = true;
            TestOutput(instr1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            LS1.BeamEnable = false;
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** SetDefaultState Test ***");
            
            LS1.SetDefaultState();

            TestOutput("Power:" + LS1.Power.ToString() );
            TestOutput("BeamEnable:" + LS1.BeamEnable.ToString());         
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            //Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

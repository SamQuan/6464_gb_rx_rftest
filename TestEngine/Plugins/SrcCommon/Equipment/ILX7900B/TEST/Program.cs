// Test Harness which is buildable on top of NUnit framework and Test Engine.

// Logging is performed by the Test Engine standard logger. 
// When bad things (i.e. exceptions) happen in normal execution, you can find a log in the 
// errorlog.txt file in the LogStore directory below where the main EXE lives.
// So, when debugging, it will probably be in ./bin/Debug/LogStore/errorlog.txt relative to this file!

// Recommend that users install TestDriven.NET 2.0 which gives 
// Test running capability from within C# Express.

// This allows you to run test code in the IDE, including in the debugger. 
// Just right-click on the [TestFixture] or [Test] tag to bring up the context menu.

// Also, TestDriven allows you to run unit-tests in the NUnit-GUI (installed as part of Test Driven) 
// as a right-click on the C# Project (Test With).

// Syntax ([TestFixture][Test] etc...) here in this template is from NUnit.

// This project is also a standalone console application, so you don't need TestDriven.NET installed to run it!
// By default output (BugOut calls) goes to console, but this can be configured.

// Links: 
//      TestDriven.NET: http://www.testdriven.net/Default.aspx?tabid=27 
//      NUnit(help for attributes): http://www.nunit.org

// Template by Paul Annetts 2006. 
// Tested in C# Express with TestDriven.NET 2.0 beta v 1438 (which includes NUnit 2.2.6).

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ILX7900B_Harness
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create the test object
            Test_Object test = new Test_Object();
            // Call the setup function (includes putting equipment online)
            test.Setup();
            // Remember the time
            DateTime start = DateTime.Now;
            try
            {
                // TODO: Call some real tests... These are demonstrations
                test.T01_FirstTest(); // will pass

                test.T02_SecondTest(); // will fail - throws AssertException
            }
            finally
            {
                // Shutdown, whether this is due to an exception (Assert failure, or otherwise).
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            // Need this line due to the Logging Thread hanging around
            Environment.Exit(0);
        }
    }
}

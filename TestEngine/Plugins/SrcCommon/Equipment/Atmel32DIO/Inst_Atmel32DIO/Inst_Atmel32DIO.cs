// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Atmel32DIO.cs
//
// Author: jerryxw.hu, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_Atmel32DIO : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Atmel32DIO(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Atmel32DIO",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Atmel32DIO)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Hardware_Unknown";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// Disable all output
        /// </summary>
        public override void SetDefaultState()
        {
            this.DisableAllChannels();
        }

        /// <summary>
        /// Enable channel x
        /// </summary>
        /// <param name="chanNo">Channel number :0~15</param>
        public void OutputEnable(int chanNo)
        {
            if (chanNo < 16 && chanNo>=0)
            {
                command = "e" + chanNo.ToString();
                System.Threading.Thread.Sleep(200);
                this.instrumentChassis.WriteLine(command, this);
            }
            else
            {
                throw new Exception("Invalid channel number !");
            }
        }

        /// <summary>
        /// Disable channel x
        /// </summary>
        /// <param name="chanNo">Channel number :0~15</param>
        public void OutputDisable(int chanNo)
        {
            if (chanNo < 16 && chanNo >= 0)
            {
                command = "d" + chanNo.ToString();
                this.instrumentChassis.WriteLine(command, this);
            }
            else
            {
                throw new Exception("Invalid channel number !");
            }
        }

        /// <summary>
        /// Disable all channels
        /// </summary>
        public void DisableAllChannels()
        {
           command = "off";
           this.instrumentChassis.WriteLine(command, this);
        }

        /// <summary>
        /// Enable all channels
        /// </summary>
        public void EnableAllChannels()
        {
            command = "on";
            this.instrumentChassis.WriteLine(command, this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Atmel32DIO instrumentChassis;
        private string command;
        #endregion
    }
}

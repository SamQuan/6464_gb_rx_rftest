// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ar9717.cs
//
// Author: HEIKO.FETTIG, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Anritsu MS9717A OSA Chassis Driver
    /// </summary>
    public class Chassis_Ar9717 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
        public Chassis_Ar9717(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord ar9717AData = new ChassisDataRecord(
               "ANRITSU MS9717A",			// hardware name 
               "0",			// minimum valid firmware version 
               "V1.0&V3.3");		// maximum valid firmware version 
            ValidHardwareData.Add("AR9717A", ar9717AData);
        }
        #endregion

        #region Chassis virtual abstracts
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    this.Write("*CLS", null);
                }
            }
        }
        /// <summary>
        /// returns the instrument error decoded
        /// </summary>
        /// <returns></returns>
        public override string GetErrorString()
        {
            string errStr = this.Query_Unchecked("ERR?", null);
            errStr = errStr + " : " + FullErrorDescription(errStr);
            return errStr;

        }
        #endregion
        private string FullErrorDescription(string strErrorNumber)
        {
            string strResponse = "";
            // decode the error
            switch (strErrorNumber)
            {
                case "001":
                    strResponse = "RAM";
                    break;
                case "002":
                    strResponse = "Slit 1";
                    break;
                case "003":
                    strResponse = "Slit 2";
                    break;
                case "004":
                    strResponse = "Wavelenegth Alignment";
                    break;
                case "005":
                    strResponse = "Attenuator";
                    break;
                case "007":
                    strResponse = "Light Source";
                    break;
                case "008":
                    strResponse = "Grating";
                    break;
                case "009":
                    strResponse = "Offset Error";
                    break;
                case "010":
                    strResponse = "Excess Optical Input";
                    break;
                case "100":
                    strResponse = "Auto measurement not completed normally";
                    break;
                case "101":
                    strResponse = "No Peak Found";
                    break;
                case "102":
                    strResponse = "No Dip Found";
                    break;
                case "104":
                    strResponse = "Trace Marker Not displayed";
                    break;
                case "110":
                    strResponse = "Optical level insufficient for wavelength calibration";
                    break;
                case "111":
                    strResponse = "Wavelenght Calibration";
                    break;
                case "112":
                    strResponse = "Optical level insufficient for optical axis auto alignment";
                    break;
                case "113":
                    strResponse = "Optical axis alignment";
                    break;
                case "114":
                    strResponse = "Resolution Calibration";
                    break;
                case "115":
                    strResponse = "TLS Calibration";
                    break;
                case "201":
                    strResponse = "Input value exceeds permitted range";
                    break;
                case "205":
                    strResponse = "Cannot be executed during Smooth processing";
                    break;
                case "206":
                    strResponse = "Cannot be executed during Peak Hold";
                    break;
                case "207":
                    strResponse = "Cannot be executed during external triggering measurement";
                    break;
                case "210":
                    strResponse = "Command valid only in Spectrum Mode";
                    break;
                case "211":
                    strResponse = "Command invalid in Auto measure mode";
                    break;
                case "212":
                    strResponse = "Command invalid in this condition";
                    break;
                case "213":
                    strResponse = "Command invalid while in memory list display";
                    break;
                case "214":
                    strResponse = "Command invalid during Title input";
                    break;
                case "215":
                    strResponse = "Command invalid during other inputs";
                    break;
                case "216":
                    strResponse = "Command invalid during measurement";
                    break;
                case "218":
                    strResponse = "Command only valid in Power Monitor Mode";
                    break;
                case "219":
                    strResponse = "Command only valid in Waveform Analysis";
                    break;
                case "220":
                    strResponse = "Command only valid in Normal Display";
                    break;
                case "221":
                    strResponse = "Command only valid in 3D Display";
                    break;
                case "222":
                    strResponse = "Command invalid in Split Screen Display";
                    break;
                case "223":
                    strResponse = "Command invalid during Normalize Display";
                    break;
                case "224":
                    strResponse = "Command invalid during 3D display";
                    break;
                case "225":
                    strResponse = "Command invalid during 3D Type 3 display";
                    break;
                case "226":
                    strResponse = "Command invalid during peak or dip sesarch";
                    break;
                case "227":
                    strResponse = "Command invalid during overlap display";
                    break;
                case "228":
                    strResponse = "Command invalid during Max Hold display";
                    break;
                case "229":
                    strResponse = "Command invalid during Power Monitor measurement";
                    break;
                case "230":
                    strResponse = "Scale should be LOG";
                    break;
                case "231":
                    strResponse = "Scale should be LIN";
                    break;
                case "232":
                    strResponse = "Level units wrong";
                    break;
                case "233":
                    strResponse = "Can not execute during Application measurement";
                    break;
                case "234":
                    strResponse = "Can not execute during Opt. Amp measurement";
                    break;
                case "235":
                    strResponse = "Can not execute during Zone Marker Display";
                    break;
                case "236":
                    strResponse = "Can not execute when Zero Span is 0nm";
                    break;
                case "237":
                    strResponse = "Can not execute in Auto Mode (PMD)";
                    break;
                case "238":
                    strResponse = "Can not execute while Analysis mode selected";
                    break;
                case "239":
                    strResponse = "Can not execute while Sweep Analysis seeected";
                    break;
                case "240":
                    strResponse = "Not Trace A or B";
                    break;
                case "241":
                    strResponse = "Can not execute when trace A-B or B-A";
                    break;
                case "242":
                    strResponse = "Can not execute when trace A-B";
                    break;
                case "243":
                    strResponse = "Can not execute when trace B-A";
                    break;
                case "244":
                    strResponse = "Can not execute when trace is A&B";
                    break;
                case "245":
                    strResponse = "Can not execute while Multi Peak executing";
                    break;
                case "246":
                    strResponse = "Can not execute OPt Amp Test";
                    break;
                case "247":
                    strResponse = "Can not execute when sample point is 5001";
                    break;
                case "248":
                    strResponse = "Can not execute in Plzn Null. Plse. and WDM test modes";
                    break;
                case "249":
                    strResponse = "Can not be set under measurement conditions";
                    break;
                case "250":
                    strResponse = "Wavelengths differ in mem A & B";
                    break;
                case "251":
                    strResponse = "Resolutions differ in mem A & B";
                    break;
                case "252":
                    strResponse = "Number of points differ in A & B";
                    break;
                case "253":
                    strResponse = "Air vac settings differ in A & B";
                    break;
                case "254":
                    strResponse = "Option";
                    break;
                case "260":
                    strResponse = "Can not execute in during TLS Tracking";
                    break;
                case "300":
                case "301":
                case "302":
                case "303":
                case "304":
                case "305":
                    strResponse = "FloppyDisk[" + strResponse + "]";
                    break;

                case "320":
                case "321":
                case "322":
                case "323":
                case "324":
                    strResponse = "Printer[" + strResponse + "]";
                    break;
                case "401":
                    strResponse = "Command: Undefined Header Recieved";
                    break;
                case "402":
                    strResponse = "Command: Numeric data incorrect";
                    break;
                case "403":
                    strResponse = "Command: Real part of numeric data incorrect, or real format input data invalid";
                    break;
                case "404":
                    strResponse = "Command: Index part of numeric data incorrect, or index format input data is invalid";
                    break;
                case "405":
                    strResponse = "Command: Units suffix incorrect";
                    break;
                case "406":
                    strResponse = "Command: Command syntax error";
                    break;
                case "407":
                    strResponse = "Command: PCB command recieved but no controller function";
                    break;
                case "420":
                    strResponse = "Abnormalitiy in connecting with TLS";
                    break;
                case "421":
                    strResponse = "No response from TLSA";
                    break;
                case "501":
                    strResponse = "Calculation cannot be performed because trace in memory was recorded with different conditions";
                    break;
                case "502":
                    strResponse = "Set memory for write status";
                    break;

                default:
                    strResponse = "Unknown error being returned[" + strResponse + "]";
                    break;

            }
            return strResponse + " Error";
        }
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ar9717.cs
//
// Author: HEIKO.FETTIG, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Anritsu MS9717B/C OSA Instrument Driver
    /// </summary>
    public class Inst_Ar9717_OSA : InstType_OSA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ar9717_OSA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord ar9717AData = new InstrumentDataRecord(
                "ANRITSU MS9717A",			// hardware name 
                "0",			// minimum valid firmware version 
                "V1.0&V3.3");		// maximum valid firmware version 
            ValidHardwareData.Add("AR9717A", ar9717AData);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ar9717",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ar9717)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string

                string idn = instrumentChassis.Query("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                string fwv = idn.Split(',')[3].Trim();

                // Log event 
                LogEvent("'FirmwareVersion' returned : " + fwv);

                return fwv;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {                
                // Read the chassis ID string and split the comma seperated fields

                string[] idn = instrumentChassis.Query("*IDN?", null).Split(',');


                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        /// <remarks>
        /// Resets the instrument, sets up sweep range 1525-1565nm and starts single sweep.
        /// </remarks>
        public override void SetDefaultState()
        {
            // Send reset command to instrument
            //this.Reset(); mqn taken out as it takes too long and it may reset auto align and mess with the spectral rbw
            // set scan area to C-Band
            this.WavelengthStart_nm = 1525;
            this.WavelengthStop_nm = 1565;
            this.Amplitude_dBperDiv = 10;
            this.TracePoints = 2001;
            this.VideoAverage = 0;
            this.VideoBandwidth = 100;
            this.ResolutionBandwidth = 0.2;
            this.SweepMode = SweepModes.Single;
            this.MarkerOn = true;
            // start single sweep
            this.SweepMode = SweepModes.Single;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        /// <remarks>If set to true, sets the instrument into the default state.</remarks>
        public override bool IsOnline
        {
            get
            {

                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region OSA InstrumentType property overrides

        /// <summary>
        /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
        /// </summary>
        /// <value>
        /// Set value has to be between 0.1 and 10 db/Div.
        /// </value>
        public override double Amplitude_dBperDiv
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("LOG?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0.1 || value > 10)
                {
                    throw new InstrumentException("Amplitude scale must be be between 0.1 and 10 dB/Div. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    //instrumentChassis.Write("LOG " + value, this);
                    this.ChassisWriteWithCheck("LOG", value, 5, 2);

                }
            }
        }

        /// <summary>
        /// This property is not available on this instrument.
        /// </summary>
        public override bool AutoRange
        {
            get
            {
                throw new InstrumentException("'AutoRange' is not available.");
            }
            set
            {
                throw new InstrumentException("'AutoRange' is not available.");
            }
        }

        /// <summary>
        /// Returns the display trace, as an array of numeric values. 
        /// </summary>
        public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
        {
            get
            {
                // read amplitude values from OSA 
                //string[] resp = instrumentChassis.Query("TSD? 1", this).Split(',');

                byte[] response = instrumentChassis.QueryByteArray("TBD? 1", this);

                // convert byte array response into amplitude array
                int numPoints = this.TracePoints;
                InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
                double waveLength = this.WavelengthStart_nm;
                double waveLengthStep = this.WavelengthSpan_nm / (numPoints - 1);

                for (int index = 0; index < numPoints; index++)
                {
                    // BitConverter uses little endian, we have big endian, so we have to swap the two bytes
                    byte[] ampByte = new byte[2] { response[2 * index + 1], response[2 * index] };
                    short ampInt = BitConverter.ToInt16(ampByte, 0);

                    // values are transferred as 16bit integers and 100 * float value
                    trace[index].power_dB = (double)ampInt * 0.01;
                    trace[index].wavelength_nm = Math.Round(waveLength, 2);
                    waveLength += waveLengthStep;
                }

                return trace;
            }
        }

        /// <summary>
        /// Reads the present marker amplitude for the OSA channel.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the instrument does not return the level in dBm.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerAmplitude_dBm
        {
            get
            {
                // Query the instrument

                string response = instrumentChassis.Query("TMK?", this);

                markerOn = true;  // remember that marker is now on
                // extract amplitude from response (wavelength, amplitude)
                string amplitude = response.Split(',')[1];
                // remove and check unit
                string unit = amplitude.Substring(amplitude.Length - 3);
                if (unit != "DBM")
                {
                    throw new InstrumentException("Instrument returned amplitude with incorrect unit. Amplitude = "
                                        + amplitude);
                }
                else
                {
                    amplitude = amplitude.Substring(0, amplitude.Length - 3);
                }
                // Convert response to double and return
                return Convert.ToDouble(amplitude);
            }
        }

        /// <summary>
        /// Shows/hides the marker for the OSA channel. 
        /// </summary>
        /// <remarks>
        /// Since the instrument does not have way to read whether a marker is active or not,
        /// this is handled using a private variable that is set to true every time the marker
        /// activated and false when all markes are erased.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        public override bool MarkerOn
        {
            get
            {
                // return soft buffer
                return markerOn;
            }
            set
            {
                if (value)
                {
                    // Query the instrument to turn on trace marker at last position 

                    string response = instrumentChassis.Query("TMK?", this);

                    markerOn = true;  // remember that marker is now on
                }
                else
                {
                    // Erase all markers
                    instrumentChassis.Write("EMK", this);
                    markerOn = false;  // remember that marker is now off
                }
            }
        }

        /// <summary>
        /// Reads/sets the marker wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between the current start and stop wavelengths.
        /// </value>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerWavelength_nm
        {
            get
            {
                // Query the instrument   

                string response = instrumentChassis.Query("TMK?", this);

                markerOn = true;  // remember that marker is now on
                // extract wavelength from response (wavelength, wavelength)
                string wavelength = response.Split(',')[0];
                // Convert response to double and return
                return Convert.ToDouble(wavelength);
            }
            set
            {
                // Check whether value is in correct range
                if (value < this.WavelengthStart_nm || value > this.WavelengthStop_nm)
                {
                    throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                         " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
                }
                else
                {
                    // Write the value to the instrument
                    //instrumentChassis.Write("TMK " + value, this);
                    this.ChassisWriteWithCheck("TMK", value, 5, 4);
                }
            }
        }

        /// <summary>
        /// Reads/sets the point averaging for the OSA channel. 
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int PointAverage
        {
            get
            {
                // Query the instrument 

                string response = instrumentChassis.Query("AVT?", this);

                // check if OFF was returned
                if (response == "OFF") response = "0";
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0 || value == 1 || value > 1000)
                {
                    throw new InstrumentException("Point Average must be between 2 and 1000 or 0. Set value = "
                                               + value);
                }
                else
                {
                    // convert 0 to OFF
                    string outValue = Convert.ToString(value);
                    if (value == 0) outValue = "OFF";
                    // Write the value to the instrument
                    ChassisWriteWithCheck("AVT", outValue, 5);

                }
            }
        }

        /// <summary>
        /// Reads/sets the reference level for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -90 and +30 dBm.
        /// </value>
        public override double ReferenceLevel_dBm
        {
            get
            {
                // Query the instrument

                string response = instrumentChassis.Query("RLV?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -90 || value > 30)
                {
                    throw new InstrumentException("Reference level must be between -90 and +30 dBm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument

                    //instrumentChassis.Write("RLV " + value, this);
                    this.ChassisWriteWithCheck("RLV", value, 5, 1);

                }
            }
        }

        /// <summary>
        /// Reads/sets the resolution bandwidth for the OSA channel.  
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 0.05, 0.07, 0.1, 0.2, 0.5, and 1 nm.
        /// </value>
        public override double ResolutionBandwidth
        {
            get
            {

                // Query the instrument   
                string response = instrumentChassis.Query("RES?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);

            }
            set
            {

                double res = 0;
                // Check whether value is in correct range
                if (value <= 0.05) res = 0.05;
                else if (value <= 0.07) res = 0.07;
                else if (value <= 0.1) res = 0.1;
                else if (value <= 0.2) res = 0.2;
                else if (value <= 0.5) res = 0.5;
                else res = 1;
                // Write the value to the instrument
                ChassisWriteWithCheck("RES", res, 10, 2);

            }
        }

        /// <summary>
        /// Returns the executional status for the OSA channel. 
        /// </summary>
        /// <value>
        /// Busy is returned when the OSA is in the process of measureing a spectrum.
        /// Idle is returned when no spectrum is measured. Unspecified is returned when OSA is in power
        /// monitor mode. DataReady is not supported.
        /// </value>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.ChannelState Status
        {
            get
            {
                if (mStatus != ChannelState.Busy)
                {
                    // error as mstatus should be busy from start sweep of other command
                    throw new InstrumentException("Status query with out OSA action request");
                }
                TimeSpan duration = DateTime.Now - mStatusStart;
                if (duration.TotalMilliseconds > mSatusPollDelayms) // impliments a none blocking delay
                {
                    try
                    {
                        if (this.ESRBitMaskSet(mCurrentStatusRegister, mCurrentStatusBitMask) == true) mStatus = ChannelState.DataReady;
                    }
                    catch (ChassisException ce)
                    {
                        // IGNORE 101 no peak found
                        if (!ce.Message.Contains("Failed VISA Query")) // ignore reset lock up
                        {
                            if (!ce.Message.Contains("101 :")) throw new ChassisException("Status generated unexpected OSA error", ce);
                        }
                    }

                    // prepare next poll pause
                    mStatusStart = DateTime.Now;
                }
                // check for OSA Lock up
                duration = DateTime.Now - mDTStartWaitTime;
                if (duration.TotalSeconds > mStatusTimeOutSeconds)
                {
                    throw new InstrumentException("The OSA appears to have locked up, suggest power cycle");
                }
                return mStatus;
            }
        }

        /// <summary>
        /// Reads/sets the sweep capture mode for the OSA channel.
        /// </summary>
        /// <value>
        /// Triggered mode is not available.
        /// Unspecified mode cannot be set.
        /// </value>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.SweepModes SweepMode
        {
            get
            {
                return mode;
            }
            set
            {
                // Select command
                string command = "";
                switch (value)
                {
                    case SweepModes.Single: command = "SSI";  // Start single sweep
                        break;
                    case SweepModes.Continuous: command = "SRT";  // Start continuous sweep
                        break;
                    case SweepModes.Unspecified: throw new InstrumentException("Cannot set to 'Unspecified' mode.");
                    case SweepModes.Triggered: throw new InstrumentException("'Triggered' mode not available.");
                }
                // Write the command to the instrument
                instrumentChassis.Write(command, this);
                this.mode = value;
            }
        }

        /// <summary>
        /// Reads/sets the number of trace points for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 51, 101, 201, 501, 1001, 2001, and 5001.
        /// </value>
        public override int TracePoints
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("MPT?", this);
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                int mpt = 0;

                // Check whether value is in correct range
                if (value <= 51) mpt = 51;
                else if (value <= 101) mpt = 101;
                else if (value <= 251) mpt = 251;
                else if (value <= 501) mpt = 501;
                else if (value <= 1001) mpt = 1001;
                else if (value <= 2001) mpt = 2001;
                else mpt = 5001;

                // Write the value to the instrument
                //instrumentChassis.Write("MPT " + mpt, this);
                this.ChassisWriteWithCheck("MPT", mpt, 10, 0);
            }
        }

        /// <summary>
        /// Reads/sets the video averaging value for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int VideoAverage
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("AVS?", this);
                // check if OFF was returned
                if (response == "OFF") response = "0";
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0 || value == 1 || value > 1000)
                {
                    throw new InstrumentException("Video Average must be between 2 and 1000 or 0. Set value = "
                                               + value);
                }
                else
                {
                    // convert 0 to OFF
                    string outValue = Convert.ToString(value);
                    if (value == 0) outValue = "OFF";
                    // Write the value to the instrument
                    //instrumentChassis.Write("AVS " + outValue, this);
                    this.ChassisWriteWithCheck("AVS", outValue, 10);
                }
            }
        }

        /// <summary>
        /// Reads/sets the video bandwidth for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.
        /// </value>
        public override double VideoBandwidth
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("VBW?", this);
                // extract value
                double vbw = 0;
                if (response.IndexOf("MHZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e6;
                else if (response.IndexOf("KHZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e3;
                else if (response.IndexOf("HZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 2));

                // Convert response to double and return
                return vbw;
            }
            set
            {
                string vbw = "";
                // Check whether value is in correct range
                if (value <= 10) vbw = "10HZ";
                else if (value <= 100) vbw = "100HZ";
                else if (value <= 1000) vbw = "1KHZ";
                else if (value <= 10000) vbw = "10KHZ";
                else if (value <= 100000) vbw = "100KHZ";
                else vbw = "1MHZ";
                // Write the value to the instrument
                ChassisWriteWithCheck("VBW", vbw, 5);

            }
        }

        /// <summary>
        /// Reads/sets the centre wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthCentre_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("CNT?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1750)
                {
                    throw new InstrumentException("Wavelength Centre must be between 600 and 1750 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    ChassisWriteWithCheck("CNT", value, 5, 2);
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength offset for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -1 and +1nm.
        /// </value>
        public override double WavelengthOffset_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("WOFS?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -1 || value > 1)
                {
                    throw new InstrumentException("Wavelength Offset must be between -1.0 and +1.0 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    ChassisWriteWithCheck("WOFS", value, 10, 2);
                    //if (this.WavelengthOffset_nm != value)
                    //    value = value;
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength span for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 0.2 and 1200nm.
        /// </value>
        public override double WavelengthSpan_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SPN?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0 || value > 1200)
                {
                    throw new InstrumentException("Wavelength Span must be between 0.2 and 1200 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    ChassisWriteWithCheck("SPN", value, 5, 2);
                }
            }
        }

        /// <summary>
        /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthStart_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("STA?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1750)
                {
                    throw new InstrumentException("Wavelength Start must be between 600 and 1750 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    ChassisWriteWithCheck("STA", value, 5, 2);
                }
            }
        }

        /// <summary>
        /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1800nm.
        /// </value>
        public override double WavelengthStop_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("STO?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1800)
                {
                    throw new InstrumentException("Wavelength Stop must be between 600 and 1800 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    ChassisWriteWithCheck("STO", value, 5, 2);

                }
            }
        }

        #endregion

        #region OSA InstrumentType function overrides

        /// <summary>
        /// Moves the marker to the minimum signal point on the display. 
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToMinimum()
        {
            // Retrieve the trace

            InstType_OSA.OptPowerPoint[] trace = this.GetDisplayTrace;

            // find min in trace
            InstType_OSA.OptPowerPoint min = trace[0];
            for (int index = 1; index < trace.Length; index++)
            {
                if (trace[index].power_dB < min.power_dB)
                    min = trace[index];
            }
            // set marker to minimum
            this.MarkerWavelength_nm = min.wavelength_nm;
        }

        /// <summary>
        /// Moves the marker to the next peak to the left/right on the display.
        /// </summary>
        /// <param name="directionToMove">
        /// Enumeration representing the direction in which to search for the next peak.
        /// If unspecified is selected, the next peak is searched.
        /// </param>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
        {
            string direction = "";

            switch (directionToMove)
            {
                case Direction.Left: direction = "LEFT";
                    break;
                case Direction.Right: direction = "RIGHT";
                    break;
                case Direction.Unspecified: direction = "NEXT";
                    break;
            }
            // prepare the test status
            this.PrepareForWait(2, 0x01, 200, 5);
            // Move the marker 
            try
            {
                instrumentChassis.Write("PKS " + direction, this);
            }
            catch (ChassisException ce)
            {
                // IGNORE 101 no peak found
                if (!ce.Message.Contains("101 :")) throw new ChassisException("Peak Search " + direction + "generated unexpected OSA error", ce);
            }
            markerOn = true;  // remember that marker is now on

        }

        /// <summary>
        /// Moves the marker to the maximum signal point on the display.
        /// </summary>
        /// <remarks>
        /// Throws anexception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToPeak()
        {

            // prepare the test status
            this.PrepareForWait(2, 0x01, 200, 5);
            try
            {
                instrumentChassis.Write("PKS PEAK", this);
            }
            catch (ChassisException ce)
            {
                // IGNORE 101 no peak found
                if (!ce.Message.Contains("101 :")) throw new ChassisException("Peak Search generated unexpected OSA error", ce);
            }
            markerOn = true;  // remember that marker is now on


        }

        /// <summary>
        /// Starts a measurement sweep, and returns immediately.
        /// </summary>
        /// <remarks>
        /// Status() property should be used to determine sweep progress/completion. 
        /// </remarks>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStop"/>
        public override void SweepStart()
        {

            // prepare the test status
            this.PrepareForWait(2, 0x02, 200, 120);
            // Start the sweep
            instrumentChassis.Write("SSI", this);

        }

        /// <summary>
        /// Stops an active measurement sweep. 
        /// </summary>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        public override void SweepStop()
        {
            // Stop the sweep
            instrumentChassis.Write("SST", this);
        }


        /// <summary>
        /// Gets / sets the internal attenuator of the osa on or off
        /// </summary>
        public override bool Attenuator
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("ATT?", this);
                //true if attenuator is on
                if (response.IndexOf("ON") >= 0)
                    return true;
                return false;
            }
            set
            {
                // Write the value to the instrument
                if (value)
                    this.ChassisWriteWithCheck("ATT", "ON", 10);
                else
                    this.ChassisWriteWithCheck("ATT", "OFF", 10);
            }
        }
        #endregion
        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        public void Reset()
        {
            // Start the reset
            this.PrepareForWait(2, 0x10, 1000, 100);
            instrumentChassis.Write_Unchecked("*RST", this);
            while (this.Status == ChannelState.Busy) ;

            markerOn = false; // remember that marker is now off
        }
       
        /// <summary>
        /// Clear the last state of the ESR register, to be done before any sweep or marker find
        /// </summary>
        /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
        private void ClearESR(byte esrRegister)
        {
            if (esrRegister < 1 || esrRegister > 3)
                throw new InstrumentException("Number of ESR Register has to be between 1 and 3. ESR# = " + esrRegister);
            else
            {

                string response = "0";
                try
                {
                    response = instrumentChassis.Query_Unchecked("ESR" + esrRegister + "?", this);
                }
                catch (Bookham.TestEngine.PluginInterfaces.Chassis.ChassisException)
                {
                    response = "0";
                }
            }
            mStatus = ChannelState.Unspecified; // do not know the state yet

        }
        /// <summary>
        /// check for ESR bits to be set
        /// </summary>
        /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
        /// <param name="esrBitMask">Mask of the bits to check.</param>
        private bool ESRBitMaskSet(byte esrRegister, byte esrBitMask)
        {
            if (esrRegister < 1 || esrRegister > 3)
                throw new InstrumentException("Number of ESR Register has to be between 1 and 3. ESR# = " + esrRegister);
            else
            {
                // check if bit is set
                string response;
                response = instrumentChassis.Query_Unchecked("ESR" + esrRegister + "?", this);
                return ((Convert.ToInt32(response) & (esrBitMask)) == (esrBitMask));
            }
        }
        /// <summary>
        /// check for ESR bits in the mask are set in ESR
        /// </summary>
        /// <param name="esrBitMask">Number of the bit to be checked.</param>
        private bool ESRBitMaskSet(byte esrBitMask)
        {
            // check if bit is set
            string response;
            response = instrumentChassis.Query_Unchecked("*ESR?", this);
            this.mCurrentESR = Convert.ToByte(response);
            return ((this.mCurrentESR & esrBitMask) == esrBitMask);
        }
        /// <summary>
        /// clear any exisiting errors
        /// should be called before any command that has a CheckOSAErrors call after it, apart from Status
        /// </summary>
        private void ClearOSAErrors()
        {
            string response = instrumentChassis.Query_Unchecked("*ESR?", this); // clear errors
        }
        /// <summary>
        /// Send a command and check that it has been set
        /// </summary>
        /// <param name="strCommand">The command header.</param>
        /// <param name="dblValue">Command value.</param>
        /// <param name="dblTimeOutSeconds">Time out for the retry loop.</param>
        /// <param name="intDecPlaces">TNumber of decimal points for the dblValue to return compare.</param>
        private void ChassisWriteWithCheck(string strCommand, double dblValue, double dblTimeOutSeconds, int intDecPlaces)
        {
            string response;
            DateTime StartTime;
            TimeSpan duration;
            StartTime = DateTime.Now;
            Int32 loops = 0; // used to introduce a pause on retry
            dblValue = Math.Round(dblValue, intDecPlaces);  // just make sure the caller does as expected
            do
            {
                response = instrumentChassis.Query(strCommand + "?", this);  // what is there
                if (response.Contains(",") == true) response = response.Split(',')[0]; // peel off first element if marker
                if (Math.Round(Convert.ToDouble(response), intDecPlaces) == dblValue) break;          // does it compare

                instrumentChassis.Write(strCommand + " " + dblValue, this);     // no so send it
                duration = DateTime.Now - StartTime;

                if (loops++ > 0) System.Threading.Thread.Sleep(50);
            } while (duration.TotalSeconds < dblTimeOutSeconds);
        }
        /// <summary>
        /// Send a command and check that it has been set
        /// </summary>
        /// <param name="strCommand">The command header.</param>
        /// <param name="strValue">Command value.</param>
        /// <param name="dblTimeOutSeconds">Time out for the retry loop.</param>
        private void ChassisWriteWithCheck(string strCommand, string strValue, double dblTimeOutSeconds)
        {
            string response;
            DateTime StartTime;
            TimeSpan duration;
            StartTime = DateTime.Now;
            Int32 loops = 0; // used to introduce a pause on retry
            do
            {
                response = instrumentChassis.Query(strCommand + "?", this);  // what is there
                if (response == strValue) break;          // does it compare
                instrumentChassis.Write(strCommand + " " + strValue, this);     // no so send it
                duration = DateTime.Now - StartTime;

                if (loops++ > 0) System.Threading.Thread.Sleep(50);
            } while (duration.TotalSeconds < dblTimeOutSeconds);

        }
        private void PrepareForWait(byte bytReg, byte bytStatusBitMask, int PollIntervalms, int TimeOutSeconds)
        {
            // store the register and bit mask to be used in status
            mCurrentStatusRegister = bytReg;
            mCurrentStatusBitMask = bytStatusBitMask;
            mSatusPollDelayms = PollIntervalms;
            mStatusStart = DateTime.Now;

            // clear any existing status bits
            this.ClearESR(bytReg);
            // clear general error register
            this.ClearOSAErrors();
            // mark current status as busy
            mStatus = ChannelState.Busy;
            // prepare start 
            mDTStartWaitTime = DateTime.Now;
            mStatusTimeOutSeconds = TimeOutSeconds;
        }
        /// <summary>
        /// Auto Align Calibration of the OSA
        /// </summary>
        public override void AutoAlign()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("ALIN 1", this);
            System.Threading.Thread.Sleep(500);
            DateTime startTime = DateTime.Now;
            TimeSpan timeOut = new TimeSpan(0, 5, 0); // reset should not take longer than 60 seconds
            // Check if reset is finished or trap error
            int response = 0;
            do
            {
                response = Convert.ToInt32(this.instrumentChassis.Query("ALIN?", this));
                switch (response)
                {
                    case 0:
                        //Do nothing success
                        break;
                    case 1:
                        //Do nothing keep waiting
                        System.Threading.Thread.Sleep(500);
                        break;
                    case 2:
                        throw new InstrumentException("Insufficient Light Level for Auto Align");
                    case 3:
                        throw new InstrumentException("Aborted due to some other fault");
                }
                if (DateTime.Now.Subtract(startTime) > timeOut)
                    throw new InstrumentException("Auto Align Timed Out!");
            } while (response == 1);
        }

        /// <summary>
        /// Returns the actual resulution bandwidth as displayed on the screen
        /// </summary>
        public double ResolutionBandwidth_Actual
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("ARED?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
        }


        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ar9717 instrumentChassis;
        /// <summary>
        /// Soft marker on/off buffer
        /// </summary>
        private Boolean markerOn;
        private InstType_OSA.SweepModes mode;
        /// <summary>
        /// The status of sweeps or marker sets
        /// </summary>
        private ChannelState mStatus;
        /// <summary>
        /// The current working register for testing status
        /// </summary>
        private byte mCurrentStatusRegister;
        /// <summary>
        /// The current working register test bit mask for testing status, normally specifys only one bit
        /// </summary>
        private byte mCurrentStatusBitMask;
        /// <summary>
        ///Current ESR register value
        /// </summary>
        private byte mCurrentESR;
        /// <summary>
        ///Time out for any status check after a wcommand which involves waiting such as sweep
        /// </summary>
        private Int32 mSatusPollDelayms;
        /// <summary>
        ///Start Time marker for sweep or marker search used for OSA lock up detection
        /// </summary>
        private DateTime mDTStartWaitTime;
        /// <summary>
        ///This is used to delay the next status query period, need to stop polling too often at the same time allowing this thread to do other things
        /// </summary>
        private DateTime mStatusStart;
        /// <summary>
        ///Time out for status poll, if the status is polled for a longer period than this then error thrown
        /// </summary>
        private int mStatusTimeOutSeconds;
        #endregion
    }
}

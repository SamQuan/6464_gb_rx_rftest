using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// 
    /// </summary>
    public enum ILX5910B_SensorType
    {
        /// <summary>
        /// 
        /// </summary>
        UnSupported = 0,
        /// <summary>
        /// 
        /// </summary>
        Thermistor_100uA = 1,
        /// <summary>
        /// 
        /// </summary>
        Thermistor_10uA = 2,
        /// <summary>
        /// 
        /// </summary>
        LM335 = 3,
        /// <summary>
        /// 
        /// </summary>
        AD590 = 4
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ILX5910B_GainType
    {
        /// <summary>
        /// 
        /// </summary>
        Gain_NotSupported = 0,
        /// <summary>
        /// 
        /// </summary>
        Gain_1 = 1,
        /// <summary>
        /// 
        /// </summary>
        Gain_3 = 3,
        /// <summary>
        /// 
        /// </summary>
        Gain_10 = 10,
        /// <summary>
        /// 
        /// </summary>
        Gain_30 = 30,
        /// <summary>
        /// 
        /// </summary>
        Gain_100 = 100,
        /// <summary>
        /// 
        /// </summary>
        Gain_300 = 300
    }
}

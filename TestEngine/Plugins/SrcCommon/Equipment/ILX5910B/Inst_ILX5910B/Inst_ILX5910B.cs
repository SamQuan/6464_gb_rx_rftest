// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_ILX5910B.cs
//
// Author: Jervis.Wang, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// 
    /// </summary>
    public class Inst_ILX5910B : InstType_TecController
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_ILX5910B(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord ilx5910BData = new InstrumentDataRecord(
                "ILX Lightwave,5910B",				// hardware name 
                "0",  			                    // minimum valid firmware version 
                "4.00");			                // maximum valid firmware version 
            ValidHardwareData.Add("ILX5910B", ilx5910BData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord ilx5910BChassisData = new InstrumentDataRecord(
                "Chassis_ILX5910B",							// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "4.00");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_ILX5910B", ilx5910BChassisData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (ChassisType_Visa)base.InstrumentChassis;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                string idnline = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] idn = idnline.Split(',');
                return idn[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string idnline = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] idn = idnline.Split(',');
                return idn[0].Trim() + "," + idn[1].Trim();
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //
            instrumentChassis.Write_Unchecked("*CLS", this);
            
            //
            instrumentChassis.Write_Unchecked("*RST", this);
            
            //
            System.Threading.Thread.Sleep(2000);
            
            //
            OutputEnabled = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;

        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";        

        #endregion

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override CallendarVanDusenCoefficients CallendarVanDusenConstants
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double DerivativeGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double IntegralGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/returns the control mode. Instrument driver should throw 
        /// if a particular mode is not supported
        /// </summary>
        public override InstType_TecController.ControlMode OperatingMode
        {
            get
            {
                // Query the control mode
                string rtn = instrumentChassis.Query_Unchecked("MODE?", this);

                // Convert return to ControlMode enum for return
                ControlMode mode;
                switch (rtn)
                {
                    case "T": mode = ControlMode.Temperature; break;
                    case "R": mode = ControlMode.Resistance; break;                    
                    default: throw new InstrumentException("Unrecognised control mode '" + rtn + "'");
                }

                // Return mode
                return mode;
            }
            set
            {
                // Initialise the mode string
                string mode;
                switch (value)
                {
                    case ControlMode.Temperature: mode = "T"; break;
                    case ControlMode.Resistance: mode = "R"; break;
                    default: throw new InstrumentException("Invalid control mode " + value.ToString());
                }

                // Set to instrument
                instrumentChassis.Write_Unchecked("MODE:" + mode, this);
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double ProportionalGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented.
        /// </summary>
        public override double SensorCurrent_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented.
        /// </summary>
        public override double SensorResistanceActual_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Not Implemented.
        /// </summary>
        public override double SensorResistanceSetPoint_ohm
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// ???
        /// </summary>
        public override SensorType Sensor_Type
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");             
            }            
        }

        /// <summary>
        /// Gets the Sensor Type of ILX 5910B.
        /// </summary>
        public ILX5910B_SensorType ILX5910BSensorType
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("SEN?", this);

                int rtn = Convert.ToInt32(rtnLine);
                
                ILX5910B_SensorType rtnSensorType;

                switch (rtn)
                {
                    case 1:
                        rtnSensorType = ILX5910B_SensorType.Thermistor_10uA;
                        break;
                    case 2:
                        rtnSensorType = ILX5910B_SensorType.Thermistor_10uA;
                        break;
                    case 3:
                        rtnSensorType = ILX5910B_SensorType.LM335;
                        break;
                    case 4:
                        rtnSensorType = ILX5910B_SensorType.AD590;
                        break;
                    default:
                        rtnSensorType = ILX5910B_SensorType.UnSupported;
                        break;
                }
                return rtnSensorType;
            }
        }

        /// <summary>
        /// Sets/Gets the S-H Coefficients
        /// </summary>
        public override SteinhartHartCoefficients SteinhartHartConstants
        {
            get
            {

                string rtnLine = instrumentChassis.Query_Unchecked("CONST?", this);
                string[] rtnData = rtnLine.Split(',');

                SteinhartHartCoefficients rtnSHData = new SteinhartHartCoefficients();
                if (rtnData.Length == 3)
                {
                    rtnSHData.A = Convert.ToDouble(rtnData[0]);
                    rtnSHData.B = Convert.ToDouble(rtnData[1]);
                    rtnSHData.C = Convert.ToDouble(rtnData[2]);
                }
                else if (rtnData.Length == 2)
                {
                    rtnSHData.A = Convert.ToDouble(rtnData[0]);
                    rtnSHData.B = Convert.ToDouble(rtnData[1]);
                }
                else
                {
                    throw new Exception("SteinhartHartCoefficients Index Error!");
                }
                return rtnSHData; 
            }
            set
            {
                instrumentChassis.Write_Unchecked("CONST " + value.A.ToString().Trim() +
                    value.B.ToString().Trim() + value.C.ToString().Trim(),this);
                // Format???
            }
        }
                
        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecCurrentActual_amp
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecCurrentCompliance_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecCurrentSetPoint_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecResistanceAC_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecResistanceDC_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecVoltageActual_volt
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecVoltageCompliance_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double TecVoltageSetPoint_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/returns the output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Query the output state
                string rtn = instrumentChassis.Query_Unchecked("OUT?", this);

                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set output
                instrumentChassis.Write_Unchecked("OUT " + boolVal, this);
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double SensorTemperatureActual_C
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public override double SensorTemperatureSetPoint_C
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");               
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/Gets Gain
        /// </summary>
        public ILX5910B_GainType Gain
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("GAIN?", this);
                
                ILX5910B_GainType rtnGain;
                
                switch (rtnLine)
                {
                    case "1":
                        rtnGain = ILX5910B_GainType.Gain_1;
                        break;
                    case "3":
                        rtnGain = ILX5910B_GainType.Gain_3;
                        break;
                    case "10":
                        rtnGain = ILX5910B_GainType.Gain_10;
                        break;
                    case "30":
                        rtnGain = ILX5910B_GainType.Gain_30;
                        break;
                    case "100":
                        rtnGain = ILX5910B_GainType.Gain_100;
                        break;
                    case "300":
                        rtnGain = ILX5910B_GainType.Gain_300;
                        break;
                    default:
                        rtnGain = ILX5910B_GainType.Gain_NotSupported;
                        break;
                }               

                return rtnGain;
            }
            set
            {
                string sendLine;

                switch (value)
                {       
                    case ILX5910B_GainType.Gain_1:
                        
                    case ILX5910B_GainType.Gain_3:
                        
                    case ILX5910B_GainType.Gain_10:
                        
                    case ILX5910B_GainType.Gain_30:
                       
                    case ILX5910B_GainType.Gain_100:
                        
                    case ILX5910B_GainType.Gain_300:
                        sendLine = value.ToString().Trim();
                        break;
                    default:
                        throw new Exception("Gain Type Error! Can't Be Written to the instrument!");                        
                }

                instrumentChassis.Write_Unchecked("Gain " + sendLine, this);
            }
        }

        /// <summary>
        /// Gets the Status of Current Calibration Mode
        /// </summary>
        public bool CurrentCalibrationMode
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("CAL:ITE?", this).Trim();
                return (rtnLine == "0" ? false : true);                
            }            
        }

        /// <summary>
        /// Gets the Status of Sensor Calibration Mode
        /// </summary>
        public bool SensorCalibrationMode
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("CAL:SEN?", this).Trim();
                return (rtnLine == "0" ? false : true);
            }            
        }

        /// <summary>
        /// Gets the Value of Status Condition Register
        /// </summary>
        public UInt32 StatusConditionRegister
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("COND?", this);
                UInt32 rtnData = Convert.ToUInt32(rtnLine);
                return rtnData;
            }
        }

        /// <summary>
        /// Sets the status of Display
        /// </summary>
        public bool DisplayStatus
        {
            set
            {
                string sendData = (value ? "ON" : "OFF");
                instrumentChassis.Write_Unchecked("DIS " + sendData.Trim(), this);
            }
        }

        /// <summary>
        /// Gets the content of Display
        /// </summary>
        public string DisplayContent
        {
            get
            {
                return instrumentChassis.Query_Unchecked("DIS?", this);
            }
        }

        /// <summary>
        /// Sets/Gets the Status of Display Auto
        /// </summary>
        public bool DisplayAutoStatus
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("DIS:AUTO?", this);
                return (rtnLine == "0" ? false : true);
            }
            set
            {
                instrumentChassis.Write_Unchecked("DIS:AUTO " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of DisplaySet
        /// </summary>
        public bool DisplaySetStatus
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("DIS:SET?", this);
                return (rtnLine == "0" ? false : true);
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write_Unchecked("DIS:SET", this);
                }
            }
        }

        /// <summary>
        /// Sets/Gets the mode of Display to Resistance
        /// </summary>
        public bool DisplayResistance
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("DIS:R?", this);
                return (rtnLine == "0" ? false : true);
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write_Unchecked("DIS:R", this);
                }
            }
        }

        /// <summary>
        /// Sets/Gets the mode of Display to Thermistor
        /// </summary>
        public bool DisplayThermistor
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("DIS:T?", this);
                return (rtnLine == "0" ? false : true);
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write_Unchecked("DIS:T", this);
                }
            }
        }

        /// <summary>
        /// Sets/Gets the status of Condition Enable Register
        /// </summary>
        public UInt32 ConditionEnableRegister
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("ENAB:COND?", this);
                return (Convert.ToUInt32(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("ENAB:COND " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of Event Enable Register
        /// </summary>
        public UInt32 EventEnableRegister
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("ENAB:EVE?", this);
                return (Convert.ToUInt32(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("ENAB:EVE " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of Outoff Enable Register
        /// </summary>
        public UInt32 OutoffEnableRegister
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("ENAB:OUTOFF?", this);
                return (Convert.ToUInt32(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("ENAB:OUTOFF " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets the calibration value of TEC Current
        /// Gets the actual value of TEC Current
        /// </summary>
        public double TECCurrent_Amps
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("ITE?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("ITE " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the TEC TE Current Limit value
        /// </summary>
        public double TECCurrentLimit_Amps
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("LIM:ITE?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("LIM:ITE " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets TEC High Temperature Limit in Degree
        /// HighTemp must >= LowTemp + 10Degree
        /// </summary>
        public double TECHighTemp_Degree
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("LIM:THI?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("LIM:THI " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets TEC High Temperature Limit in Degree
        /// HighTemp must >= LowTemp + 10Degree
        /// </summary>
        public double TECLowTemp_Degree
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("LIM:TLO?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("LIM:TLO " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of LOCK Switch
        /// </summary>
        public bool LOCKSwitchStatus
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("LOCK:ADJ?", this);
                return (rtnLine == "0" ? false : true);
            }
            set
            {
                instrumentChassis.Write_Unchecked("LOCK:ADJ " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the Resistance Value
        ///    SensorType    ValueRepresent   
        ///     100uA       0.001-500.0Kohms    
        ///      10uA       0.001-500.0Kohms
        ///     AD590       0.01-5000.0uA
        ///     LM335       0.1-50000.0mV
        /// </summary>
        public double Resistance
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("R?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("R " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the Temperature Value
        /// </summary>
        public double Temperature_Degree
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("T?", this);
                return (Convert.ToDouble(rtnLine));
            }
            set
            {
                instrumentChassis.Write_Unchecked("T " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Gets the Value of Resistance Set Point
        /// </summary>
        public double ResistanceSetPoint
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("SET:R?", this);
                return (Convert.ToDouble(rtnLine));
            }
        }

        /// <summary>
        /// Gets the Value of Temperature Set Point
        /// </summary>
        public double TemperatureSetPoint
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("SET:T?", this);
                return (Convert.ToDouble(rtnLine));
            }
        }

        /// <summary>
        /// Gets the total time has passed since the 5910B was last powered up.
        /// </summary>
        public double TotalTimePowerUp_Seconds
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("TIME?", this);
                string[] rtnData = rtnLine.Split(':');
                double totalTime_sec;
                switch (rtnData.Length)
                {
                    case 1:
                        totalTime_sec = Convert.ToDouble(rtnData[0]);
                        break;
                    case 2:
                        totalTime_sec = Convert.ToDouble(rtnData[0]) * 60 + Convert.ToDouble(rtnData[1]);
                        break;
                    case 3:
                        totalTime_sec = Convert.ToDouble(rtnData[0]) * 36000 + Convert.ToDouble(rtnData[1]) * 60 + Convert.ToDouble(rtnData[2]);
                        break;
                    default:
                        throw new Exception("Total Time_PowerUp Format Error!");                       
                }
                return totalTime_sec;
            }
        }

        /// <summary>
        /// Gets the total time has passed since the last TIMER query was issued.
        /// </summary>
        public double TotalTimeReQuery_Seconds
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("TIMER?", this);
                string[] rtnData = rtnLine.Split(':');
                double totalTime_sec;
                switch (rtnData.Length)
                {
                    case 1:
                        totalTime_sec = Convert.ToDouble(rtnData[0]);
                        break;
                    case 2:
                        totalTime_sec = Convert.ToDouble(rtnData[0]) * 60 + Convert.ToDouble(rtnData[1]);
                        break;
                    case 3:
                        totalTime_sec = Convert.ToDouble(rtnData[0]) * 36000 + Convert.ToDouble(rtnData[1]) * 60 + Convert.ToDouble(rtnData[2]);
                        break;
                    default:
                        throw new Exception("Total Time_ReQuery Format Error!");
                }
                return totalTime_sec;
            }
        }

        /// <summary>
        /// Sets/Gets the Inc/Dec step
        /// </summary>
        public double Step
        {
            get
            {
                string rtnLine = instrumentChassis.Query_Unchecked("STEP?", this);
                return Convert.ToDouble(rtnLine);
            }
            set
            {
                instrumentChassis.Write_Unchecked("STEP " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Decrease some parameter by one step 
        /// </summary>
        public void DecByOneStep()
        {
            instrumentChassis.Write_Unchecked("DEC", this);
        }

        /// <summary>
        /// Increase some parameter by one step
        /// </summary>
        public void IncByOneStep()
        {
            instrumentChassis.Write_Unchecked("INC", this);
        }

        /// <summary>
        /// This command is useful for creating delays which don't require a lot 
        /// of program code and don't tie up the GPIB during execution.
        /// </summary>
        /// <param name="milliseconds">milliseconds = 0.001second</param>
        public void Delay(int milliseconds)
        {
            instrumentChassis.Write_Unchecked("DELAY " + milliseconds.ToString().Trim(), this);
        }

        /// <summary>
        /// Set Current Calibration Mode
        /// </summary>
        public void SetCurrentCalibrationMode()
        {
            instrumentChassis.Write_Unchecked("CAL:ITE", this);            
        }

        /// <summary>
        /// Set Sensor Calibration Mode
        /// </summary>
        public void SetSensorCalibrationMode()
        {
            instrumentChassis.Write_Unchecked("CAL:SEN", this);
        }

        /// <summary>
        /// Read Peltier Current in Amps
        /// </summary>
        /// <returns></returns>
        public double[] readPeltierCurrent_Amps()
        {
            string rtnLine = instrumentChassis.Query_Unchecked("ITE?", this);
            string[] rtnData = rtnLine.Split(',');
            double[] peltierCurrent = new double[rtnData.Length];
            for (int i = 0; i < rtnData.Length; i++)
            {
                peltierCurrent[i] = Convert.ToDouble(rtnData[i]);
            }
            return peltierCurrent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SHConstA"></param>
        /// <param name="SHConstB"></param>
        /// <param name="SHConstC"></param>
        public void useThermistorWithTemperatureControl(double SHConstA, double SHConstB, double SHConstC)
        {
            OperatingMode = ControlMode.Temperature;
            SteinhartHartCoefficients SHConst = new SteinhartHartCoefficients(SHConstA,SHConstB,SHConstC);     
            SteinhartHartConstants = SHConst;
        }
    }
}

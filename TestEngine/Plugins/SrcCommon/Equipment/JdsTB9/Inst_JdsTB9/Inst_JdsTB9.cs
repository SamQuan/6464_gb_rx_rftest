// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_JdsTB9.cs
//
// Author: mark.fullalove, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// JDS TB9 optical filter
    /// </summary>
    public class Inst_JdsTB9 : InstType_OpticalTuneableFilter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JdsTB9(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "JDS Uniphase, TB9",				// hardware name 
                "0",  			// minimum valid firmware version 
                "9999");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            InstrumentDataRecord instrVariant2 = new InstrumentDataRecord(
                "JDS FITEL INC.,TB9",				// hardware name 
                "2.00",  			// minimum valid firmware version 
                "9999");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant2", instrVariant2);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JdsTB9",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "1.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_JdsTB9)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write_Unchecked("CLR", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // clear the status byte
                    //instrumentChassis.Write_Unchecked("CSB", null);                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JdsTB9 instrumentChassis;
        #endregion

        #region Unsupported methods
        public override double Attenuation_dB
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override void Execute_PeakSearch(double startWl_nm, double stopWl_nm)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override double OpticalPower_dBm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override double OpticalPower_mW
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        #endregion

        public override double Wavelength_nm
        {
            get
            {
                string wavelength_m = instrumentChassis.Query_Unchecked("WVL?", this);
                double wavelength_nm = Convert.ToDouble(wavelength_m) * 1e9;
                return wavelength_nm;
            }
            set
            {
                instrumentChassis.Write_Unchecked("WVL " + value.ToString() + "NM", this);
            }
        }
    }
}

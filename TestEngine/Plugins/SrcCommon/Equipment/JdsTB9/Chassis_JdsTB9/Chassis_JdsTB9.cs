// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_JdsTB9.cs
//
// Author: mark.fullalove, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// JDS TB9 optical filter
    /// </summary>
    public class Chassis_JdsTB9 : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_JdsTB9(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord chassisData1 = new ChassisDataRecord(
                "JDS Uniphase, TB9",			// hardware name 
                "0",			// minimum valid firmware version 
                "9999");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData1", chassisData1);

            ChassisDataRecord chassisData2 = new ChassisDataRecord(
                "JDS FITEL INC.,TB9",			// hardware name 
                "2.00",			// minimum valid firmware version 
                "9999");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData2", chassisData2);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // manufacturer, model, serial no, firmware
                // e.g. "JDS Uniphase, TB9, 0, 0"
                string[] idn = this.Query_Unchecked("IDN?", null).Split(',');
                return idn[3];
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // manufacturer, model, serial no, firmware
                // e.g. "JDS Uniphase, TB9, 0, 0"
                string[] idn = this.Query_Unchecked("IDN?", null).Split(',');
                return idn[0] + "," + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // nothing                   
                }
            }
        }
        #endregion
    }
}

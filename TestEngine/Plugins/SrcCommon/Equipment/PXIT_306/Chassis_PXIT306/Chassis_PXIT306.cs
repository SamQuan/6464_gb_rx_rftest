// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_PXIT_306.cs
//
// Author: Paul.Annetts, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for PXIT-306 photodiode electrical or optical input card.
    /// </summary>
    public class Chassis_PXIT306 : ChassisType_PXI
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_PXIT306(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "PXIT 306",			// hardware name 
                "0",			    // minimum valid firmware version 
                "1");               // maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);



            // set cached bias voltages - set only!
            biasCacheV_chan1 = 0.0;
            biasCacheV_chan2 = 0.0;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// PXIT Card firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // 'PXITGetCardVersion' returns a random(ish) number based (I think)
                // on the Visa session number or thread ID. Completely useless for CardVersion.
                // PXIT strikes again!
                //Int16 cardVersion = PXIT306Import.PXITGetCardVersion(VisaSessionHandle);
                Int16 cardVersion = 1;
                return cardVersion.ToString();
            }
        }

        /// <summary>
        /// PXIT Card hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                Int16 cardType = PXIT306Import.PXITGetCardType(VisaSessionHandle);
                // card model type is actually coded in Hex! (as specified in PXIT 305 user manual) 
                // i.e. PXIT 305 is returned as 0x305 (decimal 775)
                string cardTypeStr = string.Format("PXIT {0:X}", cardType);
                return cardTypeStr;
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (!value) // if setting offline                
                {
                    // Uninitialise before dropping session
                    Int16 status = PXIT306Import.PXIT306UnInitialiseModule(VisaSessionHandle);
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    
                    // basic check for valid driver installation
                    try
                    {
                        // just query the DLL versions (no card required!)
                        double d1 = PXIT306Import.PXITVersion();
                        double d2 = PXIT306Import.PXIT306Version();
                    }
                    catch (Exception e)
                    {
                        throw new ChassisException("PXIT 306 Driver DLL Installation Missing/Corrupt", e);
                    }

                    // Initialise after setting up Visa session
                    Int16 status = PXIT306Import.PXIT306InitialiseModule(VisaSessionHandle);
                }
            }
        }

        #endregion

        #region Fns for instruments
        /// <summary>
        /// Read current autorange
        /// </summary>
        /// <param name="channel">channel number</param>
        /// <returns>Current in uA</returns>
        public double ReadCurrentAutoRange_A(byte channel)
        {
            // bit 7: 0=autorange 1=manual range
            //
            // need to specify an initial range in bits 0-2;
            // 0 = 0-10 mA
            // 1 = 0-1 mA
            // 2 = 0-100 uA 
            // ... dropping down a decade each time 
            byte range1 = 0x01; // autoranging, initially in 0-1 mA range
            byte range2 = 0x01;
            double uA_1;
            double uA_2;
            Int16 status = PXIT306Import.PXIT306ReadElectricalCurrent(VisaSessionHandle, ref range1,
                ref range2, out uA_1, out uA_2);
            this.checkStatus(status);

            // convert from uA to mA
            double current_uA = 0.0;
            if (channel == 1) current_uA = uA_1;
            else if (channel == 2) current_uA = uA_2;
            else throw new ChassisException("Invalid channel: " + channel);

            double current_A = current_uA / 1e6;
            return current_A;
        }

        /// <summary>
        /// Set bias voltage
        /// </summary>
        /// <param name="channel">Channel number</param>
        /// <param name="biasV">Bias voltage</param>
        public void SetBiasCurrent_V(byte channel, double biasV)
        {
            double bias1=0.0, bias2=0.0;

            if (channel == 1)
            {
                bias1 = biasV;
                bias2 = biasCacheV_chan2;
            }
            else if (channel == 2)
            {
                bias1 = biasCacheV_chan1;
                bias2 = biasV;
            }
            else throw new ChassisException("Invalid channel: " + channel);

            Int16 status = PXIT306Import.PXIT306SetBiasVoltages(VisaSessionHandle, bias1, bias2);
            this.checkStatus(status);

            // if got here set it OK, update cache
            biasCacheV_chan1 = bias1;
            biasCacheV_chan2 = bias2;
        }

        /// <summary>
        /// Read current autorange
        /// </summary>
        /// <param name="channel">channel number</param>
        /// <returns>Current in uA</returns>
        public double GetBiasCurrentCache_V(byte channel)
        {
            if (channel == 1) return biasCacheV_chan1;
            else if (channel == 2) return biasCacheV_chan2;
            else throw new ChassisException("Invalid channel: " + channel);
        }

        #endregion

        #region Private data and helpers
        private double biasCacheV_chan1;
        private double biasCacheV_chan2;

        /// <summary>
        /// Check return error status
        /// </summary>
        /// <param name="status">Status value</param>
        private void checkStatus(Int16 status)
        {
            if (status != 0)
            {
                throw new ChassisException("Invalid PXIT-306 status: " + status);
            }
        }
        #endregion
    }
}

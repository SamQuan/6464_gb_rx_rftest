// Copyright @ Oclaro
//
// Bookham Optical Delay Line driver
// Bookham.TestSolution.Instruments
//
// Instr_OZODL650.cs
//
// Author: wendy.wen, 2011
// Design: [ODL-650-MC Operating Instructions_revD.pdf]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Optical Delay Line Driver
    /// </summary>
    public class Instr_OZODL650 : InstType_ODL
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_OZODL650(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrODL650 = new InstrumentDataRecord(
                "ODL-650",				// hardware name 
                "0",  			// minimum valid firmware version 
                "4.10");			// maximum valid firmware version 
            ValidHardwareData.Add("instrODL650", instrODL650);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_OZODL650",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.1");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_OZODL650", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_OZODL650)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to home position
        /// </summary>
        public override void SetDefaultState()
        {
            string command = "FH";
            //this.instrumentChassis.WriteLine(command, this);
            string resp = this.instrumentChassis.Query(command, this);
            System.Threading.Thread.Sleep(1000);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_OZODL650 instrumentChassis;
        #endregion

        /// <summary>
        /// To re-starts and re-initializes the unit
        /// </summary>
        public override void Reset()
        {
            string command = "RESET";
            //this.instrumentChassis.WriteLine(command, this);
            string respond = this.instrumentChassis.Query(command, this);
            System.Threading.Thread.Sleep(100);
        }

        /// <summary>
        /// To query the current status on the unit
        /// </summary>
        public override InstType_ODL.OperatingStatuses Status
        {
            get
            {
                string command = "Q?";
                string res = this.instrumentChassis.Query(command, this);
                if (res.ToLower().Contains(OperatingStatuses.Idle.ToString()))
                {
                    return OperatingStatuses.Idle;
                }
                else if (res.ToLower().Contains(OperatingStatuses.Busy.ToString()))
                {
                    return OperatingStatuses.Busy;
                }
                else if (res.ToLower().Contains(OperatingStatuses.Home.ToString()))
                {
                    return OperatingStatuses.Home;
                }
                else if (res.ToLower().Contains(OperatingStatuses.End.ToString()))
                {
                    return OperatingStatuses.End;
                }
                else
                {
                    throw new InstrumentException("Invalid Operating Status returned");
                }

            }
        }

        /// <summary>
        /// To Set or Return the absolute minimum time delay(ps)
        /// </summary>
        public override double DelayMin_ps
        {
            get
            {
                string command = "M?";
                string res = this.instrumentChassis.Query(command, this);
                if (res.ToLower().Contains("unknown"))
                {
                    System.Threading.Thread.Sleep(100);
                    this.SetDefaultState();
                    res = this.instrumentChassis.Query(command, this);
                }
                string delayStr = res.ToLower().Replace("min time:", "").Replace("done", "").Replace("ps", "");
                double rtn = Convert.ToDouble(delayStr.Trim());
                return rtn;
            }
            set
            {
                string command = "M" + value.ToString();
                this.instrumentChassis.WriteLine(command, this);
                System.Threading.Thread.Sleep(100);
            }
        }

        /// <summary>
        /// To set or get the motor an absolute number of delay 
        /// from the home position
        /// </summary>
        public override double Delay_ps
        {
            get
            {
                string command = "T?";
                string res = this.instrumentChassis.Query(command, this);
                if (res.ToLower().Contains("unknown"))
                {
                    System.Threading.Thread.Sleep(100);
                    this.SetDefaultState();
                    res = this.instrumentChassis.Query(command, this);
                }
                string delayStr = res.ToLower().Replace("time:", "").Replace("done", "").Replace("ps", "");
                double rtn = Convert.ToDouble(delayStr.Trim());
                return rtn;
            }
            set
            {
                string command = "T" + Convert.ToString(Math.Round(value, 2));
                //this.instrumentChassis.WriteLine(command, this);
                this.instrumentChassis.Query(command, this);
                System.Threading.Thread.Sleep(100);
            }
        }

        /// <summary>
        /// To set the motor an absolute number of setps 
        /// from the home position
        /// </summary>
        public override int Step
        {
            get
            {
                string command = "S?";
                string res = this.instrumentChassis.Query(command, this);
                if (res.ToLower().Contains("unknown"))
                {
                    System.Threading.Thread.Sleep(100);
                    this.SetDefaultState();
                    res = this.instrumentChassis.Query(command, this);
                }
                string delayStr = res.ToLower().Replace("step:", "").Replace("done", "").Replace("ps", "");
                int rtn = Convert.ToInt32(delayStr.Trim());
                return rtn;
            }
            set
            {
                string command = "S" + value.ToString();
                //this.instrumentChassis.WriteLine(command, this);
                string resp = this.instrumentChassis.Query(command, this);
                System.Threading.Thread.Sleep(100);
            }
        }

    }
}

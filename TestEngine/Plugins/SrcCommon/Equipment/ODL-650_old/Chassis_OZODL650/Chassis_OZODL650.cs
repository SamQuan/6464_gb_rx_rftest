// Copyright @ Oclaro
//
// Bookham Optical Delay Line driver
// Bookham.TestSolution.Chassis
//
// Chassis_OZODL650.cs
//
// Author: wendy.wen, 2011
// Design: [ODL-650-MC Operating Instructions_revD.pdf]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// Optical Delay Line Driver Chassis
    /// </summary>
    public class Chassis_OZODL650 : ChassisType_Serial
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_OZODL650(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisODL650 = new ChassisDataRecord(
                "ODL-650",			// hardware name 
                "0",			// minimum valid firmware version 
                "4.10");		// maximum valid firmware version 
            ValidHardwareData.Add("chassisODL650", chassisODL650);

            string[] resByComma = resourceString.Split(',');

            if (resByComma[0].Contains("COM"))
            {
                if ((resByComma.Length < 1) && (resByComma.Length > 2))
                    throw new ChassisException(
                        "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");

                // Setup RS232 variables
                this.BaudRate = Convert.ToInt32((resByComma.Length == 2) ? resByComma[1] : "9600");  // 9600;

                
                this.DataBits = 8;
                this.StopBits = System.IO.Ports.StopBits.One;
                this.Handshaking = System.IO.Ports.Handshake.None;
                this.Parity = System.IO.Ports.Parity.None;
                this.InputBufferSize_bytes = 1024;
                this.OutputBufferSize_bytes = 1024;
                this.Timeout_ms = 50000;
                this.NewLine = "\r\n";

            }
            else
                throw new ChassisException(
                    "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string command = "V1";
                this.WriteLine(command, null);
                string respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null);
                }

                respond = respond.Substring(0,respond.Length-4);
                string firmware = respond.Split(' ')[1].Split('_')[1];
                return firmware;
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string command = "E0";
                string respond = "";
                this.WriteLine(command, null);
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null);
                }

                command = "V1";
                this.WriteLine(command, null);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null);
                }

                respond = respond.Substring(0, respond.Length - 4);
                string hardware = respond.Split(' ')[0];
                return hardware;
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    this.Configure(BaudRate, DataBits, StopBits,
                        Parity, Handshaking, InputBufferSize_bytes, OutputBufferSize_bytes);
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                                     
                }
            }
        }
        #endregion
    }
}

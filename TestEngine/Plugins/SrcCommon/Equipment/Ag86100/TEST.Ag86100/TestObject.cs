using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS.Ag86100;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;

namespace TEST.Ag86100
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_Ag86100 testChassis;
        private Inst_Ag86100_Channel testInstr;
        private Chassis_BkhmMMS bertChassis;
        private Inst_BkhmMMSBert bertInstr;
        private Inst_BkhmMMSXfpHost xfpHostInstr;

        string dcaVisaResource = "GPIB1::7::INSTR";
        string bertVisaResource = "COM11";
        string scopeChannel = "1";
        string bertSlot = "3";
        string xfpHostSlot = "4";

        private IInstType_RfClockSource clkSrc;
        private IInstType_BertPatternGen pattGen;
        #endregion
        
        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_Ag86100("DCA_Chassis", "Chassis_Ag86100", dcaVisaResource);
            TestOutput(testChassis, "Created OK");
            testChassis.EnableLogging = true;

            bertChassis = new Chassis_BkhmMMS("BERT_Chassis", "Chassis_BkhmMMS", bertVisaResource);
            TestOutput(bertChassis, "Created OK");
            bertChassis.EnableLogging = true;

            // create instrument objects            
            testInstr = new Inst_Ag86100_Channel("DCA_Instr",
                "Inst_Ag86100_Channel", scopeChannel, "", testChassis);            
            TestOutput(testInstr, "Created OK");
            testInstr.EnableLogging = true;

            bertInstr = new Inst_BkhmMMSBert("BERT_Instr",
                "Inst_BkhmMMSBert", bertSlot, "", bertChassis);
            TestOutput(bertInstr, "Created OK");
            bertInstr.EnableLogging = true;

            xfpHostInstr = new Inst_BkhmMMSXfpHost("XFP_Host",
               "Inst_BkhmMMSXfpHost", xfpHostSlot, "", bertChassis);
            TestOutput(xfpHostInstr, "Created OK");
            xfpHostInstr.EnableLogging = true;

            // put them online
            testChassis.IsOnline = true;
            TestOutput(testChassis, "IsOnline set true OK");
            bertChassis.IsOnline = true;
            TestOutput(bertChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            TestOutput(testInstr, "IsOnline set true OK");
            bertInstr.IsOnline = true;
            TestOutput(bertInstr, "IsOnline set true OK");
            xfpHostInstr.IsOnline = true;
            TestOutput(xfpHostInstr, "IsOnline set true OK");

            xfpHostInstr.PowerEnabled = true;
            // get the generic BERT stuff
            clkSrc = bertInstr;
            pattGen = bertInstr;
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(testChassis, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(testChassis, "HW: " + testChassis.HardwareIdentity);
            TestOutput(testChassis, "FW: " + testChassis.FirmwareVersion);
            TestOutput(testInstr, "Driver: " + testInstr.DriverName + ":" + testInstr.DriverVersion);
            TestOutput(testInstr, "HW: " + testInstr.HardwareIdentity);
            TestOutput(testInstr, "FW: " + testInstr.FirmwareVersion);            
        }

        [Test]
        public void T02_Setup()
        {
            TestOutput("\n\n*** T02_Setup ***");
            // setup XFP
            xfpHostInstr.XfpEnabled = true;
            TestOutput(xfpHostInstr, "XFP enabled...");
            // setup BERT
            clkSrc.Frequency_MHz = 9953.0;
            clkSrc.Enabled = true;
            pattGen.PatternType = InstType_BertDataPatternType.Prbs;
            pattGen.PrbsLength = 31;
            pattGen.PattGenEnabled = true;
            TestOutput(bertInstr, "Setup of BERT done...");

            // setup DCA
            testChassis.SetDefaultState();
            testInstr.SetDefaultState();
            testChassis.ClearAll();
            testChassis.SetLabel(0, "This is a user label on row 0");
            testChassis.SetLabel(1, "This is a user label on row 1");
            testChassis.ClearLabels();
            testChassis.SetLabel(0, "DCA Test Engine Driver Test Harness");
            testChassis.AutoScale();

            // change DCA mode
            TestOutput(testChassis, "DCA Mode: " + testChassis.DCAMode);
            testChassis.DCAMode = DcaMode.EyeMask;
            Assert.AreEqual(testChassis.DCAMode, DcaMode.EyeMask);
            testChassis.DCAMode = DcaMode.Oscilloscope;
            Assert.AreEqual(testChassis.DCAMode, DcaMode.Oscilloscope);
            testChassis.DCAMode = DcaMode.EyeMask;
            Assert.AreEqual(testChassis.DCAMode, DcaMode.EyeMask);

            // Change colour mode
            TestOutput(testChassis, "Colour Mode: " + testChassis.EyeColourMode);
            testChassis.EyeColourMode = DcaEyeColourMode.ColourGrade;
            Assert.AreEqual(DcaEyeColourMode.ColourGrade, testChassis.EyeColourMode);
            testChassis.EyeColourMode = DcaEyeColourMode.Greyscale;
            Assert.AreEqual(DcaEyeColourMode.Greyscale, testChassis.EyeColourMode);
            testChassis.EyeColourMode = DcaEyeColourMode.ColourGrade;
            Assert.AreEqual(DcaEyeColourMode.ColourGrade, testChassis.EyeColourMode);

            // get/set time base
            TestOutput(testChassis, "Timebase(s): " + testChassis.TimebaseRange_s);
            testChassis.TimebaseRange_s = testChassis.TimebaseRange_s * 2;
            TestOutput(testChassis, "Timebase(s): " + testChassis.TimebaseRange_s);   
         

        }

        [Test]
        public void T03_Trigger()
        {
            TestOutput("\n\n*** T03_Trigger ***");
            TestOutput(testChassis, "Trigger Mode: " + testChassis.TriggerSource);
            testChassis.TriggerSource = DcaTriggerSrc.FreeRun;
            Assert.AreEqual(testChassis.TriggerSource, DcaTriggerSrc.FreeRun);
            testChassis.TriggerSource = DcaTriggerSrc.FrontPanel;
            Assert.AreEqual(testChassis.TriggerSource, DcaTriggerSrc.FrontPanel);
            testChassis.TriggerSource = DcaTriggerSrc.LeftModule;
            Assert.AreEqual(testChassis.TriggerSource, DcaTriggerSrc.LeftModule);
            //testChassis.TriggerSource = DcaTriggerSrc.RightModule;
            //Assert.AreEqual(testChassis.TriggerSource, DcaTriggerSrc.RightModule);
            testChassis.TriggerSource = DcaTriggerSrc.FrontPanel;            
        }

        [Test]
        public void T04_Cal()
        {
            TestOutput("\n\n*** T04_Cal ***");            
            testChassis.DCAMode = DcaMode.Oscilloscope;
            bool modCalReqd = testInstr.CalModuleRequired;
            TestOutput(testInstr, "ModuleCalReqd? " + modCalReqd);
            if (modCalReqd)
            {
                // Module calibration
                xfpHostInstr.XfpEnabled = false;
                TestOutput("Calibrating module...");
                testInstr.CalibrateModule();
            }                       
        }

        [Test]
        public void T05_WaveLengthAndFilters()
        {
            TestOutput("\n\n*** T05_WaveLengthAndFilters ***");            
            // do filter setup
            FilterDescription[] fdes = testInstr.AvailFilters;
            TestOutput(testInstr, "Available filters... " );
            foreach (FilterDescription f in fdes)
            {
                TestOutput(testInstr, "Filter: " + f);
            }
            TestOutput(testInstr, "Current Filter: " + testInstr.CurrentFilter);

            FilterDescription setFilter;
            setFilter.DesignRate_Mbps = 9953.0;
            setFilter.FilterOrder = 4;
            testInstr.CurrentFilter = setFilter;

            TestOutput(testInstr, "Current Filter: " + testInstr.CurrentFilter);

            TestOutput(testInstr, "FilterOn? " + testInstr.FilterOn);
            testInstr.FilterOn = false;
            TestOutput(testInstr, "FilterOn? " + testInstr.FilterOn);
            testInstr.FilterOn = true;
            TestOutput(testInstr, "FilterOn? " + testInstr.FilterOn);  
          
            // do wavelength setup
            TestOutput(testInstr, "Opt Wl? " + testInstr.OpticalWavelength);
            testInstr.OpticalWavelength = DcaWavelength.W1310nm;
            Assert.AreEqual(DcaWavelength.W1310nm, testInstr.OpticalWavelength);
            testInstr.OpticalWavelength = DcaWavelength.W1550nm;
            Assert.AreEqual(DcaWavelength.W1550nm, testInstr.OpticalWavelength);
            //testInstr.OpticalWavelength = DcaWavelength.W850nm;
            //Assert.AreEqual(DcaWavelength.W850nm, testInstr.OpticalWavelength);
            //testInstr.OpticalWavelength = DcaWavelength.User;
            //Assert.AreEqual(DcaWavelength.User, testInstr.OpticalWavelength);

        }

        [Test]
        public void T06_EyeMaskSetup()
        {
            TestOutput("\n\n*** T06_EyeMaskSetup ***");
            xfpHostInstr.XfpEnabled = true;
            testChassis.SetDefaultState();
            testInstr.SetDefaultState();            
            testChassis.DCAMode = DcaMode.EyeMask;
            testInstr.OpticalWavelength = DcaWavelength.W1550nm;
            testChassis.SetLabel(0, "DCA Test Engine Driver Test Harness");
            
            TestOutput(testChassis, "Nbr scan points: " + testChassis.NbrScanPoints);
            testChassis.NbrScanPoints = 1350;
            Assert.AreEqual(1350, testChassis.NbrScanPoints);
        }

        [Test]
        public void T07_StartStopAcquisition()
        {
            TestOutput("\n\n*** T07_StartStopAcquisition ***");
            TestOutput("Starting acquisition");
            testChassis.StartAcquisition(1000);
            System.Threading.Thread.Sleep(100);
            TestOutput("Aborting acquisition");
            testChassis.AbortAcquisition();
            TestOutput("Restarting acquisition");
            testChassis.StartAcquisition(20);
            TestOutput("Wait for acquisition to complete");
            testChassis.WaitForAcquisitionComplete();
            TestOutput("Continuing acquisition");
            testChassis.ContinueAcquisition(40);
            TestOutput("Wait for acquisition to complete");
            testChassis.WaitForAcquisitionComplete();
        }

        [Test]
        public void T08_UnfilteredMeas()
        {
            TestOutput("\n\n*** T08_UnfilteredMeas ***");
            testInstr.FilterOn = false;
            TestOutput("Initial Scale: "+ testInstr.VerticalScale);
            TestOutput("Initial Offest: " + testInstr.VerticalOffset);

            testInstr.VerticalScale = 2e-5;
            testInstr.VerticalOffset = 1e-5;

            TestOutput("Modified Scale: " + testInstr.VerticalScale);
            TestOutput("Modified Offest: " + testInstr.VerticalOffset);

            Assert.AreEqual(testInstr.VerticalScale, 2e-5);
            Assert.AreEqual(testInstr.VerticalOffset, 1e-5);


            testChassis.AutoScale();
            TestOutput("Calibrating ER");
            xfpHostInstr.XfpEnabled = false;
            testInstr.CalibrateER();
            xfpHostInstr.XfpEnabled = true;

            TestOutput("New Scale: " + testInstr.VerticalScale);
            TestOutput("New Offest: " + testInstr.VerticalOffset);

            TestOutput("Taking scans...");
            testChassis.StartAcquisition(100);
            testChassis.WaitForAcquisitionComplete();
            TestOutput("Measurement thres: " + testChassis.MeasurementThresholds);
            testChassis.MeasurementThresholds = new DcaMeasThresholds(20.0, 50.0, 80.0);
            Assert.AreEqual(new DcaMeasThresholds(20.0, 50.0, 80.0), testChassis.MeasurementThresholds);

            TestOutput("ExtinctionRatio_dB: " + testChassis.ExtinctionRatio_dB);
            TestOutput("AveragePower_dBm: " + testChassis.AveragePower_dBm); 
            TestOutput("JitterPkPk_ps: " + testChassis.JitterPkPk_ps);
            TestOutput("Risetime_ps: " + testChassis.Risetime_ps);
            TestOutput("Falltime_ps: " + testChassis.Falltime_ps);
            
        }

        [Test]
        public void T09_FilteredMeas()
        {
            TestOutput("\n\n*** T09_FilteredMeas ***");
            testInstr.CurrentFilter = new FilterDescription(9953, 4);
            testInstr.FilterOn = true;
            testChassis.AutoScale();

            TestOutput("Calibrating ER");
            xfpHostInstr.XfpEnabled = false;
            testInstr.CalibrateER();
            xfpHostInstr.XfpEnabled = true;

            TestOutput("Taking scans...");
            testChassis.StartAcquisition(100);
            testChassis.WaitForAcquisitionComplete();
            TestOutput("Measurement thres: " + testChassis.MeasurementThresholds);
            testChassis.MeasurementThresholds = new DcaMeasThresholds(20.0, 50.0, 80.0);
            Assert.AreEqual(new DcaMeasThresholds(20.0, 50.0, 80.0), testChassis.MeasurementThresholds);

            TestOutput("ExtinctionRatio_dB: " + testChassis.ExtinctionRatio_dB);
            TestOutput("AveragePower_dBm: " + testChassis.AveragePower_dBm); 
            TestOutput("EyeSignal2Noise_dB: " + testChassis.EyeSignal2Noise_dB);
            TestOutput("JitterRms_ps: " + testChassis.JitterRms_ps);
            TestOutput("CrossingPoint_Pc: " + testChassis.CrossingPoint_Pc);
        }

        [Test]
        public void T10_MaskTest()
        {
            TestOutput("\n\n*** T10_MaskTest ***");
            //clear things up
            TestOutput(testChassis, "Mask mode: " + testChassis.MaskTestOn);
            TestOutput(testChassis, "Mask name: " + testChassis.MaskName);            
            testChassis.MaskTestOn = false;
            testChassis.ClearMask();
            Assert.AreEqual(false, testChassis.MaskTestOn);
            Assert.IsNull(testChassis.MaskName);

            //init mask test STM256 mask (should be bad!)...
            testChassis.LoadMask("STM256_OC768.msk");
            TestOutput(testChassis, "Mask mode: " + testChassis.MaskTestOn);
            testChassis.MaskTestOn = true;
            Assert.AreEqual(true, testChassis.MaskTestOn);
            displayMaskResults();

            //init mask test 9.993 mask (should be good)...
            testChassis.LoadMask("10GbE_9_953_May02.msk");
            TestOutput(testChassis, "Mask mode: " + testChassis.MaskTestOn);
            testChassis.MaskTestOn = true;
            Assert.AreEqual(true, testChassis.MaskTestOn);
            displayMaskResults();
            
            testChassis.MaskMargin_Pc = +60;
            testChassis.MaskMarginOn = true;
            displayMaskResults();
        }

        private void displayMaskResults()
        {
            TestOutput("***MASK TEST RESULTS***");
            TestOutput(testChassis, "Mask name: " + testChassis.MaskName);
            bool marginOn = testChassis.MaskMarginOn;
            TestOutput(testChassis, "Margins on? " + marginOn);
            if (marginOn)
            {
                TestOutput(testChassis, "Margin (pc): " + testChassis.MaskMargin_Pc);
            }
            TestOutput(testChassis, "Total hits: " + testChassis.MaskHitsTotal);
            if (marginOn)
            {
                TestOutput(testChassis, "Hits in margins: " + testChassis.MaskHitsInMargin);
            }
            TestOutput(testChassis, "Hits in mask: " + testChassis.MaskHitsInMask);
            TestOutput(testChassis, "Hits in Region1: " + testChassis.MaskHitsInRegion(1));
            TestOutput(testChassis, "Hits in Region2: " + testChassis.MaskHitsInRegion(2));
            TestOutput(testChassis, "Hits in Region3: " + testChassis.MaskHitsInRegion(3));            
        }

        [Test]
        [ExpectedException("Ag86100OperationFailed")] 
        public void T11_BadMask1()
        {
            TestOutput("\n\n*** T11_BadMask1 ***");
            //clear things up            
            testChassis.MaskTestOn = false;
            testChassis.ClearMask();
            TestOutput(testChassis, "Mask hits: " + testChassis.MaskHitsTotal);
        }

        [Test]
        [ExpectedException("Ag86100OperationFailed")]
        public void T12_BadMask2()
        {
            TestOutput("\n\n*** T12_BadMask2 ***");
            //clear things up            
            testChassis.MaskTestOn = false;
            testChassis.ClearMask();
            TestOutput(testChassis, "Mask hits: " + testChassis.MaskHitsInRegion(1));
        }


        [Test]
        public void T13_ScreenDump()
        {
            TestOutput("\n\n*** T13_ScreenDump ***");
            testChassis.LoadMask("10GbE_9_953_May02.msk");
            testChassis.MaskMargin_Pc = 60;
            testChassis.MaskMarginOn = true;
            testChassis.MaskTestOn = true;
            foreach (DcaImageDownloadType type in Enum.GetValues(typeof(DcaImageDownloadType)))
            {
                foreach (DcaImageInversion inv in Enum.GetValues(typeof(DcaImageInversion)))
                {
                    foreach (DcaImageType format in Enum.GetValues(typeof(DcaImageType)))
                    {
                        string fileName = String.Format("test{0}-{1}.{2}",
                            type, inv, format);
                        TestOutput(testChassis, "Saving: " + fileName);
                        testChassis.SaveScreenToPcDisk(fileName, format,
                            type, inv);

                    }
                }
            }           
        }

        #region Private helper fns        
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(Instrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(Chassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

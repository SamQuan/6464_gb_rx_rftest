// [Copyright]
//
// Bookham Test Library
// Bookham.TestLibrary.Chassis
//
// Ag86100Exception.cs
//
// Author: paul.annetts, 2006
// Design: Agilent 86100 DCA Instrument Driver Detailed Design

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS.Ag86100
{
    /// <summary>
    /// Agilent DCA exception
    /// </summary>
    public class Ag86100Exception : Exception
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public Ag86100Exception(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerEx">Inner Exception that was caught</param>
        public Ag86100Exception(string message, Exception innerEx)
            :base(message, innerEx)
        {
        }
    }

    /// <summary>
    /// Agilent DCA failed to perform operation (e.g. autoscale, calibration)
    /// </summary>
    public class Ag86100OperationFailed: Ag86100Exception
    {
         /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public Ag86100OperationFailed(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerEx">Inner Exception that was caught</param>
        public Ag86100OperationFailed(string message, Exception innerEx)
            : base(message, innerEx)
        {
        }
    }

    /// <summary>
    /// Agilent DCA failed to perform requested measurement
    /// </summary>
    public class Ag86100MeasException : Ag86100Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public Ag86100MeasException(string message)
            : base(message)
        {
        }
    }
}

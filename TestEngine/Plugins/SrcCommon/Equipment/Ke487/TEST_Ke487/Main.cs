// Test Harness which is buildable on top of NUnit framework and Test Engine.

// Logging is performed by the Test Engine standard logger. 
// When bad things (i.e. exceptions) happen in normal execution, you can find a log in the 
// errorlog.txt file in the LogStore directory below where the main EXE lives.
// So, when debugging, it will probably be in ./bin/Debug/LogStore/errorlog.txt relative to this file!

// Recommend that users install TestDriven.NET 2.0 which gives 
// Test running capability from within C# Express.

// This allows you to run test code in the IDE, including in the debugger. 
// Just right-click on the [TestFixture] or [Test] tag to bring up the context menu.

// Also, TestDriven allows you to run unit-tests in the NUnit-GUI (installed as part of Test Driven) 
// as a right-click on the C# Project (Test With).

// Syntax ([TestFixture][Test] etc...) here in this template is from NUnit.

// This project is also a standalone console application, so you don't need TestDriven.NET installed to run it!
// By default output (BugOut calls) goes to console, but this can be configured.

// Links: 
//      TestDriven.NET: http://www.testdriven.net/Default.aspx?tabid=27 
//      NUnit(help for attributes): http://www.nunit.org

// Template by Paul Annetts 2006. 
// Tested in C# Express with TestDriven.NET 2.0 beta v 1438 (which includes NUnit 2.2.6).

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.ChassisNS;

namespace TEST_Ke487
{
    class test
    {
       public static void  Main()
       {
            string VisaResource = "GPIB0::2::INSTR";
            Chassis_Ke487 testChassis;
            Inst_Ke487 testInst;
            testChassis = new Chassis_Ke487("TestChassis", "Chassis_Ke487", VisaResource);
            testInst = new Inst_Ke487("TestInst", "Inst_Ke487", "1", "", testChassis);
            testChassis.IsOnline = true;
            testInst.IsOnline = true;
            testInst.SetDefaultState();
            System.Threading.Thread.Sleep(1000);
            testInst.VoltageSetPoint_Volt =10;
            System.Threading.Thread.Sleep(5000);
            double i=testInst.CurrentActual_amp;
        }
        
        }
    }


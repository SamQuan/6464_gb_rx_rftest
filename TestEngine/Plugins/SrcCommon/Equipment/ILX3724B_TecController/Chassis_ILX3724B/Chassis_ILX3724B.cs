// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Ke2510.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	#region Public Functions
	
	/// <summary>
	/// Summary description for Template chassis driver.
	/// </summary>
	public class Chassis_ILX3724B : ChassisType_Visa
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
		public Chassis_ILX3724B(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information
			
			// Add details of valid chassis(s)
            ChassisDataRecord ILX3724BData = new ChassisDataRecord(
                "ILX Lightwave 3724B",		// hardware name 
                "0",										// minimum valid firmware version 
                "5.0");										// maximum valid firmware version 
            ValidHardwareData.Add("ILX3724B", ILX3724BData);
		}


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
				
				// Return the firmware version in the 4th comma seperated field
				return idn[3].Substring(0,3);
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string and thsplit the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
		
				// Return field1, the manufacturer name and field 2, the model number
				return idn[0] + " " + idn[1];
			}
		}


		#endregion
	}
	
}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ke2510.cs
//
// Author: T Foster & Joseph Olajubu
// Design: 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Instrument driver for ILX3724B TEC controller
	/// </summary>
	public class Inst_ILX3724B_TecController : InstType_TecController
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_ILX3724B_TecController(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            InstrumentDataRecord ILX3724B_TecControllerData = new InstrumentDataRecord(
                "ILX Lightwave 3724B",
                "0",
                "5.0");

            ValidHardwareData.Add("ILX3724B_TecController", ILX3724B_TecControllerData);

            InstrumentDataRecord ILX3724BChassisData = new InstrumentDataRecord(
                "Chassis_ILX3724B",
                "0",
                "1.0.0.0");
            ValidChassisDrivers.Add("Chassis_ILX3724B", ILX3724BChassisData);



            instrumentChassis = (ChassisType_Visa)base.InstrumentChassis;
        }



		#region Public Instrument Methods and Properties


		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
				string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');

				// Build the string from 2 substrings, the manufacturer name & the model number
				string hid = idn[0] + " " + idn[1];

				// Log event 
				LogEvent("'HardwareIdentity' returned : " + hid);

				return hid;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
				string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');				

				// The 4th substring containins the firmware version.
				// Version is the first 3 characters
				string fv = idn[3].Substring(0,3);

				// Log event 
				LogEvent("'FirmwareVersion' returned : " + fv);

				// Return 
				return fv;
			}
		}

		/// <summary>
		/// Configures the instrument into a default state.
		/// 
		/// </summary>

        public override void SetDefaultState()
        {
            this.Write("*RST");
        }

		#endregion

		#region Public TecController InstrumentType Methods and Properties

		/// <summary>
		/// Sets/returns the output state
		/// </summary>
        public override bool OutputEnabled
        {
            get
            {
                string commandString = "TEC:OUT?";

                string rtn = this.Query(commandString);

                if (rtn.Equals("0"))
                {
                    return false;
                }
                if (rtn.Equals("1"))
                {
                    return true;
                }
                throw new Exception("Error status!");
            }
            set
            {
                string commandString = "TEC:OUT ";

                if (value)
                {
                    commandString += "1";
                }
                else
                {
                    commandString += "0";
                }

                this.Write(commandString);
            }
        }


		/// <summary>
		/// Sets/returns the control mode. Instrument driver should throw 
		/// if a particular mode is not supported
		/// </summary>
        public override InstType_TecController.ControlMode OperatingMode
        {
            get
            {
                string commandString = "TEC:MODE?";

                string rtn = this.Query(commandString);

                if (rtn.Equals("T"))
                {
                    return ControlMode.Temperature;
                }
                if (rtn.Equals("R"))
                {
                    return ControlMode.Resistance;
                }
                if (rtn.Equals("ITE"))
                {
                    return ControlMode.Current;
                }

                throw new Exception("Error status!");
            }
            set
            {
                string commandString = "TEC:MODE:";

                if (value.Equals(InstType_TecController.ControlMode.Current))
                {
                    commandString += "ITE";
                }
                if (value.Equals(InstType_TecController.ControlMode.Resistance))
                {
                    commandString += "R";
                }
                if (value.Equals(InstType_TecController.ControlMode.Temperature))
                {
                    commandString += "T";
                }
                if (value.Equals(InstType_TecController.ControlMode.Voltage))
                {
                    throw new Exception("The voltage mode is not usable!");
                }
                this.Write(commandString);
            }
        }
        /// <summary>
        /// Sets/returns the Proportional Gain Constant.
        /// </summary>
        public override double ProportionalGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/returns the Derivative Gain Constant.
        /// </summary>
        public override double DerivativeGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/returns the Integral Gain Constant.
        /// </summary>
        public override double IntegralGain
        {
            get
            {
                string commandString = "TEC:GAIN?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:GAIN " + value.ToString();

                this.Write(commandString);
            }
        }

		/// <summary>
		/// Sets/returns the sensor type
		/// </summary>
        public override InstType_TecController.SensorType Sensor_Type
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Sets/returns the Thermistor Sensor Steinhart-Hart Equation coefficients. 
        /// </summary>
        public override SteinhartHartCoefficients SteinhartHartConstants
        {
            get
            {
                string commandString = "TEC:CONST?";

                string[] rtn = this.Query(commandString).Split(',');

                SteinhartHartCoefficients coefficients = new SteinhartHartCoefficients();

                coefficients.A = double.Parse(rtn[0]);
                coefficients.B = double.Parse(rtn[1]);
                coefficients.C = double.Parse(rtn[2]);

                return coefficients;
            }
            set
            {
                string commandString = "TEC:CONST " + value.A.ToString() + ',' + value.B.ToString() + ',' + value.C.ToString();

                this.Write(commandString);
            }
        }


        /// <summary>
        /// Sets/returns the RTD Sensor Callendar-Van Dusen Equation coefficients. 
        /// </summary>
        public override CallendarVanDusenCoefficients CallendarVanDusenConstants
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        

        /// <summary>
        /// Sets/returns the temperature set point.
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public override double SensorTemperatureSetPoint_C
        {
            get
            {
                string commandString = "TEC:SET:T?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:T " + value.ToString();

                this.Write(commandString);
            }
        }

        /// <summary>
        /// Returns the actual temperature.
        /// </summary>
        public override double SensorTemperatureActual_C
        {
            get
            {
                string commandString = "TEC:T?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
        }

        /// <summary>
        /// Set/returns the sensor resistance  set point
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public override double SensorResistanceSetPoint_ohm
        {
            get
            {
                string commandString = "TEC:SET:R?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:R " + value.ToString();

                this.Write(commandString);
            }
        }

        /// <summary>
        /// Returns the actual sensor resistance
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public override double SensorResistanceActual_ohm
        {
            get
            {
                string commandString = "TEC:R?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
        }


        /// <summary>
        /// Set/returns the peltier current set point
        /// </summary>
        public override double TecCurrentSetPoint_amp
        {
            get
            {
                string commandString = "TEC:SET:ITE?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:ITE " + value.ToString();

                this.Write(commandString);
            }
        }

        /// <summary>
        /// Returns the actual TEC (peltier) current.
        /// </summary>
        public override double TecCurrentActual_amp
        {
            get
            {
                string commandString = "TEC:ITE?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }

        }


        /// <summary>
        /// Return the actual TEC voltage
        /// </summary>
        public override double TecVoltageActual_volt
        {
            get
            {
                string commandString = "TEC:V?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }

        }

        /// <summary>
        /// Set/returns the TEC voltage set point
        /// </summary>
        public override double TecVoltageSetPoint_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                string commandString = "TEC:V " + value.ToString();
                this.Write(commandString);
            }
        }


		/// <summary>
		/// Set/returns the sensor current.  
		/// </summary>
        public override double SensorCurrent_amp
        {
            get
            {
                string commandString = "TEC:SET:ITE?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:ITE " + value.ToString();

                this.Write(commandString);
            }
        }

		/// <summary>
		/// Returns the TEC DC resistance
		/// </summary>
        public override double TecResistanceDC_ohm
        {
            get
            {
                string commandString = "TEC:R?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
        }

		/// <summary>
		/// Returns the TEC AC resistance
		/// </summary>
        public override double TecResistanceAC_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

		/// <summary>
		/// Set/returns the TEC compliance current
		/// </summary>
        public override double TecCurrentCompliance_amp
        {
            get
            {
                string commandString = "TEC:LIM:ITE?";

                string rtn = this.Query(commandString);

                return double.Parse(rtn);
            }
            set
            {
                string commandString = "TEC:LIM:ITE " + value.ToString();

                this.Write(commandString);
            }
        }

		/// <summary>
		/// Set/returns the TEC compliance voltage
		/// </summary>
        public override double TecVoltageCompliance_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

		#endregion

        #region private function
        private void Write(string cmd)
        {
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        private string Query(string cmd)
        {
            return (instrumentChassis.Query_Unchecked(cmd, this));
        }

        #endregion

		#region Private Data

        private ChassisType_Visa instrumentChassis;
		#endregion
	}
}

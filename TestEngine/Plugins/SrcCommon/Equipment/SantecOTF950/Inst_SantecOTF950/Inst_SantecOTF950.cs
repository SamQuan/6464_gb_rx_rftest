// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SantecOTF950.cs
//
// Author: wendy.wen, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestSolution.ChassisNS;
using System.Windows.Forms;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_SantecOTF950 : InstType_OpticalTuneableFilter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SantecOTF950(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord santecOtf950 = new InstrumentDataRecord(
                 "SantecOTF950",				// hardware name 
                 "0.0.0.0",  			// minimum valid firmware version 
                 "2.0.0.0");			// maximum valid firmware version 
            ValidHardwareData.Add("SantecOTF950", santecOtf950);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SantecOTF950",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("SantecOTF950", chassisInfo);

            //// Configure valid chassis driver information
            //// TODO: Update with useful key name, real chassis name and versions
            //InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
            //    "MyChassis",								// chassis driver name  
            //    "0.0.0.0",									// minimum valid chassis driver version  
            //    "2.0.0.0");									// maximum valid chassis driver version
            //ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_SantecOTF950)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //this.Reset();   // Takes too long!
            SetWavelength_nm(1550.0);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    string val = this.instrumentChassis.Query_Unchecked(":WAV?", null);     
                }
            }
        }
        #endregion


        #region property parameter area
        

        /// <summary>
        /// Gets and sets operable wavelength in nm.
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                double currentWavelength = double.Parse(this.instrumentChassis.Query_Unchecked(":WAV?", this));
                return currentWavelength * 1E9;


            }
            set
            {
                SetWavelength_nm(value);
            }
        }

        /// <summary>
        /// get the Wavelength max value
        /// </summary>
        public double WavelengthMax
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked(":WAV? MAX", this));
            }

        }

        /// <summary>
        /// get the WavelengthMin
        /// </summary>
        public double WavelengthMin
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked(":WAV? MIN", this));
            }

        }

        /// <summary>
        /// get the Wavelength state
        /// </summary>
        public double WavelengthState
        {
            get
            {
                return double.Parse(this.instrumentChassis.Query_Unchecked(":WAV:STAT?", this));
            }

        }

        /// <summary>
        /// get and set the frequency
        /// </summary>
        public double Frequency_Ghz
        {
            get 
            {
                double currentFreq= double.Parse(this.instrumentChassis.Query_Unchecked(":FREQ?", this));
                return currentFreq * 1E-9;
            }
            set
            {
                SetFreq_Ghz(value);
            }
        }


        /// <summary>
        /// get and set the bandwidth
        /// </summary>
        public double BandWidth_nm
        {
            get
            {
                double currBand= double.Parse(this.instrumentChassis.Query_Unchecked(":BAND?", this));
                return currBand * 1E9;
            }
            set
            {
                SetBand_nm(value);
            }
        }

        #endregion

        #region Santec 950 function

        #endregion


        #region Santec 930 functions




        /// <summary>
        /// Reset instrument.
        /// </summary>
        public void Reset()
        {
            this.instrumentChassis.Write_Unchecked("*RST", this);
            System.Threading.Thread.Sleep(20000);
            //waitForSlidersToSettle(40000, 500);
        }

        /// <summary>
        /// Set Wavelength
        /// </summary>
        /// <param name="Wl_nm"> Target wavelength in nm</param>
        /// 
        private void SetWavelength_nm(double Wl_nm)
        {

            

            double diff_nm = 9999;

            //int maxTime = 20;

            //int count=0;

            double actual_nm = 0;

            //bool retryFlag = false;
            
            diff_nm = CalcWAVDiff(Wl_nm,out actual_nm);


            if (diff_nm > 0.1)
            {
                

                throw new InstrumentException("Set Wavelength_nm failed. Expected " + Wl_nm + " nm. Got " + actual_nm + " nm.");
                
            }
        }

        private void SetFreq_Ghz(double freq_Ghz)
        {
            this.instrumentChassis.Write_Unchecked(":FREQ " + freq_Ghz +"GHz", this);
            /* Wait up to 40 seconds for sliders to settle. */
            //this.waitForSlidersToSettle(40 * 1000, 100);
            System.Threading.Thread.Sleep(1000);
            /* Read back wavelength to see if settled wavelength is as expected. */
            double actual_ghz = Frequency_Ghz;
            double diff_nm = Math.Abs(actual_ghz - freq_Ghz);
            if (diff_nm > 10)
            {
                throw new InstrumentException("Set frequency failed. Expected " + freq_Ghz + " ghz. Got " + actual_ghz + " ghz.");
            }
        }

        private void SetBand_nm(double band_nm)
        {
            this.instrumentChassis.Write_Unchecked(":BAND " + band_nm + "nm", this);
            /* Wait up to 40 seconds for sliders to settle. */
            //this.waitForSlidersToSettle(40 * 1000, 100);
            System.Threading.Thread.Sleep(1000);
            /* Read back wavelength to see if settled wavelength is as expected. */
            double actual_nm = BandWidth_nm;
            double diff_nm = Math.Abs(actual_nm - band_nm);
            if (diff_nm > 10)
            {
                throw new InstrumentException("Set bandwidth failed. Expected " + band_nm + " nm. Got " + actual_nm + " nm.");
            }
        }

        #endregion


        private double CalcWAVDiff(double Wl_nm,out double actual_nm)
        {
            //Reset();
            //System.Threading.Thread.Sleep(4000);

            this.instrumentChassis.Write_Unchecked(":WAV " + Wl_nm + "nm", this);

            bool operationFlag = true;
            int iter = 0;
            do
            { 
                System.Threading.Thread.Sleep(500);
                string result=this.instrumentChassis.Query_Unchecked(":WAV:STAT?", this);
                
                if (result.Trim()=="0")
                {
                    operationFlag = false;
                }
                iter++;
                if (iter > 40)
                {
                    throw new InstrumentException("Set wavelength operation didn't complete within 20 seconds!");
                }
               

                
                
            } while (operationFlag);

            
            actual_nm = Wavelength_nm;

            double diff_nm = Math.Abs(actual_nm - Wl_nm);

            return diff_nm;
        }


        #region IInstrType_OpticalTuneableFilter

        public override void Execute_PeakSearch(double wl_start, double wl_stop)
        {
            throw new InstrumentException("this function(Peak search) not implement!");
            
        }

        public override double OpticalPower_dBm
        {
            get
            {
                throw new InstrumentException("this function(Optical power) not implement!");
 
            }
        }

        public override double OpticalPower_mW
        {
            get
            {
                throw new InstrumentException("this function(Optical power) not implement!");

            }
 
        }

        public override double Attenuation_dB
        {
            get
            {
                throw new InstrumentException("this function(attenuation) not implement!");

            }
            set
            {
                throw new InstrumentException("this function(attenuation) not implement!");
            }
        }

        #endregion


        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_SantecOTF950 instrumentChassis;
        #endregion
    }
}

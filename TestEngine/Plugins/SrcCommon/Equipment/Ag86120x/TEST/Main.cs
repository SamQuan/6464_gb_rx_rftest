using System;

namespace EquipmentTest_Inst_Ag86120x
{
	public class TestWrapper
	{
		public static int Main(string[] args)
		{
			// Init tester 
			Inst_Ag86120x_Test test = new Inst_Ag86120x_Test();

			// Run tests
			test.Setup();

			// Create chassis
			test.T01_CreateInst("GPIB0::11::INSTR");

			test.T02_TryCommsNotOnline();

			// set online
			test.T03_SetOnline();

			test.T04_DriverVersion();

			test.T05_FirmwareVersion();
			
			test.T06_HardwareID();

			test.T07_SetDefaultState();

            test.T08_Power_mW();
            
            test.T09_Power_dBm();
            
            test.T10_MeasurementMedium();
            
            test.T11_Frequency();
            
            test.T12_Wavelength();

            test.T13_TimingComparision(true);

            test.T13_TimingComparision(false);

            // Shutdown
			test.ShutDown();

			// End
			return 0;
		}
	}
}

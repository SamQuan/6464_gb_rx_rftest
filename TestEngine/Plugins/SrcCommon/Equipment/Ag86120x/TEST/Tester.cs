using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;

using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

//using NationalInstruments.VisaNS;

namespace EquipmentTest_Inst_Ag86120x
{
	/// <exclude />	
	[TestFixture]
	public class Inst_Ag86120x_Test 
	{		
		/// <exclude />
		/// <summary>
		/// Constructor 
		/// </summary>
        public Inst_Ag86120x_Test()
		{}

		/// <exclude />
		[TestFixtureSetUp]
		public void Setup()
		{
			// initialise logging domain
			Bookham.TestEngine.Framework.Logging.Initializer.Init();
			Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);			
		}

		/// <exclude />
		[TestFixtureTearDown]
		public void ShutDown()
		{
			testChassis.IsOnline = false;

			// Test end
			Debug.WriteLine("Test Finished!");
		}

		[Test]
		public void T01_CreateInst(string visaResource)
		{
			testChassis = new Chassis_Ag86120x("TestChassis", "Chassis_Ag86120x", visaResource);
			Debug.WriteLine("Chassis created OK");
			testInst = new Inst_Ag86120x("TestInst", "Inst_Ag86120x","1","", testChassis);
			Debug.WriteLine("Instrument created OK");
            
		}

		/// <exclude />
		[Test]
		[ExpectedException(typeof(System.Exception))]
		public void T02_TryCommsNotOnline()
		{
			 //Cannot try this as logging shuts down the app after the exception
			try
			{
				Debug.WriteLine(testInst.HardwareIdentity);
			}
			catch(ChassisException e)
			{
				Debug.WriteLine("Expected exception :" + e.Message);
			}
		}

		[Test]
		public void T03_SetOnline()
		{
			testChassis.IsOnline = true;
			Debug.WriteLine("Chassis IsOnline set true OK");
			testInst.IsOnline = true;
			Debug.WriteLine("Instrument IsOnline set true OK");
		}

		[Test]
		public void T04_DriverVersion()
		{
			Debug.WriteLine(testInst.DriverVersion);
		}

		[Test]
		public void T05_FirmwareVersion()
		{
			Debug.WriteLine(testInst.FirmwareVersion);
		}

		[Test]
		public void T06_HardwareID()
		{
			Debug.WriteLine(testInst.HardwareIdentity);
		}

		[Test]
		public void T07_SetDefaultState()
		{
			testInst.SetDefaultState();
			Debug.WriteLine("Default state set OK");
		}

		[Test]
		public void T08_Power_mW()
		{
			Debug.WriteLine("Power in Mw = " + testInst.Power_mW);
		}

        [Test]
        public void T09_Power_dBm()
        {
            Debug.WriteLine("Power in dBm = " + testInst.Power_dBm);
        }

        [Test]
        public void T10_MeasurementMedium()
        {
            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Air;
            Debug.WriteLine("Measurement Medium set to Air, reading back = " + testInst.MeasurementMedium);
            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Vacuum;
            Debug.WriteLine("Measurement Medium set to Vacuum, reading back = " + testInst.MeasurementMedium);
        }

        [Test]
        public void T11_Frequency()
        {
            Debug.WriteLine("Measurement frequency =" + testInst.Frequency_GHz + " GHz");
            double f = testInst.Frequency_GHz;
            f = testInst.Frequency_GHz;
            f = testInst.Frequency_GHz;
        }

        [Test]
        public void T12_Wavelength()
        {
            Debug.WriteLine("Wavelength =" + testInst.Wavelength_nm + " nm");
        }


        [Test]

        public void T13_TimingComparision(bool Safemode)
        {

            int i;



            System.DateTime StartTime;

            System.DateTime EndTime;

            System.TimeSpan Duration;



            StartTime = System.DateTime.Now;

            double reading;



            Debug.WriteLine("Start Time: " + StartTime);

            int maxloops = 10;


            testInst.SafeModeOperation = Safemode;

            testInst.MeasurementMedium = InstType_Wavemeter.Medium.Air;

            testInst.SetDefaultState();
		
            for (i = 0; i < maxloops; i++)
            {                
                Debug.Write(i.ToString() + " ");
                reading= testInst.Power_dBm;
                reading= testInst.Wavelength_nm;
            }

            Debug.WriteLine("End!");

            EndTime = System.DateTime.Now;

            Debug.WriteLine("End Time: " + EndTime);

            Duration = EndTime - StartTime;

            Debug.WriteLine("SafeMode = " + Safemode + "\n " +

                "Did " + maxloops + " sets of operations in " + Duration.TotalSeconds + " seconds");



        }


		/// <summary>
		/// Chassis & Inst references
		/// </summary>
		private	Chassis_Ag86120x testChassis;
		private	Inst_Ag86120x testInst;

	}

}

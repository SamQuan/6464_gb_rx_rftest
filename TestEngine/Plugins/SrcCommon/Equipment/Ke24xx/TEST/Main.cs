using System;

namespace EquipmentTest_Inst_Ke24xx
{
	public class TestWrapper
	{
		public static int Main(string[] args)
		{
			// Init tester 
			Inst_Ke24xx_Test test = new Inst_Ke24xx_Test();
           
			// Run tests
			test.Setup();

			// Create chassis
            test.T01_CreateInst("GPIB0::1::INSTR", "GPIB0::1::INSTR");
           

			test.T02_TryCommsNotOnline();
            

			// set online
			test.T03_SetOnline();
            


			test.T04_DriverVersion();

			test.T05_FirmwareVersion();
			
			test.T06_HardwareID();

			test.T07_SetDefaultState();
           

			test.T08_EnableOutput();

			test.T09_Current_amp();

			test.T10_Voltage_volt();

            test.T11_FourWireSense();

            test.T12_SweepTest();

            test.T13_TriggerTest();

            test.T14_TriggerTest2();

            test.T14_TriggerTest3();

			// Shutdown
			test.ShutDown();

			// End
			return 0;
		}
	}
}

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;


namespace EquipmentTest_Inst_Ke24xx
{
	/// <exclude />	
	[TestFixture]
	public class Inst_Ke24xx_Test 
	{		
		/// <exclude />
		/// <summary>
		/// Constructor 
		/// </summary>
		public Inst_Ke24xx_Test()
		{}

		/// <exclude />
		[TestFixtureSetUp]
		public void Setup()
		{
			// initialise logging domain
			Bookham.TestEngine.Framework.Logging.Initializer.Init();
			Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);			
		}

		/// <exclude />
		[TestFixtureTearDown]
		public void ShutDown()
		{
			testInst.OutputEnabled = false;
            testInst2.OutputEnabled = false;
			testChassis.IsOnline = false;

			// Test end
			Debug.WriteLine("Test Finished!");
		}

		[Test]
        public void T01_CreateInst(string visaResource1, string visaResource2)
		{
			testChassis = new  Chassis_Ke24xx("TestChassis", "Chassis_Ke24xx", visaResource1);
            testChassis2 = new Chassis_Ke24xx("TestChassis2", "Chassis_Ke24xx", visaResource2);
			Debug.WriteLine("Chassis created OK");
            testInst = new Inst_Ke24xx("TestInst", "Inst_Ke24xx", "1", "", testChassis);
            testInst2 = new Inst_Ke24xx("TestInst2", "Inst_Ke24xx", "1", "", testChassis2);
            Debug.WriteLine("Instrument created OK");
			
		}

      
		/// <exclude />
		[Test]
		[ExpectedException(typeof(System.Exception))]
		public void T02_TryCommsNotOnline()
		{
			 //Cannot try this as logging shuts down the app after the exception
			try
			{
				Debug.WriteLine(testInst.HardwareIdentity);
			}
			catch(ChassisException e)
			{
				Debug.WriteLine("Expected exception :" + e.Message);
			}
		}

		[Test]
		public void T03_SetOnline()
		{
            testChassis.IsOnline = true;
			testChassis2.IsOnline = true;
			Debug.WriteLine("Chassis IsOnline set true OK");
			testInst.IsOnline = true;
            testInst2.IsOnline = true;
			Debug.WriteLine("Instrument IsOnline set true OK");
		}

		[Test]
		public void T04_DriverVersion()
		{
			Debug.WriteLine(testInst.DriverVersion);
		}

		[Test]
		public void T05_FirmwareVersion()
		{
			Debug.WriteLine(testInst.FirmwareVersion);
		}

		[Test]
		public void T06_HardwareID()
		{
			Debug.WriteLine(testInst.HardwareIdentity);
		}

		[Test]
		public void T07_SetDefaultState()
		{
			testInst.SetDefaultState();
			Debug.WriteLine("Default state set OK");
			testInst.HardwareData["4wireSense"] = "false";
			testInst.SetDefaultState();
			Debug.WriteLine("Default state set OK with 4 wire sense");
            testInst2.SetDefaultState();
            Debug.WriteLine("Default state set OK");
            testInst2.HardwareData["4wireSense"] = "false";
            testInst2.SetDefaultState();
            Debug.WriteLine("Default state set OK with 4 wire sense");
		}

		[Test]
		public void T08_EnableOutput()
		{
           
            testInst.CurrentComplianceSetPoint_Amp = 0.002;
            testInst.VoltageSetPoint_Volt = 5.0;
            testInst.OutputEnabled = true;
            double current = testInst.CurrentActual_amp;




			Debug.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
			testInst.OutputEnabled = true;
			Debug.WriteLine("Output state set to 'true' OK");
			Debug.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
		}

		[Test]
		public void T09_Current_amp()
		{
			testInst.CurrentComplianceSetPoint_Amp = 0.01;
            Debug.WriteLine("Compl_Current_amp : " + testInst.CurrentComplianceSetPoint_Amp);
            Debug.WriteLine("Current_amp : " + testInst.CurrentActual_amp);
            testInst.CurrentSetPoint_amp = 0.010;
			Debug.WriteLine("Current_amp set to 0.010");
            Debug.WriteLine("Current_amp : " + testInst.CurrentActual_amp);
            testInst.CurrentSetPoint_amp = 0.0;
		}

		[Test]
		public void T10_Voltage_volt()
		{
			testInst.VoltageComplianceSetPoint_Volt = 0.5;
            Debug.WriteLine("Compl_Voltage_volt : " + testInst.VoltageComplianceSetPoint_Volt);
			Debug.WriteLine("Voltage_volt : " + testInst.VoltageActual_Volt);
			testInst.VoltageSetPoint_Volt = 0.5;
			Debug.WriteLine("Voltage_volt set to 0.5");
			Debug.WriteLine("Voltage_volt : " + testInst.VoltageActual_Volt);
            testInst.VoltageSetPoint_Volt = 0.0;
		}

        [Test]
        public void T11_FourWireSense()
        {
            testInst.FourWireSense = true;
            if (testInst.FourWireSense)
            {
                Debug.WriteLine("4 wire sense is correctly showing as being on");
            }
            testInst.FourWireSense = false;
            if (!testInst.FourWireSense)
            {
                Debug.WriteLine("4 wire sense is correctly showing as being off");
            }
        }

        [Test]
        public void T12_SweepTest()
        {
            //setup
            testInst.OutputEnabled = false;

            testInst.UseFrontTerminals = false;
            if (!testInst.UseFrontTerminals)
            {
                Debug.WriteLine("Correctly showing as not using front terminals");
            }
            testInst.UseFrontTerminals = true;
            if (testInst.UseFrontTerminals)
            {
                Debug.WriteLine("Correctly showing as using front terminals");
            }

            //initialise inst
            testInst.SenseCurrent(0.13, 0.12);
            testInst.VoltageComplianceSetPoint_Volt = 10;
            testInst.VoltageSetPoint_Volt = 8;
            testInst.ClearSweepTriggering();


            //setup and do a voltage sweep using single meter

            testInst.ClearSweepTriggering();
            testInst.ClearSweepTraceData();
            testInst.InitSourceVMeasureI_VoltageSweep(8, 10, 200, 1, 2);
            testInst.OutputEnabled = true;
            testChassis.Timeout_ms = 8000;
            testInst.StartSweep();
            testInst.WaitForOperationToComplete();

            //Inst_Ke24xx.SweepData myData = testInst.GetSweepDataSet();
            //Assert.AreEqual(Math.Round(myData.voltage_V[0], 2), 8.0);
            //Assert.AreEqual(Math.Round(myData.voltage_V[1], 2), 10.0);
            //int dummy = 0;
        }
        
        [Test]
        public void T13_TriggerTest()
        {

            //setup one meter to do a voltage sweep 

            testInst.ClearSweepTriggering();
            testInst.ClearSweepTraceData();
            testInst.InitSourceVMeasureI_VoltageSweep(8, 10, 20, 2, 1);
            testInst.OutputEnabled = true;
            testChassis.Timeout_ms = 8000;
          

            //setup another to do a voltage sweep

            testInst2.ClearSweepTriggering();
            testInst2.ClearSweepTraceData();
            testInst2.InitSourceVMeasureI_VoltageSweep(8, 10, 20, 2, 1);
            
            testInst2.OutputEnabled = true;
            testChassis2.Timeout_ms = 8000;
            testInst.StartSweep();
            testInst2.StartSweep();
            testInst2.WaitForOperationToComplete();
            //Inst_Ke24xx.SweepData myData2 = testInst2.GetSweepDataSet();

            //Inst_Ke24xx.SweepData myData =   testInst.GetSweepDataSet();
        }
        
        [Test]
        public void T14_TriggerTest2()
        {
             //setup one meter to do a current sweep 
            testInst.OutputEnabled = false;
            testInst2.OutputEnabled = false;
            testInst.ClearSweepTriggering();
            testInst.ClearSweepTraceData();
            testInst.InitSourceIMeasureV_CurrentSweep(0.1, 0.8, 20, 2, 1);
            testInst.OutputEnabled = true;
            testChassis.Timeout_ms = 8000;
          

            //setup another to do a current sweep

            testInst2.ClearSweepTriggering();
            testInst2.ClearSweepTraceData();
            testInst2.InitSourceIMeasureV_CurrentSweep(0.1, 0.8, 20, 2, 1);
            
            testInst2.OutputEnabled = true;
            testChassis2.Timeout_ms = 8000;
            testInst.StartSweep();
            testInst2.StartSweep();
            testInst2.WaitForOperationToComplete();
            //Inst_Ke24xx.SweepData myData2 = testInst2.GetSweepDataSet();
            //Inst_Ke24xx.SweepData myData = testInst.GetSweepDataSet();
        }

        [Test]
        public void T14_TriggerTest3()
        {
            testInst2.SourceSweptCurrent(0.1, 0.15, 20);
            testInst2.IntegrationRate_SenseCurrent= 0.2;
            testInst2.SetIsenseCompliance(0.5);
            testInst2.SetSourceDelayMeasureAuto();
            testInst2.SetSourceDelayMeasureManual(0.1);
            testInst2.SourceFixedCurrent();
            testInst2.SourceFixedVoltage();
            testInst2.SourceSweptCurrent(0.2, 0.4, 20);
            testInst.SourceSweptVoltage(0.1, 2, 20);
            testInst2.StartSweep();
            testInst.StartSweep();

        }
            //testInst2.SourceSweptCurrent(0.1, 0.15, 20);
            //testInst2.SenseVoltage(10, 12);
            //testInst2.SenseVoltageRange(22);
            //testInst2.SetCurrent(0.5);
            //testInst2.TriggerAmplitudeAmps(0.6);
            //testInst2.TriggerAmplitudeVolts(12);
            //testInst2.InitSourceVMeasI_VoltageSweep(10, 12, true, 2, 2, 1, true);

            ////testInst.SetMeasurementAccuracy(true,2,3,4,true);
            //testInst.SetAveraging(44);
            //testInst.SetIntegrationRate(5);
            //testInst.SetAutoZero(true);
            //testInst.AutoZeroRefresh();
            //testInst.SetSourceDelayMeasure(true,3);
            //testInst.InitSourceIMeasV_CurrentSweep(0.5,1.2,10,1,2);
            //testInst.InitSourceIMeasV_TriggeredFixedCurrent
            //testInst.InitSourceIMeasV_UntriggeredSpotMeas
            //testInst.InitSourceVMeasI_MeasurementAccuracy
            //testInst.SenseCurrent
            //testInst.SetIsenseCompliance
            //testInst.GetIsenseRange
            //testInst.InitSourceVMeasI_VoltageSweep
            //testInst.ConfigureVoltageSweepReadings
            //testInst.SourceSweptVoltage
            //testInst.InitSourceVMeasI_TriggeredFixedVoltage
            //testInst.InitSourceVMeasI_UntriggeredSpotMeas
            //testInst.ConfigureTriggering
            //testInst.ConfigureTriggerlines
            //testInst.DisableTriggering
            //testInst.AbortSweep
            //testInst.CleanUpSweep
            //testInst.ClearSweepTriggering
            //testInst.ClearSweepTraceData
            //testInst.GoSweepGo
            //testInst.WaitForSweepToFinish
            //testInst.GetMeasurementStatus
            //testInst.SpotMeasureCurrent
            //testInst.ParseBinaryData

       // }

		/// <summary>
		/// Chassis & Inst references
		/// </summary>
		private	Chassis_Ke24xx testChassis;
        private Chassis_Ke24xx testChassis2;
		private	Inst_Ke24xx testInst;
        private Inst_Ke24xx testInst2;

	}

}

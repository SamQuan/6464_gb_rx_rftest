// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Ke24xx.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	#region Public Functions
	
	/// <summary>
	/// Ke24xx Chassis Driver
	/// </summary>
	public class Chassis_Ke24xx : ChassisType_Visa488_2
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
		public Chassis_Ke24xx(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information
			
			// Add details of Ke2400 chassis
			ChassisDataRecord ke2400_2Data = new ChassisDataRecord(
				"KEITHLEY INSTRUMENTS INC. MODEL 2400",		// hardware name 
                "C00",										// minimum valid firmware version 
				"C30");										// maximum valid firmware version 
            ValidHardwareData.Add("ke2400_2Data", ke2400_2Data);

            ChassisDataRecord ke2400_3Data = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440",		// hardware name 
                "C00",										// minimum valid firmware version 
                "C30");										// maximum valid firmware version 
            ValidHardwareData.Add("ke2400_3Data", ke2400_3Data);

            ChassisDataRecord ke2400_4Data = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400",		// hardware name 
                "C00",										// minimum valid firmware version 
                "C33");										// maximum valid firmware version 
            ValidHardwareData.Add("ke2400_4Data", ke2400_4Data);
		}


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');
    
				// Return the firmware version in the 4th comma seperated field
				// The first 3 characters are the firmware version

                if (idn.Length == 4)
                    
                    return idn[3].Substring(0, 3);
                else
                {    
                    string[] mystrings = idn[2].Split(' ');
                    
                    return mystrings[1].Trim();
                }
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string and split the comma seperated fields
				string[] idn = Query_Unchecked("*IDN?", null).Split(',');		
		
				// Return field1, the manufacturer name and field 2, the model number
				return idn[0] + " " + idn[1];
			}
		}

        /// <summary>
        /// Override IsOnline to provide error check settings
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // needed for error detection routines
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                    this.StandardEventRegisterMask = 60;
                }
            }
        }
		#endregion
	}
	
}

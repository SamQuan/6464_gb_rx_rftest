// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Inst_Ke24xx.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;
using System.Collections.Generic;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Instrument driver for Keithley 24xx SMU
	/// Provides basic source functions only for 'ElectricalSource' instrument type
	/// </summary>
	public class Inst_Ke24xx : InstType_TriggeredElectricalSource
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
		public Inst_Ke24xx(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information

			// Add Ke2400 details
			InstrumentDataRecord ke2400Data = new InstrumentDataRecord(	
				"KEITHLEY INSTRUMENTS INC. MODEL 2400",			// hardware name 
				"C00",											// minimum valid firmware version 
				"C30");											// maximum valid firmware version 
			ke2400Data.Add("MaxCurrentLimitAmp", "1.05");		// maximum current limit
			ke2400Data.Add("MaxVoltageLimit", "210.0");			// maximum voltage limit
			ke2400Data.Add("MaxVoltageLimitLV", "21.0");		// maximum voltage limit if LV 
			ke2400Data.Add("IsLVUnit", "true");					// Indicates this is an LV (low voltage) unit
																// Set this parameter false for standard units
			ke2400Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
			ke2400Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
			ke2400Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
			ke2400Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
			ValidHardwareData.Add("Ke2400", ke2400Data);

			// Add Ke2410 details
			InstrumentDataRecord ke2410Data = new InstrumentDataRecord(	
				"KEITHLEY INSTRUMENTS INC. MODEL 2410",			// hardware name 
				"C00",											// minimum valid firmware version 
				"C22");											// maximum valid firmware version 
			ke2410Data.Add("MaxCurrentLimitAmp", "1.05");		// maximum current limit
			ke2410Data.Add("MaxVoltageLimit", "1100.0");		// maximum voltage limit
			ke2410Data.Add("MaxVoltageLimitLV", "1100.0");		// LV mode not supported
			ke2410Data.Add("IsLVUnit", "false");				
			ke2410Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
			ke2410Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
			ke2410Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
			ke2410Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
			ValidHardwareData.Add("Ke2410", ke2410Data);

			// Add Ke2420 details
			InstrumentDataRecord ke2420Data = new InstrumentDataRecord(	
				"KEITHLEY INSTRUMENTS INC. MODEL 2420",			// hardware name 
				"C00",											// minimum valid firmware version 
				"C22");											// maximum valid firmware version 
			ke2420Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
			ke2420Data.Add("MaxVoltageLimit", "63.0");			// maximum voltage limit
			ke2420Data.Add("MaxVoltageLimitLV", "63.0");		// LV mode not supported
			ke2420Data.Add("IsLVUnit", "false");				
			ke2420Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
			ke2420Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
			ke2420Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
			ke2420Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
			ValidHardwareData.Add("Ke2420", ke2420Data);

			// Add Ke2430 details
			InstrumentDataRecord ke2430Data = new InstrumentDataRecord(	
				"KEITHLEY INSTRUMENTS INC. MODEL 2430",			// hardware name 
				"C00",											// minimum valid firmware version 
				"C22");											// maximum valid firmware version 
			ke2430Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
			ke2430Data.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
			ke2430Data.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
			ke2430Data.Add("IsLVUnit", "false");				
			ke2430Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
			ke2430Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
			ke2430Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
			ke2430Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
			ValidHardwareData.Add("Ke2430", ke2430Data);

            // Add Ke2440 details
            InstrumentDataRecord ke2440Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C30");											// maximum valid firmware version 
            ke2440Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
            ke2440Data.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
            ke2440Data.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
            ke2440Data.Add("IsLVUnit", "false");
            ke2440Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2440Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2440Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2440Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("ke2440Data", ke2440Data);

            // Add Ke2440 details
            InstrumentDataRecord ke2400_2Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C33");											// maximum valid firmware version 
            ke2400_2Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
            ke2400_2Data.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
            ke2400_2Data.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
            ke2400_2Data.Add("IsLVUnit", "false");
            ke2400_2Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2400_2Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2400_2Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2400_2Data.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("ke2400_2Data", ke2400_2Data);

			// Configure valid chassis information
			// Add 24xx chassis details
			InstrumentDataRecord ke24xxChassisData = new InstrumentDataRecord(	
				"Chassis_Ke24xx",								// chassis driver name  
				"0",											// minimum valid chassis driver version  
				"1.0.0.0");										// maximum valid chassis driver version
			ValidChassisDrivers.Add("Chassis_Ke24xx", ke24xxChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (Chassis_Ke24xx) base.InstrumentChassis;
            // Set to a known state
            //instrumentChassis.Clear();

            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(4);
            for (int ii = 1; ii <= 4; ii++)
            {
                digiOutLines.Add(new Inst_Ke24xx_DigiOutLine(instrumentName + "_D" + ii, instrumentChassis, ii));
            }
        }


		#region Public Instrument Methods and Properties


		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
                string hid = instrumentChassis.HardwareIdentity;
                // Log event 
                LogEvent("'HardwareIdentity' returned : " + hid);

                return hid;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
                string fv = instrumentChassis.FirmwareVersion;
				// Log event 
				LogEvent("'FirmwareVersion' returned : " + fv);
				// Return 
				return fv;
                
			}
		}

        private void Beep(int tone_Hz, int duration_ms)
        {
            if ( tone_Hz != 0 )
                instrumentChassis.Write(":SYST:BEEP:IMM " + tone_Hz + "," + (Single)(duration_ms/100), this);
            System.Threading.Thread.Sleep(duration_ms);
        }

		/// <summary>
		/// Configures the instrument into a default state.
		/// 
		/// </summary>
		public override void SetDefaultState()
		{
            // Clear any pending operations
            instrumentChassis.Clear();

			// Set factory default
			instrumentChassis.Write("*RST", this);

			// Set line frequency
			instrumentChassis.Write(":SYST:LFR " + HardwareData["LineFrequency_Hz"], this);

			// Set line output off state
			instrumentChassis.Write(":OUTP:SMOD " + HardwareData["OutputOffState"].ToUpper(), this);

			// Set line output terminals (front or rear)
            instrumentChassis.Write(":ROUT:TERM " + HardwareData["TerminalsFrontRear"].ToUpper(), this);
            
			// Set 4 wire sense
			if (HardwareData["4wireSense"].ToLower() == "true")
			{
				instrumentChassis.Write(":SYST:RSEN 1", this);
			}

            // Beeper off
            instrumentChassis.Write(":SYST:BEEP:STAT OFF", this);
		}

		#endregion

		#region Public ElectricalSource InstrumentType Methods and Properties

		/// <summary>
		/// Sets/returns the output state
		/// </summary>
		public override bool OutputEnabled
		{
			get 
			{
				// Query the output state
				string rtn = instrumentChassis.Query(":OUTP:STAT?", this);

				// Return bool value
				return (rtn == trueStr);
			}
			set 
			{
				// Convert bool value to string
				string boolVal = value ? trueStr : falseStr;

				// Set output
				instrumentChassis.Write(":OUTP:STAT " + boolVal, this);
			}
		}

		/// <summary>
        ///  Sets/returns the current level set point
		/// </summary>
		public override double CurrentSetPoint_amp
		{
			get 
			{				
				// Make sure we are in current mode
				string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
				if (mode.ToLower() != "curr")
				{
                    throw new InstrumentException("Attempting to read output current set point when not in Current Mode.");
				}

                // Get current source set level in amps
                //string rtn = instrumentChassis.Query(":SENS:CURR:LEV?", this); // Incorrect - MF
                string rtn = instrumentChassis.Query(":SOUR:CURR:LEV:AMPL?", this);


                // Convert and return
                return Convert.ToDouble(rtn);
			}
			
			set
			{				
				// Make sure we are in current mode
				string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
				if (mode.ToLower() != "curr")
				{
					// Is output on?
					bool outputEnabled = OutputEnabled;

					// Set function to current. this disabled the output if it is on
					instrumentChassis.Write(":SOUR:FUNC CURR", this);

					// set output back on if needed
					if (outputEnabled) OutputEnabled = true;
				}

				// Set current source level in amps
                //instrumentChassis.Write(":SOUR:CURR:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:CURR:LEV:IMM:AMPL " + value.ToString().Trim(), this);    // IMMEDIATE
            }
		}

        /// <summary>
        /// Returns the actual measured current level 
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                //instrumentChassis.Write_Unchecked(":SYST:AZER:STAT ON", this);
                //instrumentChassis.Write_Unchecked(":FORM ASC", this);

                // Read current in amps
                string[] meas = instrumentChassis.Query(":MEAS:CURR?", this).Split(',');
                // If only a single element is returned then use it, otherwire CURR will be the second. 
                int index = meas.Length == 1 ? 0 : 1;
                double rtn = Convert.ToDouble(meas[index]);
                
                //if ( ! m_bAutoZero )
                    //instrumentChassis.Write_Unchecked(":SYST:AZER:STAT OFF",this);

                // Return
                return rtn;
            }
        }

		/// <summary>
        /// Sets/returns the compliance current setpoint
		/// Compliance current being the maximum positive or negative current the instrument will supply
		/// </summary>
        public override double CurrentComplianceSetPoint_Amp
		{
			get 
			{
				// Read compliance current in amps
				string rtn = instrumentChassis.Query(":SENS:CURR:PROT:LEV?", this);				
				
				// Convert and return
				return Convert.ToDouble(rtn);								
			}
			
			set
			{
				// Check value with compliance
				double compCurr = Convert.ToDouble(HardwareData["MaxCurrentLimitAmp"]);
				if (Math.Abs(value) > compCurr)
				{
					throw new InstrumentException("Set value level '" + value.ToString() +
						"' is greater than maximum current '" + compCurr.ToString() + "'");
				}

				// Set current in amps
				instrumentChassis.Write(":SENS:CURR:PROT:LEV " + value.ToString().Trim(), this);
				instrumentChassis.Write(":SOUR:CURR:RANG " + value.ToString().Trim(), this);
			}
		}

		/// <summary>
        /// Sets/returns the voltage setpoint level
		/// </summary>
		public override double VoltageSetPoint_Volt
		{
			get 
			{
                // Make sure we are in voltage mode
                string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
                if (mode.ToLower() != "volt")
                {
                    throw new InstrumentException("Attempting to read output voltage set point when not in Voltage Mode.");
                }

                // Get voltage source set level in volts
                //string rtn = instrumentChassis.Query(":SENS:VOLT:LEV?", this); // This is incorrect - MF
                string rtn = instrumentChassis.Query(":SOUR:VOLT:LEV:AMPL?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
			}
			
			set
			{
				// Make sure we are in voltage mode
				string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
				if (mode.ToLower() != "volt")
				{
					// Is output on?
					bool outputEnabled = OutputEnabled;

					// Set function to volt. this disabled the output if it is on
					instrumentChassis.Write(":SOUR:FUNC VOLT", this);

					// set output back on if needed
					if (outputEnabled) OutputEnabled = true;
				}

				// Set voltage level
                //instrumentChassis.Write(":SOUR:VOLT:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:VOLT:LEV:IMM:AMPL " + value.ToString().Trim(), this); // apply immediately
            }
		}


        /// <summary>
        ///  Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Read voltage in volts
                string[] meas = instrumentChassis.Query(":MEAS:VOLT?", this).Split(',');

                // Get the 1st value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }


		/// <summary>
        /// Sets/returns the compliance voltage set point
		/// Compliance voltage being the maximum positive or negative voltage the instrument will supply
		/// </summary>
        public override double VoltageComplianceSetPoint_Volt
		{
			get 
			{
				// Read current in volts
				string rtn = instrumentChassis.Query(":SENS:VOLT:PROT:LEV?", this);				
				
				// Convert and return
				return Convert.ToDouble(rtn);								
			}
			
			set
			{
				// Determine compliance voltage
				// Some units are low voltage (LV) which have their top range disabled
				double compVolt;
				if (HardwareData["IsLVUnit"].ToLower() == "true")
				{
					compVolt = Convert.ToDouble(HardwareData["MaxVoltageLimitLV"]);
				}
				else
				{
					compVolt = Convert.ToDouble(HardwareData["MaxVoltageLimit"]);
				}

				if (Math.Abs(value) > compVolt)
				{
					throw new InstrumentException("Set value level '" + value.ToString() +
						"' is greater than maximum voltage possible '" + compVolt.ToString() + "'");
				}

				// Set voltage
				instrumentChassis.Write(":SENS:VOLT:PROT:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:VOLT:RANG " + value.ToString().Trim(), this);
            }
		}




        

        //
        //  **********    new functions as reqd for mz... **************
        //



      
        /// <summary>
        /// Set the source mode to be fixed current 
        /// </summary>
        public override void SourceFixedCurrent()
        {        	
            instrumentChassis.Write(":SOUR:FUNC CURR", this);
            instrumentChassis.Write(":SOUR:CURR:MODE FIX", this);
        }

        /// <summary>
        /// Set the source mode to be fixed voltage 
        /// </summary>
        public override void SourceFixedVoltage()
        {
            instrumentChassis.Write(":SOUR:FUNC VOLT", this);
            instrumentChassis.Write(":SOUR:VOLT:MODE FIX", this);
        }


        
        ///// <summary>
        ///// ConfigureCurrentSweepReadings to be current or voltage
        ///// </summary>
        ///// <param name="measureCurrent">are we measuring current?</param>
        //public void ConfigureCurrentSweepReadings(bool measureCurrent)
        //{
        //    if (measureCurrent)
        //    {
        //        instrumentChassis.Write(":SOUR:FUNC CURR", this);
        //    }
        //    else
        //    {
        //        instrumentChassis.Write(":FORM:ELEM VOLT", this);
        //    }
        //}

        /// <summary>
        /// Configure sweep reading parameters
        /// </summary>
        public override void ConfigureCurrentSweepReadings()
        {
            instrumentChassis.Write(":SOUR:FUNC CURR", this);
            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
        }

        /// <summary>
        /// Set / Get to indicate if we are using 4 wire sense
        /// </summary>
        public bool FourWireSense
        {
            get
            {
                string rtn = instrumentChassis.Query(":SYST:RSEN?", this);
                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set FourWireSense
                instrumentChassis.Write(":SYST:RSEN " + boolVal, this);
            }
        }

        /// <summary>
        /// Set / Get to indicate if we are using the front terminals,
        /// (of course if we aren't then we must be using the rear terminals)
        /// </summary>
        public bool UseFrontTerminals
        {
            get
            {
                string rtn =instrumentChassis.Query(":ROUT:TERM?", this);
                // Return bool value
				return (rtn == "FRON");
            }
            set
            {               
                string terminals = value ? "FRON" :"REAR";
                instrumentChassis.Write(":ROUT:TERM " + terminals, this);
            }
        }
        

        /// <summary>
        /// Source a swept current
        /// </summary>
        /// <param name="Imin_amp">The value we are seeping from</param>
        /// <param name="Imax_amp">The value we are seeping to</param>
        /// <param name="numPts">The number of points we will be sweeping over</param>
        public override void SourceSweptCurrent(double Imin_amp, double Imax_amp, int numPts)
        {
            double Istep_A = (double)(Imax_amp - Imin_amp) / (numPts - 1);

            instrumentChassis.Write(":SOUR:CURR:START " + Imin_amp, this);
            instrumentChassis.Write(":SOUR:CURR:STOP " + Imax_amp, this);
            instrumentChassis.Write(":SOUR:CURR:STEP " + Istep_A, this);
            instrumentChassis.Write(":SOUR:SWE:POIN " + numPts, this);
            instrumentChassis.Write(":SOUR:CURR:MODE SWE", this);
            // Force autozero off for a big speedup
            instrumentChassis.Write(":SYST:AZER:STAT OFF", this);

        }

        /// <summary>
        /// Set up both the sense voltage compliance and the sense voltage range
        /// </summary>
        /// <param name="senseCompliance_volt">sense voltage compliance</param>
        /// <param name="senseRange_volt">sense voltage range</param>
        public override void SenseVoltage(double senseCompliance_volt, double senseRange_volt)
        {
            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);

            instrumentChassis.Write(":SENS:FUNC 'VOLT:DC'", this);
            instrumentChassis.Write(":SENS:VOLT:PROT " + senseCompliance_volt, this);
            SenseVoltageRange = senseRange_volt;
        }

        /// <summary>
        /// Set the voltage sense range
        /// </summary>
        public double SenseVoltageRange
        {
            set
            {
                instrumentChassis.Write(":SENS:VOLT:RANG " + value, this);
            }
        }

        /// <summary>
        /// Set the source current value immediately
        /// </summary>
        /// <param name="value_amp"></param>
        public override void SetCurrent(double value_amp)
        {
            instrumentChassis.Write(":SOUR:CURR:LEV:IMM:AMPL " + value_amp, this);
        }

       

      
        /// <summary>
        /// Get  / Set specified current amplitude when triggered
        /// </summary>
        public double TriggerAmplitude_amp
        {
            set
            {
                instrumentChassis.Write(":SOUR:CURR:TRIG " + value, this);
            }
            get 
            {
                // Read the current trigger amplitude back
                string rtn = instrumentChassis.Query(":SOUR:CURR:TRIG?", this);

                // Convert and return
                return Convert.ToDouble(rtn);	
            }
        }

        /// <summary>
        /// Get  / Set specified voltage amplitude when triggered
        /// </summary>
        public double TriggerAmplitude_volt
        {
            set
            {
                instrumentChassis.Write(":SOUR:VOLT:TRIG  " + value, this);
            }
            get
            {
                // Read the current trigger amplitude back
                string rtn = instrumentChassis.Query(":SOUR:VOLT:TRIG?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }
        }



        /// <summary>
        /// Set up measurement for sourcing current and measuring voltage
        /// </summary>
        /// <param name="voltageCompliance_V">the voltage compliance figure you want</param>
        /// <param name="voltageRange_V">the voltage range you want</param>
        /// <param name="sourceMeasAutoDelay">whether we use an automaic delay or not</param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="AutoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void InitSourceIMeasureV_MeasurementAccuracy(double voltageCompliance_V, double voltageRange_V, bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s, int numAverages, double integrationRate, bool AutoZeroState)
        {
            SourceFixedCurrent();
            SenseVoltage(voltageCompliance_V, voltageRange_V);

            SetMeasurementAccuracy(sourceMeasAutoDelay, sourceMeasSpecificDelay_s, numAverages, integrationRate, AutoZeroState);        
        }

        /// <summary>
        /// Setting our measurment accuracy up
        /// </summary>
        /// <param name="sourceMeasAutoDelay">whether we use an automaic delay or not</param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="autoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void SetMeasurementAccuracy(bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s, int numAverages, double integrationRate, bool autoZeroState)
	    {
            if (!sourceMeasAutoDelay)
            {
                SetSourceDelayMeasureManual(sourceMeasSpecificDelay_s);
            }
            else
            {
                SetSourceDelayMeasureAuto();
            }

            AveragingCount = numAverages;
		    IntegrationRate_SenseVoltage = integrationRate;
		    AutoZero =  autoZeroState;
	    }

        /// <summary>
        /// Set measurement accuracy
        /// </summary>
        /// <param name="sourceMeasAutoDelay">Auto delay on/off</param>
        /// <param name="sourceMeasSpecificDelay_s">Source measure delay S</param>
        /// <param name="numAverages">Number of averages per measurement</param>
        /// <param name="integrationRate">Integration rate</param>
        public override void SetMeasurementAccuracy(bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s, int numAverages, double integrationRate)
        {
            // TODO - check last param
            SetMeasurementAccuracy(sourceMeasAutoDelay, sourceMeasSpecificDelay_s, numAverages, integrationRate, false);
        }

        /// <summary>
        /// Get / set the averaging count
        /// </summary>
        public Int32 AveragingCount
        {
            set
            {
                // Averaging
                if (value < 2)
                    instrumentChassis.Write(":AVER OFF", this);
                else
                {
                    instrumentChassis.Write(":AVER ON", this);
                    instrumentChassis.Write(":AVER:TCON REP", this);
                    instrumentChassis.Write(":AVER:COUN " + value, this);
                }
            }
            get
            {
                // Read the averaging count
                string meas = instrumentChassis.Query(":AVER:COUN?", this);
                Int32 rtn = Convert.ToInt32(meas);
                return (rtn);                
            }
        }

        /// <summary>
        /// Getting  / Setting the sense voltage integration rate
        /// </summary>
        public double IntegrationRate_SenseVoltage
        {
            get
            {
                // Read current in amps
                string meas = instrumentChassis.Query("SENS:VOLT:NPLC?", this);

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas);

                return (rtn);                
            }
            set
            {
	            double setRate = value;

                if (value > 10.0)
		            setRate=10.0;
                if (value < 0.01)
		            setRate = 0.01;
               
	            // Set Integration rate - not that it is set for all measurement types
	            instrumentChassis.Write( "SENS:VOLT:NPLC " + setRate, this );
            }
        }

        /// <summary>
        /// Getting  / Setting the sense current integration rate
        /// </summary>
        public double IntegrationRate_SenseCurrent
        {
            get
            {
                // Read current in amps
                string meas = instrumentChassis.Query("SENS:CURR:NPLC?", this);

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas);

                return (rtn);
            }
            set
            {
                double setRate = value;

                if (value > 10.0)
                    setRate = 10.0;
                if (value < 0.01)
                    setRate = 0.01;

                // Set Integration rate - not that it is set for all measurement types
                instrumentChassis.Write("SENS:CURR:NPLC " + setRate, this);
            }
        }

        /// <summary>
        /// Autozero the meter
        /// </summary>
        public bool AutoZero
        {
            set
            {
                if (value == false)
                {
                    // Autozero OFF, NPLC cache ON
                    instrumentChassis.Write(":SYST:AZER:STAT OFF", this);		// Turn off Autozero
                    instrumentChassis.Write(":SYST:AZER ONCE", this);			// Force an immediate update - Helps to prevent drift			 
                    instrumentChassis.Write(":SYST:AZER:CACH:STAT 1", this);	// Enable NPLC caching
                    instrumentChassis.Write(":SYST:AZER:CACH:REFR", this);	// Update the NPLC cache values

                    // Used in the sweep to see whether we send the AutoZeroRefresh or not
                    m_bAutoZero = false;
                }
                else
                {
                    // Autozero ON, NPLC cache OFF
                    instrumentChassis.Write(":SYST:AZER 1", this);   // Turn on autozero

                    m_bAutoZero = true;
                }
            }
        }

        
        /// <summary>
        /// Force an immediate autozero update, function only used if Autozero is turned off
        /// </summary>
        public override void AutoZeroRefresh()
        {
	        if(!m_bAutoZero)
	        {
		        instrumentChassis.Write( ":SYST:AZER ONCE", this );		// Force an immediate update - Helps to prevent drift			 
		        instrumentChassis.Write( ":SYST:AZER:CACH:REFR", this);	// Update the NPLC cache values
	        }
        }

        /// <summary>
        /// Set it to a manual delay time and specify the time reqd
        /// </summary>
        /// <param name="specificDelay_s">specific time period for delays</param>
        public void SetSourceDelayMeasureManual( double specificDelay_s )
        {	        
	        instrumentChassis.Write( ":SOUR:DEL:AUTO OFF", this );
	        instrumentChassis.Write( ":SOUR:DEL " + specificDelay_s, this );
        }

        /// <summary>
        /// Set it to an automatic delay time
        /// </summary>
        public void SetSourceDelayMeasureAuto()
        {    
            instrumentChassis.Write(":SOUR:DEL:AUTO ON", this);
        }

        /// <summary>
        /// Set source delay 
        /// </summary>
        /// <param name="autoDelay">Auto delay on/off</param>
        /// <param name="specificDelay_s">Source delay S</param>
        public override void SetSourceDelayMeasure(bool autoDelay, double specificDelay_s)
        {
            if (autoDelay)
            {
                SetSourceDelayMeasureAuto();
            }
            else
            {
                SetSourceDelayMeasureManual(specificDelay_s);
            }
        }


        /// <summary>
        /// Sets up the meter ready for doing a current sweep, sourcing current and measuring voltage
        /// </summary>
        /// <param name="Imin_amp">The value to sweep from</param>
        /// <param name="Imax_amp">The value to sweep to</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceIMeasureV_CurrentSweep(double Imin_amp, double Imax_amp, int numPts, int inputTriggerLine, int outputTriggerLine)
        {

            // Define sweep
            SourceSweptCurrent(Imin_amp, Imax_amp, numPts);

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureCurrentSweepReadings();

        }

        /// <summary>
        /// Sets up the meter ready for doing a single fixed value current, sourcing current and measuring voltage
        /// </summary>
        /// <param name="I_amp">the fixed current you wish to set</param>
        /// <param name="numPts">Perform count source-measure operation in the trigger layer, and this is the count </param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceIMeasureV_TriggeredFixedCurrent(double I_amp, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            SetCurrent(I_amp);				// This applies the current immediately.

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureCurrentSweepReadings();
            // MF - check
            SourceFixedCurrent();
        }

        /// <summary>
        /// Setup for doing a untriggered spot measurment
        /// </summary>
        public override void InitSourceIMeasureV_UntriggeredSpotMeas()
        {
            SourceFixedCurrent();
            DisableTriggering();

            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
            instrumentChassis.Write("FORM ASCII", this);
        }

        //
        // Source voltage, measure current
        //
       
        /// <summary>
        /// Set up measurement for sourcing voltage and measuring current
        /// </summary>
        /// <param name="currentCompliance_A"></param>
        /// <param name="currentRange_A"></param>
        /// <param name="sourceMeasAutoDelay"></param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="AutoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void InitSourceVMeasureI_MeasurementAccuracy(double currentCompliance_A, double currentRange_A, bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s, int numAverages, double integrationRate, bool AutoZeroState)
        {
            SourceFixedVoltage();
            SenseCurrent(currentCompliance_A, currentRange_A);

            SetMeasurementAccuracy(sourceMeasAutoDelay, sourceMeasSpecificDelay_s, numAverages, integrationRate, AutoZeroState);
        }

        /// <summary>
        /// Set up the current compliance and range
        /// </summary>
        /// <param name="IsenseCompliance_amp">compliance  value</param>
        /// <param name="IsenseRange_amp">range value</param>
        /// <returns>a bool to indicate that both compliance and range have been set</returns>
        public override bool SenseCurrent(double IsenseCompliance_amp, double IsenseRange_amp)
        {
	         instrumentChassis.Write( ":FORM:ELEM VOLT,CURR", this ); 

	         instrumentChassis.Write( ":SENS:FUNC 'CURR:DC'", this );

	        // Range is set to the lowest range containing the compliance value
             bool complianceSet = SetIsenseCompliance(IsenseCompliance_amp);

	        // Should now be able to set a higher range if required
	        bool rangeSet = false;
            double Value = IsenseRange_amp;

            //if (IsenseRange_amp > Value)
	        //{
                instrumentChassis.Write(":SENS:CURR:RANG " + IsenseRange_amp, this);
		        rangeSet = true;
	        //}
            
	        return ( complianceSet && rangeSet );
        }

        /// <summary>
        /// Setting the current sense compliance value up based on knowledge of the ke2400 allowable values, thather than just throwing
        /// exceptions
        /// </summary>
        /// <param name="reqCompliance_amp"></param>
        /// <returns>bool to indicate that was able to set up compliance ok</returns>
        public bool SetIsenseCompliance( double reqCompliance_amp )
        {
	        bool status = true;

	        double[] ranges =  {	1.05e-6,
								        10.5e-6,
								        105.0e-6,
								        1.05e-3,
								        10.5e-3,
								        105.0e-3,
								        1.050		};

	        // range max is 1.0 each step down divide by 10

	        double range_now = IsenseRange_amp;
	        double compliance_now = CurrentComplianceSetPoint_Amp; 
        	uint index_now;
	        // what is current range index?
	        for( index_now = 0; (index_now < 7) && (range_now != ranges[index_now]); index_now++ );
        	
            uint index_reqd;
	        // What should new range be ?
	        for( index_reqd = 0; (index_reqd < 7) && (Math.Abs(reqCompliance_amp)  >= ranges[index_reqd]); index_reqd++ );
        	
	        if( index_reqd >= index_now ) // if increase range
	        {
		        if( index_now == 6 ) // can't increase_range any more
		        {	// Compliance to max
			        instrumentChassis.Write( ":SENS:CURR:PROT " + ranges[6], this );
			        status = false;
		        }
		        else
		        {
			        // Step up range while it is less than reqd
			        for(  ; index_reqd >= index_now ; index_now++ )
			        {
				        // Compliance
                        double myVal = ranges[index_now] / 1.05;
				        instrumentChassis.Write( ":SENS:CURR:PROT " + myVal, this );
        				

				        // Range
				        
                        instrumentChassis.Write( ":SENS:CURR:RANG " + ranges[index_now], this );
			        }
        			
			        // Set final Compliance
			        instrumentChassis.Write( ":SENS:CURR:PROT " + reqCompliance_amp, this );
		        }
	        }
	        else
	        {
		        if ( index_now == 0 )
		        {
			        instrumentChassis.Write( ":SENS:CURR:PROT " + ranges[0], this );
			        status = false;
		        }
		        else
		        {
			        // Step it down while it is more than reqd
			        for (  ; index_reqd <= index_now ; index_now-- )
			        {
				        // Range
				        instrumentChassis.Write( ":SENS:CURR:RANG " + ranges[index_now], this );
        				
				        // Compliance
                        double myVal = ranges[index_now] / 1.05;
				        instrumentChassis.Write( ":SENS:CURR:PROT " + myVal, this );
			        }
        			
			        // Set final Compliance
			        if( ranges[index_now + 1] * 0.01 > reqCompliance_amp )
				        instrumentChassis.Write( ":SENS:CURR:PROT " + reqCompliance_amp , this);
			        else
				        instrumentChassis.Write( ":SENS:CURR:PROT " + ranges[index_now + 1]  , this);
		        }
	        }

	        return status;
        }

        /// <summary>
        /// Get the sense range (current)
        /// </summary>
        /// <returns>the sense range value</returns>
        public double IsenseRange_amp
        {
            get
            {
                string rtn = instrumentChassis.Query(":SENS:CURR:RANG:UPP?", this);
                return Convert.ToDouble(rtn);
            }
        }

        /// <summary>
        /// Set up for sourcing voltage and measuring current, 
        /// and possibly doing a triggered current sweep
        /// </summary>
        /// <param name="Vmin_volt">minimum voltage for sweep</param>
        /// <param name="Vmax_volt">maximum voltage for sweep</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceVMeasureI_VoltageSweep(double Vmin_volt, double Vmax_volt, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            // Define sweep
            SourceSweptVoltage(Vmin_volt, Vmax_volt, numPts);

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Always returns in the order SENSE, SOURCE
            ConfigureVoltageSweepReadings();
        }

        // sourceImeasureV

        /// <summary>
        /// configure so it will provide data readings in the order Voltage, Current
        /// </summary>
        public override void ConfigureVoltageSweepReadings()
        {
	        instrumentChassis.Write( ":FORM:ELEM VOLT,CURR" , this); 
        }

        /// <summary>
        /// set up for a swept voltage sweep (doesn't use triggerlink calls)
        /// </summary>
        /// <param name="Vmin_volt">start voltage</param>
        /// <param name="Vmax_volt">finish voltage</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        public override void SourceSweptVoltage( double Vmin_volt, double Vmax_volt, int numPts )
        {
	        double stepsize = (double)(Vmax_volt - Vmin_volt)/(numPts - 1);

            instrumentChassis.Write(":SOUR:VOLT:START " + Vmin_volt, this);
            instrumentChassis.Write(":SOUR:VOLT:STOP " + Vmax_volt, this);
            instrumentChassis.Write(":SOUR:VOLT:STEP " + stepsize, this);
            instrumentChassis.Write(":SOUR:SWE:POIN " + numPts, this);
            instrumentChassis.Write(":SOUR:VOLT:MODE SWE", this);
            // Force autozero off for a big speedup
            instrumentChassis.Write(":SYST:AZER:STAT OFF", this);
        }

        /// <summary>
        /// Setup for sourcing a fixed voltage and measuring I (using triggers)
        /// </summary>
        /// <param name="setPoint_volt"></param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceVMeasureI_TriggeredFixedVoltage(double setPoint_volt, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            VoltageSetPoint_Volt = setPoint_volt;

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureVoltageSweepReadings();
            // MF - check
            SourceFixedVoltage();
        }

        /// <summary>
        /// Switches back from triggered type, and sets up for sourcing voltage whilst measuring current
        /// </summary>
        public override void InitSourceVMeasureI_UntriggeredSpotMeas()
        {
            SourceFixedVoltage();
            DisableTriggering();

            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
            instrumentChassis.Write("FORM ASCII", this);
        }





      
        /// <summary>
        /// Configure your trigger linking stuff
        /// </summary>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        /// <param name="triggerViaTLinks">are we triggering via the tlinks or immediately</param>
        public override void ConfigureTriggering(int numPts, int inputTriggerLine, int outputTriggerLine, bool triggerViaTLinks)
        {
            if (triggerViaTLinks)
            {
                instrumentChassis.Write(":TRIG:SOUR TLINK", this);			// Expect external trigger
            }
            else
            {
                instrumentChassis.Write(":TRIG:SOUR IMM", this);
            }
            ConfigureTriggerlines(inputTriggerLine, outputTriggerLine);

            instrumentChassis.Write(":TRIG:TCON:DIR SOUR", this);		// Trigger bypass set to SOURce will bypass the control source once (other option -> ACCeptor )
            instrumentChassis.Write(":TRIG:COUN " + numPts, this);	// Perform count source-measure operation in the trigger layer
            instrumentChassis.Write(":TRIG:TCON:OUTP SENS", this);		// Output trigger after each measurement
            //instrumentChassis.Write(":TRIG:TCON:OUTP SOUR", this);		// Output trigger after source is set - Causes a 1 point offset ?
        }


        /// <summary>
        /// Allows you to configure triggerlines 
        /// </summary>
        /// <param name="inputTriggerLine">The input line you wish to select</param>
        /// <param name="outputTriggerLine">The output line you wish to associate with the input line</param>
        public override void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine)
        {
            instrumentChassis.Write(":TRIG:ILIN " + inputTriggerLine, this);	// Default is 1
            instrumentChassis.Write(":TRIG:OLIN " + outputTriggerLine, this);	// Default is 2
        }

        /// <summary>
        /// Disable the triggering
        /// </summary>
        public override void DisableTriggering()
        {
            instrumentChassis.Write(":ABOR", this);
            instrumentChassis.Write(":TRIG:CLE", this);
            instrumentChassis.Write(":ARM:SOUR IMM", this);			// Source immediately. Dont wait for external trigger.
            instrumentChassis.Write(":TRIG:SOUR IMM", this);			// Source immediately. Dont wait for external trigger.
            instrumentChassis.Write(":TRIG:TCON:OUTP NONE", this);		// No trigger output.
        }



        /// <summary>
        /// Aborting a sweep 
        /// </summary>
        public override void AbortSweep()
        {
            CleanUpSweep();
        }

        /// <summary>
        /// Cleans up a sweep, aborts it and clears the triggering and traces
        /// </summary>
        public override void CleanUpSweep()
        {
	        // Abort the sweep
            // Abort any pending operations
            instrumentChassis.Clear();
            instrumentChassis.Write(":OUTPUT OFF", this);
            instrumentChassis.Write(":ABORT", this);
            ClearSweepTriggering();
	        ClearSweepTraceData();
        }

        /// <summary>
        /// Clears the trigger
        /// </summary>
        public override void ClearSweepTriggering()
        {
            instrumentChassis.Write(":TRIG:CLEAR", this);
            // Autozero ON - prevents strange readings !
            instrumentChassis.Write_Unchecked(":SYST:AZER:STAT ON", this);
            // Output format = ASCII
            instrumentChassis.Write_Unchecked(":FORM ASC", this);
        }

        /// <summary>
        /// Clears the trace
        /// </summary>
        public override void ClearSweepTraceData()
        {
            instrumentChassis.Write(":TRAC:CLEAR", this); 
        }

        /// <summary>
        /// Starts a sweep, this command uses a write unchecked... so you need to
        /// call a WaitForOperationToComplete() if you wish to ensure operation has finished.
        /// That, or GetESRStatus for yourself for your specific requirements
        /// </summary>
        public override void StartSweep()
        {
            //play();
            instrumentChassis.Write_Unchecked(":INIT", this);
        }

        /// <summary>
        /// Trigger measurement
        /// </summary>
        public override void Trigger()
        {
            instrumentChassis.Write_Unchecked(":INIT", this);
        }


        /// <summary>
        /// Making use of the chassis OPC* command
        /// </summary>
        public void WaitForOperationToComplete()
        {
            instrumentChassis.WaitForOperationComplete(); 
        }

        /// <summary>
        /// Wait for sweep to finish
        /// </summary>
        /// <returns>True when sweep ends</returns>
        public override bool WaitForSweepToFinish()
        {
            int oldTimeout = instrumentChassis.Timeout_ms;
            instrumentChassis.Timeout_ms = 30000;
            instrumentChassis.WaitForOperationComplete();
            instrumentChassis.Timeout_ms = oldTimeout;
            return true;
        }
        
        /// <summary>
        /// Gets the esr status, (hooking into the instrument chassis)
        /// </summary>
        /// <returns>the standard event register byte</returns>
        public byte GetESRStatus()
        {
	       return(instrumentChassis.StandardEventRegister);           
        }

        /// <summary>
        /// A public structure for the sweep data that will be returned
        /// </summary>
        public struct SweepData
        {
            /// <summary>
            /// An array of voltages
            /// </summary>
            public Single[] voltage_V;
            /// <summary>
            /// An array of currents
            /// </summary>
            public Single[] current_mA;
        }


        /// <summary>
        /// Returns the data from a sweep,
        /// </summary>
        /// <returns>a sweepdata structure containing an array of voltages and an array of currents</returns>
        public override void GetSweepDataSet(out Double[] VoltageData, out Double[] CurrentData)
        {
            //instrumentChassis.Write("FORM:BORD NORM", this ); // Normal byte order
            //instrumentChassis.Write("FORM:BORD SWAP", this); // Big Endian byte order
            //instrumentChassis.Write("FORM REAL, 32", this );

            //Guorong:"write" is not supported in K2400.change "write" to write_unchecked
            instrumentChassis.Write_Unchecked("FORM:BORD SWAP", this);
            System.Threading.Thread.Sleep(20);
            instrumentChassis.Write_Unchecked("FORM REAL, 32", this);

            byte[] buffer = instrumentChassis.QueryByteArray_Unchecked("FETCH?", this);
            
            Single[] valueList = ParseBinaryData( buffer );

            VoltageData = new Double[valueList.Length / 2];
            CurrentData = new Double[valueList.Length / 2];

            int x = 0;
            
            for (int i = 0; i < valueList.Length; i+=2)
            {
                CurrentData[x] = (double)valueList[i + 1];
                VoltageData[x] = (double)valueList[i];
                x++;
            }          
        }

        /// <summary>
        /// Takes the binary data (byte array), ignores the first 2 bytes, then
        /// converts groups of 4 bytes into singles and eventually returns an array of singles
        /// </summary>
        /// <param name="buffer">the bute array</param>
        /// <returns>an array of singles</returns>
        public override Single[] ParseBinaryData( byte[] buffer )
        {
            Single[] valueList;

            int buffSize = buffer.Length;
            int index = 0;
            valueList = new Single[(buffSize-2) / 4];

            // Ignore the first 2 bytes as they contain header information
            for ( int i = 2; i+3 < buffer.Length; i += 4 )
            {  
                byte[] subArray = Alg_ArrayFunctions.ExtractSubArray(buffer, i, i+4);

                valueList[index] = Alg_BigEndianConvert.BigEndianBytesToSingle(subArray);
                
                index++;
            }

            return valueList;
        }

        #endregion

        /// <summary>
        /// Get/Set to enable or disable the hardware interlock. 
        /// </summary>
        public bool EnableInterLock
        {

            get
            {

                string cmd = ":OUTP:INT:STAT?";

                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                // "1": Enable interlock

                // "0": Disable interlock

                return rsp.Contains("1");

            }

            set
            {

                string cmd = ":OUTP:INT:STAT " + (value ? "1" : "0");

                instrumentChassis.Write_Unchecked(cmd, this);

            }

        }

        /// <summary>
        /// Get/Set output-off state of the SourceMeter.
        /// </summary>
        public EnumOutputOffType OutputOffState
        {

            get
            {

                string cmd = ":OUTP:SMOD?";

                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                switch (rsp)
                {

                    case "HIMP":

                        return EnumOutputOffType.HIMPedance;

                    case "NORM":

                        return EnumOutputOffType.NORMal;

                    case "ZERO":

                        return EnumOutputOffType.ZERO;

                    case "GUAR":

                        return EnumOutputOffType.GUARd;

                    default:

                        throw new InstrumentException("Unexpected response string getting with query \'" +

                            cmd + "\' : " + rsp);

                }

            }

            set
            {

                string cmdPrefix = ":OUTP:SMOD ";

                string valStr = "";



                switch (value)
                {

                    case EnumOutputOffType.HIMPedance:

                        valStr = "HIMP";

                        break;

                    case EnumOutputOffType.NORMal:

                        valStr = "NORM";

                        break;

                    case EnumOutputOffType.ZERO:

                        valStr = "ZERO";

                        break;

                    case EnumOutputOffType.GUARd:

                        valStr = "GUAR";

                        break;

                }

                this.instrumentChassis.Write(cmdPrefix + valStr, this);

            }

        }

        /// <summary>
        /// With auto output-off enabled, an :INITiate (or :READ? or MEASure?) will
        /// start source-measure operation. The output will turn on at the beginning
        /// of each SDM (source-delay-measure) cycle and turn off after each measurement is completed.
        /// </summary>
        public bool EnableAutoOutput
        {

            get
            {

                string cmd = ":SOUR:CLE:AUTO?";

                string rsp = this.instrumentChassis.Query_Unchecked(cmd, this);



                return (rsp.Contains("1"));

            }

            set
            {

                string cmd = ":SOUR:CLE:AUTO " + (value ? 1 : 0);

                this.instrumentChassis.Write(cmd, this);

            }

        }

        /// <summary>
        /// 
        /// </summary>
        public enum EnumOutputOffType
        {

            /// <summary>
            /// Disconnect Input/Output
            /// </summary>
            HIMPedance,

            /// <summary>
            /// Normal output-off state
            /// </summary>
            NORMal,

            /// <summary>
            /// Zero output-off state
            /// </summary>
            ZERO,

            /// <summary>
            /// Guard output-off state
            /// </summary>
            GUARd,

        }
        
        /// <summary>
        /// Change the output on the line specified by the slot ID only
        /// </summary>
        public bool DigitalOutputState_SlotID {

            get {

                int lineNumber;
                if (!int.TryParse(base.Slot, out lineNumber)) {

                    throw new InstrumentException("Invalid Slot ID has been specified for the Ke24xx");
                }

                return this.digiOutLines[lineNumber - 1].LineState;
            }
            set {

                int lineNumber;
                if (!int.TryParse(base.Slot, out lineNumber))
                {

                    throw new InstrumentException("Invalid Slot ID has been specified for the Ke24xx");

                }

                this.digiOutLines[lineNumber - 1].LineState = value;
            }
        }

		#region Private Data

		// Chassis reference
		private Chassis_Ke24xx instrumentChassis;
        private List<IInstType_DigitalIO> digiOutLines;

        // Bool values
        private const string falseStr = "0";
		private const string trueStr = "1";

        
        private bool m_bAutoZero;


		#endregion
	}
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ke24xx_DigiOut.cs
//
// Author: Paul.Annetts, 2007, 
//         ported to 24xx by Mark Fullalove
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Keithley Virtual Digital IO line instrument
    /// </summary>
    public class Inst_Ke24xx_DigiOutLine : UnmanagedInstrument, IInstType_DigitalIO
    {
        internal Inst_Ke24xx_DigiOutLine(string linename, Chassis_Ke24xx ke24xx, int lineNbr)
            : base(linename, ke24xx)
        {
            // remember the chassis
            this.instrumentChassis = ke24xx;
            // digital line number
            this.lineNbr = lineNbr;
            // mask value (obtained by left-bit-shifting 1)
            this.lineNbrMask = 1 << (lineNbr - 1);
            // anti-mask value (bit-wise exclusive-OR of binary 1111 and the mask)
            this.lineNbrAntiMask = 0x0F ^ this.lineNbrMask;
        }

        #region Private data
        /// <summary>
        /// Underlying instrument
        /// </summary>
        private Chassis_Ke24xx instrumentChassis;
        
        /// <summary>
        /// Digital output line number 
        /// </summary>
        private int lineNbr;
        /// <summary>
        /// Mask for the digital output line number (used to extract/insert specific value for this line)
        /// </summary>
        private int lineNbrMask;
        /// <summary>
        /// Anti-Mask for the digital output line number 
        /// (used to retrieve values for all other lines apart from this one)
        /// </summary>
        private int lineNbrAntiMask;        
        #endregion

        /// <summary>
        /// Line state
        /// </summary>
        public bool LineState
        {
            get
            {
                int allLines = getAllLinesState();
                // mask out this line's value
                bool lineState;
                if ((allLines & lineNbrMask) > 0) lineState = true;
                else lineState = false;
                // return it
                return lineState;
            }
            set
            {
                int allLines = getAllLinesState();
                // value of all lines apart from this one
                int otherLines = allLines & lineNbrAntiMask;
                // calculate the new line setting
                int newLines;
                if (value) newLines = otherLines + lineNbrMask;
                else newLines = otherLines;

                // now send it to the instrument
                string cmd = "SOURCE2:TTL " + newLines;
                instrumentChassis.Write(cmd, null);
            }
        }

        /// <summary>
        /// Get state of all lines of the Ke24xx
        /// </summary>
        /// <returns>integer containing all lines state (bit-masked)</returns>
        private int getAllLinesState()
        {
            // get states of all lines in the Ke24xx
            string resp = instrumentChassis.Query("SOURCE2:TTL?", null);
            int val = int.Parse(resp);
            return val; 
        }

        /// <summary>
        /// Default State
        /// </summary>
        public override void SetDefaultState()
        {
            LineState = false;
        }
    }
}

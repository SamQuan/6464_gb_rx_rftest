// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ag70843.cs
//
// Author: joseph.olajubu, 2007
// Design: BERT Driver Agilent 71562/70843 DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for Ag70843 BERT Mainframe                                                                                                                              
    /// </summary>
    public class Chassis_Ag70843 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ag70843(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord chassis70843BData = new ChassisDataRecord(
                "HEWLETT-PACKARD 70843B",	// hardware name 
                "B.00.00",		            // minimum valid firmware version 
                "B.01.08");		            // maximum valid firmware version 
            ValidHardwareData.Add("Ag70843BChassisData", chassis70843BData);

            ChassisDataRecord chassis70843CData = new ChassisDataRecord(
                "AGILENT TECHNOLOGIES 70843C",	// hardware name 
                "C.00.00",		            // minimum valid firmware version 
                "C.01.02");		            // maximum valid firmware version 
            ValidHardwareData.Add("Ag70843CChassisData", chassis70843CData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {  

                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                return idn[3];
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    
                    // Setup Standard Error register mask  on a 488.2 instrument
                    base.Timeout_ms = 20000;
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 16 + 32;
                    // clear the status registers
                    this.Write("*CLS", null);
                }
            }
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag70843_ErrorDetector.cs
//
// Author: joseph.olajubu, 2007
// Design: BERT Driver Agilent 71562/70843 DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for Agilent 70843 Error Detector
    /// </summary>
    public class Inst_Ag70843_ErrorDetector : Instrument, IInstType_BertErrorAnalyser
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag70843_ErrorDetector(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instr70843B = new InstrumentDataRecord(
                "HEWLETT-PACKARD 70843B",	// hardware name 
                "B.00.00",		            // minimum valid firmware version 
                "B.01.08");		            // maximum valid firmware version 
            ValidHardwareData.Add("Ag70843B", instr70843B);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag70843",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "1.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag70843", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag70843)chassisInit;
        }
        #endregion

        #region Instrument overrides
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.write("*CLS");
            this.write("*RST");
            this.write("*CLS");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region IInstType_BertErrorAnalyser Members

        /// <summary>
        /// Get the analyser state (true: running, false: not running)
        /// </summary>
        public bool AnalyserEnabled
        {
            get 
            {
                string resp = this.Query("SENS1:GATE?");

                if (resp == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Start the Analyser
        /// </summary>
        public void AnalyserStart()
        {
           this.write("*CLS");
           this.write("SENS1:GATE 1");
        }


        /// <summary>
        /// Stop the Analyser
        /// </summary>
        public void AnalyserStop()
        {
            this.write("SENS1:GATE 0");
        }


        /// <summary>
        /// Automatically setup the receiver in terms of eye delay and threshold, and 
        /// also pattern synchronisation
        /// </summary>
        public void AutoSetup()
        {
            // Set the sync threshold to a value that will improve sync.
            this.write("SENSE1:SYNC:THR 1E-2");

            //Initiate a continuous search for the value of data/clock delay that puts
            //the active clock edge in the center of the data eye, midway between the two relative delay
            //points with a measured BER just in excess of the BER configured by the
            //EYE:THReshold command. If successful, the command leaves the data/clock delay at
            //this value and the center of the eye can be found by querying the data delay value.              
            this.write("SENSE1:EYE:TCENTRE ON");

            //Initiate a continuous search for the zero-to-one threshold voltage
            //midway between the two zero-to-one threshold voltages with a measured BER just in
            //excess of the BER configured by the EYE: THReshold command. If successful, the
            //command leaves the zero-one-threshold at this value and the center of the eye can be
            //found by querying the zero-one-threshold value.
            this.write("SENSE1:EYE:ACENTRE ON");

            //enables an automatic mode in which the zero-to-one threshold level 
            //is set to the mean of the input signal.
            this.write("SENSE1:VOLTAGE:ZOTHRESHOLD:AUTO ON");

            this.write("*CLS");
        }

        /// <summary>
        /// Ratio of errored bits to total bit count
        /// </summary>
        public double ErrorRatio
        {
            get 
            {
                return Convert.ToDouble(this.Query("FETCH:SENSE1:ERAT?"));
            }
        }


        /// <summary>
        /// How many errored bits
        /// </summary>
        public long ErroredBits
        {
            get
            {
                return Convert.ToInt64(this.Query("FETCH:SENSE1:ECOUNT?"));
            }
        }

        /// <summary>
        /// The number of errors
        /// accumulated since the start of the gating period, where
        /// each error is a true data zero received as a data one.
        /// </summary>
        public long  ErroredBitsZerosRecievedAsOnes
        {
            get
            {
                return Convert.ToInt64(this.Query("FETCH:SENSE1:ECOUNT:ZASONE?"));
            }
        }

        /// <summary>
        /// The error ratio accumulated since the start of the gating period,
        /// where each error is a true data zero received as a data one.
        /// </summary>
        public double ErrorRatioOfZerosRecievedAsOnes
        {
            get
            {
                return Convert.ToDouble(this.Query("FETCH:SENSE1:ERATIO:ZASONE?"));
            }
        }

        /// <summary>
        ///The number of errors
        ///accumulated since the start of the gating period, where
        ///each error is a true data one received as a data zero
        /// </summary>
        public long  ErroredBitsOnesRecievedAsZeros
        {
            get
            {
                return Convert.ToInt64(this.Query("FETCH:SENSE1:ECOUNT:OASZERO?"));
            }
        }

        /// <summary>
        /// The error ratio accumulated since the start of the gating period,
        /// where each error is a true data one received as a data zero
        /// </summary>
        public double ErrorRatioOfOnesRecievedAsZeros
        {
            get
            {
                return Convert.ToDouble(this.Query("FETCH:SENSE1:ERATIO:OASZERO?"));
            }
        }

        /// <summary>
        /// Get/set the Eye Delay
        /// </summary>
        public double MeasPhaseDelay_ps
        {
            get
            {
                return Convert.ToDouble(this.Query("INPUT1:DELAY?"));
            }
            set
            {
                this.write("INPUT1:DELAY " + value);
            }
        }

        /// <summary>
        /// Get/set the Eye threshold voltage
        /// </summary>
        public double MeasThreshold_V
        {
            get
            {
                return Convert.ToDouble(this.Query("SENSE1:VOLT:ZOTHRESHOLD?"));
            }
            set
            {
                this.write("SENSE1:VOLT:ZOTHRESHOLD" + value);
            }
        }


        /// <summary>
        /// Get/set the Pattern type.
        /// In this version of the driver, only PRBS Patterns shall be supported.
        /// </summary>
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                string resp = this.Query("SENSE1:PATTERN?").Trim();
                if (resp.StartsWith("PRBS"))
                {
                    //This should really be the only response seen, unless
                    //someones been playing with the front panel
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }

            }
            set
            {
                if (value == InstType_BertDataPatternType.Prbs)
                {
                    this.write("SENSE1:PATTERN PRBS" + this.prbsLength);
                }
                else
                {
                    throw new InstrumentException("Only PRBS Pattern type is supported by this device driver");
                }
            }
        }

        /// <summary>
        /// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        /// </summary>
        public int PrbsLength
        {
            get
            {
                return this.prbsLength;
            }
            set
            {
                //Only valid values are 7,10,15,23 or 31
                if ((value == 7) || (value == 10) || (value == 15) || (value == 23) || (value == 31))
                {
                    this.prbsLength = value;
                    this.write("SENSE1:PATTERN PRBS" + this.prbsLength);
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS length attempted to be set: " + value);
                }
            }
        }

        /// <summary>
        /// Get/set the Data Polarity of the input
        /// </summary>
        public InstType_BertDataPolarity Polarity
        {
            get
            {

                string resp = this.Query("INPUT1:POLARITY?");

                if (resp == "NORM")
                {
                    return InstType_BertDataPolarity.Normal;
                }
                else if (resp == "INV")
                {
                    return InstType_BertDataPolarity.Inverted;
                }
                else
                {
                    throw new InstrumentException("Invalid response to Polarity Query from instrument: " + resp);
                }
            }
            set
            {
                if (value == InstType_BertDataPolarity.Normal)
                {
                    this.write("INPUT1:POLARITY NORMAL");
                }
                else
                {
                    this.write("INPUT1:POLARITY INV");
                }
            }
        }

        /// <summary>
        /// Has there been a sync loss since the analyser was last enabled?
        /// </summary>
        public bool SyncLossDetected
        {
            get 
            {
                string resp = this.Query("FETCH:LOSS:SYNC?").Trim();

                if (resp == "+0.00000E+000")
                {
                    return false;
                }

                double count = Convert.ToDouble(resp);

                if (count >  0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Get/Set user defined pattern. Not supported by this version of the driver.
        /// </summary>
        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }


        /// <summary>
        /// Get/set the User Pattern length. Not supported by this version of the driver.
        /// </summary>
        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region Private methods
        /// <summary>
        /// Write a command to the instrument
        /// </summary>
        /// <param name="command">The command</param>
        private void write(string command)
        {
            instrumentChassis.Write_Unchecked(command, this);

            if (command == "*RST")
            {
                instrumentChassis.Write_Unchecked(command, this);
                System.Threading.Thread.Sleep(8000);
            }
            else if (command == "*TST")
            {
                instrumentChassis.Write_Unchecked(command, this);
                System.Threading.Thread.Sleep(15000);
            }
            else
            {
                instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Query the instrument
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string Query (string command)
        {
           return instrumentChassis.Query(command, this).Trim();
        }
        #endregion



        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag70843 instrumentChassis;


        //PRBS sequence length. Default value is 23, 
        //which is what the instrument sets itself to after *RST
        //Valid values are 7,10,15,23 or 31
        int prbsLength = 23;
        #endregion

    }
}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;


namespace TEST.Ag71562_70843
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // Clock Chassis Reference
        private Chassis_Ag70340_Clocksource clockChassis;

        // PPG Chassis Reference
        private Chassis_Ag70843 ppgChassis;

        // Error Detector Chassis Reference
        private Chassis_Ag70843 errChassis;

        // Clock Instrument Reference
        private Inst_Ag70340_ClockSource clockInstr;

        // PPG Instrument Reference
        private Inst_Ag70843_PPG ppgInstr;

        // Error detector Instrument Reference
        private Inst_Ag70843_ErrorDetector errInstr;

        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            TestOutput("Creating clock chassis object");
            clockChassis = new Chassis_Ag70340_Clocksource("clockChassis", "Chassis_Ag70340_Clocksource", "GPIB0::19::INSTR");
            TestOutput(clockChassis, "Created OK");

            TestOutput("Creating PPG chassis object");
            ppgChassis = new Chassis_Ag70843("ppgChassis", "Chassis_Ag70843", "GPIB0::20::INSTR");
            TestOutput(ppgChassis, "Created OK");

            TestOutput("Creating Error Detector chassis object");
            errChassis = new Chassis_Ag70843("errChassis", "Chassis_Ag70843", "GPIB0::21::INSTR");
            TestOutput(errChassis, "Created OK");

            // create instrument objects            
            TestOutput("Creating clock instrument object");
            clockInstr = new Inst_Ag70340_ClockSource("clockInstr", "Inst_Ag70340_ClockSource", "", "", clockChassis);
            TestOutput(clockInstr, "Created OK");
           
            TestOutput("Creating PPG instrument object");
            ppgInstr = new Inst_Ag70843_PPG("ppgInstr", "Inst_Ag70843_PPG", "", "", ppgChassis);
            TestOutput(ppgInstr, "Created OK");

            TestOutput("Creating Error Detector instrument object");
            errInstr = new Inst_Ag70843_ErrorDetector("errInstr", "Inst_Ag70843_ErrorDetector", "", "", errChassis);
            TestOutput(errInstr, "Created OK");

            // put them online
            TestOutput("Putting equipment objects online");
            clockChassis.IsOnline = true;
            clockChassis.EnableLogging = true;
            TestOutput(clockChassis, "IsOnline set true OK");

            clockInstr.IsOnline = true;
            clockInstr.EnableLogging = true;
            TestOutput(clockInstr, "IsOnline set true OK");

            ppgChassis.IsOnline = true;
            ppgChassis.EnableLogging = true;
            TestOutput(ppgChassis, "IsOnline set true OK");

            ppgInstr.IsOnline = true;
            ppgInstr.EnableLogging = true;
            TestOutput(ppgInstr, "IsOnline set true OK");

            errChassis.IsOnline = true;
            errChassis.EnableLogging = true;
            TestOutput(errChassis, "IsOnline set true OK");  

            errInstr.IsOnline = true;
            errInstr.EnableLogging = true;
            TestOutput(errInstr, "IsOnline set true OK");  
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            TestOutput("Taking the chassis offline!");
            clockChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_CheckDefaultState()
        {
            TestOutput("\n\n*** T01_CheckDefaultState ***");
            clockInstr.SetDefaultState();
            //Assert.AreEqual(false, clockInstr.Enabled);
            Assert.AreEqual(3000, clockInstr.Frequency_MHz);
            Assert.AreEqual(0, clockInstr.Amplitude_dBm);
            TestOutput("\n\n*** T01_CheckDefaultState: Confirmed Clock instrument set to defualt state ***");

            ppgInstr.SetDefaultState();
            Assert.AreEqual(false, ppgInstr.PattGenEnabled);
            Assert.AreEqual(true, ppgInstr.ClockOutputsTrack);
            Assert.AreEqual(true, ppgInstr.DataOutputsTrack);
            Assert.AreEqual(0.5, ppgInstr.GetOutputVoltage(InstType_BertPattGenOutput.Data).Amplitude_V);

            InstType_BertRfCouplingMode mode = ppgInstr.GetOutputCoupling(InstType_BertPattGenOutput.Data);
            if (mode != InstType_BertRfCouplingMode.DcCoupledTerm_0V)
            {
                Assert.Fail ("PPG output coupling not reset to default of DC 0V");
            }
            Assert.AreEqual(50, ppgInstr.GetOutputCrossingPt_Percent(InstType_BertPattGenOutput.Data));

            InstType_BertOutputVoltageDef vDef = ppgInstr.GetOutputVoltage(InstType_BertPattGenOutput.Data);
            Assert.AreEqual(0.5, vDef.Amplitude_V);
            Assert.AreEqual(0, vDef.Offset_V);

            InstType_BertDataPatternType patternType = ppgInstr.PatternType;
            if (patternType != InstType_BertDataPatternType.Prbs)
            {
                Assert.Fail("Invalid defualt pattern type");
            }

            Assert.AreEqual (23, ppgInstr.PrbsLength);

            clockInstr.Frequency_MHz = 10709;
            Assert.AreEqual(10709, clockInstr.Frequency_MHz);

            clockInstr.Enabled = true;
            Assert.AreEqual(true, clockInstr.Enabled);


            //Enable the pattern Generator Output
            ppgInstr.PattGenEnabled = true;
            Assert.AreEqual(true, ppgInstr.PattGenEnabled);
            System.Threading.Thread.Sleep(2000);

            TestOutput("\n\n*** T01_CheckDefaultState: Confirmed PPG instrument set to defualt state ***");

            errInstr.SetDefaultState();
            
            errInstr.AutoSetup();
            Assert.AreEqual(false, errInstr.AnalyserEnabled);


            patternType = errInstr.PatternType;
            if (patternType != InstType_BertDataPatternType.Prbs)
            {
                Assert.Fail("Error Gernerator: Invalid default pattern type");
            }
            Assert.AreEqual(23, errInstr.PrbsLength);

            InstType_BertDataPolarity polarity = errInstr.Polarity;

            if (polarity != InstType_BertDataPolarity.Normal)
            {
                Assert.Fail("Error Gernerator: Invalid default Polarity");
            }

            TestOutput("\n\n*** T01_CheckDefaultState: Confirmed Error Detector instrument set to defualt state ***");
        }

        [Test]
        public void T02_ErrorFreeTrafficloopback()
        {
            TestOutput("\n\n*** T02_ErrorFreeTrafficloopback ***");
            errInstr.AnalyserStart();
            System.Threading.Thread.Sleep(5000);
            Assert.AreEqual(0, errInstr.ErrorRatio);
            Assert.AreEqual(0, errInstr.ErroredBits);
            errInstr.AnalyserStop();
        }

        [Test]
        public void T03_ChangePatternToPrbs31()
        {
            TestOutput("\n\n*** T03_ChangePatternToPrbs31 ***");
            ppgInstr.PrbsLength = 31;
            Assert.AreEqual(31, ppgInstr.PrbsLength);

            errInstr.PrbsLength = 31;
            Assert.AreEqual(31, errInstr.PrbsLength);

            //System.Threading.Thread.Sleep(10000);

            //errInstr.AutoSetup();

            errInstr.AnalyserStart();
            System.Threading.Thread.Sleep(5000);
            Assert.AreEqual(0, errInstr.ErrorRatio);
            Assert.AreEqual(0, errInstr.ErroredBits);
            errInstr.AnalyserStop();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

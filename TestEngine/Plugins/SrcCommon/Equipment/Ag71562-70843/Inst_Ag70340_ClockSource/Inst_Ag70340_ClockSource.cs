// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag70340_ClockSource.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 70340 RF Clock source Instrument Driver 
    /// </summary>
    public class Inst_Ag70340_ClockSource : Instrument, IInstType_RfClockSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag70340_ClockSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instr70340A = new InstrumentDataRecord(
                "HEWLETT-PACKARD 70340A",				// hardware name 
                "REV0.00",  			// minimum valid firmware version 
                "REV7.24");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag70340A", instr70340A);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag70340_Clocksource",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "1.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag70340_Clocksource", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag70340_Clocksource)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //Set default state with the output enabled, and also run a self test.
            instrumentChassis.Write("*CLS", this);
            instrumentChassis.Write("*RST", this);
            instrumentChassis.Write_Unchecked("*TST?", this);
            System.Threading.Thread.Sleep(30000);
            string resp = instrumentChassis.Read_Unchecked(this);

            if (resp != "+0")
            {
                throw new InstrumentException("Ag70340 Clock source failed self test");
            }

            this.Enabled = false;

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region IInstType_RfClockSource Members

        /// <summary>
        /// Set/Get the ouput amplitude of the clock in Volts. 
        /// Not supported by this instrument.
        /// </summary>
        public double Amplitude_Vrms
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Set/Get the ouput amplitude of the clock in dBm
        /// </summary>
        public double Amplitude_dBm
        {
            get
            {
                return Convert.ToDouble (instrumentChassis.Query("POW:LEV?" , this));
            }
            set
            {
                instrumentChassis.Write("POW:LEV " + value + " DBM", this);
            }
        }


        /// <summary>
        /// Set/Get Output Enable state of the clock.
        /// </summary>
        public bool Enabled
        {
            get
            {
                bool ret;
                string resp = instrumentChassis.Query("OUTP:STAT?", this).Trim();

                switch (resp)                
                {
                    case "+0": 
                        ret = false;
                        break;

                    case "+1":
                        ret = true;
                        break;

                    default:
                        throw new InstrumentException ("Invalid response to output state query command: " + resp);
                   
                }

                return ret;
            }
            set
            {
                if (value)
                {
                    //Enable output
                    instrumentChassis.Write("OUTP:STAT ON", this);
                }
                else
                {
                    //Disable Output
                    instrumentChassis.Write("OUTP:STAT OFF", this);
                }
            }
        }

        /// <summary>
        /// Get/Set the output frequency of the Clock in MHz
        /// </summary>
        public double Frequency_MHz
        {
            get
            {
                string resp = instrumentChassis.Query("FREQ?", this).Trim();
                return (Convert.ToDouble(resp) / 1000000);
            }
            set
            {
                //Instrument supports 1.0 - 20 GHz 
                if ((value >= 1000) && (value <= 20000))
                {
                    instrumentChassis.Write("FREQ " + value + "MHZ", this);
                }
                else
                {
                    throw new InstrumentException("Invalid Clock Frequency Specified : " + value);
                }
            }
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag70340_Clocksource instrumentChassis;
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_JDS_SC_OPTSW : InstType_Switch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JDS_SC_OPTSW(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware Unknown",				// hardware name 
                "Firmware Unknown",  			// minimum valid firmware version 
                "Firmware Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("JDS_SC_OPTSW", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JDS_SC_OPTSW",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_JDS_SC_OPTSW", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JDS_SC_OPTSW)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                //string idn = instrumentChassis.Query_Unchecked("IDN?", this);
                //string[] info = idn.Split(',');
                //opComplete();

                //return info[3].Trim();
                return "Firmware Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
               
                //string idn = instrumentChassis.Query_Unchecked("IDN?", this);
                //string[]info=idn.Split(',');
                //opComplete();

                //return info[1].Trim();
                return "Hardware Unknown";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    //instrumentChassis.Write_Unchecked("*CLS",this);
                    int max=this.InputMaximumPosition;
                    int min = this.InputMinimumPosition;
                }
            }
        }

        #endregion

        #region OPTICAL SWITCH InstrumentType property overrides

        public override int InputMaximumPosition
        {
            get
            {
                maxPosition= Convert.ToInt32 (instrumentChassis.Query_Unchecked("CLOSE" + base.Slot +"? MAX", this));
                opComplete();
                return maxPosition;
            }
        }

        public override int InputMinimumPosition
        {
            get
            {
                return 0;
            }
        }

        public override int InputSwitchPosition
        {
            get
            {
                currentPosition = Convert.ToInt16(instrumentChassis.Query_Unchecked("CLOSE" + base.Slot + "?", this));
                opComplete();
                return currentPosition;
            }
            set
            {
                if (value <= InputMaximumPosition)
                {
                    instrumentChassis.Write_Unchecked("CLOSE" + base.Slot + " " + value.ToString(), this);
                    opComplete();
                }
                else
                {
                    throw new InstrumentException("Switch position requested is outside the operational range of this instruments");
                }
            }
        }

        public override int OutputMaximumPosition
        {
            get
            {
                // just call the input switch position routine as they are the same
                return InputMaximumPosition;
            }
        }

        public override int OutputMinimumPosition
        {
            get 
            {
                return 0;
            }
        }

        public override int OutputSwitchPosition
        {
            get
            {
                // just call the input switch position routine as they are the same
                return InputSwitchPosition;
            }
            set
            {
                // just call the input switch position routine as they are the same
                InputSwitchPosition=value;
            }
        }


        private void opComplete()
        { 
            int comp=0,count=0;
            while(comp!=1)
            {
                comp= Convert.ToInt16(instrumentChassis.Query_Unchecked("OPC?",this));
                count++;
            }
        }



        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JDS_SC_OPTSW instrumentChassis;
        private int currentPosition, maxPosition;
        #endregion
    }
}

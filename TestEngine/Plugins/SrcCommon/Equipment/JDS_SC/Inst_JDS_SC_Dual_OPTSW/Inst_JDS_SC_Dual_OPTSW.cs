// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// JDS SC Optical Switch Driver
    /// </summary>
    public class Inst_JDS_SC_Dual_OPTSW : InstType_SimpleSwitch
    {
        #region Constructor

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JDS_SC_Dual_OPTSW(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware Unknown",				// hardware name 
                "Firmware Unknown",  			// minimum valid firmware version 
                "Firmware Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("JDS_SC_OPTSW", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JDS_SC_OPTSW",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_JDS_SC_OPTSW", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JDS_SC_OPTSW)chassisInit;
        }

        #endregion

        #region InstType_SimpleSwitch property Overridess
        /// <summary>
        /// Returns the Maximum Settable output Switch Position
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                string response = this.instrumentChassis.Query_Unchecked("CLOSE" + base.Slot + "? MAX", this);
                int maxPosition = Convert.ToInt32(response);
                opComplete();
                return maxPosition;
            }
        }

        /// <summary>
        /// Returns the Minimum Settable Output Switch Position
        /// </summary>
        public override int MinimumSwitchState
        {
            get { return 0; }
        }

        /// <summary>
        /// Sets / Gets the Output Switch Position
        /// </summary>
        public override int SwitchState
        {
            get
            {
                //found sometimes, it will return a string like 'N 3', retry then go pass
                //try to re query it if the return is not a single charater. do three times
                string response = "";
                int i = 0;

                response = this.instrumentChassis.Query_Unchecked("CLOSE" + base.Slot + "?", this);
                while (response.Length != 1 && i < 3)
                {
                    System.Threading.Thread.Sleep(200);
                    response = this.instrumentChassis.Query_Unchecked("CLOSE" + base.Slot + "?", this);
                    i++;
                }

                opComplete();
                return Convert.ToInt32(response);
            }
            set
            {
                if ((value <= this.MaximumSwitchState) && (value >= this.MinimumSwitchState))
                {
                    this.instrumentChassis.Write_Unchecked("CLOSE" + base.Slot + " " + value.ToString(), this);
                    opComplete();
                }
                else
                {
                    throw new InstrumentException("Switch position requested is outside the operational range of this instruments");
                }
            }
        }
        #endregion
        #region Instrument Property Overrides
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                //string idn = instrumentChassis.Query_Unchecked("IDN?", this);
                //string[] info = idn.Split(',');
                //opComplete();

                //return "S" + info[3].Trim();
                return "Firmware Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {

                //string idn = instrumentChassis.Query_Unchecked("IDN?", this);
                //string[]info=idn.Split(',');
                //opComplete();

                //return "H" + info[1].Trim();
                return "Hardware Unknown";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    //instrumentChassis.Write_Unchecked("*CLS",this);
                    //Add to clear device: Clear the SRQ mask register and the status register
                    instrumentChassis.Write_Unchecked("CLR", this);
                    opComplete(); // flush
                    int max = this.MaximumSwitchState;
                    int min = this.MinimumSwitchState;
                }
            }
        }

        #endregion

        #region Private member function


        private void opComplete()
        {
            int count = 0;
            string reply;
            do
            {
                //Add to clear device: Clear the SRQ mask register and the status register
                //instrumentChassis.Write_Unchecked("CLR", this);
                reply = instrumentChassis.Query_Unchecked("OPC?", this);
                if (reply[0] == '1')
                    break;
                System.Threading.Thread.Sleep(1000);
                count++;
            } while (reply[0] != '1');
        }



        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JDS_SC_OPTSW instrumentChassis;
        //private int currentPosition, maxPosition;
        #endregion
    }
}

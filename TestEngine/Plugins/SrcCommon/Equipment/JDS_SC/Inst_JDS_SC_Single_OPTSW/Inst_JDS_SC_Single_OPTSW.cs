// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// JDS SC Optical Switch Driver
    /// </summary>
    public class Inst_JDS_SC_Single_OPTSW : InstType_SimpleSwitch
    {
        #region Constructor

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_JDS_SC_Single_OPTSW(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware Unknown",				// hardware name 
                "Firmware Unknown",  			// minimum valid firmware version 
                "Firmware Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("JDS_SC_OPTSW", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_JDS_SC_OPTSW",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_JDS_SC_OPTSW", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_JDS_SC_OPTSW)chassisInit;
        }

        #endregion

        #region InstType_SimpleSwitch property Overrides

        /// <summary>
        /// Returns the Maximum Settable output Switch Position
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                string response = this.instrumentChassis.Query_Unchecked("CLOSE? MAX", this);
                int maxPosition = Convert.ToInt32(response);
                opComplete();
                return maxPosition;
            }
        }

        /// <summary>
        /// Returns the Minimum Settable Output Switch Position
        /// </summary>
        public override int MinimumSwitchState
        {
            get { return 0; }
        }

        /// <summary>
        /// Sets / Gets the Output Switch Position
        /// </summary>
        public override int SwitchState
        {
            get
            {
                string response = this.instrumentChassis.Query_Unchecked("CLOSE?", this);
                opComplete();
                return Convert.ToInt32(response);
            }
            set
            {
                if ((value <= this.MaximumSwitchState) && (value >= this.MinimumSwitchState))
                {
                    this.instrumentChassis.Write_Unchecked("CLOSE" + value.ToString(), this);
                    opComplete();
                }
                else
                {
                    throw new InstrumentException("Switch position requested is outside the operational range of this instruments");
                }
            }
        }

        #endregion

        #region Instrument Property Overrides

        /// <summary>
        /// Returns the firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get { return "Firmware Unknown"; }
        }

        /// <summary>
        /// Returns the Hardware Identity
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "Hardware Unknown"; }
        }

        /// <summary>
        /// Sets the instrument Default State
        /// </summary>
        public override void SetDefaultState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
            }
        }

        #endregion

        #region Private Data

        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_JDS_SC_OPTSW instrumentChassis;

        #endregion

        #region Private Member Functions

        private void opComplete()
        {
            int comp = 0, count = 0;
            while (comp != 1)
            {
                comp = Convert.ToInt16(this.instrumentChassis.Query_Unchecked("OPC?", this));
                count++;
            }
        }

        #endregion
    }
}

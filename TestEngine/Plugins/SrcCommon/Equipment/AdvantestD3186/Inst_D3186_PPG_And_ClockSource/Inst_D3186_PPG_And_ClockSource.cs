// [Copyright]
//
// Bookham Test Library
// Bookham.TestSolution.Instruments
//
// Inst_D3186_PPG_And_ClockSource.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Advantest D3186 PPG and RF Clock source Instrument Driver.
    /// </summary>
    public class Inst_D3186_PPG_And_ClockSource : Instrument, IInstType_BertPatternGen, IInstType_RfClockSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_D3186_PPG_And_ClockSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrD3186A04 = new InstrumentDataRecord(
              "ADVANTEST D3186",			// hardware name 
              "REV      A00",			            // minimum valid firmware version 
              "REV      A00");		        // maximum valid firmware version 
            ValidHardwareData.Add("D3186A04", instrD3186A04);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_D3186",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_D3186", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_D3186)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //Reset the instrument
            this.Reset();

            this.PattGenEnabled = false;

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online  
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_D3186 instrumentChassis;
        #endregion

        #region IInstType_BertPatternGen Members

        /// <summary>
        /// Get/set whether the clock outputs track
        /// </summary>
        public bool ClockOutputsTrack
        {
            get
            {
                bool ret;
                string resp = instrumentChassis.Query("CTRK?", this).Trim();

                if (resp == "CTRKOF")
                {
                    ret = false;
                }
                else if (resp == "CTRKON")
                {
                    ret = true;
                }
                else
                {
                    throw new InstrumentException("Invalid response from instrument on Clock Output Track query: " + resp);
                }

                return ret;
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write("CTRKON", this);
                }
                else
                {
                    instrumentChassis.Write("CTRKOFF", this);
                }
            }
        }

        /// <summary>
        /// Get/set whether the data outputs track
        /// </summary>
        public bool DataOutputsTrack
        {
            get
            {
                bool ret;
                string resp = instrumentChassis.Query("DTRK?", this).Trim();

                if (resp == "DTRKOF")
                {
                    ret = false;
                }
                else if (resp == "DTRKON")
                {
                    ret = true;
                }
                else
                {
                    throw new InstrumentException("Invalid response from instrument on Data Output Track query: " + resp);
                }

                return ret;
            }
            set
            {
                if (value)
                {
                    instrumentChassis.Write("DTRKON", this);
                }
                else
                {
                    instrumentChassis.Write("DTRKOFF", this);
                }
            }
        }

        /// <summary>
        /// Get Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Which mode</returns>
        public InstType_BertRfCouplingMode GetOutputCoupling(InstType_BertPattGenOutput output)
        {
            string resp;
            InstType_BertRfCouplingMode ret;
            string outputSpecifierString = getOutputSpecifierString(output);

            resp = instrumentChassis.Query(outputSpecifierString + "OM?", this).Trim();

            if (resp == outputSpecifierString + "AC")
            {
                ret = InstType_BertRfCouplingMode.AcCoupled;
            }
            else if (resp == outputSpecifierString + "GND")
            {
                ret = InstType_BertRfCouplingMode.DcCoupledTerm_0V;
            }
            else if (resp == outputSpecifierString + "M2V")
            {
                ret = InstType_BertRfCouplingMode.DcCoupledTerm_minus2V;
            }
            else
            {
                throw new InstrumentException("Invalid response to Coupling Termination Voltage query: " + resp);
            }       

            return ret;
        }

        /// <summary>
        /// Set Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="mode">Which mode</param>
        public void SetOutputCoupling(InstType_BertPattGenOutput output, InstType_BertRfCouplingMode mode)
        {
            string outputSpecifierString = getOutputSpecifierString(output);
            string modeString = "";

            switch (mode)
            {
                case InstType_BertRfCouplingMode.AcCoupled:
                    modeString = "AC";
                    break;

                case InstType_BertRfCouplingMode.DcCoupledTerm_0V:
                    modeString = "GND";
                    break;

                case InstType_BertRfCouplingMode.DcCoupledTerm_minus2V:
                    modeString = "M2V";
                    break;
            }

            instrumentChassis.Write(outputSpecifierString + modeString, this);
        }


        /// <summary>
        /// Get Crossing Point percentage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Crossing Point percentage</returns>
        public double GetOutputCrossingPt_Percent(InstType_BertPattGenOutput output)
        {
            string outputSpecifierString = getOutputSpecifierString (output);
            //Return the result of query. The query returns a header and a value, separated by a spcae character. 
            // Split off the header.
            return Convert.ToDouble(this.instrumentChassis.Query(outputSpecifierString + "CRP?", this).Split(' ')[1]);
        }

        /// <summary>
        /// Set Crossing Point percentage for the specified output. 
        /// This instrument only supports Integer Percentages between 20 and 80%
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="percent">Crossing Point percentage</param>
        public void SetOutputCrossingPt_Percent(InstType_BertPattGenOutput output, double percent)
        {
            if ((percent >= 20) & (percent <= 80))
            {
                //Convert percentage into an integer, as the instrument only supports inegral percentages.
                int intPercent = (int)percent;

                //Get the output specifier
                string outputSpecifierString = getOutputSpecifierString(output);
                
                //Set the crossing point percentage.
                this.instrumentChassis.Write(outputSpecifierString + "CRP " + intPercent, this);
            }
            else
            {
                throw new Exception("Invalid crossing Point Percentage Specified: " + percent);
            }
        }

        /// <summary>
        /// Get Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Output Voltage</returns>
        public InstType_BertOutputVoltageDef GetOutputVoltage(InstType_BertPattGenOutput output)
        {
            InstType_BertOutputVoltageDef ret;

            //Get the output specifier
            string outputSpecifierString = getOutputSpecifierString(output);

            //Extract Voltages from response string - instrument returns a header and value separated by a space char.
            //Extract the header to get at the value from the response.
            ret.Amplitude_V = Convert.ToDouble(this.instrumentChassis.Query(outputSpecifierString + "AMP?", this).Trim().Split(' ')[1]);
            ret.Offset_V = Convert.ToDouble(this.instrumentChassis.Query(outputSpecifierString + "OFF?", this).Trim().Split(' ')[1]);

            return ret;
        }


        /// <summary>
        /// Set Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="vDef">Voltage setup structure</param>
        public void SetOutputVoltage(InstType_BertPattGenOutput output, InstType_BertOutputVoltageDef vDef)
        {
            //Check that valid voltages are given  for the specified output, given it's current output coupling mode.
            InstType_BertRfCouplingMode mode = GetOutputCoupling(output);
            switch (mode)
            {
                case InstType_BertRfCouplingMode.AcCoupled:
                case InstType_BertRfCouplingMode.DcCoupledTerm_0V:
                    if ((vDef.Amplitude_V < 0.5) || (vDef.Amplitude_V > 2))
                    {
                        string error = "Invalid Output Voltage Specified: " + vDef.Amplitude_V + " Valid range is 0.5 to 2.0 Volts when output coupling is AC or 0V";
                        throw new InstrumentException(error);
                    }
                    break;

                case InstType_BertRfCouplingMode.DcCoupledTerm_minus2V:
                    if ((vDef.Offset_V < -1) || (vDef.Offset_V > -0.6))
                    {
                        string error = "Invalid Output Voltage Offset Specified: " + vDef.Offset_V + " Valid range is -1.0 to -0.6 Volts when output coupling is -2V";
                        throw new InstrumentException(error);
                    }
                    break;
            }

            //Make sure we specify our values to 2 decimal places.
            decimal voltage = (decimal)vDef.Amplitude_V;
            string voltageString = Decimal.Round (voltage, 2).ToString();

            decimal offset = (decimal)vDef.Offset_V;
            string offsetString = Decimal.Round (offset, 2).ToString();

            //Write the Voltage Amplitude & Offset to the instrument
            string outputSpecifierString = getOutputSpecifierString(output);
            this.instrumentChassis.Write(outputSpecifierString + "AMP " + voltageString, this);
            this.instrumentChassis.Write(outputSpecifierString + "OFF " + offsetString, this);
        }

        /// <summary>
        /// Get/set Pattern Generator state (true:enabled, false:disabled)
        /// </summary>
        public bool PattGenEnabled
        {
            get
            {
                string resp = this.instrumentChassis.Query("OUT?", this).Trim();
                if (resp == "OUTON")
                {
                    return true;
                }
                else if (resp == "OUTOF")
                {
                    return false;
                }
                else
                {
                    throw new InstrumentException("Invalid response to Patt Gen Enabled Query");
                }
            }
            set
            {
                if (value)
                {
                    //Enable output
                    this.instrumentChassis.Write("OUTON", this);
                }
                else
                {
                    //Disable output
                    this.instrumentChassis.Write("OUTOF", this);
                }
            }
        }


        /// <summary>
        /// Get/set the Pattern type.
        /// In this version of the driver, only PRBS Patterns shall be supported.
        /// </summary>
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                string resp = this.instrumentChassis.Query("PM?", this);
                if (resp == "PRBS")
                {
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }
            }
            set
            {
                if (value != InstType_BertDataPatternType.Prbs)
                {
                    throw new InstrumentException("Only PRBS Patter Type supported by this driver");
                }
                else
                {
                    this.instrumentChassis.Write("PRBS", this);
                }
            }
        }


        /// <summary>
        /// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        /// </summary>
        public int PrbsLength
        {
           get
            {
                string resp = this.instrumentChassis.Query("PB?", this).Trim();
                if (resp == "PB 07,0")
                {
                    return 7;
                }
                else if (resp == "PB 09,0")
                {
                    return 9;
                }
                else if (resp == "PB 10,0")
                {
                    return 10;
                }
                else if (resp == "PB 11,0")
                {
                    return 11;
                }
                else if (resp == "PB 15,0")
                {
                    return 15;
                }
                else if (resp == "PB 23,0")
                {
                    return 23;
                }
                else if (resp == "PB 31,0")
                {
                    return 31;
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length recieed from instrument: " + resp);
                }
            }
            set
            {
                if ((value == 7) || (value == 9) || (value == 10) || (value == 11)
                    || (value == 15) || (value == 23) || (value == 31))
                {
                    if ((value == 7) || (value == 9))
                    {
                        this.instrumentChassis.Write("PB 0" + value, this);
                    }
                    else
                    {
                        this.instrumentChassis.Write("PB " + value, this);
                    }
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length Specified : " +  value);
                }
            }
        }

        /// <summary>
        /// Get/Set user defined pattern. Not supported by this version of the driver.
        /// </summary>
        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Get/Set user defined pattern length. Not supported by this version of the driver.
        /// </summary>
        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region IInstType_RfClockSource Members

        /// <summary>
        /// Set/Get the ouput amplitude of the clock in Volts. 
        /// On this instrument, the amplitude is fixed at 1Vp-p.
        /// Setting to anything other than this is not supported.
        /// </summary>
        public double Amplitude_Vrms
        {
            get
            {
                //Fixed on this instrument to 1 Vp-p
                return 1;
            }
            set
            {
                //Cannot really set this on this instrument. allow it to be set to 1 (whic it is fixed at), but nothing else.
                if (value != 1)
                {
                    throw new Exception("Clock amplitude is fixed at 1Vp-p on this instrument. Attempted to set: " + value + "V");
                }
            }
        }

        /// <summary>
        /// Set/Get the output amplitude of the clock in dBm. Not suppored by this instrument.
        /// </summary>
        public double Amplitude_dBm
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Set/Get Output Enable state of the clock.
        /// Clock output cannot be disabled on this instrument.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return true;
            }
            set
            {
                if (!value)
                {
                    throw new InstrumentException("Disabling Clock Source output not supported by this instrument"); 
                }
            }
        }

        /// <summary>
        /// Get/Set the output frequency of the Clock in MHz
        /// </summary>
        public double Frequency_MHz
        {
            get
            {
                //Response is frequency in HZ
                double resp = Convert.ToDouble (this.instrumentChassis.Query("CR?", this).Trim().Split(' ')[1]);

                //Return the frequncy in MHz
                return (resp/1000000);
            }
            set
            {
                if ((value >= 150) && (value <= 12000))
                {
                    this.instrumentChassis.Write("CR " + value, this);
                }
                else
                {
                    throw new InstrumentException("Attempt to set clock source to invalid Frequency: " + value + "MHz"); 
                }
            }
        }

        #endregion

        #region Private Helper functions
        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        private void Reset()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("C", this);

            //Wait 2 seconds.
            Thread.Sleep(2000);

            // Start the reset
            instrumentChassis.Write_Unchecked("Z", this);

            //Wait 11 seconds (the manual says it takes approx 10 seconds to reset).
            Thread.Sleep(11000);
        }

        /// <summary>
        /// Get the PPG Output Specifier String
        /// </summary>
        /// <param name="output">The Output</param>
        /// <returns>The Output specifier string</returns>
        private string getOutputSpecifierString(InstType_BertPattGenOutput output)
        {
            string outputSpecifierString = "";

            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    outputSpecifierString = "C";
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    outputSpecifierString = "CB";
                    break;

                case InstType_BertPattGenOutput.Data:
                    outputSpecifierString = "D";
                    break;

                case InstType_BertPattGenOutput.DataBar:
                    outputSpecifierString = "DB";
                    break;
            }

            return outputSpecifierString;
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_D3186.cs
//
// Author: Joseph Olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;
using System.Threading;


namespace Bookham.TestLibrary.ChassisNS
{
   /// <summary>
   /// Advantest D3186 PPG Chassis Driver
   /// </summary>
   public class Chassis_D3186 : ChassisType_Visa
   {
      #region Constructor
      /// <summary>
      /// Chassis Constructor.
      /// </summary>
      /// <param name="chassisNameInit">Chassis name</param>
      /// <param name="driverNameInit">Chassis driver name</param>
      /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
      public Chassis_D3186(string chassisNameInit, string driverNameInit,
          string resourceString)
         : base(chassisNameInit, driverNameInit, resourceString)
      {
         // Setup expected valid hardware variants 
         ChassisDataRecord D3186A04Data = new ChassisDataRecord(
             "ADVANTEST D3186",			// hardware name 
             "REV      A00",			            // minimum valid firmware version 
             "REV      A00");		        // maximum valid firmware version 
         ValidHardwareData.Add("D3186A04", D3186A04Data);
      }
      #endregion

      #region Chassis overrides
      /// <summary>
      /// Firmware version of this chassis.
      /// </summary>
      public override string FirmwareVersion
      {
          get
          {
              // Read the chassis ID string
              string idn = Query_Unchecked("IDN?", null);

              // Return the firmware version in the 3rd comma seperated field
              return idn.Split(',')[2].Trim();
          }
      }

      /// <summary>
      /// Hardware Identity of this chassis.
      /// </summary>
      public override string HardwareIdentity
      {
          get
          {
              // Read the chassis ID string and split the comma seperated fields
              string[] idn = Query_Unchecked("IDN?", null).Split(',');

              // Return field1, the manufacturer name and field 2, the model number
              return idn[0] + " " + idn[1];
          }
      }

      /// <summary>
      /// Setup the chassis as it goes online
      /// </summary>
      public override bool IsOnline
      {
         get
         {
            return base.IsOnline;
         }
         set
         {
            // setup base class
            base.IsOnline = value;

            if (value) // if setting online                
            {
                //Reset the instrument
                this.Write_Unchecked("C", null);

                //Mask the Status byte from raising SRQs.
                //bit0: Unused
                //bit1: Syntax error - 1 if in error
                //bit2: Unused
                //bit3: Unused
                //bit4: Unused
                //bit5: Unused
                //bit6; RQS - always the same as bit1 
                //bit7: BUSY bit: Always set when operating delay line, 
                //      floppy disk and transferring word pattern to generation circuit. 0 otherwise.
               

                //Clear the status byte register.
                //this.Write_Unchecked("CSB", null);                                           
            }
         }
      }


      /// <summary>
      /// Writes a command to the chassis and will wait for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
      public void Write(string command, Instrument i)
      {
          this.Write(command, i, false, true);
      }

      /// <summary>
      /// Writes a command to the chassis. Dependent on options given this will wait 
      /// for completion and check status
      /// byte and other registers for error conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
      /// <param name="errorCheck">If async set to false, then this parameter determines
      /// whether errors are checked for</param>
      public void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
      {
          this.Write_Unchecked(command, i);
          if (asyncSet || !errorCheck) return;

          //Wait 100ms for the command to take effect (arbitary).
          Thread.Sleep(50);

          //Check for syntax error
          if (IsErrorInStatusByte())
          {
              //Syntax error. Raise an exception
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }
      }

      /// <summary>
      /// Sends a command to the chassis and waits for a response. Dependent on 
      /// arguments, will check the status byte and other registers for error 
      /// conditions.
      /// </summary>
      /// <param name="command">Command to send</param>
      /// <param name="i">Which instrument is this from?</param>
      /// <returns>string response</returns>
      public string Query(string command, Instrument i)
      {
          string resp = this.Query_Unchecked(command, i);

          if (IsErrorInStatusByte())
          {
              // collate information about the error from the chassis and throw 
              // a ChassisException
              throw new ChassisException("Status Register indicates syntax in  command: " + command);
          }

          return resp;
      }

       /// <summary>
       /// Returns the Status Byte from the instrument.
       /// </summary>
       /// <returns>The status byte.</returns>
       public byte QueryStatusByte()
       {
           return (byte)this.GetStatusByte();
       }
      #endregion

      #region helper methods

      /// <summary>
      /// Query the status byte register and return true if an error 
      /// condition is signalled.
      /// </summary>
      /// <returns>true if "Syntax error" bit is set</returns>
       private bool IsErrorInStatusByte()
       {
           byte statusByte = (byte)this.GetStatusByte();

           //Bit 1 = 1, indicates a command syntax error.
           if ((statusByte & 0x02) != 0)
           {
               //Syntax error.
               return true;
           }
           else
           {
               return false;
           }
       }
      #endregion
  }
}

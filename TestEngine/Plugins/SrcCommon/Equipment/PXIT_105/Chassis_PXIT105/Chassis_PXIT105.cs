// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// ChassisPXIT_105_Actuator.cs
//
// Author: paul.annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for PXIT 105 Actuator card
    /// </summary>
    public class Chassis_PXIT105 : ChassisType_PXI
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_PXIT105(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {

            // Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "PXIT 105",			// hardware name 
                "0",			// minimum valid firmware version 
                "9999");   // maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);
        }
        #endregion        

        #region Chassis overrides
        /// <summary>
        /// PXIT Card firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            {
                Int16 cardVersion = PXITSwitchImport.PXITGetCardVersion(VisaSessionHandle);
                return cardVersion.ToString();
            }
        }

        /// <summary>
        /// PXIT Card hardware identity
        /// </summary>
        public override string HardwareIdentity
	    {
            get
            {
                Int16 cardType = PXITSwitchImport.PXITGetCardType(VisaSessionHandle);
                // card model type is actually coded in Hex! (as specified in PXIT 305 user manual) 
                // i.e. PXIT 305 is returned as 0x305 (decimal 775)
                string cardTypeStr = string.Format("PXIT {0:X}", cardType);
                return cardTypeStr;
            }
        }
               

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // basic check for valid driver installation
                    try
                    {
                        // just query the DLL versions (no card required!)
                        double d1 = PXITSwitchImport.PXITVersion();
                        double d2 = PXITSwitchImport.PXITRelayVersion();
                    }
                    catch (Exception e)
                    {
                        throw new ChassisException("PXIT 105 Driver DLL Installation Missing/Corrupt", e);
                    }                 
                }
            }
        }

        /// <summary>
        /// Is the specified relay closed?
        /// </summary>
        /// <param name="relayNbr">Relay number</param>
        /// <returns>True=Closed, False=Open</returns>      
        public bool RelayClosed(byte relayNbr)
        {
            Int16 relayStatus = PXITSwitchImport.PXITRelayStatus(VisaSessionHandle, relayNbr);
            if (relayStatus == 3) return true;
            else if(relayStatus == 2) return false;
            else 
            {
                string errMsg = String.Format("Invalid Relay #{0} status: {1}",
                    relayNbr, relayStatus);
                throw new ChassisException(errMsg);    
            }
        }

        /// <summary>
        /// Close Relay
        /// </summary>
        /// <param name="relayNbr">Which relay to close</param>
        public void CloseRelay(byte relayNbr)
        {
            Int16 relayStatus = PXITSwitchImport.PXITCloseRelay(VisaSessionHandle, relayNbr);
            if (relayStatus!=0)
            {
                string errMsg = String.Format("Invalid Relay #{0} status: {1}",
                    relayNbr, relayStatus);
                throw new ChassisException(errMsg);
            }            
        }

        /// <summary>
        /// Open Relay
        /// </summary>
        /// <param name="relayNbr">Which relay to close</param>
        public void OpenRelay(byte relayNbr)
        {
            Int16 relayStatus = PXITSwitchImport.PXITOpenRelay(VisaSessionHandle, relayNbr);
            if (relayStatus != 0)
            {
                string errMsg = String.Format("Invalid Relay #{0} status: {1}",
                    relayNbr, relayStatus);
                throw new ChassisException(errMsg);
            }
        }
        

        #endregion
    }
}

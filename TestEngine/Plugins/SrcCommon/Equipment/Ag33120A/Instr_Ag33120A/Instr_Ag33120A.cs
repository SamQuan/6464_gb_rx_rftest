// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_Ag33120A.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// TODO: 
    /// 1. Add reference to the Instrument type you are implementing.
    /// 2. Add reference to the Chassis type you are connecting via.
    /// 3. Change the base class to the type of instrument you are implementing.
    /// 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    /// 5. Fill in the gaps.
    /// </summary>
    public class Instr_Ag33120A : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_Ag33120A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_Ag33120A = new InstrumentDataRecord(
                "HEWLETT-PACKARD 33120A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "8.0-5.0-1.0");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33120A", instr_Ag33120A);

            InstrumentDataRecord instr_Ag33120A2 = new InstrumentDataRecord(
                "HEWLETT-PACKARD 33120A",				// hardware name 
                "0",  			        // minimum valid firmware version 
                "7.0-5.0-1.0");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33120A2", instr_Ag33120A2);

            InstrumentDataRecord instr_Ag33511B = new InstrumentDataRecord(
   "Agilent Technologies 33511B",				// hardware name 
   "0",  			        // minimum valid firmware version 
   "3.03-1.19-2.00-52-00");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33511B", instr_Ag33511B);

            InstrumentDataRecord instr_Ag33511B2 = new InstrumentDataRecord(
  "Agilent Technologies 33511B",				// hardware name 
  "0",  			        // minimum valid firmware version 
  "3.05-1.19-2.00-52-00");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33511B2", instr_Ag33511B2);


            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassis_33120A = new InstrumentDataRecord(
                "Chassis_Ag33120A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.1");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag33120A", chassis_33120A);


            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Ag33120A)chassisInit;

            this.cmdFreq = "FREQ";
            this.cmdAmplitude = "VOLT";
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[3].Trim();
               
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[0] + " " + idnArray[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //throw new InstrumentException("The method or operation is not implemented.");
            this.WaveFunction = WaveType.SIN;
            this.Amplitude_mV = 1000;
            this.DcOffset_Volt = 0.5;
            this.OutputEnableSysnc = true;
            this.OutputEnabled = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion


        #region signal generater

        /// <summary>
        /// set/get wave function
        /// </summary>
        public WaveType WaveFunction
        {
            get
            {
                string command = "SOURce:FUNCtion:SHAPe?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                WaveType waveFunction = (WaveType)Enum.Parse(typeof(WaveType), str);
                return waveFunction;
            }
            set
            {
                string command = "SOURce:FUNCtion:SHAPe " + Enum.GetName(typeof(WaveType), value);
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// set/get dc offset  
        /// </summary>
        public double DcOffset_Volt
        {
            get
            {
                string command = "VOLT:OFFS?";
                string str = this.instrumentChassis.Query_Unchecked(command, this);
                double dcOffset = double.Parse(str);
                return dcOffset;
            }
            set
            {
                string command = "VOLT:OFFS " + value.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        /// <summary>
        /// Get or Set the Frequency
        /// </summary>
        public double Frequency_Khz
        {
            get
            {
                string command = this.cmdFreq + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                double value = double.Parse(res);
                frequency = value / 1000;
                return frequency;
            }
            set
            {
                frequency = value;
                string command = this.cmdFreq + " " + frequency + "KHZ";
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Get or Set the Amplitude(Vpp)
        /// </summary>
        public double Amplitude_mV
        {
            get
            {
                string command = this.cmdAmplitude + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                double value = double.Parse(res);
                amplitude = value * 1000;
                return amplitude;
            }
            set
            {
                amplitude = value;
                string command = this.cmdAmplitude + " " + amplitude + "MVPP";
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool OutputEnableSysnc
        {
            get
            {
                string command = "OUTP:SYNC?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                if (res.Trim() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                string temp;
                if (value)
                {
                    temp = "ON";
                }
                else
                {
                    temp = "OFF";
                }
                string command = "OUTP:SYNC " + temp;
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// not support this function
        /// </summary>
        public bool OutputEnabled
        {
            get
            {
                throw new InstrumentException("not support this function");
            }
            set
            {
                //throw new InstrumentException("not support this function");
                if (!HardwareIdentity.Contains("33120A"))
                {
                    string temp;
                    if (value)
                    {
                        temp = "ON";
                    }
                    else
                    {
                        temp = "OFF";
                    }
                    string command = "OUTP " + temp;
                    this.instrumentChassis.Write_Unchecked(command, this);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void SetTriggerToBus()
        {
            string command = "TRIG:SOUR BUS";
            this.instrumentChassis.Write_Unchecked(command, this);

        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        public enum WaveType
        {   
            /// <summary>
            /// sinusoid wave
            /// </summary>
            SIN,
            /// <summary>
            /// square wave
            /// </summary>
            SQU,
            /// <summary>
            /// ramp wave
            /// </summary>
            RAMP,
            /// <summary>
            /// triangle wave
            /// </summary>
            TRI,

            /// <summary>
            /// noise wave
            /// </summary>
            NOIS,

            /// <summary>
            /// DC wave
            /// </summary>
            DC,

            /// <summary>
            /// user define wave
            /// </summary>
            USER
        }


        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag33120A instrumentChassis;

        private double frequency;
        private double amplitude;
        
        #endregion

        #region command area

        private string cmdFreq;
        private string cmdAmplitude;

        #endregion
    }
}

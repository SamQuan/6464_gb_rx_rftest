using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Test_Ag33120A
{
    class TestObject
    {
        public void Init()
        {
            address = "GPIB0::13::INSTR";

            chassis = new Chassis_Ag33120A("Ag33120A", "Chassis_Ag33120A", address);
            instrument = new Instr_Ag33120A("Inst_33120A", "Instr_Ag33120A", "", "", chassis);

            chassis.IsOnline = true;
            instrument.IsOnline = true;
        }

        public void Run()
        {
            instrument.OutputEnableSysnc = true;

            instrument.WaveFunction = Instr_Ag33120A.WaveType.SIN;
            instrument.DcOffset_Volt = 0.5;
            
            instrument.Frequency_Khz = 70;
            double freq = instrument.Frequency_Khz;

            instrument.Amplitude_mV = 500;
            double amplitude = instrument.Amplitude_mV;
        }

        public void shutDown()
        {
            
            instrument.OutputEnableSysnc = false;
            //instrument.OutputEnabled = false;
            instrument.IsOnline = false;
            chassis.IsOnline = false;
            
        }

        #region private parameter

        private Chassis_Ag33120A chassis;
        private Instr_Ag33120A instrument;

        private string address;

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ag33120A.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    /// <summary>
    /// ag33120A Chassis Driver
    /// </summary>
    public class Chassis_Ag33120A : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ag33120A(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassis_Ag33120A = new ChassisDataRecord(
                "HEWLETT-PACKARD 33120A",			// hardware name 
                "0",			// minimum valid firmware version 
                "8.0-5.0-1.0");		// maximum valid firmware version 
            ValidHardwareData.Add("Ag33120A", chassis_Ag33120A);

            ChassisDataRecord chassis_Ag33120A2 = new ChassisDataRecord(
                "HEWLETT-PACKARD 33120A",			// hardware name 
                "0",			// minimum valid firmware version 
                "7.0-5.0-1.0");		// maximum valid firmware version 
            ValidHardwareData.Add("Ag33120A2", chassis_Ag33120A2);

            ChassisDataRecord chassis_Ag33500B = new ChassisDataRecord(
               "Agilent Technologies 33511B",			// hardware name 
               "0",			// minimum valid firmware version 
               "3.03-1.19-2.00-52-00");		// maximum valid firmware version 
            ValidHardwareData.Add("Ag33500B", chassis_Ag33500B);

            ChassisDataRecord chassis_Ag33500B2 = new ChassisDataRecord(
              "Agilent Technologies 33511B",			// hardware name 
              "0",			// minimum valid firmware version 
              "3.05-1.19-2.00-52-00");		// maximum valid firmware version 
            ValidHardwareData.Add("Ag33500B2", chassis_Ag33500B2);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                // TODO: Update
                string command = "*IDN?";
                string res = Query_Unchecked(command,null);
                string[] idnArray = res.Split(',');
                return idnArray[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = Query_Unchecked(command, null);
                string[] idnArray = res.Split(',');
                return idnArray[0] + " " + idnArray[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                try
                {
                    base.IsOnline = value;
                }
                catch (Exception ex)
                {
                    string m = ex.ToString();
                }
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Th2800_EnvironChamber.cs
//
// Author: brendan.kavanagh, 2006
// Design: 1. Thermotron 2800 Programmer Computer Interface Manual (1995).
//         2. Thermotron 2800 Driver DD issue 0.1 (Paul Annetts, 2006).

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes; // Chassis_Therm2800 and added ref - btk 

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument class for Thermotron 2800 Environmental Chamber.
    /// </summary>
    public class Inst_Th2800_EnvironChamber : InstType_SimpleTempControl
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Th2800_EnvironChamber(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord therm2800HwData = new InstrumentDataRecord(
                "2800",				// hardware name (plucked from DID cmd) 
                "0",  			// minimum valid firmware version 
                "V2.05.00");	// maximum valid firmware version (plucked from DID cmd) 
            ValidHardwareData.Add("ThermotronHw", therm2800HwData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Therm2800",						// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Therm2800", chassisInfo);

            // initialise this instrument's chassis
            // casting it to the appropriate type.
            this.instrumentChassis = (Chassis_Therm2800)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state - a safe set-point temperature of 20.0 C and "OFF".
        /// </summary>
        public override void SetDefaultState()
        {
            this.SensorTemperatureSetPoint_C = 20.0; // set to room temperature of twenty degrees C
            this.OutputEnabled = false; // switch oven off
           
        }

        /// <summary>
        /// Setup the instrument as it goes online (value is just cached).
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
            }
        }
        #endregion

        #region InstType_EnvironChamber Extensions

        /// <summary>
        /// Get the actual temperature (in degrees C). 
        /// </summary>
        public override double SensorTemperatureActual_C
        {
            get
            {   // Send temperature variable read command (pg 3-4): 
                string tempString = this.GetCmd("D1V"); // "Dump chan 1 temp Variable"
                return Double.Parse(tempString, System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Get/set the temperature set-point (in degrees C).
        /// </summary>
        public override double SensorTemperatureSetPoint_C
        {
            get
            {
                string setPointTemp = this.GetCmd("D1S"); // Dump chan 1 temp Setpoint (Ref. 1 pg 3-4)
                return Double.Parse(setPointTemp, System.Globalization.CultureInfo.InvariantCulture);
            }
            set
            {
                // The instrument can cope with decimal point & exponent formats.
                // Use command 'Load chan # 1 with Setpoint 'x' degrees C' (Ref. 1 pg 3-7)
                string valAsString = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                string cmdString = "L1S" + valAsString; // Load chan # 1 with Setpoint + value.
                this.SetCmd(cmdString);
            }
        }                

        /// <summary>
        /// Get/set the state of the environmental chamber (on/off).
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Send Status byte query command:
                string statusString = this.GetCmd("DST");

                byte statusByte = Byte.Parse(statusString);
                statusByte = (byte)((int)statusByte & 0x03); // Mask off irrelevant bits

                bool state;

                if (statusByte == 0) // the "Stop" state (Ref. 1 pg 5-2)
                {
                    state = false;
                }
                else if (statusByte == 2) // the "Run Manual" state (Ref. 1 pg 5-2)
                {
                    state = true;
                }
                else
                {
                    throw new InstrumentException("Unexpected status (3 bits): " +
                        statusByte.ToString(System.Globalization.CultureInfo.InvariantCulture));
                }

                return state;
            }
            set
            {
                if (value == false)
                {
                    // Then send the "Stop" control command (Ref. 1 pg 3-2):
                    this.SetCmd("S");
                }
                else if (value == true) // turn oven "on"
                {
                    // Then send the "Run Manual" control command (Ref. 1 pg 3-2)(see example pg 6-3):
                    this.SetCmd("RM");
                }                
            }
        }
                
        #endregion
        // end of InstType_EnvironChamber Extensions

        #region Private data and helper functions

        /// <summary>
        /// "Set" command - write to the chassis to change its state.
        /// It follows the write command with a status query and throws an exception if bad.
        /// </summary>
        /// <param name="command">Command to send</param>
        private void SetCmd(string command)
        {
            this.instrumentChassis.Write_Unchecked_Wrapped(command, this);
            this.instrumentChassis.CheckErrorStatus(); // throw if error detected 
            // Write a log:
            base.LogEvent("Wrote: " + command);
        }

        /// <summary>
        /// "Get" command - read some information from the chassis.
        /// It follows the read command with a status query and throws an exception if bad.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <returns>response string</returns>
        private string GetCmd(string command)
        {
            string response = this.instrumentChassis.Query_Unchecked_Wrapped(command, this);
            this.instrumentChassis.CheckErrorStatus(); // throw if error detected
            // Write a log:
            base.LogEvent("Read command: " + command + " Response: " + response);
            return response;
        }

        /// <summary>
        /// Instrument's chassis instance.
        /// </summary>
        private Chassis_Therm2800 instrumentChassis;
        #endregion

        #region Thermontron Command Guide
            // PAW 25-09-2006
            // I    Initialise
            // DRV  Get Humidity
            // DRS  Get Humidity Set Point
            // DTS  Get Temp Set Point
            // D1V  Get Temp
            // DIN  Get Interval Number
            // DTL  Get Interval time left
            // R    Run Program from Hold
            // RP5  RunProgram & Program number 5
            // RM   Run Porgram Manual
            // RP   Return to program
            // S    Stop Program
            // H    Hold Program
            // CB   Clear Buffer
            // P    Print Command
            // L1S  Set Temp
            // LPM  Load Program
            // LPI  Load Program Interval
            // LAX  Set Auxiliarys
            // LSMO Disable SRQ
            // LKS1 Enable Lockout
            // LKSO Disable Lockout
            // LRT5 Set System Clock 5
        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Therm2800_Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Therm2800_Test test = new Therm2800_Test();
            test.Setup();
            DateTime start = DateTime.Now;
            try
            {
                //test.T01_Versions();
                test.T01_ChassisFunction();
                test.T02_InitInstrumentFunction();
                test.T03_RunInstrumentFunction();
   
            }
            finally
            {
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            Environment.Exit(0);
        }
    }
}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using System.Threading;

namespace TEST.BkhmBert
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_BkhmMMS testChassis;
        private Inst_BkhmMMSBert bertInstr;
        private Inst_BkhmMMSXfpHost xfpHostInstr;
        string chassisVisaResourceString = "COM2";
        string bertCardId = "11";
        string xfpHostCardId = "7";
        #endregion
        
        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_BkhmMMS("BERT Chassis", "Chassis_BkhmMMS", chassisVisaResourceString);
            TestOutput(testChassis, "Created OK");
            testChassis.EnableLogging = true;

            // create instrument objects            
            bertInstr = new Inst_BkhmMMSBert("BERT Instrument",
                "Inst_BkhmMMSBert", bertCardId, "", testChassis);            
            TestOutput(bertInstr, "Created OK");
            bertInstr.EnableLogging = true;

            xfpHostInstr = new Inst_BkhmMMSXfpHost("XFP Host",
                "Inst_BkhmMMSXfpHost", xfpHostCardId, "", testChassis);
            TestOutput(xfpHostInstr, "Created OK");
            xfpHostInstr.EnableLogging = true;

            // put them online
            testChassis.IsOnline = true;
            TestOutput(testChassis, "IsOnline set true OK");
            bertInstr.IsOnline = true;
            TestOutput(bertInstr, "IsOnline set true OK");
            xfpHostInstr.IsOnline = true;
            TestOutput(xfpHostInstr, "IsOnline set true OK");                   
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(testChassis, "Driver: " + testChassis.DriverName + ":" + testChassis.DriverVersion);
            TestOutput(testChassis, "HW: " + testChassis.HardwareIdentity);
            TestOutput(testChassis, "FW: " + testChassis.FirmwareVersion);
            TestOutput(bertInstr, "Driver: " + bertInstr.DriverName + ":" + bertInstr.DriverVersion);
            TestOutput(bertInstr, "HW: " + bertInstr.HardwareIdentity);
            TestOutput(bertInstr, "FW: " + bertInstr.FirmwareVersion);
            TestOutput(xfpHostInstr, "Driver: " + xfpHostInstr.DriverName + ":" + xfpHostInstr.DriverVersion);
            TestOutput(xfpHostInstr, "HW: " + xfpHostInstr.HardwareIdentity);
            TestOutput(xfpHostInstr, "FW: " + xfpHostInstr.FirmwareVersion);      
        }

        [Test]
        public void T02_BERT_Test()
        {
            int delay_ms = 1000;
            TestOutput("\n\n*** T02_BERT_Test ***");
            TestOutput(bertInstr, "Freq=" + bertInstr.Frequency_MHz.ToString());
            bertInstr.Frequency_MHz = 9953;
            TestOutput(bertInstr, "Freq=" + bertInstr.Frequency_MHz.ToString());
            System.Threading.Thread.Sleep(delay_ms);
            bertInstr.Frequency_MHz = 10703;
            TestOutput(bertInstr, "Freq=" + bertInstr.Frequency_MHz.ToString());
            System.Threading.Thread.Sleep(delay_ms);
            bertInstr.Frequency_MHz = 11096;
            TestOutput(bertInstr, "Freq=" + bertInstr.Frequency_MHz.ToString());

            bertInstr.PattGenEnabled = false;
            TestOutput(bertInstr, "PatternType=" + bertInstr.PatternType.ToString());
            bertInstr.PatternType = InstType_BertDataPatternType.Prbs;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternType=" + bertInstr.PatternType.ToString());
            bertInstr.PatternType = InstType_BertDataPatternType.User;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternType=" + bertInstr.PatternType.ToString());
            bertInstr.PatternType = InstType_BertDataPatternType.Prbs;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternType=" + bertInstr.PatternType.ToString());

            TestOutput(bertInstr, "PatternLength=" + bertInstr.PrbsLength.ToString());
            bertInstr.PrbsLength = 7;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternLength=" + bertInstr.PrbsLength.ToString());
            bertInstr.PrbsLength = 23;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternLength=" + bertInstr.PrbsLength.ToString());
            bertInstr.PrbsLength = 31;
            System.Threading.Thread.Sleep(delay_ms);
            TestOutput(bertInstr, "PatternLength=" + bertInstr.PrbsLength.ToString());
            

            TestOutput(bertInstr, "PattGen=" + bertInstr.PattGenEnabled.ToString());
            bertInstr.PattGenEnabled = true;
            TestOutput(bertInstr, "PattGen=" + bertInstr.PattGenEnabled.ToString());
            System.Threading.Thread.Sleep(delay_ms);
            bertInstr.PattGenEnabled = false;
            TestOutput(bertInstr, "PattGen=" + bertInstr.PattGenEnabled.ToString());

            TestOutput(bertInstr, "Analyser=" + bertInstr.AnalyserEnabled.ToString());            
        }

        [Test]
        public void T03_XFPHost_Test()
        {
            TestOutput("\n\n*** T03_XFPHost_Test ***");
            TestOutput(xfpHostInstr, "XFP Enabled? " + xfpHostInstr.XfpEnabled);
            xfpHostInstr.XfpEnabled = false;
            Thread.Sleep(2000);
            TestOutput(xfpHostInstr, "XFP Enabled? " + xfpHostInstr.XfpEnabled);
            xfpHostInstr.XfpEnabled = true;
            Thread.Sleep(2000);
            TestOutput(xfpHostInstr, "XFP Enabled? " + xfpHostInstr.XfpEnabled);
            TestOutput(xfpHostInstr, "XFP Powered? " + xfpHostInstr.PowerEnabled);
            xfpHostInstr.PowerEnabled = false;
            Thread.Sleep(3000);
            TestOutput(xfpHostInstr, "XFP Powered? " + xfpHostInstr.PowerEnabled);
            xfpHostInstr.PowerEnabled = true;
            Thread.Sleep(3000);
            TestOutput(xfpHostInstr, "XFP Powered? " + xfpHostInstr.PowerEnabled);
            Thread.Sleep(3000);
            xfpHostInstr.PowerEnabled = false;
            TestOutput(xfpHostInstr, "XFP Powered? " + xfpHostInstr.PowerEnabled);
        }

        [Test]
        public void T04_BERT_Meas()
        {
            TestOutput("\n\n*** T04_BERT_Meas ***");
            bertInstr.Frequency_MHz = 10703.0;
            bertInstr.PattGenEnabled = true;
            xfpHostInstr.XfpEnabled = true;
            for (int ii = 0; ii < 10; ii++)
            {
                bertLoop();
            }            
        }

        private void bertLoop()
        {
            bertInstr.AnalyserStart();
            Thread.Sleep(2000);
            bertInstr.AnalyserStop();
            TestOutput(bertInstr, "Errs: " + bertInstr.ErroredBits);
            TestOutput(bertInstr, "Err-Ratio: " + bertInstr.ErrorRatio);
            TestOutput(bertInstr, "Lost sync: " + bertInstr.SyncLossDetected);
        }

        #region Private helper fns        
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(Instrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(Chassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

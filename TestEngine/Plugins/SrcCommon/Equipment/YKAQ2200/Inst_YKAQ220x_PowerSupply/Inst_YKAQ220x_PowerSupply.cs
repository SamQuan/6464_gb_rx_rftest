// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_Ag662x.cs
//
// Author: K Pillar
// Design: As specified in Ag662x driver design DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
//using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for Agilent 662x SMU
    /// Provides basic source functions only for 'ElectricalSource' instrument type
    /// </summary>
    public class Inst_YKAQ220x_PowerSupply : InstType_ElectricalSource
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_YKAQ220x_PowerSupply(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information
            //
            //Note: In this case slot refers to the same thing as channel
            //
            // Add Inst_Ag662x details
            InstrumentDataRecord aq2200_642Data = new InstrumentDataRecord(
                "YOKOGAWA AQ2200-642 TRANSCEIVER IF MODULE",
               "0",
              "99.9999");				// maximum valid firmware version 

            aq2200_642Data.Add("MaxOutputs", "5");

            ValidHardwareData.Add("AQ2200_642", aq2200_642Data);




            // Configure valid chassis information
            // Add ag662x chassis details
            InstrumentDataRecord ykaq220ChassisData = new InstrumentDataRecord(
                "Chassis_YKAQ220x",	// chassis driver name  
                "0",				// minimum valid chassis driver version  
                "2.21-00");			// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_YKAQ220x", ykaq220ChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_YKAQ220x)base.InstrumentChassis;
            this.commandIdn = ":SLOT" + Slot + ":IDN?";
            this.commandOutpStem = ":OUTP" + Slot;
            this.commandOffsStem = ":INP" + Slot + ":OFFS";
            //ensure safe mode operation by default
            safeMode = true;
        }


        #region Public Instrument Methods and Properties

        /// <summary>
        /// We need to override the is online function so that we can check the indicated
        /// slot is in fact correct
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return instrumentChassis.IsOnline;
            }
            set
            {
                instrumentChassis.IsOnline = value;
                //if (value == true)
                //{
                //    Int16 myMaxOutputs = Convert.ToInt16(HardwareData["MaxOutputs"]);
                //    Int16 slotNo = Convert.ToInt16(this.Slot);
                //    if ((slotNo < 1) || (slotNo > myMaxOutputs))
                //    {
                //        throw new InstrumentException("This instrument supports only " + myMaxOutputs.ToString() + " slots, but config table says this is using slot " + this.Slot);
                //    }
                //}
            }
        }


        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string command;
                string[] resp;
                string hwID;

                // check if the instrument channel specified is actually there!
                this.checkSlotNotEmpty();
                //this.checkHeadNotEmpty();
                // Read the HEAD IDN? string which has 4 comma seperated substrings                
                //command = String.Format(":SLOT{0}:HEAD{1}:IDN?", this.Slot, this.SubSlot);
                command = String.Format(":SLOT{0}:IDN?", this.Slot);
                resp = instrumentChassis.Query_Unchecked(command, this).Split(',');
                if (resp.Length > 0)
                {
                    // There IS a head... This isn't a power sensor module.
                    // Model Nbr after 1st comma
                    hwID = resp[0] + " " + resp[1];
                }
                else
                {
                    // No head... This is a power sensor module.
                    // However the previous command will cause an error to be recorded, so clear the registers!
                    instrumentChassis.Write_Unchecked("*CLS", this);

                    // Read the SLOT IDN? string which has 4 comma seperated substrings
                    command = String.Format(":SLOT{0}:IDN?", this.Slot);
                    resp = instrumentChassis.Query_Unchecked(command, this).Split(',');
                    if (resp.Length == 0)
                    {
                        string errStr = String.Format("No  power meter available in '{0}' at {1}:{2}",
                            this.ChassisName, this.Slot, this.SubSlot);
                        throw new InstrumentException(errStr);
                    }
                    // Model Nbr after 1st comma
                    hwID = resp[0] + " " + resp[1];
                }
                return hwID;
            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {

                 this.checkSlotNotEmpty();
                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format(":SLOT{0}:IDN?", this.Slot);
                string resp = instrumentChassis.Query_Unchecked(command, this);
                string pluginFwVer = resp.Split(',')[3];

                if (pluginFwVer.Contains("-"))
                {

                    pluginFwVer = "V1.0";
                }
                // Return the 4th substring containing the firmware version
                return pluginFwVer;
            }
            
        }

        /// <summary>
        /// Configures the instrument into a default state.
        /// 
        /// </summary>
        public override void SetDefaultState()
        {
            // Set factory default
            // Send a reset, *RST, and a cls

         
            string cmdRest = string.Format(":SLOT{0}:PRES", this.Slot);
            instrumentChassis.Write_Unchecked(cmdRest, this);
            instrumentChassis.Write_Unchecked("*CLS", this);
      
            this.CheckForAnyErrors();
        }

        #endregion

        #region Public ElectricalSource InstrumentType Methods and Properties

        /// <summary>
        /// Set/Return output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Read the current output state
                bool result;
                string cmd = commandOutpStem + "?";
                string resp = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
                if (resp == "1")
                {
                    result = true;
                }
                else if (resp == "0")
                {
                    result = false;
                }
                else
                {
                    throw new InstrumentException("Invalid response to '" + cmd + "': " + resp);
                }
                return result;
            }
            set
            {
                //Set the output state
                string cmd;

                if (value)
                {
                    cmd = commandOutpStem + (char)32 + "ON";
                }
                else
                {
                    cmd = commandOutpStem + (char)32 + "OFF";
                }

                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Sets/returns the current (Iset) setpoint level//
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Gets the actual measured output current level
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                // Read current in amps
                string cmd = String.Format(":READ{0}:CURRent:PS{1}", this.Slot, this.SubSlot);
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd+"?", this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the voltage level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                // Read voltage in Volts
                string cmd = String.Format(":OUTPut{0}:VOLTage:PS{1}", this.Slot, this.SubSlot);
                     
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd + "?", this);
                if (SubSlot =="5")
                {
                    if (meas == "V3_3")
                    {
                        meas = "3.3";
                    }
                    if (meas == "V5_0")
                    {

                        meas = "5.0";


                    }
                }  
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }

            set
            {
                double LimitSetpoint=0;
                // Set voltage  source level in volts
                string command = String.Format(":OUTPut{0}:VOLTage:PS{1}", this.Slot, this.SubSlot);
                if (SubSlot != "5")
                {
                    LimitSetpoint = Convert.ToDouble(value.ToString().Trim());
                }
     
                if (SubSlot == "1" )
                {
                  if (LimitSetpoint >= 4.75 && LimitSetpoint <= 5.25)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + value.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of voltage limits!");
                    }
                }
                
                else if(SubSlot == "2")
                {
                    if ((LimitSetpoint - 3.465) < 0.15)
                        LimitSetpoint = 3.465;
                    if (LimitSetpoint >= 3.135 && LimitSetpoint <= 3.465)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + LimitSetpoint.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of voltage limits!");
                    }
                }
                else  if (SubSlot == "3" )
                {
                    if (LimitSetpoint >= 0.8 && LimitSetpoint <= 1.89)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + value.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of voltage limits!");
                    }
                }
                else if (SubSlot == "4")
                {
                    if (LimitSetpoint >= -5.46 && LimitSetpoint <= -4.94)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + value.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of voltage limits!");
                    }
                }
                else if (SubSlot == "5")
                {
                    if (value == 3.3)
                    {
                        instrumentChassis.Write_Unchecked(command + " V3_3", this);
                    }
                    else if (value == 5.0)
                    {
                        instrumentChassis.Write_Unchecked(command + " V5_0", this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                    }
                }
                else
                {
                    throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                }

                //this.CheckForAnyErrors();
            }
        }
        

        /// <summary>
        /// Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {//:outp2:volt:PS1?
                // Read voltage in Volts
                string cmd = String.Format(":OUTPut{0}:VOLTage:PS{1}", this.Slot, this.SubSlot);

                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd + "?", this);
                if (SubSlot =="5")
                {
                    if (meas == "V3_3")
                    {
                        meas = "3.3";
                    }
                    if (meas == "V5_0")
                    {

                        meas = "5.0";
                    
                    
                    }
                }  
                //this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }

        /// <summary>
        /// Sets/returns the compliance voltage set point
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            get
            {
                // Read voltage compliance in volts
                throw new InstrumentException("The method or operation is not implemented.");

            }
        }


        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return (safeMode);
            }
            set
            {
                safeMode = value;
            }
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// This function does two checks, first it will call the chassis level checks, ie are there any errors
        /// Second, it checks for slot specific errors, ie status registers 
        /// </summary>
        private void CheckForAnyErrors()
        {
            if (SafeModeOperation)
            {
                this.CheckErrorsstatus();
                this.checkSlotNotEmpty();
                //this.CheckForSlotSpecificErrors();
            }
        }
        private void CheckErrorsstatus()
        {
           
            string errorString = instrumentChassis.EmptyString_QueryUnchecked("SYST:ERR?", this);

            if (!errorString.Contains("No Error"))
            {


                instrumentChassis.Write_Unchecked("*CLS", this);

            }
        }
        private void checkSlotNotEmpty()
        {
            bool slotEmpty;

            // All the firmware is in the plugin slot, not the head
            string command = String.Format(":SLOT{0}:EMPTY?", this.Slot);
            string resp = this.instrumentChassis.Query(command, this);
            if (resp == "1") slotEmpty = true;
            if (resp == "0") slotEmpty = false;
            else
            {
                string errStr = String.Format("Command '{0}' returned bad value: '{1}'",
                    command, resp);
                throw new InstrumentException(errStr);
            }

            if (slotEmpty)
            {
                throw new InstrumentException("Slot " + this.Slot + " is empty!");
            }
        }






        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            if (safeMode)
            {
                this.instrumentChassis.Write(command, this, true, writeMaxTries, writeDelay_ms);
            }
            else
            {
                // if not in safe mode commands are async and no error checking
                this.instrumentChassis.Write(command, this, false, writeMaxTries, writeDelay_ms);
            }
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }


        #endregion


        #region Private Data



        // Chassis reference
        private Chassis_YKAQ220x instrumentChassis;

        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";

        //status register bytes
        private const byte overVoltageProtection = 8;
        private const byte overTemperatureProtection = 16;
        private const byte overCurrentProtection = 64;
        private const byte coupledParameter = 128;
        // Common command stems for most commands
        private string commandIdn;
        private string commandOutpStem;
        private string commandOffsStem;
        private bool safeMode;
        // Common sleep constants.
        private const int writeMaxTries = 20;
        private const int writeDelay_ms = 100;
        #endregion

        #region Unsupported methods

        /// <summary>
        /// Sets/returns the compliance current setpoint//CurrentComplianceSetPoint_Amp
        /// </summary>
        public override  double CurrentSetPoint_amp
        {
            get
            {
                // Read voltage in Volts
                string cmd = String.Format(":outp{0}:curr:lim:PS{1}", this.Slot, this.SubSlot);
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd + "?", this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }

            set
            {
                // Set voltage  source level in volts
                string command = String.Format(":outp{0}:curr:lim:PS{1}", this.Slot, this.SubSlot);
                double LimitSetpoint = Convert.ToDouble(value.ToString().Trim());
                if (SubSlot =="1"|| SubSlot == "3")
                {
                    if (LimitSetpoint >0.1 && LimitSetpoint <= 1.8)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + value.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                    }
                }
                else   if (SubSlot == "2" || SubSlot == "4")
                {
                    if (LimitSetpoint > 0.1 && LimitSetpoint <= 3)
                    {
                        instrumentChassis.Write_Unchecked(command + " " + value.ToString().Trim(), this);
                    }
                    else
                    {
                        throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                    }
                }
                else if (SubSlot =="5")
                {
                    if (this.VoltageActual_Volt < 4.5)
                    {
                        if (LimitSetpoint >0.1&& LimitSetpoint<=1.0)
                        {
                            instrumentChassis.Write_Unchecked(command + "V3_3  " + value.ToString().Trim(), this);
                        }
                        else
                        {
                            throw new InstrumentException("Sub slot " + SubSlot + " out of Current limits!");
                        }
                       


                    }
                    else
                    {
                        if (LimitSetpoint > 0.1 && LimitSetpoint <= 2)
                        {
                            instrumentChassis.Write_Unchecked(command + "V5_0  " + value.ToString().Trim(), this);
                        }
                        else
                        {
                            throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                        }
                       
                        
                    
                    
                    }
                
                }
                else
                {
                    throw new InstrumentException("Slot " + this.Slot + " out of Current limits!");
                }
          
                //this.CheckForAnyErrors();
            }
          
        }

        #endregion
    }
}

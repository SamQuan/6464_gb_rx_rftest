// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_AQ220x.cs
//
// Author: 
// Design: YoKoGaWa AQ220x Main frame Driver 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for YokoGawa YKAQ220x Lightwave Measurement System.
    /// </summary>
    public class Chassis_YKAQ220x : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="visaResourceString">VISA resource string for communicating with the chassis</param>
        public Chassis_YKAQ220x(string chassisNameInit, string driverNameInit,
            string visaResourceString)
            : base(chassisNameInit, driverNameInit, visaResourceString)
        {
            // maximum allowed chassis version - if we know of a newer good firmware version,
            // update the following line and all the chassis variants will be updated!

            //string maxChassisVersion = "7.00(99999)";
            // Setup expected valid hardware variants 
            // Add AQ2200 details
            ChassisDataRecord aq2202Data = new ChassisDataRecord(
                "YOKOGAWA AQ2202 FRAME CONTROLLER",			// hardware name 
                //"YOKOGAWA",
                "0.0",								// minimum valid firmware version 
                "99.99");					// maximum valid firmware version 
            ValidHardwareData.Add("AQ2202", aq2202Data);

            // Add AQ2201 details

            ChassisDataRecord aq2201Data = new ChassisDataRecord(
                "YOKOGAWA AQ2202 FRAME CONTR_LLER",			// hardware name 
                "0.0",								// minimum valid firmware version 
                "99.99");					// maximum valid firmware version 
            ValidHardwareData.Add("AQ2201", aq2201Data);

             ChassisDataRecord aq2211Data = new ChassisDataRecord(
                "YOKOGAWA AQ2211",			// hardware name 
                "2.21-00",								// minimum valid firmware version 
                "9.99-99");					// maximum valid firmware version 
            ValidHardwareData.Add("AQ2211", aq2211Data);

            ChassisDataRecord aq2212Data = new ChassisDataRecord(
                        "YOKOGAWA AQ2212",			// hardware name 
                        "2.21-00",								// minimum valid firmware version 
                        "9.99-99");					// maximum valid firmware version 
            ValidHardwareData.Add("AQ2212", aq2212Data);


            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
            
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {

                //this.Clear();
                 //Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                 //Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
            
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                //this.Clear();
                // Read the chassis ID string and split the comma seperated fields
                //string resp = Query_Unchecked("*IDN?", null);
                //string[] idn = resp.Split(',');
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                //idn[0]: manufacture name
                //idn[1]: device name

               return idn[0] + " " + idn[1];
              
            }
           
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // NOTHING TO DO
                }

                // setup base class
                base.IsOnline = value;

                //if (value) // if setting online                
                //{
                //    // Set up Chassis standard event register mask
                //    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                //    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                //    // clear the status registers
                //    this.Write("*CLS", null);
                //}
            }
        }
        /// <summary>
        /// Queries an IEEE standard binary block from an Agilent Chassis
        /// </summary>
        /// <param name="cmd">Command to send to equipment</param>
        /// <param name="i">Equipment reference</param>
        /// <returns>Reply byte array</returns>
        /// <remarks>Need new version to deal with Agilent extra termination character!</remarks>
        public new byte[] QueryIEEEBinary_Unchecked(string cmd, Instrument i)
        {
            // get the file data
            byte[] bytes = base.QueryIEEEBinary_Unchecked(cmd, i);
            // Agilent appends a linefeed character after this, so need to read this back too
            // so that it doesn't mess up any future comms. (ignore it)
            this.Read_Unchecked(null);
            return bytes;
        }

        /// <summary>
        /// Specific command to try and catch empty string responses from the 6626
        /// (put in as a result of integration test issue)
        /// </summary>
        /// <param name="cmd">Our string command</param>
        /// <param name="instr">the instrument</param>
        /// <returns>the string response</returns>
        public string EmptyString_QueryUnchecked(string cmd, Instrument instr)
        {
            string result;
            bool valid = false;
            int count = 0;
            do
            {
                result = this.Query_Unchecked(cmd, instr);

                if (result == "")
                {
                    // Sleep for 500ms waiting for the DUT to stabilise
                    System.Threading.Thread.Sleep(500);
                    count++;
                }
                else
                {
                    valid = true;
                }
            } while ((!valid) && (count < 3));

            return (result);
        }
        #endregion
    }
}

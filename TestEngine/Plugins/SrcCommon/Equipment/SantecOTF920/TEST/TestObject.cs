using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace SantecOTF9x0Harness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_SantecOTF920 testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_SantecOTF920 testOTF;        
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::1::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string inst1Name = "Instrument";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_SantecOTF920(chassisName, "Chassis_SantecOTF9x0", visaResource);
            TestOutput(chassisName, "Created OK");

            // create instrument objects            
            testOTF = new Inst_SantecOTF920(inst1Name, "Inst_SantecOTF9x0", "x", "y", testChassis);
            TestOutput(inst1Name, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testOTF.IsOnline = true;
            TestOutput(inst1Name, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

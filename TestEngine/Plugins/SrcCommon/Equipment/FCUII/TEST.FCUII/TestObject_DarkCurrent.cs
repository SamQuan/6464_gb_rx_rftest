using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.Instruments;
using System.Collections.Specialized;
using System.IO;
using ReadTestEngineConfig;
using System.Text.RegularExpressions;

namespace TEST.FCUII
{
    /// <summary>
    /// 
    /// </summary>
    public class FcuDarkCurr
    {
        public int chan;
        public double VoltSet_V;
        public double darkCurrent_mA;
    }
    /// <exclude />	
    [TestFixture]
    public class Test_ObjectDarkCurrent
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_SMUTI SmutiChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_SMUTI_MainControlSMU MainCtlCard;

        private List<Inst_SMUTI_TriggeredSMU> listInstrument = new List<Inst_SMUTI_TriggeredSMU>();
        private List<int> ChanNums = new List<int>();


        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            string directoryPath = @"C:\ConherentRx_RfTest_Lab\TestEngine\Plugins\SrcCommon\Equipment\FCUII";
            List<ChassisCollection> chassisCollection = new List<ChassisCollection>();
            List<InstrumentCollection> instrCollection = new List<InstrumentCollection>();
            
            ReadConfig readConfig = new ReadConfig();
            readConfig.ReadTestEngineConfig(directoryPath,ref chassisCollection,ref instrCollection);
            
            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            foreach (ChassisCollection chassis in chassisCollection)
            {
                if (chassis.DriverName=="Chassis_SMUTI")
                {
                    SmutiChassis = new Chassis_SMUTI("Chassis_SMUTI", "Chassis_SMUTI", chassis.ResourceString);
                    SmutiChassis.IsOnline = true;
                }
            }
            TestOutput(SmutiChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            string instName = instrCollection[0].InstrumentName;
            int iCount = 0;
            foreach (InstrumentCollection inst in instrCollection)
            {
                if (inst.DriverName == "Inst_SMUTI_TriggeredSMU")
                {
                    Inst_SMUTI_TriggeredSMU instment;
                    instment = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU", "Inst_SMUTI_TriggeredSMU", inst.Slot, inst.SubSlot, SmutiChassis);
                    listInstrument.Add(instment);
                    string ChanNum = listInstrument[iCount].Slot + listInstrument[iCount].SubSlot;
                    ChanNums.Add(int.Parse(ChanNum));
                    iCount++;
                    instment.IsOnline = true;
                }
            }

            TestOutput("Instrument Created OK");

            

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            //testChassis.IsOnline = true;
            SmutiChassis.EnableLogging = true;
            TestOutput(SmutiChassis, "IsOnline set true OK");

        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("shut down all channel output");

            MainCtlCard.SetDefaultState();

            TestOutput("Don't forget to take the chassis offline!");
            SmutiChassis.IsOnline = false;
            MainCtlCard.IsOnline = false;
            // Test end
            TestOutput("Shut down,test finished!");
        }

        [Test]
        public void T01_DriverNameTest()
        {
            TestOutput("\n\n*** T01_chanssisNameTest ***");
            string chanssisName = MainCtlCard.ChassisName;
            TestOutput(chanssisName);
            string InstrName = MainCtlCard.Name;
            TestOutput(InstrName);
        }

        [Test]
        public void T02_FcuDarkCurrentTest()
        {

            listInstrument[0].VoltageSetPoint_Volt = 3.3;
            
            List<FcuDarkCurr> listFcuDarkCurr_mA = new List<FcuDarkCurr>();
            string DarkCurrCalFile = "C:\\100G_conherentRx\\TestEngine\\Plugins\\SrcCommon\\Equipment\\FCUII\\DarkCurrentCal.csv";
            List<string> returnData = new List<string>();
            using (StreamReader sr = new StreamReader(DarkCurrCalFile))
            {
                StringBuilder fileData = new StringBuilder();
                string title = sr.ReadLine();
                int lineNumber = 0;
                while (sr.Peek() > -1)
                {
                    FcuDarkCurr fcuDarkCurrent = new FcuDarkCurr();
                    string str = sr.ReadLine();
                    string[] strTemp = str.Split(',');
                    fcuDarkCurrent.chan = int.Parse(strTemp[0]);
                    fcuDarkCurrent.VoltSet_V = double.Parse(strTemp[1]);
                    fcuDarkCurrent.darkCurrent_mA = double.Parse(strTemp[2]);
                    listFcuDarkCurr_mA.Add(fcuDarkCurrent);
                }
                sr.Close();
            }

            TestOutput("Start meansure dark current!");
            foreach (FcuDarkCurr fcuDarkCurr in listFcuDarkCurr_mA)
            {
                for (int i = 0; i < ChanNums.Count; i++)
                {
                    if (fcuDarkCurr.chan == ChanNums[i])
                    {
                        //listInstrument[i].VoltageSetPoint_Volt = fcuDarkCurr.VoltSet_V;
                        //fcuDarkCurr.darkCurrent_mA = listInstrument[i].CurrentActual_Amp;
                        double darkCurrent_mA = fcuDarkCurr.darkCurrent_mA;
                        listInstrument[i].DarkCurrent_mA.Add(fcuDarkCurr.VoltSet_V.ToString() + "V", darkCurrent_mA);
                        TestOutput("");
                        TestOutput("SetVoltage_V: " + fcuDarkCurr.VoltSet_V);
                        TestOutput("DarkCurrent_mA: " + darkCurrent_mA);
                        
                        break;
                    }
                }
            }

            List<string> listData = new List<string>();
            StringBuilder sb = new StringBuilder();
            sb.Append("ChanNum,VoltSet_V,CurrentMeas_Amps");
            listData.Add(sb.ToString());
            for (int i = 0; i < listFcuDarkCurr_mA.Count; i++)
            {
                sb.Remove(0, sb.Length);
                sb.Append(listFcuDarkCurr_mA[i].chan.ToString());
                sb.Append(",");
                sb.Append(listFcuDarkCurr_mA[i].VoltSet_V.ToString());
                sb.Append(",");
                sb.Append(listFcuDarkCurr_mA[i].darkCurrent_mA.ToString());
                listData.Add(sb.ToString());
            }

            using (StreamWriter sw = new StreamWriter(DarkCurrCalFile))
            {
                foreach (string var in listData)
                {
                    sw.WriteLine(var);
                }
            }
        }

        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }
        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        protected string MzDataDirectory()
        {
            string FileName = string.Format("sweepData_DBxxxxxx.xxx_{0:yyyyMMddhhmmss}.csv", DateTime.Now);
            string dir = Directory.GetDirectoryRoot(System.Environment.CurrentDirectory) + "FcuMarkIIResult";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, FileName);
        }


        #endregion
    }

}

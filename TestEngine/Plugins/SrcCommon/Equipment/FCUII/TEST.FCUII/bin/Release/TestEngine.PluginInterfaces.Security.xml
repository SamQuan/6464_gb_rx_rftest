<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.PluginInterfaces.Security</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.ILogWriter">
            <summary>
            Interface to be supplied by Security Server allowing plugin to write to logs.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ILogWriter.DeveloperWrite(System.String)">
            <summary>
            Write to Developer log file.
            </summary>
            <param name="text">Entry text.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ILogWriter.ErrorRaise(System.String)">
            <summary>
            Raise a fatal error exception.
            </summary>
            <param name="text">Error text.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ILogWriter.ErrorWrite(System.String)">
            <summary>
            Write to error log.
            </summary>
            <param name="text">Log entry text.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ILogWriter.ErrorWriteWithException(System.String,System.Exception)">
            <summary>
            Write into error log with exception.
            </summary>
            <param name="text">Log entry text.</param>
            <param name="exceptionObj">Wrapped exception.</param>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.ISecurityPlugin">
            <summary>
            Security plugin classes must implement this interface.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ISecurityPlugin.LoginRequest(Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg,Bookham.TestEngine.PluginInterfaces.Security.ILogWriter)">
            <summary>
            A request to log in a user from GUI. Check credentials.
            </summary>
            <param name="msg">Unauthenticated credentials message from control.</param>
            <param name="logWriter">Reference to log write access object.</param>
            <returns>Result of request.</returns>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.ISecurityPlugin.LogoutNotification(System.String,Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel,Bookham.TestEngine.PluginInterfaces.Security.ILogWriter)">
            <summary>
            Notification that the user has logged out.
            </summary>
            <param name="user">Name of logging out user.</param>
            <param name="priv">Privilege level of that user.</param>
            <param name="logWriter">Reference to log write access object.</param>
            <returns>Success of logout process.</returns>
        </member>
        <member name="P:Bookham.TestEngine.PluginInterfaces.Security.ISecurityPlugin.Control">
            <summary>
            Gets the type of the plugin GUI control. Must be ManagedCtlBase subclass.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg">
            <summary>
            Message payload (plugged in control -> Security server) requesting check credentials and
            login if good.
            </summary>
            <remarks>Specific security plugins may wish to subclass this class for its own use, perhaps
            to add a password</remarks>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg.#ctor(System.String)">
            <summary>
            Construct login request message.
            </summary>
            <param name="userId">User name.</param>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg.UserId">
            <summary>
            Gets the User id to check.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus">
            <summary>
            Enum listing success/failure states LoginRequestStatus.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus.Success">
            <summary>
            Request succeeded.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus.UserDoesNotExist">
            <summary>
            Request failed due to user id not in user database.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus.InvalidPassword">
            <summary>
            Request failed due to incorrect password.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus.UserAlreadyLoggedIn">
            <summary>
            Request failed because someone else is already logged in.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus.InternalError">
            <summary>
            Internal error prevents login.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg">
            <summary>
            Message payload (Security server -> plugged in security control) indicating the result of
            a Login Request.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg.#ctor(Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel)">
            <summary>
            Construct the login response message indicating success.
            </summary>
            <param name="priv">User's privilege level.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg.#ctor(Bookham.TestEngine.PluginInterfaces.Security.LoginRequestStatus)">
            <summary>
            Construct the login response message indicating failure.
            </summary>
            <param name="status">Error status.</param>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg.Status">
            <summary>
            Gets the result of the original login request.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg.Privilege">
            <summary>
            Gets the user's privilege level in the case of a successful login.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.LogoutStatus">
            <summary>
            Enum listing logout success/failure state.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LogoutStatus.Success">
            <summary>
            Logout completed.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LogoutStatus.NoUserLoggedIn">
            <summary>
            Logout failed because there is no user logged in.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.LogoutStatus.InternalError">
            <summary>
            An internal error occurred while logging out.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.NamespaceDoc">
            <summary>
            Test Engine Security Plug-in Interface Definition.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel">
            <summary>
            Privilege level of the user
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel.Operator">
            <summary>
            Operator privilege level
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel.Technician">
            <summary>
            Technician privilege level
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase">
            <summary>
            Base class for custom security GUI controls.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.#ctor">
            <summary>
            Constructor. Sets up base control.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.Dispose(System.Boolean)">
            <summary>
            Clean up any resources being used.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.InitializeComponent">
            <summary>
            Required method for Designer support - do not modify
            the contents of this method with the code editor.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.SecurityCtlBase_MsgReceived(System.Object,System.Int64,System.Int64)">
            <summary>
            Incoming message from message centre. (Event raised by ManagedCtlBase.)
            </summary>
            <param name="payload">Message payload.</param>
            <param name="inMsgSeq">Incoming message sequence number. Ignored.</param>
            <param name="respSeq">Response sequence number. Ignored.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.SendLoginRequest(Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg)">
            <summary>
            Send a login request message, containing unauthenticated credentials.
            Response will arrive on LoginResponse event handler.
            </summary>
            <param name="reqMsg">Unauthenticated credentials.</param>
        </member>
        <member name="E:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.LoginResponse">
            <summary>
            Login Response event. Raised when security server responds to login request.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.LoginResponseDelegate">
            <summary>
            Delegate for the LoginResponse event.
            </summary>
            <remarks>Passes in payload from security server.</remarks>
        </member>
        <member name="T:Bookham.TestEngine.PluginInterfaces.Security.SecurityException">
            <summary>
            Exception thrown on failure of security interface.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityException.#ctor(System.String)">
            <summary>
            Construct exception class with message.
            </summary>
            <param name="message">Exception text.</param>
        </member>
        <member name="M:Bookham.TestEngine.PluginInterfaces.Security.SecurityException.#ctor(System.String,System.Exception)">
            <summary>
            Construct exception class with message and inner exception.
            </summary>
            <param name="message">Exception text.</param>
            <param name="innerException">Inner exception.</param>
        </member>
    </members>
</doc>

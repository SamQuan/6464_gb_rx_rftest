// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_TEC.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_TEC
    /// </summary>
    public class Inst_SMUTI_TEC :Instrument, IInstType_TecController
    {
        private Chassis_SMUTI instChassis;


        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_TEC(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Combined 4 Channel High Current Low Voltage TEC 
            // & 4 Channel Very Low Current Low Voltage Thermistor card.
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord TecData = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            TecData.Add("TECCurrentSource", "1.5");		// 1.5A
            TecData.Add("TECVoltageSense", "5");	// 5V
            TecData.Add("RthCurrentSource", "0.0001");		// 100uA
            TecData.Add("RthVoltageSense", "5");		// 5V
            TecData.Add("MinTemperatureLimit_C", "25.0");		// minimum TEC voltage limit
            TecData.Add("MaxTemperatureLimit_C", "80.0");	// maximum TEC voltage limit
            TecData.Add("RthAbilityToGround", "true");			// connect negative terminals to analogue 0V
            ValidHardwareData.Add("TecData", TecData);


            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SMUTI",								// chassis driver name  
                "FW:0001",									// minimum valid chassis driver version  
                "FW:1001");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            instChassis = (Chassis_SMUTI)chassisInit;

        }



        public override string FirmwareVersion
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string HardwareIdentity
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override void SetDefaultState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region IInstType_TecController Members

        public CallendarVanDusenCoefficients CallendarVanDusenConstants
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double DerivativeGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double IntegralGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_TecController.ControlMode OperatingMode
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double ProportionalGain
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double SensorCurrent_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double SensorResistanceActual_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double SensorResistanceSetPoint_ohm
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_TecController.SensorType Sensor_Type
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public SteinhartHartCoefficients SteinhartHartConstants
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double TecCurrentActual_amp
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double TecCurrentCompliance_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double TecCurrentSetPoint_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double TecResistanceAC_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double TecResistanceDC_ohm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double TecVoltageActual_volt
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double TecVoltageCompliance_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double TecVoltageSetPoint_volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

        #region IInstType_SimpleTempControl Members

        public bool OutputEnabled
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double SensorTemperatureActual_C
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double SensorTemperatureSetPoint_C
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion
    }
}

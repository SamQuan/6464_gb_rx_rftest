using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.Solution.Config;
using System.Data;
using System.Xml;

namespace ReadTestEngineConfig
{
    public class ReadConfig
    {
        public void ReadTestEngineConfig(string directoryPath, ref Dictionary<string, ChassisCollection> chassisCollection, ref Dictionary<string, InstrumentCollection> instrumentCollection)
        {
            string filePath = Path.GetFullPath(Path.Combine(directoryPath, "TestEquipmentConfig.xml"));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            XmlNode root = xmlDoc.SelectSingleNode("TestEquipmentConfig");
            XmlNodeList xnl = xmlDoc.SelectSingleNode("TestEquipmentConfig").ChildNodes;
            XmlNodeList chassisxnl = xnl[0].ChildNodes;
            for (int i = 0; i < chassisxnl.Count; i++)
            {
                XmlNode chassises = xnl[0].ChildNodes[i];
                ChassisCollection chass = new ChassisCollection();
                foreach (XmlNode chassis in chassises)
                {
                    XmlElement xeChassis = (XmlElement)chassis;
                    switch (xeChassis.Name)
                    {
                        case "ChassisName":
                            chass.ChassisName = xeChassis.InnerText;
                            break;
                        case "DriverName":
                            chass.DriverName = xeChassis.InnerText;
                            break;
                        case "DriverVersion":
                            chass.DriverVersion = xeChassis.InnerText;
                            break;
                        case "ResourceString":
                            chass.ResourceString = xeChassis.InnerText;
                            break;
                        case "Online":
                            chass.Online = xeChassis.InnerText;
                            break;
                        default:
                            break;
                    }
                }
                chassisCollection.Add(chass.ChassisName, chass);
            }
            XmlNodeList instrumentxnl = xnl[1].ChildNodes;
            for (int j = 0; j < instrumentxnl.Count; j++)
            {
                XmlNode instruments = xnl[1].ChildNodes[j];
                InstrumentCollection instr = new InstrumentCollection();
                foreach (XmlNode instrument in instruments)
                {
                    XmlElement xeInstrument = (XmlElement)instrument;
                    switch (xeInstrument.Name)
                    {
                        case "InstrumentName":
                            instr.InstrumentName = xeInstrument.InnerText;
                            break;
                        case "DriverName":
                            instr.DriverName = xeInstrument.InnerText;
                            break;
                        case "DriverVersion":
                            instr.DriverVersion = xeInstrument.InnerText;
                            break;
                        case "ChassisName":
                            instr.ChassisName = xeInstrument.InnerText;
                            break;
                        case "Slot":
                            instr.Slot = xeInstrument.InnerText;
                            break;
                        case "SubSlot":
                            instr.SubSlot = xeInstrument.InnerText;
                            break;
                        case "Online":
                            instr.Online = xeInstrument.InnerText;
                            break;
                        default:
                            break;
                    }
                }
                instrumentCollection.Add(instr.InstrumentName, instr);
            }
        }

        public void ReadTestEngineConfig(string directoryPath, ref List<ChassisCollection> chassisCollection, ref List<InstrumentCollection> instrumentCollection)
        {
            string filePath = Path.GetFullPath(Path.Combine(directoryPath, "TestEquipmentConfig.xml"));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            XmlNode root = xmlDoc.SelectSingleNode("TestEquipmentConfig");
            XmlNodeList xnl = xmlDoc.SelectSingleNode("TestEquipmentConfig").ChildNodes;
            XmlNodeList chassisxnl = xnl[0].ChildNodes;
            for (int i = 0; i < chassisxnl.Count; i++)
            {
                XmlNode chassises = xnl[0].ChildNodes[i];
                ChassisCollection chass = new ChassisCollection();
                foreach (XmlNode chassis in chassises)
                {
                    XmlElement xeChassis = (XmlElement)chassis;
                    switch (xeChassis.Name)
                    {
                        case "ChassisName":
                            chass.ChassisName = xeChassis.InnerText;
                            break;
                        case "DriverName":
                            chass.DriverName = xeChassis.InnerText;
                            break;
                        case "DriverVersion":
                            chass.DriverVersion = xeChassis.InnerText;
                            break;
                        case "ResourceString":
                            chass.ResourceString = xeChassis.InnerText;
                            break;
                        case "Online":
                            chass.Online = xeChassis.InnerText;
                            break;
                        default:
                            break;
                    }
                }
                chassisCollection.Add(chass);
            }
            XmlNodeList instrumentxnl = xnl[1].ChildNodes;
            for (int j = 0; j < instrumentxnl.Count; j++)
            {
                XmlNode instruments = xnl[1].ChildNodes[j];
                InstrumentCollection instr = new InstrumentCollection();
                foreach (XmlNode instrument in instruments)
                {
                    XmlElement xeInstrument = (XmlElement)instrument;
                    switch (xeInstrument.Name)
                    {
                        case "InstrumentName":
                            instr.InstrumentName = xeInstrument.InnerText;
                            break;
                        case "DriverName":
                            instr.DriverName = xeInstrument.InnerText;
                            break;
                        case "DriverVersion":
                            instr.DriverVersion = xeInstrument.InnerText;
                            break;
                        case "ChassisName":
                            instr.ChassisName = xeInstrument.InnerText;
                            break;
                        case "Slot":
                            instr.Slot = xeInstrument.InnerText;
                            break;
                        case "SubSlot":
                            instr.SubSlot = xeInstrument.InnerText;
                            break;
                        case "Online":
                            instr.Online = xeInstrument.InnerText;
                            break;
                        default:
                            break;
                    }
                }
                instrumentCollection.Add(instr);
            }
        }

    }
    /// <summary>
    /// ReadTestEngineConfig
    /// </summary>
    /// <param name="directoryPath">TestEquipmentConfig file dir</param>
    /// <param name="chassisCollection">ChassisCollection List</param>
    /// <param name="instrumentCollection">InstrumentCollection List</param>
    /// <summary>
    /// ChassisCollection
    /// </summary>
    public class ChassisCollection
    {
        public string ChassisName;
        public string DriverName;
        public string DriverVersion;
        public string ResourceString;
        public string Online;

    }
    /// <summary>
    /// InstrumentCollection
    /// </summary>
    public class InstrumentCollection
    {
        public string InstrumentName;
        public string DriverName;
        public string DriverVersion;
        public string ChassisName;
        public string Slot;
        public string SubSlot;
        public string Online;
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    static class GetActualCurrentValue
    {
        public void GetActualCurrentValue(string Current)
        {
            Inst_SMUTI_TriggeredSMU temp;
            TempCurrent = Current;
        }

        public void GetActualVoltageValue(string Voltage)
        {
            Inst_SMUTI_TriggeredSMU temp;
            TempVoltage = Voltage;
        }


        public static string TempCurrent;
        public static string TempVoltage;
    }    
 
}

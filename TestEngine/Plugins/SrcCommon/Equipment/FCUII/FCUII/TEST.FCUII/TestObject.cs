using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.Instruments;
using System.Collections.Specialized;
using System.IO;
using ReadTestEngineConfig;
using System.Text.RegularExpressions;

namespace TEST.FCUII
{

    /// <summary>
    /// SweepDataType
    /// </summary>
    public enum SweepDataType
    {
        /// <summary>Left-arm mod bias voltage</summary>
        LeftArmModBias_V,
        /// <summary>Left-arm mod bias current</summary>
        LeftArmModBias_mA,
        /// <summary>Right-arm mod bias voltage</summary>
        RightArmModBias_V,
        /// <summary>Right-arm mod bias current</summary> 
        RightArmModBias_mA,
        /// <summary>Left-arm imbalance voltage</summary>
        LeftArmImb_V,
        /// <summary>Left-arm imbalance current</summary>
        LeftArmImb_mA,
        /// <summary>Right-arm imbalance voltage</summary>
        RightArmImb_V,
        /// <summary>Right-arm imbalance current</summary>
        RightArmImb_mA,
        /// <summary>Power tap current</summary>
        PowerTap_mA,
        /// <summary>Power tap voltage</summary>
        PowerTap_V,
        /// <summary>Optical Fibre Power</summary>
        FibrePower_mW,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum sweepResultMemNumber
    {

        LeftImb = 1,
        RightImb,
        LeftModBias,
        RightModBias,
        Ctap
    }
    /// <summary>
    /// 
    /// </summary>
    public struct VoltageSweepSetupData
    {
        public double fixedModBias_V;
        public double leftImbalance_A;
        public double rightImbalance_A;
        public double sweepStart_V;
        public double sweepStop_V;
        public double stepSize_V;
        public double tapBias_V;
        public double complianceVolt_V;
        public double complianceCurr_A;
    }
    /// <summary>
    /// 
    /// </summary>
    public struct CurrentSweepSetupData
    {
        public double leftModBias_V;
        public double rightModBias_V;
        public double sweepStart_A;
        public double sweepStop_A;
        public double stepSize_A;
        public double tapBias_V;
        public double fixedImb_A;
        public double complianceVolt_V;
        public double complianceCurr_A;
    }

    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_SMUTI SmutiChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_SMUTI_MainControlSMU MainCtlCard;    
        
        private Inst_SMUTI_TriggeredSMU FS1Instr;
        private Inst_SMUTI_TriggeredSMU FS2Instr;
        private Inst_SMUTI_TriggeredSMU FS3Instr;
        private Inst_SMUTI_TriggeredSMU FS4Instr;
        private Inst_SMUTI_TriggeredSMU FS5Instr;
        private Inst_SMUTI_TriggeredSMU FS6Instr;
        private Inst_SMUTI_TriggeredSMU FS7Instr;
        private Inst_SMUTI_TriggeredSMU FS8Instr;

        private Inst_SMUTI_TriggeredSMU GainInstr;
        private Inst_SMUTI_TriggeredSMU SOAInstr;
        private Inst_SMUTI_TriggeredSMU RearInstr;
        private Inst_SMUTI_TriggeredSMU PhaseInstr;

        private Inst_SMUTI_TriggeredSMU RefPDInstr;
        private Inst_SMUTI_TriggeredSMU FilterPDInstr;
        private Inst_SMUTI_TriggeredSMU LockerTxInstr;

        //MZ instrment
        private Inst_SMUTI_TriggeredSMU MzCtapInstr;
        private Inst_SMUTI_TriggeredSMU MzLeftModInstr;
        private Inst_SMUTI_TriggeredSMU MzRightModInstr;
        private Inst_SMUTI_TriggeredSMU MzLeftImbInstr;
        private Inst_SMUTI_TriggeredSMU MzRightImbInstr;

        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            //string directoryPath = @"C:\40G_markII\TestEngine\Plugins\SrcCommon\Equipment\FCUII";
            string directoryPath = @"C:\TestSets\TCMZ_Final_FcuMk2\TestEngine\Plugins\SrcCommon\Equipment\FCUII";
            Dictionary<string, ChassisCollection> chassisCollection = new Dictionary<string, ChassisCollection>();
            Dictionary<string, InstrumentCollection> instrCollection = new Dictionary<string, InstrumentCollection>();
            ReadConfig readConfig = new ReadConfig();
            readConfig.ReadTestEngineConfig(directoryPath,ref chassisCollection,ref instrCollection);
            
            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");

            SmutiChassis = new Chassis_SMUTI("Chassis_SMUTI", "Chassis_SMUTI", chassisCollection["FCUMARKII"].ResourceString);
            TestOutput(SmutiChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            MainCtlCard = new Inst_SMUTI_MainControlSMU("Inst_SMUTI_MainControlSMU", "Inst_SMUTI_MainControlSMU", "", "", SmutiChassis);
            FS1Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU1", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_1"].Slot, instrCollection["FrontSection_1"].SubSlot, SmutiChassis);
            FS2Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU2", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_2"].Slot, instrCollection["FrontSection_2"].SubSlot, SmutiChassis);
            FS3Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU3", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_3"].Slot, instrCollection["FrontSection_3"].SubSlot, SmutiChassis);
            FS4Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU4", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_4"].Slot, instrCollection["FrontSection_4"].SubSlot, SmutiChassis);
            FS5Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU5", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_5"].Slot, instrCollection["FrontSection_5"].SubSlot, SmutiChassis);
            FS6Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU6", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_6"].Slot, instrCollection["FrontSection_6"].SubSlot, SmutiChassis);
            FS7Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU7", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_7"].Slot, instrCollection["FrontSection_7"].SubSlot, SmutiChassis);
            FS8Instr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU8", "Inst_SMUTI_TriggeredSMU", instrCollection["FrontSection_8"].Slot, instrCollection["FrontSection_8"].SubSlot, SmutiChassis);
            
            GainInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU9", "Inst_SMUTI_TriggeredSMU", instrCollection["Gain"].Slot, instrCollection["Gain"].SubSlot, SmutiChassis);
            SOAInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU10", "Inst_SMUTI_TriggeredSMU", instrCollection["SOA"].Slot, instrCollection["SOA"].SubSlot, SmutiChassis);
            RearInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU11", "Inst_SMUTI_TriggeredSMU", instrCollection["Rear"].Slot, instrCollection["Rear"].SubSlot, SmutiChassis);
            PhaseInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU12", "Inst_SMUTI_TriggeredSMU", instrCollection["Phase"].Slot, instrCollection["Phase"].SubSlot, SmutiChassis);

            //RefPDInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU13", "Inst_SMUTI_TriggeredSMU", instrCollection["RefPD"].Slot, instrCollection["RefPD"].SubSlot, SmutiChassis);
            //FilterPDInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU14", "Inst_SMUTI_TriggeredSMU", instrCollection["FilterPD"].Slot, instrCollection["FilterPD"].SubSlot, SmutiChassis);
            LockerTxInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU15", "Inst_SMUTI_TriggeredSMU", instrCollection["LockerTx"].Slot, instrCollection["LockerTx"].SubSlot, SmutiChassis);

            MzCtapInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU16", "Inst_SMUTI_TriggeredSMU", instrCollection["MzCtap"].Slot, instrCollection["MzCtap"].SubSlot, SmutiChassis);
            MzLeftModInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU17", "Inst_SMUTI_TriggeredSMU", instrCollection["MzLeftMod"].Slot, instrCollection["MzLeftMod"].SubSlot, SmutiChassis);
            MzRightModInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU18", "Inst_SMUTI_TriggeredSMU", instrCollection["MzRightMod"].Slot, instrCollection["MzRightMod"].SubSlot, SmutiChassis);
            MzLeftImbInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU19", "Inst_SMUTI_TriggeredSMU", instrCollection["MzLeftImb"].Slot, instrCollection["MzLeftImb"].SubSlot, SmutiChassis);
            MzRightImbInstr = new Inst_SMUTI_TriggeredSMU("Inst_SMUTI_TriggeredSMU20", "Inst_SMUTI_TriggeredSMU", instrCollection["MzRightImb"].Slot, instrCollection["MzRightImb"].SubSlot, SmutiChassis);

            
            TestOutput("Instrument Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            //testChassis.IsOnline = true;
            SmutiChassis.IsOnline = true;
            SmutiChassis.EnableLogging = true;
            TestOutput(SmutiChassis, "IsOnline set true OK");

            MainCtlCard.IsOnline = true;

            FS1Instr.IsOnline = true;
            FS2Instr.IsOnline = true;
            FS3Instr.IsOnline = true;
            FS4Instr.IsOnline = true;
            FS5Instr.IsOnline = true;
            FS6Instr.IsOnline = true;
            FS7Instr.IsOnline = true;
            FS8Instr.IsOnline = true;

            GainInstr.IsOnline = true;
            SOAInstr.IsOnline = true;
            RearInstr.IsOnline = true;
            PhaseInstr.IsOnline = true;
           // RefPDInstr.IsOnline = true;
            //FilterPDInstr.IsOnline = true;
            LockerTxInstr.IsOnline = true;

            //set mz instrument online
            MzCtapInstr.IsOnline = true;
            MzLeftImbInstr.IsOnline = true;
            MzRightImbInstr.IsOnline = true;
            MzLeftModInstr.IsOnline = true;
            MzRightModInstr.IsOnline = true;


            TestOutput("Instrument IsOnline set true OK");


            MainCtlCard.SetDefaultState();
            
            MainCtlCard.OverallmapProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(testInstr_OverallmapProgressChanged);
            MainCtlCard.SupermapProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(testInstr_SupermapProgressChanged);

        }

        /// <summary>
        /// updata supermapping progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void testInstr_SupermapProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Console.WriteLine("SupermapProgress Get Line:" + e.ProgressPercentage);
        }

        /// <summary>
        /// updata overallmapping progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void testInstr_OverallmapProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Console.WriteLine("OverallmapProgress Get Line:" + e.ProgressPercentage);
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("shut down all channel output");

            MainCtlCard.SetDefaultState();

            TestOutput("Don't forget to take the chassis offline!");
            SmutiChassis.IsOnline = false;
            MainCtlCard.IsOnline = false;
            // Test end
            TestOutput("Shut down,test finished!");
        }

        [Test]
        public void T01_DriverNameTest()
        {
            TestOutput("\n\n*** T01_chanssisNameTest ***");
            string chanssisName = MainCtlCard.ChassisName;
            TestOutput(chanssisName);
            string InstrName = MainCtlCard.Name;
            TestOutput(InstrName);
        }

        [Test]
        public void T02_SetAndReadCurrentTest()
        {
            MzCtapInstr.VoltageComplianceSetPoint_Volt = -3.0;
            MzCtapInstr.CurrentSetPoint_Amp = -1.0 / 1000;
            MzCtapInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            double voltageComPliance_volt = MzCtapInstr.VoltageComplianceSetPoint_Volt;
            TestOutput("complianceVoltage = " + voltageComPliance_volt.ToString());
            double current = MzCtapInstr.CurrentSetPoint_Amp;
            TestOutput("set output current = " + current.ToString());
            current = MzCtapInstr.CurrentActual_Amp;
            TestOutput("rea output current = " + current.ToString());
        }

        [Test]
        public void T03_SetAndReadVoltageTest()
        {
            MzCtapInstr.CurrentComplianceSetPoint_Amp = -1.0 / 1000;
            MzCtapInstr.VoltageSetPoint_Volt =-3.0;
            MzCtapInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            double voltage = MzCtapInstr.VoltageSetPoint_Volt;
            TestOutput("set output voltage = " + voltage.ToString());
            voltage = MzCtapInstr.VoltageActual_Volt;
            TestOutput("real output voltage = " + voltage.ToString());
        }

        [Test]
        public void T04_currentSweepTest()
        {
            TestOutput(" do mz LI sweep........" );
            MainCtlCard.SetDefaultState();
            //MzLeftImbInstr.ClearSweepSetting();
            //MzRightImbInstr.ClearSweepSetting();
            //MzLeftModInstr.ClearSweepSetting();
            //MzRightModInstr.ClearSweepSetting();
            //MzCtapInstr.CleanUpSweep();
            //laser setting
            GainInstr.VoltageComplianceSetPoint_Volt = 2;
            GainInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            GainInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            SOAInstr.VoltageComplianceSetPoint_Volt = 2;
            SOAInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            SOAInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            PhaseInstr.CurrentSetPoint_Amp = 4.0 / 1000;
            PhaseInstr.VoltageComplianceSetPoint_Volt = 2;
            PhaseInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            RearInstr.CurrentSetPoint_Amp = 1.473 / 1000;
            RearInstr.VoltageComplianceSetPoint_Volt = 2;
            RearInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            FS6Instr.CurrentSetPoint_Amp = 5.0 / 1000;
            FS6Instr.VoltageComplianceSetPoint_Volt = 2;
            FS6Instr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            FS7Instr.CurrentSetPoint_Amp = 3.575 / 1000;
            FS7Instr.VoltageComplianceSetPoint_Volt = 2;
            FS7Instr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //MZ setting
            MZSweepData sweepData = new MZSweepData();

            CurrentSweepSetupData sweepSetupData = new CurrentSweepSetupData();
            sweepSetupData.leftModBias_V = 0;
            sweepSetupData.rightModBias_V = 0;
            sweepSetupData.tapBias_V = -3.5;
            sweepSetupData.sweepStart_A = 10.0 / 1000;
            sweepSetupData.sweepStop_A = 0.0 / 1000;
            sweepSetupData.stepSize_A = 0.1/1000;
            sweepSetupData.complianceCurr_A = -5.0 / 1000;
            sweepSetupData.complianceVolt_V = 2;
            sweepSetupData.fixedImb_A = 0.0 / 1000;

            double sweepDelay_mS = 1;
            Inst_SMUTI_TriggeredSMU.SweepModes sweepMode = Inst_SMUTI_TriggeredSMU.SweepModes.BOTH;
            Inst_SMUTI_TriggeredSMU.SweepModes CtapSweepMode = Inst_SMUTI_TriggeredSMU.SweepModes.BOTH;
            int TrigInLine = 0;
            int TrigOutLine = 0;


            
            //left mod bais
            MzLeftModInstr.CurrentComplianceSetPoint_Amp = sweepSetupData.complianceCurr_A;
            MzLeftModInstr.VoltageSetPoint_Volt = sweepSetupData.leftModBias_V;

            //right mod bais
            MzRightModInstr.CurrentComplianceSetPoint_Amp = sweepSetupData.complianceCurr_A;
            MzRightModInstr.VoltageSetPoint_Volt = sweepSetupData.rightModBias_V;

            //left imb
            MzLeftImbInstr.InitSourceIMeasureV_CurrentSweep(sweepSetupData.sweepStart_A, sweepSetupData.sweepStop_A,
                sweepSetupData.stepSize_A, sweepSetupData.complianceVolt_V, TrigInLine, TrigOutLine);
            MzLeftImbInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzLeftImbInstr.SweepMode = sweepMode;
            MzLeftImbInstr.TriggerInEnable = false;
            MzLeftImbInstr.SaveSweepSetting(1, Inst_SMUTI_TriggeredSMU.SweepType.Current);
            
            //right imb
            MzRightImbInstr.InitSourceIMeasureV_TriggeredFixedCurrent(sweepSetupData.fixedImb_A, sweepSetupData.complianceVolt_V,
                TrigInLine, TrigOutLine);
            //MzRightImbInstr.InitSourceIMeasureV_CurrentSweep(sweepSetupData.sweepStop_A, sweepSetupData.sweepStart_A,
             //    sweepSetupData.stepSize_A, sweepSetupData.complianceVolt_V, TrigInLine, TrigOutLine);
            MzRightImbInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzRightImbInstr.SweepMode = sweepMode;
            MzRightImbInstr.TriggerInEnable = false;
            MzRightImbInstr.SaveSweepSetting(2, Inst_SMUTI_TriggeredSMU.SweepType.Current);

            //power tap
            MzCtapInstr.InitSourceVMeasureI_TriggeredFixedVoltage(sweepSetupData.tapBias_V, sweepSetupData.complianceCurr_A,
                                                                    TrigInLine, TrigOutLine);
            MzCtapInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzCtapInstr.SweepMode = CtapSweepMode;
            MzLeftModInstr.TriggerInEnable = false;
            MzCtapInstr.SaveSweepSetting(3, Inst_SMUTI_TriggeredSMU.SweepType.Voltage);

            //start sweep,remind:should start from the sweeping channel
            MzLeftImbInstr.StartSweep(1);

            Thread.Sleep(100);

            double[] voltsArray = null;
            double[] currentArray = null;
            //MzLeftModInstr.GetSweepDataSet((int)sweepResultMemNumber.LeftModBias,out currentArray);
            //sweepData.Sweeps[SweepDataType.LeftArmImb_mA] = currentArray;

            //MzRightModInstr.GetSweepDataSet((int)sweepResultMemNumber.RightModBias, out currentArray);
            //sweepData.Sweeps[SweepDataType.RightArmImb_mA] = currentArray;

            //MzLeftImbInstr.GetSweepDataSet((int)sweepResultMemNumber.LeftImb, out voltsArray);
            //sweepData.Sweeps[SweepDataType.LeftArmModBias_V] = voltsArray;

            //MzRightImbInstr.GetSweepDataSet((int)sweepResultMemNumber.RightImb, out voltsArray);
            //sweepData.Sweeps[SweepDataType.RightArmModBias_V] = voltsArray;

            MzCtapInstr.GetMzSweepDataSet(3, out currentArray);
            sweepData.Sweeps[SweepDataType.PowerTap_mA] = currentArray;

            List<string> listSweepData = new List<string>();
            StringBuilder sb = new StringBuilder();
            sb.Append("PowerTap_mA");
            listSweepData.Add(sb.ToString());
            for (int i = 0; i < currentArray.Length; i++)
            {
                sb.Remove(0, sb.Length);
                //sb.Append(sweepData.Sweeps[SweepDataType.LeftArmImb_mA][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.RightArmImb_mA][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.LeftArmModBias_V][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.RightArmModBias_V][i].ToString());
                //sb.Append(",");
                sb.Append(sweepData.Sweeps[SweepDataType.PowerTap_mA][i].ToString());
                listSweepData.Add(sb.ToString());
            }

            string sweepDataFile = MzDataDirectory();
            using (StreamWriter sw = new StreamWriter(sweepDataFile))
            {
                foreach (string var in listSweepData)
                {
                    sw.WriteLine(var);
                }
            }

            TestOutput("mz LI sweep finish ");
        }

        [Test]
        public void T05_VoltageSweepTest()
        {

            TestOutput("do mz LV sweep...... ");
            MainCtlCard.SetDefaultState();
            //laser setting
            GainInstr.VoltageComplianceSetPoint_Volt = 2;
            GainInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            GainInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            SOAInstr.VoltageComplianceSetPoint_Volt = 2;
            SOAInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            SOAInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            PhaseInstr.CurrentSetPoint_Amp = 4.0 / 1000;
            PhaseInstr.VoltageComplianceSetPoint_Volt = 2;
            PhaseInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            RearInstr.CurrentSetPoint_Amp = 1.473 / 1000;
            RearInstr.VoltageComplianceSetPoint_Volt = 2;
            RearInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            FS6Instr.CurrentSetPoint_Amp = 5.0 / 1000;
            FS6Instr.VoltageComplianceSetPoint_Volt = 2;
            FS6Instr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            FS7Instr.CurrentSetPoint_Amp = 3.575 / 1000;
            FS7Instr.VoltageComplianceSetPoint_Volt = 2;
            FS7Instr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //mz sweep settjing
            MZSweepData sweepData = new MZSweepData();
            VoltageSweepSetupData sweepSetupData = new VoltageSweepSetupData();
            sweepSetupData.sweepStart_V = -5;
            sweepSetupData.sweepStop_V = 0;
            sweepSetupData.stepSize_V = 0.1;
            sweepSetupData.leftImbalance_A = 3.448 / 1000;
            sweepSetupData.rightImbalance_A = 3.508 / 1000;
            sweepSetupData.fixedModBias_V = 0.0;
            sweepSetupData.tapBias_V = -3.5;
            sweepSetupData.complianceCurr_A = -5.0/1000;
            sweepSetupData.complianceVolt_V = 2;

            double sweepDelay_mS = 1;

            MzLeftModInstr.CleanUpSweep();
            MzRightModInstr.CleanUpSweep();
            MzCtapInstr.CleanUpSweep();

            Inst_SMUTI_TriggeredSMU.SweepModes sweepMode = Inst_SMUTI_TriggeredSMU.SweepModes.BOTH;
            Inst_SMUTI_TriggeredSMU.SweepModes CtapSweepMode = Inst_SMUTI_TriggeredSMU.SweepModes.BOTH;
            int TrigInLine = 0;
            int TrigOutLine = 0;

            MzLeftModInstr.InitSourceVMeasureI_VoltageSweep(sweepSetupData.sweepStart_V, sweepSetupData.sweepStop_V,
                sweepSetupData.stepSize_V, sweepSetupData.complianceCurr_A, TrigInLine, TrigOutLine);
            MzLeftModInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzLeftModInstr.SweepMode = sweepMode;
            MzLeftModInstr.TriggerInEnable = false;
            MzLeftModInstr.SaveSweepSetting(1, Inst_SMUTI_TriggeredSMU.SweepType.Voltage);

            //MzRightModInstr.InitSourceVMeasureI_TriggeredFixedVoltage(sweepSetupData.fixedModBias_V, sweepSetupData.complianceCurr_A, TrigInLine, TrigOutLine);
            MzRightModInstr.InitSourceVMeasureI_VoltageSweep(sweepSetupData.sweepStop_V, sweepSetupData.sweepStart_V, sweepSetupData.stepSize_V,
                sweepSetupData.complianceCurr_A, TrigInLine, TrigOutLine);
            MzRightModInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzRightModInstr.SweepMode = sweepMode;
            MzRightModInstr.TriggerInEnable = false;
            MzRightModInstr.SaveSweepSetting(2, Inst_SMUTI_TriggeredSMU.SweepType.Voltage);

            MzLeftImbInstr.CurrentSetPoint_Amp = sweepSetupData.leftImbalance_A;
            MzLeftImbInstr.VoltageComplianceSetPoint_Volt = sweepSetupData.complianceVolt_V;
            MzLeftImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            //MzLeftImbInstr.InitSourceIMeasureV_TriggeredFixedCurrent(sweepSetupData.leftImbalance_A, sweepSetupData.complianceVolt_V, TrigInLine, 0);
            //MzLeftImbInstr.SweepDelayTime_mS = sweepDelay_mS;
            //MzLeftImbInstr.SweepMode = sweepMode;
            //MzLeftImbInstr.SaveSweepSetting((int)sweepResultMemNumber.LeftImb);
            MzRightImbInstr.CurrentSetPoint_Amp = sweepSetupData.rightImbalance_A;
            MzRightImbInstr.VoltageComplianceSetPoint_Volt = sweepSetupData.complianceVolt_V;
            MzRightImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            //MzRightImbInstr.InitSourceIMeasureV_TriggeredFixedCurrent(sweepSetupData.rightImbalance_A, sweepSetupData.complianceVolt_V, 0, 0);
            //MzRightImbInstr.SweepDelayTime_mS = sweepDelay_mS;
            //MzRightImbInstr.SweepMode = sweepMode;
            //MzRightImbInstr.SaveSweepSetting((int)sweepResultMemNumber.RightImb);

            MzCtapInstr.InitSourceVMeasureI_TriggeredFixedVoltage(sweepSetupData.tapBias_V, sweepSetupData.complianceCurr_A,
                                                                    TrigInLine, TrigOutLine);
            MzCtapInstr.SweepDelayTime_mS = sweepDelay_mS;
            MzCtapInstr.SweepMode = CtapSweepMode;
            MzCtapInstr.TriggerInEnable = false;
            MzCtapInstr.SaveSweepSetting(3, Inst_SMUTI_TriggeredSMU.SweepType.Voltage);

            //start sweeping
            //MzCtapInstr.StartSweep((int)sweepResultMemNumber.Ctap);
            MzLeftModInstr.StartSweep(1);


            Thread.Sleep(5);

            double[] voltsArray = null;
            double[] currentArray = null;
            //MzLeftModInstr.GetSweepDataSet((int)sweepResultMemNumber.LeftModBias,out currentArray);
            //sweepData.Sweeps[SweepDataType.LeftArmImb_mA] = currentArray;

            //MzRightModInstr.GetSweepDataSet((int)sweepResultMemNumber.RightModBias, out currentArray);
            //sweepData.Sweeps[SweepDataType.RightArmImb_mA] = currentArray;

            //MzLeftImbInstr.GetSweepDataSet((int)sweepResultMemNumber.LeftImb, out voltsArray);
            //sweepData.Sweeps[SweepDataType.LeftArmModBias_V] = voltsArray;

            //MzRightImbInstr.GetSweepDataSet((int)sweepResultMemNumber.RightImb, out voltsArray);
            //sweepData.Sweeps[SweepDataType.RightArmModBias_V] = voltsArray;

            MzCtapInstr.GetMzSweepDataSet(3, out currentArray);
            sweepData.Sweeps[SweepDataType.PowerTap_mA] = currentArray;
            MzLeftModInstr.GetMzSweepDataSet(1, out currentArray);

            List<string> listSweepData = new List<string>();
            StringBuilder sb = new StringBuilder();
            sb.Append("PowerTap_mA");
            listSweepData.Add(sb.ToString());
            for (int i = 0; i < currentArray.Length; i++)
            {
                sb.Remove(0, sb.Length);
                //sb.Append(sweepData.Sweeps[SweepDataType.LeftArmImb_mA][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.RightArmImb_mA][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.LeftArmModBias_V][i].ToString());
                //sb.Append(",");
                //sb.Append(sweepData.Sweeps[SweepDataType.RightArmModBias_V][i].ToString());
                //sb.Append(",");
                sb.Append(sweepData.Sweeps[SweepDataType.PowerTap_mA][i].ToString());
                listSweepData.Add(sb.ToString());
            }

            string sweepDataFile = MzDataDirectory();
            using (StreamWriter sw = new StreamWriter(sweepDataFile))
            {
                foreach (string var in listSweepData)
                {
                    sw.WriteLine(var);
                }
            }

            TestOutput("mz LV sweep finish ");
        }

        [Test]
        public void T06_OverallMapingTest()
        {
            string[] fileOverAllMapResult;
            MainCtlCard.SetDefaultState();
            TestOutput("Instrument initialize........wait a few seconds..... ");
            //mz setting
            MzLeftModInstr.VoltageSetPoint_Volt = 0;// -2.6644;
            MzLeftModInstr.CurrentComplianceSetPoint_Amp = -5.0 / 1000;
            MzLeftModInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzRightModInstr.VoltageSetPoint_Volt = -1.2849;
            MzRightModInstr.CurrentComplianceSetPoint_Amp = -5.0 / 1000;
            MzRightModInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzLeftImbInstr.CurrentSetPoint_Amp = 3.5 / 1000;
            MzLeftImbInstr.VoltageComplianceSetPoint_Volt = 2;
            MzLeftImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzRightImbInstr.CurrentSetPoint_Amp = 3.5 / 1000;
            MzRightImbInstr.VoltageComplianceSetPoint_Volt = 2;
            MzRightImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //Gain setting
            GainInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            GainInstr.VoltageComplianceSetPoint_Volt = 2;
            GainInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //SOA setting
            SOAInstr.CurrentSetPoint_Amp = 150.0 / 1000;
            SOAInstr.VoltageComplianceSetPoint_Volt = 2;
            SOAInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //phase current setting
            PhaseInstr.CurrentSetPoint_Amp = 4.0 / 1000;
            PhaseInstr.VoltageSetPoint_Volt = 2;
            PhaseInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //rear and PD setting
            RearInstr.VoltageComplianceSetPoint_Volt = 2;
            RefPDInstr.VoltageSetPoint_Volt = 3.0;
            FilterPDInstr.VoltageSetPoint_Volt = 3.0;
            RefPDInstr.CurrentComplianceSetPoint_Amp = 3.0 / 1000;
            FilterPDInstr.CurrentComplianceSetPoint_Amp = 3.0 / 1000;

            //front section compliance voltage setting
            FS1Instr.VoltageComplianceSetPoint_Volt = 2;
            FS2Instr.VoltageComplianceSetPoint_Volt = 2;
            FS3Instr.VoltageComplianceSetPoint_Volt = 2;
            FS4Instr.VoltageComplianceSetPoint_Volt = 2;
            FS5Instr.VoltageComplianceSetPoint_Volt = 2;
            FS6Instr.VoltageComplianceSetPoint_Volt = 2;
            FS7Instr.VoltageComplianceSetPoint_Volt = 2;
            FS8Instr.VoltageComplianceSetPoint_Volt = 2;

            //associate rear refPD filterPD FrontSection 
            RearInstr.AssociatedRear();
            RefPDInstr.AssociateOverMapMeasChan(Inst_SMUTI_TriggeredSMU.OverallMapMeaChan.REFerence);
            FilterPDInstr.AssociateOverMapMeasChan(Inst_SMUTI_TriggeredSMU.OverallMapMeaChan.FILTer);
            FS1Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt1);
            FS2Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt2);
            FS3Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt3);
            FS4Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt4);
            FS5Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt5);
            FS6Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt6);
            FS7Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt7);
            FS8Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt8);

            TestOutput("Instrument initialize ok");

            TestOutput("do overAll mapping........");
            fileOverAllMapResult = MainCtlCard.OverAllMpaing(1);
            TestOutput("overAll mapping ok");

        }

        [Test]
        public void T07_SuperModeMapingTest()
        {
            MainCtlCard.SetDefaultState();
            List<string> superModeMappingResult;
            TestOutput("Instrument initialize........ " );
            //mz setting
            MzLeftModInstr.VoltageSetPoint_Volt = 0;// -2.6644;
            MzLeftModInstr.CurrentComplianceSetPoint_Amp = -5.0 / 1000;
            MzLeftModInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzRightModInstr.VoltageSetPoint_Volt = -1.2849;
            MzRightModInstr.CurrentComplianceSetPoint_Amp = -5.0 / 1000;
            MzRightModInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzLeftImbInstr.CurrentSetPoint_Amp = 3.5 / 1000;
            MzLeftImbInstr.VoltageComplianceSetPoint_Volt = 2;
            MzLeftImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            MzRightImbInstr.CurrentSetPoint_Amp = 3.5 / 1000;
            MzRightImbInstr.VoltageComplianceSetPoint_Volt = 2;
            MzRightImbInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            //dsdbr setting
            GainInstr.VoltageComplianceSetPoint_Volt = 2;
            GainInstr.CurrentSetPoint_Amp = 150 / 1000.0;
            GainInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            
            SOAInstr.VoltageComplianceSetPoint_Volt = 2;
            SOAInstr.CurrentSetPoint_Amp = 150 / 1000.0;
            SOAInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            PhaseInstr.VoltageComplianceSetPoint_Volt = 2;
            RearInstr.VoltageComplianceSetPoint_Volt = 2;
            LockerTxInstr.VoltageSetPoint_Volt = 3;
            LockerTxInstr.CurrentComplianceSetPoint_Amp = 3.0 / 1000;
            LockerTxInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            RefPDInstr.VoltageSetPoint_Volt = 3;
            RefPDInstr.CurrentComplianceSetPoint_Amp = 3.0 / 1000;
            RefPDInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;
            FilterPDInstr.VoltageSetPoint_Volt = 3;
            FilterPDInstr.CurrentComplianceSetPoint_Amp = 3.0 / 1000;
            FilterPDInstr.OutputEnable = Inst_SMUTI_TriggeredSMU.OutputState.ON;

            FS1Instr.VoltageComplianceSetPoint_Volt = 2;
            FS2Instr.VoltageComplianceSetPoint_Volt = 2;
            FS3Instr.VoltageComplianceSetPoint_Volt = 2;
            FS4Instr.VoltageComplianceSetPoint_Volt = 2;
            FS5Instr.VoltageComplianceSetPoint_Volt = 2;
            FS6Instr.VoltageComplianceSetPoint_Volt = 2;
            FS7Instr.VoltageComplianceSetPoint_Volt = 2;
            FS8Instr.VoltageComplianceSetPoint_Volt = 2;

            PhaseInstr.AssociatedPhase();
            RearInstr.AssociatedRear();
            bool SMPR = false;
            if (SMPR)
            {
                RefPDInstr.AssociateSupModeMeasChan(Inst_SMUTI_TriggeredSMU.MeasureType.Current, Inst_SMUTI_TriggeredSMU.SupModMapMeaChan.RMEAS);
                FilterPDInstr.AssociateSupModeMeasChan(Inst_SMUTI_TriggeredSMU.MeasureType.Current, Inst_SMUTI_TriggeredSMU.SupModMapMeaChan.TMEAS);
            }
            else 
            LockerTxInstr.AssociateSupModeMeasChan(Inst_SMUTI_TriggeredSMU.MeasureType.Current, Inst_SMUTI_TriggeredSMU.SupModMapMeaChan.TMEAS);


            FS1Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt1);
            FS2Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt2);
            FS3Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt3);
            FS4Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt4);
            FS5Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt5);
            FS6Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt6);
            FS7Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt7);
            FS8Instr.AssociatedFrontSection(Inst_SMUTI_TriggeredSMU.FrontSectionName.FRONt8);
            TestOutput("Instrument initialize ok");

            TestOutput("set middle line data..........");
            MainCtlCard.SetMiddleLineData(@"C:\40G_markII\TestEngine\Plugins\SrcCommon\Equipment\FCUII\Im_DSDBR01_DB123175.030_20100604141401_SM0.csv");
           TestOutput("middle line data setting ok");
           TestOutput("do super mode mapping........");
           superModeMappingResult = MainCtlCard.SuperModeMaping(1, SMPR);
           TestOutput("super mode mapping ok");
            
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        protected string MzDataDirectory()
        {
            string FileName = string.Format("sweepData_DBxxxxxx.xxx_{0:yyyyMMddhhmmss}.csv", DateTime.Now);
            string dir = Directory.GetDirectoryRoot(System.Environment.CurrentDirectory) + "FcuMarkIIResult";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, FileName);
        }


        #endregion

    }

    /// <summary>
    /// A structure to contain all of the data collected from the instruments connected to the MZ
    /// </summary>
    public class MZSweepData
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MZSweepData()
        {
            this.sweeps = new Dictionary<SweepDataType, double[]>();
        }

        /// <summary>
        /// Private dictionary - protects us from losing the entire dictionary!
        /// </summary>
        private Dictionary<SweepDataType, double[]> sweeps;

        /// <summary>
        /// Sweeps dictionary object
        /// </summary>
        public Dictionary<SweepDataType, double[]> Sweeps
        {
            get { return sweeps; }
        }
    }

}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_ElectricalSource.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]


using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_ElectricalSource
    /// </summary>
    public class Inst_SMUTI_ElectricalSource : InstType_ElectricalSource
    {
        private Chassis_SMUTI instChassis;

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_ElectricalSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 

            // 8 Channel High Current Low Voltage card (DSDR High Current )
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord HighCurrentLowVoltageCard = new InstrumentDataRecord(
                "Bookham Tech PLC",				// hardware name 
                "FW:0001",  			// minimum valid firmware version 
                "FW:1001");			// maximum valid firmware version 
            ValidHardwareData.Add("HighCurrentLowVoltageCard", HighCurrentLowVoltageCard);

            //TODO: add other type card

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SMUTI",								// chassis driver name  
                "FW:0001",									// minimum valid chassis driver version  
                "FW:1001");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            instChassis = (Chassis_SMUTI)chassisInit;
        }

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            //TODO:Parse string according return format string
            get { return instChassis.GetSlotID(int.Parse(Slot)); }
        }
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            //TODO:Parse string according return format string
            get { return instChassis.GetSlotID(int.Parse(Slot)); }
        }
        /// <summary>
        /// 
        /// </summary>
        public override void SetDefaultState()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        public override double CurrentActual_amp 
        {
            get 
            {
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                string current_A = instChassis.Query(command, this);
                return double.Parse(current_A);
            } 
        }
        /// <summary>
        /// 
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                double currentCompliance;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                currentCompliance = double.Parse(instChassis.Query(command, this));
                return currentCompliance;
            }
            set
            {
                string command;
                command = string.Format("SOURce{0}:CHANnel{1}:CURRent" + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this);
            }
        }
        /// <summary>
/// 
/// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                double currentSetPoint;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                currentSetPoint = double.Parse(instChassis.Query(command, this));
                return currentSetPoint;
            }
            set
            {
                string command;
                command = string.Format("SOURce{0}:CHANnel{1}:CURRent" + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this);
                OutputEnabled = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                string command;
                EnableState enableState;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut?", this.Slot, this.SubSlot);
                enableState = (EnableState)Enum.Parse(typeof(EnableState), instChassis.Query(command, this));
                if (enableState == EnableState.GND || enableState == EnableState.OFF)
                    return false;
                else
                    return true;
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut" + value.ToString(), this.SubSlot,this.SubSlot);
                instChassis.Write(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enableState"></param>
        public void SetOutputEnabled(EnableState enableState)
        {
            string command;
            command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut" + Enum.GetName(typeof(EnableState), enableState), this.Slot, this.SubSlot);
            instChassis.Write(command, this);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public EnableState GetOutputEnableState()
        {
            string command;
            EnableState outPutState;
            command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut?", this.Slot, this.SubSlot);
            outPutState = (EnableState)Enum.Parse(typeof(EnableState),instChassis.Query(command, this));
            return outPutState;
        }
        /// <summary>
        /// 
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                string voltage_V = instChassis.Query(command, this);
                return double.Parse(voltage_V);
            }
        }
        /// <summary>
/// 
/// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                double voltageCompliance;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                voltageCompliance = double.Parse(instChassis.Query(command, this));
                return voltageCompliance;
            }
            set
            {
                string command;
                command = string.Format("SOURce{0}:CHANnel{1}:VOLTage" + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this);
            }
        }
        /// <summary>
/// 
/// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                double voltageSetPoint;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                voltageSetPoint = double.Parse(instChassis.Query(command, this));
                return voltageSetPoint;
            }
            set
            {
                string command;
                command = string.Format("SOURce{0}:CHANnel{1}:VOLTage" + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this);
            }
        }
        /// <summary>
/// 
/// </summary>
        public enum EnableState
        {
            //
            OFF,
            //
            ON,
            //
            GND
        }


    }
}

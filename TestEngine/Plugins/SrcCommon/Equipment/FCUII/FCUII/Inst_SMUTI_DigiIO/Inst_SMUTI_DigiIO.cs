// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_DigiIO.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_DigiIO
    /// </summary>
    public class Inst_SMUTI_DigiIO : Instrument,IInstType_DigitalIO, IInstType_DigiIOCollection
    {


        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_DigiIO(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Processor, 4 TriggerLink lines, 
            // & 8 Channel Opto-isolated TTL SPI Digital IO card
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord DigitalIOData = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            DigitalIOData.Add("Processor", "STM32F101VB");
            ValidHardwareData.Add("DigitalIOData", DigitalIOData);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SMUTI",								// chassis driver name  
                "FW:0001",									// minimum valid chassis driver version  
                "FW:1001");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_SMUTI)chassisInit;

            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(8);
            //for (int ii = 1; ii <= 8; ii++)
            //{
            //    digiOutLines.Add(new Inst_SMUTI_DigiOutLine(instrumentNameInit + "_D" + ii, instrumentChassis, ii));
            //}
        }

        #region Public Instrument Methods and Properties
        /// <summary>
        /// get FirmwareVersion
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            { 
                return instrumentChassis.FirmwareVersion;
            }
        }
        /// <summary>
        /// get HardwareIdentity
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }
        /// <summary>
        /// Get/Set instrument online status
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                foreach (IInstType_DigitalIO dio in digiOutLines)
                {
                    dio.IsOnline = value;
                }
            }
        }
        /// <summary>
        /// set digital IO output state
        /// </summary>
        public override void SetDefaultState()
        {
            for (int i = 0; i < 8; i++)
            {
                digLineNumber = i;
                LineState = false;
                System.Threading.Thread.Sleep(50);
            }
        }
        #endregion

        #region IInstType_DigitalIO Members

        public int digLineNumber;
        /// <summary>
        ///  Line state,Get lineState,set the specified digital IO State
        /// </summary>
        public bool LineState
        {
            get
            {
                bool linesState = false;
                for (int i = 0; i < 8; i++)
                {
                    linesState =linesState | GetLineState;
                }
                return linesState;
            }
            set
            {
                SetLineState = value;
            }
        }
        /// <summary>
        /// get dedicated DI value on the processor card
        /// </summary>
        public bool GetDIValue
        {
            get
            {
                string resp = instrumentChassis.Query(":SENSe1:CHANne" + digLineNumber + ":TTL?", this);
                bool dIValue = bool.Parse(resp);
                return dIValue;
            }
        }
        /// <summary>
        /// get Line state
        /// </summary>
        private bool GetLineState
        {
            get
            {
                string resp = instrumentChassis.Query(":SOURce1:CHANne" + this.SubSlot + ":TTL?", this);
                bool lineState = bool.Parse(resp);
                // return it
                return lineState;
            }
        }
        /// <summary>
        /// Set Line State
        /// </summary>
        private bool SetLineState
        {
            set
            {
                instrumentChassis.Write(":SOURce1:CHANne" + this.SubSlot + ":TTL" + value.ToString(), this);
            }
        }

        #endregion

        #region IInstType_DigiIOCollection Members
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get
            {
                return digiOutLines;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
           
            return digiOutLines[lineNumber-1];

        }

        #endregion

        #region Private Data

        // Chassis reference
        internal Chassis_SMUTI instrumentChassis;
        internal List<IInstType_DigitalIO> digiOutLines;
        #endregion                        
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_TriggeredSMU.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.ComponentModel;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_TriggeredSMU
    /// </summary>
    public class Inst_SMUTI_TriggeredSMU : InstType_ElectricalSource
    {
        private Chassis_SMUTI instChassis;

        #region some enum variable define
        /// <summary>
        /// channel output state
        /// </summary>
        public enum OutputState
        {
            /// <summary>
            /// output off
            /// </summary>
            OFF,
            /// <summary>
            /// output on
            /// </summary>
            ON,
            /// <summary>
            /// output connect gnd
            /// </summary>
            GND
        }

        /// <summary>
        /// Front section name
        /// </summary>
        public enum FrontSectionName
        {
            /// <summary>
            /// 
            /// </summary>
            FRONt1,
            /// <summary>
            /// 
            /// </summary>
            FRONt2,
            /// <summary>
            /// 
            /// </summary>
            FRONt3,
            /// <summary>
            /// 
            /// </summary>
            FRONt4,
            /// <summary>
            /// 
            /// </summary>
            FRONt5,
            /// <summary>
            /// 
            /// </summary>
            FRONt6,
            /// <summary>
            /// 
            /// </summary>
            FRONt7,
            /// <summary>
            /// 
            /// </summary>
            FRONt8
        }

        /// <summary>
        /// over all mapping measure channel
        /// </summary>
        public enum OverallMapMeaChan
        {
            /// <summary>
            /// Reference channel
            /// </summary>
            REFerence,

            /// <summary>
            /// Filter channel
            /// </summary>
            FILTer

        }

        /// <summary>
        /// measure type,voltage or current measure
        /// </summary>
        public enum MeasureType
        {
            /// <summary>
            /// measure voltage
            /// </summary>
            Voltage,
            /// <summary>
            ///measure current 
            /// </summary>
            Current
        }

        /// <summary>
        /// enum of super mode mapping measure channel,Tx or Rx
        /// </summary>
        public enum SupModMapMeaChan
        {
            /// <summary>
            /// Associate to Tx channel
            /// </summary>

            TMEAS,

            /// <summary>
            /// Associate to Rx channel
            /// </summary>

            RMEAS
        }

        /// <summary>
        /// sweep mode
        /// </summary>
        public enum SweepModes
        {
            /// <summary>
            /// can only use as power supply
            /// </summary>

            ONLYPOWER,
            /// <summary>
            /// can only use as Multimeter
            /// </summary>

            ONLYMETER,
            /// <summary>
            /// can use as power supply and multimeter
            /// </summary>

            BOTH
        }

        /// <summary>
        /// sweep direction ,forward or reverse
        /// </summary>
        protected enum SweepDirection
        {

            /// <summary>
            /// forward sweep
            /// </summary>
            forward,
            /// <summary>
            /// reverse sweep
            /// </summary>
            reverse,
        }

         /// <summary>
        /// 
        /// </summary>
        public enum AllChannelName
        {
            /// <summary>
            /// 
            /// </summary>
            TestInstr,
            /// <summary>
            /// 
            /// </summary>
            FS1Instr,
            /// <summary>
            /// 
            /// </summary>
            FS2Instr,
            /// <summary>
            /// 
            /// </summary>
            FS3Instr,
            /// <summary>
            /// 
            /// </summary>
            FS4Instr,
            /// <summary>
            /// 
            /// </summary>
            FS5Instr,
            /// <summary>
            /// 
            /// </summary>
            FS6Instr,
            /// <summary>
            /// 
            /// </summary>
            FS7Instr,
            /// <summary>
            /// 
            /// </summary>
            FS8Instr,

            /// <summary>
            /// 
            /// </summary>
            GainInstr,
            /// <summary>
            /// 
            /// </summary>
            SOAInstr,
            /// <summary>
            /// 
            /// </summary>
            RearInstr,
            /// <summary>
            /// 
            /// </summary>
            PhaseInstr,

            /// <summary>
            /// 
            /// </summary>
            RefPDInstr,
            /// <summary>
            /// 
            /// </summary>
            FilterPDInstr,
            /// <summary>
            /// 
            /// </summary>
            LockerTxInstr,
            /// <summary>
            /// 
            /// </summary>
            LockerRxInstr,

            /// <summary>
            /// MZ instrment
            /// </summary>
            MzCtapInstr,
            /// <summary>
            /// 
            /// </summary>
            MzLeftModInstr,
            /// <summary>
            /// 
            /// </summary>
            MzRightModInstr,
            /// <summary>
            /// 
            /// </summary>
            MzLeftImbInstr,
            /// <summary>
            /// 
            /// </summary>
            MzRightImbInstr,
            /// <summary>
            /// 
            /// </summary>
            RearSoaInstr,
            /// <summary>
            /// 
            /// </summary>
            MainInstr
        }

        /// <summary>
        /// sweep ,overall map or super mode map
        /// </summary>
        public enum Sweep
        {
            /// <summary>
            /// 
            /// </summary>
            OverallMap,
            /// <summary>
            /// 
            /// </summary>
            SuperMap
        }

        /// <summary>
        /// sweep mode, voltage sweep(LV) or current sweep(LI)
        /// </summary>
        public enum SweepType
        {
            /// <summary>
            /// voltage sweep(LV)
            /// </summary>

            Voltage,
            /// <summary>
            /// current sweep(LI)
            /// </summary>

            Current
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public event ProgressChangedEventHandler OverallmapProgressChanged;
        /// <summary>
        /// 
        /// </summary>
        public event ProgressChangedEventHandler SupermapProgressChanged;

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_TriggeredSMU(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 

            // 8 Channel High Current Low Voltage card (DSDR High Current )
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord HighCurrentLowVoltageCard = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("HighCurrentLowVoltageCard", HighCurrentLowVoltageCard);

            //TODO: add other type card

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SMUTI",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            //instChassis = (Chassis_SMUTI)chassisInit;
            instChassis = (Chassis_SMUTI)base.InstrumentChassis;

            SetPointValue.CurrentSetValue.AddDouble(Slot + SubSlot, 0);

            SetPointValue.VoltageSetValue.AddDouble(Slot + SubSlot, 0);

        }

        #region Inst_SMUTI_TriggeredSMU function


        double fixedSweepCurrent = 0;
        double fixedSweepVoltage = 0;
        /// <summary>
        /// 这里需要调试！！！
        /// </summary>
        public double FixedSweepCurrent
        {
            get
            {
                return fixedSweepCurrent;
            }
            set
            {
                fixedSweepCurrent = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double FixedSweepVoltage
        {
            get
            {
                return fixedSweepVoltage;
            }
            set
            {
                fixedSweepVoltage = value;
            }
        }

        /// <summary>
        /// Cleans up a sweep, aborts it and clears the triggering and traces
        /// </summary>
        public void CleanUpSweep()
        {
            instChassis.Clear();
            OutputEnabled = false;
            DisableTriggering();
            ClearSweepSetting();


        }

        /// <summary>
        /// clear sweep sweep memory settings
        /// </summary>
        public void ClearSweepSetting()
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:MEM:CLE ALL", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// Configure Triggerlines
        /// </summary>
        /// <param name="inputTriggerLine"></param>
        /// <param name="outputTriggerLine"></param>
        public void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine)
        {

            TriggerInLineNumber = inputTriggerLine;
            TriggerOutLineNumber = outputTriggerLine;
        }

        /// <summary>
        /// disable Triigger setting
        /// </summary>
        public void DisableTriggering()
        {
            this.TriggerInLineNumber = 0;
            this.TriggerOutLineNumber = 0;
        }

        /// <summary>
        /// InitSourceIMeasureV_CurrentSweep
        /// </summary>
        /// <param name="Istart_A">sweep start current in Amp</param>
        /// <param name="Istop_A">sweep stop current in Amp</param>
        /// <param name="Istep_A">sweep step current in Amp</param>
        /// <param name="Vcompliance_V">compliance current in Amp</param>
        /// <param name="TrigInLine">trigger in line number</param>
        /// <param name="TrigOutLine">trigger out line number</param>
        public void InitSourceIMeasureV_CurrentSweep(double Istart_A, double Istop_A, double Istep_A, double Vcompliance_V, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartCurrent_Amp = Istart_A;
            this.SweepStopCurrent_Amp = Istop_A;
            this.SweepStepCurrent_Amp = Istep_A;
            this.VoltageComplianceSetPoint_Volt = Vcompliance_V;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);
        }

        /// <summary>
        /// InitSourceVMeasureI_VoltageSweep,when Triggerin parameter > 0,configure Trigger line signal
        /// </summary>
        /// <param name="Vstart_V">sweep start voltage at V</param>
        /// <param name="Vstop_V">sweep stop voltage at V</param>
        /// <param name="Vstep_V">sweep step voltage at V</param>
        /// <param name="Icompliance_A">compliance current at amp</param>
        /// <param name="TrigInLine">trigger in line number,when this param > 0,set trigger in</param>
        /// <param name="TrigOutLine">trigger out line number</param>
        public void InitSourceVMeasureI_VoltageSweep(double Vstart_V, double Vstop_V, double Vstep_V, double Icompliance_A, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartVoltage_Volt = Vstart_V;
            this.SweepStopVoltage_Volt = Vstop_V;
            this.SweepStepVoltage_Volt = Vstep_V;
            this.CurrentComplianceSetPoint_Amp = Icompliance_A;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);

        }

        /// <summary>
        /// InitSourceVMeasureI_VoltageSweepFixedCurrent
        /// </summary>
        /// <param name="iFixed_A">current set on the fixed pin</param>
        /// <param name="Vcompliance_V">compliance voltage</param>
        /// <param name="TrigInLine">TrigIn Line number </param>
        /// <param name="TrigOutLine">TrigOut Line number</param>
        public void InitSourceIMeasureV_TriggeredFixedCurrent(double iFixed_A, double Vcompliance_V, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartCurrent_Amp = iFixed_A;
            this.SweepStopCurrent_Amp = iFixed_A;
            this.SweepStepCurrent_Amp = 0;
            this.VoltageComplianceSetPoint_Volt = Vcompliance_V;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);
        }

        /// <summary>
        /// Init Source V Measure I ,Triggered at Fixed Voltage
        /// </summary>
        /// <param name="vFixed_V">voltage set on the fixed pin</param>
        /// <param name="Icompliance_A">compliance current</param>
        /// <param name="TrigInLine">trig in line number</param>
        /// <param name="TrigOutLine">trig out line number</param>
        public void InitSourceVMeasureI_TriggeredFixedVoltage(double vFixed_V, double Icompliance_A, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartVoltage_Volt = vFixed_V;
            this.SweepStopVoltage_Volt = vFixed_V;
            this.SweepStepVoltage_Volt = 0;
            this.CurrentComplianceSetPoint_Amp = Icompliance_A;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);

        }

        #endregion

        #region Instrument override
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            //TODO:Parse string according return format string
            get
            {
                return instChassis.FirmwareVersion;
            }
        }
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            //TODO:Parse string according return format string
            get
            {
                return instChassis.HardwareIdentity;
            }
        }
        /// <summary>
        /// this function can't not support,not support reset single port statues
        /// </summary>
        public override void SetDefaultState()
        {
            // throw new Exception("this function can't not support");
           //instChassis.SetDefaultState();
        }

        /// <summary>
        ///  Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region InstType_ElectricalSource override

        /// <summary>
        ///  Properties of compliance current value 
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                try
                {
                    double currentCompliance;
                    string command;
                    command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                    string current = instChassis.Query(command, instChassis.serialPort_FCUII);
                    currentCompliance = double.Parse(current);
                    return currentCompliance;
                }
                catch (Exception)
                {
                    double returnData = 1.0;
                    return returnData;
                }
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:CURRent " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
                double currTemp = CurrentComplianceSetPoint_Amp;
            }
        }

        /// <summary>
        /// get the real current output
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                string current_A = SetPointValue.CurrentSetValue.ReadDouble(this.Slot + this.SubSlot).ToString();

                //这里只回度LockerTx 和LockerRx 

                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                // if (this.Slot.Contains("6") && this.SubSlot.Contains("1") || this.Slot.Contains("6") && this.SubSlot.Contains("2"))//
                {
                    try
                    {
                        current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                        //string current_A = get_ActualCurrent;
                        //if (this.Slot.Contains("4") && this.SubSlot.Contains("1"))
                        //{ current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII); }
                        //return double.Parse(current_A);
                        double.Parse(current_A);
                    }
                    catch
                    {
                        current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                        double.Parse(current_A);
                    }
                }
                return double.Parse(current_A);
            }
        }

        /// <summary>
        /// Properties of current value of set point 
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                double currentSetPoint;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                string current;//  = instChassis.Query(command, instChassis.serialPort_FCUII);
                try
                {
                    current = instChassis.Query(command, instChassis.serialPort_FCUII);
                    currentSetPoint = double.Parse(current);
                }
                catch
                {
                    current = instChassis.Query(command, instChassis.serialPort_FCUII);
                    currentSetPoint = double.Parse(current);
                }
                return currentSetPoint;
            }
            set
            {
                //string TempFile;
                string command;
                double CurrTemp = 0;
                value = Math.Round(value, 7);

                //This modify just for the only front section is 0.0 for MK2;
                if (value == 0.0)
                {
                    value = 0.000001;
                }

                get_ActualCurrent = value.ToString();
                command = string.Format(":SOURce{0}:CHANnel{1}:CURRent " + value.ToString(), this.Slot, this.SubSlot);
                SetPointValue.CurrentSetValue.UpdateDouble(this.Slot + this.SubSlot, value);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
                CurrTemp = CurrentSetPoint_amp;
                ////if (((CurrTemp / value > 1.2 || CurrTemp / value < 0.8) && value > 0.0001))
                ////{
                ////    //string[] temp ={DateTime.Now.ToString(), this.Slot, this.SubSlot, value, CurrTemp, CurrTemp / value};
                ////    string TempFile1 = string.Format(":Time{0},Slot:{1},SubSlot:{2},SetCurr:{3},SenseCurr:{4},Error:{5}", DateTime.Now.ToString("yyyyMMddHHmmssff"),
                ////    this.Slot, this.SubSlot, value, CurrTemp, CurrTemp / value);
                ////    string commandOn;
                ////    //commandOn = string.Format(":SOURce{0}:CHANnel{1}:OUTPut " + Enum.GetName(typeof(OutputState), value), this.Slot, this.SubSlot);
                ////    instChassis.Write(command, this.instChassis.serialPort_FCUII);

                ////    CurrTemp = CurrentSetPoint_Amp;
                ////    GetTheSetValue.sw.WriteLine(TempFile1);
                ////    GetTheSetValue.sw.Flush();
                ////    if ((CurrTemp / value > 1.2 || CurrTemp / value < 0.8) && value > 0.0001)
                ////    {
                ////        string TempFile2 = string.Format("FlagError:Time{0},Slot:{1},SubSlot:{2},SetCurr:{3},SenseCurr:{4},Error:{5}", DateTime.Now.ToString("yyyyMMddHHmmssff"),
                ////    this.Slot, this.SubSlot, value, CurrTemp, CurrTemp / value);
                ////        GetTheSetValue.sw.WriteLine(TempFile2);
                ////        GetTheSetValue.sw.Flush();
                ////    }
                ////}
            }
        }

        /// <summary>
        /// get the rear voltage output
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);

                try
                {
                    string voltage_V = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                    //string voltage_V = get_ActualVoltage;
                    return double.Parse(voltage_V);
                }
                catch
                {
                    string voltage_V = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                    //string voltage_V = get_ActualVoltage;
                    return double.Parse(voltage_V);
                }
            }
        }

        /// <summary>
        /// set/get compliance voltage
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                double voltageCompliance;
                string command;
                instChassis.serialPort_FCUII.DiscardInBuffer();
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                string voltAge = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                voltageCompliance = double.Parse(voltAge);
                return voltageCompliance;
            }
            set
            {
                string command;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                command = string.Format(":SOURce{0}:CHANnel{1}:VOLTage " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// get/set voltage set point
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                double voltageSetPoint;
                string command;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                voltageSetPoint = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return voltageSetPoint;
            }
            set
            {
                //string VolTempFile;
                string command;
                double volTemp = 0;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                value = Math.Round(value, 7);
                get_ActualVoltage = value.ToString();
                command = string.Format(":SOURce{0}:CHANnel{1}:VOLTage " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
                volTemp = VoltageActual_Volt;
                ////if (Math.Abs(value) > 0.01 && (volTemp / value > 1.05 || volTemp / value < 0.95))
                ////{
                ////    string VolTempFile1 = string.Format(":Time{0},Slot:{1},SubSlot:{2},SetVol:{3},SenseVol:{4},Error:{5}", DateTime.Now.ToString("yyyyMMddHHmmssff"),
                ////    this.Slot, this.SubSlot, value, volTemp, volTemp / value);
                ////    GetTheSetValue.sw.WriteLine(VolTempFile1);
                ////    GetTheSetValue.sw.Flush();
                ////    instChassis.Write(command, this.instChassis.serialPort_FCUII);
                ////    volTemp = VoltageActual_Volt;
                ////    if (Math.Abs(value) > 0.01 && (volTemp / value > 1.05 || volTemp / value < 0.95))
                ////    {
                ////        string VolTempFile2 = string.Format("FlagError:Time{0}, Slot:{1}, SubSlot:{2}, SetVol:{3}, SenseVol:{4}, Error:{5} ", DateTime.Now.ToString("yyyyMMddHHmmssff"),
                ////    this.Slot, this.SubSlot, value, volTemp, volTemp / value);
                ////        GetTheSetValue.sw.WriteLine(VolTempFile2);
                ////        GetTheSetValue.sw.Flush();
                ////    }
                ////}
            }
        }

        /// <summary>
        /// Sets/returns the output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut?", this.Slot, this.SubSlot);
                string outputStatus = instChassis.Query(command, instChassis.serialPort_FCUII);
                switch (outputStatus)
                {
                    case "LOW or OFF":
                        outputStatus = "OFF";
                        break;
                    case "HIGH or ON":
                        outputStatus = "ON";
                        break;
                    default:
                        break;
                }
                OutputState outputEnable = (OutputState)Enum.Parse(typeof(OutputState), outputStatus);

                if (outputEnable == OutputState.OFF || outputEnable == OutputState.GND)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            set
            {
                OutputState outputState = OutputState.OFF;
                if (value)
                {
                    outputState = OutputState.ON;
                }
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut " + Enum.GetName(typeof(OutputState), outputState), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        #endregion

        /// <summary>
        /// get/set outputState ,GND/ON/OFF
        /// </summary>
        public OutputState OutputEnable
        {
            get
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut?", this.Slot, this.SubSlot);
                string outputStatus = instChassis.Query(command, instChassis.serialPort_FCUII);
                switch (outputStatus)
                {
                    case "LOW or OFF":
                        outputStatus = "OFF";
                        break;
                    case "HIGH or ON":
                        outputStatus = "ON";
                        break;
                    default:
                        break;
                }
                OutputState outputEnable = (OutputState)Enum.Parse(typeof(OutputState), outputStatus);
                return outputEnable;
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut " + Enum.GetName(typeof(OutputState), value), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        #region Basic Operation Function

        /// <summary>
        /// set the channel sweepMode,determine if source or sense or both is swept
        /// </summary>
        public SweepModes SweepMode
        {
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:MODE " + Enum.GetName(typeof(SweepModes), value), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set channel to single sweep or continuous sweep,
        /// true: set to single sweep,
        /// false: set to continuous sweep,
        /// single sweep: no need Trigger in signal but can provide Trigger out singal,
        /// continuous sweep: need Trigger in signal and can provide Trigger out singal,like work on Trigger links mode
        /// </summary>
        public bool TriggerInEnable
        {
            set
            {
                string enableState = "OFF";
                if (value)
                    enableState = "ON";
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:SINGl " + enableState, this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep delay time ,minimum delay time need to >2mS
        /// </summary>
        public double SweepDelayTime_mS
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:DEL?", this.Slot, this.SubSlot);
                double sweepDelayTime = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII)) * 1000;
                return sweepDelayTime;
            }
            set
            {
                string command;
                double delay_s = value / 1000;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:DEL " + delay_s.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep start voltage
        /// </summary>
        public double SweepStartVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STAR?", this.Slot, this.SubSlot);
                double sweepStartVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStartVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STAR " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// get/set sweep start current,0--20mA
        /// </summary>
        public double SweepStartCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STAR?", this.Slot, this.SubSlot);
                double sweepStartCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStartCurrent;
            }
            set
            {
                string command;
                double sweepStartCurrent = value;
                if (sweepStartCurrent > 20.0 / 1000 || sweepStartCurrent < 0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STAR " + sweepStartCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep stop voltage
        /// </summary>
        public double SweepStopVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STOP?", this.Slot, this.SubSlot);
                double sweepStopVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStopVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STOP " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep stop current,0--20mA
        /// </summary>
        public double SweepStopCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STOP?", this.Slot, this.SubSlot);
                double sweepStopCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStopCurrent;
            }
            set
            {
                string command;
                double sweepStopCurrent = value;
                if (sweepStopCurrent > 20.0 / 1000 || sweepStopCurrent < 0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STOP " + sweepStopCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep step voltage 
        /// </summary>
        public double SweepStepVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STEP?", this.Slot, this.SubSlot);
                double sweepStepVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStepVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STEP " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep step current
        /// </summary>
        public double SweepStepCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STEP?", this.Slot, this.SubSlot);
                double sweepStepCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStepCurrent;
            }
            set
            {
                string command;
                double sweepStepCurrent = value;
                if (sweepStepCurrent > 20.0 / 1000 || sweepStepCurrent < 0.0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STEP " + sweepStepCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// Starts the sweep for the specify memory location
        /// </summary>
        /// <param name="memoryNumber">memory number</param>
        public void StartSweep(int memoryNumber)
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:MEM:IMM " + memoryNumber.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// Clear Up all Source Sweep Setting
        /// </summary>
        public void ClearUpSourSweepSetting()
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:CLE ALL", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// Clear up the specify source sweep settings
        /// </summary>
        /// <param name="MemoryNumber">memory number</param>
        public void ClearUpSourSweepSetting(int MemoryNumber)
        {
            string command;
            if (MemoryNumber > 8 || MemoryNumber < 1)
                throw new Exception("invalid Memory Number, the Sweep Memory Number should be between 1 to 5 ");
            else
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CLE " + MemoryNumber.ToString(), this.Slot, this.SubSlot);

            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// Save the cached sweep settings to the source sweep list,1--8
        /// </summary>
        /// <param name="MemoryNumber">memory number,this number also is save sweep result memory number</param>
        /// <param name="sweepType">sweep type</param>
        public void SaveSweepSetting(int MemoryNumber, SweepType sweepType)
        {
            string command;
            if (MemoryNumber > 8 || MemoryNumber < 1)
            {
                throw new Exception("invalid Memory Number, the Memory Number should be between 1 to 8 ");
            }
            else
            {
                switch (sweepType)
                {
                    case SweepType.Voltage:
                        command = string.Format(":SOUR{0}:CHAN{1}:SWE:Volt:SAV " + MemoryNumber.ToString(), this.Slot, this.SubSlot);
                        break;
                    case SweepType.Current:
                        command = string.Format(":SOUR{0}:CHAN{1}:SWE:Curr:SAV " + MemoryNumber.ToString(), this.Slot, this.SubSlot);
                        break;
                    default:
                        throw new Exception("Invalid parameter");
                        //break;
                }
            }
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// get MZ sweep result
        /// </summary>
        /// <param name="MemoryNumber">memory number that saved sweep result</param>
        /// <param name="measureData">sweep result</param>
        public void GetMzSweepDataSet(int MemoryNumber, out double[] measureData)
        {
            string command;
            command = string.Format(":SENS{0}:CHAN{1}:SWE:MEM " + MemoryNumber.ToString(), this.Slot, this.SubSlot);
            instChassis.WriteLine(command, this.instChassis.serialPort_FCUII);
            StringBuilder sb = new StringBuilder();
            string readData;
            string strReadData = "";
            do
            {
                try
                {
                    readData = instChassis.serialPort_FCUII.ReadLine();
                }
                catch (Exception)
                {
                    //clear read buffer
                    instChassis.serialPort_FCUII.DiscardInBuffer();
                    //rewrite the command
                    instChassis.serialPort_FCUII.WriteLine(command + "\r");
                    readData = instChassis.serialPort_FCUII.ReadLine();

                }

                if (readData.Contains("START"))
                {
                    string sTemp = readData;
                    continue;
                }
                if (readData.Contains("END")) break;
                strReadData = readData;

            } while (true);
            instChassis.serialPort_FCUII.ReadLine();
            //instChassis.serialPort_FCUII.ReadLine();
            System.Threading.Thread.Sleep(20);
            string[] tempStr = strReadData.Split(',');
            measureData = new Double[tempStr.Length];

            for (int i = 0; i < tempStr.Length; i++)
            {
                try
                {
                    measureData[i] = double.Parse(tempStr[i].Trim());
                }
                catch (Exception)
                {
                    string strTmp = tempStr[i].Trim();
                }
            }

        }

        /// <summary>
        /// set/get which trigger-out line is associated with which channel 
        /// </summary>
        private int TriggerOutLineNumber
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:TRIG?", this.Slot, this.SubSlot);
                int triggerOutLineNumber = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return triggerOutLineNumber;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:TRIG " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sence trigger in line 
        /// </summary>
        private int TriggerInLineNumber
        {
            get
            {
                string command;
                command = string.Format(":SENS{0}:CHAN{1}:SWE:TRIG?", this.Slot, this.SubSlot);
                string number = instChassis.Query(command, instChassis.serialPort_FCUII);
                int triggerInLineNumber = int.Parse(number);
                return triggerInLineNumber;
            }
            set
            {
                string command;
                command = string.Format(":SENS{0}:CHAN{1}:SWE:TRIG " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// reset
        /// </summary>
        public void RestFcuMark2()
        {
            string commad;
            commad = "*rst";
            instChassis.Write(commad, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// set channel associated to Rear,when do mapping,need to use this function
        /// </summary>
        public void AssociatedRear()
        {

            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR REAR", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// set channel associated to Phase
        /// </summary>
        public void AssociatedPhase()
        {

            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR PHAS", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// set channel associated to FrontSection
        /// </summary>
        /// <param name="FrontSection"> front section name</param>
        public void AssociatedFrontSection(FrontSectionName FrontSection)
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR " + FrontSection.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// when do super mode mapping,set channel associate to Tx or Rx
        /// </summary>
        /// <param name="measrueType">measure Tpye,voltage or current</param>
        /// <param name="measureChan">measure channel,Tx or Rx</param>
        public void AssociateSupModeMeasChan(MeasureType measrueType, SupModMapMeaChan measureChan)
        {
            string command;
            string meaType = Enum.GetName(typeof(MeasureType), measrueType);
            string meaChan = Enum.GetName(typeof(SupModMapMeaChan), measureChan);
            command = string.Format(":SENS{0}:CHAN{1}:" + meaType + " " + meaChan, this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// when do overall mapping,set channel associated to Reference measure or filter measure channel
        /// </summary>
        /// <param name="overallMapMeasureType"> measure channel</param>
        public void AssociateOverMapMeasChan(OverallMapMeaChan overallMapMeasureType)
        {
            string command;
            string meaType = Enum.GetName(typeof(OverallMapMeaChan), overallMapMeasureType);
            command = string.Format(":SENS{0}:CHAN{1}:CURRent " + meaType, this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// do overall maping,return overall mapping data
        /// </summary>
        /// <param name="delayTime_mS">overall mapping sweep delay time</param>
        /// <returns>file of overall map result</returns>
        public List<string> OverAllMpaing(double delayTime_mS)
        {
            double indexLines = 0;
            string command;
            double delayTime_S = delayTime_mS / 1000;
            command = string.Format(":OMAP DEL" + delayTime_S.ToString());
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

            List<string> OverallMapingData = new List<string>();
            List<string> referPdMappingData = new List<string>();
            List<string> filterPdMappingData = new List<string>();
            string readData;
            //string[] result;
            try
            {
                do
                {
                    readData = instChassis.serialPort_FCUII.ReadLine();

                    if (readData.Contains("STAR")) continue;
                    if (readData.Contains("END")) break;
                    if (readData == " ") break;

                    //updata overMapping Percent
                    indexLines++;
                    //double temp = Math.Round(indexLine / overMappingMaxLines, 2) * 100;
                    double temp = indexLines;
                    this.overMappingPercent = int.Parse(temp.ToString());
                    double powerRatio = 999;

                    //List<string> listResult = new List<string>();
                    if (OverallmapProgressChanged != null)
                    {
                        ProgressChangedEventArgs arg = new ProgressChangedEventArgs(overMappingPercent, null);
                        OverallmapProgressChanged(this.instChassis.serialPort_FCUII, arg);
                    }


                    string[] tempData = readData.Split(',');
                    StringBuilder strPowerRatio = new StringBuilder();
                    StringBuilder strReferPD = new StringBuilder();
                    StringBuilder strFilterPD = new StringBuilder();
                    for (int i = 0; i < tempData.Length; i++)
                    {
                        if (tempData[i] != "" && tempData[i] != ",")
                        {
                            bool isDigit = true;
                            string[] array = tempData[i].Split('&');
                            for (int j = 0; j < array.Length; j++)
                            {
                                foreach (char var in array[j].Trim())
                                {
                                    if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                    {
                                        isDigit = false;
                                        break;
                                    }
                                }
                            }

                            string[] array2 = tempData[i].Split('&');
                            if (isDigit)
                            {
                                if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                    powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                else
                                    powerRatio = 999;
                            }
                            else
                                powerRatio = 999;

                            strPowerRatio.Append(powerRatio.ToString("#0.0000"));
                            strPowerRatio.Append(",");
                            strFilterPD.Append(array2[0].Trim());
                            strFilterPD.Append(",");
                            strReferPD.Append(array2[1].Trim());
                            strReferPD.Append(",");
                        }

                    }
                    //remove the last ","
                    strPowerRatio.Remove(strPowerRatio.Length - 1, 1);
                    strFilterPD.Remove(strFilterPD.Length - 1, 1);
                    strReferPD.Remove(strReferPD.Length - 1, 1);

                    OverallMapingData.Add(strPowerRatio.ToString());
                    referPdMappingData.Add(strReferPD.ToString());
                    filterPdMappingData.Add(strFilterPD.ToString());

                } while (readData != " ");
            }
            catch (Exception)
            {
                throw;
            }
            //make result file
            string overallMapDataFileName = DataDirectory("OverMap_PowerRatio", SweepDirection.forward, Sweep.OverallMap);
            string referPdMapDataFileName = DataDirectory("ReferPD_OverMap_", SweepDirection.forward, Sweep.OverallMap);
            string filterPdMapDataFileName = DataDirectory("FilterPD_OverMap_", SweepDirection.forward, Sweep.OverallMap);

            List<string> overallMapResultFileName = new List<string>();
            overallMapResultFileName.Add(overallMapDataFileName);
            overallMapResultFileName.Add(referPdMapDataFileName);
            overallMapResultFileName.Add(filterPdMapDataFileName);

            Dictionary<string, List<string>> overallMapResult = new Dictionary<string, List<string>>();
            overallMapResult.Add(overallMapDataFileName, OverallMapingData);
            overallMapResult.Add(referPdMapDataFileName, referPdMappingData);
            overallMapResult.Add(filterPdMapDataFileName, filterPdMappingData);

            foreach (string fileName in overallMapResultFileName)
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (string var in overallMapResult[fileName])
                    {
                        sw.WriteLine(var);
                    }
                    sw.Close();
                }
            }
            
            //return overallMapDataFileName;
            List<string> overallMapData = new List<string>();
            //string[] overallMapData = new string[] { referPdMapDataFileName, filterPdMapDataFileName };
            overallMapData.Add(referPdMapDataFileName);
            overallMapData.Add(filterPdMapDataFileName); 
            return overallMapData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smFileName"></param>
        /// <returns></returns>
        private List<string> GetMiddleLineData(string smFileName)
        {
            List<string> returnData = new List<string>();
            using (StreamReader sr = new StreamReader(smFileName))
            {
                StringBuilder fileData = new StringBuilder();
                string title = sr.ReadLine();
                int lineNumber = 0;
                while (sr.Peek() > -1)
                {
                    string str = sr.ReadLine();
                    lineNumber++;
                    StringBuilder sb = new StringBuilder();
                    string[] strTemp = str.Split(',');
                    for (int i = 0; i < strTemp.Length; i++)
                    {
                        if (i != 1)
                        {
                            double temp = double.Parse(strTemp[i]) / 1000;
                            strTemp[i] = temp.ToString("##0.######");
                        }
                        sb.Append(strTemp[i]);
                        sb.Append(",");
                    }
                    sb.Remove(sb.Length - 1, 1);

                    fileData.Append(sb);
                    fileData.Append(",");
                    if (lineNumber > 50)
                    {
                        fileData.Remove(fileData.Length - 1, 1);
                        returnData.Add(fileData.ToString());
                        fileData.Remove(0, fileData.Length);
                        lineNumber = 0;
                    }
                }

                //remove the last ","
                if (lineNumber > 0)
                {
                    fileData.Remove(fileData.Length - 1, 1);
                    returnData.Add(fileData.ToString());
                }
                sr.Close();
            }

            return returnData;

        }

        /// <summary>
        /// send middileLine data to Fcu
        /// </summary>
        /// <param name="smFileName">super mode middle file</param>
        public void SetMiddleLineData(string smFileName)
        {
            List<string> fileData = new List<string>();
            fileData = GetMiddleLineData(smFileName);

            instChassis.serialPort_FCUII.DiscardInBuffer();

            for (int i = 0; i < fileData.Count; i++)
            {
                int startPont = i * 51;
                instChassis.Write(":MIDDle " + startPont.ToString() + "," + fileData[i], this.instChassis.serialPort_FCUII);
            }




        }

       /// <summary>
        /// do super mode maping,return super mode maping data
       /// </summary>
        /// <param name="delayTime_mS">sweep delay time</param>
       /// <param name="powerRatioUsed">true meas use power ratio calculate,false meas no used</param>
        /// <returns>List contain 2 file of forward map and reverse map result</returns>
        public List<string> SuperModeMaping(double delayTime_mS, bool powerRatioUsed)
        {


            string command;
            double delayTime_S = delayTime_mS / 1000;
            command = string.Format(":SMAP DEL " + delayTime_S.ToString());
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

            List<string> forwardMappingData = new List<string>();
            List<string> reverseMappingData = new List<string>();
            List<string> returnData = new List<string>();
            List<string> referPdForwardMappingData = new List<string>();
            List<string> referPdReverseMappingData = new List<string>();
            List<string> filterPdForwardMappingData = new List<string>();
            List<string> filterPdReverseMappingData = new List<string>();

            string readData;
            //string[] result;
            double indexLines = 0;
            try
            {
                do
                {
                    readData = instChassis.serialPort_FCUII.ReadLine();

                    if (readData.Contains("STAR")) continue;
                    if (readData.Contains("END")) break;

                    //updata overMapping Percent
                    indexLines++;
                    //double temp = Math.Round(indexLines / superModeMappingMaxLines, 2) * 100;
                    double temp = indexLines;
                    this.superModeMappingPercent = int.Parse(temp.ToString());

                    if (SupermapProgressChanged != null)
                    {
                        ProgressChangedEventArgs arg = new ProgressChangedEventArgs(this.superModeMappingPercent, null);
                        SupermapProgressChanged(this.instChassis.serialPort_FCUII, arg);
                    }
                    //split forward and reverse mapping data
                    string[] tempStr = readData.Split(',');
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbReferPD = new StringBuilder();
                    StringBuilder sbFilterPD = new StringBuilder();
                    double powerRatio;
                    //if (false)
                    //{
                    //    //forward mapping data
                    //    for (int i = 0; i <= tempStr.Length / 2; i++)
                    //    {
                    //        sb.Append(tempStr[i]);
                    //        sb.Append(",");
                    //    }
                    //    sb.Remove(sb.Length - 1, 1);
                    //    forwardMappingData.Add(sb.ToString());

                    //    //reverse mapping data
                    //    sb.Remove(0, sb.Length);
                    //    for (int i = tempStr.Length - 1; i >= tempStr.Length / 2; i--)
                    //    {
                    //        sb.Append(tempStr[i]);
                    //        sb.Append(",");
                    //    }
                    //    sb.Remove(sb.Length - 1, 1);
                    //    reverseMappingData.Add(sb.ToString());
                    //}
                    //else
                    //{
                        //forward mapping data(powerRation)
                        for (int i = 0; i <= tempStr.Length / 2; i++)
                        {
                            if (tempStr[i].Contains("&"))
                            {
                                bool isDigit = true;
                                string[] array = tempStr[i].Split('&');
                                for (int j = 0; j < array.Length; j++)
                                {
                                    foreach (char var in array[j].Trim())
                                    {
                                        if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                        {
                                            isDigit = false;
                                            break;
                                        }
                                    }
                                }

                                string[] array2 = tempStr[i].Split('&');
                                if (isDigit)
                                {
                                    if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                        powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                    else
                                        powerRatio = 999;
                                }
                                else
                                    powerRatio = 999;
                                sb.Append(powerRatio.ToString("#0.0000"));
                                sb.Append(",");
                                sbFilterPD.Append(array2[0].Trim());
                                sbFilterPD.Append(",");
                                sbReferPD.Append(array2[1].Trim());
                                sbReferPD.Append(",");

                            }
                        }
                        //remove the last ","
                        sb.Remove(sb.Length - 1, 1);
                        sbFilterPD.Remove(sbFilterPD.Length - 1, 1);
                        sbReferPD.Remove(sbReferPD.Length - 1, 1);

                        forwardMappingData.Add(sb.ToString());
                        referPdForwardMappingData.Add(sbReferPD.ToString());
                        filterPdForwardMappingData.Add(sbFilterPD.ToString());

                        //reverse mapping data(powerRatio)
                        sb.Remove(0, sb.Length);
                        sbFilterPD.Remove(0, sbFilterPD.Length);
                        sbReferPD.Remove(0, sbReferPD.Length);
                        for (int i = tempStr.Length - 1; i >= tempStr.Length / 2; i--)
                        {
                            if (tempStr[i].Contains("&"))
                            {
                                bool isDigit = true;
                                string[] array = tempStr[i].Split('&');
                                for (int j = 0; j < array.Length; j++)
                                {
                                    foreach (char var in array[j].Trim())
                                    {
                                        if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                        {
                                            isDigit = false;
                                            break;
                                        }
                                    }
                                }

                                string[] array2 = tempStr[i].Split('&');
                                if (isDigit)
                                {
                                    if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                        powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                    else
                                        powerRatio = 999;
                                }
                                else
                                    powerRatio = 999;
                                sb.Append(powerRatio.ToString("#0.0000"));
                                sb.Append(",");
                                sbFilterPD.Append(array2[0].Trim());
                                sbFilterPD.Append(",");
                                sbReferPD.Append(array2[1].Trim());
                                sbReferPD.Append(",");
                            }
                        }

                        sb.Remove(sb.Length - 1, 1);
                        sbFilterPD.Remove(sbFilterPD.Length - 1, 1);
                        sbReferPD.Remove(sbReferPD.Length - 1, 1);

                        reverseMappingData.Add(sb.ToString());
                        referPdReverseMappingData.Add(sbReferPD.ToString());
                        filterPdReverseMappingData.Add(sbFilterPD.ToString());

                    //}
                    Thread.Sleep(10);
                } while (readData != "");
            }
            catch (Exception)
            {
                throw;
            }
            string Locker_TX_forwardFileName;
            string Locker_TX_reverseFileName;
            string Locker_RX_forwardFileName;
            string Locker_RX_reverseFileName;

            string refPdForwardFileName;
            string filterPdForwardFileName;
            string refPdReverseFileName;
            string filterPdReverseFileName;

            Locker_TX_forwardFileName = DataDirectory("LockerTx_", SweepDirection.forward, Sweep.SuperMap);
            Locker_TX_reverseFileName = DataDirectory("LockerTx_", SweepDirection.reverse, Sweep.SuperMap);
            Locker_RX_forwardFileName = DataDirectory("LockerRx_", SweepDirection.forward, Sweep.SuperMap);
            Locker_RX_reverseFileName = DataDirectory("LockerRx_", SweepDirection.reverse, Sweep.SuperMap);
        
            refPdForwardFileName = DataDirectory("RefPD_", SweepDirection.forward, Sweep.SuperMap);
            filterPdForwardFileName = DataDirectory("filterPD_", SweepDirection.forward, Sweep.SuperMap);
            refPdReverseFileName = DataDirectory("RefPD_", SweepDirection.reverse, Sweep.SuperMap);
            filterPdReverseFileName = DataDirectory("filterPD_", SweepDirection.reverse, Sweep.SuperMap);

            List<string> SmMapResultFileName = new List<string>();
            SmMapResultFileName.Add(Locker_TX_forwardFileName);
            SmMapResultFileName.Add(Locker_TX_reverseFileName);
            SmMapResultFileName.Add(Locker_RX_forwardFileName);
            SmMapResultFileName.Add(Locker_RX_reverseFileName);
            SmMapResultFileName.Add(refPdForwardFileName);
            SmMapResultFileName.Add(filterPdForwardFileName);
            SmMapResultFileName.Add(refPdReverseFileName);
            SmMapResultFileName.Add(filterPdReverseFileName);

            Dictionary<string, List<string>> SmMapResult = new Dictionary<string, List<string>>();
            SmMapResult.Add(Locker_TX_forwardFileName, forwardMappingData);
            SmMapResult.Add(Locker_TX_reverseFileName, reverseMappingData);
            SmMapResult.Add(Locker_RX_forwardFileName, forwardMappingData);
            SmMapResult.Add(Locker_RX_reverseFileName, reverseMappingData);

            SmMapResult.Add(refPdForwardFileName, referPdForwardMappingData);
            SmMapResult.Add(refPdReverseFileName, referPdReverseMappingData);
            SmMapResult.Add(filterPdForwardFileName, filterPdForwardMappingData);
            SmMapResult.Add(filterPdReverseFileName, filterPdReverseMappingData);

            foreach (string fileName in SmMapResultFileName)
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (string var in SmMapResult[fileName])
                    {
                        sw.WriteLine(var);
                    }
                    sw.Close();
                }
            }

            returnData.Add(Locker_TX_forwardFileName);
            returnData.Add(Locker_TX_reverseFileName);
            returnData.Add(Locker_RX_forwardFileName);
            returnData.Add(Locker_RX_reverseFileName);
            return returnData;
        }

        /// <summary>
        /// Get temperature of the source card
        /// </summary>
        /// <returns></returns>
        public double GetSouCardTemperature()
        {
            string command;
            command = string.Format(":TEMP{0}", this.Slot);
            double temperature = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
            return temperature;
        }

        /// <summary>
        /// get middle line data from flash,this function can use to check weather the middle data sent to FCUII
        /// </summary>
        /// <returns></returns>
        public double[] GetMidLineDataFromFlash()
        {
            string measureData = instChassis.Query(":MIDD?", this.instChassis.serialPort_FCUII);
            string[] data = measureData.Split(',');
            List<double> middleLineData = new List<double>();
            for (int i = 0; i < data.Length; i++)
            {
                middleLineData.Add(double.Parse(data[i]));
            }
            return middleLineData.ToArray();
        }

        /// <summary>
        /// sense current,return current value
        /// </summary>
        /// <returns>return current value at amps</returns>
        public double SenseCurrent()
        {
            string command;
            command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
            string current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII);
            return double.Parse(current_A);
        }

        /// <summary>
        /// sense current,output ADC vuale
        /// </summary>
        /// <param name="adc">ADC value of current</param>
        public void SenseCurrent(out int adc)
        {
            string command;
            command = string.Format(":SOURce{0}:CHANnel{1}:CURRent?ADC", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));

        }

        /// <summary>
        /// sense voltage,return voltage
        /// </summary>
        /// <returns>voltage_Volt</returns>
        public double SenseVoltage()
        {
            string command;
            //get sense current
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
            string voltage_V = instChassis.Query(command, this.instChassis.serialPort_FCUII);
            return double.Parse(voltage_V);
        }

        /// <summary>
        /// sense voltage,out put ADC Value
        /// </summary>
        /// <param name="adc">ADC value of voltage</param>
        public void SenseVoltage(out int adc)
        {
            //get sense current
            string command;
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?adc", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));

        }

        

        /// <summary>
        ///  Get Actual current
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public string Get_CurrentActual_Amp(AllChannelName section)
        {
            //double sectionCurrent = 0;
            string a;
            switch (section)
            {
                case AllChannelName.FS1Instr:
                    a = get_ActualCurrent;
                    break;
                default:
                    throw new ArgumentException("Unknown DSDBR section!");
            }
            return a;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void WriteCommand_DAC(string command)
        {
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="current"></param>
        public void Sent_CurrentSetPoint_Amp(Inst_SMUTI_TriggeredSMU section, double current)
        {
            string command;
            current = Math.Round(current, 7);
            get_ActualCurrent = current.ToString();
            command = string.Format(":SOURce{0}:CHANnel{1}:CURRent " + current.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }



        #endregion

        #region private function

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preStr"></param>
        /// <param name="sweepDirction"></param>
        /// <param name="sweepType"></param>
        /// <returns></returns>
        protected string DataDirectory(string preStr, SweepDirection sweepDirction, Sweep sweepType)
        {
            string FileName = "";
            switch (sweepType)
            {
                case Sweep.OverallMap:
                    FileName = string.Format("{0}{1:yyyyMMddhhmmss}.csv", preStr, DateTime.Now);
                    break;
                case Sweep.SuperMap:
                    FileName = string.Format("{0}{1}{2:yyyyMMddhhmmss}.csv", preStr, Enum.GetName(typeof(SweepDirection), sweepDirction), DateTime.Now);
                    break;
                default:
                    break;
            }
            //string dir = Directory.GetDirectoryRoot(System.Environment.CurrentDirectory) + "FcuMarkIIResult";
            string dir = Directory.GetCurrentDirectory() + "\\results\\FcuMarkIIResult";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, FileName);
        }
        #endregion
       
        #region private val
        private int overMappingPercent = 0;
        private const int overMappingMaxLines = 147;
        private int superModeMappingPercent = 0;
        private const int superModeMappingMaxLines = 201;

        /// <summary>
        /// 
        /// </summary>
        public static string get_ActualCurrent;
        /// <summary>
        /// 
        /// </summary>
        public static string get_ActualVoltage;
        /// <summary>
        /// 
        /// </summary>
        public static string get_Ctapcurrent;

        #endregion



    }

    static class SetPointValue
    {
        static public DatumList CurrentSetValue = new DatumList();

        static public DatumList VoltageSetValue = new DatumList();

    }

    static class GetTheSetValue
    {
        static public string Mid_filename = @"D:\01_Test_Program\40G_GoldBox_FCU_MK_II\TestEngine\LogStore\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";

        static public List<string> actulValue = new List<string>();
        static public StreamWriter sw = new StreamWriter(Mid_filename);
        static public StringBuilder sb = new StringBuilder();
    }
}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace TEST.Ke2510
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        private Chassis_Ke2510 testChassis;
        private Inst_Ke2510 testInstr;        
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_Ke2510("MyChassis", "Chassis_Ke2510", "GPIB0::17::INSTR");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_Ke2510("TEC1", "Inst_Ke2510", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput(testChassis, "IsOnline set true OK");
            testInstr.IsOnline = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Setup()
        {
            TestOutput("\n\n*** T01_Setup ***");
            testInstr.SetDefaultState();
        }

        [Test]
        public void T02_TecSetup()
        {
            TestOutput("\n\n*** T02_TecSetup ***");
            IInstType_TecController tec = testInstr;
            TestOutput(tec, "Enabled? " + tec.OutputEnabled);
            TestOutput(tec, "TEC IActual: " + tec.TecCurrentActual_amp);
            TestOutput(tec, "TEC IMax: " + tec.TecCurrentCompliance_amp);
            TestOutput(tec, "TEC ISet: " + tec.TecCurrentSetPoint_amp);
            TestOutput(tec, "TEC VActual: " + tec.TecVoltageActual_volt);
            TestOutput(tec, "TEC VMax: " + tec.TecVoltageCompliance_volt);
            TestOutput(tec, "TEC VSet: " + tec.TecVoltageSetPoint_volt);
            TestOutput(tec, "Sensor I: " + tec.SensorCurrent_amp);
            TestOutput(tec, "Sensor RActual: " + tec.SensorResistanceActual_ohm);
            TestOutput(tec, "Sensor RSet: " + tec.SensorResistanceSetPoint_ohm);
            TestOutput(tec, "Sensor TActual: " + tec.SensorTemperatureActual_C);
            TestOutput(tec, "Sensor TSet: " + tec.SensorTemperatureSetPoint_C);

            tec.SensorTemperatureSetPoint_C = 15.0;
            //tec.OutputEnabled = true;
        }

        [Test]
        public void T03_DigiIO()
        {
            TestOutput("\n\n*** T03_DigiIO ***");
            IInstType_DigiIOCollection digiIOCollection = (IInstType_DigiIOCollection)testInstr;
            // read back the state
            foreach (IInstType_DigitalIO digiIoLine in digiIOCollection.DigiIoLines)
            {
                string str = string.Format("Line '{0}' = {1}", digiIoLine.Name, digiIoLine.LineState);
                TestOutput(digiIOCollection, str);
            }

            // set the state to false
            foreach (IInstType_DigitalIO digiIoLine in digiIOCollection.DigiIoLines)
            {
                digiIoLine.LineState = false;
                Assert.AreEqual(false, digiIoLine.LineState);
            }

            // set the state to true
            foreach (IInstType_DigitalIO digiIoLine in digiIOCollection.DigiIoLines)
            {
                digiIoLine.LineState = true;
                Assert.AreEqual(true, digiIoLine.LineState);
            }
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag4338B_MilliOhmMeter.cs
//
// Author: , 2006
// Design: As per 4338B Driver DD

using System;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 4338B Milli OhmMeter.
    /// </summary>
    public class Inst_Ag4338B_MilliOhmMeter:InstType_MultiMeter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag4338B_MilliOhmMeter(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // Configure valid hardware information
            string maxMilliOhmMeterFwVersion = "01.02";

            // Add 4338B MilliOhmMeter details
            InstrumentDataRecord ag4338BData = new InstrumentDataRecord(
                "HEWLETT-PACKARD 4338B",					                    // hardware name 
                "0",											// minimum valid firmware version 
                maxMilliOhmMeterFwVersion);						// maximum valid firmware version 
            ag4338BData.Add("MaximumMilliOhmRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag4338B", ag4338BData);


            // Configure valid chassis information
            // Add 4338B chassis details
            InstrumentDataRecord ag4338BChassisData = new InstrumentDataRecord(
                "Chassis_Ag4338B",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag4338B", ag4338BChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (ChassisType_Visa) base.InstrumentChassis;


            // Generate the common command stem that most commands use!
            //this.commandStem = String.Format(":SENS{0}:CHAN{1}:",
            //        this.Slot, this.SubSlot);
            //this.commandPowStem = this.commandStem + "POW:";

            // default to safe mode
            //this.safeMode = true;
        }
        #endregion

        #region Defined interface for Inst_Ag4338B_MiliOhmMeter
        public override InstType_MultiMeter.MeterAcType AcType
        {
            get { throw new Exception("The method or operation 'AcType' in 4338B is not implemented."); }
        }

        public override double GetReading()
        {
            //throw new Exception("The method or operation is not implemented.");
            string commandString = ":FETC?";
            string reply = this.Query(commandString);
            reply = reply.Split(',')[1];
            while (!char.IsDigit(reply[reply.Length - 1]))
            {
                reply = reply.Substring(0, reply.Length - 1);
            }
            return double.Parse(reply) + calibrationFactor_Ohm;
        }

        public override double IntegrationTime_s
        {
            get
            {
                //throw new Exception("The method or operation is not implemented.");
                string commandString = ":FIMP:APER?";
  
                return double.Parse(this.Query(commandString));//0.35 or 0.7 or 0.9
            }
            set
            {
                //throw new Exception("The method or operation is not implemented.");
                string commandString = ":SENS:FIMP:APER " + value.ToString();
                
                this.Write(commandString);
            }
        }

        public override double MeterRange
        {
            get
            {
                //throw new Exception("The method or operation is not implemented.");
                string queryResult = "";
                string commandString = ":SENS:FIMP:RANG:AUTO?";

                queryResult = this.Query(commandString);
                if (int.Parse(queryResult) == 1)
                {
                    return InstType_MultiMeter.AutoRange;
                }
                else
                {
                    commandString = ":SENS:FIMP:RANG?";
                }

                return double.Parse(this.Query(commandString));
            }
            set
            {
                //throw new Exception("The method or operation is not implemented.");
                string commandString = ":SENS:FIMP:RANG:AUTO ON";
                if (value.ToString() == InstType_MultiMeter.AutoRange.ToString())
                {
                    commandString = ":SENS:FIMP:RANG:AUTO ON";
                }
                else
                {
                    commandString = ":SENS:FIMP:RANG " + value.ToString();
                }

                this.Write(commandString);
            }
        }

        public override InstType_MultiMeter.MeterMode MultiMeterMode
        {
            get
            {
                //throw new Exception("The method or operation is not implemented.");
                string currentFunction = this.Query(":SENS:FUNC?").ToUpper();//Always "FIMP"
                if (currentFunction.ToString() == "\"FIMP\"")
                {
                    return MeterMode.Resistance2wire_ohms;
                }
                else
                {
                    throw new Exception("Unknown MultiMeter Mode");
                }
            }
            set
            {
                //throw new Exception("The method or operation is not implemented.");
                string commandString = ":SENS:FUNC 'FIMP'";
                if (value == MeterMode.Resistance2wire_ohms)
                {
                    this.Write(commandString);
                }
                else
                {
                    throw new Exception("Unknown MultiMeter Mode");
                }
            }
        }

        public override string FirmwareVersion
        {
            //get { throw new Exception("The method or operation is not implemented."); }
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = this.Query("*IDN?").Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                return idn[3];
            }
        }

        public override string HardwareIdentity
        {
            //get { throw new Exception("The method or operation is not implemented."); }
            get
            {
                // Read the full IDN? string which has 4 comma seperated substrings
                string[] idn = this.Query("*IDN?").Split(',');

                // Build the string from 2 substrings, the manufacturer name & the model number
                string hid = idn[0].Trim() + " " + idn[1].Trim();

                // Log event 
                LogEvent("'HardwareIdentity' returned : " + hid);

                return hid;
            }
        }

        public override void SetDefaultState()
        {
            //throw new Exception("The method or operation is not implemented.");
            this.Write(":SYST:PRES");
        }
        #endregion


        #region public method

        public void Wait()
        {
            this.Write("*WAI");
        }

        public void SetAutoRangeOFF()
        {
            this.Write(":SENS:FIMP:RANGE:AUTO OFF");
        }

        public double SetTestSignalLevel
        {
            set
            {
                this.Write(":SOUR:CURR " + value.ToString());
            }
        }

        public double SetAverageRate
        {
            set
            {
                this.Write(":SENS:AVER:COUN " + value.ToString());
            }
        }

        public void SetTriggerInt()
        {
            this.Write(":TRIG:SOUR INT;:INIT:CONT ON");
        }

        public int StatusOperationEnable
        {
            get
            {
                return int.Parse(this.Query(":STAT:OPER:ENAB?"));
            }
            set
            {
                this.Write(":STAT:OPER:ENAB " + value.ToString());
            }
        }

        public int StatusOperationEvent
        {
            get
            {
                return int.Parse(this.Query(":STAT:OPER?"));
            }
        }

        public int StatusQuestionableCondition
        {
            get
            {
                return int.Parse(this.Query(":STAT:QUES:COND?"));
            }
        }

        public int StatusQuestionableEnable
        {
            get
            {
                return int.Parse(this.Query(":STAT:QUES:ENAB?"));
            }
            set
            {
                this.Write(":STAT:QUES:ENAB " + value.ToString());
            }
        }

        public int StatusQuestionableEvent
        {
            get
            {
                return int.Parse(this.Query(":STAT:QUES?"));
            }
        }

        /// <summary>
        /// CalibrationFactor_Ohm
        /// </summary>
        public double CalibrationFactor_Ohm
        {
            get
            {
                return calibrationFactor_Ohm;
            }
            set
            {
                calibrationFactor_Ohm = value;
            }
        }

        #endregion


        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;

        /// <summary>
        /// calibrationfactor_Ohm
        /// </summary>
        private double calibrationFactor_Ohm;

        #endregion


        #region Private function
        /// <summary>
        /// Write command to instrument
        /// </summary>
        /// <param name="cmd">the command we wish to write to chassis</param>
        private void Write(string cmd)
        {
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Query command to instrument 
        /// </summary>
        /// <param name="cmd">the query string</param>
        /// <returns></returns>
        private string Query(string cmd)
        {
            return (instrumentChassis.Query_Unchecked(cmd, this));
        }

        #endregion
    }
}

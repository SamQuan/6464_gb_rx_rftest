// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag11713B_DigiIOAsSwitch.cs
//
// Author: sam.quan 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Instrument driver for Ag11713B DAU's 34903A 20-Channel Actuator / General-Purpose Switch Card,
    /// modelled as 8 general purpose Digital IO lines. Using IInstType_DigiIOCollection to allow a single 
    /// entry in the Test Engine's equipment configuration file.
    /// </summary>
    public class Inst_Ag11713B_DigiIOAsSwitch : Instrument, IInstType_DigiIOCollection
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag11713B_DigiIOAsSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // Add Ag11713B internal Switch details
            InstrumentDataRecord ag11713B_DigiIOAsSwitch_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 11713B",		// hardware name - chassis name only.
                "0",							// minimum valid firmware version 
                "9-1-2:1.0");							// maximum valid firmware version 
            ValidHardwareData.Add("Ag11713B_DigiIOAsSwitch", ag11713B_DigiIOAsSwitch_Data);// note "DigiIOAsSwitch" suffix 

            // Configure valid chassis driver information
            // Add 34970A chassis details
            InstrumentDataRecord ag11713BChassisData = new InstrumentDataRecord(
                "Chassis_Ag34970A",								// chassis driver name  
                "0.0.0.0",										// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag11713B_DigiIOAsSwitch", ag11713BChassisData);// note "_Combo" suffix

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag11713B)base.InstrumentChassis;

            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(8);
            for (int ii = 1; ii <= 8; ii++)
            {
                digiOutLines.Add(new Inst_Ag11713B_DigiOutLine(instrumentNameInit + "_D" + ii, instrumentChassis, int.Parse(slotInit), ii));
            }
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
          
                return this.instrumentChassis.FirmwareVersion; 
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {

                return this.instrumentChassis.HardwareIdentity; 
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                foreach (IInstType_DigitalIO dio in digiOutLines)
                {
                    dio.IsOnline = value;
                }                  
            }
        }

        #endregion

        #region IInstType_DigiIOCollection Members
        /// <summary>
        /// Get a Digital IO Line by line number
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
            return digiOutLines[lineNumber - 1];
        }
        /// <summary>
        /// Get enumerator for Digital IO Line
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get
            {
                return digiOutLines;
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag11713B instrumentChassis;

        /// <summary>
        /// List of Digital IO Lines
        /// </summary>
        internal List<IInstType_DigitalIO> digiOutLines;
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Ag11713B_Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Ag11713B_Test test = new Ag11713B_Test();
            test.Setup();
            DateTime start = DateTime.Now;
            try
            {
                
            }
            finally
            {
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            Environment.Exit(0);
        }
    }
}

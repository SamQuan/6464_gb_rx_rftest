using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.Instruments;

namespace Ag11713B_Tester
{
    /// <exclude />	
    [TestFixture]
    public class Ag11713B_Test
    {
        #region Constants
        const string visaResource = "//pai-tx-labj1/GPIB0::9::INSTR";

        const string chassisName = "Chassis";
        const string sw1Name = "Switch 1";

        const string switchSlot = "1";
        const string swCh1 = "1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Agilent 11713B Driver Test Initialising ***");

            // create equipment objects
            testChassis = new Chassis_Ag11713B(chassisName, "Chassis_Ag11713B", visaResource);
            TestOutput(chassisName, "Created OK");


            testSw1 = new Inst_Ag11713B_DigiIOAsSwitch(sw1Name, "Inst_Ag11713B_Switch", switchSlot, swCh1, testChassis);
            TestOutput(sw1Name, "Created OK");
           
            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            
            testSw1.IsOnline = true;
            TestOutput(sw1Name, "IsOnline set true OK");
            
            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }        
       
        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver=" + testChassis.DriverVersion);
            TestOutput(chassisName, "Firmware=" + testChassis.FirmwareVersion);
            TestOutput(chassisName, "HW=" + testChassis.HardwareIdentity);

          

            TestOutput(sw1Name, "Driver=" + testSw1.DriverVersion);
            TestOutput(sw1Name, "Firmware=" + testSw1.FirmwareVersion);
            TestOutput(sw1Name, "HW=" + testSw1.HardwareIdentity);

          
        }

        [Test]
        public void T02_ChassisFunction()
        {
            TestOutput("\n\n*** T02_ChassisFunction ***");
            //TestOutput(chassisName, "Slot1=" + testChassis.GetSlotID(1));
            
        }

        [Test]
        public void T08_ChassisSwitch()
        {
            TestOutput("\n\n*** T08_ChassisSwitch ***");
            TestOutput("Initial position of switches");
            int switchSlot = Int32.Parse(Ag11713B_Test.switchSlot);
            int swCh1 = Int32.Parse(Ag11713B_Test.swCh1);

            //TestOutput(chassisName, testChassis.GetSwitchPosn(switchSlot, swCh1).ToString());

            TestOutput("Exclusive close #1");
            //testChassis.CloseSwitch(switchSlot, swCh1, true);
            //Assert.AreEqual(Chassis_Ag11713B.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh1));
            
            TestOutput("Exclusive close #2");
            //testChassis.CloseSwitch(switchSlot, swCh2, true);
            //Assert.AreEqual(Chassis_Ag11713B.SwitchPosn.Open, testChassis.GetSwitchPosn(switchSlot, swCh1));
            
            TestOutput("Close both switches");
            //testChassis.CloseSwitch(switchSlot, swCh1, false);
            //Assert.AreEqual(Chassis_Ag11713B.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh1));

            TestOutput("Open #1 only");
            //testChassis.OpenSwitch(switchSlot, swCh1);
            //Assert.AreEqual(Chassis_Ag11713B.SwitchPosn.Open, testChassis.GetSwitchPosn(switchSlot, swCh1));           
        }

        [Test]
        public void T09_SwitchObjects()
        {
            TestOutput("\n\n*** T09_SwitchObjects ***");
            TestOutput("Testing switch setup");
            //Assert.AreEqual(1, testSw1.MinimumSwitchState);
            //Assert.AreEqual(2, testSw1.MaximumSwitchState);

            TestOutput("Current Positions");
            //TestOutput(sw1Name, testSw1.SwitchState.ToString());

            TestOutput("Toggle Switches");
            //testSw1.SwitchState = 1;
            //Assert.AreEqual(1, testSw1.SwitchState);
           // testSw1.SwitchState = 2;
           // Assert.AreEqual(2, testSw1.SwitchState);
           // testSw1.SwitchState = 1;
           // Assert.AreEqual(1, testSw1.SwitchState);

           
            TestOutput("Default State");
            testSw1.SetDefaultState();
           // TestOutput(sw1Name, testSw1.SwitchState.ToString());

        }

        #region Private helper fns and data
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        
        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_Ag11713B testChassis;
        private Inst_Ag11713B_DigiIOAsSwitch testSw1;
        #endregion

    }

}

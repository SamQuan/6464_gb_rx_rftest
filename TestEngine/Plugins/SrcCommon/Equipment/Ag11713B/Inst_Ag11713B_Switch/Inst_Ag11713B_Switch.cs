// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag11713B_Switch.cs
//
// Author: Sam.Quan, 2011
// Design: As specified in 34970A Driver DD 

using System;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    ///  Agilent 11713B Data Acquisition Unit driver for switching
    /// </summary>
    public class Inst_Ag11713B_Switch : InstType_SimpleSwitch
    {
        /// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag11713B_Switch(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information
			// Add Ag34970A internal Switch details
            InstrumentDataRecord ag11713B_Switch_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 11713B",				// hardware name 
                "0",							// minimum valid firmware version 
                "9-1-2:1.0");							// maximum valid firmware version 
            ValidHardwareData.Add("Ag11713B", ag11713B_Switch_Data);
									
			// Configure valid chassis information
            // Add 34970A chassis details
            InstrumentDataRecord ag11713BChassisData = new InstrumentDataRecord(
                "Chassis_Ag11713B",								// chassis driver name  
				"0.0.0.0",										// minimum valid chassis driver version  
				"2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag11713B", ag11713BChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (Chassis_Ag11713B) base.InstrumentChassis;
            // initialise the slot and channel number provided
            
                                             
        }

        #region Instrument Overrides
        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware, and the plugin hardware 
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // by default open this switch
            this.SwitchState = 1;
        }

        /// <summary>
        /// Set instrument on/offline. When setting online, initialises the instrument and checks
        /// that it is valid.
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // record the plugin type that this DMM channel is in
                    this.chassisPluginType = instrumentChassis.GetSlotID(chassisSlot);
                    
                    // check can get switch current function OK. If not a valid slot / sub-slot combination
                    // this function will throw an exception
                    try
                    {
                        int posn = this.SwitchState;
                    }
                    catch (Exception e)
                    {
                        throw new InstrumentException("Invalid slot and channel for switch operation", e);
                    }
                }
            }
        }
        #endregion

        #region Switch functions
        /// <summary>
        /// Output minimum position: always 1
        /// </summary>
        public override int MinimumSwitchState
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Output maximum position: always 2
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                // only support 1x2 switches in the 34903A chassis
                return 2;
            }
        }

        /// <summary>
        /// Get / Set output switch position. 1 = OPEN, 2 = CLOSED.
        /// </summary>
        public override int SwitchState
        {
            get
            {
                // Is the switch closed?
                string command = String.Format("ROUT:CLOS? (@{0})", chassisChannel);
                string resp = GetCmd(command);
                // What does the response mean?
                if (resp == "0") return 1; // OPEN
                else if (resp == "1") return 2; // CLOSED
                else throw new InstrumentException("Invalid response from OutputSwitchPosition: " + resp);                
            }
            set
            {
                string command;
                // generate the appropriate command based on what we're being asked to do.
                switch (value)
                {
                    case 1:
                        command = String.Format("ROUT:OPEN (@{0})", chassisChannel);                    
                        break;
                    case 2:
                        command = String.Format("ROUT:CLOS (@{0})", chassisChannel);
                        break;
                    default:
                        throw new InstrumentException("Invalid switch position: " + value.ToString());
                }
                // send the command
                SetCmd(command);
            }
        }
        #endregion

        #region Private data and helper functions
        /// <summary>
        /// "Set" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void SetCmd(string command)
        {
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// "Get" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string GetCmd(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// Instrument chassis object
        /// </summary>
        private Chassis_Ag11713B instrumentChassis;

        /// <summary>
        /// Chassis slot for the DMM
        /// </summary>
        private int chassisSlot;

        /// <summary>
        /// Chassis channel for the DMM
        /// </summary>
        private int chassisSubSlot;

        /// <summary>
        /// Chassis Plugin Type (Model number)
        /// </summary>        
        private string chassisPluginType;

        /// <summary>
        /// Which channel number is the Switch (Global for chassis)
        /// </summary>       
        private int chassisChannel;
        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Ag11713B.cs
//
// Author: sam.quan 2011
// Design: As specified in 11713B Driver 

using System;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.ChassisTypes;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Agilent 11713B Data Acquisition Unit Chassis driver
    /// </summary>
    public class Chassis_Ag11713B : ChassisType_Visa488_2
    {
        /// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
        public Chassis_Ag11713B(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information
            // Add Ag11713B details
            ChassisDataRecord ag11713BData = new ChassisDataRecord(
                "HEWLETT-PACKARD 11713B",			// hardware name 
                "0",								// minimum valid firmware version 
                "9-1-2");					// maximum valid firmware version 
            ValidHardwareData.Add("Ag11713B", ag11713BData);
            
        }

        #region Chassis override functions
        /// <summary>
        /// Setup the chassis as soon as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                // if setting online
                if (value)
                {
                                      
                }
            }
        }

		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
                // Read the chassis ID string
				string idn = this.Idn;
				
				// Return the firmware version in the 4th comma seperated field
				return idn.Split(',')[3].Trim();
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
                // Read the chassis ID string and split the comma seperated fields
				string[] idn = this.Idn.Split(',');		
		
				// Return field1, the manufacturer name and field 2, the model number
				return idn[0] + " " + idn[1];
			}
        }

        #endregion

    }
}

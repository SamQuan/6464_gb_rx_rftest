// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_SantecOTF910.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis driver for the Santec OTF910.
    /// </summary>
    public class Chassis_SantecOTF910 : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_SantecOTF910(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "SantecOTF910",			// hardware name 
                "0.0.0.0",			// minimum valid firmware version 
                "2.0.0.0");		// maximum valid firmware version 
            ValidHardwareData.Add("SantecOTF910", chassisData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                /* OTF firmware does not have a published interface returning the version number. */
                return "1.2.3.4";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                /* OTF firmware does not have a published interface returning this information. */
                return "SantecOTF910";
            }
        }

        /// <summary>
        /// Get/Set Chassis online state
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    string val = this.Query_Unchecked("WA", null);
                }
            }
        }
        #endregion
    }
}

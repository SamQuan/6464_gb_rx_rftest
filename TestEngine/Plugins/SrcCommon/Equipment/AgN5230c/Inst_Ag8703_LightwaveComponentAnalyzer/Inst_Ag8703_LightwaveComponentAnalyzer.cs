// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag8703_LightwaveComponentAnalyzer.cs
//
// Author: cypress.chen, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestSolution.ChassisNS;

// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// Agilent 8703A/B instrument driver
    /// </summary>
    public class Inst_Ag8703_LightwaveComponentAnalyzer :InstType_LightwaveComponentAnalyzer
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag8703_LightwaveComponentAnalyzer(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrAg8703A = new InstrumentDataRecord(
                "HEWLETT PACKARD 8703A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "1.00");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag8703A", instrAg8703A);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag8703",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag8703", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_AgN5230c)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = this.instrumentChassis.GetIdn();

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the instrument ID string and split the comma seperated fields
                string[] idn = this.instrumentChassis.GetIdn().Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {            
            // Send a reset, *RST, and a cls
            instrumentChassis.Write_Unchecked("*RST",this);
            System.Threading.Thread.Sleep(10);
            instrumentChassis.Write_Unchecked("*CLS",this);
            System.Threading.Thread.Sleep(10);

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    instrumentChassis.Timeout_ms = 10000;

                    System.Threading.Thread.Sleep(5);
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_AgN5230c instrumentChassis;

        #endregion

        #region Lightwave Componet Analyzer overrides        
        
        /// <summary>
        /// set/get which channel is active for measurement
        /// but the getter doesn't work!!!!!!!!!!
        /// </summary>
        public override Channel ActiveMeausurementChannel
        {
            get
            {
                string command = "CHAN1?";
                string result = QueryDataFromInstrument(command);
                if (result == "1")
                {
                    return Channel.Channel1;
                }
                System.Threading.Thread.Sleep(5);
                command = "CHAN2?";
                result = QueryDataFromInstrument(command);
                if (result == "1")
                {
                    return Channel.Channel2;
                }
                throw new InstrumentException("Maybe the active channel is not supported currenly!");
            }
            set
            {
                string command = null;
                switch (value)
                {
                    case Channel.Channel1:
                        command = "CHAN1";
                        break;
                    case Channel.Channel2:
                        command = "CHAN2";
                        break;
                    default:
                        throw new InstrumentException("This channel is not supported--" + value.ToString());                        
                }

                WriteDataFromInstrumentWithCheck(command);

            }
        }

        /// <summary>
        /// Set/get what data are displayed
        /// </summary>
        public override void SetDataDisplayMode(DataDisplayMode dataDisplayMode)
        {

            string command = null;
            switch (dataDisplayMode)
            {
                case DataDisplayMode.DataOnly:
                    command = "DISPDATA";
                    break;
                case DataDisplayMode.DataAndMemory:
                    command = "DISPDATM";
                    break;
                case DataDisplayMode.DataDividedByMemory:
                    command = "DISPDDM";
                    break;
                case DataDisplayMode.DataMinusMemory:
                    command = "DISPDMM";
                    break;
                case DataDisplayMode.MemoryOnly:
                    command = "DISPMEMO";
                    break;
                case DataDisplayMode.DataPlusMemory:
                    command = "DISPDPM";
                    break;
                case DataDisplayMode.DataTimesMemory:
                    command = "DISPDTM";
                    break;
                case DataDisplayMode.Memory1DividedByMemory2:
                    command = "DISPM1DM";
                    break;
                case DataDisplayMode.Memory1MinusByMemory2:
                    command = "DISPM1MM";
                    break;
                case DataDisplayMode.Memory1PlusMemory2:
                    command = "DISPM1PM";
                    break;
                case DataDisplayMode.Memory1timesMemory2:
                    command = "DISPM1TM";
                    break;
                case DataDisplayMode.Memory2DividedByMemory1:
                    command = "DISPM2DM";
                    break;
                case DataDisplayMode.Memory2MinuseMemory1:
                    command = "DISPM2MM";
                    break;
                case DataDisplayMode.MathResults:
                    command = "DISPMATH";
                    break;
                case DataDisplayMode.MemoryMinusData:
                    command = "DISPMMD";
                    break;
                case DataDisplayMode.MemoryDividedByData:
                    command = "DISPMDD";
                    break;
                default:
                    throw new InstrumentException("This data display mode is not supported--" + dataDisplayMode.ToString());
            }
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Stores the data trace in channel memory 
        /// </summary>
        public override void StoreTraceDataInChannelMemory()
        {
            string command = "DATI";
            WriteDataFromInstrumentWithCheck(command);
        }

        /// <summary>
        /// Set/get active marker 
        /// </summary>
        public override Marker ActiveMarker
        {
            get
            {
                Marker marker = Marker.Marker1;
                string command = "MARK?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "1":
                        marker = Marker.Marker1;
                        break;
                    case "2":
                        marker = Marker.Marker2;
                        break;
                    case "3":
                        marker = Marker.Marker3;
                        break;
                    case "4":
                        marker = Marker.Marker4;
                        break;
                    case "5":
                        marker = Marker.Marker5;
                        break;
                    default:
                        throw new InstrumentException("Marker query result unrecognized--" + result);
                }
                return marker;
            }
            set
            {
                string command = null;
                switch (value)
                {
                    case Marker.Marker1:
                        command = "MARK1";
                        break;
                    case Marker.Marker2:
                        command = "MARK2";
                        break;
                    case Marker.Marker3:
                        command = "MARK3";
                        break;
                    case Marker.Marker4:
                        command = "MARK4";
                        break;
                    case Marker.Marker5:
                        command = "MARK5";
                        break;
                    default:
                        throw new InstrumentException("This Marker is not supported--" + value.ToString());
                }
                instrumentChassis.Query_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the reference position 
        /// Range:0-10
        /// </summary>
        public override double ReferencePosition
        {
            get
            {
                string command = "REFP?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unrecognized Reference Position result:" + result,ex);
                }
                
            }
            set
            {
                if (value < 0 || value > 10)
                    throw new InstrumentException("Out of range(1-10):" + value.ToString());
                string command = "REFP " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
       
        /// <summary>
        /// Set/get the output power level 
        /// </summary>
        public override double SourceOutputPower_dBm
        {
            get
            {
                string command = "POWE?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unrecognized Source Output Power:" + result,ex);
                }

            }
            set
            {
                string command = "POWE " + value.ToString();
                try
                {
                    instrumentChassis.Write_Unchecked(command, this);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Out of Range:" + value.ToString(),ex);
                }
            }
        }

        /// <summary>
        /// Set/get the IF bandwidth 
        /// Range:Choose from 10, 30,100, 300, 1000, 3000
        /// </summary>
        public override double ForwardCurrentBandwidth
        {
            get
            {
                string command = "IFBW?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unrecognized Forward Current BW:" + result,ex);
                }

            }
            set
            {
                if (value == 10 || value == 30 || value == 100 || value == 300 ||
                        value == 1000 || value == 3000)
                {
                    string command = "IFBW " + value.ToString();
                    instrumentChassis.Write_Unchecked(command, this);
                    return;
                }

                throw new InstrumentException("Out of Range(10,30,100,300,1000,3000):" + value.ToString());
            }
        }

        /// <summary>
        /// Set/get the sweep time
        /// Unit:second
        /// Range:0-86400
        /// </summary>
        public override double SweepTime
        {
            get
            {
                string command = "SWET?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unrecognized Sweep time result:" + result, ex);
                }
            }
            set
            {
                string command="SWEA";
                if (value == AUTO_SWEEP_TIME)
                {
                    instrumentChassis.Write_Unchecked(command, this);
                    return;
                }

                if (value < 0 || value > 86400 )
                    throw new Exception("Out of Range(0-86400):" + value.ToString());
                command = "SWET " + value.ToString()+ " S";                

                instrumentChassis.Write_Unchecked(command, this);

                //The SWET command should be followed by a wait equal to 2 sweeps. 
                System.Threading.Thread.Sleep(Convert.ToInt32(2 * value * 1000));
            }
        }

        /// <summary>
        /// Set/get the reference line value 
        /// </summary>
        public override double ReferenceLineValue
        {
            get
            {
                string command = "REFV?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {
                    throw new InstrumentException("Unknown Refernce Line Value result:" + result, ex);
                }                

            }
            set
            {
                try
                {
                    CheckDisplayFormatRange(value);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
                string command = "REFV " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set the reference value to that of the active marker�s amplitude 
        /// </summary>
        public override void SetReferenceValueToActiveMarker()
        {
            string command = "MARKREF";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Preset the analyzer to the factory preset state 
        /// </summary>
        public override void Preset()
        {
            string command = "PRES";
            WriteDataFromInstrumentWithCheck(command);
        }

        /// <summary>
        /// Sets the trace scale factor 
        /// </summary>
        public override double Scale
        {
            get
            {
                string command = "SCAL?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {
                    throw new InstrumentException("Unkknown Scale returned result:" + result, ex);
                        
                }
            }
            set
            {
                try
                {
                    CheckDisplayFormatRange(value);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string command = "SCAL " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the number of points in the sweep, or in a sweep segment 
        /// Range:3,11,21,26,51,101,201,401,801,1601
        /// </summary>
        public override uint NumberOfSweepPoint
        {
            get
            {
                string command = "POIN?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return Convert.ToUInt32(double.Parse(result));
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unknow sweeep point number result:" + result,ex);
                }
            }
            set
            {
                if (value != 3 && value != 11 && value != 21 && value != 26 && value != 51 && value != 101
                        && value != 201 && value != 401 && value != 801 && value != 1601)
                {
                    throw new InstrumentException("Out of sweep point number Range:" + value.ToString());
                }

                string command = "POIN " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get smoothing state
        /// </summary>
        public override bool SmoothingEnable
        {
            get
            {
                string command = "SMOOO?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "1":
                        return true;
                    case "0":
                        return false;
                    default:
                        throw new InstrumentException("Unknown smoothing state result:" + result);
                }
            }
            set
            {
                string command = "SMOOO ";
                if (value)
                {
                    command = command + "ON";
                }
                else
                {
                    command = command + "OFF";
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the smoothing aperture as a percent of the trace. 
        /// Range:0.05-20
        /// </summary>
        public override double SmoothingAperturePercent
        {
            get
            {
                string command = "SMOOAPER?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {
                    throw new InstrumentException("Unknown smoothing aperture percent result:" + result,ex);
                }
            }
            set
            {
                if (value < 0.05 || value > 20)
                {
                    throw new InstrumentException("Out of smoothing aperture percent range(0.05-20%):" + value.ToString());
                }
                string command = "SMOOAPER " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the averaging on and off on active channel 
        /// </summary>
        public override bool ActiveChannelAveragingEnable
        {
            get
            {
                string command = "AVERO?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "0":
                        return false;
                    case "1":
                        return true;
                    default:
                        throw new InstrumentException("Unknown active channel averaging state:" + result);
                }
            }
            set
            {
                string command = "AVERO ";
                if (value)
                {
                    command = command + "ON";
                }
                else
                {
                    command = command + "OFF";
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the averaging factor on the active channel 
        /// Range:0-999
        /// </summary>
        public override double ActiveChannelAveragingFactor
        {
            get
            {
                string command = "AVERFACT?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unknow active channel averageing result:" + result,ex);
                }
            }
            set
            {
                if (value < 0 || value > 999)
                    throw new InstrumentException("Out of active channel average range(0-999):" + value.ToString());
                string command = "AVERFACT " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Recalls from save/recall registers
        /// </summary>
        /// <param name="registerNumber">register number</param>
        public override void RecallRegister(int registerNumber)
        {
            if (registerNumber < 1 || registerNumber > 31)
                throw new InstrumentException("Out of recall register range(1-5):" + registerNumber.ToString());
            string command = "RECA" + registerNumber.ToString();
            WriteDataFromInstrumentWithCheck(command);
        }

        /// <summary>
        /// Set a specified marker active and its stimulus value 
        /// </summary>
        /// <param name="marker">marker</param>
        /// <param name="value">stimulus value</param>
        public override void SetSpecifiedMarkerStimulus_GHz(Marker marker, double value)
        {
            if (value < 0.13 || value > 20)
            {
                throw new InstrumentException("Out of stimulus range(0.13-20GHz):" + value);
            }
            if (marker < Marker.Marker1 || marker > Marker.Marker5)
            {
                throw new InstrumentException("Out of marker range(mark1-mark5):" + marker.ToString());
            }
            string command = "MARK";
            command = command + Convert.ToInt32(marker).ToString();
            command = command + " " + value.ToString() + " GHZ";
            instrumentChassis.Write_Unchecked(command, this);

        }

        /// <summary>
        /// double ReadSpecifiedMarkerStinulus_GHz(Marker)
        /// </summary>
        /// <param name="marker"></param>
        /// <returns></returns>
        public override double ReadSpecifiedMarkerStimulus_GHz(Marker marker)
        {
            if (marker < Marker.Marker1 || marker > Marker.Marker5)
            {
                throw new InstrumentException("Out of marker range(mark1-mark5):" + marker.ToString());
            }
            string command = "MARK";
            command = command + Convert.ToInt32(marker).ToString() + "?";
            string result = QueryDataFromInstrument(command);
            try
            {
                return double.Parse(result) / GIGA;
            }
            catch (Exception ex)
            {

                throw new InstrumentException("Unknow marker stimulus resutl:" + result,ex);
            }
        }

        /// <summary>
        /// Places the fixed marker at the active marker position and makes it the delta reference
        /// </summary>
        public override void ZeroMarker()
        {
            string command = "MARKZERO";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// search for maximum on current channel's trace 
        /// </summary>
        public override void SetMarkerMaximum()
        {
            string command = "MARKMAXI";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// search for maximum on current channel's trace 
        /// </summary>
        public override void SetMarkerMinimum()
        {
            string command = "MARKMINI";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Trigger instrument sweep in a specified mode 
        /// </summary>
        /// <param name="sweepTriggerMode">Sweep trigger mode</param>
        public override void TriggerSweep(SweepTriggerMode sweepTriggerMode)
        {
            string command = null;
            bool canSupportOPC = false;
            int timeOut = 0;

            switch (sweepTriggerMode)
            {
                case SweepTriggerMode.Single:
                    command = "SING";
                    canSupportOPC = true;
                    break;
                case SweepTriggerMode.Group:
                    double averagineFactor = ActiveChannelAveragingFactor;
                    System.Threading.Thread.Sleep(5);
                    timeOut = instrumentChassis.Timeout_ms;                    
                    System.Threading.Thread.Sleep(5);
                    instrumentChassis.Timeout_ms = (int)(averagineFactor * SweepTime * 1000);
                    System.Threading.Thread.Sleep(5);

                    command = "NUMG" + Convert.ToInt32(averagineFactor).ToString();
                    canSupportOPC = true;
                    break;
                case SweepTriggerMode.Continuous:
                    command = "CONT";
                    break;
                case SweepTriggerMode.Hold:
                    command = "HOLD";
                    break;
                default:
                    throw new InstrumentException("Unknown sweep trigger mode:" + sweepTriggerMode.ToString());
            }
            if (canSupportOPC)
            {                
                WriteDataFromInstrumentWithCheck(command);
                
            }
            else
            {
                instrumentChassis.Write_Unchecked(command, this);
            }
            if (sweepTriggerMode == SweepTriggerMode.Group)
            {
                instrumentChassis.Timeout_ms = timeOut;
            }
        }

        /// <summary>
        /// Set/get start frequency 
        /// Range:0.13-20
        /// </summary>
        public override double StartFrequency_GHz
        {
            get
            {
                string command = "STAR?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result) / GIGA;
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unknow start frequency:" + result,ex);
                }
            }
            set
            {
                if (value < 0.13 || value > 20)
                {
                    throw new InstrumentException("Out of Start Frequence range(0.13-20):" + value.ToString());
                }
                string command = "Star " + value.ToString() + " GHZ";
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get stop frequence 
        /// </summary>
        public override double StopFrequency_GHz
        {
            get
            {
                string command = "STOP?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result) / GIGA;
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unknow stop frequency:" + result, ex);
                }
            }
            set
            {
                if (value < 0.13 || value > 20)
                {
                    throw new InstrumentException("Out of Stop Frequence range(0.13-20):" + value.ToString());
                }
                string command = "Stop " + value.ToString() + " GHZ";
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get s-parameter mode
        /// </summary>
        public override SparameterMeasurementMode SparameterMeasurementMode
        {
            get
            {
                string command = _sparameterMode.ToString() + "?";
                string result = QueryDataFromInstrument(command);
                if (result == "1")
                    return _sparameterMode;


                string[] sXX = Enum.GetNames(typeof(SparameterMeasurementMode));
                foreach (string cmd in sXX)
                {
                    if (cmd != _sparameterMode.ToString())
                    {
                        System.Threading.Thread.Sleep(20);
                        command = cmd + "?";
                        result = instrumentChassis.Query_Unchecked(command, this);
                        if (result == "1")
                        {
                            _sparameterMode = (SparameterMeasurementMode)Enum.Parse(typeof(SparameterMeasurementMode), cmd);
                            return _sparameterMode;
                        }
                    }
                }

                throw new InstrumentException("Unknown S-parameter Measurement mode!");

            }
            set
            {
                string command = null;
                switch (value)
                {
                    case SparameterMeasurementMode.S11:
                        command = "S11";
                        break;
                    case SparameterMeasurementMode.S21:
                        command = "S21";
                        break;
                    case SparameterMeasurementMode.S12:
                        command = "S12";
                        break;
                    case SparameterMeasurementMode.S22:
                        command = "S22";
                        break;
                    default:
                        throw new InstrumentException("Unknow s-parameter mode:" + value.ToString());         
                }
                instrumentChassis.Write_Unchecked(command, this);
                _sparameterMode = value;
            }
        }

        /// <summary>
        /// Set/get display format 
        /// </summary>
        public override DisplayFormat DisplayFormat
        {
            get
            {
                Dictionary<Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer.DisplayFormat, string> dictionary = new Dictionary<DisplayFormat, string>();                
                dictionary.Add(DisplayFormat.LinearMagnitude, "LINM?");
                dictionary.Add(DisplayFormat.LogMagnitude, "LOGM?");
                dictionary.Add(DisplayFormat.Phase, "PHAS?");
                dictionary.Add(DisplayFormat.Polar, "POLA?");
                dictionary.Add(DisplayFormat.Real, "REAL?");
                dictionary.Add(DisplayFormat.SmithChart, "SMIC?");
                dictionary.Add(DisplayFormat.SWR, "SWR?");
                dictionary.Add(DisplayFormat.Imaginary, "IMAG?");//it seems Ag8703a doesn't support this command

                string command = dictionary[_displayFormat];
                string result = QueryDataFromInstrument(command);
                if (result == "1")
                    return _displayFormat;

                dictionary.Remove(_displayFormat);
                foreach (string cmd in dictionary.Values)
                {
                    System.Threading.Thread.Sleep(20);
                    result = QueryDataFromInstrument(cmd);
                    if (result == "1")
                    {
                        foreach (DisplayFormat displayFormat in dictionary.Keys)
                        {
                            if (cmd == dictionary[displayFormat])
                            {
                                _displayFormat = displayFormat;
                                return _displayFormat;
                            }
                        }
                    }
                }

                throw new InstrumentException("Unknow display format!");
            }
            set
            {
                string command = null;
                switch (value)
                {
                    case DisplayFormat.Delay:
                        command = "DELA";
                        break;
                    case DisplayFormat.Imaginary://it seems Ag8703a doesn't support this command
                        command = "IMAG";
                        break;
                    case DisplayFormat.LinearMagnitude:
                        command = "LINM";
                        break;
                    case DisplayFormat.LogMagnitude:
                        command = "LOGM";
                        break;
                    case DisplayFormat.Phase:
                        command = "PHAS";
                        break;
                    case DisplayFormat.Polar:
                        command = "POLA";
                        break;
                    case DisplayFormat.Real:
                        command = "REAL";
                        break;
                    case DisplayFormat.SmithChart:
                        command = "SMIC";
                        break;
                    case DisplayFormat.SWR:
                        command = "SWR";
                        break;
                    default:
                        break;
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Sets electrical length so group delay is zero at the active marker's stimulus.
        /// </summary>
        public override void SetMarkerDelay()
        {
            string command = "MARKDELA";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Set/get the electrical delay offset in second 
        /// Range:-10~+10
        /// </summary>
        public override double ElectricalDelayOffset_s
        {
            get
            {
                string command = "ELED?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Unknown electrical delay offset:" + result,ex);
                }
            }
            set
            {
                if (value < -10 || value > 10)
                {
                    throw new InstrumentException("Out of Electrical delay offset(-10~+10):" + value.ToString());
                }
                string command = "ELED " + value.ToString() + " S";
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get the phase offset. 
        /// Range:0-360 degrees
        /// </summary>
        public override double PhaseOffset
        {
            get
            {
                string command = "PHAO?";
                string result = QueryDataFromInstrument(command);
                try
                {
                    return double.Parse(result);
                }
                catch (Exception ex)
                {
                    throw new InstrumentException("Unknown Phase offset:" + result,ex);
                }
            }
            set
            {
                if (value < 0 || value > 360)
                {
                    throw new InstrumentException("Out of Phase offset range(0-360):" + value.ToString());
                }
                string command = "PHAO " + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Measurement restart 
        /// </summary>
        public override void MeasurementRestart()
        {
            string command = "REST";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Waits for all buffered instructions to complete before returning to the caller 
        /// </summary>
        public override void WaitForCleanSweep()
        {
            string command = "WAIT";
            WriteDataFromInstrumentWithCheck(command);
        }

        /// <summary>
        /// Auto scale the active channel 
        /// </summary>
        public override void AutoScale()
        {
            string command = "AUTO";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Sets the centre stimulus value to that of the active marker's stimulus value. 
        /// </summary>
        public override void SetMarkerToCentre()
        {
            string command = "MARKCENT";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Sets the start stimulus to that of the active marker�s. 
        /// </summary>
        public override void SetMarkerToStart()
        {
            string command = "MARKSTAR";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// gets the trace data in a specified format 
        /// </summary>
        /// <param name="outputFormat"></param>
        /// <returns></returns>
        public override string GetSpecifiedFormatTraceData(OutputFormat outputFormat)
        {
            string command = null;
            switch (outputFormat)
            {
                case OutputFormat.Binary:
                    command = "FORM1";
                    break;
                case OutputFormat.IEEE32bitFloatingPoint:
                    command = "FORM2";
                    break;
                case OutputFormat.IEEE64bitFloatingPoint:
                    command = "FORM3";
                    break;
                case OutputFormat.AsciiFloatingPoint:
                    command = "FORM4";
                    break;
                case OutputFormat.Pcdos32bitFloatingPoint:
                    command = "FORM5";
                    break;
                default:
                    throw new InstrumentException("Invalid output format:" + outputFormat.ToString());
            }

            instrumentChassis.Write_Unchecked(command, this);
            System.Threading.Thread.Sleep(20);

            command = "OUTPFORM";
            string result = QueryDataFromInstrument(command,true);
            return result;
        }        

        /// <summary>
        /// gets the trace data from Ag8703
        /// </summary>
        /// <returns></returns>
        public override Trace GetTraceData()
        {
            string result = GetSpecifiedFormatTraceData(OutputFormat.AsciiFloatingPoint);
            string[] resultArray = result.Split('\n');
            double startFreq = StartFrequency_GHz;
            double stopFreq = StopFrequency_GHz;
            uint sweepCount = NumberOfSweepPoint; 
            double stepFreq = (stopFreq - startFreq) / (sweepCount - 1);
            double x=0;
            double y=0;
            int i = 0;

            Trace trace = new Trace();

            try
            {

                foreach (string pointString in resultArray)
                {
                    int index = pointString.IndexOf(",");
                    x = startFreq + i * stepFreq;
                    y = double.Parse(pointString.Substring(0, index));
                    i++;

                    trace.Add(x, y);
                }

            }
            catch (Exception ex)
            {
                throw new InstrumentException("cannot get trace data!", ex);
            }
            //for (int i = 0; i < sweepCount; i++)
            //{
            //    x = startFreq + i * stepFreq;
                
            //    y = (double)BitConverter.ToSingle(resultBytes, point); 
            //    trace.Add(x, y);
            //}

            
            return trace;
        }

        #endregion

        #region public Agilent 8703 specified methods

        /// <summary>
        /// new version for NumberOfSweepPoint
        /// suggest to use this method if using set
        /// </summary>
        public SweepPointCount NumberOfSweepPoint2
        {
            set
            {
                NumberOfSweepPoint = Convert.ToUInt32(value);
            }
            get
            {
                SweepPointCount value=SweepPointCount.Number801;
                try
                {
                    value = (SweepPointCount)Enum.Parse(typeof(SweepPointCount), NumberOfSweepPoint.ToString());
                }
                catch (Exception ex)
                {
                    throw new InstrumentException("cannot convert number of sweep point:" + NumberOfSweepPoint.ToString(),ex);
                }                
               
                return value;
                
            }
        }

        /// <summary>
        /// Set/get number of channels which display in instrument monitor 
        /// only support one or dual
        /// </summary>
        public ChannelDisplayNumber ChannelDisplayNumber
        {
            get
            {
                string command = "DUAC?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "0":
                        return ChannelDisplayNumber.One;
                    case "1":
                        return ChannelDisplayNumber.Dual;
                    default:
                        throw new InstrumentException("Unknown channel display number result" + result);
                }
            }
            set
            {
                string command = "DUAC ";
                switch (value)
                {
                    case ChannelDisplayNumber.One:
                        command = command + "OFF";
                        break;
                    case ChannelDisplayNumber.Dual:
                        command = command + "ON";
                        break;
                    default:
                        throw new InstrumentException("Invalid channel display number!" + value.ToString());
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get mark couple state 
        /// </summary>
        public MarkerCoupleState MarkerCoupleState
        {
            get
            {
                string command = "MARKCOUP?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "0":
                        return MarkerCoupleState.MarkerUnCouple;
                    case "1":
                        return MarkerCoupleState.MarkerCouple;
                    default:
                        throw new InstrumentException("Unknown marker couple state:" +result);

                }
            }
            set
            {
                string command = null;
                switch (value)
                {
                    case MarkerCoupleState.MarkerCouple:
                        command = "MARKCOUP";
                        break;
                    case MarkerCoupleState.MarkerUnCouple:
                        command = "MARKUNCO";
                        break;
                    default:
                        throw new InstrumentException("invalid marker couple state:" + value.ToString());
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreTalkerListenerOperation()
        {
            string command = "TALKLIST";
            instrumentChassis.Write_Unchecked(command,this);
        }

        /// <summary>
        /// not supportedin 8703A
        /// </summary>
        public bool FrequencySubsetCalibrationEnable
        {
            get
            {
                string commad = "FRES?";
                string result = QueryDataFromInstrument(commad);
                switch (result)
                {
                    case "0":
                        return false;
                    case "1":
                        return true;
                    default:
                        throw new InstrumentException("Unknown frequency subset calibration:" + result);
                }
            }
            set
            {
                string command = null;
                if (value)
                {
                    command = "FRESON";
                }
                else
                {
                    command = "FRESOFF";
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Set/get measurement mode 
        /// </summary>
        public PortMeasurementType PortMeasurementType
        {
            get
            {
                string command = "MEAS" + _portMeasurementType.ToString() +"?";
                string result = QueryDataFromInstrument(command);
                if (result == "1")
                    return _portMeasurementType;

                foreach (string cmd in Enum.GetNames(typeof(PortMeasurementType)))
                {
                    if (cmd != _portMeasurementType.ToString())
                    {
                        command = "MEAS" + cmd +"?";
                        result = QueryDataFromInstrument(command);
                        System.Threading.Thread.Sleep(5);
                        if (result == "1")
                        {
                            return (PortMeasurementType)Enum.Parse(typeof(PortMeasurementType), cmd);
                        }
                    }

                }

                throw new InstrumentException("Unknow port measurement returned type!");
            }
            set
            {
                string command = null;
                switch (value)
                {
                    case PortMeasurementType.A:
                        command = "MEASA";
                        break;
                    case PortMeasurementType.B:
                        command = "MEASB";
                        break;
                    case PortMeasurementType.R:
                        command = "MEASR";
                        break;
                    case PortMeasurementType.EO1:
                        command = "MEASEO1";
                        break;
                    case PortMeasurementType.O1:
                        command = "MEASO1";
                        break;
                    case PortMeasurementType.OE1:
                        command = "MEASOE1";
                        break;
                    case PortMeasurementType.OE2:
                        command = "MEASOE2";
                        break;
                    case PortMeasurementType.Off:
                        command = "MEASOFF";
                        break;
                    case PortMeasurementType.OO1:
                        command = "MEASOO1";
                        break;
                    default:
                        throw new InstrumentException("invalid port measurement type:" + value.ToString());
                }
                _portMeasurementType = value;
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// Saves to save/recall registers 01?1.
        /// </summary>
        /// <param name="register"></param>
        public void SaveStateInSpecifiedRegister(int register)
        {
            if (register < 1 || register > 31)
                throw new InstrumentException("Out of Range(1~31):" + register.ToString());
            string command = "SAVE" + register;
            instrumentChassis.Write_Unchecked(command, this);
            //WriteDataFromInstrumentWithCheck(command);

        }

        /// <summary>
        /// Begins the sequence for an S11 1-port calibration (ES models), 
        /// or a reflection 1-port calibration (ET models)
        /// </summary>
        public void CalibrateS11OnPort1()
        {
            string command = "CALIS111";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Begins the sequence for an S22 1-port
        /// calibration.
        /// </summary>
        public void CalibrateS22OnPort1()
        {
            string command = "CALIS221";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Begins the sequence for a short, load, open,
        /// thru (SLOT) 2-port calibration.
        /// </summary>
        public void CalbrateShortOpenLoadOnPort2()
        {
            string command = "CALIFUL2";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Begins the sequence for a response calibration. 
        /// </summary>
        public void CalibrateResponse()
        {
            string command = "CALIRESP";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Acts as though the indicated softkey was pressed. 
        /// </summary>
        /// <param name="softkey"></param>
        public void PressSoftkey(Softkey softkey)
        {
            string command = null;
            switch (softkey)
            {
                case Softkey.Softkey1:
                    command = "SOFT1";
                    break;
                case Softkey.Softkey2:
                    command = "SOFT2";
                    break;
                case Softkey.Softkey3:
                    command = "SOFT3";
                    break;
                case Softkey.Softkey4:
                    command = "SOFT4";
                    break;
                case Softkey.Softkey5:
                    command = "SOFT5";
                    break;
                case Softkey.Softkey6:
                    command = "SOFT6";
                    break;
                case Softkey.Softkey7:
                    command = "SOFT7";
                    break;
                case Softkey.Softkey8:
                    command = "SOFT8";
                    break;
                default:
                    throw new InstrumentException("Invalid softkey:" + softkey.ToString());
            }
            instrumentChassis.Write_Unchecked(command, this);
            
        }

        /// <summary>
        /// Completes the response calibration sequence 
        /// </summary>
        public void CompleteResponseCalibrationSequence()
        {
            string command = "RESPDONE";
            WriteDataFromInstrumentWithCheck(command);
        }
        
        /// <summary>
        /// Completes the reflection calibration subsequence of a 2-port calibration. 
        /// </summary>
        public void CompleteReflectionCalibration()
        {
            string command = "REFD";
            WriteDataFromInstrumentWithCheck(command);
        }

        /// <summary>
        /// For 7 Standard commands:
        ///Select a standard from a class during a calibration sequence.
        ///If a class is requested, the analyzer will do one of two things:
        ///?If there is only one standard in the class, it will measure that 
        /// standard automatically.
        ///?If there are several standards in the class, then one of these 
        /// commands must be used to select one of these standards, causing it
        /// to be measured.
        ///For CLASS commands:
        ///call reflection standard classes during a calibration sequence. If 
        ///only one standard is in the class, it is measured. If there is more 
        ///than one, the standard being used must be selected with Standard 
        ///commands.
        /// </summary>
        /// <param name="calibrationStandardType"></param>
        public void MeasureCalibraionStandard(CalibrationStandardType calibrationStandardType)
        {
            string command = null;
            switch (calibrationStandardType)
            {
                case CalibrationStandardType.STANA:
                    command = "STANA";
                    break;
                case CalibrationStandardType.STANB:
                    command = "STANB";
                    break;
                case CalibrationStandardType.STANC:
                    command = "STANC";
                    break;
                case CalibrationStandardType.STAND:
                    command = "STAND";
                    break;
                case CalibrationStandardType.STANE:
                    command = "STANE";
                    break;
                case CalibrationStandardType.STANF:
                    command = "STANF";
                    break;
                case CalibrationStandardType.STANG:
                    command = "STANG";
                    break;
                case CalibrationStandardType.CLASS11A:
                    command = "CLASS11A";
                    break;
                case CalibrationStandardType.CLASS11B:
                    command = "CLASS11B";
                    break;
                case CalibrationStandardType.CLASS11C:
                    command = "CLASS11C";
                    break;
                case CalibrationStandardType.CLASS22A:
                    command = "CLASS22A";
                    break;
                case CalibrationStandardType.CLASS22B:
                    command = "CLASS22B";
                    break;
                case CalibrationStandardType.CLASS22C:
                    command = "CLASS22C";
                    break;
                default:
                    throw new InstrumentException("Invalid calibration standard type:" + calibrationStandardType.ToString());
            }
            WriteDataFromInstrumentWithCheck(command);

        }

        /// <summary>
        /// Outputs the oldest error message in the error queue. Sends the error number first,
        /// and then the error message itself, as an ASCII string no longer than 50 characters. 
        /// </summary>
        public string ErrorString
        {
            get
            {
                string command="OUTPERRO?";
                return QueryDataFromInstrument(command);
            }
        }

        /// <summary>
        /// Set/get error correction is on or off 
        /// </summary>
        public bool ErrorCorrectionEnable
        {
            get
            {
                string command = "CORR?";
                string result = QueryDataFromInstrument(command);
                switch (result)
                {
                    case "0":
                        return false;
                    case "1":
                        return true;
                    default:
                        throw new InstrumentException("Unknown error correction state:" + result);
                }
	
            }
            set
            {
                string command=null;
                if (value)
                {
                    command = "CORR ON";
                }
                else
                {
                    command = "CORR OFF";
                }
                instrumentChassis.Write_Unchecked(command, this);

            }
        }

        #endregion

        #region Private members
        /// <summary>
        /// The Ag8703A is quit old.There is an issue when using Query method in chassis directly.
        /// </summary>
        /// <param name="command">command</param>
        /// <returns>query result</returns>
        private string QueryDataFromInstrument(string command)
        {
            return QueryDataFromInstrument(command, false);
        }

        /// <summary>
        /// The Ag8703A is quit old.There is an issue when using Query method in chassis directly.
        /// </summary>
        /// <param name="command">command</param>
        /// <param name="largeData">if it's large data,largeData=true</param>
        /// <returns>query result</returns>
        private string QueryDataFromInstrument(string command,bool largeData)
        {
            try
            {
                if (largeData)
                {
                    int timeOut = instrumentChassis.Timeout_ms;
                    if (timeOut > 6000)
                    {
                        System.Threading.Thread.Sleep(5);
                        instrumentChassis.Timeout_ms = 6000;
                    }
                    System.Threading.Thread.Sleep(10);
                    instrumentChassis.Write_Unchecked(command, this);
                    System.Threading.Thread.Sleep(5);                    

                    StringBuilder builder = new StringBuilder();
                    builder.Append(instrumentChassis.Read_Unchecked(this));
                    bool completed = false;
                    while (!completed)
                    {
                        try
                        {
                            System.Threading.Thread.Sleep(5);
                            builder.Append(instrumentChassis.Read_Unchecked(this));
                        }
                        catch
                        {
                            completed = true;
                        }
                    }

                    System.Threading.Thread.Sleep(5);

                    instrumentChassis.Timeout_ms = timeOut;
                    return builder.ToString();
                }
                else
                {
                    instrumentChassis.Write_Unchecked(command, this);
                    System.Threading.Thread.Sleep(5);
                    string result = instrumentChassis.Read_Unchecked(this);
                    return result;

                }

                
            }
            catch (Exception ex)
            {
                throw new InstrumentException("cannot query dat from instrument.Command is:" + command, ex);
            }
        }


        /// <summary>
        /// This method is used for the commands which is OPC-compatible.
        /// the method "Query""/"Write" of ChassisType_Visa488_2 doesn't work
        /// </summary>
        /// <param name="command"></param>
        private void WriteDataFromInstrumentWithCheck(string command)
        {
            string newCommand = "*OPC?;" + command;
            string result= QueryDataFromInstrument(newCommand);
            if (result == "0")
            {
                throw new InstrumentException("The command is not completed? The command is:" + newCommand);

            }
        }



        /// <summary>
        /// check whether value is in the range when the instrument is in current display format
        /// if not,throw an exception
        /// </summary>
        /// <param name="value"></param>
        private void CheckDisplayFormatRange(double value)
        {
            double min = double.MinValue;
            double max = double.MaxValue;
            DisplayFormat displayFormat = this.DisplayFormat;
            switch (displayFormat)
            {
                case DisplayFormat.Delay:
                    break;
                case DisplayFormat.Imaginary:
                    break;
                case DisplayFormat.LinearMagnitude:
                    max = 500;
                    min = 10;
                    break;
                case DisplayFormat.LogMagnitude:
                    max = 500;
                    min = 0.001;
                    break;
                case DisplayFormat.Phase:
                    max = 500;
                    min = 1.0e-12;
                    break;
                case DisplayFormat.Polar:
                    max = 500;
                    min = 1.0e-15;
                    break;
                case DisplayFormat.Real:
                    break;
                case DisplayFormat.SmithChart:
                    max = 500;
                    min = 1.0e-15;
                    break;
                case DisplayFormat.SWR:
                    break;
                default:
                    throw new InstrumentException("Unknown Display Format!");
            }

            if (value < min || value > max)
            {
                throw new InstrumentException("Out of Reference Line Value Range(" + min.ToString() + "-" + max.ToString() +
                        "):" + value.ToString());
            }
        }        

        /// <summary>
        /// 1 Giga=1.0e+9
        /// </summary>
        private const double GIGA = 1e+9;

        /// <summary>
        /// used to contain s-parameter value and improve speed
        /// </summary>
        private SparameterMeasurementMode _sparameterMode = SparameterMeasurementMode.S11;

        /// <summary>
        /// used to contain display format value and improve speed
        /// </summary>
        private DisplayFormat _displayFormat = DisplayFormat.LogMagnitude;

        private PortMeasurementType _portMeasurementType = PortMeasurementType.A;
        #endregion
    }
}

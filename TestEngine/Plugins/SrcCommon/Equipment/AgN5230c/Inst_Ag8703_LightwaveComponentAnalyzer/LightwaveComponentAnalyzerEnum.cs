using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer
{
    /// <summary>
    /// Number of Sweep point enum
    /// </summary>
    public enum SweepPointCount
    {
        /// <summary>
        /// 
        /// </summary>
        Number3=3,
        /// <summary>
        /// 
        /// </summary>
        Number11=11,
        /// <summary>
        /// 
        /// </summary>
        Number21=21,
        /// <summary>
        /// 
        /// </summary>
        Number26=26,
        /// <summary>
        /// 
        /// </summary>
        Number51=51,
        /// <summary>
        /// 
        /// </summary>
        Number101=101,
        /// <summary>
        /// 
        /// </summary>
        Number201=201,
        /// <summary>
        /// 
        /// </summary>
        Number401=401,
        /// <summary>
        /// 
        /// </summary>
        Number801=801,
        /// <summary>
        /// 
        /// </summary>
        Number1601=1601
    }

    /// <summary>
    /// number of channels which display in the instrument monitor
    /// </summary>
    public enum ChannelDisplayNumber
    {
        /// <summary>
        /// 
        /// </summary>
        One,
        /// <summary>
        /// display dual channels at the same time
        /// </summary>
        Dual
    }

    /// <summary>
    /// port measurement type
    /// </summary>
    public enum PortMeasurementType
    {
        /// <summary>
        /// Measures and displays input A on the active channel.
        /// </summary>
        A,
        /// <summary>
        /// Measures and displays input B on the active channel.
        /// </summary>
        B,
        /// <summary>
        /// Measures and displays input R on the active channel.
        /// </summary>
        R,
        /// <summary>
        /// Measures and displays electrical-to-optical transmission.
        /// </summary>
        EO1,
        /// <summary>
        /// Measures and displays optical reflection.
        /// </summary>
        O1,
        /// <summary>
        /// Measures and displays optical-to-electrical transmission in port 1
        /// </summary>
        OE1,
        /// <summary>
        /// Measures and displays optical-to-electrical transmission in port 2.
        /// </summary>
        OE2,
        /// <summary>
        /// Switches off marker function measurements.
        /// </summary>
        Off,
        /// <summary>
        /// Measures and displays optical transmission
        /// </summary>
        OO1
    }

    /// <summary>
    /// couple state
    /// </summary>
    public enum MarkerCoupleState
    {
        /// <summary>
        /// 
        /// </summary>
        MarkerCouple,
        /// <summary>
        /// 
        /// </summary>
        MarkerUnCouple
    }

    /// <summary>
    /// softkey on the instrument
    /// </summary>
    public enum Softkey
    {
        /// <summary>
        /// 
        /// </summary>
        Softkey1,
        /// <summary>
        /// 
        /// </summary>
        Softkey2,
        /// <summary>
        /// 
        /// </summary>
        Softkey3,
        /// <summary>
        /// 
        /// </summary>
        Softkey4,
        /// <summary>
        /// 
        /// </summary>
        Softkey5,
        /// <summary>
        /// 
        /// </summary>
        Softkey6,
        /// <summary>
        /// 
        /// </summary>
        Softkey7,
        /// <summary>
        /// 
        /// </summary>
        Softkey8
    }

    /// <summary>
    /// calibration standard type
    /// </summary>
    public enum CalibrationStandardType
    {
        /// <summary>
        /// 
        /// </summary>
        STANA,
        /// <summary>
        /// 
        /// </summary>
        STANB,
        /// <summary>
        /// 
        /// </summary>
        STANC,
        /// <summary>
        /// 
        /// </summary>
        STAND,
        /// <summary>
        /// 
        /// </summary>
        STANE,
        /// <summary>
        /// 
        /// </summary>
        STANF,
        /// <summary>
        /// 
        /// </summary>
        STANG,
        /// <summary>
        /// 
        /// </summary>
        CLASS11A,
        /// <summary>
        /// 
        /// </summary>
        CLASS11B,
        /// <summary>
        /// 
        /// </summary>
        CLASS11C,
        /// <summary>
        /// 
        /// </summary>
        CLASS22A,
        /// <summary>
        /// 
        /// </summary>
        CLASS22B,
        /// <summary>
        /// 
        /// </summary>
        CLASS22C
    }
}

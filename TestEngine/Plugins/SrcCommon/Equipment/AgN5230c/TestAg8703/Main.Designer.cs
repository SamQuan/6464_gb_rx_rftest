namespace TestAg8703
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonInitialize = new System.Windows.Forms.Button();
            this.buttonGetStartFrequency = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonGetStopFrequency = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonGetTraceData = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonPreset = new System.Windows.Forms.Button();
            this.buttonSetActiveChannel = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownChannel = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonGetDisplayFormat = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonGetAveragingFactor = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonTriggerSweep = new System.Windows.Forms.Button();
            this.comboBoxSweepMode = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonSetSourcePower = new System.Windows.Forms.Button();
            this.numericUpDownPower = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonGetSourcePower = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonSetSweepTime = new System.Windows.Forms.Button();
            this.numericUpDownSweepTime = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonGetSweepTime = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.listBoxOutput = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownGPIB = new System.Windows.Forms.NumericUpDown();
            this.buttonClearOutput = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonCalibrateS22 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSweepTime)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGPIB)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonInitialize, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetStartFrequency, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetStopFrequency, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetTraceData, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.buttonPreset, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.buttonSetActiveChannel, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownChannel, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetDisplayFormat, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetAveragingFactor, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.buttonTriggerSweep, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.comboBoxSweepMode, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.buttonSetSourcePower, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownPower, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetSourcePower, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.buttonSetSweepTime, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownSweepTime, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.buttonGetSweepTime, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label16, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.buttonCalibrateS22, 1, 14);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(537, 210);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "2";
            // 
            // buttonInitialize
            // 
            this.buttonInitialize.Location = new System.Drawing.Point(59, 6);
            this.buttonInitialize.Name = "buttonInitialize";
            this.buttonInitialize.Size = new System.Drawing.Size(144, 29);
            this.buttonInitialize.TabIndex = 2;
            this.buttonInitialize.Text = "Initialize";
            this.buttonInitialize.UseVisualStyleBackColor = true;
            this.buttonInitialize.Click += new System.EventHandler(this.buttonInitialize_Click);
            // 
            // buttonGetStartFrequency
            // 
            this.buttonGetStartFrequency.Location = new System.Drawing.Point(59, 44);
            this.buttonGetStartFrequency.Name = "buttonGetStartFrequency";
            this.buttonGetStartFrequency.Size = new System.Drawing.Size(144, 29);
            this.buttonGetStartFrequency.TabIndex = 3;
            this.buttonGetStartFrequency.Text = "Get Start Frequency";
            this.buttonGetStartFrequency.UseVisualStyleBackColor = true;
            this.buttonGetStartFrequency.Click += new System.EventHandler(this.buttonGetStartFrequency_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "3";
            // 
            // buttonGetStopFrequency
            // 
            this.buttonGetStopFrequency.Location = new System.Drawing.Point(59, 82);
            this.buttonGetStopFrequency.Name = "buttonGetStopFrequency";
            this.buttonGetStopFrequency.Size = new System.Drawing.Size(144, 29);
            this.buttonGetStopFrequency.TabIndex = 6;
            this.buttonGetStopFrequency.Text = "Get Stop Frequency";
            this.buttonGetStopFrequency.UseVisualStyleBackColor = true;
            this.buttonGetStopFrequency.Click += new System.EventHandler(this.buttonGetStopFrequency_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "4";
            // 
            // buttonGetTraceData
            // 
            this.buttonGetTraceData.Location = new System.Drawing.Point(59, 120);
            this.buttonGetTraceData.Name = "buttonGetTraceData";
            this.buttonGetTraceData.Size = new System.Drawing.Size(144, 27);
            this.buttonGetTraceData.TabIndex = 8;
            this.buttonGetTraceData.Text = "Get Trace Data";
            this.buttonGetTraceData.UseVisualStyleBackColor = true;
            this.buttonGetTraceData.Click += new System.EventHandler(this.buttonGetTraceData_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "5";
            // 
            // buttonPreset
            // 
            this.buttonPreset.Location = new System.Drawing.Point(59, 158);
            this.buttonPreset.Name = "buttonPreset";
            this.buttonPreset.Size = new System.Drawing.Size(144, 28);
            this.buttonPreset.TabIndex = 10;
            this.buttonPreset.Text = "Preset";
            this.buttonPreset.UseVisualStyleBackColor = true;
            this.buttonPreset.Click += new System.EventHandler(this.buttonPreset_Click);
            // 
            // buttonSetActiveChannel
            // 
            this.buttonSetActiveChannel.Location = new System.Drawing.Point(59, 196);
            this.buttonSetActiveChannel.Name = "buttonSetActiveChannel";
            this.buttonSetActiveChannel.Size = new System.Drawing.Size(144, 29);
            this.buttonSetActiveChannel.TabIndex = 11;
            this.buttonSetActiveChannel.Text = "Set Active Channel";
            this.buttonSetActiveChannel.UseVisualStyleBackColor = true;
            this.buttonSetActiveChannel.Click += new System.EventHandler(this.buttonSetActiveChannel_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "6";
            // 
            // numericUpDownChannel
            // 
            this.numericUpDownChannel.Location = new System.Drawing.Point(212, 196);
            this.numericUpDownChannel.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownChannel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownChannel.Name = "numericUpDownChannel";
            this.numericUpDownChannel.Size = new System.Drawing.Size(144, 20);
            this.numericUpDownChannel.TabIndex = 13;
            this.numericUpDownChannel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "7";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(59, 234);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 28);
            this.button1.TabIndex = 15;
            this.button1.Text = "Get Active Channel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "8";
            // 
            // buttonGetDisplayFormat
            // 
            this.buttonGetDisplayFormat.Location = new System.Drawing.Point(59, 272);
            this.buttonGetDisplayFormat.Name = "buttonGetDisplayFormat";
            this.buttonGetDisplayFormat.Size = new System.Drawing.Size(142, 29);
            this.buttonGetDisplayFormat.TabIndex = 17;
            this.buttonGetDisplayFormat.Text = "Get Display Format";
            this.buttonGetDisplayFormat.UseVisualStyleBackColor = true;
            this.buttonGetDisplayFormat.Click += new System.EventHandler(this.buttonGetDisplayFormat_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 307);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "9";
            // 
            // buttonGetAveragingFactor
            // 
            this.buttonGetAveragingFactor.Location = new System.Drawing.Point(59, 310);
            this.buttonGetAveragingFactor.Name = "buttonGetAveragingFactor";
            this.buttonGetAveragingFactor.Size = new System.Drawing.Size(142, 29);
            this.buttonGetAveragingFactor.TabIndex = 19;
            this.buttonGetAveragingFactor.Text = "Get Averaging Factor";
            this.buttonGetAveragingFactor.UseVisualStyleBackColor = true;
            this.buttonGetAveragingFactor.Click += new System.EventHandler(this.buttonGetAveragingFactor_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 345);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "10";
            // 
            // buttonTriggerSweep
            // 
            this.buttonTriggerSweep.Location = new System.Drawing.Point(59, 348);
            this.buttonTriggerSweep.Name = "buttonTriggerSweep";
            this.buttonTriggerSweep.Size = new System.Drawing.Size(142, 29);
            this.buttonTriggerSweep.TabIndex = 21;
            this.buttonTriggerSweep.Text = "Trigger Sweep";
            this.buttonTriggerSweep.UseVisualStyleBackColor = true;
            this.buttonTriggerSweep.Click += new System.EventHandler(this.buttonTriggerSweep_Click);
            // 
            // comboBoxSweepMode
            // 
            this.comboBoxSweepMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSweepMode.FormattingEnabled = true;
            this.comboBoxSweepMode.Items.AddRange(new object[] {
            "Single",
            "Group",
            "Continuous",
            "Hold"});
            this.comboBoxSweepMode.Location = new System.Drawing.Point(212, 348);
            this.comboBoxSweepMode.Name = "comboBoxSweepMode";
            this.comboBoxSweepMode.Size = new System.Drawing.Size(139, 21);
            this.comboBoxSweepMode.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 383);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "11";
            // 
            // buttonSetSourcePower
            // 
            this.buttonSetSourcePower.Location = new System.Drawing.Point(59, 386);
            this.buttonSetSourcePower.Name = "buttonSetSourcePower";
            this.buttonSetSourcePower.Size = new System.Drawing.Size(142, 29);
            this.buttonSetSourcePower.TabIndex = 24;
            this.buttonSetSourcePower.Text = "Set Source Power";
            this.buttonSetSourcePower.UseVisualStyleBackColor = true;
            this.buttonSetSourcePower.Click += new System.EventHandler(this.buttonSetSourcePower_Click);
            // 
            // numericUpDownPower
            // 
            this.numericUpDownPower.DecimalPlaces = 1;
            this.numericUpDownPower.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownPower.Location = new System.Drawing.Point(212, 386);
            this.numericUpDownPower.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownPower.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numericUpDownPower.Name = "numericUpDownPower";
            this.numericUpDownPower.Size = new System.Drawing.Size(138, 20);
            this.numericUpDownPower.TabIndex = 25;
            this.numericUpDownPower.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 421);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "12";
            // 
            // buttonGetSourcePower
            // 
            this.buttonGetSourcePower.Location = new System.Drawing.Point(59, 424);
            this.buttonGetSourcePower.Name = "buttonGetSourcePower";
            this.buttonGetSourcePower.Size = new System.Drawing.Size(142, 29);
            this.buttonGetSourcePower.TabIndex = 27;
            this.buttonGetSourcePower.Text = "Get Source Power";
            this.buttonGetSourcePower.UseVisualStyleBackColor = true;
            this.buttonGetSourcePower.Click += new System.EventHandler(this.buttonGetSourcePower_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 459);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "13";
            // 
            // buttonSetSweepTime
            // 
            this.buttonSetSweepTime.Location = new System.Drawing.Point(59, 462);
            this.buttonSetSweepTime.Name = "buttonSetSweepTime";
            this.buttonSetSweepTime.Size = new System.Drawing.Size(142, 29);
            this.buttonSetSweepTime.TabIndex = 29;
            this.buttonSetSweepTime.Text = "Set Sweep Time";
            this.buttonSetSweepTime.UseVisualStyleBackColor = true;
            this.buttonSetSweepTime.Click += new System.EventHandler(this.buttonSetSweepTime_Click);
            // 
            // numericUpDownSweepTime
            // 
            this.numericUpDownSweepTime.Location = new System.Drawing.Point(212, 462);
            this.numericUpDownSweepTime.Name = "numericUpDownSweepTime";
            this.numericUpDownSweepTime.Size = new System.Drawing.Size(132, 20);
            this.numericUpDownSweepTime.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 497);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "14";
            // 
            // buttonGetSweepTime
            // 
            this.buttonGetSweepTime.Location = new System.Drawing.Point(59, 500);
            this.buttonGetSweepTime.Name = "buttonGetSweepTime";
            this.buttonGetSweepTime.Size = new System.Drawing.Size(142, 29);
            this.buttonGetSweepTime.TabIndex = 32;
            this.buttonGetSweepTime.Text = "Get Sweep Time";
            this.buttonGetSweepTime.UseVisualStyleBackColor = true;
            this.buttonGetSweepTime.Click += new System.EventHandler(this.buttonGetSweepTime_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(212, 231);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "Don\'t support";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 210);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(537, 10);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxOutput);
            this.panel1.Controls.Add(this.listBoxOutput);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.numericUpDownGPIB);
            this.panel1.Controls.Add(this.buttonClearOutput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 220);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(537, 265);
            this.panel1.TabIndex = 3;
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutput.Location = new System.Drawing.Point(3, 140);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutput.Size = new System.Drawing.Size(531, 95);
            this.textBoxOutput.TabIndex = 5;
            // 
            // listBoxOutput
            // 
            this.listBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxOutput.FormattingEnabled = true;
            this.listBoxOutput.Location = new System.Drawing.Point(0, 0);
            this.listBoxOutput.Name = "listBoxOutput";
            this.listBoxOutput.Size = new System.Drawing.Size(537, 134);
            this.listBoxOutput.TabIndex = 4;
            this.listBoxOutput.Click += new System.EventHandler(this.listBoxOutput_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "GPIB Address:";
            // 
            // numericUpDownGPIB
            // 
            this.numericUpDownGPIB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownGPIB.Location = new System.Drawing.Point(94, 241);
            this.numericUpDownGPIB.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDownGPIB.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownGPIB.Name = "numericUpDownGPIB";
            this.numericUpDownGPIB.Size = new System.Drawing.Size(92, 20);
            this.numericUpDownGPIB.TabIndex = 2;
            this.numericUpDownGPIB.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // buttonClearOutput
            // 
            this.buttonClearOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearOutput.Location = new System.Drawing.Point(442, 237);
            this.buttonClearOutput.Name = "buttonClearOutput";
            this.buttonClearOutput.Size = new System.Drawing.Size(81, 25);
            this.buttonClearOutput.TabIndex = 1;
            this.buttonClearOutput.Text = "Clear Output";
            this.buttonClearOutput.UseVisualStyleBackColor = true;
            this.buttonClearOutput.Click += new System.EventHandler(this.buttonClearOutput_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 535);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "15";
            // 
            // buttonCalibrateS22
            // 
            this.buttonCalibrateS22.Location = new System.Drawing.Point(59, 538);
            this.buttonCalibrateS22.Name = "buttonCalibrateS22";
            this.buttonCalibrateS22.Size = new System.Drawing.Size(142, 29);
            this.buttonCalibrateS22.TabIndex = 35;
            this.buttonCalibrateS22.Text = "Calibrate S22";
            this.buttonCalibrateS22.UseVisualStyleBackColor = true;
            this.buttonCalibrateS22.Click += new System.EventHandler(this.buttonCalibrateS22_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 485);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Main";
            this.Text = "Ag8703 Test";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSweepTime)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGPIB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonClearOutput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonInitialize;
        private System.Windows.Forms.NumericUpDown numericUpDownGPIB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxOutput;
        private System.Windows.Forms.Button buttonGetStartFrequency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonGetStopFrequency;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonGetTraceData;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonPreset;
        private System.Windows.Forms.Button buttonSetActiveChannel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownChannel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonGetDisplayFormat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonGetAveragingFactor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonTriggerSweep;
        private System.Windows.Forms.ComboBox comboBoxSweepMode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonSetSourcePower;
        private System.Windows.Forms.NumericUpDown numericUpDownPower;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonGetSourcePower;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button buttonSetSweepTime;
        private System.Windows.Forms.NumericUpDown numericUpDownSweepTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonGetSweepTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonCalibrateS22;
    }
}


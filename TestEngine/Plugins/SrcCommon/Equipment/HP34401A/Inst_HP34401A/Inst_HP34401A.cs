// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_HP34401A.cs
//
// Author: koonlin.peng, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_HP34401A : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_HP34401A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34401A",				// hardware name 
                "0.0.0.0",  			// minimum valid firmware version 
                "2.0.0.0");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_HP34401A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("HP34401", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_HP34401A)chassisInit;
            this.instrumentChassis.Timeout_ms = 10000;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.MeasureMothod = MeasureFunc.CURR_DC;
            this.CalulateMethod = CALCulateFunc.NULL;
            this.CalculateState = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online
                    this.instrumentChassis.Write_Unchecked("*CLS", this);
                }
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public CALCulateFunc CalulateMethod
        {
            get
            {
                string res = this.instrumentChassis.Query_Unchecked("CALC:FUNC?", this);
                if (res == CALCulateFunc.NULL.ToString())
                    return CALCulateFunc.NULL;
                else if (res == CALCulateFunc.DB.ToString())
                    return CALCulateFunc.DB;
                else if (res == CALCulateFunc.DBM.ToString())
                    return CALCulateFunc.DBM;
                else if (res == CALCulateFunc.AVER.ToString())
                    return CALCulateFunc.AVER;
                else if (res == CALCulateFunc.LIM.ToString())
                    return CALCulateFunc.LIM;
                else
                    throw new Exception("Invalid response");
            }
            set
            {
                switch (value)
                {
                    case CALCulateFunc.NULL:
                        this.instrumentChassis.Write_Unchecked("CALC:FUNC NULL", this);
                        break;
                    case CALCulateFunc.DB:
                        this.instrumentChassis.Write_Unchecked("CALC:FUNC DB", this);
                        break;
                    case CALCulateFunc.DBM:
                        this.instrumentChassis.Write_Unchecked("CALC:FUNC DBM", this);
                        break;
                    case CALCulateFunc.AVER:
                        this.instrumentChassis.Write_Unchecked("CALC:FUNC AVER", this);
                        break;
                    case CALCulateFunc.LIM:
                        this.instrumentChassis.Write_Unchecked("CALC:FUNC LIM", this);
                        break;
                    default:
                        throw new Exception("Invalid calculate Func.");
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        public short TrigCount
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("TRIG:COUN?", this);
                if (result.Length != 0)
                    return Convert.ToInt16(result);
                else
                    return 0;
            }
            set
            {
                this.instrumentChassis.Write_Unchecked(string.Format("TRIG:COUN {0}", value), this);
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool CalculateState
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("CALC:STAT?", this);
                return (result == "1") ? true : false;
            }
            set
            {
                if (value)
                {
                    this.instrumentChassis.Write_Unchecked("CALC:STAT ON", this);
                    System.Threading.Thread.Sleep(500);
                    this.instrumentChassis.Write_Unchecked("INIT", this);
                }
                else
                {
                    this.instrumentChassis.Write_Unchecked("CALC:STAT OFF", this);
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public MeasureFunc MeasureMothod
        {
            set
            {
                switch(value)
                {
                    case MeasureFunc.VOLT_DC:
                        this.instrumentChassis.Write_Unchecked("FUNC \"VOLT:DC\"", this);
                        break;
                    case MeasureFunc.CURR_DC:
                        this.instrumentChassis.Write_Unchecked("FUNC \"CURR:DC\"", this);
                        break;
                    default:
                        throw new Exception("The measure function has not implemented.");
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        public double ReadValue()
        {
            double value = 0;
            string strResult = this.instrumentChassis.Query_Unchecked("READ?", this);
            value = Convert.ToDouble(strResult);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        public double CalculateMIN
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("CALC:AVER:MIN?", this);
                return Convert.ToDouble(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double CalculateMAX
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("CALC:AVER:MAX?", this);
                return Convert.ToDouble(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double CalculateAVER
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("CALC:AVER:AVER?", this);
                return Convert.ToDouble(result);
            }
        }

        /// <summary>
        /// Get the resistance value
        /// Preset and make a 2-wire ohms measurement with the specified range
        /// and resolution. The reading is sent to the output buffer.
        /// </summary>
        public double Resistance
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("MEASure:RESistance?", this);
                return Convert.ToDouble(result);
            }
        }

        /// <summary>
        /// Get the Fresistance value
        /// Preset and make a 4-wire ohms measurement with the specified range
        /// and resolution. The reading is sent to the output buffer.
        /// </summary>
        public double FResistance
        {
            get
            {
                string result = this.instrumentChassis.Query_Unchecked("MEASure:FRESistance?", this);
                return Convert.ToDouble(result);
            }
        }

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_HP34401A instrumentChassis;

        public enum MeasureFunc
        {
            VOLT_DC = 1,
            VOLT_DC_RAT,
            VOLT_AC,
            CURR_DC,
            CURR_AC,
            RESISTANCE,
            FRESISTANCE,
            PERIOD,
            CONTINUITY,
            DIODE
        }

        private MeasureFunc measureFunction = MeasureFunc.VOLT_DC;

        public enum CALCulateFunc
        {
            NULL = 0,
            DB,
            DBM,
            AVER,
            LIM
        }

        #endregion
    }
}

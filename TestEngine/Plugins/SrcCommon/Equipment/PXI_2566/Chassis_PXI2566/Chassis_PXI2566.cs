// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// ChassisPXI_2566_Actuator.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.DAQmx;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for PXI-2566 Actuator card
    /// </summary>
    public class Chassis_PXI2566 : ChassisType_PXI
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_PXI2566(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {

            ////// Setup expected valid hardware variants 
            ////ChassisDataRecord chassisData = new ChassisDataRecord(
            ////    "PXI-2566",		// hardware name 
            ////    "8.3",			// minimum valid firmware version 
            ////    "8.3");         // maximum valid firmware version 
            ////ValidHardwareData.Add("ChassisData", chassisData);


            //Cypress: Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "PXI-2566",		// hardware name 
                "8.0",			// minimum valid firmware version 
                "8.3");         // maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);


            // basic check for valid driver installation
            try
            {
                // just query the DLL version (no card required!)
                long daqMzVers = DaqSystem.Local.DriverMajorVersion;
            }
            catch (Exception e)
            {
                throw new ChassisException("NI DaqMx Driver DLL Installation Missing/Corrupt", e);
            }
        }
        #endregion        

        #region Chassis overrides
        /// <summary>
        /// Card firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            {
                // Return the DLL version as we cannot query the card 
                long majorVers = DaqSystem.Local.DriverMajorVersion;
                long minorVers = DaqSystem.Local.DriverMinorVersion;
                return majorVers.ToString() + "." + minorVers.ToString();
            }
        }

        /// <summary>
        /// Card hardware identity
        /// </summary>
        public override string HardwareIdentity
	    {
            get
            {
                return relayCard.ProductType;
            }
        }
               
        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // Expecting resource string like 'PXI2::5::INSTR'
                    string[] visaRes = base.ResourceInfo.Split( new char[] {':'} );
                    long pciBusNo = Convert.ToInt16(visaRes[0].Replace("PXI", ""));
                    long pciDevNo = Convert.ToInt16(visaRes[2]);
                    string[] devices = DaqSystem.Local.Devices;
                    
                    // Look at all NI DAQ devices until one matches the bus type and PCI location
                    foreach (string devName in devices)
                    {
                        Device d = DaqSystem.Local.LoadDevice(devName);
                        string bus = d.BusType.ToString().ToUpper();
                        if (visaRes[0].Contains(bus) && d.PciBusNumber == pciBusNo && d.PciDeviceNumber == pciDevNo)
                        {
                            relayCard = d;
                            break;
                        }
                    }
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // No setup required
                }
            }
        }

        /// <summary>
        /// Is the specified relay closed?
        /// </summary>
        /// <param name="relayNbr">Relay number</param>
        /// <returns>True=Closed, False=Open</returns>      
        public bool RelayClosed(byte relayNbr)
        {
            SwitchRelayPosition relayState = DaqSystem.Local.GetSwitchRelayPosition(relayId(relayNbr));
            return relayState == SwitchRelayPosition.Open ? false : true;
        }

        /// <summary>
        /// Close Relay
        /// </summary>
        /// <param name="relayNbr">Which relay to close</param>
        public void CloseRelay(byte relayNbr)
        {
            DaqSystem.Local.CloseSwitchRelays(relayId(relayNbr), true);
        }

        /// <summary>
        /// Open Relay
        /// </summary>
        /// <param name="relayNbr">Which relay to close</param>
        public void OpenRelay(byte relayNbr)
        {
            DaqSystem.Local.OpenSwitchRelays(relayId(relayNbr), true);        
        }

        #endregion

        #region Private Functions

        private string relayId(byte relayNum)
        {
            string id = "/" + relayCard.DeviceID + "/k" + relayNum.ToString();
            return id;
        }

        #endregion

        #region Private data

        Device relayCard;

        #endregion
    }
}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace TEST.PXI_2566_Actuator
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_PXI2566 testChassis;

        private Inst_PXI2566 testInstr;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "PXI2::15::INSTR";
        const string chassisName = "Chassis";
        const string inst1Name = "Instrument";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_PXI2566(chassisName, "Chassis_PXI2566", visaResource);
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_PXI2566(inst1Name, "Inst_PXI2566", "", "", testChassis);
            TestOutput(testInstr, "Created OK");
            
            // put them online
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput(testChassis, "IsOnline set true OK");
            testInstr.IsOnline = true;
            TestOutput(testInstr, "IsOnline set true OK");
            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(testChassis.HardwareIdentity + ":" + testChassis.FirmwareVersion);
            TestOutput(testInstr.HardwareIdentity + ":" + testInstr.FirmwareVersion);
        }

        [Test]
        public void T02_RelayPosns()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            IInstType_DigitalIO testRelay_0 = testInstr.GetDigiIoLine(0);
            IInstType_DigitalIO testRelay_15 = testInstr.GetDigiIoLine(15);

            TestOutput(testRelay_0, "Relay 0 Posn = " + testRelay_0.LineState);
            testRelay_0.LineState = true;
            Assert.AreEqual(true, testRelay_0.LineState);
            testRelay_0.LineState = false;
            Assert.AreEqual(false, testRelay_0.LineState);

            TestOutput(testRelay_15, "Relay 15 Posn = " + testRelay_15.LineState);
            testRelay_15.LineState = true;
            Assert.AreEqual(true, testRelay_15.LineState);
            testRelay_15.LineState = false;
            Assert.AreEqual(false, testRelay_15.LineState);            
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// InstPXI_2566_Relay.cs
//
// Author: Tony Foster, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{   
    /// <summary>
    /// Single relay on a PXI-2566 card
    /// </summary>
    public class Inst_PXI2566_DigiIO : UnmanagedInstrument, IInstType_DigitalIO
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="pxi2566">Instrument's chassis</param>
        /// <param name="relayNbr">Relay Nbr</param>
        public Inst_PXI2566_DigiIO(Chassis_PXI2566 pxi2566, byte relayNbr)
            : base("PXI2566_D" + relayNbr, pxi2566)
        {            
            // get the relay number (slot)
            this.relayNbr = relayNbr;

            // initialise this instrument's chassis
            // don't forget to change the private variable type too.
            this.instrumentChassis = pxi2566;
        }
        #endregion
       

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_PXI2566 instrumentChassis;

        /// <summary>
        /// relay number within the card
        /// </summary>
        private byte relayNbr;
        #endregion

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.LineState = false;
        }

        /// <summary>
        /// Set relay state - false=Open, true=Closed
        /// </summary>
        public bool LineState
        {
            get
            {
                bool relayState = instrumentChassis.RelayClosed(relayNbr);
                return relayState;
            }
            set
            {
                if (value)
                {
                    instrumentChassis.CloseRelay(relayNbr);
                }
                else
                {
                    instrumentChassis.OpenRelay(relayNbr);
                }
            }
        }
    }
}

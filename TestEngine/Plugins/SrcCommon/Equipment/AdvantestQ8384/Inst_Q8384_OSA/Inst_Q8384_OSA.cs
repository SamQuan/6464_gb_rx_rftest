// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_Q8384_OSA.cs
//
// Author: Joseph Olajubu, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using System.Threading;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Advantest Q8384 OSA Instrument Driver
    /// </summary>
    public class Inst_Q8384_OSA : InstType_OSA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Q8384_OSA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord Q8384B00Data = new InstrumentDataRecord(
                "ADVANTEST Q8384",			// hardware name 
                "B00 A02",			// minimum valid firmware version 
                "B00 A02");		// maximum valid firmware version 
            ValidHardwareData.Add("Q8384B00", Q8384B00Data);

            // Setup expected valid hardware variants 
            InstrumentDataRecord Q8384B01Data = new InstrumentDataRecord(
                "ADVANTEST Q8384",			// hardware name 
                "B01 A02",			// minimum valid firmware version 
                "B01 A02");		// maximum valid firmware version 
            ValidHardwareData.Add("Q8384B01", Q8384B01Data);

            // Setup expected valid hardware variants 
            InstrumentDataRecord Q8384BX1Data = new InstrumentDataRecord(
                "ADVANTEST Q8384",			// hardware name 
                "BX1 A02",			// minimum valid firmware version 
                "BX1 A02");		// maximum valid firmware version 
            ValidHardwareData.Add("Q8384BX1", Q8384BX1Data);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Q8384",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Q8384)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = instrumentChassis.Query("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                string fwv = idn.Split(',')[3].Trim();

                // Log event 
                LogEvent("'FirmwareVersion' returned : " + fwv);

                return fwv;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        /// <remarks>
        /// Resets the instrument, sets up sweep range 1525-1565nm and starts single sweep.
        /// </remarks>
        public override void SetDefaultState()
        {
            // Send reset command to instrument
            this.Reset();
            
            // set scan area to C-Band
            this.WavelengthStart_nm = 1525;
            this.WavelengthCentre_nm = 1545;
            this.WavelengthStop_nm = 1565;

            // start single sweep
            this.SweepMode = SweepModes.Single;

            //Turn off headers for GPIB responses
            instrumentChassis.Write("HED0", this);

            //Make sure that response data is in ASCII format
            instrumentChassis.Write("FMT0", this);

            //Set the data separator to be the "," character.
            instrumentChassis.Write("SDL0", this);

            //Switch off all Cursors (markers).
            instrumentChassis.Write("CUR0", this); //switch on cursors
            instrumentChassis.Write("XAC0", this); //switch on wavelength cursor 1
            instrumentChassis.Write("XBC0", this); //switch on wavelength cursor 1
            instrumentChassis.Write("YAC0", this); //switch off level cursor 1
            instrumentChassis.Write("YBC0", this); //switch off level cursor 1
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        /// <remarks>If set to true, sets the instrument into the default state.</remarks>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region OSA InstrumentType property overrides
        /// <summary>
        /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
        /// </summary>
        /// <value>
        /// Set value has to be between 0.1 and 10 db/Div.
        /// </value>
        public override double Amplitude_dBperDiv
        {
            get
            {
                // Query the instrument 
                string response = instrumentChassis.Query("LSC?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0.1 || value > 10)
                {
                    throw new InstrumentException("Amplitude scale must be be between 0.1 and 10 dB/Div. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("LSC" + value, this);
                }
            }
        }



        /// <summary>
        /// This property is not available on this instrument.
        /// </summary>
        public override bool AutoRange
        {
            get
            {
                throw new InstrumentException("'AutoRange' is not available.");
            }
            set
            {
                throw new InstrumentException("'AutoRange' is not available.");
            }
        }

        /// <summary>
        /// Returns the display trace, as an array of numeric values. 
        /// </summary>
        public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
        {
            get
            {
                int numPoints = this.TracePoints;
                // read amplitude values from OSA
                double[] response = instrumentChassis.QueryDoubleArray("OSD0", this, numPoints);
                
                // convert double array response into trace array                
                InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
                double waveLength = this.WavelengthStart_nm;
                double waveLengthStep = this.WavelengthSpan_nm / (numPoints - 1);

                for (int index = 0; index < numPoints; index++)
                {
                    trace[index].power_dB = response[index];
                    trace[index].wavelength_nm = waveLength;
                    waveLength = Math.Round(waveLength + waveLengthStep, 2);
                }

                return trace;
            }
        }

        /// <summary>
        /// Reads the present marker amplitude for the OSA channel.
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerAmplitude_dBm
        {
            get
            {
                // Query the instrument. Amplitude data is in the second item of comma separated data.                 
                string amplitude = instrumentChassis.Query("OCD?", this).Split(',')[1].Trim();

                // Convert response to double and return
                return Convert.ToDouble(amplitude);
            }
        }

        /// <summary>
        /// Shows/hides the marker for the OSA channel. 
        /// </summary>
        /// <remarks>
        /// Both Cursors are required to be shown or hidden on this instrument.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        public override bool MarkerOn
        {
            get
            {
                string cursorMode = instrumentChassis.Query("XAC?", this).Trim();
                if (cursorMode == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (value)
                {
                    // Turn on trace cursors. 
                    instrumentChassis.Write("CUR1", this);
                    instrumentChassis.Write("XAC1", this);
                    instrumentChassis.Write("XBC1", this);
                }
                else
                {
                    // Switch off all cursors.
                    instrumentChassis.Write("CUR0", this);
                    instrumentChassis.Write("XAC0", this);
                    instrumentChassis.Write("XBC0", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the marker wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between the current start and stop wavelengths.
        /// </value>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerWavelength_nm
        {
            get
            {
                // Query the instrument   
                string wavelength = instrumentChassis.Query("XAS?", this);

                // Convert response to double and then from meters to nm and return
                return Convert.ToDouble(wavelength) * 1E9;
            }
            set
            {
                // Check whether value is in correct range
                if (value < this.wavelengthStart || value > this.wavelengthStop)
                {
                    throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                         " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
                }
                else
                {
                    // Write the value to the instrument. Set both cursors to the same point.
                    instrumentChassis.Write("XAS" + value + "nm", this);
                    instrumentChassis.Write("XBS" + value + "nm", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the point averaging for the OSA channel. 
        /// </summary>
        /// <value>
        /// The set value has to be between 1 (OFF) and 64.
        /// </value>
        public override int PointAverage
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("AVG?", this);

                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 1 || value > 64)
                {
                    throw new InstrumentException("Point Average must be between 1 and 64. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("AVG" + value, this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the reference level for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -87 and +23 dBm.
        /// </value>
        public override double ReferenceLevel_dBm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("REF?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -87 || value > 23)
                {
                    throw new InstrumentException("Reference level must be between -87 and +23 dBm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("REF" + value + "dBm", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the resolution bandwidth for the OSA channel.  
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 0.01, 0.02, 0.05, 0.1, 0.2, and 0.5nm.
        /// </value>
        public override double ResolutionBandwidth
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("RES?", this);
                // Convert response to double, and nm form meters and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {
                double res = 0;
                // Check whether value is in correct range
                if (value <= 0.01) res = 0.01;
                else if (value <= 0.02) res = 0.02;
                else if (value <= 0.05) res = 0.05;
                else if (value <= 0.1) res = 0.1;
                else if (value <= 0.2) res = 0.2;
                else res = 0.5;
                // Write the value to the instrument
                instrumentChassis.Write("RES" + res + "nm", this);
            }
        }

        /// <summary>
        /// Returns the executional status for the OSA channel. 
        /// </summary>
        /// <value>
        /// Busy is returned when the OSA is in the process of measureing a spectrum.
        /// Idle is returned when no spectrum is measured. Unspecified is returned when OSA is in power
        /// monitor mode. DataReady is not supported.
        /// </value>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.ChannelState Status
        {
            get
            {               
                // Convert response and return
                InstType_OSA.ChannelState status = ChannelState.Unspecified;

                // Query the instrument   
                byte flags = (byte)instrumentChassis.QueryStatusByte();

                //Check to see if  the measure end bit is 0. If it is, a measurement is in progress, 
                //If the measurement mode on the instrument is not STOP (0).
                int response = (int)flags & 0x01; 

                if ((response == 0) && (measurementModeActive == true)) 
                {
                    //Bit 0 will be clear if the instrument is carrying out a measurement or 
                    //it has been set into a non-measurement mode (MEAS0).
                    status = ChannelState.Busy;
                }
                else
                {
                    status = ChannelState.Idle;
                }

                return status;
            }
        }

        /// <summary>
        /// Reads/sets the sweep capture mode for the OSA channel.
        /// </summary>
        /// <value>
        /// Triggered mode is not supported int his version of the driver.
        /// Unspecified mode cannot be set.
        /// </value>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.SweepModes SweepMode
        {
            get
            {
                return this.sweepMode;
            }
            set
            {
                // Select command
                string command = "";
                switch (value)
                {
                    case SweepModes.Single: 
                        command = "MEA1";  // Switch to single sweep mode.
                        break;

                    case SweepModes.Continuous: 
                        command = "MEA2";  // Start continuous sweep
                        break;

                    case SweepModes.Unspecified: 
                        throw new InstrumentException("Cannot set to 'Unspecified' mode.");
                    
                    case SweepModes.Triggered: 
                        throw new InstrumentException("'Triggered' mode not supported in this version of the driver.");
                }

                this.sweepMode = value;
                measurementModeActive = true;

                // Write the command to the instrument
                instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Reads/sets the number of trace points for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 101, 201, 501, 1001, 2001, 5001, and 10001.
        /// </value>
        public override int TracePoints
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SPT?", this);

                //Decode the response and return.
                switch (Convert.ToInt32(response))
                {
                    case 0:
                        return 101;

                    case 1:
                        return 201;

                    case 2:
                        return 501;


                    case 3:
                        return 1001;

                    case 4:
                        return 2001;

                    case 5:
                        return 5001;


                    case 6:
                        return 10001;

                    default:
                        throw new InstrumentException("Invalid Tracepoints value returned by instrument: " + response);
                }
            }
            set
            {
                int tracePoints = 0;

                // Check whether value is in correct range
                if (value <= 101) tracePoints = 0; //Required value for 101
                else if (value <= 201) tracePoints = 1;//Required value for 201
                else if (value <= 501) tracePoints = 2;//Required value for 501
                else if (value <= 1001) tracePoints = 3;//Required value for 1001
                else if (value <= 2001) tracePoints = 4;//Required value for 2001
                else if (value <= 5001) tracePoints = 5;//Required value for 5001
                else tracePoints = 6;//Required value for 10001

                // Write the value to the instrument
                instrumentChassis.Write("SPT" + tracePoints, this);
            }
        }

        /// <summary>
        /// Reads/sets the video averaging value for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int VideoAverage
        {
            get
            {
                throw new InstrumentException("Video Average Not supported on this instrument"); 
            }
            set
            {
                throw new InstrumentException("Video Average Not supported on this instrument");   
            }
        }

        /// <summary>
        /// Reads/sets the video bandwidth for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.
        /// </value>
        public override double VideoBandwidth
        {
            get
            {
                throw new InstrumentException("Video Bandwidth Not supported on this instrument");
            }
            set
            {
                throw new InstrumentException("Video Bandwidth Not supported on this instrument");
            }
        }

        /// <summary>
        /// Reads/sets the centre wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthCentre_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("CEN?", this);
                // Convert response to double and then from meters to nm and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1750)
                {
                    throw new InstrumentException("Wavelength Centre must be between 600 and 1750 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("CEN" + value + "nm", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength offset for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -1 and +1nm.
        /// </value>
        public override double WavelengthOffset_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("CLF?", this);
                // Convert response to double and then from meters to nm and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {
                // Check whether value is in correct range
                if (value < -1 || value > 1)
                {
                    throw new InstrumentException("Wavelength Offset must be between -1.0 and +1.0 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("CLF" + value + "nm", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength span for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 0.2 and 1100nm.
        /// </value>
        public override double WavelengthSpan_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SPA?", this);
                // Convert response to double and then from meters to nm and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0.2 || value > 1100)
                {
                    throw new InstrumentException("Wavelength Span must be between 0.2 and 1100 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("SPA" + value + "NM", this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1650nm.
        /// </value>
        public override double WavelengthStart_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("STA?", this);

                // Convert response to double, and then from um to nm and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {

                // Check whether value is in correct range
                if (value < 600 || value > 1650)
                {
                    throw new InstrumentException("Wavelength Start must be between 600 and 1650 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("STA" + value + "nm", this);
                    //Store value locally for quicker access.
                    this.wavelengthStart = value;
                }
            }
        }

        /// <summary>
        /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1700nm.
        /// </value>
        public override double WavelengthStop_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("STO?", this);
                // Convert response to double and then from meters to nm and return
                return Convert.ToDouble(response) * 1E9;
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1700)
                {
                    throw new InstrumentException("Wavelength Stop must be between 600 and 1700 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("STO" + value + "nm", this);
                    
                    //Store value locally for quicker access.
                    this.wavelengthStop = value;
                }
            }
        }

        /// <summary>
        /// Attenuator function
        /// </summary>
        public override bool Attenuator
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }

        #endregion

        #region OSA InstrumentType function overrides

        /// <summary>
        /// Moves the marker to the minimum signal point on the display. 
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToMinimum()
        {
            // Retrieve the trace
            InstType_OSA.OptPowerPoint[] trace = this.GetDisplayTrace;
            // find min in trace
            InstType_OSA.OptPowerPoint min = trace[0];
            for (int index = 1; index < trace.Length; index++)
            {
                if (trace[index].power_dB < min.power_dB)
                    min = trace[index];
            }
            // set marker to minimum
            this.MarkerWavelength_nm = min.wavelength_nm;
        }

        /// <summary>
        /// Moves the marker to the next peak to the left/right on the display.
        /// </summary>
        /// <param name="directionToMove">
        /// Enumeration representing the direction in which to search for the next peak.
        /// If unspecified is selected, the next peak is searched.
        /// </param>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
        {
            string commandString = "";

            switch (directionToMove)
            {
                case Direction.Left: commandString = "LPS";
                    break;
                case Direction.Right: commandString = "RPS";
                    break;
                case Direction.Unspecified: commandString = "RPS";
                    break;
            }

            // Move the marker
            instrumentChassis.Write_Unchecked(commandString, this);

            //Unfortunately, the instrument moves Cursor 2, even though the manual 
            //says it's supposed to mover Cursor 1! Now set Cursor 1.
            string cursor2Position = instrumentChassis.Query("XBS?", this);
            double position = Math.Round (Convert.ToDouble(cursor2Position) * 1E9, 2);

            instrumentChassis.Write("XAS" + position.ToString() + "nm", this);
        }

        /// <summary>
        /// Moves the marker to the maximum signal point on the display.
        /// </summary>
        /// <remarks>
        /// Throws anexception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToPeak()
        {
            // Get the peak from the instrument
            string peak = instrumentChassis.Query("OPK?", this).Split(',')[0].Trim();
            double peakDouble = Math.Round(Convert.ToDouble(peak) * 1E9, 2);

            //Enable the Marker
            instrumentChassis.Write("XAC1", this);

            //Move the marker to the peak
            instrumentChassis.Write("XAS" + peakDouble.ToString() + "nm", this);

            //Also move the second (hidden) marker to the peak - this is nesseray for Left peak 
            //and right peak search functions to work, which depsite what the manual says seem to 
            //work on lambda cursor 2 rather than lambda cursor 1....
            instrumentChassis.Write("XBS" + peakDouble.ToString() + "nm", this);
        }

        /// <summary>
        /// Starts a measurement sweep, and returns immediately.
        /// </summary>
        /// <remarks>
        /// Status() property should be used to determine sweep progress/completion. 
        /// </remarks>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStop"/>
        public override void SweepStart()
        {
            // Start the sweep
            if (this.sweepMode == SweepModes.Single)
            {
                //Do a single sweep.
                instrumentChassis.Write("MEA1", this);
            }
            else
            {
                //Start a continuous sweep.
                instrumentChassis.Write("MEA2", this);
            }

            measurementModeActive = true;
        }

        /// <summary>
        /// Stops an active measurement sweep. 
        /// </summary>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        public override void SweepStop()
        {
            // Stop the sweep
            instrumentChassis.Write("MEA0", this);
            measurementModeActive = false;
        }

        /// <summary>
        /// Autoalign OSA
        /// </summary>
        public override void AutoAlign()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }
        #endregion

        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        private void Reset()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("C", this);

            //Wait a couple of seconds.
            Thread.Sleep(2000);
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Q8384 instrumentChassis;
        
        /// <summary>
        /// The instrument's sweep mode.
        /// </summary>
        private SweepModes sweepMode;

        private double wavelengthStart;
        private double wavelengthStop;

        private bool measurementModeActive;
        #endregion

        
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using System.Threading;
using System.IO;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>Multi Purpose Control Board Instrument driver</summary>
    public partial class Instr_SPI : Instrument
    {
                
        //Calibration Data will have the following format

        //DAC/ADC address (based on global enum) = 2 bytes      
        //Calibration Type = 1 byte , taking value 0x00 , 0x01      
        //Calibration Slope = 4 bytes float
        //Calibration Offset =  4 bytes float
        //Calibration is indexed using the 16bit ADC/DAC address
        private static Dictionary<DIO_Channels, CalibrationItem> CalibrationTable;

        public void ReadCalDataFromEEPROM(string DefaultSettingsFileNameAndPath)
        {
            ReadCalDataFromFile(DefaultSettingsFileNameAndPath);            
            //Read memory block containing electrical cal info            
          
        }

        public void ReadCalDataFromFile(string FileNameAndPath)
        {
           
        
            if (!File.Exists(FileNameAndPath))
                throw new InstrumentException("ReadCalDataFromFile: File does not exist:" + FileNameAndPath);

            CalibrationTable.Clear();
            
            //ReadFile
            using (StreamReader SR = new StreamReader( FileNameAndPath))
            {
                //Consume Header
                SR.ReadLine();

                //Assuming fixed column order! 
                //We Could Index the header names but they are just as likely to change!

                while (!SR.EndOfStream)
                {

                    string[] Line = SR.ReadLine().Split(',');

                    //Check is we can parse to our DIO enum
                    if (Enum.IsDefined(typeof(DIO_Channels), Line[0]))
                    {
                        DIO_Channels CalChannel = (DIO_Channels)Enum.Parse(typeof(DIO_Channels), Line[0]);
                        bool isSigned = (Line[2].ToUpper() == "TRUE") ? true : false;
                        bool isLinear = (Line[3].ToUpper() == "TRUE") ? true : false;
                        UInt16 MinCounts = UInt16.Parse(Line[4].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        UInt16 MaxCounts = UInt16.Parse(Line[5].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        double MinReal = double.Parse(Line[6]);
                        double MaxReal = double.Parse(Line[7]);
                        double CalGradient = double.Parse(Line[8]);
                        double CalOffset = double.Parse(Line[9]);

                        CalibrationItem CalItem = new CalibrationItem((isLinear) ? CalibrationType.LIN : CalibrationType.LOG,isSigned, MinCounts, MaxCounts, MinReal, MaxReal, CalGradient, CalOffset);
                        if(!CalibrationTable.ContainsKey(CalChannel)) CalibrationTable.Add(CalChannel, CalItem);
                    }

                }

            }



        }


        public void WriteCalDataToEEPROM()
        {
            

        }

        public void SetCalibrationItem(DIO_Channels DIOChannel, CalibrationItem CalibrationData)
        {

            if (CalibrationTable.ContainsKey(DIOChannel))
            {
                CalibrationTable[DIOChannel] = CalibrationData;
               
            }
            else
            {

                CalibrationTable.Add(DIOChannel, CalibrationData);

            }

        }

        public CalibrationItem GetCalibrationItem(DIO_Channels DIOChannel)
        {
            //Default Calibration retruns a dac/adc value as a double/
            CalibrationItem CalItem = new CalibrationItem(CalibrationType.LIN,false,0, 0xFFFF, -999, 999, 1, 0);

            if (CalibrationTable.ContainsKey(DIOChannel))
            {
                CalItem = CalibrationTable[DIOChannel];

            }
            else
                throw new InstrumentException("GetCalibrationItem:Channel " + DIOChannel.ToString() + " Not defined in calibration table");

            return CalItem;
        }  
        

        

        public enum CalibrationType
        {
            LIN,
            LOG

        }

        public class CalibrationItem
        {

            public CalibrationItem(CalibrationType CalType, bool IsSigned, UInt16 MinCounts, UInt16 MaxCounts, double MinReal, double MaxReal, double CalSlope, double CalOffset)
            {                             
                mCalType = CalType;
                mIsSigned = IsSigned;
                mCalSlope = CalSlope;
                mCalOffset = CalOffset;
                mMinReal = MinReal;
                mMaxReal = MaxReal;
                mMinCounts = MinCounts;
                mMaxCounts = MaxCounts;


            }        

            public CalibrationType CalType
            {
                get { return this.mCalType; }
            }


            public bool IsSigned
            {
                get { return this.mIsSigned; }
            }


            public double CalSlope
            {
                get { return this.mCalSlope; }
            }

            public double CalOffset
            {
                get { return this.mCalOffset; }
            }

            public double MaxReal
            {
                get { return this.mMaxReal; }
            }

            public double MinReal
            {
                get { return this.mMinReal; }
            }


            public UInt16 MaxCounts
            {
                get { return this.mMaxCounts; }
            }

            public UInt16 MinCounts
            {
                get { return this.mMinCounts; }
            }


            private readonly CalibrationType mCalType;
            private readonly bool mIsSigned;
            private readonly double mCalSlope;
            private readonly double mCalOffset;

            private readonly double mMaxReal;
            private readonly double mMinReal;
            private readonly UInt16 mMaxCounts;
            private readonly UInt16 mMinCounts;




            public double ConvertCountsToReal(UInt16 value)
            {
                double RealVal = -999.0;

                              
                if (this.CalType == CalibrationType.LIN)
                {

                    if ((this.IsSigned) && ((value & 0x8000) == 0x8000))
                    {                     
                          RealVal = (this.CalSlope * (value - 0x10000)) + this.CalOffset;                       
                    }
                    else                    
                        RealVal = (this.CalSlope * value) + this.CalOffset;

                }
                else if (this.CalType == CalibrationType.LOG)
                {

                    if ((this.IsSigned) && ((value & 0x8000) == 0x8000))
                    {
                        RealVal = Math.Pow(10, (this.CalSlope * (value - 0x10000)) + this.CalOffset);

                    }
                    else
                        RealVal = Math.Pow(10, (this.CalSlope * value) + this.CalOffset);

                }


                return RealVal;
            }

            public UInt16 ConvertRealToCounts(double value)
            {

                UInt16 CountVal = 0;

                if (this.CalType == CalibrationType.LIN)
                {
                    
                    if ((this.IsSigned) && (value < 0.0))
                    {
                        UInt16 temp = Convert.ToUInt16(Math.Abs(((value - this.CalOffset) / this.CalSlope)));
                        CountVal = Convert.ToUInt16(0x10000 - temp);            
                    }
                    else                                     
                        CountVal = Convert.ToUInt16(Math.Max(0.0,(value - this.CalOffset) / this.CalSlope));

                }
                else if (this.CalType == CalibrationType.LOG)
                {
                    if ((this.IsSigned) && (value < 0.0))
                    {
                        UInt16 temp = Convert.ToUInt16(Math.Abs((Math.Log10(value) - this.CalOffset) / this.CalSlope));
                        CountVal = Convert.ToUInt16(0x10000 - temp);
                    }
                    else
                        CountVal = Convert.ToUInt16(Math.Max(0.0, (Math.Log10(value) - this.CalOffset) / this.CalSlope));

                }


                return CountVal;

            }



        }


    }
}
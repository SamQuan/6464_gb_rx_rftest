﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.TestLibrary.InstrTypes
{
    public abstract class Inst_SPI : Instrument
    {
 
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="instrumentName">Instrument name</param>
            /// <param name="driverName">Instrument driver name</param>
            /// <param name="slotId">Slot ID for the instrument</param>
            /// <param name="subSlotId">Sub Slot ID for the instrument</param>
            /// <param name="chassis">Chassis through which the instrument communicates</param>
            public Inst_SPI(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
                : base(instrumentName, driverName, slotId, subSlotId, chassis)
            {
            }

            /// <summary>
            /// To re-starts and re-initializes the unit
            /// </summary>
            public abstract void Reset();

            /// <summary>
            /// To query the current status on the unit
            /// </summary>
            public abstract OperatingStatuses Status
            {
                get;
            }

            public void WriteDIO_SPI(double Addr, double value)
            {
                double total = 0;
                total = Addr + value;
            }
   
            /// <summary>
            /// Operating status list
            /// </summary>
            public enum OperatingStatuses
            {
                Idle,
                Busy,
                Home,
                End
            }
        }
   
}

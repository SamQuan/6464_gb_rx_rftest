// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_MPCB.cs
//
// Author: rob.taylor-collins, 2012
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics; // StackTrace

using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>Common platform testboard.</summary>
    public class Chassis_DE0 : ChassisType_Serial
    {
        #region Constructor 

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_DE0(string chassisNameInit, string driverNameInit, string resourceString)
            : base(chassisNameInit, driverNameInit, GetComPortId(resourceString))
        {

           
            
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "Oclaro DDS",			    // hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 


            ValidHardwareData.Add("ChassisData", chassisData);


            
            // COM Defaults
            this.comPort = "COM1";
            this.baudRate = 9600;
            this.dataBits = 8;
            this.stopBits = 1;
            this.handShaking = Handshake.None;
            this.parity = Parity.None;
            this.inputBufferSize_bytes = 512;
            this.outputBufferSize_bytes = 512;
            this.newLine = "\r";
            this.timeOut_ms = 1000;

            // Extract COM Settings - Only configure when we go on-line
            string[] resByComma = resourceString.Split(',');

            foreach (string resEntry in resByComma)
            {
                string[] resFields = resEntry.Split('=');

                switch (resFields[0].ToUpper().Trim())
                {
                    case "COMPORT":
                        this.comPort = resFields[1];
                        break;

                    case "BAUDRATE":
                        this.baudRate = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    case "DATABITS":
                        this.dataBits = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    case "STOPBITS":
                        this.stopBits = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    case "HANDSHAKE":
                        this.handShaking = (Handshake)Enum.Parse(typeof(Handshake), resFields[1], true);
                        break;

                    case "PARITY":
                        this.parity = (Parity)Enum.Parse(typeof(Parity), resFields[1], true);
                        break;

                    case "INBUFFER":
                        this.inputBufferSize_bytes = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    case "OUTBUFFER":
                        this.outputBufferSize_bytes = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    case "NEWLINE":
                        this.newLine = resFields[1];
                        break;

                    case "TIMEOUT":
                        this.timeOut_ms = int.Parse(resFields[1], System.Globalization.CultureInfo.InvariantCulture);
                        break;

                    default:
                        throw new ChassisException("Bad resource string: " + resEntry);

                }
            }

            //Disable writing the the logger.
            base.EnableLogging = false;
        }
        
        #endregion

        #region Chassis overrides
        
        /// <summary>Firmware version of this chassis.</summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>Hardware Identity of this chassis.</summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Oclaro DDS";
            }
        }

        /// <summary>Setup the chassis as it goes online</summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // Setup RS232 variables
                    this.BaudRate = baudRate;
                    this.DataBits = dataBits;
                    this.Handshaking = handShaking;
                    this.Parity = parity;
                    this.InputBufferSize_bytes = inputBufferSize_bytes;
                    this.OutputBufferSize_bytes = outputBufferSize_bytes;
                    this.NewLine = newLine;
                    this.Timeout_ms = timeOut_ms;

                    // Setup RS232 variables
                    ConfigureComPort();
                    //Disable writing the the logger.                
                    this.EnableLogging = false;
                }

                // setup base class
                base.IsOnline = value;
            }
        }
        
        #endregion

        #region private Methods / Properties

 
        /// <summary>
        /// Writes data to the serial port.
        /// </summary>
        /// <param name="data">bytes to write</param>
        /// <param name="instrument">equipment reference</param>
        private new void Write(byte[] data, Instrument instrument)
        {
            base.Write(data, instrument);
            AllowModuleToProcessCommand();
        }


        /// <summary>
        /// Read all the data current held in the serial port buffer
        /// </summary>
        /// <param name="instrument">equipment reference</param>
        /// <returns>comms buffer data bytes</returns>
        private new byte[] ReadByteArray(Instrument instrument)
        {
            byte[] retVal = base.ReadByteArray(instrument);

            return retVal;
        }

  

        /// <summary>How long to allow the MPCB to process the user command</summary>
        public Int32 ReadWriteDelay_ms
        {
            get { return readWriteDelay_ms; }

            set { readWriteDelay_ms = value; }
        }

        /// <summary>Reinitialise the com port</summary>
        public void ConfigureComPort()
        {
            this.Configure(this.BaudRate, this.DataBits, this.StopBits, this.Parity, this.Handshaking, this.InputBufferSize_bytes, this.OutputBufferSize_bytes, this.NewLine);
        }

        public void ToggleComPortState()
        {
            base.IsOnline = !this.IsOnline;
            Thread.Sleep(2);
            base.IsOnline = !this.IsOnline;
        }

               

        public byte[] SendCommandToDE0(byte Id_Byte, byte[] AddressAndData)
        {
                      

            byte[] RxData = new byte[]{0};

            try
            {

                if (System.Threading.Monitor.TryEnter(ThreadLock, 20000))
                {

                    int RetryCount = 0;
                    while (RetryCount <= 3)
                    {

                        try
                        {

                            this.TransmitDE0Packet(Id_Byte, AddressAndData);

                            System.Threading.Thread.Sleep(ReadWriteDelay_ms);

                            byte RxIDByte = 0;
                            int RxPacketLength = 0;

                            this.RecieveDE0Packet(this.timeOut_ms, out RxIDByte, out RxPacketLength, out RxData);

                        }
                        catch (Exception Ex)
                        {
                            ToggleComPortState();
                            RetryCount++;
                            System.Threading.Thread.Sleep(ReadWriteDelay_ms * RetryCount);

                            if (RetryCount > 3)
                            {
                                System.Threading.Monitor.Exit(ThreadLock);
                                throw new ChassisException(Ex.Message);
                            }
                            else
                                continue;

                        }
                   

                        break;
                    }


                    System.Threading.Monitor.Exit(ThreadLock);

                }
                else
                    throw new ChassisException("Unable to Lock Thread");


            }
            catch (Exception ex)
            {
                string TxPayload = "";
                foreach(byte BVal in AddressAndData) TxPayload = TxPayload + BVal.ToString("X2") + " ";
                throw new ChassisException("SendCommandToDE0:" + ex.Message + " Payload:" + TxPayload);

            }          
            
            return RxData;

        }
        



        private void TransmitDE0Packet(byte Id_Byte, byte[] AddressAndData)
        {

                //Calc Packet length
                UInt16 Packet_length =Convert.ToUInt16(AddressAndData.Length);
                byte[] PckLengthBytes = BitConverter.GetBytes(Packet_length);
                Array.Reverse(PckLengthBytes);

                // Create the Full packet
                List<byte> PckToSendBytes = new List<byte>();
                PckToSendBytes.Add(Id_Byte);
                PckToSendBytes.Add(PckLengthBytes[0]);
                PckToSendBytes.Add(PckLengthBytes[1]);
                PckToSendBytes.AddRange(AddressAndData);

                this.Write(PckToSendBytes.ToArray(), null);

                             
        }



                 


        private void RecieveDE0Packet(int RxTimeOut_ms, out byte Id_Byte, out int PacketLength, out byte[] Data)
        {
            //Const Values
            int headerIdx = 0;
            int lenIdx = 1;
            int dataIdx = 3;
            int headerErrBitMask = 0x80;
   
            //Rx Success Flags
            bool Pck_Length_Detected = false;
            bool All_Pck_Rxed = false;
            
            //Rx Bytes
            List<byte> Rx_Bytes = new List<byte>();

            //Init return values
            Id_Byte = 0;
            PacketLength = 0;
            Data = new byte[]{0};       

            //Timeout timer
            System.Diagnostics.Stopwatch TimeoutTimer = new Stopwatch();
            TimeoutTimer.Reset();
            TimeoutTimer.Start();

            int i = 0;
            while ((All_Pck_Rxed != true))
            {                
               
                //read the incoming data                 
                byte[] buffer = this.ReadByteArray(null);
                                     
                ////Format is Header[1B] + Length[2B] + data[Length]
                for (i = 0; i < buffer.Length; i++)
                {
                    Rx_Bytes.Add(buffer[i]);
                }

                if ((Rx_Bytes.Count > dataIdx) && (Pck_Length_Detected == false))
                {
                    Id_Byte = Rx_Bytes.ToArray()[headerIdx];
                    PacketLength = (UInt16)(Rx_Bytes.ToArray()[lenIdx] << 8);
                    PacketLength += (UInt16)(Rx_Bytes.ToArray()[lenIdx + 1]);
                    Pck_Length_Detected = true;

                }

                if ((Rx_Bytes.Count == PacketLength + 3) && (Pck_Length_Detected == true))
                {
                    All_Pck_Rxed = true;
                    Data = Rx_Bytes.GetRange(dataIdx, PacketLength).ToArray();

                }


                if (TimeoutTimer.ElapsedMilliseconds > RxTimeOut_ms)
                {
                    throw new ChassisException("RecieveDE0Packet:TIMEOUT");
                }


                // Check if error bit is set and exit
                if ((Id_Byte & headerErrBitMask) == headerErrBitMask)
                {
                    throw new ChassisException("RecieveDE0Packet:HEADER ERROR");              
                                                                   
                }

              

        }



}
               
          



        #endregion

        #region Debug messaging

        #endregion 

        #region Private Data 

        /// <summary>Delay to allow the MPCB to complete the current command processing before processing the next user request</summary>
        private int readWriteDelay_ms = 10;
        private static object ThreadLock = new object();

        private readonly string comPort;
        private readonly Int32 baudRate;
        private readonly Int32 dataBits;
        private readonly Int32 stopBits;
        private readonly Handshake handShaking;
        private readonly Parity parity;
        private readonly Int32 inputBufferSize_bytes;
        private readonly Int32 outputBufferSize_bytes;
        private readonly string newLine;
        private readonly Int32 timeOut_ms = 5000;

    

        #endregion

        #region Private Methods 

        /// <summary>ChassisType_Serial constructor needs a comport id</summary>
        public static string GetComPortId(string resrcString)
        {
            string[] resByComma = resrcString.Split(',');

            foreach (string resEntry in resByComma)
            {
                if (resEntry.ToUpper().Trim().Contains("COM"))
                {
                    string[] resFields = resEntry.Split('=');

                    return resFields[1];
                }
            }

            return resrcString;
        }

        /// <summary>A short delay</summary>
        private void AllowModuleToProcessCommand()
        {
            System.Threading.Thread.Sleep(this.readWriteDelay_ms);
        }

        #endregion
    }
}

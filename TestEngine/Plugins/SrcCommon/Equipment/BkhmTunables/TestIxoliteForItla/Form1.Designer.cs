namespace IxoliteGetNopTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.setButton = new System.Windows.Forms.Button();
            this.d7CheckBox = new System.Windows.Forms.CheckBox();
            this.d6CheckBox = new System.Windows.Forms.CheckBox();
            this.d5CheckBox = new System.Windows.Forms.CheckBox();
            this.d4CheckBox = new System.Windows.Forms.CheckBox();
            this.d3CheckBox = new System.Windows.Forms.CheckBox();
            this.d2CheckBox = new System.Windows.Forms.CheckBox();
            this.d1CheckBox = new System.Windows.Forms.CheckBox();
            this.d0CheckBox = new System.Windows.Forms.CheckBox();
            this.parStatusLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.noptextBox = new System.Windows.Forms.TextBox();
            this.singleStatusReadButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.setButton);
            this.panel1.Controls.Add(this.d7CheckBox);
            this.panel1.Controls.Add(this.d6CheckBox);
            this.panel1.Controls.Add(this.d5CheckBox);
            this.panel1.Controls.Add(this.d4CheckBox);
            this.panel1.Controls.Add(this.d3CheckBox);
            this.panel1.Controls.Add(this.d2CheckBox);
            this.panel1.Controls.Add(this.d1CheckBox);
            this.panel1.Controls.Add(this.d0CheckBox);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(55, 220);
            this.panel1.TabIndex = 1;
            // 
            // setButton
            // 
            this.setButton.Location = new System.Drawing.Point(4, 188);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(40, 23);
            this.setButton.TabIndex = 8;
            this.setButton.Text = "Set";
            this.setButton.UseVisualStyleBackColor = true;
            this.setButton.Click += new System.EventHandler(this.setButton_Click);
            // 
            // d7CheckBox
            // 
            this.d7CheckBox.AutoSize = true;
            this.d7CheckBox.Checked = true;
            this.d7CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d7CheckBox.Location = new System.Drawing.Point(4, 164);
            this.d7CheckBox.Name = "d7CheckBox";
            this.d7CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d7CheckBox.TabIndex = 7;
            this.d7CheckBox.Text = "D7";
            this.d7CheckBox.ThreeState = true;
            this.d7CheckBox.UseVisualStyleBackColor = true;
            // 
            // d6CheckBox
            // 
            this.d6CheckBox.AutoSize = true;
            this.d6CheckBox.Checked = true;
            this.d6CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d6CheckBox.Location = new System.Drawing.Point(4, 141);
            this.d6CheckBox.Name = "d6CheckBox";
            this.d6CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d6CheckBox.TabIndex = 6;
            this.d6CheckBox.Text = "D6";
            this.d6CheckBox.ThreeState = true;
            this.d6CheckBox.UseVisualStyleBackColor = true;
            // 
            // d5CheckBox
            // 
            this.d5CheckBox.AutoSize = true;
            this.d5CheckBox.Checked = true;
            this.d5CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d5CheckBox.Location = new System.Drawing.Point(4, 118);
            this.d5CheckBox.Name = "d5CheckBox";
            this.d5CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d5CheckBox.TabIndex = 5;
            this.d5CheckBox.Text = "D5";
            this.d5CheckBox.ThreeState = true;
            this.d5CheckBox.UseVisualStyleBackColor = true;
            // 
            // d4CheckBox
            // 
            this.d4CheckBox.AutoSize = true;
            this.d4CheckBox.Checked = true;
            this.d4CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d4CheckBox.Location = new System.Drawing.Point(4, 95);
            this.d4CheckBox.Name = "d4CheckBox";
            this.d4CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d4CheckBox.TabIndex = 4;
            this.d4CheckBox.Text = "D4";
            this.d4CheckBox.ThreeState = true;
            this.d4CheckBox.UseVisualStyleBackColor = true;
            // 
            // d3CheckBox
            // 
            this.d3CheckBox.AutoSize = true;
            this.d3CheckBox.Checked = true;
            this.d3CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d3CheckBox.Location = new System.Drawing.Point(4, 72);
            this.d3CheckBox.Name = "d3CheckBox";
            this.d3CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d3CheckBox.TabIndex = 3;
            this.d3CheckBox.Text = "D3";
            this.d3CheckBox.ThreeState = true;
            this.d3CheckBox.UseVisualStyleBackColor = true;
            // 
            // d2CheckBox
            // 
            this.d2CheckBox.AutoSize = true;
            this.d2CheckBox.Checked = true;
            this.d2CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d2CheckBox.Location = new System.Drawing.Point(4, 49);
            this.d2CheckBox.Name = "d2CheckBox";
            this.d2CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d2CheckBox.TabIndex = 2;
            this.d2CheckBox.Text = "D2";
            this.d2CheckBox.ThreeState = true;
            this.d2CheckBox.UseVisualStyleBackColor = true;
            // 
            // d1CheckBox
            // 
            this.d1CheckBox.AutoSize = true;
            this.d1CheckBox.Checked = true;
            this.d1CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d1CheckBox.Location = new System.Drawing.Point(4, 26);
            this.d1CheckBox.Name = "d1CheckBox";
            this.d1CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d1CheckBox.TabIndex = 1;
            this.d1CheckBox.Text = "D1";
            this.d1CheckBox.ThreeState = true;
            this.d1CheckBox.UseVisualStyleBackColor = true;
            // 
            // d0CheckBox
            // 
            this.d0CheckBox.AutoSize = true;
            this.d0CheckBox.Checked = true;
            this.d0CheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.d0CheckBox.Location = new System.Drawing.Point(4, 3);
            this.d0CheckBox.Name = "d0CheckBox";
            this.d0CheckBox.Size = new System.Drawing.Size(40, 17);
            this.d0CheckBox.TabIndex = 0;
            this.d0CheckBox.Text = "D0";
            this.d0CheckBox.ThreeState = true;
            this.d0CheckBox.UseVisualStyleBackColor = true;
            // 
            // parStatusLabel
            // 
            this.parStatusLabel.AutoSize = true;
            this.parStatusLabel.Font = new System.Drawing.Font("Courier New", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.parStatusLabel.Location = new System.Drawing.Point(77, 12);
            this.parStatusLabel.Name = "parStatusLabel";
            this.parStatusLabel.Size = new System.Drawing.Size(602, 73);
            this.parStatusLabel.TabIndex = 2;
            this.parStatusLabel.Text = "0 0 0 0 0 0 0 0";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(760, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Start Status Read";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(90, 102);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(664, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // noptextBox
            // 
            this.noptextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.noptextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noptextBox.Location = new System.Drawing.Point(90, 138);
            this.noptextBox.Multiline = true;
            this.noptextBox.Name = "noptextBox";
            this.noptextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.noptextBox.Size = new System.Drawing.Size(1295, 285);
            this.noptextBox.TabIndex = 8;
            // 
            // singleStatusReadButton
            // 
            this.singleStatusReadButton.Location = new System.Drawing.Point(895, 102);
            this.singleStatusReadButton.Name = "singleStatusReadButton";
            this.singleStatusReadButton.Size = new System.Drawing.Size(117, 23);
            this.singleStatusReadButton.TabIndex = 9;
            this.singleStatusReadButton.Text = "Once status read";
            this.singleStatusReadButton.UseVisualStyleBackColor = true;
            this.singleStatusReadButton.Click += new System.EventHandler(this.singleStatusReadButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1397, 428);
            this.Controls.Add(this.singleStatusReadButton);
            this.Controls.Add(this.noptextBox);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.parStatusLabel);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button setButton;
        private System.Windows.Forms.CheckBox d7CheckBox;
        private System.Windows.Forms.CheckBox d6CheckBox;
        private System.Windows.Forms.CheckBox d5CheckBox;
        private System.Windows.Forms.CheckBox d4CheckBox;
        private System.Windows.Forms.CheckBox d3CheckBox;
        private System.Windows.Forms.CheckBox d2CheckBox;
        private System.Windows.Forms.CheckBox d1CheckBox;
        private System.Windows.Forms.CheckBox d0CheckBox;
        private System.Windows.Forms.Label parStatusLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox noptextBox;
        private System.Windows.Forms.Button singleStatusReadButton;
    }
}


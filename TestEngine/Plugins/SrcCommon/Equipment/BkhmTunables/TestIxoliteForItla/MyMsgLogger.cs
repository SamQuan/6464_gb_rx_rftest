using System;
using System.Collections.Generic;
using System.Text;

namespace IxoliteGetNopTester
{
    class MyMsgLogger : IxoliteLibDotNet.IMsgLogger
    {
        Queue<string> myLogQueue;
        StringBuilder hexTrans;

        #region IMsgLogger Members

        public void LastMsgOK()
        {
            //myLogQueue.Enqueue("M LastMsgOK()");
        }

        public byte LastMsgRcvd(ref object indx)
        {
            myLogQueue.Enqueue("M LastMsgRcvd(" + indx.GetType().FullName + "/" + indx.ToString() + ")");
            return 0;
        }

        public byte LastMsgRcvdOK(ref object indx)
        {
            myLogQueue.Enqueue("M LastMsgRcvdOK(" + indx.GetType().FullName + "/" + indx.ToString() + ")");
            return 0;
        }

        public string LastMsgRcvdStr()
        {
            string msg = hexTrans.ToString();
            hexTrans = new StringBuilder();
            return msg;
        }

        public byte LastMsgSent(ref object indx)
        {
            myLogQueue.Enqueue("M LastMsgSent(" + indx.GetType().FullName + "/" + indx.ToString() + ")");
            return 0;
        }

        public byte LastMsgSentOK(ref object indx)
        {
            myLogQueue.Enqueue("M LastMsgSentOK(" + indx.GetType().FullName + "/" + indx.ToString() + ")");
            return 0;
        }

        public string LastMsgSentStr()
        {
            string msg = hexTrans.ToString();
            hexTrans = new StringBuilder();
            return msg;
        }

        public void LogINTERNAL(ref short errnum, ref string errsrc, ref string ErrMsg, ref string where)
        {
            myLogQueue.Enqueue("M LogINTERNAL(" + errnum + ", " + errsrc + ", " + ErrMsg + ", " + where + ")"); 
        }

        public void LogToFile(ref string what, ref string fname)
        {
            myLogQueue.Enqueue("M LogToFile(" + what + "," + fname + ")");
        }

        public void NotifyFATAL(ref string what)
        {
            myLogQueue.Enqueue("M NotifyFATAL(" + what + ")");
        }

        public void NotifyUser(string msg)
        {
            myLogQueue.Enqueue("M NotifyUser(" + msg + ")");
        }

        public void chompLastMsgSent()
        {
        }

        public void clearLastMsgRcvd()
        {
            hexTrans = new StringBuilder();
        }

        public void clearLastMsgSent()
        {
            hexTrans = new StringBuilder();
        }

        public void getMainFormMSCommsObject()
        {
        }

        public void logFATAL(ref string strErrMsg)
        {
            myLogQueue.Enqueue("E" + strErrMsg);
        }

        public void logLastMsgRcvd(ref byte DataByte)
        {
            hexTrans.Append(DataByte.ToString("X02") + " ");
        }

        public void logLastMsgSent(ref byte DataByte)
        {
            hexTrans.Append(DataByte.ToString("X02") + " ");
        }

        public void logMESSAGE(ref string strErrMsg)
        {
            myLogQueue.Enqueue("M" + strErrMsg);
        }

        public bool logMsgQueueEmpty()
        {
            return this.myLogQueue.Count == 0;
        }

        public void logMsgQueueInit()
        {
            this.myLogQueue = new Queue<string>();
            this.hexTrans = new StringBuilder();
        }

        public string logMsgQueueRead()
        {
            return myLogQueue.Dequeue();
        }

        #endregion

        #region IMsgLogger Members



        #endregion
    }
}

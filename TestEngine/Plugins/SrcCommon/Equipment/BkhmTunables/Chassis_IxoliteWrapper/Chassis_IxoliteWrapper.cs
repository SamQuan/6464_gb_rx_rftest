// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// Chassis_IxoliteWrapper.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using System.IO.Ports;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis class wrapping the Ixolite Library.
    /// </summary>
    public class Chassis_IxoliteWrapper : Bookham.TestEngine.PluginInterfaces.Chassis.Chassis
    {
        #region Private data
        /// <summary>
        /// Reference to Ixolite setup class.
        /// </summary>
        private readonly IxoliteLibDotNet.LibSetup ixoSetup;

        // References to command Izolite classes.
        private readonly IxoliteLibDotNet.MSAStandardCommands ixoMsaStd;
        private readonly IxoliteLibDotNet.MSAVendorCommands ixoMsaVen;
        private readonly IxoliteLibDotNet.OIFStandardCommands ixoOifStd;
        private readonly IxoliteLibDotNet.OIFVendorCommands ixoOifVen;

        /// <summary>
        /// Flag indicating if the D0, D1, S5 and S7 parallel port lines may be used.
        /// </summary>
        private bool i2cInUse;

        /// <summary>
        /// Initialised Parallel Port Address
        /// </summary>
        private long parPortAddr;

        /// <summary>
        /// Device I2C address
        /// </summary>
        private byte i2cAddr;

        /// <summary>
        /// I2C Maximum rate
        /// </summary>
        private int maxI2CRate_kHz = 0;

        /// <summary>
        /// Which Instance of Chassis Ixolite is this?
        /// </summary>
        private int thisInstance;

        /// <summary>
        /// How many Instances of Chassis Ixolite are there?
        /// </summary>
        private static int nbrInstances = 0;

        /// <summary>
        /// Which instance of Chassis Ixolite last accessed the hardware?
        /// </summary>
        private static int currentInstance = 0;


        private SerialPort _serialPort;
        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        #endregion

        /// <summary>
        /// Set up chassis.
        /// </summary>
        /// <param name="chassisName">Name of chassis.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="resourceStringId">"LPT,[port addr HEX],[I2C addr HEX],[maxI2CRate_kHz]" or "COM,[com port],[rate]".</param>
        public Chassis_IxoliteWrapper(string chassisName, string driverName, string resourceStringId)
            : base(chassisName, driverName, resourceStringId)
        {
            //store Chassis ixolite Instance information
            thisInstance = ++nbrInstances;
            currentInstance = thisInstance;

            this.ValidHardwareData.Add("Ixolite", new ChassisDataRecord("Unknown", "Unknown", "Unknown"));

            this.ixoSetup = new IxoliteLibDotNet.LibSetup();
            this.ixoSetup.IxoInitialise(new IxoliteMsgLogger());
            this.ixoMsaStd = new IxoliteLibDotNet.MSAStandardCommands();
            this.ixoMsaVen = new IxoliteLibDotNet.MSAVendorCommands();
            this.ixoOifStd = new IxoliteLibDotNet.OIFStandardCommands();
            this.ixoOifVen = new IxoliteLibDotNet.OIFVendorCommands();

            /* Set up Comms */

            string[] resByComma = resourceStringId.Split(',');
            if (resByComma[0] == "LPT")
            {
                if ((resByComma.Length < 3) || (resByComma.Length > 4))
                {
                    throw new ChassisException("Bad resource string. Use \"LPT,nnn,nn,nnn\". eg \"LPT,378,41\"");
                }

                /* Extract parallel port addr */
                this.parPortAddr = long.Parse(resByComma[1], System.Globalization.NumberStyles.HexNumber);

                /* Extract I2C module address. */
                this.i2cAddr = byte.Parse(resByComma[2], System.Globalization.NumberStyles.HexNumber);

                // Extract max I2C rate

                if (resByComma.Length > 3)
                {
                    this.maxI2CRate_kHz = int.Parse(resByComma[3], System.Globalization.CultureInfo.InvariantCulture);
                }

                /* Set up Parallel Comms */
                this.ixoSetup.SetParallelComms(ref this.parPortAddr, ref this.i2cAddr, ref this.maxI2CRate_kHz);
                this.i2cInUse = true;
            }
            else if (resByComma[0] == "COM")
            {
                if ((resByComma.Length != 2) && (resByComma.Length != 3))
                {
                    throw new ChassisException(
                        "Bad resource string. Use \"COM,n\" or \"COM,n,rate\". eg \"COM,1,9600\"");
                }

                /* Load com port number and rate from resource id. (Default 9600). */
                string comPort = resByComma[1];
                string rate = (resByComma.Length == 3) ? resByComma[2] : "9600";

                /* Set up Serial Binary Comms */
                _serialPort = buildSerPort(comPort, rate);
                this.ixoSetup.SetSerialComms(_serialPort);
                this.i2cInUse = false;
            }
            else if (resByComma[0] == "COMPAR")
            {
                if ((resByComma.Length != 3) && (resByComma.Length != 4))
                {
                    throw new ChassisException(
                        "Bad resource string. Use \"COMPAR,n,parport\" or \"COMPAR,n,rate,parport\". eg \"COMPAR,1,9600,378\"");
                }

                /* Load com port number and rate from resource id. (Default 9600). */
                string comPort = resByComma[1];
                string rate = (resByComma.Length == 4) ? resByComma[2] : "9600";

                /* Last item is parport address. */
                short parPort = short.Parse(resByComma[resByComma.Length - 1], System.Globalization.NumberStyles.HexNumber);

                /* Set up Serial Binary Comms with parallel port support. */
                _serialPort = buildSerPort(comPort, rate);
                this.ixoSetup.SetSerialCommsWithParallelPinControl(_serialPort, parPort);
                this.i2cInUse = false;
            }
            else
            {
                throw new ChassisException("Bad resource string. Use \"LPT,nnn,nn\" or \"COM,n\".");
            }
        }

        /// <summary>
        /// Construct a serial port object from parts of the resource string.
        /// </summary>
        /// <param name="comPort">COMx port portion of resource string.</param>
        /// <param name="rate">bps rate portion of resource string.</param>
        /// <returns>Constructed SerialPort handler instance.</returns>
        private SerialPort buildSerPort(string comPort, string rate)
        {
            return new System.IO.Ports.SerialPort(
                "COM" + comPort,                                /* COMn */
                int.Parse(rate),                                /* bps */
                System.IO.Ports.Parity.None, 8, StopBits.One);  /* 8N1 */
        }


        /// <summary>
        /// For systems with multiple 300 Pin MSA Compliant  devices to drive - force the corrrect
        /// Paralel Port Address, Device Address and Max I2C rate. 
        /// </summary>
        private void restoreParallelCommsContext()
        {
            //No need to restore context if there is only 1 instance of Chassis Ixolite
            if (nbrInstances > 1)
            {
                //Only restore context if a different instance of ixolite last Accessed the hardware.
                if (currentInstance != thisInstance)
                {
                    //Call new IxoliteLibDotNet call to restore context for Parallel Port Based I2C Comms.
                    this.ixoSetup.RestoreParallelCommsContext(ref this.parPortAddr, ref this.i2cAddr, ref this.maxI2CRate_kHz);
                    currentInstance = thisInstance;
                }
            }
        }

        #region Chassis abstract function implementations
        public override string FirmwareVersion
        {
            get { return "Unknown"; }
        }

        public override string HardwareIdentity
        {
            get { return "Unknown"; }
        }
        #endregion

        #region Error handling
        /// <summary>
        /// Update the logs after a call into Ixolite.
        /// </summary>
        private string updateLogger(string errorText, TunableProtocol errorProtocol)
        {
            System.Text.StringBuilder retStr = new StringBuilder("");
            bool errorflag = false;

            while (this.ixoSetup.IsLogEmpty() == false)
            {
                string taggedMessage = this.ixoSetup.LogRead();
                string message = taggedMessage.Substring(1);

                if (taggedMessage[0] == 'E')
                {
                    errorflag = true;
                }

                this.LogEvent(message);

                retStr.Append(message);
                retStr.Append("\r\n");
            }

            if (errorflag)
            {
                throw new TunableModuleException(errorText + "\r\n" + retStr.ToString(), errorProtocol);
            }

            return retStr.ToString();
        }

        /// <summary>
        /// General string returned by VB indicating error.
        /// </summary>
        private const string errorStringNeg9999 = "-9999";

        /// <summary>
        /// General double value returned by VB indicating error.
        /// </summary>
        private const double errorDoubleNeg9999 = -9999.0;

        /// <summary>
        /// General short value returned by VB indicating error.
        /// </summary>
        private const short errorShortNeg9999 = -9999;

        /// <summary>
        /// General int value returned by VB indicating error.
        /// </summary>
        private const int errorIntNeg9999 = -9999;

        /// <summary>
        /// Self contained exception thrower.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="protocol">Protocol id.</param>
        private static void throwError(string message, TunableProtocol protocol)
        {
            throw new TunableModuleException(message, protocol);
        }
        #endregion

        #region Ixolite return value checking functions
        /// <summary>
        /// Check result of an Ixolite function returning bool. false represents error.
        /// </summary>
        /// <param name="result">Result of Ixolite function call.</param>
        /// <param name="error">Exception message.</param>
        /// <param name="protocol">Protocol causing error.</param>
        private void checkIxoliteReturnBool(bool result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == false) throwError(error + "\r\n" + logText, protocol);
        }

        /// <summary>
        /// Checks if a result value is near the -9999 error code.
        /// </summary>
        /// <param name="result">Result of Ixolite function returning double or float.</param>
        /// <param name="maxDev">Maximum devation from -9999 allowable.</param>
        /// <returns>True if result is within -9999 plus-or-minus maxDev. False otherwise.</returns>
        private bool isNearNeg9999(double result, double maxDev)
        {
            return (result > errorDoubleNeg9999 - maxDev) && (result < errorDoubleNeg9999 + maxDev);
        }

        /// <summary>
        /// Check result of an Ixolite function returning double. -9999.0 (within 0.01%) represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private double checkIxoliteReturnDouble(double result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (isNearNeg9999(result, 1)) throwError(error + "\r\n" + logText, protocol);
            return result;
        }

        /// <summary>
        /// Check result of an Ixolite function returning float. -9999.0 (within 0.1%) represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private float checkIxoliteReturnFloat(float result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (isNearNeg9999(result, 10)) throwError(error + "\r\n" + logText, protocol);
            return result;
        }


        /// <summary>
        /// Check result of an Ixolite function returning string. "-9999" represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non "-9999" return value.</returns>
        private string checkIxoliteReturnString(string result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorStringNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result.TrimEnd('\0');
        }

        /// <summary>
        /// Check result of an Ixolite function returning short. -9999 represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private short checkIxoliteReturnShort(short result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorShortNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result;
        }

        /// <summary>
        /// Check result of an Ixolite function returning int. -9999 represents error.
        /// </summary>
        /// <param name="result">Result of ixolite function call.</param>
        /// <param name="error">Error message.</param>
        /// <param name="protocol">Protocol.</param>
        /// <returns>Non -9999 return value.</returns>
        private int checkIxoliteReturnInt(int result, string error, TunableProtocol protocol)
        {
            string logText = this.updateLogger(error, protocol);
            if (result == errorIntNeg9999) throwError(error + "\r\n" + logText, protocol);
            return result;
        }
        #endregion

        #region Helper functions
        private ITUBand bandStringToBand(string bandStr, string functionName)
        {
            switch (bandStr)
            {
                case "C":
                    return ITUBand.CBand;
                case "L":
                    return ITUBand.LBand;
                case "S":
                    return ITUBand.SBand;
                default:
                    throw new TunableModuleException(functionName + " returned \"" + bandStr + "\", instead of C, L or S.", TunableProtocol.MSA);
            }
        }

        private string bandToLetter(ITUBand band)
        {
            switch (band)
            {
                case ITUBand.CBand: return "C";
                case ITUBand.LBand: return "L";
                case ITUBand.SBand: return "S";
                default:
                    {
                        throw new ChassisException("Called bandToLetter with bad value - " + band.ToString());
                    }
            }
        }

        /// <summary>
        /// Calls (Get/Set)ControlRegister with mask and bits.
        /// </summary>
        /// <param name="mask">Bit mask. Bits with a value of 1 in mask are modified in register only.</param>
        /// <param name="newBits">New bit values for masked bits.</param>
        public void ModifyControlRegister(int mask, int newBits)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Fetch old register value. */
            int oldReg = this.GetControlRegister();

            /* Modify by mask and new value. Zeroify the masked bits then OR in new value. */
            int newReg = (oldReg & ~mask) | (newBits & mask);

            /* Send new values back to device. */
            this.SetControlRegister(newReg);
        }

        /// <summary>
        /// Calls ModifyControlRegsister for single bit sets.
        /// </summary>
        /// <param name="bitNum">Bit number to modify 0..15.</param>
        /// <param name="bitVal">New bit value.</param>
        public void ModifyControlRegisterSingleBit(int bitNum, bool bitVal)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            if ((bitNum < 0) || (bitNum > 15))
            {
                throw new ChassisException("ModifyControlRegisterSingleBit called with bad bitNum (" + bitNum + ")");
            }

            int mask = 1 << bitNum;
            this.ModifyControlRegister(mask, bitVal ? mask : 0);
        }

        /// <summary>
        /// Extracts a single bit (as a bool) from the control register.
        /// </summary>
        /// <param name="bitNum">Bit number 0..15.</param>
        /// <returns>Bit value as a boolean.</returns>
        public bool GetControlRegisterSingleBit(int bitNum)
        {

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            if ((bitNum < 0) || (bitNum > 15))
            {
                throw new ChassisException("GetControlRegisterSingleBit called with bad bitNum (" + bitNum + ")");
            }

            return (this.GetControlRegister() & (1 << bitNum)) != 0;
        }

        /// <summary>
        /// Converts a bool parameter value to a string, where true translates to an enum-esque string value.
        /// False values convert to an empty string, which qualify as "any other value".
        /// </summary>
        /// <param name="nv">Bool value.</param>
        /// <param name="ifTrue">String to return if nv is true.</param>
        /// <returns>Value of ifTrue, or "".</returns>
        private string boolToString(bool nv, string ifTrue)
        {
            return nv ? ifTrue : "";
        }
        #endregion

        #region Joe Collins legacy functions
        #region internal delegate
        private delegate T MethodDelegate<T>();
        private delegate TReturn MethodDelegate<TReturn, TInput>(ref TInput input);
        private delegate TReturn MethodDelegate2<TReturn, TInput>(TInput input);
        private delegate TReturn MethodDelegate<TReturn, TInput1, TInput2>(ref TInput1 input1, ref TInput2 input2);
        private delegate TReturn MethodDelegate<TReturn, TInput1, TInput2, TInput3>(ref TInput1 input1, ref TInput2 input2, ref TInput3 input3);

        private T InvokeMethodWithEvent<T>(string command, MethodDelegate<T> methodDelegate)
        {
            //Raises Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            T result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = methodDelegate.Invoke();
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);

                if (typeof(T) == typeof(String))
                {
                    result = (T)((object)e.DataRead);
                }
                else
                {
                    result = (T)(typeof(T)).GetMethod("Parse").Invoke(null, new object[] { e.DataRead });
                }
            }
            return result;
        }

        private TReturn InvokeMethodWithEvent<TReturn, TInput>(string command, MethodDelegate<TReturn, TInput> methodDelegate,
                    ref TInput input)
        {
            //Raises Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            TReturn result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = methodDelegate.Invoke(ref input);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString() + "*|*" + input.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                string[] outputArray = e.DataRead.Split(new string[] { "*|*" }, StringSplitOptions.None);

                if (typeof(TReturn) == typeof(String))
                {
                    result = (TReturn)((object)outputArray[0]);
                }
                else
                {
                    result = (TReturn)(typeof(TReturn)).GetMethod("Parse").Invoke(null, new object[] { outputArray[0] });
                }

                if (input.GetType() == typeof(string))
                {
                    input = (TInput)((object)outputArray[1]);
                }
                else
                {
                    input = (TInput)input.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[1] });
                }


            }
            return result;
        }

        private TReturn InvokeMethodWithEvent<TReturn, TInput>(string command, MethodDelegate2<TReturn, TInput> methodDelegate,
                    TInput input)
        {
            //Raises Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            TReturn result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = methodDelegate.Invoke(input);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);

                if (typeof(TReturn) == typeof(String))
                {
                    result = (TReturn)((object)e.DataRead);
                }
                else
                {
                    result = (TReturn)(typeof(TReturn)).GetMethod("Parse").Invoke(null, new object[] { e.DataRead });
                }

            }
            return result;
        }

        private TReturn InvokeMethodWithEvent<TReturn, TInput1, TInput2>(string command, MethodDelegate<TReturn, TInput1, TInput2> methodDelegate,
                    ref TInput1 input1, ref TInput2 input2)
        {
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            TReturn result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = methodDelegate.Invoke(ref input1, ref input2);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString() + "*|*" + input1.ToString() + "*|*" + input2.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                string[] outputArray = e.DataRead.Split(new string[] { "*|*" }, StringSplitOptions.None);

                if (typeof(TReturn) == typeof(String))
                {
                    result = (TReturn)((object)outputArray[0]);
                }
                else
                {
                    result = (TReturn)(typeof(TReturn)).GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[0] });
                }

                if (input1.GetType() == typeof(string))
                {
                    input1 = (TInput1)((object)outputArray[1]);
                }
                else
                {
                    input1 = (TInput1)input1.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[1] });
                }

                if (input2.GetType() == typeof(string))
                {
                    input2 = (TInput2)((object)outputArray[2]);
                }
                else
                {
                    input2 = (TInput2)input2.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[2] });
                }
            }
            return result;
        }

        private TReturn InvokeMethodWithEvent<TReturn, TInput1, TInput2, TInput3>(string command, MethodDelegate<TReturn, TInput1, TInput2, TInput3> methodDelegate,
                ref TInput1 input1, ref TInput2 input2, ref TInput3 input3)
        {
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            TReturn result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = methodDelegate.Invoke(ref input1, ref input2, ref input3);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString() + "*|*" + input1.ToString() + "*|*" + input2.ToString() + "*|*" + input3.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);

                string[] outputArray = e.DataRead.Split(new string[] { "*|*" }, StringSplitOptions.None);

                if (typeof(TReturn) == typeof(String))
                {
                    result = (TReturn)((object)outputArray[0]);
                }
                else
                {
                    result = (TReturn)(typeof(TReturn)).GetMethod("Parse").Invoke(null, new object[] { outputArray[0] });
                }

                if (input1.GetType() == typeof(string))
                {
                    input1 = (TInput1)((object)outputArray[1]);
                }
                else
                {
                    input1 = (TInput1)input1.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[1] });
                }

                if (input2.GetType() == typeof(string))
                {
                    input2 = (TInput2)((object)outputArray[2]);
                }
                else
                {
                    input2 = (TInput2)input2.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[2] });
                }

                if (input3.GetType() == typeof(string))
                {
                    input3 = (TInput3)((object)outputArray[3]);
                }
                else
                {
                    input3 = (TInput3)input3.GetType().GetMethod("Parse").Invoke(null, new object[] { outputArray[3] });
                }
            }
            return result;
        }

        private string StringToString(string input)
        {
            return input;
        }
        #endregion

        #region Wrap MSAStandardCommands.cls (Legacy)

        /// <summary>
        /// set Tx command
        /// </summary>
        /// <param name="txCmd">tx command</param>
        public void SetTxCommand(TxCommand txCmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte d1 = txCmd.Data1;
            byte d2 = txCmd.Data2;
            byte d3 = txCmd.Data3;

            string command = "SetTxCommand:" + d1.ToString() + ":" + d2.ToString() + ":" + d3.ToString();
            bool result = InvokeMethodWithEvent<bool, byte, byte, byte>(command, new MethodDelegate<bool, byte, byte, byte>(this.ixoMsaStd.SetTxCommand),
                        ref d1, ref d2, ref d3);

            this.checkIxoliteReturnBool(result, "SetTxCommand failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TxCommand ReadTxCommand()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte d1 = 0;
            byte d2 = 0;
            byte d3 = 0;

            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            string command = "ReadTxCommand";
            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoMsaStd.ReadTxCommand(ref d1, ref d2, ref d3);
                OnChassisQuery(command, new byte[] { d1, d2, d3, (byte)(result ? 1 : 0) }, this.Name, "");
            }
            else
            {
                byte[] byteResult = OnChassisQuery(command, this.Name, "");
                d1 = byteResult[0];
                d2 = byteResult[1];
                d3 = byteResult[2];
                result = byteResult[3] == 0 ? false : true;
            }

            checkIxoliteReturnBool(result, "ReadTxCommand failed.", TunableProtocol.MSA);
            return new TxCommand(d1, d2, d3);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="thresh_pc"></param>
        public void SetRxThreshold(double thresh_pc)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetRxThreshold:" + thresh_pc.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(this.ixoMsaStd.SetRxThreshold),
                            ref thresh_pc);

            this.checkIxoliteReturnBool(result, "SetRxThreshold failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double ReadRxThreshold()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            try
            {
                string command = "ReadRxThreshold";
                double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.ReadRxThreshold));

                return result;
            }
            catch
            {
                throw new ChassisException("GetRxThreshold failed.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SaveTxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SaveTxRegister";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.SaveTxRegister));

            this.checkIxoliteReturnBool(result, "SaveTxRegister failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SaveRxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SaveRxRegister";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.SaveRxRegister));

            this.checkIxoliteReturnBool(result, "SaveRxRegister failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreTxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "RestoreTxRegister";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.RestoreTxRegister));

            this.checkIxoliteReturnBool(result, "RestoreTxRegister failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreRxRegister()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "RestoreRxRegister";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.RestoreRxRegister));

            this.checkIxoliteReturnBool(result, "RestoreRxRegister failed.", TunableProtocol.MSA);
        }

        /// <summary>
        /// 
        /// </summary>
        public void EnterProtectMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "EnterProtectMode";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.EnterProtectMode));

            this.checkIxoliteReturnBool(result, "EnterProtectMode failed.", TunableProtocol.MSA);
        }

        public void EnterVendorProtectMode(string password)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "EnterVendorProtectMode";
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(this.ixoMsaStd.EnterVendorProtectMode),
                            ref password);

            this.checkIxoliteReturnBool(result, "EnterVendorProtectMode failed.", TunableProtocol.MSA);
        }

        public void EnterSoftMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "EnterSoftMode";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.EnterSoftMode));

            this.checkIxoliteReturnBool(result, "EnterSoftMode failed.", TunableProtocol.MSA);
        }

        public void EnterPinMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();


            string command = "EnterPinMode";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.EnterPinMode));

            this.checkIxoliteReturnBool(result, "EnterPinMode failed.", TunableProtocol.MSA);
        }

        public void ExitProtectMode()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ExitProtectMode";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.ExitProtectMode));

            this.checkIxoliteReturnBool(result, "ExitProtectMode failed.", TunableProtocol.MSA);
        }

        public void ResetCPN()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ResetCPN";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.ResetCPN));

            this.checkIxoliteReturnBool(result, "ResetCPN failed.", TunableProtocol.MSA);
        }

        public void SelectITUChannel(ITUBand band, double chan)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandStr = bandToLetter(band);

            string command = "SelectITUChannel:" + band.ToString() + ":" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, string, double>(command, new MethodDelegate<bool, string, double>(this.ixoMsaStd.SelectITUChannel),
                            ref bandStr, ref chan);

            this.checkIxoliteReturnBool(result, "SelectITUChannel failed.", TunableProtocol.MSA);
        }

        public BandChannel GetFirstChannel()
        {
            string bandStr = "";
            double chan = 0.0;

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetFirstChannel:" + bandStr + ":" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, string, double>(command, new MethodDelegate<bool, string, double>(ixoMsaStd.GetFirstChannel), ref bandStr, ref chan);

            this.checkIxoliteReturnBool(result, "GetFirstChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetFirstChannel"), chan);
        }

        public BandChannel GetLastChannel()
        {
            string bandStr = "";
            double chan = 0.0;
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetLastChannel:" + bandStr + ":" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, string, double>(command, new MethodDelegate<bool, string, double>(ixoMsaStd.GetLastChannel), ref bandStr, ref chan);

            this.checkIxoliteReturnBool(result, "GetLastChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetLastChannel"), chan);
        }

        public BandChannel GetITUChannel()
        {
            string bandStr = "";
            double chan = 0.0;
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetITUChannel:" + bandStr + ":" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, string, double>(command, new MethodDelegate<bool, string, double>(ixoMsaStd.GetITUChannel), ref bandStr, ref chan);


            this.checkIxoliteReturnBool(result, "GetITUChannel failed.", TunableProtocol.MSA);
            return new BandChannel(bandStringToBand(bandStr, "GetITUChannel"), chan);
        }

        public double ChannelSpacing()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ChannelSpacing";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.ChannelSpacing));

            return this.checkIxoliteReturnDouble(result, "ChannelSpacing failed.", TunableProtocol.MSA);
        }

        public string GetUnitSerialNumber_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetUnitSerialNumber_MSA";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.GetUnitSerialNumber));

            return this.checkIxoliteReturnString(result, "GetUnitSerialNumber failed.", TunableProtocol.MSA);
        }

        public string GetUnitPartNumber_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetUnitPartNumber_MSA";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.GetUnitPartNumber));

            return this.checkIxoliteReturnString(result, "GetUnitPartNumber failed.", TunableProtocol.MSA);
        }

        public string GetManufactureDate_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetManufactureDate_MSA";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.GetManufactureDate));

            return this.checkIxoliteReturnString(result, "GetManufactureDate failed.", TunableProtocol.MSA);
        }

        public string GetSupplier_MSA()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();


            string command = "GetSupplier_MSA";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.GetSupplier));

            return this.checkIxoliteReturnString(result, "GetSupplier failed.", TunableProtocol.MSA);
        }

        public string GetModuleType()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetModuleType";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.GetModuleType));

            return this.checkIxoliteReturnString(result, "GetModuleType failed.", TunableProtocol.MSA);
        }

        public double GetLaserBiasCurrent()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetLaserBiasCurrent";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.LaserBiasCurrent));

            return this.checkIxoliteReturnDouble(result, "LaserBiasCurrent failed.", TunableProtocol.MSA);
        }

        public double LaserOutputPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "LaserOutputPower";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.LaserOutputPower));

            return this.checkIxoliteReturnDouble(result, "LaserOutputPower failed.", TunableProtocol.MSA);
        }

        public double SubMountTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SubMountTemp";
            MethodDelegate<double> methodDelegate = new MethodDelegate<double>(this.ixoMsaStd.SubMountTemp);

            double result = InvokeMethodWithEvent<double>(command, methodDelegate);

            return this.checkIxoliteReturnDouble(result, "SubMountTemp failed.", TunableProtocol.MSA);
        }

        public double RxACPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "RxACPower";
            MethodDelegate<double> methodDelegate = new MethodDelegate<double>(this.ixoMsaStd.RxACPower);

            double result = InvokeMethodWithEvent<double>(command, methodDelegate);

            return this.checkIxoliteReturnDouble(result, "RxACPower failed.", TunableProtocol.MSA);
        }



        public double RxAvgPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "RxAvgPower";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.RxAvgPower));

            return this.checkIxoliteReturnDouble(result, "RxAvgPower failed.", TunableProtocol.MSA);
        }

        public double LaserWavelength()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "LaserWavelength";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.LaserWavelength));

            return this.checkIxoliteReturnDouble(result, "LaserWavelength failed.", TunableProtocol.MSA);
        }

        public double ModuleTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ModuleTemp";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.ModuleTemp));

            return this.checkIxoliteReturnDouble(result, "ModuleTemp failed.", TunableProtocol.MSA);
        }

        public double APDTemp()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "APDTemp";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.APDTemp));

            return this.checkIxoliteReturnDouble(result, "APDTemp failed.", TunableProtocol.MSA);
        }

        public double LaserAbsoluteTemperature()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "LaserAbsolutTemperature";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.LaserAbsoluteTemperature));

            return this.checkIxoliteReturnDouble(result, "LaserAbsoluteTemperature failed.", TunableProtocol.MSA);
        }

        public double ModBias()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ModeBias";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(this.ixoMsaStd.ModBias));

            return this.checkIxoliteReturnDouble(result, "ModBias failed.", TunableProtocol.MSA);
        }

        public void LinkStatus()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "LinkStatus";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.LinkStatus));
            this.checkIxoliteReturnBool(result, "LinkStatus failed.", TunableProtocol.MSA);
        }

        public int ReadMaxI2CRate()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "ReadMaxI2CRate";

            string kbps = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(this.ixoMsaStd.ReadMaxI2CRate));
            kbps = this.checkIxoliteReturnString(kbps, "ReadMaxI2CRate failed.", TunableProtocol.MSA);

            /* Remove "kpbs" suffix if it exists. */
            int lengthSubtractFour = kbps.Length - 4;
            if (kbps.Substring(lengthSubtractFour) == "kbps")
            {
                kbps = kbps.Substring(0, lengthSubtractFour);
            }

            return int.Parse(kbps);
        }

        public void Loopback()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "Loopback";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(this.ixoMsaStd.Loopback));
            this.checkIxoliteReturnBool(result, "Loopback failed.", TunableProtocol.MSA);
        }

        public bool WaitForTempToSettle(int count, double wait_s, double target_degC, double devi_degC)
        {
            int n;

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Calculate bounds */
            double lowTemp = target_degC - devi_degC;
            double highTemp = target_degC + devi_degC;

            for (n = 0; n <= count; ++n)
            {
                /* Read laser temparature. */
                double now_degC = LaserAbsoluteTemperature();

                /* Check against bounds. */
                if ((now_degC >= lowTemp) && (now_degC <= highTemp))
                {
                    /* In range, return true for success. */
                    return true;
                }

                /* else, wait. unless this is the final loop. */
                if (n != count)
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(wait_s));
                }
            }

            /* If we get here, the temperature did not settle quickly enough. */
            return false;
        }

        #endregion

        #region Wrap MSAVendorCommands.cls (Legacy)

        public void SetCalMzMod(short digipot, bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");

            string command = "SetCalMzMode:" + digipot.ToString() + ":" + nv.ToString();
            bool result = InvokeMethodWithEvent<bool, short, string>(command, new MethodDelegate<bool, short, string>(this.ixoMsaVen.SetCalMzMod),
                            ref digipot, ref eeprom);


            checkIxoliteReturnBool(result, "SetCalMzMod failed.", TunableProtocol.MSA);
        }

        public short GetCalMzMod(bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");

            string command = "GetCalMzMod:" + nv.ToString();
            short result = InvokeMethodWithEvent<short, string>(command, new MethodDelegate<short, string>(this.ixoMsaVen.GetCalMzMod),
                            ref eeprom);
            return checkIxoliteReturnShort(result, "GetCalMzMod failed.", TunableProtocol.MSA);
        }

        public void SetCalMzBiasN(short modBias, bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");

            string command = "SetCalMzBiasN:" + modBias.ToString() + ":" + nv.ToString();
            bool result = InvokeMethodWithEvent<bool, short, string>(command, new MethodDelegate<bool, short, string>(this.ixoMsaVen.SetCalMzBiasN),
                            ref modBias, ref eeprom);
            checkIxoliteReturnBool(result, "SetCalMzBiasN failed.", TunableProtocol.MSA);
        }

        public short GetCalMzBiasN(bool nv)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string eeprom = boolToString(nv, "EEPROM");

            string command = "GetCalMzBiasN:" + nv.ToString();
            short result = InvokeMethodWithEvent<short, string>(command, new MethodDelegate<short, string>(this.ixoMsaVen.GetCalMzBiasN),
                            ref eeprom);
            return checkIxoliteReturnShort(result, "GetCalMzBiasN failed.", TunableProtocol.MSA);
        }

        public short GetADCReading(AdcId adc)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "GetADCReading:" + adc.ToString();
            byte adcAsByte = (byte)adc;
            short result = InvokeMethodWithEvent<short, byte>(command, new MethodDelegate<short, byte>(ixoMsaVen.GetADCReading), ref adcAsByte);
            return checkIxoliteReturnShort(result, "GetADCReading failed.", TunableProtocol.MSA);
        }

        public short GetSerdesRegister(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetSerdesRegister:" + index.ToString();

            short result = InvokeMethodWithEvent<short, byte>(command, new MethodDelegate<short, byte>(this.ixoMsaVen.GetSerdesRegister),
                            ref index);
            return checkIxoliteReturnShort(result, "GetSerdesRegister failed.", TunableProtocol.MSA);
        }

        public void SetSerdesRam(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetSerdesRam:" + index.ToString() + ":" + value.ToString();
            bool result = InvokeMethodWithEvent<bool, byte, double>(command, new MethodDelegate<bool, byte, double>(this.ixoMsaVen.SetSerdesRam),
                            ref index, ref value);
            checkIxoliteReturnBool(result, "SetSerdesRam failed.", TunableProtocol.MSA);
        }

        public void SetSerdesRegister(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetSerdesRegister:" + index.ToString() + ":" + value.ToString();
            bool result = InvokeMethodWithEvent<bool, byte, double>(command, new MethodDelegate<bool, byte, double>(this.ixoMsaVen.SetSerdesRegister),
                            ref index, ref value);
            checkIxoliteReturnBool(result, "SetSerdesRegister failed.", TunableProtocol.MSA);
        }

        public void SetSerdesEEPROM(byte index, double value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetSerdesEEPROM:" + index.ToString() + ":" + value.ToString();
            bool result = InvokeMethodWithEvent<bool, byte, double>(command, new MethodDelegate<bool, byte, double>(this.ixoMsaVen.SetSerdesEEPROM),
                            ref index, ref value);

            checkIxoliteReturnBool(result, "SetSerdesEEPROM failed.", TunableProtocol.MSA);
        }

        public void SetRxPwrMonCalTable(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetRxPwrMonCalTable:" + index.ToString();
            bool result = InvokeMethodWithEvent<bool, byte>(command, new MethodDelegate<bool, byte>(ixoMsaVen.SetRxPwrMonCalTable),
                            ref index);
            checkIxoliteReturnBool(result, "SetRxPwrMonCalTable failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrMonCalTable(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetRxPwrMonCalTable:" + index.ToString();
            short result = InvokeMethodWithEvent<short, byte>(command, new MethodDelegate<short, byte>(ixoMsaVen.GetRxPwrMonCalTable), ref index);
            return checkIxoliteReturnShort(result, "GetRxPwrMonCalTable failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrMonCalTableSize()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetRxPwrMonCalTableSize";

            short result = InvokeMethodWithEvent<short>(command, new MethodDelegate<short>(ixoMsaVen.GetRxPwrMonCalTableSize));
            return checkIxoliteReturnShort(result, "GetRxPwrMonCalTableSize failed.", TunableProtocol.MSA);
        }

        public short GetRxPwrPoint(byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetRxPwrPoint:" + index.ToString();
            short result = InvokeMethodWithEvent<short, byte>(command, new MethodDelegate<short, byte>(ixoMsaVen.GetRxPwrPoint),
                                ref index);
            return checkIxoliteReturnShort(result, "GetRxPwrPoint failed.", TunableProtocol.MSA);
        }

        public float GetRawOpticalPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetRawOpticalPower";
            float result = InvokeMethodWithEvent<float>(command, new MethodDelegate<float>(ixoMsaVen.GetRawOpticalPower));

            return checkIxoliteReturnFloat(result, "GetRawOpticalPower failed.", TunableProtocol.MSA);
        }

        public int GetModMonPower()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetModMonPower";
            int result = InvokeMethodWithEvent<int>(command, new MethodDelegate<int>(ixoMsaVen.GetModMonPower));
            return checkIxoliteReturnInt(result, "GetModMonPower failed.", TunableProtocol.MSA);
        }

        public int GetMZModAmplitudeMon()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "GetMZModAmplitudeMon";
            int result = InvokeMethodWithEvent<int>(command, new MethodDelegate<int>(ixoMsaVen.GetMZModAmplitudeMon));
            return checkIxoliteReturnInt(result, "GetMZModAmplitudeMon failed.", TunableProtocol.MSA);
        }

        public void SaveRxCal()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SaveRxCal";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoMsaVen.SaveRxCal));
            checkIxoliteReturnBool(result, "SaveRxCal failed.", TunableProtocol.MSA);
        }

        public void RecallRxCal()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "RecallRxCal";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoMsaVen.RecallRxCal));
            checkIxoliteReturnBool(result, "RecallRxCal failed.", TunableProtocol.MSA);
        }

        public void SetCalTxPowerMon(short uW)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SetCalTxPowerMon:" + uW.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoMsaVen.SetCalTxPowerMon),
                            ref uW);
            checkIxoliteReturnBool(result, "SetCalTxPowerMon failed.", TunableProtocol.MSA);
        }

        public void SetBOLLaserPower(short uW)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetBOLLaserPower:" + uW.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoMsaVen.SetBOLLaserPower), ref uW);
            checkIxoliteReturnBool(result, "SetBOLLaserPower failed.", TunableProtocol.MSA);
        }

        public void SetModulator(short dac)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetModulator:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoMsaVen.SetModulator), ref dac);
            checkIxoliteReturnBool(result, "SetModulator failed.", TunableProtocol.MSA);
        }

        public short GetModulatorDac()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "GetModulatorDac";
            short result = InvokeMethodWithEvent<short>(command, new MethodDelegate<short>(ixoMsaVen.GetModulatorDac));
            return checkIxoliteReturnShort(this.ixoMsaVen.GetModulatorDac(), "GetModulatorDac failed.", TunableProtocol.MSA);
        }

        public void SaveModulator(short dac)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SaveModulator:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoMsaVen.SaveModulator), ref dac);
            checkIxoliteReturnBool(result, "SaveModulator failed.", TunableProtocol.MSA);
        }

        public void SetModMonGain(bool high)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string highStr = boolToString(high, "high");
            string command = "SetModMonGain:" + high.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetModMonGain), ref highStr);

            checkIxoliteReturnBool(result, "SetModMonGain failed.", TunableProtocol.MSA);
        }

        public void SetMZLoop(bool open)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string openStr = boolToString(open, "open");
            string command = "SetMZLoop:" + open.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetMZLoop), ref openStr);
            checkIxoliteReturnBool(result, "SetMZLoop failed.", TunableProtocol.MSA);
        }

        public void SetChirp(bool positive)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string positiveStr = boolToString(positive, "positive");

            string command = "SetChirp:" + positive.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetChirp), ref positiveStr);
            checkIxoliteReturnBool(result, "SetChirp failed.", TunableProtocol.MSA);
        }

        public void ModulatorDriver(bool enable)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string enableStr = boolToString(enable, "enable");
            string command = "ModulatorDriver:" + enable.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.ModulatorDriver), ref enableStr);
            checkIxoliteReturnBool(result, "ModulatorDriver failed.", TunableProtocol.MSA);
        }

        public void SaveModulatorSettings()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SaveModulatorSettings";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoMsaVen.SaveModulatorSettings));
            checkIxoliteReturnBool(result, "SaveModulatorSettings failed.", TunableProtocol.MSA);
        }

        public void RecallModulatorSettings()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "RecallModulatorSettings";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoMsaVen.RecallModulatorSettings));
            checkIxoliteReturnBool(result, "RecallModulatorSettings failed.", TunableProtocol.MSA);
        }

        public void SetFirstChanBand(ITUBand band)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            string command = "SetFirstChanBand:" + band.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetFirstChanBand), ref bandLetter);
            checkIxoliteReturnBool(result, "SetFirstChanBand failed.", TunableProtocol.MSA);
        }

        public void SetLastChanBand(ITUBand band)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            string command = "SetLastChanBand:" + band.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetLastChanBand),
                                ref bandLetter);
            checkIxoliteReturnBool(result, "SetLastChanBand failed.", TunableProtocol.MSA);
        }

        public void SetPinModeBand(ITUBand band)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string bandLetter = bandToLetter(band);
            string command = "SetPinModeBand:" + band.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetPinModeBand),
                        ref bandLetter);
            checkIxoliteReturnBool(result, "SetPinModeBand failed.", TunableProtocol.MSA);
        }

        public void SetFirstChanFreq_MSA(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SetFirstChanFreq_MSA:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoMsaVen.SetFirstChanFreq),
                        ref GHz);
            checkIxoliteReturnBool(result, "SetFirstChanFreq failed.", TunableProtocol.MSA);
        }

        public void SetLastChanFreq(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SetLastChanFreq:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoMsaVen.SetLastChanFreq),
                        ref GHz);
            checkIxoliteReturnBool(result, "SetLastChanFreq failed.", TunableProtocol.MSA);
        }

        public void SetPinModeFreq(double GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SetPinModeFreq:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoMsaVen.SetPinModeFreq),
                        ref GHz);
            checkIxoliteReturnBool(result, "SetPinModeFreq failed.", TunableProtocol.MSA);
        }

        public void SetChanSpacing(short GHz)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SetChanSpacing:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoMsaVen.SetChanSpacing),
                        ref GHz);
            checkIxoliteReturnBool(result, "SetChanSpacing failed.", TunableProtocol.MSA);
        }

        public void SaveBootData()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "SaveBootData";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoMsaVen.SaveBootData));
            checkIxoliteReturnBool(result, "SaveBootData failed.", TunableProtocol.MSA);
        }

        public void OIFLaser(bool enable)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string enableStr = boolToString(enable, "enable");
            string command = "OIFLaser:" + enable.ToString();

            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.OIFLaser),
                            ref enableStr);
            checkIxoliteReturnBool(result, "OIFLaser failed.", TunableProtocol.MSA);
        }

        public void SetSerialNumber_MSA(string value)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetSerialNumber_MSA:" + value;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetSerialNumber),
                        ref value);

            checkIxoliteReturnBool(result, "SetSerialNumber failed.", TunableProtocol.MSA);
        }

        public void SetManufactureDate_MSA(string date)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetManufactureDate_MSA:" + date;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetManufactureDate),
                        ref date);
            checkIxoliteReturnBool(result, "SetManufactureDate_MSA failed.", TunableProtocol.MSA);
        }

        public void SetHardwareRevision(string rev)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetHardwareRevision:" + rev;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetHardwareRevision),
                        ref rev);
            checkIxoliteReturnBool(result, "SetHardwareRevision failed.", TunableProtocol.MSA);
        }

        public void SetPartNumber_MSA(string partno)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            string command = "SetPartNumber_MSA:" + partno;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoMsaVen.SetPartNumber),
                        ref partno);
            checkIxoliteReturnBool(result, "SetPartNumber failed.", TunableProtocol.MSA);
        }

        public void SetLaserTemperature_ohms(double rTarget)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            /* Calculate ADC from rTarget. */
            double adc = 65535.0 * rTarget / (rTarget + 10000);

            string command = "SetLaserTemperature_ohms:" + rTarget.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoMsaVen.SetLaserTempByAdc),
                        ref adc);
            checkIxoliteReturnBool(result, "SetLaserTempByAdc failed.", TunableProtocol.MSA);
        }

        public double GetLaserTemperature_ohms()
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            string command = "GetLaserTemperature_ohms";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(ixoMsaVen.GetLaserTempByAdc));

            double adc = checkIxoliteReturnDouble(result, "GetLaserTempByAdc failed.", TunableProtocol.MSA);

            /* Calculate the target back from the ADC. */
            return (10000.0 * adc) / (65535.0 - adc);
        }

        #endregion

        #region Wrap OIFStandardCommands.cls (Legacy)

        public int GetNOP()
        {
            string command = "GetNOP";
            int result = InvokeMethodWithEvent<int>(command, new MethodDelegate<int>(ixoOifStd.GetNOP));

            return checkIxoliteReturnInt(result, "GetNOP failed.", TunableProtocol.OIF);

        }

        public string GetDeviceType()
        {
            string command = "GetDeviceType";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetDeviceType));
            return checkIxoliteReturnString(result, "GetDeviceType failed.", TunableProtocol.OIF);
        }

        public string GetManufacturer_OIF()
        {
            string command = "GetManufacturer_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetManufacturer));
            return checkIxoliteReturnString(result, "GetManufacturer failed.", TunableProtocol.OIF);
        }

        public string GetModelNumber_OIF()
        {
            string command = "GetModelNumber_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetModelNumber));
            return checkIxoliteReturnString(result, "GetModelNumber failed.", TunableProtocol.OIF);
        }

        public string GetSerialNumber_OIF()
        {
            string command = "GetSerialNumber_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetSerialNumber));
            return checkIxoliteReturnString(result, "GetSerialNumber failed.", TunableProtocol.OIF);
        }

        public string GetManufacturingDate_OIF()
        {
            string command = "GetManufacturingDate_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetManufacturingDate));
            return checkIxoliteReturnString(result, "GetManufacturingDate failed.", TunableProtocol.OIF);
        }

        public string GetReleaseCode()
        {
            string command = "GetReleaseCode";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetReleaseCode));
            return checkIxoliteReturnString(result, "GetReleaseCode failed.", TunableProtocol.OIF);
        }

        public string GetReleaseBackwards()
        {
            string command = "GetReleaseBackwards";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifStd.GetReleaseBackwards));
            return checkIxoliteReturnString(result, "GetReleaseBackwards failed.", TunableProtocol.OIF);
        }

        public void SaveConfig()
        {
            string command = "SaveConfig";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoOifStd.SaveConfig));
            checkIxoliteReturnBool(result, "SaveConfig failed.", TunableProtocol.OIF);
        }

        public int GetIOCap()
        {
            int iocapValue = 0;
            string command = "GetIOCap";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetIOCap),
                        ref iocapValue);
            checkIxoliteReturnBool(result, "GetIOCap failed.", TunableProtocol.OIF);
            return iocapValue;
        }

        public void SetIOCap(int value)
        {
            string command = "SetIOCap";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.SetIOCap),
                        ref value);
            checkIxoliteReturnBool(result, "SetIOCap failed.", TunableProtocol.OIF);
        }

        public OifStatus GetStatusF()
        {
            string command = "GetStatusF";
            int result = InvokeMethodWithEvent<int>(command, new MethodDelegate<int>(ixoOifStd.GetStatusF));
            return new OifStatus(checkIxoliteReturnInt(result, "GetStatusF failed.", TunableProtocol.OIF));
        }

        public void ClearStatusF()
        {
            string command = "ClearStatusF";
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifStd.SetStatusF(0x00FF);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);

                result = bool.Parse(e.DataRead);
            }

            checkIxoliteReturnBool(result, "SetStatusF failed.", TunableProtocol.OIF);
        }

        public OifStatus GetStatusW()
        {
            string command = "GetStatusW";
            int result = InvokeMethodWithEvent<int>(command, new MethodDelegate<int>(ixoOifStd.GetStatusW));
            return new OifStatus(checkIxoliteReturnInt(result, "GetStatusW failed.", TunableProtocol.OIF));
        }

        public void ClearStatusW()
        {
            string command = "ClearStatusW";
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);


            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifStd.SetStatusW(0x00FF);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);

                result = bool.Parse(e.DataRead);
            }
            checkIxoliteReturnBool(result, "SetStatusW failed.", TunableProtocol.OIF);
        }

        public double GetFPowTh()
        {
            double returnValue = 0.0;
            string command = "GetFPowTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetFPowTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetFPowTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFPowTh(double thresh)
        {
            string command = "SetFPowTh:" + thresh.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetFPowTh),
                        ref thresh);
            checkIxoliteReturnBool(result, "SetFPowTh failed.", TunableProtocol.OIF);
        }

        public double GetWPowTh()
        {
            double returnValue = 0.0;
            string command = "GetWPowTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetWPowTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetWPowTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWPowTh(double th)
        {
            string command = "SetWPowTh:" + th.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetWPowTh),
                        ref th);
            checkIxoliteReturnBool(result, "SetWPowTh failed.", TunableProtocol.OIF);
        }

        public double GetFFreqTh()
        {
            double returnValue = 0.0;
            string command = "GetFFreqTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetFFreqTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetFFreqTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFFreqTh(double th)
        {
            string command = "SetFFreqTh:" + th.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetFFreqTh),
                        ref th);
            checkIxoliteReturnBool(result, "SetFFreqTh failed.", TunableProtocol.OIF);
        }

        public double GetWFreqTh()
        {
            double returnValue = 0.0;
            string command = "GetWFreqTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetWFreqTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetWFreqTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWFreqTh(double th)
        {
            string command = "SetWFreqTh:" + th.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetWFreqTh),
                        ref th);
            checkIxoliteReturnBool(result, "SetWFreqTh failed.", TunableProtocol.OIF);
        }

        public double GetFThermTh()
        {
            double returnValue = 0.0;
            string command = "GetFThermTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetFThermTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetFThermTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFThermTh(double th)
        {
            string command = "SetFThermTh:" + th.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetFThermTh),
                            ref th);
            checkIxoliteReturnBool(result, "SetFThermTh failed.", TunableProtocol.OIF);
        }

        public double GetWThermTh()
        {
            double returnValue = 0.0;
            string command = "GetWThermTh";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetWThermTh),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetWThermTh failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetWThermTh(double th)
        {
            string command = "SetWThermTh:" + th.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetWThermTh),
                        ref th);
            checkIxoliteReturnBool(result, "SetWThermTh failed.", TunableProtocol.OIF);
        }

        public OifStatus GetSRQT()
        {
            int returnValue = 0;
            string command = "GetSRQT";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetSRQT),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetSRQT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetSRQT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            string command = "SetSRQT:" + mask.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.SetSRQT),
                        ref maskAsInt);
            checkIxoliteReturnBool(result, "SetSRQT failed.", TunableProtocol.OIF);
        }

        public OifStatus GetFATALT()
        {
            int returnValue = 0;
            string command = "GetFATALT";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetFATALT),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetFATALT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetFATALT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            string command = "SetFATALT:" + maskAsInt.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.SetFATALT),
                        ref maskAsInt);
            checkIxoliteReturnBool(result, "SetFATALT failed.", TunableProtocol.OIF);
        }

        public OifStatus GetALMT()
        {
            int returnValue = 0;
            string command = "GetALMT";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetALMT),
                        ref returnValue);
            checkIxoliteReturnBool(result, "GetALMT failed.", TunableProtocol.OIF);
            return new OifStatus(returnValue);
        }

        public void SetALMT(OifStatus mask)
        {
            int maskAsInt = mask.GetAsInt();
            string command = "SetALMT:" + maskAsInt.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.SetALMT),
                    ref maskAsInt);
            checkIxoliteReturnBool(result, "SetALMT failed.", TunableProtocol.OIF);
        }

        public void SelectChannel(short chan)
        {
            string command = "SelectChannel:" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.SelectChannel),
                      ref chan);
            checkIxoliteReturnBool(result, "SelectChannel failed.", TunableProtocol.OIF);
        }

        public short GetChannel()
        {
            short chan = 0;
            string command = "GetChannel";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetChannel),
                    ref chan);
            checkIxoliteReturnBool(result, "GetChannel failed.", TunableProtocol.OIF);
            return chan;
        }

        public void SetPowerSetpoint(double pwr)
        {
            string command = "SetPowerSetpoint:" + pwr.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetPowerSetpoint),
                        ref pwr);
            checkIxoliteReturnBool(result, "SetPowerSetpoint failed.", TunableProtocol.OIF);
        }

        public double GetPowerSetpoint()
        {
            double pwr = 0.0;
            string command = "GetPowerSetpoint";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetPowerSetpoint),
                        ref pwr);
            checkIxoliteReturnBool(result, "GetPowerSetpoint failed.", TunableProtocol.OIF);
            return pwr;
        }

        public void SoftwareEnableOutput(bool Sena)
        {
            string enableStr = boolToString(Sena, "enable");
            string command = "SoftwareEnableOutput:" + Sena.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifStd.SoftwareEnableOutput),
                        ref enableStr);
            checkIxoliteReturnBool(result, "SoftwareEnableOutput failed.", TunableProtocol.OIF);
        }

        public void ModuleReset()
        {
            string command = "ModuleReset";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoOifStd.ModuleReset));
            checkIxoliteReturnBool(result, "ModuleReset failed.", TunableProtocol.OIF);
        }

        public void SoftReset()
        {
            string command = "SoftReset";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoOifStd.SoftReset));
            checkIxoliteReturnBool(result, "SoftReset failed.", TunableProtocol.OIF);
        }

        public bool GetSoftwareEnableOutput()
        {
            short sena = 0;
            string command = "GetSoftwareEnableOutput";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetSoftwareEnableOutput),
                        ref sena);
            checkIxoliteReturnBool(result, "GetSoftwareEnableOutput failed.", TunableProtocol.OIF);
            return (sena != 0);
        }

        public bool GetADT()
        {
            int mcb = 0;
            string command = "GetADT";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetModuleConfigBehaviour), ref mcb);
            checkIxoliteReturnBool(result, "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);
            return (mcb & 0x0002) != 0;   /* bit 1 */
        }

        public void SetADT(bool adt)
        {
            int mcb = 0;
            string command = "SetADT:" + adt.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command + ":Get", new MethodDelegate<bool, int>(ixoOifStd.GetModuleConfigBehaviour), ref mcb);
            checkIxoliteReturnBool(result, "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);

            int newMcb = (mcb & ~0x0002) | (adt ? 0x0002 : 0x0000);  /* Write into bit 1 */
            byte newMcbAsByte = checked((byte)newMcb);

            result = InvokeMethodWithEvent<bool, byte>(command + ":Set", new MethodDelegate<bool, byte>(ixoOifStd.SetModuleConfigBehaviour), ref newMcbAsByte);
            checkIxoliteReturnBool(result, "SetModuleConfigBehaviour failed.", TunableProtocol.OIF);
        }

        public bool GetSDF()
        {
            int mcb = 0;
            string command = "GetSDF";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetModuleConfigBehaviour), ref mcb);
            checkIxoliteReturnBool(result, "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);
            return (mcb & 0x0004) != 0;   /* bit 2 */
        }

        public void SetSDF(bool sdf)
        {
            int mcb = 0;
            string command = "SetSDF:" + sdf.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command + ":Get", new MethodDelegate<bool, int>(ixoOifStd.GetModuleConfigBehaviour), ref mcb);
            checkIxoliteReturnBool(result, "GetModuleConfigBehaviour failed.", TunableProtocol.OIF);

            int newMcb = (mcb & ~0x0004) | (sdf ? 0x0004 : 0x0000);  /* Write into bit 2 */
            byte newMcbAsByte = checked((byte)newMcb);
            result = InvokeMethodWithEvent<bool, byte>(command + ":Set", new MethodDelegate<bool, byte>(ixoOifStd.SetModuleConfigBehaviour), ref newMcbAsByte);
            checkIxoliteReturnBool(result, "SetModuleConfigBehaviour failed.", TunableProtocol.OIF);
        }

        public void SetFrequencyGrid(short GHz)
        {
            string command = "SetFrequencyGrid:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.SetFrequencyGrid), ref GHz);
            checkIxoliteReturnBool(result, "SetFrequencyGrid failed.", TunableProtocol.OIF);
        }

        public short GetFrequencyGrid()
        {
            short returnValue = 0;
            string command = "GetFrequencyGrid";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetFrequencyGrid), ref returnValue);
            checkIxoliteReturnBool(result, "GetFrequencyGrid failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public double GetFirstChanFreq_OIF()
        {
            double returnValue = 0.0;
            string command = "GetFirstChanFreq_OIF";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetFirstChanFreq), ref returnValue);
            checkIxoliteReturnBool(result, "GetFirstChanFreq failed.", TunableProtocol.OIF);
            return returnValue;
        }

        public void SetFirstChanFreq_OIF(double freq)
        {
            string command = "SetFirstChanFreq_OIF:" + freq.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetFirstChanFreq), ref freq);
            checkIxoliteReturnBool(result, "SetFirstChanFreq failed.", TunableProtocol.OIF);
        }

        public double GetLaserFrequency()
        {
            double lf = 0.0;
            string command = "GetLaserFrequency";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetLaserFrequency), ref lf);
            checkIxoliteReturnBool(result, "GetLaserFrequency failed.", TunableProtocol.OIF);
            return lf;

        }

        public double GetLaserOutputPower()
        {
            double oop = 0.0;
            string command = "GetLaserOutputPower";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetLaserOutputPower), ref oop);
            checkIxoliteReturnBool(result, "GetLaserOutputPower failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetCurrentTemperature()
        {
            double ctemp = 0.0;
            string command = "GetCurrentTemperature";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetCurrentTemperature), ref ctemp);
            checkIxoliteReturnBool(result, "GetCurrentTemperature failed.", TunableProtocol.OIF);
            return ctemp;
        }

        public double GetOpticalPowerMin()
        {
            double oop = 0.0;
            string command = "GetOpticalPowerMin";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetOpticalPowerMin), ref oop);
            checkIxoliteReturnBool(result, "GetOpticalPowerMin failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetOpticalPowerMax()
        {
            double oop = 0.0;
            string command = "GetOpticalPowerMax";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetOpticalPowerMax), ref oop);
            checkIxoliteReturnBool(result, "GetOpticalPowerMax failed.", TunableProtocol.OIF);
            return oop;
        }

        public double GetLaserFirstFreq()
        {
            double rval = 0.0;
            string command = "GetLaserFirstFreq";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetLaserFirstFreq), ref rval);
            checkIxoliteReturnBool(result, "GetLaserFirstFreq failed.", TunableProtocol.OIF);
            return rval;
        }

        public void SetLaserFirstFreq(double freq)
        {
            string command = "SetLaserFirstFreq:" + freq.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetLaserFirstFreq), ref freq);
            checkIxoliteReturnBool(result, "SetLaserFirstFreq failed.", TunableProtocol.OIF);
        }

        public double GetLaserLastFreq()
        {
            double rval = 0.0;
            string command = "GetLaserLastFreq";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetLaserLastFreq), ref rval);
            checkIxoliteReturnBool(result, "GetLaserLastFreq failed.", TunableProtocol.OIF);
            return rval;
        }

        public void SetLaserLastFreq(double freq)
        {
            string command = "SetLaserLastFreq:" + freq.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetLaserLastFreq), ref freq);
            checkIxoliteReturnBool(result, "SetLaserLastFreq failed.", TunableProtocol.OIF);
        }

        public void SetMinFrequencyGrid(double GHz)
        {
            string command = "SetMinFrequencyGrid:" + GHz.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetMinFrequencyGrid), ref GHz);
            checkIxoliteReturnBool(result, "SetMinFrequencyGrid failed.", TunableProtocol.OIF);
        }

        public short GetMinFrequencyGrid()
        {
            short rval = 0;
            string command = "GetMinFrequencyGrid";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetMinFrequencyGrid), ref rval);
            checkIxoliteReturnBool(result, "GetMinFrequencyGrid failed.", TunableProtocol.OIF);
            return rval;
        }

        public DitherState GetDitherEnable()
        {
            int retVal = 0;
            string command = "GetDitherEnable";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifStd.GetDitherEnable), ref retVal);
            checkIxoliteReturnBool(result, "GetDitherEnable failed.", TunableProtocol.OIF);
            return new DitherState(retVal);
        }

        public void SetDitherEnable(DitherState flags)
        {
            byte de = checked((byte)(flags.GetAsInt()));
            string command = "SetDitherEnable:" + de.ToString();
            bool result = InvokeMethodWithEvent<bool, byte>(command, new MethodDelegate<bool, byte>(ixoOifStd.SetDitherEnable), ref de);
            checkIxoliteReturnBool(result, "SetDitherEnable failed.", TunableProtocol.OIF);
        }

        public short GetDitherR()
        {
            short retVal = 0;
            string command = "GetDitherR";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetDitherR), ref retVal);
            checkIxoliteReturnBool(result, "GetDitherR failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherR(short dith)
        {
            string command = "SetDitherR:" + dith.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.SetDitherR), ref dith);
            checkIxoliteReturnBool(result, "SetDitherR failed.", TunableProtocol.OIF);
        }

        public double GetDitherF()
        {
            double retVal = 0.0;
            string command = "GetDitherF";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetDitherF), ref retVal);
            checkIxoliteReturnBool(result, "GetDitherF failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherF(double dith)
        {
            string command = "SetDitherF:" + dith.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetDitherF), ref dith);
            checkIxoliteReturnBool(result, "SetDitherF failed.", TunableProtocol.OIF);
        }

        public double GetDitherA()
        {
            double retVal = 0.0;
            string command = "GetDitherA";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetDitherA), ref retVal);
            checkIxoliteReturnBool(this.ixoOifStd.GetDitherA(ref retVal), "GetDitherA failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetDitherA(double dith)
        {
            string command = "SetDitherA:" + dith.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.SetDitherA), ref dith);
            checkIxoliteReturnBool(result, "SetDitherA failed.", TunableProtocol.OIF);
        }

        public double GetTCaseL()
        {
            double retVal = 0;
            string command = "GetTCaseL";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetTCaseL), ref retVal);
            checkIxoliteReturnBool(result, "GetTCaseL failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetTCaseL(double tcl)
        {
            string command = "SetTCaseL:" + tcl.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate2<bool, double>(ixoOifStd.SetTCaseL), tcl);
            checkIxoliteReturnBool(result, "SetTCaseL failed.", TunableProtocol.OIF);
        }

        public double GetTCaseH()
        {
            double retVal = 0;
            string command = "GetTCaseH";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifStd.GetTCaseH), ref retVal);
            checkIxoliteReturnBool(result, "GetTCaseH failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetTCaseH(double tch)
        {
            string command = "SetTCaseH:" + tch.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate2<bool, double>(ixoOifStd.SetTCaseH), tch);
            checkIxoliteReturnBool(result, "SetTCaseH failed.", TunableProtocol.OIF);
        }

        public short GetFAgeTh()
        {
            short retVal = 0;
            string command = "GetFAgeTh";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetFAgeTh), ref retVal);
            checkIxoliteReturnBool(result, "GetFAgeTh failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetFAgeTh(short age)
        {
            string command = "SetFAgeTh:" + age.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.SetFAgeTh), ref age);
            checkIxoliteReturnBool(result, "SetFAgeTh failed.", TunableProtocol.OIF);
        }

        public short GetWAgeTh()
        {
            short retVal = 0;
            string command = "GetWAgeTh";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.GetWAgeTh), ref retVal);
            checkIxoliteReturnBool(result, "GetWAgeTh failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetWAgeTh(short age)
        {
            string command = "SetWAgeTh:" + age.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifStd.SetWAgeTh), ref age);
            checkIxoliteReturnBool(result, "SetWAgeTh failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommand.cls (IOifBkhm) (Legacy)

        public double GetPCBTemp()
        {
            string command = "GetPCBTemp";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(ixoOifVen.GetPCBTemp));
            double monTemp = checkIxoliteReturnDouble(result, "GetPCBTemp failed.", TunableProtocol.OIF);
            if (monTemp > 327.67)
            {
                return monTemp - 655.36;
            }
            else
            {
                return monTemp;
            }
        }

        public double GetWaveCode()
        {
            string command = "GetWaveCode";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(ixoOifVen.GetWaveCode));
            return checkIxoliteReturnDouble(result, "GetWaveCode failed.", TunableProtocol.OIF);
        }

        public void SetWaveCode(short wc)
        {
            string command = "SetWaveCode:" + wc.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetWaveCode), ref wc);
            checkIxoliteReturnBool(result, "SetWaveCode failed.", TunableProtocol.OIF);
        }

        public void SaveChannel(short chan)
        {
            string command = "SaveChannel:" + chan.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SaveChannel), ref chan);
            checkIxoliteReturnBool(result, "SaveChannel failed.", TunableProtocol.OIF);
        }

        public int GetSOADAC()
        {
            int dac = 0;
            string command = "GetSOADAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetSOADAC), ref dac);
            checkIxoliteReturnBool(result, "GetSOADAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetSOADAC(int dac)
        {
            string command = "SetSOADAC:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetSOADAC), ref dac);
            checkIxoliteReturnBool(result, "SetSOADAC failed.", TunableProtocol.OIF);
        }

        public int GetGainDAC()
        {
            int dac = 0;
            string command = "GetGainDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetGainDAC), ref dac);
            checkIxoliteReturnBool(result, "GetGainDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetGainDAC(int dac)
        {
            string command = "SetGainDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetGainDAC), ref dac);
            checkIxoliteReturnBool(result, "SetGainDAC failed.", TunableProtocol.OIF);
        }

        public int GetFrontEvenDAC()
        {
            int dac = 0;
            string command = "GetFrontEvenDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetFrontEvenDAC), ref dac);
            checkIxoliteReturnBool(result, "GetFrontEvenDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetFrontEvenDAC(int dac)
        {
            string command = "SetFrontEvenDAC:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetFrontEvenDAC), ref dac);
            checkIxoliteReturnBool(result, "SetFrontEvenDAC failed.", TunableProtocol.OIF);
        }

        public int GetFrontOddDAC()
        {
            int dac = 0;
            string command = "GetFrontOddDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetFrontOddDAC), ref dac);
            checkIxoliteReturnBool(result, "GetFrontOddDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetFrontOddDAC(int dac)
        {
            string command = "SetFrontOddDAC:" + dac;
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetFrontOddDAC), ref dac);
            checkIxoliteReturnBool(result, "SetFrontOddDAC failed.", TunableProtocol.OIF);
        }

        public int GetRearDAC()
        {
            int dac = 0;
            string command = "GetRearDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetRearDAC), ref dac);
            checkIxoliteReturnBool(result, "GetRearDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetRearDAC(int dac)
        {
            string command = "SetRearDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetRearDAC), ref dac);
            checkIxoliteReturnBool(result, "SetRearDAC failed.", TunableProtocol.OIF);
        }

        public int GetPhaseDAC()
        {
            int dac = 0;
            string command = "GetPhaseDAC";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetPhaseDAC), ref dac);
            checkIxoliteReturnBool(result, "GetPhaseDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetPhaseDAC(int dac)
        {
            string command = "SetPhaseDAC:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetPhaseDAC), ref dac);
            checkIxoliteReturnBool(result, "SetPhaseDAC failed.", TunableProtocol.OIF);
        }

        public short GetDitherDAC()
        {
            short dac = 0;
            string command = "GetDitherDAC";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetDitherDAC), ref dac);
            checkIxoliteReturnBool(this.ixoOifVen.GetDitherDAC(ref dac), "GetDitherDAC failed.", TunableProtocol.OIF);
            return dac;
        }

        public void SetDitherDAC(short dac)
        {
            string command = "SetDitherDAC:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetDitherDAC), ref dac);
            checkIxoliteReturnBool(result, "SetDitherDAC failed.", TunableProtocol.OIF);
        }

        public int GetLockerTxMon()
        {
            int ltxmon = 0;
            string command = "GetLockerTxMon";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetLockerTxMon), ref ltxmon);
            checkIxoliteReturnBool(result, "GetLockerTxMon failed.", TunableProtocol.OIF);
            return ltxmon;
        }

        public int GetLockerRefMon()
        {
            int lrefmon = 0;
            string command = "GetLockerRefMon";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetLockerRefMon), ref lrefmon);
            checkIxoliteReturnBool(result, "GetLockerRefMon failed.", TunableProtocol.OIF);
            return lrefmon;
        }

        public int GetLaserPowerMon(short n)
        {
            int lpm = 0;
            string command = "GetLaserPowerMon:" + n.ToString();
            bool result = InvokeMethodWithEvent<bool, int, short>(command, new MethodDelegate<bool, int, short>(ixoOifVen.GetLaserPowerMon), ref lpm, ref n);
            checkIxoliteReturnBool(result, "GetLaserPowerMon failed.", TunableProtocol.OIF);
            return lpm;
        }

        public int GetLsPhaseSense()
        {
            int lsps = 0;
            string command = "GetLsPhaseSense";
            bool reslt = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetLsPhaseSense), ref lsps);
            checkIxoliteReturnBool(this.ixoOifVen.GetLsPhaseSense(ref lsps), "GetLsPhaseSense failed.", TunableProtocol.OIF);
            return lsps;
        }

        public short GetTxCoarsePot()
        {
            short tcp = 0;
            string command = "GetTxCoarsePot";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetTxCoarsePot), ref tcp);
            checkIxoliteReturnBool(result, "GetTxCoarsePot failed.", TunableProtocol.OIF);
            return tcp;
        }

        public void SetTxCoarsePot(short pot)
        {
            /* Ixolite function takes two references, returning back the set value. */
            short tcp = pot;
            string command = "SetTxCoarsePot:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short, short>(command, new MethodDelegate<bool, short, short>(ixoOifVen.SetTxCoarsePot), ref tcp, ref pot);
            checkIxoliteReturnBool(result, "SetTxCoarsePot failed.", TunableProtocol.OIF);
        }

        public short GetRefCoarsePot()
        {
            short rcp = 0;
            string command = "GetRefCoarsePot";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetRefCoarsePot), ref rcp);
            checkIxoliteReturnBool(result, "GetRefCoarsePot failed.", TunableProtocol.OIF);
            return rcp;
        }

        public void SetRefCoarsePot(short rcp)
        {
            short pot = rcp;
            string command = "SetRefCoarsePot";
            bool result = InvokeMethodWithEvent<bool, short, short>(command, new MethodDelegate<bool, short, short>(ixoOifVen.SetRefCoarsePot), ref rcp, ref pot);
            checkIxoliteReturnBool(result, "SetRefCoarsePot failed.", TunableProtocol.OIF);
        }

        public int GetPhaseMin()
        {
            int pmin = 0;
            string command = "GetPhaseMin";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetPhaseMin), ref pmin);
            checkIxoliteReturnBool(result, "GetPhaseMin failed.", TunableProtocol.OIF);
            return pmin;
        }

        public void SetPhaseMin(int pmin)
        {
            string command = "SetPhaseMin:" + pmin.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetPhaseMin), ref pmin);
            checkIxoliteReturnBool(result, "SetPhaseMin failed.", TunableProtocol.OIF);
        }

        public int GetPhaseMax()
        {
            int pmax = 0;
            string command = "GetPhaseMax";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetPhaseMax), ref pmax);
            checkIxoliteReturnBool(result, "GetPhaseMax failed.", TunableProtocol.OIF);
            return pmax;
        }

        public void SetPhaseMax(int pmax)
        {
            string command = "SetPhaseMax:" + pmax.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetPhaseMax), ref pmax);
            checkIxoliteReturnBool(result, "SetPhaseMax failed.", TunableProtocol.OIF);
        }

        public int GetPowerMonFactor()
        {
            double retVal = 0.0;
            string command = "GetPowerMonFactor";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetPowerMonFactor), ref retVal);
            checkIxoliteReturnBool(result, "GetPowerMonFactor failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetPowerMonFactor(int pmf)
        {
            double pmfAsDouble = pmf;
            string command = "SetPowerMonFactor:" + pmf.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetPowerMonFactor), ref pmfAsDouble);
            checkIxoliteReturnBool(result, "SetPowerMonFactor failed.", TunableProtocol.OIF);
        }

        public int GetLockerErrorFactor()
        {
            double retVal = 0.0;
            string command = "GetLockerErrorFactor";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetLockerErrorFactor), ref retVal);
            checkIxoliteReturnBool(result, "GetLockerErrorFactor failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetLockerErrorFactor(int lef)
        {
            double lefAsDouble = lef;
            string command = "SetLockerErrorFactor:" + lef.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetLockerErrorFactor), ref lefAsDouble);
            checkIxoliteReturnBool(result, "SetLockerErrorFactor failed.", TunableProtocol.OIF);
        }

        public void SelectChannel_OIFVen(short channel)
        {
            string command = "SelectChannel_OIFVen:" + channel.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SelectChannel), ref channel);
            checkIxoliteReturnBool(result, "SelectChannel failed.", TunableProtocol.OIF);
        }

        public int GetControlRegister()
        {
            byte upper = 0;
            byte lower = 0;
            string command = "GetControlRegister";
            bool result = InvokeMethodWithEvent<bool, byte, byte>(command, new MethodDelegate<bool, byte, byte>(ixoOifVen.GetRawControlRegister), ref upper, ref lower);
            checkIxoliteReturnBool(result, "GetRawControlRegister failed.", TunableProtocol.OIF);
            return (upper << 8) | lower;
        }

        public void SetControlRegister(int newVal)
        {
            byte upper = checked((byte)((newVal >> 8) & 0x00ff));
            byte lower = checked((byte)(newVal & 0x00ff));

            string command = "SetControlRegister:" + newVal.ToString();

            //Rasies Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = ixoOifVen.SetRawControlRegister(upper, lower);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(command, new byte[] { upper, lower, (byte)(result ? 1 : 0) }, this.Name, "");
            }
            else
            {
                //ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                byte[] resultArray = OnChassisQuery(command, this.Name, "");
                result = resultArray[2] == 0 ? false : true;
                upper = resultArray[0];
                lower = resultArray[1];
            }

            checkIxoliteReturnBool(result, "SetRawControlRegister failed.", TunableProtocol.OIF);
        }

        public double GetLaserPowerEstimate()
        {
            string command = "GetLaserPowerEstimate";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(ixoOifVen.GetLaserPowerEstimate));
            return checkIxoliteReturnDouble(result, "GetLaserPowerEstimate failed.", TunableProtocol.OIF);
        }

        public int GetPowerMonOffset()
        {
            double retVal = 0.0;
            string command = "GetPowerMonOffset";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetPowerMonOffset), ref retVal);
            checkIxoliteReturnBool(this.ixoOifVen.GetPowerMonOffset(ref retVal), "GetPowerMonOffset failed.", TunableProtocol.OIF);
            return (int)retVal;
        }

        public void SetPowerMonOffset(int pmo)
        {
            double pmoAsDouble = pmo;
            string command = "SetPowerMonOffset:" + pmo.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetPowerMonOffset), ref pmoAsDouble);
            checkIxoliteReturnBool(result, "SetPowerMonOffset failed.", TunableProtocol.OIF);
        }

        /* The following functions call Ixolite with an "I" parameter value, meaning current, or any other value
         * for voltage. */
        public short GetSrcNumber(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            short retVal = 0;
            string command = "GetSrcNumber:" + srcMode.ToString();
            bool result = InvokeMethodWithEvent<bool, string, short>(command, new MethodDelegate<bool, string, short>(ixoOifVen.GetSrcNumber), ref istr, ref retVal);
            checkIxoliteReturnBool(this.ixoOifVen.GetSrcNumber(ref istr, ref retVal), "GetSrcNumber failed.", TunableProtocol.OIF);
            return retVal;
        }


        public short GetSrcIndex(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            short retVal = 0;
            string command = "GetSrcIndex:" + srcMode.ToString();
            bool result = InvokeMethodWithEvent<bool, string, short>(command, new MethodDelegate<bool, string, short>(ixoOifVen.GetSrcIndex), ref istr, ref retVal);
            checkIxoliteReturnBool(result, "GetSrcIndex failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSrcIndex(SrcMode srcMode, short pot)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            string command = "SetSrcIndex:" + srcMode.ToString() + ":" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, string, short>(command, new MethodDelegate<bool, string, short>(ixoOifVen.SetSrcIndex), ref istr, ref pot);
            checkIxoliteReturnBool(result, "SetSrcIndex failed.", TunableProtocol.OIF);
        }

        public void SetSrcNumber(SrcMode srcMode, short pot)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            string command = "SetSrcNumber:" + srcMode.ToString() + ":" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, string, short>(command, new MethodDelegate<bool, string, short>(ixoOifVen.SetSrcNumber), ref istr, ref pot);
            checkIxoliteReturnBool(this.ixoOifVen.SetSrcNumber(ref istr, ref pot), "SetSrcNumber failed.", TunableProtocol.OIF);
        }

        /* Set up index and number before calling G/SetSrcValue */
        public float GetSrcValue(SrcMode srcMode)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            string command = "GetSrcValue:" + srcMode.ToString();
            byte[] floatAsBytes = new byte[4];

            bool result = InvokeMethodWithEvent<bool, string, byte, byte>(command, new MethodDelegate<bool, string, byte, byte>(ixoOifVen.GetSrcLSW), ref istr, ref floatAsBytes[1], ref floatAsBytes[0]);
            checkIxoliteReturnBool(result, "GetSrcLSW failed.", TunableProtocol.OIF);
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command, new MethodDelegate<bool, string, byte, byte>(ixoOifVen.GetSrcMSW), ref istr, ref floatAsBytes[3], ref floatAsBytes[2]);
            checkIxoliteReturnBool(result, "GetSrcMSW failed.", TunableProtocol.OIF);

            return System.BitConverter.ToSingle(floatAsBytes, 0);
        }

        public void SetSrcValue(SrcMode srcMode, float value)
        {
            string istr = boolToString(srcMode == SrcMode.Current, "I");
            byte[] floatAsBytes = System.BitConverter.GetBytes(value);

            string command = "SetSrcValue:" + srcMode.ToString() + ":" + value.ToString();
            bool result = InvokeMethodWithEvent<bool, string, byte, byte>(command, new MethodDelegate<bool, string, byte, byte>(ixoOifVen.SetSrcLSW), ref istr, ref floatAsBytes[1], ref floatAsBytes[0]);
            checkIxoliteReturnBool(result, "SetSrcLSW failed.", TunableProtocol.OIF);
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command, new MethodDelegate<bool, string, byte, byte>(ixoOifVen.SetSrcMSW), ref istr, ref floatAsBytes[3], ref floatAsBytes[2]);
            checkIxoliteReturnBool(result, "SetSrcMSW failed.", TunableProtocol.OIF);
        }

        public short GetSBSDitherMultiplier()
        {
            /* Note that the value is only 8 bits, but carried as short, duer a quirk in JC's code. */
            short retVal = 0;
            string command = "GetSBSDitherMultiplier";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetSBSDitherMultiplier), ref retVal);
            checkIxoliteReturnBool(result, "GetSBSDitherMultiplier failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSBSDitherMultiplier(byte mult)
        {
            string command = "SetSBSDitherMultiplier:" + mult.ToString();
            bool result = InvokeMethodWithEvent<bool, byte>(command, new MethodDelegate<bool, byte>(ixoOifVen.SetSBSDitherMultiplier), ref mult);
            checkIxoliteReturnBool(result, "SetSBSDitherMultiplier failed.", TunableProtocol.OIF);
        }

        public double GetWavelengthError()
        {
            string command = "GetWavelengthError";
            double result = InvokeMethodWithEvent<double>(command, new MethodDelegate<double>(ixoOifVen.GetWavelengthError));
            return checkIxoliteReturnDouble(result, "GetWavelengthError failed.", TunableProtocol.OIF);
        }

        public int GetBaseDac()
        {
            int retVal = 0;
            string command = "GetBaseDac";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetBaseCamp), ref retVal);
            checkIxoliteReturnBool(result, "GetBaseCamp failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetBaseDac(int dac)
        {
            string command = "SetBaseDac:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetBaseCamp), ref dac);
            checkIxoliteReturnBool(result, "SetBaseCamp failed.", TunableProtocol.OIF);
        }

        public int GetBaseDac2()
        {
            int retVal = 0;
            string command = "GetBaseDac2";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetBaseCamp2), ref retVal);
            checkIxoliteReturnBool(result, "GetBaseCamp2 failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetBaseDac2(int dac)
        {
            string command = "SetBaseDac2:" + dac.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.SetBaseCamp2), ref dac);
            checkIxoliteReturnBool(result, "SetBaseCamp2 failed.", TunableProtocol.OIF);
        }

        public double GetPlateauPower()
        {
            double retVal = 0.0;
            string command = "GetPlateauPower";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetPlateauPower), ref retVal);
            checkIxoliteReturnBool(result, "GetPlateauPower failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetPlateauPower(double pwr)
        {
            string command = "SetPlateauPower:" + pwr.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetPlateauPower), ref pwr);
            checkIxoliteReturnBool(result, "SetPlateauPower failed.", TunableProtocol.OIF);
        }

        public double GetSinkSlope()
        {
            double retVal = 0.0;
            string command = "GetSinkSlope";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetSinkSlope), ref retVal);
            checkIxoliteReturnBool(result, "GetSinkSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSinkSlope(double slp)
        {
            string command = "SetSinkSlope:" + slp.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetSinkSlope), ref slp);
            checkIxoliteReturnBool(result, "SetSinkSlope failed.", TunableProtocol.OIF);
        }

        public double GetSourceSlope()
        {
            double retVal = 0.0;
            string command = "GetSourceSlope";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetSourceSlope), ref retVal);
            checkIxoliteReturnBool(result, "GetSourceSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetSourceSlope(double slp)
        {
            string command = "SetSourceSlope:" + slp.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetSourceSlope), ref slp);
            checkIxoliteReturnBool(result, "SetSourceSlope failed.", TunableProtocol.OIF);
        }


        /* Constants for use by Control Reg Front Section code. */
        private const int oddBitLocation = 6;
        private const int evenBitLocation = 8;

        public void SetControlRegFrontSection(int i)
        {
            /* Check valid range. */
            if ((i < 1) || (i > 7))
            {
                throw new ChassisException("SetControlRegFrontSection called with " + i + ". (Must be 0..7)");
            }

            /* Calculate new value, moving ODD (i/2) and EVEN ((i-1)/2) into its slot.
             *  i   ODD  EVEN       Control register format
             *  1    00   00        .... ..ee oo.. ....
             *  2    01   00
             *  3    01   01
             *  4    10   01
             *  5    10   10
             *  6    11   10
             *  7    11   11
             */
            int newBits = ((i / 2) << oddBitLocation) | (((i - 1) / 2) << evenBitLocation);

            /* Modify control register, masking bits 6..9. */
            this.ModifyControlRegister(0x03C0, newBits);
        }

        public int GetControlRegFrontSection()
        {
            /* Get control register values */
            int regValue = this.GetControlRegister();

            /* Extract odd and even parts. */
            int odd = (regValue >> oddBitLocation) & 0x0003;
            int even = (regValue >> evenBitLocation) & 0x0003;

            /* Find bit 0 of i by looking at the differnce between odd and even. */
            int bit0;
            if (odd == even)
            {
                bit0 = 1;   /* If ODD == EVEN, i is ODD. */
            }
            else if (odd == (even + 1))
            {
                bit0 = 0;   /* If ODD != EVEN, i is EVEN. */
            }
            else
            {
                throw new TunableModuleException("Control register has bad value. ODD=" + odd + " EVEN=" + even, TunableProtocol.OIF);
            }

            /* Bits 1 and 2 are odd value. Bit 0 is bit 0. */
            return (odd * 2) + bit0;
        }

        public void SetControlRegLockerOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(15, on);
        }

        public bool GetControlRegLockerOn()
        {
            return this.GetControlRegisterSingleBit(15);
        }

        public void SetControlRegLockerSlopeOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(14, on);
        }

        public bool GetControlRegLockerSlopeOn()
        {
            return this.GetControlRegisterSingleBit(14);
        }

        public void SetControlRegLaserTecOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(13, on);
        }

        public bool GetControlRegLaserTecOn()
        {
            return this.GetControlRegisterSingleBit(13);
        }

        public void SetControlRegAutoLockStart()
        {
            this.ModifyControlRegisterSingleBit(4, true);
        }

        public void SetControlRegDitherOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(3, on);
        }

        public bool GetControlRegDitherOn()
        {
            return this.GetControlRegisterSingleBit(3);
        }

        public void SetControlRegProfilingOn(bool on)
        {
            this.ModifyControlRegisterSingleBit(1, on);
        }

        public bool GetControlRegProfilingOn()
        {
            return this.GetControlRegisterSingleBit(1);
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (IWbttItlaCommon) (Legacy)

        public void SetUnitSerialNumber_OIF(string serno)
        {
            string command = "SetUnitSerialNumber_OIF:" + serno;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetUnitSerialNumber), ref serno);
            checkIxoliteReturnBool(result, "SetUnitSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetUnitBuildDate_OIF(string bdate)
        {
            string command = "SetUnitBuildDate_OIF:" + bdate;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetUnitBuildDate), ref  bdate);
            checkIxoliteReturnBool(result, "SetUnitBuildDate failed.", TunableProtocol.OIF);
        }

        public void SetModuleNumber_OIF(string moduleNumber)
        {
            string command = "SetModuleNumber_OIF:" + moduleNumber;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetUnitManufacturer), ref moduleNumber);
            /* Ixolite VB uses this register's old name. */
            checkIxoliteReturnBool(result, "SetModuleNumber failed.", TunableProtocol.OIF);
        }

        public bool GetPowerControlStateOn()
        {
            string state = "";
            string command = "GetPowerControlStateOn";
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.GetPowerControlState), ref state);
            checkIxoliteReturnBool(result, "GetPowerControlState failed.", TunableProtocol.OIF);
            return (state == "on");
        }

        public void SetPowerControlStateOn(bool enabled)
        {
            string stateStr = boolToString(enabled, "on");
            string command = "SetPowerControlStateOn:" + enabled.ToString();
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetPowerControlState), ref stateStr);
            checkIxoliteReturnBool(result, "SetPowerControlState failed.", TunableProtocol.OIF);
        }

        public string GetLaserSerialNumber_OIF()
        {
            string command = "GetLaserSerialNumber_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifVen.GetLaserSerialNumber));
            return checkIxoliteReturnString(result, "GetLaserSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetLaserSerialNumber_OIF(string serno)
        {
            string command = "SetLaserSerialNumber_OIF:" + serno;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetLaserSerialNumber), ref serno);
            checkIxoliteReturnBool(result, "SetLaserSerialNumber failed.", TunableProtocol.OIF);
        }

        public string GetBoardSerialNumber_OIF()
        {
            string command = "GetBoardSerialNumber_OIF";
            string result = InvokeMethodWithEvent<string>(command, new MethodDelegate<string>(ixoOifVen.GetBoardSerialNumber));
            return checkIxoliteReturnString(result, "GetBoardSerialNumber failed.", TunableProtocol.OIF);
        }

        public void SetBoardSerialNumber_OIF(string serno)
        {
            string command = "SetBoardSerialNumber_OIF:" + serno;
            bool result = InvokeMethodWithEvent<bool, string>(command, new MethodDelegate<bool, string>(ixoOifVen.SetBoardSerialNumber), ref serno);
            checkIxoliteReturnBool(result, "SetBoardSerialNumber failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommands.cls (IWbttFcuCommon) (Legacy)

        public short GetVTECLevel()
        {
            string command = "GetVTECLevel";
            return checkIxoliteReturnShort(this.ixoOifVen.GetVTECLevel(), "GetVTECLevel failed.", TunableProtocol.OIF);
        }

        public short GetLsBiasSense()
        {
            short lsbs = 0;
            string command = "GetLsBiasSense";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetLsBiasSense), ref lsbs);
            checkIxoliteReturnBool(result, "GetLsBiasSense failed.", TunableProtocol.OIF);
            return lsbs;
        }

        public int GetLockerErrorMon()
        {
            int errmon = 0;
            string command = "GetLockerErrorMon";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetLockerErrorMon), ref errmon);
            checkIxoliteReturnBool(result, "GetLockerErrorMon failed.", TunableProtocol.OIF);
            return errmon;
        }

        public int GetLockerReference()
        {
            int lckref = 0;
            string command = "GetLockerReference";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetLockerReference), ref lckref);
            checkIxoliteReturnBool(result, "GetLockerReference failed.", TunableProtocol.OIF);
            return lckref;
        }

        public short GetTxFinePot()
        {
            short tfp = 0;
            string command = "GetTxFinePot";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetTxFinePot), ref tfp);
            checkIxoliteReturnBool(result, "GetTxFinePot failed.", TunableProtocol.OIF);
            return tfp;
        }

        public void SetTxFinePot(short pot)
        {
            short tfp = pot;
            string command = "SetTxFinePot:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short, short>(command, new MethodDelegate<bool, short, short>(ixoOifVen.SetTxFinePot), ref tfp, ref pot);
            checkIxoliteReturnBool(result, "SetTxFinePot failed.", TunableProtocol.OIF);
        }

        public short GetLockerRangePot()
        {
            short pot = 0;
            string command = "GetLockerRangePot";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetLockerRangePot), ref pot);
            checkIxoliteReturnBool(result, "GetLockerRangePot failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetLockerRangePot(short pot)
        {
            string command = "SetLockerRangePot:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetLockerRangePot), ref pot);
            checkIxoliteReturnBool(result, "SetLockerRangePot failed.", TunableProtocol.OIF);
        }

        public short GetPowerMonPot()
        {
            short pot = 0;
            string command = "GetPowerMonPot";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetPowerMonPot), ref pot);
            checkIxoliteReturnBool(result, "GetPowerMonPot failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetPowerMonPot(short pot)
        {
            string command = "SetPowerMonPot:" + pot;
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetPowerMonPot), ref pot);
            checkIxoliteReturnBool(result, "SetPowerMonPot failed.", TunableProtocol.OIF);
        }

        public short GetAtmelReference()
        {
            string command = "GetAtmelReference";
            short result = InvokeMethodWithEvent<short>(command, new MethodDelegate<short>(ixoOifVen.GetAtmelReference));
            return checkIxoliteReturnShort(result, "GetAtmelReference failed.", TunableProtocol.OIF);
        }

        #endregion

        #region Wrap OIFVendorCommands.cls (WBTT only) (Legacy)
        public void SetMinPowerSetpoint(double dBm)
        {
            string command = "SetMinPowerSetpoint:" + dBm.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetMinPowerSetpoint), ref dBm);
            checkIxoliteReturnBool(result, "SetMinPowerSetpoint failed.", TunableProtocol.OIF);
        }

        public void SetMaxPowerSetpoint(double dBm)
        {
            string command = "SetMaxPowerSetpoint:" + dBm.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetMaxPowerSetpoint), ref dBm);
            checkIxoliteReturnBool(result, "SetMaxPowerSetpoint failed.", TunableProtocol.OIF);
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (FCU only) (Legacy)
        public void SetIVSource(float value, IVSourceIndexNumbers index)
        {
            string command = "SetIVSource:" + value.ToString() + ":" + index.ToString();
            /* Set index. */
            int indexAsInt = (int)index;
            bool result = InvokeMethodWithEvent<bool, int>(command + ":1", new MethodDelegate<bool, int>(ixoOifVen.SetIVSourceIndex), ref indexAsInt);
            checkIxoliteReturnBool(result, "SetIVSourceIndex failed.", TunableProtocol.OIF);

            /* Set value. */
            byte[] valueAsBytes = System.BitConverter.GetBytes(value);
            string lsw = "LSW";
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command + ":2", new MethodDelegate<bool, string, byte, byte>(ixoOifVen.SetIVSource), ref lsw,
                        ref valueAsBytes[1], ref valueAsBytes[0]);
            checkIxoliteReturnBool(result, "SetIVSource failed.", TunableProtocol.OIF);
            string msw = "MSW";
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command + ":3", new MethodDelegate<bool, string, byte, byte>(ixoOifVen.SetIVSource), ref msw,
                        ref valueAsBytes[3], ref valueAsBytes[2]);
            checkIxoliteReturnBool(result, "SetIVSource failed.", TunableProtocol.OIF);

            /* Apply new settings. */
            result = InvokeMethodWithEvent<bool>(command + ":4", new MethodDelegate<bool>(ixoOifVen.SetIVSourceDAC));
            checkIxoliteReturnBool(result, "SetIVSourceDAC failed.", TunableProtocol.OIF);
        }

        public float GetIVSource(IVSourceIndexNumbers index)
        {
            int indexAsInt = (int)index;
            string command = "GetIVSource:" + index.ToString();
            bool result = InvokeMethodWithEvent<bool, int>(command + ":1", new MethodDelegate<bool, int>(ixoOifVen.SetIVSourceIndex), ref indexAsInt);
            checkIxoliteReturnBool(result, "SetIVSourceIndex failed.", TunableProtocol.OIF);

            byte[] valueAsBytes = new byte[4];
            string lsw = "LSW";
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command + ":2", new MethodDelegate<bool, string, byte, byte>(ixoOifVen.GetIVSource), ref lsw,
                            ref valueAsBytes[1], ref valueAsBytes[0]);
            checkIxoliteReturnBool(result, "GetIVSource failed.", TunableProtocol.OIF);
            string msw = "MSW";
            result = InvokeMethodWithEvent<bool, string, byte, byte>(command + ":3", new MethodDelegate<bool, string, byte, byte>(ixoOifVen.GetIVSource), ref msw,
                            ref valueAsBytes[3], ref valueAsBytes[2]);
            checkIxoliteReturnBool(result, "GetIVSource failed.", TunableProtocol.OIF);

            return System.BitConverter.ToSingle(valueAsBytes, 0);
        }

        public short GetTECADC()
        {
            short tadc = 0;
            string command = "GetTECADC";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetTECADC), ref tadc);
            checkIxoliteReturnBool(result, "GetTECADC failed.", TunableProtocol.OIF);
            return tadc;
        }

        public short GetLeftMZBias()
        {
            short pot = 0;
            string command = "GetLeftMZBias";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetLeftMZBias), ref pot);
            checkIxoliteReturnBool(result, "GetLeftMZBias failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetLeftMZBias(short pot)
        {
            string command = "SetLeftMZBias:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetLeftMZBias), ref pot);
            checkIxoliteReturnBool(result, "SetLeftMZBias failed.", TunableProtocol.OIF);
        }

        public short GetRightMZBias()
        {
            short pot = 0;
            string command = "GetRightMZBias";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetRightMZBias), ref pot);
            checkIxoliteReturnBool(result, "GetRightMZBias failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetRightMZBias(short pot)
        {
            string command = "SetRightMZBias:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetRightMZBias), ref pot);
            checkIxoliteReturnBool(result, "SetRightMZBias failed.", TunableProtocol.OIF);
        }

        public short GetMZModulation()
        {
            short pot = 0;
            string command = "GetMZModulation";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetMZModulation), ref pot);
            checkIxoliteReturnBool(result, "GetMZModulation failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetMZModulation(short pot)
        {
            string command = "SetMZModulation:" + pot;
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetMZModulation), ref pot);
            checkIxoliteReturnBool(result, "SetMZModulation failed.", TunableProtocol.OIF);
        }

        public short GetLeftImbalance()
        {
            return ((short)GetOifRegisterAsUint(0xD7));
        }

        public void SetLeftImbalance(short dac)
        {
            SetOifRegisterAsUint(0xD7, dac);
        }

        public short GetRightImbalance()
        {
            return ((short)GetOifRegisterAsUint(0xD8));
        }

        public void SetRightImbalance(short dac)
        {
            SetOifRegisterAsUint(0xD8, dac);
        }

        public int GetRawThermisterBridgeAlt_OifBkhm()
        {
            int therm = 0;
            string command = "GetRawThermisterBridgeAlt_OifBkhm";
            bool result = InvokeMethodWithEvent<bool, int>(command, new MethodDelegate<bool, int>(ixoOifVen.GetRawThermisterBridgeAlt), ref therm);
            checkIxoliteReturnBool(result, "GetRawThermisterBridgeAlt failed.", TunableProtocol.OIF);
            return therm;
        }
        #endregion

        #region Wrap OIFVendorCommands.cls (ITLA only) (Legacy)
        public void SetOffGridTuning(double offGrid)
        {
            string command = "SetOffGridTuning:" + offGrid.ToString();
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.SetOffGridTuning), ref offGrid);
            checkIxoliteReturnBool(result, "SetOffGridTuning failed.", TunableProtocol.OIF);
        }

        public double GetOffGridTuning()
        {
            double offGrid = 0.0;
            string command = "GetOffGridTuning";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetOffGridTuning), ref offGrid);
            checkIxoliteReturnBool(result, "GetOffGridTuning failed.", TunableProtocol.OIF);
            return offGrid;
        }

        public void UnlockModule()
        {
            string command = "UnlockModule";
            bool result = InvokeMethodWithEvent<bool>(command, new MethodDelegate<bool>(ixoOifVen.UnlockModule));
            checkIxoliteReturnBool(result, "UnlockModule failed.", TunableProtocol.OIF);
        }

        public short GetOffGridPositive()
        {
            short pot = 0;
            string command = "GetOffGridPositive";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetOffGridPositive), ref pot);
            checkIxoliteReturnBool(result, "GetOffGridPositive failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetOffGridPositive(short pot)
        {
            string command = "SetOffGridPositive:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetOffGridPositive), ref pot);
            checkIxoliteReturnBool(result, "SetOffGridPositive failed.", TunableProtocol.OIF);
        }

        public short GetOffGridNegative()
        {
            short pot = 0;
            string command = "GetOffGridNegative";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetOffGridNegative), ref pot);
            checkIxoliteReturnBool(result, "GetOffGridNegative failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetOffGridNegative(short pot)
        {
            string command = "SetOffGridNegative:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetOffGridNegative), ref pot);
            checkIxoliteReturnBool(result, "SetOffGridNegative failed.", TunableProtocol.OIF);
        }

        public short GetChannelCentre()
        {
            short pot = 0;
            string command = "GetChannelCentre";
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.GetChannelCentre), ref pot);
            checkIxoliteReturnBool(result, "GetChannelCentre failed.", TunableProtocol.OIF);
            return pot;
        }

        public void SetChannelCentre(short pot)
        {
            string command = "SetChannelCentre:" + pot.ToString();
            bool result = InvokeMethodWithEvent<bool, short>(command, new MethodDelegate<bool, short>(ixoOifVen.SetChannelCentre), ref pot);
            checkIxoliteReturnBool(result, "SetChannelCentre failed.", TunableProtocol.OIF);
        }


        public int GetModeCentrePhaseDAC_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDC);
        }

        public void SetModeCentrePhaseDAC_OifBkhm(int dac)
        {
            SetOifRegisterAsUInt(0xDC, checked((UInt16)dac));
        }

        public int GetBOLPhaseDAC_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDD);
        }

        public void SetBOLPhaseDAC_OifBkhm(int dac)
        {
            SetOifRegisterAsUInt(0xDD, checked((UInt16)dac));
        }

        public int GetSOACalibrationTableSize_OifBkhm()
        {
            return GetOifRegisterAsInt(0xDE);
        }

        public int GetSOACalibrationTableValue_uW_OifBkhm(int idx)
        {
            SetOifRegisterAsInt(0xDE, checked((short)idx));
            return GetOifRegisterAsInt(0xDF);
        }

        public void SetSOACalibrationTableValue_uW_OifBkhm(int idx, int value)
        {
            SetOifRegisterAsInt(0xDE, checked((short)idx));
            SetOifRegisterAsInt(0xDF, checked((short)value));
        }

        public double GetLowPowerEtalonSlope()
        {
            double retVal = 0.0;
            string command = "GetLowPowerEtalonSlope";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate<bool, double>(ixoOifVen.GetLowPowerEtalonSlope), ref retVal);
            checkIxoliteReturnBool(result, "GetLowPowerEtalonSlope failed.", TunableProtocol.OIF);
            return retVal;
        }

        public void SetLowPowerEtalonSlope(double lef)
        {
            string command = "SetLowPowerEtalonSlope";
            bool result = InvokeMethodWithEvent<bool, double>(command, new MethodDelegate2<bool, double>(ixoOifVen.SetLowPowerEtalonSlope), lef);
            checkIxoliteReturnBool(result, "SetLowPowerEtalonSlope failed.", TunableProtocol.OIF);
        }



        #endregion

        #region Parallel port pin control
        /// <summary>
        /// Sets parallel port output data register.
        /// </summary>
        /// <param name="mask">Bits to modify.</param>
        /// <param name="bits">New bit values.</param>
        public void SetParPortData(byte mask, byte bits)
        {
            string command = "SetParPortData:" + mask.ToString() + ":" + bits.ToString();
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            if (chassisOnlineCheckEventArgs.Online)
            {
                this.ixoSetup.SetParPortData(mask, bits);
            }
            //raise chassis write event for Test Engine Core.
            OnChassisWrite(command, this.Name, "");
        }

        /// <summary>
        /// Gets parallel port input status register.
        /// </summary>
        /// <returns>Current status register value.</returns>
        public byte GetParPortStatus()
        {
            string command = "GetParPortStatus";
            byte result = InvokeMethodWithEvent<byte>(command, new MethodDelegate<byte>(ixoSetup.GetParPortStatus));
            return result;
        }

        /// <summary>
        /// Gets flag indicating if the I2C reserved pins are allowed to be used.
        /// </summary>
        public bool AllowParPortI2CReserved
        {
            get { return !this.i2cInUse; }
        }

        #endregion

        #region OIF register utility functions
        /// <summary>
        /// Gets stated OIF register as an unsigned integer.
        /// </summary>
        /// <param name="oifReg">Register number to get.</param>
        /// <returns>Uint16 value inside an int. 0..64k</returns>
        public int GetOifRegisterAsUint(byte oifReg)
        {
            byte upper = 0;
            byte lower = 0;

            string command = "GetOifRegisterAsUint:" + oifReg.ToString();
            //Raises Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = ixoOifVen.GetOifRegisterAsBytes(oifReg, ref upper, ref lower);
                OnChassisQuery(command, new byte[] { upper, lower, (byte)(result ? 1 : 0) }, this.Name, "");
            }
            else
            {
                byte[] byteResult = OnChassisQuery(command, this.Name, "");
                upper = byteResult[0];
                lower = byteResult[1];
                result = byteResult[2] == 0 ? false : true;
            }

            bool success = result;

            //this.ixoOifVen.GetOifRegisterAsBytes(oifReg, ref upper, ref lower);
            string errMsg = string.Format("Get OIF 0x{0:X02} failed.", oifReg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);

            return (upper << 8) | lower;
        }

        /// <summary>
        /// Sets an OIF register as an unsigned integer.
        /// </summary>
        /// <param name="oifReg">OIF register number to set.</param>
        /// <param name="ui16">New value. 0..64k only.</param>
        public void SetOifRegisterAsUint(byte oifReg, int ui16)
        {
            byte upper = checked((byte)((ui16 >> 8) & 0xff));
            byte lower = checked((byte)(ui16 & 0xff));
            string command = "SetOifRegisterAsUint:" + oifReg.ToString() + ":" + ui16.ToString();
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = ixoOifVen.SetOifRegisterAsBytes(oifReg, upper, lower);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                result = bool.Parse(e.DataRead);
            }

            bool success = result;
            //this.ixoOifVen.SetOifRegisterAsBytes(oifReg, upper, lower);
            string errMsg = string.Format("Set OIF 0x{0:X02} failed.", oifReg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);
        }

        public short GetOifRegisterAsInt(short reg)
        {
            byte upper = 0;
            byte lower = 0;
            string command = "GetOifRegisterAsInt:" + reg.ToString();

            //Raises Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifVen.GetOifRegisterAsBytes(checked((byte)reg), ref upper, ref lower);
                OnChassisQuery(command, new byte[] { upper, lower, (byte)(result ? 1 : 0) }, this.Name, "");
            }
            else
            {
                byte[] byteResult = OnChassisQuery(command, this.Name, "");
                upper = byteResult[0];
                lower = byteResult[1];
                result = byteResult[2] == 0 ? false : true;
            }

            checkIxoliteReturnBool(result,
                "GetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);

            short ret = (short)((upper << 8) | lower);
            return ret;
        }

        public void SetOifRegisterAsInt(short reg, short val)
        {
            byte upperRegByte = (byte)((val & 0xff00) >> 8);
            byte lowerRegByte = (byte)(val & 0x00ff);

            string command = "SetOifRegisterAsInt:" + reg.ToString() + ":" + val.ToString();
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifVen.SetOifRegisterAsBytes(checked((byte)reg), upperRegByte, lowerRegByte);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                result = bool.Parse(e.DataRead);
            }

            checkIxoliteReturnBool(result,
                "SetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// All DAC transactions are using 2 8 bit registers
        /// We are expecting an unsigned 16 bit value, and break out the 
        /// upper and lower bytes from this data
        /// </summary>
        /// <param name="reg"></param>
        /// <param name="val"></param>
        public void SetOifRegisterAsUInt(short reg, UInt16 val)
        {
            byte upperRegByte = (byte)((val & 0xff00) >> 8);
            byte lowerRegByte = (byte)(val & 0x00ff);

            string command = "SetOifRegisterAsUInt:" + reg.ToString() + ":" + val.ToString();
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifVen.SetOifRegisterAsBytes(checked((byte)reg), upperRegByte, lowerRegByte);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                result = bool.Parse(e.DataRead);
            }

            checkIxoliteReturnBool(result,
                "SetOifRegisterAsBytes(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// Send a string to the device.
        /// </summary>
        /// <param name="reg">Register number</param>
        /// <param name="val">The string to set.</param>
        public void SetOifRegisterAsString(short reg, string val)
        {
            string command = "SetOifRegisterAsString:" + reg.ToString() + ":" + val;
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifVen.SetOifRegisterAsString(checked((byte)reg), ref val);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString(), command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                result = bool.Parse(e.DataRead);
            }
            checkIxoliteReturnBool(result,
                "SetOifRegisterAsString(0x" + reg.ToString("x") + ") failed.", TunableProtocol.OIF);
        }

        /// <summary>
        /// Get a string from the device.
        /// </summary>
        /// <param name="reg">Register Number.</param>
        /// <returns>The string from the device.</returns>
        public string GetOifRegisterAsString(short reg)
        {
            string response = "";
            string command = "GetOifRegisterAsString:" + reg.ToString();
            //Rasie Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);

            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = this.ixoOifVen.GetOifRegisterAsString(checked((byte)reg), ref response);
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, result.ToString() + "*|*" + response, command);
                OnChassisQuery(e);
            }
            else
            {
                ChassisQueryEventArgs e = new ChassisQueryEventArgs(this.Name, "", command);
                OnChassisQuery(e);
                string[] resultArray = e.DataRead.Split(new string[] { "*|*" }, StringSplitOptions.None);
                result = bool.Parse(resultArray[0]);
                response = resultArray[1];
            }

            bool success = result;
            string errMsg = string.Format("Get OIF String 0x{0:X02} failed.", reg);
            this.checkIxoliteReturnBool(success, errMsg, TunableProtocol.OIF);

            return response;
        }
        #endregion

        #region I�C command utility functions

        /// <summary>
        /// Perform an I�C transaction by calling the VB code.
        /// </summary>
        /// <remarks>Arrays will be shifted to/from 1-base internally.</remarks>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="cmdPayload">Command payload, after length byte. (Zero-based)</param>
        /// <returns>Response payload, after length byte. (Zero-based)</returns>
        private byte[] performI2cTransaction(byte cmd, params byte[] cmdPayload)
        {
            /* Prepare command payload by moving it into a 1-based array. */
            byte[] shiftedCmdPayload = new byte[cmdPayload.Length + 1];
            Array.Copy(cmdPayload, 0, shiftedCmdPayload, 1, cmdPayload.Length);

            /* Prepare buffer for response. */
            byte[] respPayload = new byte[250];
            byte actualLen = 0;

            //Rasies Chassis Online Check Event to get Online value.
            ChassisOnlineCheckEventArgs chassisOnlineCheckEventArgs = new ChassisOnlineCheckEventArgs(this.Name);
            OnChassisOnlineCheck(chassisOnlineCheckEventArgs);
            string command = "performI2cTransaction:" + cmd.ToString();
            bool result;
            if (chassisOnlineCheckEventArgs.Online)
            {
                result = ixoMsaVen.TransactionWithBytes(
                cmd, shiftedCmdPayload,
                ref respPayload, ref actualLen);

                byte[] respArray = new byte[respPayload.Length + 2];
                Array.Copy(respPayload, 0, respArray, 2, respPayload.Length);
                respArray[0] = (byte)(result ? 1 : 0);
                respArray[1] = actualLen;
                OnChassisQuery(command, respArray, this.Name, "");
            }
            else
            {
                byte[] byteResult = OnChassisQuery(command, this.Name, "");
                result = byteResult[0] == 0 ? false : true;
                actualLen = byteResult[1];
                Array.Copy(byteResult, 2, respPayload, 0, respPayload.Length);
            }


            /* Call Ixolite. */
            bool success = result;
            string errMsg = String.Format("I2C command 0x{0:X02} failed.", cmd);
            checkIxoliteReturnBool(success, errMsg, TunableProtocol.MSA);

            /* Shift 1-based array back into a 0-based array. */
            byte[] trimResp = new byte[actualLen];
            Array.Copy(respPayload, 1, trimResp, 0, actualLen);
            return trimResp;
        }

        /// <summary>
        /// Send an MSA Set command with a sub command and an ascii string.
        /// </summary>
        /// <param name="cmd">Command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="ascStr">String holding only ascii 7bit characters.</param>
        public void SetI2cWithSubcmdAndString(byte cmd, byte subcmd, string ascStr)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = new byte[ascStr.Length + 1];
            cmdPayload[0] = subcmd;
            Encoding.ASCII.GetBytes(ascStr, 0, ascStr.Length, cmdPayload, 1);

            /* Perform transaction, ignoring response payload. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Send an MSA command witha  sub command and an ascii string padded to a given length.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="ascStr">String payload holding only 7-bit characters.</param>
        /// <param name="pad">Byte value to pad string with.</param>
        /// <param name="strLen">Target length of string+padding. (Not including subcmd byte.)</param>
        public void SetI2cWithSubcmdAndPaddedString(byte cmd, byte subcmd, string ascStr, byte pad, int strLen)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = new byte[strLen + 1];
            cmdPayload[0] = subcmd;

            /* If pad byte is non-zero (filled with zeros on new) fill remainder, from [1] with
             * pad bytes. */
            if (pad != 0)
            {
                for (int i = 1; i < cmdPayload.Length; ++i)
                {
                    cmdPayload[i] = pad;
                }
            }

            /* Copy 7bit ASCII form of string into cmdPayload. */
            Encoding.ASCII.GetBytes(ascStr, 0, ascStr.Length, cmdPayload, 1);

            /* Perform transaction, ignoring response payload. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Send an MSA Set command with byte array payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="payload">Set payload.</param>
        public void SetI2cWithBytes(byte cmd, params byte[] payload)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, payload);
        }

        /// <summary>
        /// Send an MSA command with sub command and array of bytes.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command byte.</param>
        /// <param name="payload">Additional payload.</param>
        public void SetI2cWithSubcmdAndBytes(byte cmd, byte subcmd, params byte[] payload)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare complete payload. */
            byte[] fullcmd = new byte[payload.Length + 1];
            fullcmd[0] = subcmd;
            Array.Copy(payload, 0, fullcmd, 1, payload.Length);

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, fullcmd);
        }

        /// <summary>
        /// Copy an array, reversing the order.
        /// </summary>
        /// <param name="src">Source array.</param>
        /// <param name="srcIndex">Index of src to start copying from.</param>
        /// <param name="dst">Destination array.</param>
        /// <param name="dstIndex">Index of dst to copy into.</param>
        /// <param name="count">Number of bytes.</param>
        private void reverseArrayCopy(byte[] src, int srcIndex, byte[] dst, int dstIndex, int count)
        {
            for (int bytePos = 0; bytePos < count; ++bytePos)
            {
                dst[dstIndex + bytePos] = src[srcIndex + (count - 1 - bytePos)];
            }
        }

        /// <summary>
        /// Convert an array of bytes in big-endian order into a signed integer. Will handle signed
        /// or unsigned values.
        /// </summary>
        /// <param name="beArray">Array of byte.</param>
        /// <param name="signExtend">True if array holds a signed value.</param>
        /// <returns>Converted signed integer.</returns>
        private int bigEndianByteArrayToSint(byte[] beArray, bool signExtend)
        {
            if (BitConverter.IsLittleEndian)
            {
                /* Create an int-sized array of zero bytes. */
                byte[] leArray = BitConverter.GetBytes((int)0);

                /* If signExtend and value is -ve (bit7 of MSB is 1),
                 * fill the array with 0xff so the resulting value is
                 * negative. Two's complement requires the the unused MSBs
                 * by filled with zeros for +ve values and 0xff for
                 * -ve values.*/
                if (signExtend && (beArray[0] >= 0x80))
                {
                    for (int i = 0; i < leArray.Length; ++i)
                    {
                        leArray[i] = 0xff;
                    }
                }

                /* Collect array lengths. */
                int leLength = leArray.Length;
                int beLength = beArray.Length;

                /* Reverse array into left hand side of leArray. */
                reverseArrayCopy(beArray, 0, leArray, 0, beLength);

                /* Return LE array converted back to int. */
                return BitConverter.ToInt32(leArray, 0);
            }
            else
            {
                return BitConverter.ToInt32(beArray, 0);
            }
        }

        /// <summary>
        /// Convert a signed integer into a big endian array of bytes. Will handle
        /// signed or unsigned values.
        /// </summary>
        /// <param name="value">Signed integer to convert.</param>
        /// <param name="beLength">Target length of array.</param>
        /// <param name="expectNegValue">True if -ve values should be expected.</param>
        /// <returns>Converted array.</returns>
        private byte[] sintToBigEndianByteArray(int value, int beLength, bool expectNegValue)
        {
            /* Check if values are in range. */
            if (beLength > 3)
            {
                throw new TunableModuleException(
                    "sintToBigEndianByteArray called with beLength of " + beLength + ". (Max 3)",
                    TunableProtocol.MSA);
            }

            /* n bytes -> 2 ** (8*n) */
            int maxAllowed = (1 << (beLength * 8)) - 1;
            int minAllowed = -maxAllowed - 1;

            if ((value < minAllowed) || (value > maxAllowed))
            {
                throw new TunableModuleException(
                    "intToBigEndianByteArray - " + value + " cannot fit in " + beLength + " bytes.",
                    TunableProtocol.MSA);
            }

            if (BitConverter.IsLittleEndian)
            {
                /* Collect bytes. Useful bytes will be to the left, LSB first. */
                byte[] leArray = BitConverter.GetBytes(value);
                int leLength = leArray.Length;

                /* Construct zero filled array for response. */
                byte[] beArray = new byte[beLength];

                /* Reverse left portion of leArray into beArray. */
                reverseArrayCopy(leArray, 0, beArray, 0, beLength);
                return beArray;
            }
            else
            {
                throw new TunableModuleException("Big Endian architures not supported.", TunableProtocol.MSA);
            }
        }

        /// <summary>
        /// Re-write a byte array with an additional byte at the beginning.
        /// </summary>
        /// <param name="b1">Byte to prefix.</param>
        /// <param name="arr2">Byte array to sufffix.</param>
        /// <returns>Combined array.</returns>
        private byte[] prefixToByteArray(byte b1, byte[] arr2)
        {
            /* Start a new array. */
            byte[] resp = new byte[arr2.Length + 1];

            /* Copy parts. */
            resp[0] = b1;
            arr2.CopyTo(resp, 1);

            /* Return. */
            return resp;
        }

        /// <summary>
        /// Re-write a byte array with two additional bytes at the beginning.
        /// </summary>
        /// <param name="b1">First byte to prefix.</param>
        /// <param name="b2">Second byte to prefix.</param>
        /// <param name="arr2">Byte array to sufffix.</param>
        /// <returns>Combined array.</returns>
        private byte[] prefixToByteArray(byte b1, byte b2, byte[] arr2)
        {
            /* Start a new array. */
            byte[] resp = new byte[arr2.Length + 2];

            /* Copy parts. */
            resp[0] = b1;
            resp[1] = b2;
            arr2.CopyTo(resp, 2);

            /* Return. */
            return resp;
        }

        /// <summary>
        /// Set an MSA value with a subcmd and an uint converted to BE byte array.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Subcmd byte.</param>
        /// <param name="value">Uint value to send.</param>
        /// <param name="nBytes">Number of bytes to send.</param>
        public void SetI2cWithSubcmdAndBigEndianUint(byte cmd, byte subcmd, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, sintToBigEndianByteArray(value, bytesInValue, false));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Perform an MSA Set command with a sub command, index byte and big endian
        /// unsigned integer payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="index">Index.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="nBytes">Number of bytes for value.</param>
        public void SetI2cWithSubcmdIndexAndBigEndianUint(byte cmd, byte subcmd, byte index, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, index, sintToBigEndianByteArray(value, bytesInValue, false));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Perform an MSA Set command with a sub command and big endian
        /// signed integer payload.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="value">Value to set.</param>
        /// <param name="nBytes">Number of bytes for value.</param>
        public void SetI2cWithSubcmdAndBigEndianSint(byte cmd, byte subcmd, int value, int bytesInValue)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Prepare command payload. */
            byte[] cmdPayload = prefixToByteArray(subcmd, sintToBigEndianByteArray(value, bytesInValue, true));

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, cmdPayload);
        }

        /// <summary>
        /// Perform an MSA Get command with zero command payload, returning specified number of bytes.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="n">Number of bytes expected.</param>
        /// <returns>Returned bytes.</returns>
        public byte[] GetI2cReturnNBytes(byte cmd, int n)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, new byte[0]);
            if (resp.Length != n)
            {
                throw new TunableModuleException(
                    "Module returned " + resp.Length + " bytes. (Expected " + n + ").",
                    TunableProtocol.MSA);
            }

            return resp;
        }

        /// <summary>
        /// Perform an MSA Get command, with sub command byte, returning an array of
        /// bytes, length checked.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="n">Number of bytes expected in payload.</param>
        /// <returns>Length checked array of bytes.</returns>
        public byte[] GetI2cWithSubcmdReturnNBytes(byte cmd, byte subcmd, int n)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd);
            if (resp.Length != n)
            {
                throw new TunableModuleException(
                    "Module returned " + resp.Length + " bytes. (Expected " + n + ").",
                    TunableProtocol.MSA);
            }

            return resp;
        }

        /// <summary>
        /// Perform an I2C get command with no command payload, returning response byte array.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <returns>Response byte array.</returns>
        public byte[] GetI2cReturnBytes(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            return performI2cTransaction(cmd, new byte[0]);
        }

        /// <summary>
        /// Perform an I2C get command with a subcommand, returning response string.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">MSA subcommand byte</param>
        /// <returns>Response byte array.</returns>
        public string GetI2cWithSubcmdReturnString(byte cmd, byte subcmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();
            byte[] resp = performI2cTransaction(cmd, subcmd);
            return Encoding.ASCII.GetString(resp);
        }

        /// <summary>
        /// Get an MSA register and treat response as a big endian uint.
        /// </summary>
        /// <param name="cmd">MSA command</param>
        /// <returns></returns>
        public int GetI2cReturnBigEndianUint(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Get response, convert to uint and return. */
            return bigEndianByteArrayToSint(performI2cTransaction(cmd, new byte[0]), false);
        }

        /// <summary>
        /// Get an MSA register with a sub command and return big endian unsigned integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdReturnBigEndianUint(byte cmd, byte subcmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd);
            return bigEndianByteArrayToSint(resp, false);
        }

        /// <summary>
        /// Get an MSA register with a sub command, index and return big endian unsigned integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="index">Index byte.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdAndIndexReturnBigEndianUint(byte cmd, byte subcmd, byte index)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            byte[] resp = performI2cTransaction(cmd, subcmd, index);
            return bigEndianByteArrayToSint(resp, false);
        }

        /// <summary>
        /// Get an MSA register with a sub command and return big endian signed integer.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        /// <param name="nBytes">Expected number of bytes.</param>
        /// <returns>Register value.</returns>
        public int GetI2cWithSubcmdReturnReturnBigEndianSint(byte cmd, byte subcmd, int nBytes)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, collect bytes. */
            byte[] resp = GetI2cWithSubcmdReturnNBytes(cmd, subcmd, nBytes);

            /* Transform into signed int. */
            return bigEndianByteArrayToSint(resp, true);
        }

        /// <summary>
        /// Perofrm an MSA command with no payload in either direction.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        public void SendI2cCommandNoPayload(byte cmd)
        {
            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, new byte[0]);
        }

        /// <summary>
        /// Send an MSA command with a sub command, but otherwise no
        /// payload in either direction.
        /// </summary>
        /// <param name="cmd">MSA command byte.</param>
        /// <param name="subcmd">Sub command.</param>
        public void SendI2cCommandWithSubcmd(byte cmd, byte subcmd)
        {

            //Make sure we are talking to the right device.
            restoreParallelCommsContext();

            /* Perform transaction, ignoring response. */
            performI2cTransaction(cmd, subcmd);
        }

        #endregion
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TunableModulesTester
{
    class Program
    {
        static void Main(string[] args)
        {
            MsaStd_Test test = new MsaStd_Test();
            test.Setup();
            DateTime start = DateTime.Now;
            try
            {
                test.T01_UnitSerialNumber();
//                test.T02_MiscTests();
  //              test.T03_TempTests();
//                test.T04_ControlRegTests();
    //            test.T05_TransactionWithBytes();
 //               test.T06_FiddleSOADac();
                test.T07_BlackBoxId();
                test.T08_TsffTemperature();
            }
            finally
            {
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press any key to exit");
            Console.Read();
            Environment.Exit(0);
        }
    }
}

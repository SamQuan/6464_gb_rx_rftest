using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.Instruments
{
    public class X2_MemoryMap
    {
        private X2_MemoryMap()
        {
        }

        public static X2_MemoryAddress GetMemoryAddress(X2_Command x2_command) 
        {
            switch (x2_command)
            {
                #region Standard XFP MSA Memory Map
                case X2_Command.IDENTIFIER:
                    return new X2_MemoryAddress(0, 0, 1);
                   
                case X2_Command.SIGNAL_CONDITIONER_CONTROL:
                    return new X2_MemoryAddress(0, 1, 1);

                case X2_Command.FIRST_FREQ_HI:
                    return new X2_MemoryAddress(0, 60, 2);

                case X2_Command.FIRST_FREQ_LO:
                    return new X2_MemoryAddress(0, 62, 2);

                case X2_Command.LAST_FREQ_HI:
                    return new X2_MemoryAddress(0, 64, 2); 

                case X2_Command.LAST_FREQ_LO:
                    return new X2_MemoryAddress(0, 66, 2);

                case X2_Command.FREQ_GRID:
                    return new X2_MemoryAddress(0, 68, 2);

                case X2_Command.ACCEPTABLE_BER:
                    return new X2_MemoryAddress(0, 70, 1);

                case X2_Command.ACTUAL_BER:
                    return new X2_MemoryAddress(0, 71, 1);
                    
                case X2_Command.WAVELENGTH_CTRL:
                    return new X2_MemoryAddress(0, 72, 2);

                case X2_Command.WAVELENGTH_ERR:
                    return new X2_MemoryAddress(0, 74, 2);
                
                case X2_Command.STANDARD_80:
                    return new X2_MemoryAddress(0, 80, 1);

                case X2_Command.LATCHED_RX_POWER_ALARM:
                    return new X2_MemoryAddress(0, 81, 1);
                case X2_Command.STANDARD_82:
                    return new X2_MemoryAddress(0, 82, 1);

                case X2_Command.STANDARD_84:
                    return new X2_MemoryAddress(0,84,1);

                case X2_Command.STANDARD_85:
                    return new X2_MemoryAddress(0, 85, 1);

                case X2_Command.PCB_TEMPERATURE:
                    return new X2_MemoryAddress(0, 96, 2);

                case X2_Command.TX_BIAS:
                    return new X2_MemoryAddress(0, 100, 2);

                case X2_Command.TX_POWER:
                    return new X2_MemoryAddress(0, 102, 2);

                case X2_Command.RX_POWER_STANDARD:
                    return new X2_MemoryAddress(0, 104, 2);

                case X2_Command.GENERAL_CONTROL_BYTE1:
                    return new X2_MemoryAddress(0, 110, 1);

                case X2_Command.GENERAL_CONTROL_BYTE2:
                    return new X2_MemoryAddress(0, 111, 1);

                case X2_Command.HOST_PASSWARD_MODIFY:
                    return new X2_MemoryAddress(0, 119, 4);

                case X2_Command.HOST_PASSWARD:
                    return new X2_MemoryAddress(0,123, 4);

                #endregion

                #region Memory Page 1

                case X2_Command.IdentifierType:
                    return new X2_MemoryAddress(1, 128, 1);

                case X2_Command.ExtendedIdentifierType:
                    return new X2_MemoryAddress(1, 129, 1);

                case X2_Command.ConnectorCode:
                    return new X2_MemoryAddress(1, 130, 1);

                case X2_Command.TransceiverCode:
                    return new X2_MemoryAddress(1, 131, 8);

                case X2_Command.EncodingCode:
                    return new X2_MemoryAddress(1, 139, 1);

                case X2_Command.BitRateMin:
                    return new X2_MemoryAddress(1, 140, 1);

                case X2_Command.BitRateMax:
                    return new X2_MemoryAddress(1, 141, 1);

                case X2_Command.LengthSMF:
                    return new X2_MemoryAddress(1, 142, 1);

                case X2_Command.LengthE50:
                    return new X2_MemoryAddress(1, 143, 1);

                case X2_Command.Length50:
                    return new X2_MemoryAddress(1, 144, 1);

                case X2_Command.Length625:
                    return new X2_MemoryAddress(1, 145, 1);

                case X2_Command.LengthCopper:
                    return new X2_MemoryAddress(1, 146, 1);

                case X2_Command.DeviceTechnology:
                    return new X2_MemoryAddress(1, 147, 1);

                case X2_Command.VendorName:
                    return new X2_MemoryAddress(1, 148, 16);

                case X2_Command.CDRRate:
                    return new X2_MemoryAddress(1, 164, 1);

                case X2_Command.VendorOUI:
                    return new X2_MemoryAddress(1, 165, 3);

                case X2_Command.VendorPartNo:
                    return new X2_MemoryAddress(1, 168, 16);

                case X2_Command.VendorRevision:
                    return new X2_MemoryAddress(1, 184, 2);

                case X2_Command.WavelengthNominal:
                    return new X2_MemoryAddress(1, 186, 2);

                case X2_Command.WavelengthTolerance:
                    return new X2_MemoryAddress(1, 188, 2);

                case X2_Command.MaxCaseTemp:
                    return new X2_MemoryAddress(1, 190, 1);

                case X2_Command.CC_Base:
                    return new X2_MemoryAddress(1, 191, 1);

                case X2_Command.PowerRequirement:
                    return new X2_MemoryAddress(1, 192, 4);

                case X2_Command.VendorSerialNumber:
                    return new X2_MemoryAddress(1, 196, 16);

                case X2_Command.VendorDateCode:
                    return new X2_MemoryAddress(1,212,8);
                
                case X2_Command.DiagnosticMonitorType:
                    return new X2_MemoryAddress(1, 220, 1);
                
                case X2_Command.EnhancedOptions:
                    return new X2_MemoryAddress(1, 221, 1);

                case X2_Command.AuxMonitoring:
                    return new X2_MemoryAddress(1, 222, 1);

                case X2_Command.CC_Ext:
                    return new X2_MemoryAddress(1, 223, 1);

                case X2_Command.VendorSpecificEEprom1:
                    return new X2_MemoryAddress(1, 224, 10);

                case X2_Command.PartCodeRevision:
                    return new X2_MemoryAddress(1, 234, 3); 

                case X2_Command.VendorSpecificEEprom2:
                    return new X2_MemoryAddress(1, 240, 16);

                
                #endregion
                #region Memory Page2
                case X2_Command.FrameType:
                    return new X2_MemoryAddress(2, 128, 1);
                case X2_Command.FDUType:
                    return new X2_MemoryAddress(2, 129, 1);
                case X2_Command.ModelID:
                    return new X2_MemoryAddress(2, 130, 24);
                case X2_Command.FDUTYPE_CLEI:
                    return new X2_MemoryAddress(2, 154, 1);
                case X2_Command.CLEI_Code:
                    return new X2_MemoryAddress(2, 155, 10);
                case X2_Command.ECI_In_BCD:
                    return new X2_MemoryAddress(2, 165, 3);
                case X2_Command.Reserved1:
                    return new X2_MemoryAddress(2, 168, 16);
                case X2_Command.Reserved2:
                    return new X2_MemoryAddress(2, 184, 16);
                case X2_Command.Reserved3:
                    return new X2_MemoryAddress(2, 200, 16);
                case X2_Command.Reserved4:
                    return new X2_MemoryAddress(2, 216, 13);
                case X2_Command.CN4200_Code:
                    return new X2_MemoryAddress(2, 229, 27);
                #endregion

                #region Memory Page 3
                #region Alarm Thresholds
                case X2_Command.ALARM_CASE_TEMP_HIGH:
                    return new X2_MemoryAddress(3, 130, 2);

                case X2_Command.ALARM_CASE_TEMP_LOW:
                    return new X2_MemoryAddress(3, 132, 2);

                case X2_Command.WARN_CASE_TEMP_HIGH:
                    return new X2_MemoryAddress(3, 134, 2);

                case X2_Command.WARN_CASE_TEMP_LOW:
                    return new X2_MemoryAddress(3, 136, 2);

                case X2_Command.ALARM_TX_POWER_HIGH:
                    return new X2_MemoryAddress(3, 154, 2);

                case X2_Command.ALARM_TX_POWER_LOW:
                    return new X2_MemoryAddress(3, 156, 2);

                case X2_Command.WARN_TX_POWER_HIGH:
                    return new X2_MemoryAddress(3, 158, 2);

                case X2_Command.WARN_TX_POWER_LOW:
                    return new X2_MemoryAddress(3, 160, 2);

                case X2_Command.ALARM_RX_POWER_HIGH:
                    return new X2_MemoryAddress(3, 162, 2);
                    
                case X2_Command.ALARM_RX_POWER_LOW:
                    return new X2_MemoryAddress(3, 164, 2);
                    
                case X2_Command.WARN_RX_POWER_HIGH:
                    return new X2_MemoryAddress(3, 166, 2);

                case X2_Command.WARN_RX_POWER_LOW:
                    return new X2_MemoryAddress(3, 168, 2);

                case X2_Command.ALARM_AUX1_HIGH:
                    return new X2_MemoryAddress(3, 170, 2);

                case X2_Command.ALARM_AUX1_LOW:
                    return new X2_MemoryAddress(3, 172, 2);

                case X2_Command.WARN_AUX1_HIGH:
                    return new X2_MemoryAddress(3, 174, 2);

                case X2_Command.WARN_AUX1_LOW:
                    return new X2_MemoryAddress(3, 176, 2);

                case X2_Command.ALARM_AUX2_HIGH:
                    return new X2_MemoryAddress(3, 178, 2);

                case X2_Command.ALARM_AUX2_LOW:
                    return new X2_MemoryAddress(3, 180, 2);

                case X2_Command.WARN_AUX2_HIGH:
                    return new X2_MemoryAddress(3, 182, 2);

                case X2_Command.WARN_AUX2_LOW:
                    return new X2_MemoryAddress(3, 184, 2);
               
                #endregion

                #region APD Control

                case X2_Command.RX_LOS_THRESH:
                    return new X2_MemoryAddress(3, 186, 2);

                case X2_Command.RX_PMON_CAL_0:
                    return new X2_MemoryAddress(3, 188, 2);

                case X2_Command.RX_PMON_CAL_1:
                    return new X2_MemoryAddress(3, 190, 2);

                case X2_Command.RX_PMON_CAL_2:
                    return new X2_MemoryAddress(3, 192, 2);

                case X2_Command.RX_PMON_CAL_3:
                    return new X2_MemoryAddress(3, 194, 2);

                case X2_Command.RX_PMON_CAL_4:
                    return new X2_MemoryAddress(3, 196, 2);

                case X2_Command.RX_PMON_CAL_5:
                    return new X2_MemoryAddress(3, 198, 2);

                case X2_Command.RX_PMON_CAL_6:
                    return new X2_MemoryAddress(3, 200, 2);

                case X2_Command.RX_PMON_CAL_7:
                    return new X2_MemoryAddress(3, 202, 2);

                case X2_Command.RX_PMON_CAL_8:
                    return new X2_MemoryAddress(3, 204, 2);

                case X2_Command.RX_PMON_CAL_9:
                    return new X2_MemoryAddress(3, 206, 2);

                case X2_Command.RX_PMON_CAL_10:
                    return new X2_MemoryAddress(3, 208, 2);

                case X2_Command.RX_PMON_CAL_11:
                    return new X2_MemoryAddress(3, 210, 2);

                case X2_Command.RX_PMON_CAL_12:
                    return new X2_MemoryAddress(3, 212, 2);

                case X2_Command.RX_PMON_CAL_13:
                    return new X2_MemoryAddress(3, 214, 2);

                case X2_Command.RX_PMON_CAL_14:
                    return new X2_MemoryAddress(3, 216, 2);

                case X2_Command.RX_PMON_CAL_15:
                    return new X2_MemoryAddress(3, 218, 2);

                case X2_Command.RX_LOOP_GAIN:
                    return new X2_MemoryAddress(3, 220, 2);

                case X2_Command.RX_BIAS_M3_DAC:
                    return new X2_MemoryAddress(3, 222, 2);

                case X2_Command.RX_BIAS_M8_DAC:
                    return new X2_MemoryAddress(3, 224, 2);

                case X2_Command.RX_TEMPCO_DAC:
                    return new X2_MemoryAddress(3, 226, 2);

                
                #endregion

                #region Temperature Control

                case X2_Command.LS_PID_SETPOINT_CELCIUS:
                    return new X2_MemoryAddress(3, 228, 2);

                case X2_Command.LS_PID_P_GAIN:
                    return new X2_MemoryAddress(3, 230, 2);

                case X2_Command.LS_PID_I_GAIN:
                    return new X2_MemoryAddress(3, 232, 2);

                case X2_Command.LS_PID_D_GAIN:
                    return new X2_MemoryAddress(3, 234, 2);

                case X2_Command.LS_PID_OVERTEMP_MARGIN:
                    return new X2_MemoryAddress(3, 236, 2);

                case X2_Command.LS_PID_OVERTEMP_DELAY:
                    return new X2_MemoryAddress(3, 238, 2);

                case X2_Command.MZ_PID_SETPOINT_CELCIUS:
                    return new X2_MemoryAddress(3, 240, 2);

                case X2_Command.MZ_PID_P_GAIN:
                    return new X2_MemoryAddress(3, 242, 2);

                case X2_Command.MZ_PID_I_GAIN:
                    return new X2_MemoryAddress(3, 244, 2);

                case X2_Command.MZ_PID_D_GAIN:
                    return new X2_MemoryAddress(3, 246, 2);

                case X2_Command.MZ_PID_OVERTEMP_MARGIN:
                    return new X2_MemoryAddress(3, 248, 2);

                case X2_Command.MZ_PID_OVERTEMP_DELAY:
                    return new X2_MemoryAddress(3, 250, 2);

                #endregion

                #region Tx Power Control

               // case X2_Command.LS_POWER_SET:
               //     return new X2_MemoryAddress(3, 252, 2);

                case X2_Command.LS_POWER_CTRL_SET:
                    return new X2_MemoryAddress(3, 252, 2);

                case X2_Command.LS_POWER_CTRL_GAIN:
                    return new X2_MemoryAddress(3, 254, 2);
                #endregion
                #endregion

                # region Memory page 4

                case X2_Command.CHAN_PARAM_WAVELENGTH:
                    return new X2_MemoryAddress(4,130, 2);

                case X2_Command.CHAN_LOCKER_POLARITY:
                    return new X2_MemoryAddress(4, 132, 2);
                                    
                case X2_Command.CHAN_PARAM_SWITCHES:
                    return new X2_MemoryAddress(4, 134, 2);
                
                case X2_Command.CHAN_PARAM_ODD:
                    return new X2_MemoryAddress(4,136, 2);
                
                case X2_Command.CHAN_PARAM_EVEN:
                    return new X2_MemoryAddress(4,138, 2);

                case X2_Command.CHAN_PARAM_GAIN:
                    return new X2_MemoryAddress(4, 140, 2);
                
                case X2_Command.CHAN_PARAM_REAR:
                    return new X2_MemoryAddress(4,142, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_CENTER:
                    return new X2_MemoryAddress(4,144, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_BOL:
                    return new X2_MemoryAddress(4,146, 2);
                
                case X2_Command.CHAN_PARAM_BFM_TRANS:
                    return new X2_MemoryAddress(4,148, 2);
                
                case X2_Command.CHAN_PARAM_BFM_REFL:
                    return new X2_MemoryAddress(4,150, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_ALARM_HIGH:
                    return new X2_MemoryAddress(4,152, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_ALARM_LOW:
                    return new X2_MemoryAddress(4,154, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_WARN_HIGH:
                    return new X2_MemoryAddress(4,156, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_WARN_LOW:
                    return new X2_MemoryAddress(4,158, 2);
                
                case X2_Command.CHAN_PARAM_PHASE_MON_BOL:
                    return new X2_MemoryAddress(4,160, 2);
                
                case X2_Command.CHAN_PARAM_LOCKER_SLOPE:
                    return new X2_MemoryAddress(4,162, 2);

                case X2_Command.CHAN_PARAM_SOA_LOCKER_ACTIVATE:
                    return new X2_MemoryAddress(4, 164, 2);

                case X2_Command.CHAN_PARAM_SOA_CALIBRATED_PWR:
                    return new X2_MemoryAddress(4, 166, 2);
                

                //don't exist in v04
                /*
                case X2_Command.CHAN_PARAM_SOA_CAL_0:
                    return new X2_MemoryAddress(4,160, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_1:
                    return new X2_MemoryAddress(4,162, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_2:
                    return new X2_MemoryAddress(4,164, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_3:
                    return new X2_MemoryAddress(4,166, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_4:
                    return new X2_MemoryAddress(4,168, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_5:
                    return new X2_MemoryAddress(4,170, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_6:
                    return new X2_MemoryAddress(4,172, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_7:
                    return new X2_MemoryAddress(4,174, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_8:
                    return new X2_MemoryAddress(4,176, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_9:
                    return new X2_MemoryAddress(4,178, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_10:
                    return new X2_MemoryAddress(4,180, 2);
                
                case X2_Command.CHAN_PARAM_SOA_CAL_11:
                    return new X2_MemoryAddress(4,182, 2);
                */
                case X2_Command.CHAN_PARAM_POW_MON_SLOPE:
                    return new X2_MemoryAddress(4, 188, 2);

                case X2_Command.CHAN_PARAM_POW_MON_OFFSET:
                    return new X2_MemoryAddress(4, 190, 2);

                case X2_Command.CHAN_PARAM_MZ_IMB_32:
                    return new X2_MemoryAddress(4, 192, 2);

                case X2_Command.CHAN_PARAM_MZ_IMB_33:
                    return new X2_MemoryAddress(4, 194, 2);

                case X2_Command.CHAN_PARAM_MZ_BIAS_34:
                    return new X2_MemoryAddress(4, 196, 2);

                case X2_Command.CHAN_PARAM_MZ_BIAS_35:
                    return new X2_MemoryAddress(4, 198, 2);

                case X2_Command.CHAN_PARAM_MZ_PW:
                    return new X2_MemoryAddress(4, 200, 2);

                case X2_Command.CHAN_PARAM_MZ_MOD:
                    return new X2_MemoryAddress(4, 202, 2);

                case X2_Command.CHAN_PARAM_MZ_X_POINT:
                    return new X2_MemoryAddress(4, 204, 2);

                case X2_Command.CHAN_PARAM_MZ_SLOPE:
                    return new X2_MemoryAddress(4, 206, 2);

                case X2_Command.CHAN_PARAM_INDEX:
                    return new X2_MemoryAddress(4, 208, 2);
                #endregion

                #region Memory Page 5

                case X2_Command.RX_BIAS_M3_VOLTS:
                    return new X2_MemoryAddress(5, 135, 2);

                case X2_Command.RX_BIAS_M8_VOLTS:
                    return new X2_MemoryAddress(5, 132, 2);

                case X2_Command.RX_POWER_MON_RAW:
                    return new X2_MemoryAddress(5, 134, 2);

                case X2_Command.RX_STATUS:
                    return new X2_MemoryAddress(5, 136, 2);

                case X2_Command.RX_VOLTS:
                    return new X2_MemoryAddress(5, 138, 2);

                case X2_Command.RX_TEMP:
                    return new X2_MemoryAddress(5, 140, 2);

                case X2_Command.RX_POWER:
                    return new X2_MemoryAddress(5, 142, 2);

                case X2_Command.DEV_DAC_OFFSET:
                    return new X2_MemoryAddress(5, 144, 2);

                case X2_Command.DEV_DAC_ODD:
                    return new X2_MemoryAddress(5, 146, 2);

                case X2_Command.DEV_DAC_PHASE:
                    return new X2_MemoryAddress(5, 148, 2);

                case X2_Command.DEV_DAC_EVEN:
                    return new X2_MemoryAddress(5, 150, 2);

                case X2_Command.DEV_DAC_GAIN:
                    return new X2_MemoryAddress(5, 152, 2);

                case X2_Command.DEV_DAC_SOA:
                    return new X2_MemoryAddress(5, 154, 2);

                case X2_Command.DEV_DAC_REAR:
                    return new X2_MemoryAddress(5, 156, 2);

                case X2_Command.DEV_DAC_LS_TEC:
                    return new X2_MemoryAddress(5, 158, 2);

                case X2_Command.DEV_DAC_APD_BIAS:
                    return new X2_MemoryAddress(5, 160, 2);

                case X2_Command.DEV_DAC_MZ_PW:
                    return new X2_MemoryAddress(5, 162, 2);

                case X2_Command.DEV_DAC_MZ_BIAS_34:
                    return new X2_MemoryAddress(5, 164, 2);

                case X2_Command.DEV_DAC_MZ_BIAS_35:
                    return new X2_MemoryAddress(5, 166, 2);

                case X2_Command.DEV_DAC_MZ_MOD:
                    return new X2_MemoryAddress(5, 168, 2);

                case X2_Command.DEV_DAC_RX_LOS:
                    return new X2_MemoryAddress(5, 170, 2);

                case X2_Command.DEV_DAC_UNUSED:
                    return new X2_MemoryAddress(5, 172, 2);

                case X2_Command.DEV_DAC_MZ_TEC:
                    return new X2_MemoryAddress(5, 174, 2);

                case X2_Command.DEV_DAC_BFM_TRANS:
                    return new X2_MemoryAddress(5, 176, 2);

                case X2_Command.DEV_DAC_BFM_REFL:
                    return new X2_MemoryAddress(5, 178, 2);

                case X2_Command.DEV_DAC_MZ_IMB_33:
                    return new X2_MemoryAddress(5, 180, 2);

                case X2_Command.DEV_DAC_MZ_IMB_32:
                    return new X2_MemoryAddress(5, 182, 2);

                case X2_Command.DEV_ADC_BFM_TRAN:
                    return new X2_MemoryAddress(5, 184, 2);

                case X2_Command.DEV_ADC_BFM_REFL:
                    return new X2_MemoryAddress(5, 186, 2);

                case X2_Command.DEV_ADC_LS_TEMP:
                    return new X2_MemoryAddress(5, 188, 2);

                case X2_Command.DEV_ADC_LS_POWER:
                    return new X2_MemoryAddress(5, 190, 2);

                case X2_Command.DEV_ADC_LS_PHASE:
                    return new X2_MemoryAddress(5, 192, 2);

                case X2_Command.DEV_ADC_MZ_TEMP:
                    return new X2_MemoryAddress(5, 194, 2);

                case X2_Command.DEV_ADC_MZ_MMI:
                    return new X2_MemoryAddress(5, 196, 2);

                case X2_Command.DEV_ADC_RX_POWER:
                    return new X2_MemoryAddress(5, 198, 2);

                case X2_Command.DEV_ADC_LS_TEC_IMON:
                    return new X2_MemoryAddress(5, 200, 2);

                case X2_Command.DEV_ADC_CASE_TEMP:
                    return new X2_MemoryAddress(5, 202, 2);

                case X2_Command.DEV_ADC_PSU_VCC5:
                    return new X2_MemoryAddress(5, 204, 2);

                case X2_Command.DEV_ADC_PSU_VCC3:
                    return new X2_MemoryAddress(5, 206, 2);

                case X2_Command.DEV_ADC_PSU_VCC2:
                    return new X2_MemoryAddress(5, 208, 2);

                case X2_Command.DEV_ADC_PSU_VEE5:
                    return new X2_MemoryAddress(5, 210, 2);

                case X2_Command.DEV_ADC_RX_TEMP:
                    return new X2_MemoryAddress(5, 212, 2);

                case X2_Command.LS_PID_SETPOINT_ADC:
                    return new X2_MemoryAddress(5, 214, 2);

                case X2_Command.LS_PID_TEMP_CELCIUS:
                    return new X2_MemoryAddress(5, 216, 2);

                case X2_Command.LS_PID_ENABLE:
                    return new X2_MemoryAddress(5, 218, 2);

                case X2_Command.MZ_PID_SETPOINT_ADC:
                    return new X2_MemoryAddress(5, 220, 2);

                case X2_Command.MZ_PID_TEMP_CELCIUS:
                    return new X2_MemoryAddress(5, 222, 2);

                case X2_Command.MZ_PID_ENABLE:
                    return new X2_MemoryAddress(5, 224, 2);

                case X2_Command.PID_STATUS:
                    return new X2_MemoryAddress(5, 226, 2);

                case X2_Command.MZ_AUTOX_SET_POINT:
                    return new X2_MemoryAddress(5, 228, 2);
                    
                case X2_Command.MZ_AUTOX_ENABLE:
                    return new X2_MemoryAddress(5, 230, 2);

                case X2_Command.MZ_AUTOX_SLOPE:
                    return new X2_MemoryAddress(5, 232, 2);

                case X2_Command.CHAN_STORE_IDX:
                    return new X2_MemoryAddress(5, 234, 2);

                //case X2_Command.LockerEnable:
                //    return new X2_MemoryAddress(5, 184, 2);

                //case X2_Command.Autolock:
                //    return new X2_MemoryAddress(5, 186, 2);
                case X2_Command.BFM_LOCKER_ENABLE:
                    return new X2_MemoryAddress(5, 236, 2);

                case X2_Command.BFM_LOCKER_SLOPE:
                    return new X2_MemoryAddress(5, 238, 2);

                case X2_Command.BFM_INIT_AUTOLOCK:
                    return new X2_MemoryAddress(5, 240, 2);

                case X2_Command.DSDBR_FRONT_SWITCHES:
                    return new X2_MemoryAddress(5, 242, 2);

                case X2_Command.EDC_REGISTER_ADDRESS:
                    return new X2_MemoryAddress(5, 244, 2);

                case X2_Command.EDC_REGISTER_VALUE:
                    return new X2_MemoryAddress(5, 246, 2);

                case X2_Command.EDC_ENABLE_PATTERN_GEN:
                    return new X2_MemoryAddress(5, 248, 2);
                    
                case X2_Command.LS_POWER_CTRL_ENABLE:
                    return new X2_MemoryAddress(5, 250, 2);

                case X2_Command.LS_POWER_MON:
                    return new X2_MemoryAddress(5, 252, 2);

                case X2_Command.LS_POWER_RAW:
                    return new X2_MemoryAddress(5, 254, 2);

                #endregion
                #region Memory Page 6
                case X2_Command.PartCode:
                    return new X2_MemoryAddress(6, 208, 16);

                case X2_Command.FirmwareDate:
                    return new X2_MemoryAddress(6, 224, 20);

                case X2_Command.FirmwareRevision:
                    return new X2_MemoryAddress(6, 246, 8);

                case X2_Command.TcmzSerialNumber:
                    return new X2_MemoryAddress(6, 128, 16);

                case X2_Command.RxSerialNumber:
                    return new X2_MemoryAddress(6, 144, 16);

                case X2_Command.PcbSerialNumber:
                    return new X2_MemoryAddress(6, 160, 16);
                #endregion

                #region Memory Page 8
                case X2_Command.VitEDCControlParam132:
                    return new X2_MemoryAddress(8, 132, 2);
                case X2_Command.VitEDCControlParam134:
                    return new X2_MemoryAddress(8, 134, 2);
                #endregion
                default:
                    break;
            }
            return null;
        }
    }

    public enum X2_Command
    {
        #region Standard
        IDENTIFIER,
        SIGNAL_CONDITIONER_CONTROL,
        ACCEPTABLE_BER,
        ACTUAL_BER,
        WAVELENGTH_CTRL,
        WAVELENGTH_ERR,
        PCB_TEMPERATURE,
        STANDARD_80,
        LATCHED_RX_POWER_ALARM,
        STANDARD_82,
        STANDARD_84,
        STANDARD_85,
        TX_BIAS,
        TX_POWER,
        RX_POWER_STANDARD,
        GENERAL_CONTROL_BYTE1,
        GENERAL_CONTROL_BYTE2,
        HOST_PASSWARD_MODIFY,
        HOST_PASSWARD,
        FIRST_FREQ_HI,
        FIRST_FREQ_LO,
        LAST_FREQ_HI,
        LAST_FREQ_LO,
        FREQ_GRID,
        #endregion

        #region Page 1
        IdentifierType,
        ExtendedIdentifierType,
        ConnectorCode,
        TransceiverCode,
        EncodingCode,
        BitRateMin,
        BitRateMax,
        LengthSMF,
        LengthE50,
        Length50,
        Length625,
        LengthCopper,
        DeviceTechnology,
        VendorName,
        CDRRate,
        VendorOUI,
        VendorPartNo,
        VendorRevision,
        WavelengthNominal,
        WavelengthTolerance,
        MaxCaseTemp,
        CC_Base,
        PowerRequirement,
        VendorSerialNumber,
        VendorDateCode,
        DiagnosticMonitorType,
        EnhancedOptions,
        AuxMonitoring,
        CC_Ext,
        VendorSpecificEEprom1,
        PartCodeRevision,
        VendorSpecificEEprom2,
        
        FirmwareVersion,
        #endregion

        #region Page 2
        FrameType,
        FDUType,
        ModelID,
        FDUTYPE_CLEI,
        CLEI_Code,
        ECI_In_BCD,
        Reserved1,
        Reserved2,
        Reserved3,
        Reserved4,
        CN4200_Code,
        #endregion

        #region Page 3
        ALARM_CASE_TEMP_HIGH,
        ALARM_CASE_TEMP_LOW,
        WARN_CASE_TEMP_HIGH,
        WARN_CASE_TEMP_LOW,
        ALARM_TX_POWER_HIGH,
        ALARM_TX_POWER_LOW,
        WARN_TX_POWER_HIGH,
        WARN_TX_POWER_LOW,
        ALARM_RX_POWER_HIGH,
        ALARM_RX_POWER_LOW,
        WARN_RX_POWER_HIGH,
        WARN_RX_POWER_LOW,
        ALARM_AUX1_HIGH,
        ALARM_AUX1_LOW,
        WARN_AUX1_HIGH,
        WARN_AUX1_LOW,
        ALARM_AUX2_HIGH,
        ALARM_AUX2_LOW,
        WARN_AUX2_HIGH,
        WARN_AUX2_LOW,
        RX_LOS_THRESH,
        RX_PMON_CAL_0,
        RX_PMON_CAL_1,
        RX_PMON_CAL_2,
        RX_PMON_CAL_3,
        RX_PMON_CAL_4,
        RX_PMON_CAL_5,
        RX_PMON_CAL_6,
        RX_PMON_CAL_7,
        RX_PMON_CAL_8,
        RX_PMON_CAL_9,
        RX_PMON_CAL_10,
        RX_PMON_CAL_11,
        RX_PMON_CAL_12,
        RX_PMON_CAL_13,
        RX_PMON_CAL_14,
        RX_PMON_CAL_15,
        RX_BIAS_M3_DAC,
        RX_BIAS_M8_DAC,
        RX_TEMPCO_DAC,
        RX_LOOP_GAIN,
        LS_PID_SETPOINT_CELCIUS,
        LS_PID_P_GAIN,
        LS_PID_I_GAIN,
        LS_PID_D_GAIN,
        LS_PID_OVERTEMP_MARGIN,
        LS_PID_OVERTEMP_DELAY,
        MZ_PID_SETPOINT_CELCIUS,
        MZ_PID_P_GAIN,
        MZ_PID_I_GAIN,
        MZ_PID_D_GAIN,
        MZ_PID_OVERTEMP_MARGIN,
        MZ_PID_OVERTEMP_DELAY,
        LS_POWER_SET,              // Doesn't exist in v0.4 map, remove it? Andy.Li
        LS_POWER_CTRL_SET,
        LS_POWER_CTRL_GAIN,
        #endregion

        #region Page 4
        CHAN_PARAM_WAVELENGTH,
        CHAN_LOCKER_POLARITY,         //V04
        CHAN_PARAM_SWITCHES,
        CHAN_PARAM_ODD,
        CHAN_PARAM_EVEN,
        CHAN_PARAM_GAIN,
        CHAN_PARAM_REAR,
        CHAN_PARAM_PHASE_CENTER,
        CHAN_PARAM_PHASE_BOL,
        CHAN_PARAM_BFM_TRANS,
        CHAN_PARAM_BFM_REFL,
        CHAN_PARAM_PHASE_ALARM_HIGH,
        CHAN_PARAM_PHASE_ALARM_LOW,
        CHAN_PARAM_PHASE_WARN_HIGH,
        CHAN_PARAM_PHASE_WARN_LOW,
        CHAN_PARAM_PHASE_MON_BOL,
        CHAN_PARAM_LOCKER_SLOPE,
        CHAN_PARAM_SOA_LOCKER_ACTIVATE,//V04
        CHAN_PARAM_SOA_CALIBRATED_PWR, //V04
        CHAN_PARAM_SOA_CAL_0,          //Don't exits from here
        CHAN_PARAM_SOA_CAL_1,
        CHAN_PARAM_SOA_CAL_2,
        CHAN_PARAM_SOA_CAL_3,
        CHAN_PARAM_SOA_CAL_4,
        CHAN_PARAM_SOA_CAL_5,
        CHAN_PARAM_SOA_CAL_6,
        CHAN_PARAM_SOA_CAL_7,
        CHAN_PARAM_SOA_CAL_8,
        CHAN_PARAM_SOA_CAL_9,
        CHAN_PARAM_SOA_CAL_10,
        CHAN_PARAM_SOA_CAL_11,        //to here in v04
        CHAN_PARAM_POW_MON_SLOPE,
        CHAN_PARAM_POW_MON_OFFSET,
        CHAN_PARAM_MZ_IMB_32,
        CHAN_PARAM_MZ_IMB_33,
        CHAN_PARAM_MZ_BIAS_34,
        CHAN_PARAM_MZ_BIAS_35,
        CHAN_PARAM_MZ_PW,
        CHAN_PARAM_MZ_MOD,
        CHAN_PARAM_MZ_X_POINT,
        CHAN_PARAM_MZ_SLOPE,
        CHAN_PARAM_INDEX,
        #endregion

        #region Page 5
        RX_BIAS_M3_VOLTS,
        RX_BIAS_M8_VOLTS,
        RX_POWER_MON_RAW,
        RX_STATUS,
        RX_VOLTS,
        RX_TEMP,
        RX_POWER,
        DEV_DAC_OFFSET,
        DEV_DAC_ODD,
        DEV_DAC_PHASE,
        DEV_DAC_EVEN,
        DEV_DAC_GAIN,
        DEV_DAC_SOA,
        DEV_DAC_REAR,
        DEV_DAC_LS_TEC,
        DEV_DAC_APD_BIAS,
        DEV_DAC_MZ_PW,
        DEV_DAC_MZ_BIAS_34,
        DEV_DAC_MZ_BIAS_35,
        DEV_DAC_MZ_MOD,
        DEV_DAC_RX_LOS,
        DEV_DAC_UNUSED,
        DEV_DAC_MZ_TEC,
        DEV_DAC_BFM_TRANS,
        DEV_DAC_BFM_REFL,
        DEV_DAC_MZ_IMB_33,
        DEV_DAC_MZ_IMB_32,
        DEV_ADC_BFM_TRAN,
        DEV_ADC_BFM_REFL,
        DEV_ADC_LS_TEMP,
        DEV_ADC_LS_POWER,
        DEV_ADC_LS_PHASE,
        DEV_ADC_MZ_TEMP,
        DEV_ADC_MZ_MMI,
        DEV_ADC_RX_POWER,
        DEV_ADC_LS_TEC_IMON,
        DEV_ADC_CASE_TEMP,
        DEV_ADC_PSU_VCC5,
        DEV_ADC_PSU_VCC3,
        DEV_ADC_PSU_VCC2,
        DEV_ADC_PSU_VEE5,
        DEV_ADC_RX_TEMP,
        LS_PID_SETPOINT_ADC,
        LS_PID_TEMP_CELCIUS,
        LS_PID_ENABLE,
        MZ_PID_SETPOINT_ADC,
        MZ_PID_TEMP_CELCIUS,
        MZ_PID_ENABLE,
        PID_STATUS,
        MZ_AUTOX_SET_POINT,//v04
        MZ_AUTOX_ENABLE,
        MZ_AUTOX_SLOPE,    //v04
        CHAN_STORE_IDX,
        BFM_LOCKER_ENABLE, //V04
        BFM_LOCKER_SLOPE,  //V04
        BFM_INIT_AUTOLOCK, //V04

        LockerEnable,      //dosent exist in v04
        Autolock,          //dosent exist in v04

        DSDBR_FRONT_SWITCHES,//V04
        EDC_REGISTER_ADDRESS,          //V04
        EDC_REGISTER_VALUE,      //V04
        EDC_ENABLE_PATTERN_GEN,       //V04
        LS_POWER_CTRL_ENABLE,
        LS_POWER_MON,
        LS_POWER_RAW,
        #endregion

        #region Page 6
        PartCode,
        FirmwareDate,
        FirmwareRevision,
        TcmzSerialNumber,
        RxSerialNumber,
        PcbSerialNumber,
        #endregion

        #region Page 8
        VitEDCControlParam132,
        VitEDCControlParam134,
        #endregion

        FIRST_CHANNEL_FREQ_THZ,
        FIRST_CHANNEL_FREQ_GHZ
    }

    public class X2_MemoryAddress
    {
        public X2_MemoryAddress(int page, int address, int length)
        {
            this.page = page;
            this.address = address;
            this.length = length;
        }
        public readonly int page;
        public readonly int address;
        public readonly int length;
    }
}

// [Copyright]
//
// Bookham X2 device driver for test software
// Bookham.TestSolution.Instruments
//
// Inst_X2.cs
//
// Author: mark.fullalove,andy.li,cypress chen 2008
// Design: [Reference design documentation]

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.Instruments
{

    public struct TX_Bias_State
    {
        public TX_Bias_State(bool highAlarm, bool lowAlarm, bool highWarning, bool lowWarning)
        {
            _highAlarm = highAlarm;
            _lowAlarm = lowAlarm;
            _highWarning = highWarning;
            _lowWarning = lowWarning;
        }

        private bool _highAlarm;
        public bool HighAlarm
        {
            get { return _highAlarm; }
        }

        private bool _lowAlarm;
        public bool LowAlarm
        {
            get { return _lowAlarm; }
        }

        private bool _highWarning;
        public bool HighWarning
        {
            get { return _highWarning; }
        }

        private bool _lowWarning;
        public bool LowWarning
        {
            get { return _lowWarning; }
        }

    }

    public struct Temperature_State
    {
        public Temperature_State(bool highAlarm, bool lowAlarm, bool highWarning, bool lowWarning)
        {
            _highAlarm = highAlarm;
            _lowAlarm = lowAlarm;
            _highWarning = highWarning;
            _lowWarning = lowWarning;
        }

        private bool _highAlarm;
        public bool HighAlarm
        {
            get { return _highAlarm; }
        }

        private bool _lowAlarm;
        public bool LowAlarm
        {
            get { return _lowAlarm; }
        }

        private bool _highWarning;
        public bool HighWarning
        {
            get { return _highWarning; }
        }

        private bool _lowWarning;
        public bool LowWarning
        {
            get { return _lowWarning; }
        }

    }

    public struct TX_Power_State
    {
        public TX_Power_State(bool highAlarm, bool lowAlarm, bool highWarning, bool lowWarning)
        {
            _highAlarm = highAlarm;
            _lowAlarm = lowAlarm;
            _highWarning = highWarning;
            _lowWarning = lowWarning;
        }

        private bool _highAlarm;
        public bool HighAlarm
        {
            get { return _highAlarm; }
        }

        private bool _lowAlarm;
        public bool LowAlarm
        {
            get { return _lowAlarm; }
        }

        private bool _highWarning;
        public bool HighWarning
        {
            get { return _highWarning; }
        }

        private bool _lowWarning;
        public bool LowWarning
        {
            get { return _lowWarning; }
        }

    }

    public class Inst_X2 : Instrument,
                                IBlackBoxIdentity,
                                IMSA_XFP,
                                IDsdbrLaserSetup,
                                ITcmzLaserSetup,
                                IReceiverSetup
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_X2(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_X2",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_X2)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Hardware_Unknown";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //do nothing...
                        
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_X2 instrumentChassis;
        public Chassis_X2 InstrumentChasis
        {
            get
            {
                return instrumentChassis;
            }
        }

        private string ReadFromChassis(X2_Command x2Command)
        {
            X2_MemoryAddress memoryAddress = X2_MemoryMap.GetMemoryAddress(x2Command);
            string request = "R" + ToHex((byte)memoryAddress.page) + "," + ToHex((byte)memoryAddress.address) + "," + ToHex((byte)memoryAddress.length) + "?";
            this.instrumentChassis.WriteLine(request, this);
            
            string response = "";
            int delayCount = 0;
            while (response.Length == 0 && delayCount < 100)
            {
                System.Threading.Thread.Sleep(100);
                response = this.instrumentChassis.Read(this);
                delayCount++;
            }
                        
            return ValidateResponse(request, response);            
        }

        private void WriteToChassis(X2_Command x2Command, string data)
        {
            X2_MemoryAddress memoryAddress = X2_MemoryMap.GetMemoryAddress(x2Command);
            string request = "W" + ToHex((byte)memoryAddress.page) + "," + ToHex((byte)memoryAddress.address) + "," + data;
            this.instrumentChassis.WriteLine(request, this);
            string response = this.instrumentChassis.ReadLine(this);

            string responseData = ValidateResponse(request, response);
            if (!responseData.Equals(data) && !responseData.Equals(""))
                throw new Exception("Invalid response to data write. Request = '" + data + "', response = '" + responseData + "'");
            System.Threading.Thread.Sleep(20);
        }

        private string ValidateResponse(string request, string response)
        {   
            // The response and request must start with the same header character
            response = response.Trim('\0');
            if (!response.StartsWith(request.Substring(0, 1)))
                throw new Exception("Invalid response to command '" + request + "' , Response starts with '" + response.Substring(0, 1) + "'");

            // Status byte must be "0"
            string statusByte = response.Substring(1, 1);
            if ( ! statusByte.Equals("0") )
                throw new Exception("Error processing command '" + request + "' , Response is '" + response + "'");

            // The actual response is the remainder of the message ignoring the first three characters ( typically "R0," )
            // The format of the data field is 2-digit hex values (upper case) for each byte, with no separators
            if (response == "W0\r"||response=="P0"||response=="W0") return "";
            return response.TrimEnd('\r').Substring(3);
        }

        private string HexToString(string hexString)
        {
            if (hexString.Length % 2 != 0)
                throw new Exception("Response string does not contain 2 byte pairs");

            StringBuilder sb  = new StringBuilder(hexString.Length / 2);
            for ( int index = 0 ; index < hexString.Length ; index+=2 )
            {
                sb.Append((char)(Convert.ToByte(hexString.Substring(index, 2), 16)));
            }
            return sb.ToString();
        }

        private double HexToDouble(string hexString)
        {
            return Convert.ToDouble(hexString);
        }

        private byte HexToByte(string hexString)
        {
            byte a = Convert.ToByte(hexString.Substring(0,2),16);
            return a;
        }

        private long HexToLong(string hexString)
        {
            return Convert.ToInt64(hexString, 16);
        }

        private int HexToInt(string hexString)
        {
            return Convert.ToInt16(hexString, 16);
        }
        private int HexToUInt(string hexString)
        {
            return Convert.ToUInt16(hexString, 16);
        }
        private String ToHex(byte byteToConvert)
        {
            string result=byteToConvert.ToString("x").PadLeft(2,'0');
            return result.Remove(0, result.Length - 2).ToUpper();
        }

        private String ToHex(int intToConvert)
        {
            string result = intToConvert.ToString("x").PadLeft(4, '0');
            return result.Remove(0, result.Length - 4).ToUpper();
        }
        
        private String ToHex(long lngToConvert)
        {
            string result = lngToConvert.ToString("x").PadLeft(8, '0');
            return result.Remove(0, result.Length - 8).ToUpper();
        }
        private String ToHex(string stringToConvert)
        {
            StringBuilder sb = new StringBuilder(stringToConvert.Length * 2);
            for (int i = 0; i < stringToConvert.Length; i++)
            {
                sb.Append(ToHex((byte)stringToConvert[i]));
            }
            return sb.ToString().ToUpper();
        }
        #endregion
                
        #region IBlackBoxIdentity Members

        public string GetBoardSerialNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.PcbSerialNumber));
        }

        public string GetLaserSerialNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.TcmzSerialNumber));
        }

        public string GetLaserTechnology()
        {
            return HexToString(ReadFromChassis(X2_Command.DeviceTechnology));
        }

        public string GetPartNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorPartNo));
        }

        public string GetRxSerialNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.RxSerialNumber));
        }

        public string GetSupplier()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorName));
        }

        public DateTime GetUnitBuildDate()
        {
            string dateAsString =  HexToString(ReadFromChassis(X2_Command.VendorDateCode));
            int year = int.Parse(dateAsString.Substring(0, 2))+2000;
            int mon = int.Parse(dateAsString.Substring(2, 2));
            int date = int.Parse(dateAsString.Substring(4, 2));

            return new DateTime(year, mon, date, 12, 0, 0);
        }   

        public string GetUnitSerialNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorSerialNumber));
        }

        public void SetBoardSerialNumber(string serno)
        {
            WriteToChassis(X2_Command.PcbSerialNumber,ToHex(serno));
        }

        public void SetLaserSerialNumber(string serno)
        {
            WriteToChassis(X2_Command.TcmzSerialNumber, ToHex(serno));
        }

        public void SetLaserTechnology(string laserTech)
        {
            WriteToChassis(X2_Command.DeviceTechnology,ToHex(laserTech));
        }

        public void SetPartNumber(string partno)
        {
            WriteToChassis(X2_Command.VendorPartNo,ToHex(partno));
        }

        public void SetRxSerialNumber(string serno)
        {
            WriteToChassis(X2_Command.RxSerialNumber,ToHex(serno));
        }

        public void SetUnitBuildDate(DateTime bdate)
        {
            string dateString = bdate.ToString("yyMMdd");
            WriteToChassis(X2_Command.VendorDateCode, ToHex(dateString));
        }

        public void SetUnitSerialNumber(string serno)
        {
            WriteToChassis(X2_Command.VendorSerialNumber, ToHex(serno));
        }

        #endregion
        
        #region IDsdbrLaserSetup Members

        public void DoAutoLockerSetup()
        {
            int i = 1;
            WriteToChassis(X2_Command.BFM_INIT_AUTOLOCK, ToHex(i));
        }

        public void EnterLaserSetupMode()
        {
            this.EnterPassword(this.defaultPassword);
        }

        public int GetBOLPhaseDAC()
        {
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_BOL)));
        }

        public int GetDitherDAC()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetEOLPhaseADC_Max()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_HIGH));
        }

        public int GetEOLPhaseADC_Min()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_LOW));
        }

        public int GetFrontEvenDAC()
        {
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_EVEN)));
        }

        public int GetFrontOddDAC()
        {
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_ODD)));
        }

        public int GetFrontSectionPair()
        {
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.DSDBR_FRONT_SWITCHES)));
        }

        public int GetGainDAC()
        {
            //return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_GAIN);
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_GAIN)));
        }

        public int GetLaserPowerMon_ADC(int n)
        {
            int val = 0;
            for (int i = 0; i < n; i++)
            {
                val += GetLaserPowerMon_ADC();
                System.Threading.Thread.Sleep(100);
            }
            val /= n;
            return val;
        }

        public int GetLaserPowerMon_ADC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.LS_POWER_RAW));
        }

        public int GetLockerErrorFactor()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_LOCKER_SLOPE));
        }

        public bool GetLockerOn()
        {
            int temp = HexToUInt(ReadFromChassis(X2_Command.BFM_LOCKER_ENABLE));
            return temp == 0 ? false : true;
        }

        public int GetLockerRefMon_ADC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_BFM_REFL));
        }

        public bool GetLockerSlopePositive()
        {
            int a = HexToUInt(ReadFromChassis(X2_Command.BFM_LOCKER_SLOPE));
            if (a == 0)
                return false;
            else
                return true;
        }

        public int GetLockerTxMon_ADC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_BFM_TRAN));
        }

        public int GetLsPhaseSense()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_LS_PHASE));
        }

        public int GetModeCentrePhaseDAC()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetPhaseDAC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_PHASE));
        }

        public bool GetPowerControlLoopOn()
        {

            return (HexToInt(ReadFromChassis(X2_Command.LS_POWER_CTRL_ENABLE)) == 1); 
        }

        public int GetPowerMonFactor()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_POW_MON_SLOPE));
        }

        public int GetPowerMonOffset()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET));
        }

        public int GetPowerMonPot()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_LS_POWER));
        }

        public bool GetPowerProfilingOn()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetRearDAC()
        {
            return Convert.ToInt32(HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_REAR)));
        }

        public int GetRefCoarsePot()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_BFM_REFL));
        }

        public int GetSBSDitherMultiplier()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetSBSDitherOn()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetSOADAC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_SOA));
        }

        public int GetTxCoarsePot()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_BFM_TRANS));
        }

        public void SaveChannel(int chan)
        {

            WriteToChassis(X2_Command.CHAN_STORE_IDX, ToHex(chan));

        }

        public void SetBOLPhaseDAC(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_BOL, ToHex(dac));
        }

        public void SetDitherDAC(int dac)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetEOLPhaseADC_Max(int pmax)
        {
            this.SetPhaseAlarmHighDacNominal(pmax);
        }

        public void SetEOLPhaseADC_Min(int pmin)
        {
            this.SetPhaseAlarmLowDacNominal(pmin);
        }

        public void SetFrontEvenDAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_EVEN, ToHex(dac));
        }

        public void SetFrontOddDAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_ODD, ToHex(dac));
        }

        public void SetFrontSectionPair(int i)
        {
            WriteToChassis(X2_Command.DSDBR_FRONT_SWITCHES, ToHex(i));
        }

        public void SetGainDAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_GAIN, ToHex(dac));
        }

        public void SetLockerErrorFactor(int lef)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_LOCKER_SLOPE, ToHex(lef));
        }

        public void SetLockerOn(bool on)
        {
            int dac = 0;
            dac = on?1:0;
            WriteToChassis(X2_Command.BFM_LOCKER_ENABLE, ToHex(dac));
        }

        public void SetLockerSlopePositive(bool positive)
        {
            int sign = positive ? 1 : 0;

           WriteToChassis(X2_Command.BFM_LOCKER_SLOPE, ToHex(sign));
            
        }

        public void SetModeCentrePhaseDAC(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_CENTER, ToHex(dac));
        }

        public void SetPhaseDAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_PHASE, ToHex(dac));
        }

        public void SetPowerControlLoopOn(bool enabled)
        {
            int dac = 0;
            if (enabled)
                dac = 1;
            else
                dac = 0;
            WriteToChassis(X2_Command.LS_POWER_CTRL_ENABLE, ToHex(dac));
        }

        public void SetPowerMonFactor(int pmf)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_POW_MON_SLOPE, ToHex(pmf));
        }

        public void SetPowerMonOffset(int pmo)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET, ToHex(pmo));
        }

        public void SetPowerMonPot(int pot)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetPowerProfilingOn(bool on)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRearDAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_REAR, ToHex(dac));
        }

        public void SetRefCoarsePot(int rcp)
        {
            WriteToChassis(X2_Command.DEV_ADC_BFM_REFL,ToHex(rcp));
        }

        public void SetSBSDitherMultiplier(int mult)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSBSDitherOn(bool on)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSOADAC(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_SOA, ToHex(dac));
        }

        public void SetTxCoarsePot(int pot)
        {
            WriteToChassis(X2_Command.DEV_DAC_BFM_TRANS,ToHex(pot));
        }

        public int SettlingTime_ms
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

        #region IMSA_XFP Members

        public void EnterOptionalPassword(byte[] password)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public byte[] DefaultPassword
        {
            get { return this.defaultPassword; }
            set { this.defaultPassword = value; }
        }
        public string GetFirmwareRevisionCode()
        { 
            return HexToString(ReadFromChassis(X2_Command.FirmwareRevision));
        }

        public void DisableI2CPacketErrorChecking()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void EnableI2CPacketErrorChecking()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void EnterPassword(byte[] password)
        {
            string hexString = "";
            for(int i = 0;i<password.Length;i++)
            {
                hexString += ToHex(password[i]);
            }
            WriteToChassis(X2_Command.HOST_PASSWARD, hexString);
        }

        public byte GetAlarmOrWarningMaskRegister(byte addr)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public byte GetAlarmOrWarningRegister(byte addr)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetAux1HighAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_AUX1_HIGH));
        }

        public int GetAux1HighWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_AUX1_HIGH));
        }

        public int GetAux1LowAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_AUX1_LOW));
        }

        public int GetAux1LowWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_AUX1_LOW));
        }

        public int GetAux2HighAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_AUX2_HIGH));
        }

        public int GetAux2HighWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_AUX2_HIGH));
        }

        public int GetAux2LowAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_AUX2_LOW));
        }

        public int GetAux2LowWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_AUX2_LOW));
        }

        public int GetAuxMeas1()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetAuxMeas2()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetBiasHighAlarmThreshold()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetBiasHighWarningThreshold()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetBiasLowAlarmThreshold()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetBiasLowWarningThreshold()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetDataNotReadyBitState()
        {
            int mask = 1;
            int temp = this.HexToUInt(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            return ((temp & mask) == 1);
        }

        public byte[] GetEnteredOptionalPassword()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetModNrPinState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public byte GetModuleIdentifier()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public byte[] GetModulePassword()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double GetModuleTemperature_DegC()
        {
            double temp = HexToUInt(ReadFromChassis(X2_Command.PCB_TEMPERATURE)) / 256.0;
            return temp ;
        }

        public bool GetNIntPinState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetPDownPinState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetRxCDRBitState()
        {
            byte mask = 1 << 3;
            byte state = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE2));
            if ((mask & state) == 0)
            {
                return false;
            }
            return true;
        }

        public double GetRxInputPower_dBm()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double GetRxInputPower_mW()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetRxLOSState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetRxNRBitState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetRxPowerHighAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_RX_POWER_HIGH));
        }

        public int GetRxPowerHighWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_RX_POWER_HIGH));
        }

        public int GetRxPowerLowAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_RX_POWER_LOW));
        }

        public int GetRxPowerLowWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_RX_POWER_LOW));
        }

        public bool GetSignalConditionerControlBit()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetSoftPDownBitState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        // Bit 6 of byte 110
        public bool GetSoftTxDisableBitState()
        {
            int mask = 1 << 6;

            byte generalCtrlByte = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));

            return Convert.ToBoolean((generalCtrlByte & mask));
        }

        public byte[] GetTableSelect()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetTempHighAlarmThreshold()
        {
            return (int)HexToInt(ReadFromChassis(X2_Command.ALARM_CASE_TEMP_HIGH));
        }

        public int GetTempHighWarningThreshold()
        {
            return (int)HexToInt(ReadFromChassis(X2_Command.WARN_CASE_TEMP_HIGH));
        }

        public int GetTempLowAlarmThreshold()
        {
            return (int)HexToInt(ReadFromChassis(X2_Command.ALARM_CASE_TEMP_LOW));
        }

        public int GetTempLowWarningThreshold()
        {
            return (int)HexToInt(ReadFromChassis(X2_Command.WARN_CASE_TEMP_LOW));
        }

        public double GetTransmitterWavelength()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double GetTxBiasCurrent_mA()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetTxCDRBitState()
        {
            byte mask = 1 << 5;
            byte state = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE2));
            if ((mask & state) == 0)
            {
                return false;
            }
            return true;
        }

        public bool GetTxDisablePinState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetTxFaultBitState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetTxNRBitState()
        {
            return GetTxALMINTState();
        }

        public double GetTxOutputPower_dBm()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double GetTxOutputPower_mW()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetTxPowerHighAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_TX_POWER_HIGH));
        }

        public int GetTxPowerHighWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_TX_POWER_HIGH));
        }

        public int GetTxPowerLowAlarmThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.ALARM_TX_POWER_LOW));
        }

        public int GetTxPowerLowWarningThreshold()
        {
            return (int)HexToUInt(ReadFromChassis(X2_Command.WARN_TX_POWER_LOW));
        }

        public string GetXFPVendorDateCode()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorDateCode));
        }

        public string GetXFPVendorName()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorName));
        }

        public byte[] GetXFPVendorOUI()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string GetXFPVendorPartNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorPartNo));
        }

        public string GetXFPVendorRev()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorRevision));
        }

        public string GetXFPVendorSerialNumber()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorSerialNumber));
        }

        public byte ReadDigitalDiagnosticRegisterByte(byte addr)
        {
            return HexToByte(ReadFromChassis(X2_Command.DiagnosticMonitorType));
        }

        public int ReadDigitalDiagnosticRegisterWord(byte addr)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public byte ReadPagedRegisterByte(byte tableSelect, byte regAddr)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string ReadPagedRegisterString(byte tableSelect, byte regStartAddr, byte length)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int ReadPagedRegisterWord(byte tableSelect, byte regAddr)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAlarmOrWarningMaskRegister(byte addr, byte mask)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux1HighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux1HighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux1LowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux1LowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux2HighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux2HighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux2LowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetAux2LowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetBiasHighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetBiasHighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetBiasLowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetBiasLowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public void SetPassword(byte[] password)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRxPowerHighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRxPowerHighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRxPowerLowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRxPowerLowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool SetSignalConditionerControlBit(bool bit)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSoftPDownBitState(bool state)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSoftTxDisableBitState(bool state)
        {
            byte generalCtrlByte = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            generalCtrlByte = SetBytebit(generalCtrlByte, 6,state);
            WriteToChassis(X2_Command.GENERAL_CONTROL_BYTE1, ToHex(generalCtrlByte));
        }

        public byte[] SetTableSelect()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTempHighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTempHighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTempLowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTempLowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTransmitterWavelength(double wavelength_nm)
        {
            WriteToChassis(X2_Command.WAVELENGTH_CTRL,ToHex((int)(wavelength_nm*20)));
        }

        public void SetTxPowerHighAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTxPowerHighWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTxPowerLowAlarmThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetTxPowerLowWarningThreshold(int thresh)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IReceiverSetup Members
        /// <summary>
        /// Enter Receiver Setup Mode
        /// </summary>
        public void EnterRxSetupMode()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// Enter Protected Command Mode
        /// </summary>
        public void EnterProtectMode()
        {
            this.EnterPassword(defaultPassword);
        }

        /// <summary>
        /// Exit protected Command mode
        /// </summary>
        public void ExitProtectMode()
        {
            //do nothing...
        }               

        /// <summary>
        /// Set the Rx Threshold (RxDTV).
        /// </summary>
        /// <param name="thresh_pc">The Threshold in Percent</param>
        public void SetRxThreshold(double thresh_pc)
        {
            double dtv = 126 * thresh_pc / 100.0;
            SetRxDTV((int)Math.Round(dtv - 63, 0));
          
        }
               
        /// <summary>
        /// Set Rx DTV value.
        /// </summary>
        /// <param name="dtv"></param>
        public void SetRxDTV(int dtv)
        {
            if (Math.Abs(dtv) > 63)
            {
                throw new ArgumentOutOfRangeException("DTV should be between -63 and 63. Inputed dtv is:" + dtv.ToString());
            }
            int sign = 0;
            if (dtv < 0)
        {
                sign = 0x40;
        }

            string dtvValueString = ToHex((int)(Math.Abs(dtv) | sign));
            string dtvAddressString = "8214";
           
            WriteToChassis(X2_Command.EDC_REGISTER_ADDRESS, dtvAddressString);

            int read = HexToUInt(ReadFromChassis(X2_Command.EDC_REGISTER_ADDRESS));
            System.Threading.Thread.Sleep(100);
            WriteToChassis(X2_Command.EDC_REGISTER_VALUE, dtvValueString);
            System.Threading.Thread.Sleep(300);
            string temp = ReadFromChassis(X2_Command.EDC_REGISTER_ADDRESS);
            temp = ReadFromChassis(X2_Command.EDC_REGISTER_VALUE);

        }

        /// <summary>
        /// Get the current Rx Threshold (RxDTV).This value is actual Dac value(from -63 to +63).
        /// </summary>
        /// <returns>The Threshold from -63 to +63.</returns>
        public double ReadRxThreshold()
        {
            string dtvAddressString = "8214";
            WriteToChassis(X2_Command.EDC_REGISTER_ADDRESS, dtvAddressString);

            int rxDtv = HexToUInt(ReadFromChassis(X2_Command.EDC_REGISTER_VALUE));
            rxDtv = ((rxDtv & 0x40) > 0 ? -1 : +1) * (0x3F & rxDtv);

            return (double)rxDtv;
        }

        /// <summary>
        /// Get M3 APD voltage bias DAC setting
        /// </summary>
        /// <returns>DAC Value</returns>
        public int GetM3ApdBiasVoltageDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.RX_BIAS_M3_DAC));
        }

        /// <summary>
        /// Set M3 APD voltage bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetM3ApdBiasVoltageDac(int dac)
        {
            WriteToChassis(X2_Command.RX_BIAS_M3_DAC, ToHex(dac));
            //SetM3ApdBiasVoltage_V(ConvertDACValueToVoltage(dac));
        }

        /// <summary>
        /// Get M10 APD voltage bias DAC setting
        /// </summary>
        /// <returns>DAC Value</returns>
        public int GetM10ApdBiasVoltageDac()
        {
            //invokes M8 method directly.
            return GetM8ApdBiasVoltageDac();
        }

        /// <summary>
        /// Set M10 APD voltage bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetM10ApdBiasVoltageDac(int dac)
        {
            //invokes M8 method directly.
            SetM8ApdBiasVoltageDac(dac);
        }

        /// <summary>
        /// Set Index to position in lookup table to store raw optical power monitor reading with adjustment for Apd voltage. 
        /// </summary>
        /// <param name="calPoint">The index</param>
        public void SetRxPowMonCalibrationPoint(byte calPoint)
        {
            if (calPoint>15)
            {
                throw new ArgumentOutOfRangeException("calPoint range should be from 0 to 15. However current calPoint is " + calPoint.ToString() + ".");
            }

            X2_Command command = (X2_Command)Enum.Parse(typeof(X2_Command), "RX_PMON_CAL_" + calPoint.ToString());

            double power_uW = this.GetRxCalPower_mW() * 1000;

            int temp = (int)(power_uW * 10);
            WriteToChassis(command, ToHex(temp));
            
        }

        /// <summary>
        /// Get Rx Loss Of Signal (LOS) Alarm Threshold
        /// </summary>
        /// <returns>The Threshold setting</returns>
        public int GetRxLOSAlarmThreshold()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_RX_LOS));
        }

        /// <summary>
        /// Set Rx Loss Of Signal (LOS) Alarm Threshold
        /// (CAL_SET_LOS_THRESHOLD)
        /// </summary>
        /// <returns>The Threshold setting</returns>
        public void SetRxLOSAlarmThreshold(int threshold)
        {
            WriteToChassis(X2_Command.RX_LOS_THRESH,ToHex(threshold));
        }


        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// </summary>
        /// <returns>The ADC Reading.</returns>
        public int GetRxPowerMon_ADC()
        { 
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_RX_POWER));
        }


        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>The ADC Reading.</returns>
        public int GetRxPowerMon_ADC(int average)
        {
            int totalADC = 0;
            for (int i = 0; i < average; i++)
            {
                totalADC += GetRxPowerMon_ADC();
                System.Threading.Thread.Sleep(10);
            }
            return (int)(totalADC / average);
        }


        /// <summary>
        /// Get the state of the RxPOWALM (Loss DC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxPOWALMState()
        {
            ////byte powAlmMask = 0x40;
            ////byte state = HexToByte(ReadFromChassis(X2_Command.LATCHED_RX_POWER_ALARM));

            ////if ((powAlmMask & state) > 0)
            ////{
            ////    return true;
            ////}
            ////else
            ////{
            ////    return false;
            ////}
            //byte mask = 1 << 1;
            //byte state = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            //if ((mask & state) > 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    bool da = GetRxSIGALMState();
            //    return false;
            //}
            return GetRxSIGALMState();
           
        }
        
        #endregion

        #region ITcmzLaserSetup Members
        /// <summary>
        /// Enter Protected Command Mode
        /// </summary>
        //public void EnterProtectMode()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        /// <summary>
        /// Exit protected Command mode
        /// </summary>
        //public void ExitProtectMode()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        /// <summary>
        /// Enable Calibration Mode
        /// </summary>
        public void EnableCalibrationMode()
        {
            EnterPassword(defaultPassword);
        }

        /// <summary>
        /// Disable Calibration mode
        /// </summary>
        public void DisableCalibrationMode()
        {
        }
             
        /// <summary>
        /// Get DAC_PWC value
        /// </summary>
        /// <returns>DAC_PWC value</returns>
        public int GetPWCDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_PW));
        }

        /// <summary>
        /// Set DAC_PWC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetPWCDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_PW, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Bias Left DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftBiasDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_BIAS_35));
        }

        /// <summary>
        /// Set MZ Left Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzLeftBiasDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_BIAS_35, ToHex(dac));
        }


        /// <summary>
        /// Get Mz Bias Left DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftBiasDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_BIAS_35));
        }


        /// <summary>
        /// Set MZ Left Bias DAC Nominal Value 
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzLeftBiasDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_BIAS_35, ToHex(dac));
        }


        /// <summary>
        /// Get Mz Bias Right DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzRightBiasDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_BIAS_34));
        }

        /// <summary>
        /// Set MZ Right Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzRightBiasDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_BIAS_34, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Mod DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzModSetDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_MOD));
        }

        /// <summary>
        /// Set MZ Mod Set DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzModSetDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_MOD, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Mod DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzModSetDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_MOD));
        }

        /// <summary>
        /// Set Mz Mod DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzModSetDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_MOD, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Left Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftImbalDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_IMB_33));
        }

        /// <summary>
        /// Set Mz Left Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzLeftImbalDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_IMB_33, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Left Imbalance DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftImbalDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_IMB_33));
        }

        /// <summary>
        /// Set Mz Left Imbalance DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzLeftImbalDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_IMB_33, ToHex(dac));
            
        }

        /// <summary>
        /// Get Mz Right Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>       
        public int GetMzRightImbalDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_MZ_IMB_32));
        }

        /// <summary>
        /// Set Mz Right Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzRightImbalDac(int dac)
        {
            WriteToChassis(X2_Command.DEV_DAC_MZ_IMB_32, ToHex(dac));
        }

        /// <summary>
        /// Get Mz Right Imbalance DAC Nominal value 
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzRightImbalDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_IMB_32));
        }


        /// <summary>
        /// Set Mz Right Imbalance DAC Nominal value 
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzRightImbalDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_IMB_32, ToHex(dac));
        }

        /// <summary>
        /// Get MMI DC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetMmiDc_ADC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_MZ_MMI));
        }

        /// <summary>
        /// Get MMI AC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetMmiAc_ADC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_MZ_MMI));
        }

        /// <summary>
        /// Get Offset for Mz Crossing  control loop 
        /// </summary>
        /// <returns>The offset</returns>
        public int GetMmiDiffOffset()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_X_POINT));
        }

        /// <summary>
        /// Set Offset for Mz Crossing  control loop. 
        /// </summary>
        /// <param name="offset">offset</param>
        public void SetMmiDiffOffset(int offset)
        {
            //WriteToChassis(X2_Command.MZ_AUTOX_ENABLE, ToHex(1));
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_X_POINT,ToHex(offset));
        }

        /// <summary>
        /// Set Position in n point look up table to store Mmitap derived power reading used for interpolating Tx power
        /// </summary>
        /// <param name="calPoint">The index</param>
        public void SetTxPowMonCalibrationPoint(byte calPoint)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Get SOA Slope in nanoWatts/dacCount
        /// </summary>
        /// <returns>The SOA Slope</returns>
        public int GetSoaSlope()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_SLOPE));
        }

        /// <summary>
        /// Store SOA Slope in nanoWatts/dacCount
        /// </summary>
        /// <param name="slope">The slope</param>
        public void SetSoaSlope(int slope)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_SLOPE,ToHex(slope));
        }


        /// <summary>
        /// Get the Calibrated SOA set Register value.
        /// This is the initial SOA value 
        /// used by the closed loop power control.
        /// </summary>
        /// <returns>value</returns>
        public int GetCalSoaRegister()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET));
        }

        /// <summary>
        /// Set the CAL_SET_SOA Register. This is the initial SOA value 
        /// used by the closed loop power control.
        /// </summary>
        /// <param name="value"></param>
        public void SetCalSoaRegister(int value)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET, ToHex(value));
        }

        /// <summary>
        /// Enable/Disable Crossing Point control loop
        /// </summary>
        /// <param name="enabled"></param>
        public void SetCrossingControlLoopOn(bool enabled)
        {
            int temp = enabled ? 1 : 0;
            WriteToChassis(X2_Command.MZ_AUTOX_ENABLE, ToHex(temp));
        }

        /// <summary>
        /// Get MZ Submount Temperature (MzTEMPMON) 
        /// </summary>
        /// <returns>The temperature in Deg C</returns>
        public double GetMzSubmountTemperature_DegC()
        {
            return (double)(HexToInt(ReadFromChassis(X2_Command.MZ_PID_TEMP_CELCIUS))/256.0);
        }

        /// <summary>
        /// Disable Modulation from Laser Driver
        /// </summary>
        public void DisableMzLaserModulation()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Enable Modulation from Laser Driver 
        /// </summary>
        public void EnableMzLaserModulation()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        ///  Disable Laser Output
        ///  Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        public void DisableLaserOutput()
        {
            byte status = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            byte laserOutputMask = 0x40; //bit 6.

            //if the laser output is enabled, disable it.
            if ((status & laserOutputMask) == 0)
            {
                WriteToChassis(X2_Command.GENERAL_CONTROL_BYTE1, ToHex((byte)(status | laserOutputMask)));
            }     
            
        }

        /// <summary>
        /// Enable Laser Output 
        /// Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        public void EnableLaserOutput()
        {
            byte status = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            byte laserOutputMask = 0x40;//bit 6.
          
            //if the laser output is disabled, enable it.
            if ((status & laserOutputMask) != 0)
            {
                WriteToChassis(X2_Command.GENERAL_CONTROL_BYTE1, ToHex((byte)(status & ~laserOutputMask)));
            }
        }


        /// <summary>
        /// Disable Internal PRBS generator
        /// </summary>
        public void DisableInternalPrbs()
        {
            throw new Exception("The method or operation is not implemented.");
        }


        /// <summary>
        /// Enable Internal PRBS generator
        /// </summary>
        public void EnableInternalPrbs()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region X2 Only

        public void SetRxStatus(int status)
        {
            WriteToChassis(X2_Command.RX_STATUS, ToHex(status));
        }

        public void SetM3ApdBiasVoltage_V(double voltage_V)
        {
            int voltage_mV = (int)(voltage_V * 1000);
            WriteToChassis(X2_Command.RX_BIAS_M3_VOLTS, ToHex(voltage_mV));
        }

        public double GetM3ApdBiasVoltage_V()
        {
            int voltage_mV = HexToUInt(ReadFromChassis(X2_Command.RX_BIAS_M3_VOLTS));
            return ((double)voltage_mV) / 1000.0;
        }

        public void SetM8ApdBiasVoltage_V(double voltage_V)
        {
            int voltage_mV = (int)(voltage_V * 1000);
            WriteToChassis(X2_Command.RX_BIAS_M8_VOLTS, ToHex(voltage_mV));
        }

        public double GetM8ApdBiasVoltage_V()
        {
            int voltage_mV = HexToUInt(ReadFromChassis(X2_Command.RX_BIAS_M8_VOLTS));
            return ((double)voltage_mV) / 1000.0;
        }

        public double ConvertDACValueToVoltage(int dac)
        {
            return dac * 0.0059082 + 12.91;
        }
        public double ConverVoltageToDACValue(double voltage)
        {
            return (int)((voltage - 12.91) / 0.0059082);
        }

        /// <summary>
        /// Gets the current sub-mount temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetSubMountTemp_degC()
        {
            double temp=(double)HexToInt(ReadFromChassis(X2_Command.LS_PID_TEMP_CELCIUS))/256.0;
            return temp;
        }
        public double SelectITUChannel(BandChannel bandChannel, bool protectModeChange)
        {
            WriteToChassis(X2_Command.WAVELENGTH_CTRL, ToHex((int)bandChannel.Chan));
            return bandChannel.Chan;
        }

        /// <summary>
        /// Returns the First Channel Freq listed in the X2 table
        /// </summary>
        /// <returns></returns>
        public double GetFirstChannelFreqGHz()
        {
            double freq_THz = Convert.ToDouble(HexToInt(ReadFromChassis(X2_Command.FIRST_CHANNEL_FREQ_THZ)));
            double freq_GHz = Convert.ToDouble(HexToInt(ReadFromChassis(X2_Command.FIRST_CHANNEL_FREQ_GHZ)));

            return( (freq_THz * 1000) + (freq_GHz / 10)); 
        }

        /// <summary>
        /// Get LsWAVEMON Value 
        /// </summary>
        /// <returns>Tx Wavelength in GHz</returns>
        public double GetLsWAVEMON_GHz()
        {
            int wavelengthDAC = HexToUInt(ReadFromChassis(X2_Command.WAVELENGTH_CTRL));
            double startWL_nm = Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(startFreq);
            double wl_nm = startWL_nm + wavelengthDAC * 0.005;
            return Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wl_nm);
        }

        /// <summary>
        /// Get LsBIASMON Value 
        /// </summary>
        /// <returns>Laser Bias Monitor Currrent in mA</returns>
        public double GetLsBIASMON_mA()
        {
            int biasDAC = HexToUInt(ReadFromChassis(X2_Command.TX_BIAS));
            double stepSize = 0.002;
            return stepSize * biasDAC;
        }

        #region Page 4 
        /// <summary>
        /// Get Mz Bias Right DAC Nominal value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzRightBiasDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_BIAS_34));
        }
        /// <summary>
        /// Set MZ Right Bias DAC Nominal
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzRightBiasDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_BIAS_34, ToHex(dac));
        }
        public int GetSoaDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_SOA_CALIBRATED_PWR));
        }
        public void SetSoaDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_SOA_CALIBRATED_PWR,ToHex(dac));
        }
        public int GetPhaseDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_CENTER));
        }
        public void SetPhaseDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_CENTER,ToHex(dac));
        }
        public int GetRearDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_REAR));
        }
        public void SetRearDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_REAR,ToHex(dac));
        }
        public int GetFsPairDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_SWITCHES));
        }
        public void SetFsPairDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_SWITCHES,ToHex(dac));
        }
        public int GetFrontOddDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_ODD));
        }
        public void SetFrontOddDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_ODD,ToHex(dac));
        }
        public int GetFrontEvenDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_EVEN));
        }
        public void SetFrontEvenDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_EVEN,ToHex(dac));
        }
        public int GetGainDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_GAIN));
        }
        public void SetGainDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_GAIN,ToHex(dac));
        }
        public int GetWavelengthDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_WAVELENGTH));
        }
        public void SetWavelengthDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_WAVELENGTH,ToHex(dac));
        }
        public int GetSoaLockActivateDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_SOA_LOCKER_ACTIVATE));
        }
        public void SetSoaLockActivateDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_SOA_LOCKER_ACTIVATE,ToHex(dac));
        }     
        public int GetPhaseAlarmHighDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_HIGH));
        }
        public void SetPhaseAlarmHighDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_HIGH,ToHex(dac));
        }
        public int GetPhaseAlarmLowDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_LOW));
        }
        public void SetPhaseAlarmLowDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_ALARM_LOW, ToHex(dac));
        }
        public int GetPhaseWarnHighDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_WARN_HIGH));
        }
        public void SetPhaseWarnHighDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_WARN_HIGH,ToHex(dac));
        }
        public int GetPhaseWarnLowDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_WARN_LOW));
        }
        public void SetPhaseWarnLowDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_WARN_LOW,ToHex(dac));
        }
        public int GetPhaseMonBolDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_PHASE_MON_BOL));
        }
        public void SetPhaseMonBolDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_PHASE_MON_BOL, ToHex(dac));
        }
        public int GetLockerPolarityDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_LOCKER_POLARITY));
        }
        public void SetLockerPolarityDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_LOCKER_POLARITY,ToHex(dac));
        }
        public int GetLockerSlopeDacNominal()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_LOCKER_SLOPE));
        }
        public void SetLockerSlopeDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_LOCKER_SLOPE, ToHex(dac));
        }
        public int GetBFMTransDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_BFM_TRANS));
        }
        public void SetBFMTransDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_BFM_TRANS, ToHex(dac));
        }
        public int GetBFMReflDacNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_BFM_REFL));
        }
        public void SetBFMReflDacNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_BFM_REFL, ToHex(dac));
        }
        public int GetChanPowMonSlopeNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_POW_MON_SLOPE));
        }
        public void SetChanPowMonSlopeNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_POW_MON_SLOPE, ToHex(dac));
        }
        public int GetChanPowMonOffsetNominal()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET));
        }
        public void SetChanPowMonOffsetNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_POW_MON_OFFSET, ToHex(dac));
        }
        public int GetMzPWNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_PW));
        }
        public void SetMzPWNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_PW, ToHex(dac));
        }
        public int GetMzModNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_MOD));
        }
        public void SetMzModNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_MOD, ToHex(dac));
        }
        public int GetMzXPointNominal()
        {
            return HexToInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_X_POINT));
        }
        public void SetMzXPointNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_X_POINT, ToHex(dac));
        }
        public int GetMzSlopeNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.CHAN_PARAM_MZ_SLOPE));
        }
        public void SetMzSlopeNominal(int dac)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_MZ_SLOPE, ToHex(dac));
        }

        #endregion

        #region Page 8
        /// <summary>
        /// Set the Value for page 8 address 132
        /// </summary>
        /// <param name="value"></param>
        public void SetVitEDCControlParam132(int value)
        {
            WriteToChassis(X2_Command.VitEDCControlParam132, ToHex(value));

            int a = HexToUInt(ReadFromChassis(X2_Command.VitEDCControlParam132));
        }

        /// <summary>
        /// set value for page 8 address is 134
        /// </summary>
        /// <param name="value"></param>
        public void SetVitEDCControlParam134(int value)
        {
            WriteToChassis(X2_Command.VitEDCControlParam134, ToHex(value));

            int a = HexToUInt(ReadFromChassis(X2_Command.VitEDCControlParam134));
        }
        #endregion

        public void SetM8ApdBiasVoltageDac(int dac)
        {
            WriteToChassis(X2_Command.RX_BIAS_M8_DAC, ToHex(dac));

            int a = HexToInt(ReadFromChassis(X2_Command.RX_BIAS_M8_DAC));
            //SetM8ApdBiasVoltage_V(ConvertDACValueToVoltage(dac));
        }

        public int GetM8ApdBiasVoltageDac()
        {
            return HexToUInt(ReadFromChassis(X2_Command.RX_BIAS_M8_DAC));
        }

        /// <summary>
        /// Gets APD temperature.
        /// </summary>
        /// <returns></returns>
        public double GetAPDTemp_degC()
        {
            double apdTemp_degC;
            apdTemp_degC = HexToUInt(ReadFromChassis(X2_Command.RX_TEMP)) / 256.0;
            return apdTemp_degC;
        }

        public double GetRxCalPower_mW()
        {
            double rxPower_mW;
            int temp = HexToUInt(ReadFromChassis(X2_Command.RX_POWER_MON_RAW));
            rxPower_mW = temp / 10000.0;
            return rxPower_mW;
        }

        public double GetRxPower_mW()
        {
            double power = HexToUInt(ReadFromChassis(X2_Command.RX_POWER_STANDARD)) / 10000.0;
            return power;
        }

        /// <summary>
        /// Get LsTEMPMON Value 
        /// </summary>
        /// <returns>Laser Temperature in Degrees C</returns>
        public double GetLsTEMPMON_DegC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_ADC_LS_TEMP));
        }

        /// <summary>
        /// Get LsPOWMON  Value 
        /// </summary>
        /// <returns>Laser output power in mW</returns>
        public double GetLsPOWMON_mW()
        {
            return Convert.ToDouble(HexToInt(ReadFromChassis(X2_Command.LS_POWER_MON)))/Convert.ToDouble(10000);           
        }

        /// <summary>
        /// Get the state of the TxALM INT alarm (all TX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxALMINTState()
        {
            byte txAlarmMask = 1 << 7;//bit 7
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_84));
            if ((state & txAlarmMask) == 0)
            {
                //TxALM INT Bit is set - normal operation
                return false;
            }
            else
            {
                //TxALM INT Bit is clear - a Tx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the RxALM INT alarm (all RX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxALMINTState()
        {
            byte txAlarmMask = 1 << 4;//bit 4
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_84));
            if ((state & txAlarmMask) == 0)
            {
                //TxALM INT Bit is set - normal operation
                return false;
            }
            else
            {
                //TxALM INT Bit is clear - a Tx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the EOLALM (Laser end of life) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetEOLALMState()
        {
            byte eolAlarmMask = (1 << 2) | (1 << 3);
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & eolAlarmMask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsWAVALM (Laser Wavelength) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsWAVALMState()
        {
            byte lsWavAlarmMak = (1 << 5);
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_85));
            if ((state & lsWavAlarmMak) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsBIASALM (Laser bias current) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsBIASALMState()
        {
            return GetEOLALMState();
        }

        /// <summary>
        /// Get the state of the LsTEMPALM (Laser temperature) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsTEMPALMState()
        {
            byte mask = (1 << 6) | (1 << 7);
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & mask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsPOWALM (Laser power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsPOWALMState()
        {
            byte mask = 1 | (1 << 1);
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & mask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the TxLOCKERR (Loss of TxPLL lock indicator) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxLOCKERRState()
        {
            byte mask = 1 << 5;
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_84));
            if ((state & mask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the RxSIGALM (Loss AC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxSIGALMState()
        {
            byte mask = 1 << 1;
            byte state = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE1));
            if ((state & mask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the state of the RxLOCKERR (Loss of lock of RxPOCLK) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxLOCKERRState()
        {
            byte mask = 1 << 3;
            byte state = HexToByte(ReadFromChassis(X2_Command.GENERAL_CONTROL_BYTE2));
            if ((state & mask) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool GetWavnleghtUnlockedState()
        {
            return GetLsWAVALMState();
        }

        public TX_Bias_State GetTxBiasState()
        {
            byte highMask = 1 << 3;
            byte lowMask = 1 << 2;
            bool highAlarm = true;
            bool lowAlarm = true;
            bool highWarning = true;
            bool lowWarning = true;

            //Gets Alarm state.
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & highMask) == 0)
            {
                highAlarm = false;
            }
            if ((state & lowMask) == 0)
            {
                lowAlarm = false;
            }

            //Gets Warning State.
            state = HexToByte(ReadFromChassis(X2_Command.STANDARD_82));
            if ((state & highMask) == 0)
            {
                highWarning = false;
            }
            if ((state & lowMask) == 0)
            {
                lowWarning = false;
            }

            return new TX_Bias_State(highAlarm, lowAlarm, highWarning, lowWarning);
        }

        public Temperature_State GetTemperatureState()
        {
            byte highMask = 1 << 7;
            byte lowMask = 1 << 6;
            bool highAlarm = true;
            bool lowAlarm = true;
            bool highWarning = true;
            bool lowWarning = true;

            //Gets Alarm state.
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & highMask) == 0)
            {
                highAlarm = false;
            }
            if ((state & lowMask) == 0)
            {
                lowAlarm = false;
            }

            //Gets Warning State.
            state = HexToByte(ReadFromChassis(X2_Command.STANDARD_82));
            if ((state & highMask) == 0)
            {
                highWarning = false;
            }
            if ((state & lowMask) == 0)
            {
                lowWarning = false;
            }

            return new Temperature_State(highAlarm, lowAlarm, highWarning, lowWarning);
        }

        public TX_Power_State GetTxPowerState()
        {
            byte highMask = 1 << 1;
            byte lowMask = 1;
            bool highAlarm = true;
            bool lowAlarm = true;
            bool highWarning = true;
            bool lowWarning = true;

            //Gets Alarm state.
            byte state = HexToByte(ReadFromChassis(X2_Command.STANDARD_80));
            if ((state & highMask) == 0)
            {
                highAlarm = false;
            }
            if ((state & lowMask) == 0)
            {
                lowAlarm = false;
            }

            //Gets Warning State.
            state = HexToByte(ReadFromChassis(X2_Command.STANDARD_82));
            if ((state & highMask) == 0)
            {
                highWarning = false;
            }
            if ((state & lowMask) == 0)
            {
                lowWarning = false;
            }

            return new TX_Power_State(highAlarm, lowAlarm, highWarning, lowWarning);
        }

        public void ClearTxRxAlarm()
        {
            ReadFromChassis(X2_Command.STANDARD_80);
            ReadFromChassis(X2_Command.LATCHED_RX_POWER_ALARM);
        }

        /// <summary>
        /// Enable PowerDownStage 
        /// Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        public void EnablePowerDownState()
        {
            string request = "P8,0";
            this.instrumentChassis.WriteLine(request, this);
            System.Threading.Thread.Sleep(100);
            string response = this.instrumentChassis.ReadLine(this);
            string responseData = ValidateResponse(request, response);
        }

        /// <summary>
        /// Disable PowerDownStage 
        /// Use with caution - may not work properly at Tx Module Setup Stage.
        /// </summary>
        public void DisablePowerDownState()
        {
                string request = "P8,1";
                this.instrumentChassis.WriteLine(request, this);
                System.Threading.Thread.Sleep(100);
                string response = this.instrumentChassis.ReadLine(this);
                string responseData = ValidateResponse(request, response);
        }

        public void DisableHardwareLaserOutput()
        {
            string command = "P7,1";
            this.instrumentChassis.WriteLine(command, this);
            string response = ValidateResponse(command, this.instrumentChassis.ReadLine(this));
        }

        public void EnableHardwareLaserOutput()
        {
            string command = "P7,0";
            this.instrumentChassis.WriteLine(command, this);
            string response = ValidateResponse(command, this.instrumentChassis.ReadLine(this));
        }

        public void SetLaserBFMTransDAC(int dac)
        {
            this.WriteToChassis(X2_Command.DEV_DAC_BFM_TRANS, ToHex(dac));
        }
        public void SetLaserBFMReflDAC(int dac)
        {
            this.WriteToChassis(X2_Command.DEV_DAC_BFM_REFL, ToHex(dac));
        }
        public int GetLaserBFMTransDAC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_BFM_TRANS));
        }
        public int GetLaserBFMReflDAC()
        {
            return HexToUInt(ReadFromChassis(X2_Command.DEV_DAC_BFM_REFL));
        }
        public int GetMzSlope()
        {
            return HexToInt(ReadFromChassis(X2_Command.MZ_AUTOX_SLOPE));
        }
        public void SetMzSlope(int slope)
        {
            WriteToChassis(X2_Command.MZ_AUTOX_SLOPE,ToHex(slope));
        }
        public void DisableAutoLockerSetup()
        {
            int i = 0;
            WriteToChassis(X2_Command.BFM_INIT_AUTOLOCK, ToHex(i));
        }
        public void Reset()
        {
            string request = "T0";
            this.instrumentChassis.WriteLine(request, this);
            string response = this.instrumentChassis.ReadLine(this);  
        }
        public int GetAutoXSetPoint()
        {
            return HexToInt(ReadFromChassis(X2_Command.MZ_AUTOX_SET_POINT));
        }
        public void SetAutoXSetPoint(int xPoint)
        {
            WriteToChassis(X2_Command.MZ_AUTOX_SET_POINT, ToHex(xPoint));
        }
        public void SetChanIndexNomal(int index)
        {
            WriteToChassis(X2_Command.CHAN_PARAM_INDEX,ToHex(index));
        }
        public void SetTxDisableState(int state)
        {
            if (state != 0) state = 1;
            string request = "P7,"+state;
            this.instrumentChassis.WriteLine(request,this);
            string response = instrumentChassis.ReadLine(this);
        }
        public int GetPIDStatus()
        {
            return HexToUInt(ReadFromChassis(X2_Command.PID_STATUS));
        }
        public void SetPIDStatus(int s)
        {
            WriteToChassis(X2_Command.PID_STATUS,ToHex(s));
        }
        public void WriteEeprom(string[] data, bool verifyRequired)
        {
            for (int i = 0; i < data.Length; i++)
            {
                string hexData = data[i].Substring(9, 32);

                string request = "D166," + i*16 + "," + hexData;
                this.instrumentChassis.WriteLine(request,this);
                if (verifyRequired)
                {
                    this.instrumentChassis.WriteLine("D166,"+i*16+",16?",this);
                    string response = instrumentChassis.ReadLine(this);
                    if (response != hexData)
                    {
                        Exception a= new Exception();
                        throw a;
                    }
                }
            }
        }
        public int GetPidStatus()
        {
            return HexToUInt(ReadFromChassis(X2_Command.PID_STATUS));
        }
        public void SetPidStatus(int s)
        {
            WriteToChassis(X2_Command.PID_STATUS, ToHex(s));
        }
        public int GetLsPidStatus()
        {
            return HexToUInt(ReadFromChassis(X2_Command.LS_PID_ENABLE));
        }
        public void SetLsPidStatus(int enable)
        {
            WriteToChassis(X2_Command.LS_PID_ENABLE,ToHex(enable));
        }
        public int GetMzPidStatus()
        {
            return HexToUInt(ReadFromChassis(X2_Command.MZ_PID_ENABLE));
        }
        public void SetMzPidStatus(int enable)
        {
            WriteToChassis(X2_Command.MZ_PID_ENABLE, ToHex(enable));
        }
        public int GetFirstFreqGHz()
        {
            int highPart = HexToUInt(ReadFromChassis(X2_Command.FIRST_FREQ_HI));
            int lowPart = HexToUInt(ReadFromChassis(X2_Command.FIRST_FREQ_LO));
            return highPart * 1000 + lowPart / 10;
        }
        public int GetFreqGrid()
        {
            return HexToUInt(ReadFromChassis(X2_Command.FREQ_GRID));
        }
        public void SyncRegPages()
        {
            ReadFromChassis(X2_Command.CHAN_PARAM_INDEX);
            System.Threading.Thread.Sleep(1000);
            ReadFromChassis(X2_Command.DEV_ADC_LS_TEMP);
            System.Threading.Thread.Sleep(1000);
        }
        #endregion

        #region Serial ID Page Members
        public void SetIdentifierType(byte n)
        {
            WriteToChassis(X2_Command.IDENTIFIER, ToHex(n));
        }
        public byte GetIdentifierType()
        {
            return HexToByte(ReadFromChassis(X2_Command.IDENTIFIER));
        }
        public void SetExtendedIdentifierType(byte n)
        {
            WriteToChassis(X2_Command.ExtendedIdentifierType, ToHex(n));
        }
        public byte GetExtendedIdentifierType()
        {
            return HexToByte(ReadFromChassis(X2_Command.ExtendedIdentifierType));
        }
        public void SetConnectorCode(byte n)
        {
            WriteToChassis(X2_Command.ConnectorCode,ToHex(n));
        }
        public byte GetConnectorCode()
        {
            return HexToByte(ReadFromChassis(X2_Command.ConnectorCode));
        }
        public void SetTransceiverCode(string code)
        {
            WriteToChassis(X2_Command.TransceiverCode, code);
        }
        public string GetTransceiverCode()
        {
            return ReadFromChassis(X2_Command.TransceiverCode);
        }
        public void SetEncodingCode(byte n)
        {
            WriteToChassis(X2_Command.EncodingCode, ToHex(n));
        }
        public byte GetEncodingCode()
        {
            return HexToByte(ReadFromChassis(X2_Command.EncodingCode));
        }

        public void SetBitRateMin(byte n)
        {
            WriteToChassis(X2_Command.BitRateMin, ToHex(n));
        }
        public byte GetBitRateMin()
        {
            return HexToByte(ReadFromChassis(X2_Command.BitRateMin));
        }

        public void SetBitRateMax(byte n)
        {
            WriteToChassis(X2_Command.BitRateMax, ToHex(n));
        }
        public byte GetBitRateMax()
        {
            return HexToByte(ReadFromChassis(X2_Command.BitRateMax));
        }

        public void SetLengthSMF(byte n)
        {
            WriteToChassis(X2_Command.LengthSMF, ToHex(n));
        }
        public byte GetLengthSMF()
        {
            return HexToByte(ReadFromChassis(X2_Command.LengthSMF));
        }

        public void SetLengthE50(byte n)
        {
            WriteToChassis(X2_Command.LengthE50, ToHex(n));
        }
        public byte GetLengthE50()
        {
            return HexToByte(ReadFromChassis(X2_Command.LengthE50));
        }

        public void SetLength50(byte n)
        {
            WriteToChassis(X2_Command.Length50, ToHex(n));
        }
        public byte GetLength50()
        {
            return HexToByte(ReadFromChassis(X2_Command.Length50));
        }

        public void SetLength625(byte n)
        {
            WriteToChassis(X2_Command.Length625, ToHex(n));
        }
        public byte GetLength625()
        {
            return HexToByte(ReadFromChassis(X2_Command.Length625));
        }

        public void SetLengthCopper(byte n)
        {
            WriteToChassis(X2_Command.LengthCopper, ToHex(n));
        }
        public byte GetLengthCopper()
        {
            return HexToByte(ReadFromChassis(X2_Command.LengthCopper));
        }

        public void SetDeviceTechnology(byte n)
        {
            WriteToChassis(X2_Command.DeviceTechnology, ToHex(n));
        }
        public byte GetDeviceTechnology()
        {
            return HexToByte(ReadFromChassis(X2_Command.DeviceTechnology));
        }

        public void SetVendorName(string name)
        {
            WriteToChassis(X2_Command.VendorName, ToHex(name));
        }
        public string GetVendorName()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorName));
        }

        public void SetCDRRate(byte n)
        {
            WriteToChassis(X2_Command.CDRRate, ToHex(n));
        }
        public byte GetCDRRate()
        {
            return HexToByte(ReadFromChassis(X2_Command.CDRRate));
        }

        public void SetVendorOUI(string hexString)
        {
            WriteToChassis(X2_Command.VendorOUI, hexString);
        }
        public string GetVendorOUI()
        {
            return HexToString( ReadFromChassis(X2_Command.VendorOUI));
        }

        public void SetVendorRevision(string n)
        {
            WriteToChassis(X2_Command.VendorRevision, ToHex(n));
        }
        public string GetVendorRevision()
        {
            return HexToString(ReadFromChassis(X2_Command.VendorRevision)).Trim();
        }

        public void SetWavelengthNominal(int n)
        {
            WriteToChassis(X2_Command.WavelengthNominal, ToHex(n));
        }
        public int GetWavelengthNominal()
        {
            return HexToUInt(ReadFromChassis(X2_Command.WavelengthNominal));
        }

        public void SetWavelengthTolerance(int n)
        {
            WriteToChassis(X2_Command.WavelengthTolerance, ToHex(n));
        }
        public int GetWavelengthTolerance()
        {
            return HexToUInt(ReadFromChassis(X2_Command.WavelengthTolerance));
        }
        public void SetMaxCaseTemp(byte n)
        {
            WriteToChassis(X2_Command.MaxCaseTemp, ToHex(n));
        }
        public byte GetMaxCaseTemp()
        {
            return HexToByte(ReadFromChassis(X2_Command.MaxCaseTemp));
        }

        public void SetCC_Base()
        {
            int check = 0;

            char[] memCell = new char[64];
            for (int i = 0; i < 4; i++)
            {
                string request = "R01," + ToHex((byte)(0x80+i*16)) + "," + "10?";
                this.instrumentChassis.WriteLine(request, this);
                string response = "";
                int delayCount = 0;
                while (response.Length == 0 && delayCount < 100)
                {
                    System.Threading.Thread.Sleep(100);
                    response = this.instrumentChassis.Read(this);
                    delayCount++;
                }

                string finalResponse = ValidateResponse(request, response);
               string temp= HexToString(finalResponse);
               int a = temp.Length;
               
                temp.ToCharArray().CopyTo(memCell, i * 16);
            }
            for (int i = 0; i < 63; i++) 
            {
                check += (byte)memCell[i];
            }
            check &= 255;
            WriteToChassis(X2_Command.CC_Base, ToHex((byte)check)); 
        }
        public void SetCC_Ext()
        {
            int check = 0;

            char[] memCell = new char[32];
            for (int i = 0; i < 2; i++)
            {
                string request = "R01," + ToHex((byte)(0xC0+i*16)) + "," + "10?";
                this.instrumentChassis.WriteLine(request, this);
                string response = "";
                int delayCount = 0;
                while (response.Length == 0 && delayCount < 100)
                {
                    System.Threading.Thread.Sleep(100);
                    response = this.instrumentChassis.Read(this);
                    delayCount++;
                }

                string finalResponse = ValidateResponse(request, response);
                string temp = HexToString(finalResponse);
                int a = temp.Length;

                temp.ToCharArray().CopyTo(memCell, i * 16);
            }
            for (int i = 0; i < 31; i++)
            {
                check += (byte)memCell[i];
            }
            check &= 255;
            WriteToChassis(X2_Command.CC_Ext, ToHex((byte)check));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SetMarketCode(string code)
        {
            if (code.Length != 27) code.PadRight(27, ' ');
            WriteToChassis(X2_Command.CN4200_Code,ToHex(code));
        }
        public string GetMarketCode()
        {
            return HexToString(ReadFromChassis(X2_Command.CN4200_Code)).Trim();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SetCleiCode(string code)
        {
            if (code.Length != 10) code.PadRight(10, ' ').Substring(0,10);
            WriteToChassis(X2_Command.CLEI_Code,ToHex(code));
        }
        public string GetCleiCode()
        {
            return HexToString(ReadFromChassis(X2_Command.CLEI_Code)).Trim();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SetECICode(string code)
        {
            
            WriteToChassis(X2_Command.ECI_In_BCD, code);
        }
        public string GetECICode()
        {
            return ReadFromChassis(X2_Command.ECI_In_BCD);
            
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SetPartCodeRevision(string code)
        {
            if (code.Length != 3) code.PadRight(3, ' ').Substring(0, 3);
            WriteToChassis(X2_Command.PartCodeRevision,ToHex(code));
        }
        public string GetPartCodeRevision()
        {
            return HexToString(ReadFromChassis(X2_Command.PartCodeRevision));
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="firmwareAddress"></param>
        /// <returns></returns>
        public void SetBandCode(string code)
        {
            WriteToChassis(X2_Command.Reserved1, ToHex(code));
        }
        public string GetBandCode()
        {
            return "C";
        }


       public int GetEdcFirmware(int firmwareAddress)
        {
            WriteToChassis(X2_Command.EDC_REGISTER_ADDRESS, ToHex(firmwareAddress));
            System.Threading.Thread.Sleep(200);
            return HexToUInt(ReadFromChassis(X2_Command.EDC_REGISTER_VALUE));
	}

    /// <summary>
    /// Gets EDC Firmware Version
    /// </summary>
    /// <returns>EDC Firmware Version</returns>
    public int GetEDCFirmwareVersion(int addr)
    {
        string edcVersionAddressString = ToHex(addr);
        WriteToChassis(X2_Command.EDC_REGISTER_ADDRESS, edcVersionAddressString);
        System.Threading.Thread.Sleep(300);
        int version = HexToUInt(ReadFromChassis(X2_Command.EDC_REGISTER_VALUE));
        return version;
    }

    /// <summary>
    /// Gets EDC Firmware Version
    /// </summary>
    /// <returns>EDC Firmware Version</returns>
    public string GetEDCFirmwareVersion()
    {
        string edcVersionAddressString = "8A02";
        WriteToChassis(X2_Command.EDC_REGISTER_ADDRESS, edcVersionAddressString);
        System.Threading.Thread.Sleep(300);
        string version = ReadFromChassis(X2_Command.EDC_REGISTER_VALUE);
        return version;
    }


 	public void SetPowerRequirement(string hexString)
        {
            WriteToChassis(X2_Command.PowerRequirement,hexString);
        }

        #endregion

        #region public helper function
        public void WriteLargeArray(byte[] arr, int pageID)
        {
            if(arr.Length!=128)
                throw new InstrumentException("Input array have to have a length of 128");
            else if(pageID<1||pageID>6)
                throw new InstrumentException("page id must between 1 and 6");
            for (int i = 0; i < 8; i++)
            {
                string request = "W" +ToHex((byte)pageID) + ","+ToHex((byte)(i*16+128))+",";
                for (int j = 0; j < 16; j++)
                {
                    request += ToHex(arr[i*16+j]);
                }
                this.instrumentChassis.WriteLine(request, this);
                string response = this.instrumentChassis.ReadLine(this);
                string responseData = ValidateResponse(request, response);
                if (!responseData.Equals(""))
                    throw new Exception("Invalid response to data write. Request = '" + request + "', response = '" + responseData + "'");
                System.Threading.Thread.Sleep(40);
                
            }
        }

        public byte SetBytebit(byte a,int b,bool state)
        {
            byte temp = 0;
            if (state) temp = 1;
            temp <<= b;
            a &= (byte)~temp;
            a |= temp;
            return a;
        }
        public int StartFreq
        {
            get
            {
                return this.startFreq;
            }
            set
            {
                this.startFreq = value;
            }
        }
        #endregion

        #region Private data
        byte[] defaultPassword = { 0x49, 0x4C, 0x4F, 0xC2 };
        int startFreq = 191500;
        int chanSpacing = 50;
        #endregion


    }
}

Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class SerialBinaryComms
	
	
	' Standard OIF error codes
	Private Const OIF_OK As Short = &H0s ' OK, No errors
	Private Const OIF_RNI As Short = &H1s ' Register Not Implemented
	Private Const OIF_RNW As Short = &H2s ' Register Not Writeable
	Private Const OIF_RVE As Short = &H3s ' Register Value range error
	Private Const OIF_CIP As Short = &H4s ' Command ignored due to pending operation
	Private Const OIF_CII As Short = &H5s ' Command ignored while module initialising
	Private Const OIF_ERE As Short = &H6s ' Extended address Range Error
	Private Const OIF_ERO As Short = &H7s ' Extended address Read Only
	Private Const OIF_EXF As Short = &H8s ' EXecution general Failure
	Private Const OIF_CIE As Short = &H9s ' Command ignored whilst optical ouput enabled
	Private Const OIF_IVC As Short = &HAs ' INvalid Configuration, command ignored
	Private Const OIF_VSE As Short = &HFs ' Vendor Specific Error
	
	' OIF Packet status.
	'Private Const OIF_OK = 0           ' OK, No errors
	Private Const OIF_XE As Short = 1 ' Execution Error
	Private Const OIF_AEA As Short = 2 ' Automatic Extended Addressing result
	Private Const OIF_CP As Short = 3 ' command not complete, pending
	
	' Application defined error codes
	Private Const ERR_UNKNOWN As Short = &HE4s ' unhandled error
	
	Private Const CE_MASK As Byte = &H8s
	
	Private PortAddr As Short
	Private PortSpeed As String
	Private PortParity As String
	Private PortDataBits As String
	Private PortStopBits As String
	Private Terminator As String
	Private CommsMode As GlobalData.CommsModeType
	Private LastMessageSent(30) As Byte
	
	
	
	
	' ############## Properties ##############
	
	
	
	
	Public Property PortAddress() As Short
		Get
			PortAddress = PortAddr + 1
		End Get
		Set(ByVal Value As Short)
			PortAddr = Value - 1
		End Set
	End Property
	
	
	
	Public ReadOnly Property mode() As GlobalData.CommsModeType
		Get
			mode = CommsMode
		End Get
	End Property
	
	
	Public ReadOnly Property speed() As String
		Get
			speed = PortSpeed
		End Get
	End Property
	
	
	Public ReadOnly Property parity() As String
		Get
			parity = PortParity
		End Get
	End Property
	
	
	Public ReadOnly Property databits() As String
		Get
			databits = PortDataBits
		End Get
	End Property
	
	
	Public ReadOnly Property stopbits() As String
		Get
			stopbits = PortStopBits
		End Get
	End Property
	
	
	Public WriteOnly Property InputTerminator() As String
		Set(ByVal Value As String)
			Terminator = Value
		End Set
	End Property
	
	
	' ############## Public Procedures Invalid In This Class ##############
	
	Public Function Init() As Boolean
		' v2.2.25 - serial comms to the module is not currently supported.
		Init = False
	End Function
	
	
	
	Public Function Inst_Write(ByRef CommandString As String) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.logFATAL("SOFTWARE ERROR: Call to Inst_Write in Serial Binary comms.")
		Inst_Write = False
	End Function
	
	
	
	Public Function Inst_Read() As String
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.logFATAL("SOFTWARE ERROR: Call to Inst_Read in Serial Binary comms.")
		Inst_Read = strGENERAL_ERROR_CODE
	End Function
	
	
	
	Public Function WriteCal(ByRef reg As Byte, ByRef value As Double, ByRef factor As Double, Optional ByRef indx As Object = Nothing) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.logFATAL("SOFTWARE ERROR: Call to WriteCal in Serial Binary comms.")
		WriteCal = False
	End Function
	
	
	
	Public Function ReadCal(ByRef reg As Byte, ByRef factor As Double, Optional ByRef indx As Object = Nothing) As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.logFATAL("SOFTWARE ERROR: Call to ReadCal in Serial Binary comms.")
		ReadCal = GENERAL_ERROR_CODE
	End Function
	
	
	Public Function SendMSA(ByRef bytCommand As Byte, ByRef params() As Byte, ByRef ParamLen As Byte, ByRef RxData() As Byte, ByRef RxLen As Byte, Optional ByRef ActualLen As Byte = 0) As Boolean
		' Comms between the PIC and the Atmel on the Wideband and Fullband boards uses OIF, and this
		' is sent by hiding the OIF commands in an MSA format (F6/7 <len> 05 <oifreg> <data1> <data2>).
		' If we dig out the OIF bit and send it via the correct interface we can retain the existing
		' structure of the rest of the GUI and accompanying text files.
		Dim intIndex As Short
		Dim oifsend(3) As Byte
		If ((bytCommand = SETINDEXEDCAL) Or (bytCommand = GETINDEXEDCAL)) And (params(LBound(params)) = OIFDEVICEREG) Then
			For intIndex = 1 To 3
				oifsend(intIndex) = params(LBound(params) + intIndex)
			Next intIndex
			If bytCommand = SETINDEXEDCAL Then
				oifsend(0) = OIFWRITECOMMAND
			Else
				oifsend(0) = OIFREADCOMMAND
			End If
			SendMSA = SendOIFCommand(oifsend, RxData, RxLen)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logFATAL("SOFTWARE ERROR: Call to SendMSA in Serial Binary comms.")
			SendMSA = False
		End If
	End Function
	
	
	Public Function SendReceive(ByVal outp As String, ByVal searchkey As String, ByRef reply As String, Optional ByRef wait As Short = 0) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.logFATAL("SOFTWARE ERROR: Call to SendReceive in Serial Binary comms.")
		SendReceive = False
	End Function
	
	
	
	' ############## Public Procedures ##############
	
	
	Public Function OpenComms(ByRef addr As Short, ByRef config As String) As Boolean
		' Expects addr=1-4 for port com1-4.
		' Configuration is done in the MainForm; all we do here is save the address.
		' This is equivalent to the PortAddress Property and is for compatability only.
		Dim mode As String
		Dim pos1 As Short
		Dim pos2 As Short
		
		On Error GoTo ErrorHandler
		
		If (addr <= 0) Or (addr >= 5) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Comms port number out-of-range (" & CStr(addr) & ").")
			GoTo Aborted
		End If
		
		PortAddr = addr - 1
		CommsMode = GlobalData.CommsModeType.SERIALBINARYMODE
		
		pos1 = InStr(1, config, ",", CompareMethod.Text)
		mode = LCase(Mid(config, 1, pos1 - 1))
		If mode <> "binary" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Invalid mode for port " & CStr(addr) & ".")
			GoTo Aborted
		End If
		
		pos2 = InStr(pos1 + 1, config, ",", CompareMethod.Text)
		PortSpeed = Mid(config, pos1 + 1, pos2 - 1)
		PortParity = Mid(config, pos2 + 1, 1)
		PortDataBits = Mid(config, pos2 + 3, 1)
		PortStopBits = Mid(config, pos2 + 5)
		
		OpenComms = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "OpenComms (Serial address = " & CStr(addr) & ")")
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logMESSAGE("Error: Failed to open com" & CStr(addr))
		PortAddr = 999 'set offline
		OpenComms = False
	End Function
	
	
	
	
	Public Function SendCommand(ByRef TxData() As Byte, ByRef TxDatalen As Byte) As Boolean
		' TxDatalen unused at this time.
		Dim outp As String
        Dim lb As Short
        Dim ub As Short
		Dim indx As Short
		Dim snd(3) As Byte
		
		On Error GoTo ErrorHandler
		
        lb = LBound(TxData) + 1
        ub = UBound(TxData)
        If (ub - lb + 1) < 4 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Logger.logFATAL("SendCommand Error: TxData array < 4 bytes long.")
            SendCommand = False
        Else
            ' Copy the TxData into a local array to ensure that we only send 4 bytes. Any more will
            ' screw up the Atmel.
            outp = ""
            For indx = 0 To 3
                LastMessageSent(indx) = TxData(lb + indx) ' Save in case of resends being required.
                snd(indx) = TxData(lb + indx)
            Next indx
            Call BIP4_Append(snd)
            For indx = 0 To 3
                If snd(indx) >= 16 Then
                    outp = outp & " " & Hex(snd(indx))
                Else
                    outp = outp & " 0" & Hex(snd(indx))
                End If
            Next indx
            'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Call Logger.logMESSAGE("Sent:" & outp)

            ' Clear the input buffer send new data.
            SerialPortComms.DiscardInBuffer()
            SerialPortComms.Write(snd, 0, 4)
            SendCommand = True
        End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SendCommand")
		SendCommand = False
	End Function
	
	
	
	
	
	Public Function ReceiveReply(ByRef RxData() As Byte, ByRef RxDatalen As Byte) As Boolean
		' RxDatalen unused at this time.
		Dim inp As String
		Dim lb As Short
		Dim tout As Double
		Dim retrycnt As Short
        Dim inbuff(4) As Byte
		Dim indx As Short
		
		On Error GoTo ErrorHandler
		
		ReceiveReply = False
		
		'    If MainForm.MSComms(PortAddr).PortOpen = True Then
		If 1 = 1 Then
            lb = LBound(RxData) + 1
            If (UBound(RxData) - lb + 1) < 4 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Logger.logFATAL("ReceiveReply Error: RxData array < 4 bytes long.")
            Else
                retrycnt = 3
                Do
                    ' Wait until there are 4 bytes from the serial port in the input buffer.
                    tout = VB.Timer()
                    'UPGRADE_WARNING: Couldn't resolve default property of object MainFormMSComms.InBufferCount. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Do Until (SerialPortComms.BytesToRead = 4) Or (VB.Timer() > tout + 1)
                        'System.Windows.Forms.Application.DoEvents()
                    Loop

                    If (VB.Timer() > tout + 1) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Logger.logFATAL("ReceiveReply: Timeout.")
                        retrycnt = 0
                    Else
                        ' Read 4 bytes into inbuff array.
                        SerialPortComms.Read(inbuff, 0, 4)
                        inp = ""
                        For indx = 0 To 3
                            RxData(lb + indx) = inbuff(indx)
                            If inbuff(indx) >= 16 Then
                                inp = inp & " " & Hex(inbuff(indx))
                            Else
                                inp = inp & " 0" & Hex(inbuff(indx))
                            End If
                        Next indx
                        'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Call Logger.logMESSAGE("Rcvd:" & inp)

                        ' Check the CE bit and the checksum. If the CE bit is set then resend the packet.
                        ' If the resend fails just drop out, otherwise decrement the retry count.
                        If (inbuff(0) And CE_MASK) <> 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Logger.logFATAL("ReceiveReply: CE received - resending.")
                            If Not SendCommand(LastMessageSent, 4) Then
                                retrycnt = 0
                                'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Logger.logFATAL("ReceiveReply: Failed resend for CE error.")
                            Else
                                retrycnt = retrycnt - 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If retrycnt <= 0 Then Logger.logFATAL("ReceiveReply: Repeated CE error at module.")
                            End If
                        Else
                            If BIP4_check(inbuff) Then
                                ReceiveReply = True
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Logger.logFATAL("ReceiveReply: Checksum failure.")
                                retrycnt = 0
                            End If
                        End If
                    End If
                Loop While (ReceiveReply = False) And (retrycnt > 0)
            End If
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logFATAL("COM" & VB6.Format(PortAddr + 1, "#0") & " not open.")
			ReceiveReply = False
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReceiveReply")
		ReceiveReply = False
	End Function
	
	
	
	
	Public Function WriteOIF(ByRef reg As Byte, ByRef value As Double, ByRef factor As Double) As Boolean
		' This procedure, and the equivalent read function, are generic routines which can
		' write or read many data values depending on the multiplication factor. Allowing
		' factor to be a double takes account of values which are divided, e.g. the loop
		' PID coefficients, by entering for example 0.1 as the factor.
		Dim lngval As Integer
		'UPGRADE_WARNING: Lower bound of array lngtemp was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim lngtemp(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFWRITECOMMAND
		Send(2) = reg
		
		If factor = 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in WriteOIF: resetting to 1.")
			factor = 1
		End If
		
		lngval = CInt(value * factor)
		Call ConvLongToBytes(lngtemp, lngval)
		Send(3) = lngtemp(3)
		Send(4) = lngtemp(4)
		
		WriteOIF = SendOIFCommand(Send, Receive, 2)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteOIF &H" & Hex(reg) & " " & CStr(value))
Aborted: 
		WriteOIF = False
	End Function
	
	
	
	
	
	Public Function ReadOIF(ByRef reg As Byte, ByRef sdata As Short, ByRef factor As Short) As Double
		' Note that "sdata" is normally zero except for commands such as "Channel Power"
		' which require the channel number. The data must be converted to an integer
		' value by the caller.
		Dim dblval As Double
		Dim intval As Short
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim ReturnCode As Byte
		Dim hexstr As String
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFREADCOMMAND
		Send(2) = reg
		Call ConvIntToBytes(temp, sdata)
		Send(3) = temp(1)
		Send(4) = temp(2)
		
		If Not SendOIFCommand(Send, Receive, 2) Then
			ReadOIF = GENERAL_ERROR_CODE
		Else
			If factor = 0 Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("SOFTWARE ERROR: factor=0 in ReadOIF: resetting to 1.")
				factor = 1
			End If
			
			If factor > 0 Then
				dblval = CDbl(Receive(1)) * 256# + CDbl(Receive(2))
				ReadOIF = dblval / factor
			Else
				intval = ConvBytesToInt(Receive)
				ReadOIF = CDbl(intval) / CDbl(-factor)
			End If
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadOIF &H" & Hex(reg))
Aborted: 
		ReadOIF = GENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function WriteByteOIF(ByRef reg As Byte, ByRef upper As Byte, ByRef lower As Byte) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(5) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(10) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFWRITECOMMAND
		Send(2) = reg
		Send(3) = upper
		Send(4) = lower
		
		WriteByteOIF = SendOIFCommand(Send, Receive, 2)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteByteOIF &H" & Hex(reg))
Aborted: 
		WriteByteOIF = False
	End Function
	
	
	
	
	Public Function ReadByteOIF(ByRef reg As Byte, ByRef upper As Byte, ByRef lower As Byte) As Boolean
		' Just reads and returns the byte values.
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(5) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(10) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo ErrorHandler
		
		Send(1) = OIFREADCOMMAND
		Send(2) = reg
		
		If Not SendOIFCommand(Send, Receive, 2) Then
			ReadByteOIF = False
		Else
			upper = Receive(1)
			lower = Receive(2)
			ReadByteOIF = True
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadByteOIF &H" & Hex(reg))
Aborted: 
		ReadByteOIF = False
	End Function
	
	
	
	
	
	Public Function WriteOIFString(ByVal reg As Byte, ByVal oifstr As String, ByVal cnt As Short) As Boolean
		' Strings usually have to be terminated with nulls, but since only the caller knows how long
		' their string is we have to take a length parameter.
		Dim lenf As Short
		Dim indx As Short
		Dim junk As String
		Dim d1 As Byte
		Dim d2 As Byte
		'UPGRADE_WARNING: Lower bound of array d was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim d(260) As Byte
		
		On Error GoTo ErrorHandler
		
		lenf = Len(oifstr)
		If lenf > cnt Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Warning: '" & oifstr & "' is longer than the specified count in WriteOIFString.")
		End If
		If cnt > 256 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Warning: Count exceeds 256 in WriteOIFString.")
			cnt = 256
		End If
		
		' Copy from the string to an array and pad out.
		For indx = 1 To cnt + 3
			If indx > lenf Then
				d(indx) = 0
			Else
				d(indx) = Asc(Mid(oifstr, indx, 1))
			End If
		Next indx
		
		' Send the count to the module.
		If Not WriteOIF(reg, CDbl(cnt), 1) Then GoTo Aborted
		
		' Now send the remaining data in byte pairs.
		For indx = 1 To (cnt + 1) \ 2
			If Not WriteByteOIF(OIFAEAREG, d(2 * indx - 1), d(2 * indx)) Then Exit For 'GoTo Aborted
		Next indx
		WriteOIFString = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WriteOIFString &H" & Hex(reg))
Aborted: 
		WriteOIFString = False
	End Function
	
	
	
	
	
	Public Function ReadOIFString(ByRef reg As Byte) As String
		Dim cnt As Short
		Dim indx As Short
		Dim junk As String
		Dim d1 As Byte
		Dim d2 As Byte
		
		On Error GoTo ErrorHandler
		
		cnt = ReadOIF(reg, 0, 1)
		If cnt = GENERAL_ERROR_CODE Then
			GoTo Aborted
		Else
			If cnt <= 0 Then
				' Don't know why (or if) this would ever happen. What do you do with
				' a zero length string?
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Call Logger.logMESSAGE("WARNING: Zero length AEA returned.")
				GoTo Aborted
			Else
				junk = ""
				For indx = 1 To (cnt + 1) \ 2
					If Not ReadByteOIF(OIFAEAREG, d1, d2) Then
						GoTo Aborted
					Else
						junk = junk & Chr(d1) & Chr(d2)
					End If
				Next indx
				ReadOIFString = junk
			End If
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadOIFString &H" & Hex(reg))
Aborted: 
		ReadOIFString = strGENERAL_ERROR_CODE
	End Function
	
	
	
	
	
	
	' ############## Private Procedures ##############
	
	
	
	
	
	
	
	Private Function SendOIFCommand(ByRef TxData() As Byte, ByRef RxData() As Byte, ByRef RxLen As Byte) As Boolean
		' RxLen is the expected number of bytes of data. It is expected to be zero or non-zero,
		' indicating that data either is or isn't (=0) expected back from the module.
		' Note: we do not check the Response bit at this time (bit 2 byte 0 of the reply).
		Dim lbtx As Short
		Dim lbrx As Short
		Dim ubrx As Short
		Dim pktstatus As Byte
		Dim indx As Short
		'UPGRADE_WARNING: Lower bound of array Rx was changed from 1 to 0. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Rx(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo ErrorHandler
		
        lbtx = LBound(TxData) + 1
        lbrx = LBound(RxData) + 1
        ubrx = UBound(RxData)
		
		If SendCommand(TxData, 4) Then
			If ReceiveReply(Rx, 4) Then
				' Check the packet status field, bits 1:0 of byte 1.
				pktstatus = Rx(1) And 3
				Select Case pktstatus
					Case OIF_OK ' OK, continue.
						ReturnCode = OIF_OK
					Case OIF_XE ' Oops! Read the NOP to find out what went wrong.
						ReturnCode = DetermineErrorReason ' Also outputs to the logger.
					Case OIF_AEA ' We assume that the caller expects this so send back a good status.
						ReturnCode = OIF_OK
					Case OIF_CP ' Read the NOP until the pending goes away or we error.
						' Rx(lb + 2) holds the bit indicating the pending operation.
						If WaitForPending(Rx(lbrx + 2)) Then
							ReturnCode = OIF_OK
						Else
							ReturnCode = ERR_UNKNOWN
						End If
				End Select
				
				If ReturnCode = OIF_OK Then
					If TxData(lbtx + 1) <> Rx(2) Then
						'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						Logger.logFATAL("OIF: Rx/Tx registers are different.")
						SendOIFCommand = False
					Else
						' The user only wants the data, not all the crud that goes with it.
						If (ubrx - lbrx) < 1 Then
							'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							Logger.logFATAL("OIF: RxData array too small - minimum size is 2 bytes.")
							SendOIFCommand = False
						Else
							RxData(lbrx) = Rx(3)
							RxData(lbrx + 1) = Rx(4)
							SendOIFCommand = True
						End If
					End If
				Else
					SendOIFCommand = False
				End If
			Else
				SendOIFCommand = False
			End If
		Else
			SendOIFCommand = False
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SendOIFCommand")
Aborted: 
		SendOIFCommand = False
	End Function
	
	
	
	
	Private Sub BIP4_Append(ByRef outpkt() As Byte)
		' Calculates and adds the BIP4 checksum to the OIF output packet (top 4 bits of byte 1).
		Dim intermed As Byte
		Dim lb As Short
		lb = LBound(outpkt)
		outpkt(lb + 0) = outpkt(lb + 0) And &HFs ' Zero top nibble
		intermed = outpkt(lb + 0) Xor outpkt(lb + 1) Xor outpkt(lb + 2) Xor outpkt(lb + 3) 'Xor all packets.
		intermed = (intermed And &HFs) Xor (CShort(intermed And &HF0s) / 16) 'Xor top & bottom nibble of result
		outpkt(lb + 0) = outpkt(lb + 0) + (intermed * 16) ' Add to top nibble of outpkt(lb+0)
	End Sub
	
	
	
	
	Private Function BIP4_check(ByRef inpkt() As Byte) As Boolean
		' Calculates the inbound BIP4 checksum and compares against the checksum in the top 4 bits of
		' byte 1 of the inbound packet.
		Dim chkresult As Byte
		Dim lb As Short
		lb = LBound(inpkt)
		chkresult = inpkt(lb + 0) And &HFs ' Zero out existing BIP4
		chkresult = chkresult Xor inpkt(lb + 1) Xor inpkt(lb + 2) Xor inpkt(lb + 3) 'Xor all packets.
		chkresult = (chkresult And &HFs) Xor (CShort(chkresult And &HF0s) / 16) 'Xor top & bottom nibble of result
		chkresult = chkresult * 16 ' Move to top nibble of chkresult.
		If (inpkt(lb + 0) And &HF0s) <> chkresult Then BIP4_check = False Else BIP4_check = True
	End Function
	
	
	
	Private Function OIFErrorDescription(ByRef code As Byte) As String
		' Return a string containing a description of every error bit set in the code.
		' This function should be called to determine the reason for an error.
		' These codes are defined in the OIF v1.2.3.
		Select Case code
			Case OIF_OK
				OIFErrorDescription = "OK, No errors"
			Case OIF_RNI
				OIFErrorDescription = "Register not implemented"
			Case OIF_RNW
				OIFErrorDescription = "Register not writeable"
			Case OIF_RVE
				OIFErrorDescription = "Register value range error"
			Case OIF_CIP
				OIFErrorDescription = "Command ignored due to pending operation"
			Case OIF_CII
				OIFErrorDescription = "Command ignored while module initialising"
			Case OIF_ERE
				OIFErrorDescription = "Extended address range error"
			Case OIF_ERO
				OIFErrorDescription = "Extended address read only"
			Case OIF_EXF
				OIFErrorDescription = "Execution general Failure"
			Case OIF_CIE
				OIFErrorDescription = "Command ignored while optical ouput enabled"
			Case OIF_IVC
				OIFErrorDescription = "Invalid configuration, command ignored"
			Case OIF_VSE
				OIFErrorDescription = "Vendor specific error"
			Case Else
				OIFErrorDescription = "UNKNOWN OIF ERROR CODE"
		End Select
	End Function
	
	
	
	Private Function DetermineErrorReason() As Byte
		' Read the NOP register to get the reason for the failure of the previous command.
		Dim nop(3) As Byte
		Dim ErrLogString As String
		
		On Error GoTo ErrorHandler
		
		nop(0) = OIFREADCOMMAND
		nop(1) = nop(2) = nop(3) = 0
		If Not SendCommand(nop, 4) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("OIF: Failed to request reason for previous failure.")
			DetermineErrorReason = ERR_UNKNOWN
		Else
			If Not ReceiveReply(nop, 4) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("OIF: Failed to receive reason for previous failure.")
				DetermineErrorReason = ERR_UNKNOWN
			Else
				nop(3) = nop(3) And OIFERRORFIELD_MASK
				ErrLogString = "OIF: Status=" & nop(3) & ": " & OIFErrorDescription(nop(3))
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL(ErrLogString)
				DetermineErrorReason = nop(3)
			End If
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "DetermineErrorReason")
		DetermineErrorReason = ERR_UNKNOWN
	End Function
	
	
	
	
	Private Function WaitForPending(ByRef pops As Byte) As Boolean
		' Read the NOP register until the pending operation has completed or we timeout.
		' Parameter "pops" is the pending ops byte from the previous reply.
        Dim nop_tx(4) As Byte
        Dim nop_rx(4) As Byte
		Dim tout As Double
		Dim pktstatus As Byte
		Dim ret_status As Boolean
		
		On Error GoTo ErrorHandler
		
        nop_tx(1) = OIFREADCOMMAND
        nop_tx(2) = nop_tx(3) = nop_tx(4) = 0
		tout = VB.Timer() + NOPTIMEOUT
		
		ret_status = False
		Do 
			' send NOP command
			If Not SendCommand(nop_tx, 4) Then
				' couldn't send packet, bail out
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("OIF: NOP send failure.")
				Exit Do
			End If
			
			' get reply from module
			If Not ReceiveReply(nop_rx, 4) Then
				' couldn't receive packet, bail out
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("OIF: NOP receive failure.")
				Exit Do
			End If
			
			' is received packet ok?
            pktstatus = nop_rx(1) And 3
			If pktstatus <> OIF_OK Then
				' unexpected NOP command status, bail out
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("OIF: NOP receive status error.")
				Exit Do
			End If
			
			' has pending operation finished?
            If (nop_rx(3) And pops) = 0 Then
                ' command has finished executing
                ret_status = True
            Else
                ' slow down the polling loop
                sDelay((0.5))

                ' any time left?
                If (VB.Timer() > tout) Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Logger.logFATAL("OIF: WaitForPending timeout.")
                    Exit Do
                End If
            End If
			
		Loop While ret_status = False
		
		WaitForPending = ret_status
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "WaitForPending")
Aborted: 
		WaitForPending = False
	End Function
End Class
Option Strict Off
Option Explicit On
Module OIFVendorRegisters
	
	
	' This module was intended to only contain the global data such as register values and
	' assorted constants for the DSDBR, with procedures in the class module, but since there
	' appears to be a problem with public types as parameters to class procedures I have
	' resorted to putting those procedures in here.
	
	Public Const PCBTEMPERATURE As Byte = &H81s ' r   deg/100
	Public Const OFFGRIDTUNING As Byte = &H82s ' rw  -8.0 to +8.0 in GHz*10
	Public Const FACTORYMODE As Byte = &H85s ' rw  0000=OIF  5A5A=text
	Public Const CAUSEOFLASTRESET As Byte = &H86s ' r
	
	' The following 4 registers allow a conversion from the "seti/geti/setv/getv" text functions to
	' similar I2C functions. The table numbers required by IVSOURCEINDEX are different to the normal
	' current/voltage table numbers (see IVSourceIndexNumbers in GlobalData).
	Public Const IVSOURCELSW As Byte = &H87s ' FCU  rw  LS 16 bits of the I/V value (see 89)
	Public Const IVSOURCEMSW As Byte = &H88s ' FCU  rw  MS 16 bits of the I/V value (see 89)
	Public Const IVSOURCEINDEX As Byte = &H89s ' FCU  rw  Index to the specific source table.
	Public Const WRITESOURCETODAC As Byte = &H8As ' FCU  w   Write anything causes dac to be written.
	
	Public Const BKHMWAVECODE As Byte = &H90s ' r   GHz - 180000
	Public Const STORECHANNEL As Byte = &H91s '  w  Channel = 1 to N
	Public Const SOADACREG As Byte = &H92s ' rw  -4095 to +4095 (ITLA=65535)
	Public Const GAINDACREG As Byte = &H93s ' rw  0 to 4095 (ITLA=65535)
	Public Const FRONTEVENDACREG As Byte = &H94s ' rw  0 to 4095 (ITLA=65535)
	Public Const FRONTODDDACREG As Byte = &H95s ' rw  0 to 4095 (ITLA=65535)
	Public Const REARDACREG As Byte = &H96s ' rw  0 to 4095 (ITLA=65535)
	Public Const PHASEDACREG As Byte = &H97s ' rw  0 to 4095 (ITLA=65535)
	Public Const DITHERDACREG As Byte = &H98s ' rw  0 to 4095
	Public Const PCBTEMPSENSOR As Byte = &H99s ' r   deg/16
	Public Const TEMPOUTLEVEL As Byte = &H9As ' r   0 to 1023
	Public Const MODITEC As Byte = &H9As ' FCU  r   0 to 1023
	Public Const VTECLEVEL As Byte = &H9Bs ' r   0 to 1023
	Public Const LASERITEC As Byte = &H9Bs ' FCU  r   0 to 1023
	Public Const LSBIASSENSELEVEL As Byte = &H9Cs ' r   0 to 1023
	Public Const V3A3SENSELEVEL As Byte = &H9Ds ' r   0 to 1023
	Public Const LOCKERTRANSMITMON As Byte = &H9Es ' r   0 to 65535
	Public Const LOCKERREFLECTMON As Byte = &H9Fs ' r   0 to 65535
	
	Public Const LOCKERERRORMON As Byte = &HA0s ' r   0 to 65535
	Public Const LASERPOWERMON As Byte = &HA1s ' r   0 to 65535
	Public Const LSPHASESENSE As Byte = &HA2s ' r   0 to 65535
	Public Const LOCKERREFERENCE As Byte = &HA3s ' r   0 to 65535
	Public Const RAWTHERMBRIDGE As Byte = &HA4s ' FCU  r   IC715
	Public Const RAWTHERMBRIDGEALT As Byte = &HA5s ' FCU  r   IC2
	Public Const TRANSMITFINEPOT As Byte = &HA6s ' rw  0 to 255
	Public Const TRANSMITCOARSEPOT As Byte = &HA7s ' rw  0 to 255
	Public Const REFLECTCOARSEPOT As Byte = &HA8s ' rw  0 to 255  (current value - see C9 et al)
	Public Const LOCKERRANGEPOT As Byte = &HA9s ' rw  0 to 255
	Public Const POWERMONPOT As Byte = &HAAs ' rw  0 to 255
	Public Const PHASEMINSETPOINT As Byte = &HABs ' rw  0 to 65535
	Public Const PHASEMAXSETPOINT As Byte = &HACs ' rw  0 to 65535
	Public Const POWERMONFACTOR As Byte = &HADs ' rw  mW light / mA detector
	Public Const LOCKERERRORFACTOR As Byte = &HAEs ' rw
	
	Public Const TILETEMPSETPOINT As Byte = &HB0s ' r
	Public Const PREVIOUSOPTIMING As Byte = &HB1s ' r   millisecs
    Public Const ATMELREFERENCE As Byte = &HB2          ' r WBTT specific. Register B2 has now been reallocated on ITLA/TTA
    Public Const LOWPOWERETALONSLOPE As Byte = &HB2     ' rw
	Public Const LASERCHANNEL As Byte = &HB3s ' rw  Causes a laser tune
	Public Const CALCONTROLREG As Byte = &HB4s ' rw
	Public Const LASERPOWERESTIMATE As Byte = &HB5s ' r   uW
	Public Const POWERMONITOROFFSET As Byte = &HB6s ' rw
	Public Const CURRENTSOURCENUMBER As Byte = &HB7s ' rw  Current cal source index (0-7)
	Public Const CURRENTSOURCEINDEX As Byte = &HB8s ' rw  Current index (0-164)
	Public Const CURRENTSOURCELSW As Byte = &HB9s ' rw  LS 16 bits of the current value
	Public Const CURRENTSOURCEMSW As Byte = &HBAs ' rw  MS 16 bits of the current value
	Public Const SBSDITHERMULTIPLIER As Byte = &HBBs
	Public Const VOLTAGESOURCENUMBER As Byte = &HBCs ' FCU  rw  Voltage cal source index (8-11)
	Public Const VOLTAGESOURCEINDEX As Byte = &HBDs ' FCU  rw  Voltage index (0=slope  1=offset)
	Public Const VOLTAGESOURCELSW As Byte = &HBEs ' FCU  rw  LS 16 bits of the voltage value
	Public Const WAVELENGTHERROR As Byte = &HBFs ' r   MHz/256
	
	Public Const BASECAMPDAC As Byte = &HC0s ' rw  DAC value for first profile point.
	Public Const BASECAMP2DAC As Byte = &HC1s ' rw  DAC value for second profile point.
	Public Const PLATEAUPOWER As Byte = &HC2s ' rw  Power in uW at SOA dac=0.
	Public Const SINKSLOPE As Byte = &HC3s ' rw  Slope before the plateau in dac/mW.
	Public Const SOURCESLOPE As Byte = &HC4s ' rw  Slope after the plateau in dac/mW.
	Public Const UNITSERIALNUMBER As Byte = &HC5s ' ITLA  w  20 char null padded string.
	Public Const UNITBUILDDATE As Byte = &HC6s ' ITLA  w  12 char null padded string.
	Public Const UNITMANUFACTURER As Byte = &HC7s ' ITLA  w  20 char null padded string.
	Public Const MODULEUNLOCK As Byte = &HC8s ' ITLA  w  Null terminated string "DSDBR123".
	Public Const OFFGRIDPOSITIVE As Byte = &HC9s ' ITLA rw  0 to 1023 Reflect Coarse Pot for +8GHz.
	Public Const OFFGRIDNEGATIVE As Byte = &HCAs ' ITLA rw  0 to 1023 Reflect Coarse Pot for -8GHz.
	Public Const CHANNELCENTRE As Byte = &HCBs ' ITLA rw  0 to 1023 Reflect Coarse Pot for centre.
	Public Const POWERCONTROLLOOP As Byte = &HCFs ' ITLA  rw  0=off 1=on
	Public Const READMZPOWER As Byte = &HCFs ' FCU  r   0 to 4095  ADC_MMI IC20-6
	
	Public Const MODDRIVERCONTROL As Byte = &HD0s ' FCU  rw  0 or 1
	Public Const LASERSERIALNUMBER As Byte = &HD1s ' ITLA rw  20 char null padded string.
	Public Const LASERTECDAC As Byte = &HD1s ' FCU  rw  0 to 4095
	Public Const BOARDSERIALNUMBER As Byte = &HD2s ' ITLA rw  20 char null padded string.
	Public Const PULSEWIDTHCTRL As Byte = &HD2s ' FCU  rw  0 to 4095  PWC IC18 Pin 6
	Public Const LEFTMZBIAS As Byte = &HD3s ' FCU  rw  0 to 4095  LEFT_MZ_BIAS IC18 Pin 7
	Public Const RIGHTMZBIAS As Byte = &HD4s ' FCU  rw  0 to 4095  RIGHT_MZ_BIAS IC18 Pin 8
	Public Const MZMODULATION As Byte = &HD5s ' FCU  rw  0 to 4095  MZ_MODULATION IC18 Pin 9
	Public Const BACKPLUS As Byte = &HD6s ' FCU  rw
	Public Const LEFTIMBALANCE As Byte = &HD7s ' FCU  rw  0 to 4095
	Public Const RIGHTIMBALANCE As Byte = &HD8s ' FCU  rw  0 to 4095
	Public Const MINPOWERSETPOINT As Byte = &HDAs ' ITLA+WB3  rw  dBm*100
	Public Const MMIDIFFOFFSET As Byte = &HDAs ' FCU  rw  +/-32767
	Public Const MAXPOWERSETPOINT As Byte = &HDBs ' ITLA+WB3  rw  dBm*100
	Public Const WIGGLELEVEL As Byte = &HDBs ' FCU  rw  +/-32767
	Public Const BIASCURRENT As Byte = &HD9s ' r   Current value of the SOA current
	Public Const VOLTAGESOURCEMSW As Byte = &HDFs ' FCU  rw  MS 16 bits of the voltage value
	
	Public Const FIRMWAREREVISION As Byte = &HFEs ' rw  0xMMmm (Major/minor)
	
	
	' Bits in the MS Byte of the Control Register
	Public Const LOCKER As Byte = &H80s
	Public Const LOCKERSLOPE As Byte = &H40s
	Public Const TECONOFF As Byte = &H20s
	Public Const THERMISTERFAULT As Byte = &H10s
	Public Const MODTECONOFF As Byte = &H10s ' Replaces THERMISTERFAULT in the FCU.
	Public Const TEMPLOCK As Byte = &H8s ' Unused in the FCU.
	Public Const LASERPSU As Byte = &H4s
	Public Const EVEN1 As Byte = &H2s
	Public Const EVEN0 As Byte = &H1s
	
	' Bits in the LS Byte of the Control Register
	Public Const ODD1 As Byte = &H80s
	Public Const ODD0 As Byte = &H40s
	Public Const SOAPOWERCONTROL As Byte = &H20s
	Public Const AUTOLOCK As Byte = &H10s
	Public Const DITHER As Byte = &H8s
	Public Const REARSOURCE As Byte = &H4s
	Public Const PROFILEONOFF As Byte = &H2s
	
	
	' Bits in the Modulator Driver Control, reg D0.
	Public Const CLOCKSELECT As Byte = &H1s ' CLKSEL IC711 Pin 50   0=Low  1=High
End Module
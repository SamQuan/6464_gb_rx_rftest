Option Strict Off
Option Explicit On
Module MSAVendorRegisters
	
	
	' This class module holds a "user defined" set of MSA sub-commands using the codes
	' &HF4 (set calibration), &HF5 (get calibration) and &HFA (do action). These commands
	' are addressed to the module firmware in the PIC using the registers listed below.
	' Commands which can read or write to EEPROM have an optional string parameter which
	' should be set to "EEPROM" for non-volatile storage.
	
	
	' Straight CAL commands - F4/F5
	Public Const CALMZMOD As Byte = &H1s
	Public Const CALMZBIASN As Byte = &H2s
	Public Const SAVECALMZBIASN As Byte = &H82s ' Saves to non-volatile memory.
	Public Const CALVEE As Byte = &H3s
	Public Const CALTXPOWERMON As Byte = &H4s ' rw in microWatts
	Public Const CALAPDBIAS As Byte = &H5s
	Public Const CALXPN As Byte = &HAs
	Public Const RXPOWERMONSIZE As Byte = &HBs
	Public Const RXPOWMAXTABLESIZE As Byte = &HBs
	Public Const MODMONDIFF As Byte = &HCs
	Public Const MODMONPOWER As Byte = &HDs
	Public Const CALMZWIGGLE As Byte = &HEs
	Public Const MODOFFSETN As Byte = &HFs
	Public Const MZMODAMPLITUDEMON As Byte = &H10s
	Public Const CALMODTYPE As Byte = &H11s
	Public Const FIRSTCHANBAND As Byte = &H12s
	Public Const FIRSTCHANFREQ As Byte = &H13s
	Public Const LASTCHANBAND As Byte = &H14s
	Public Const LASTCHANFREQ As Byte = &H15s
	Public Const CHANSPACING As Byte = &H16s ' 8=25GHz  16=50GHz  32=100GHz
	Public Const OPTPOWRAW As Byte = &H18s
	Public Const CALRXTHRESHOLD As Byte = &H1Cs
	Public Const MZSTATE As Byte = &H1Ds
	Public Const DRIVESTATE As Byte = &H1Es
	Public Const CALMZBIAS As Byte = &H26s
	Public Const CALMZBIASP As Byte = &H27s
	Public Const CHIRP As Byte = &H28s
	Public Const CALXP As Byte = &H29s
	Public Const CALXPP As Byte = &H2As
	Public Const PINMODEBAND As Byte = &H2Bs
	Public Const PINMODEFREQ As Byte = &H2Cs
	Public Const MODOFFSET As Byte = &H2Es
	Public Const MODOFFSETP As Byte = &H2Fs
	
	Public Const BOLLASERPOWER As Byte = &HB0s ' rw in uW   "B0" sets EEPROM (reg 30 + top bit)
	
	
	' Indexed CAL commands - F6/F7
	Public Const RXPWRMONCALTABLE As Byte = &H0s ' r
	Public Const RXPOWERPOINTS As Byte = &H1s ' r  dBm * 100
	Public Const SERDESEEPROM As Byte = &H83s
	Public Const SERDESRAM As Byte = &H3s
	Public Const SERDESREGISTER As Byte = &H4s
	Public Const GETADC As Byte = &H6s ' See User Interface Manual - table 9
	
	
	' "Set String" commands - F8
	Public Const MODULESERIALNUMBER As Byte = &H0s
	Public Const MODULEMANUFACTUREDATE As Byte = &H1s
	Public Const MODULEHARDWAREREVISION As Byte = &H2s
	Public Const MODULEPARTNUMBER As Byte = &H3s
	
	
	' "DoAction" commands - FA
	Public Const ENABLETXDRIVE As Byte = &H0s
	Public Const DISABLETXDRIVE As Byte = &H1s
	Public Const ENABLEOIFLASER As Byte = &H6s
	Public Const DISABLEOIFLASER As Byte = &H7s
	Public Const MZCLOSEDLOOP As Byte = &HFs
	Public Const MZOPENLOOP As Byte = &H10s
	Public Const SAVEMODSETTINGS As Byte = &H11s
	Public Const RECALLMODSETTINGS As Byte = &H12s
	Public Const SAVERX As Byte = &H13s
	Public Const RECALLRX As Byte = &H14s
	Public Const ENABLEMODMONHIGHGAIN As Byte = &H19s
	Public Const DISABLEMODMONHIGHGAIN As Byte = &H1As
	Public Const SAVETOEEPROM As Byte = &H1Bs
	
	
	' This one might be in the wrong file.
	Public Const CHIRPCOMMAND As Byte = &HF1s
	Public Const NEGATIVECHIRP As Byte = &H0s
	Public Const POSITIVECHIRP As Byte = &H1s
	
	Public Const SETSTRING As Byte = &HF8s
	Public Const DOACTION As Byte = &HFAs
	
	Public Const CBANDCODE As Byte = &H43s
	Public Const LBANDCODE As Byte = &H4Cs
	
	Public Const EEPROMBIT As Byte = &H80s
End Module
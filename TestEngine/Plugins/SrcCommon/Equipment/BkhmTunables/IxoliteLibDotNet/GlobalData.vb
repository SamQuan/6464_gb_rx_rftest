Option Strict Off
Option Explicit On
Module GlobalData
	
	
	' ##########################################################################################
	' Global constants.
	
	Public Const TAG_POSTFIX As String = "_APPLICATION"
	Public Const APP_ID As String = "Ixolite"
	
	' General error flags.
	Public Const GENERAL_ERROR_CODE As Short = -9999
	Public Const strGENERAL_ERROR_CODE As String = "-9999"
	
	' Global colour scheme.
	Public Const GREEN As Integer = &HC000
	Public Const DARKGREEN As Integer = &H8000
	Public Const RED As Integer = &HFF
	Public Const YELLOW As Integer = &H80FFFF
	Public Const ORANGE As Integer = &HCCFF
	Public Const BLACK As Integer = &H0
	Public Const WHITE As Integer = &HFFFFFF
	Public Const GREY As Integer = &H808080
	Public Const MAUVE As Integer = &HFF00FF
    Public Const BLUE As Integer = &HFF0000
	
	
	'Public Const OIFLOCKREGISTER As Byte = &H16
	'Public Const OIFLOCKLEVEL3KEY As String = "devlLockLevel"
	
	Public Const OIFAEAREG As Byte = &HBs ' Needed in both serial and parallel comms.
	
	Public Const CBAND As String = "C"
	Public Const LBAND As String = "L"
	Public Const BANDERROR As String = "Error"
	Public Const MIN_ITU_CHANNEL_NUMBER As Short = 1
    Public Const MAX_ITU_CHANNEL_NUMBER As Short = 255 ' Changed for MSA v4.1/SX2
	Public Const MIN_ITU_CBAND_CHANNEL As Short = 1
	Public Const MAX_ITU_CBAND_CHANNEL As Short = 91
	Public Const MIN_ITU_LBAND_CHANNEL As Short = 92
    Public Const MAX_ITU_LBAND_CHANNEL As Short = 255
	
	' The min/max frequency are currently only used once in the Setup Form.
	Public Const MAX_ITU_FREQUENCY As Double = 196.1 ' = channel 1
	Public Const MIN_ITU_FREQUENCY As Double = 184.5 ' = channel 233   Changed for MSA v4.1
	
	Public Const MAX_MSA_PARAM_LENGTH As Byte = 18
	
	Public Const INI_COMMS As String = "comms"
	Public Const INI_SETUPFORM As String = "setupform"
	Public Const INI_CALFORM As String = "calform"
	Public Const INI_GENERAL As String = "general"
	Public Const INI_DEVICETYPES As String = "devicetypes"
	Public Const INI_DEVICECODES As String = "devicecodes"
	Public Const INI_LOGIN As String = "login"
	
	' NOP register masks.
	Public Const NOPPENDINGMASK As Integer = &HFF00s
	Public Const NOPLOCKEDMASK As Integer = &HC0s
	Public Const NOPMRDYMASK As Integer = &H10s
	Public Const NOPERRORMASK As Integer = &HFs
	
	
	Public Const HALFMODE As String = "on"
	Public Const QUARTERMODE As String = "off"
	Public Const FIRSTCHANMODE As Short = -1 ' Qtr chan = -1  Half chan = 1
	Public Const HALFCHANMODE As Short = 1
	Public Const QTRCHANMODE As Short = -1
	Public Const NEGFREQUENCYGRIDGHZ As Short = -50
	Public Const MINFREQUENCYGRIDGHZ As Short = 50
	Public Const MAXPOWERERROR As Double = 0.03 ' dBm
	Public Const OFFGRIDTUNINGLIMIT As Double = 8 ' +/-GHz from centre frequency.
	
	Public Const WBBOARDLINELENGTH As Short = 9 ' Dac, Odd, Even, Phase, R/Low, R/High, Gain, SOA/F, SOA/R
	Public Const FCUBOARDLINELENGTH As Short = 13 ' As WB plus CMZBias-Left/Right, CMZImbalance-Left/Right
	Public Const ITLABOARDLINELENGTH As Short = 9 ' As WB
	
	Public Const WBLINELENGTH As Short = 13 ' Chan, Soa, Gain, Phase, Rear, 8 * Front
	Public Const WB3LINELENGTH As Short = 13 ' As WB
	Public Const FCUCMZLINELENGTH As Short = 18 ' As WB + CMZLeft/Right AC/DC and CMZVPi for FCU with CMZ laser
	Public Const FCUDBRLINELENGTH As Short = 13 ' As WB for FCU board with DBR laser
	Public Const ITLALINELENGTH As Short = 13 ' As WB for ITLA board with DBR laser
	
	
	' ##########################################################################################
	' Global enums and types.
	
#If True = False Then
	'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression True = False did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
	' This is a fiddle which prevents VB from changing the case of the following enums if
	' they are typed in with a different case in the code.
	Dim FRONTODDCURRENT As Byte
	Dim FRONTEVENCURRENT As Byte
	Dim PHASECURRENT As Byte
	Dim REARLOWCURRENT As Byte
	Dim REARHIGHCURRENT As Byte
	Dim GAINCURRENT As Byte
	Dim SOAFORWARDCURRENT As Byte
	Dim SOAREVERSECURRENT As Byte
	Dim CMZLEFTBIASVOLTAGE As Byte
	Dim CMZRIGHTBIASVOLTAGE As Byte
	Dim CMZLEFTIMBALVOLTAGE As Byte
	Dim CMZRIGHTIMBALVOLTAGE As Byte
	Dim FRONTODDSOURCE As Byte
	Dim FRONTEVENSOURCE As Byte
	Dim REARSOURCE As Byte
	Dim PHASESOURCE As Byte
	Dim GAINSOURCE As Byte
	Dim SOASOURCE As Byte
	Dim LEFTSOURCE As Byte
	Dim RIGHTSOURCE As Byte
	Dim LEFTIMBALSOURCE As Byte
	Dim RIGHTIMBALSOURCE As Byte
#End If
	
	Public Enum CurrentNames
		FRONTODDCURRENT = 1 ' These enums are used to index the currents and
		FRONTEVENCURRENT ' DAC values for the currents.
		PHASECURRENT ' These are "F" and "FDAC" in the "Currents" type,
		REARLOWCURRENT ' and "Current" in the "IsrcCalibration" type.
		REARHIGHCURRENT
		GAINCURRENT
		SOAFORWARDCURRENT
		SOAREVERSECURRENT
		CMZLEFTBIASVOLTAGE
		CMZRIGHTBIASVOLTAGE
		CMZLEFTIMBALVOLTAGE
		CMZRIGHTIMBALVOLTAGE
	End Enum
	
	Public Enum IVSourceIndexNumbers
		FRONTODDSRCINDEX = 0 ' See OIF Vendor Registers 87-8A.
		FRONTEVENSRCINDEX = 1
		REARSRCINDEX = 2
		PHASESRCINDEX = 3
		GAINSRCINDEX = 4
		SOASRCINDEX = 16
		LEFTSRCINDEX = 10
		RIGHTSRCINDEX = 11
		LEFTIMBALSRCINDEX = 14
		RIGHTIMBALSRCINDEX = 15
	End Enum
	
	
#If True = False Then
	'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression True = False did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
	' This is a fiddle which prevents VB from changing the case of the following enums if
	' they are typed in with a different case in the code.
	Dim INVALID As Byte
	Dim ENGINEERING As Byte
	Dim PRODUCTION As Byte
	Dim CUSTOMER As Byte
	Dim SPECIAL As Byte
	Dim SERIALBINARYMODE As Byte
	Dim SERIALTEXTMODE As Byte
	Dim PARALLELMODE As Byte
	Dim UNKNOWN As Byte
	Dim NOTSELECTED As Byte
	Dim WIDEBAND As Byte
	Dim WIDEBAND3 As Byte
	Dim ITLA As Byte
	Dim TLEK As Byte
	Dim FCUDBR As Byte
	Dim FCUCMZ As Byte
	Dim UNASSIGNED As Byte
#End If
	
	Public Enum UserType
		INVALID ' Or CUSTOMER
		ENGINEERING
		PRODUCTION
		CUSTOMER
		SPECIAL ' Customer plus ....
	End Enum
	
	
	Public Enum CommsModeType
		SERIALBINARYMODE
		SERIALTEXTMODE
		PARALLELMODE
	End Enum
	
	Public Enum ModuleVariantType
		UNKNOWN = -2
		NOTSELECTED = -1
		WIDEBAND = 0
		WIDEBAND3 = 1
		FCUDBR = 2
		FCUCMZ = 3
		ITLA = 4
		TLEK = 5
		UNASSIGNED = 6
	End Enum
	
	
	
	
	
	Public Structure IsrcCalibrationType
		Dim Count As Short ' Number of cal points read.
		Dim Packing As Short ' (Max dac size + 1) / 4096
		Dim AvgSlope As Double ' Average slope (mA/dac) of response, excluding the ends.
		Dim Direction As Short ' +1 or -1 to indicate whether current goes up or down with dac.
		Dim Interval As Short ' Dac step size.
		<VBFixedArray(4095)> Dim Current() As Double ' Normally current but can be voltage for the MZ arms.
		<VBFixedArray(4095)> Dim DACNumber() As Integer
		
		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			ReDim Current(4095)
			ReDim DACNumber(4095)
		End Sub
	End Structure
	
	
	Public Structure ModuleDataType
		'UPGRADE_NOTE: Variant was upgraded to Variant_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim Variant_Renamed As ModuleVariantType
		<VBFixedArray(12)> Dim MaxDAC() As Integer ' DAC size for each current in "CurrentNames" enum and regs A7+A8.
		Dim Initialised As Boolean
		Dim CalState As Boolean ' Not started or complete = False, started = True.
		Dim Status As String ' PASS/FAIL type of status depending on limit checks.
		Dim UserComments As String
		Dim IsProfiled As Boolean
		Dim CalOffGrid As Boolean ' Calibrate the off-grid tuning points (ITLA +/-8GHz).
		Dim ModulatorFitted As Boolean
		Dim AutoScanReqd As Boolean ' Only used in the engineering cal?
		<VBFixedArray(2)> Dim SerialNumber() As String ' 0 = Laser   1 = Board   2 = Serial
		Dim PowerTarget As Double ' Target cal power in dBm. Max output power (reg DB).
		Dim MinOutputPower As Double ' Min output power in dBm (reg DA).
		Dim PowerOffset As Double ' Value to add to power meter to get actual power.
		Dim SplitterSlope As Double ' Variation of splitter loss as dB/THz.
		Dim SplitterCalFreq As Double ' Frequency (GHz) at which the splitter loss was measured.
		Dim ModulatorLoss As Double
		Dim LaserCalPower As Double ' Power of the "gold box" laser calibration.
		Dim FromChannel As Short ' First channel to be calibrated.
		Dim ToChannel As Short
		Dim FirstChannel As Short ' First channel in the cal input file.
		Dim LastChannel As Short ' Last channel in the cal input file.
		Dim Waveband As String
		Dim LowestCalFreq As Double ' Lowest frequency of the calibrated channels - should equate to
		Dim HighestCalFreq As Double '           the highest calibrated ITU channel number.
		Dim FirstChannelFreq As Double ' As defined in the OIF
		Dim ChannelSpacing As Short ' As defined in the OIF
		Dim MaxFreqError As Single ' Determined by the wavemeter type.
		Dim FrequencyOffset As Double ' Currently unused (v3.1.1)
		Dim PMPot As Short ' Reg AA
		Dim StartPCBTemp As Double ' Reg 81 at start of cal.
		Dim EndPCBTemp As Double
		Dim AtmelRef As Short ' Reg B2
		Dim LsBiasSense As Short ' Reg 9C but for channel 1 only.
		
		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			'UPGRADE_WARNING: Lower bound of array MaxDAC was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
			ReDim MaxDAC(12)
			ReDim SerialNumber(2)
		End Sub
	End Structure
	
	
	' Forms which may be opened.
	Public MainForm As System.Windows.Forms.Form
	Public MSAControlForm As System.Windows.Forms.Form
	Public OIFControlForm As System.Windows.Forms.Form
	'UPGRADE_WARNING: Lower bound of array SetupForm was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Public SetupForm(10) As System.Windows.Forms.Form
	Public CalForm As System.Windows.Forms.Form
    Public Logger As IMsgLogger
	Public MeasurementForm As System.Windows.Forms.Form
	Public ProgBar As System.Windows.Forms.Form
	Public TextTerm As System.Windows.Forms.Form
	
	' Comms classes. "Comms" will point to either the parallel or serial comms procedures.
	' "MSA", "OIF" and "CAL" are not real comms classes, just groupings of different commands.
	' The remainder (oven and meters) point to either GPIB or serial comms depending on how the
	' individual instrument is connected.
	Public Comms As Object
	Public MSA As Object
	Public OIF As Object
	Public CAL As Object
	Public DBR As Object
	Public CBH As Object ' The Call Back Handler form.
	Public Wavemeter As Object
	Public Powermeter As Object
	Public osa As Object
	Public RIN As Object
	Public Oven As Object
	Public DMM As Object
	Public Attenuator As Object
	Public BERmeter As Object
	Public PSU As Object
	Public DCA As Object
    Public SerialPortComms As System.IO.Ports.SerialPort
	
	' Currently untested and unimplemented matrix maths intended for the calculation of dither
	' parameters during Santur calibration. (v2.3.0)
	Public Matrix As Object
	
	' ##########################################################################################
	' Global data.
	
	'UPGRADE_WARNING: Arrays in structure Module may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
	'UPGRADE_NOTE: Module was upgraded to Module_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Module_Renamed As ModuleDataType
	'UPGRADE_WARNING: Lower bound of array IsrcCal was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    ''UPGRADE_WARNING: Array IsrcCal may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    'Public IsrcCal(12) As IsrcCalibrationType
    ''UPGRADE_WARNING: Array ChanCal may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    'Public ChanCal(200) As ChannelCalibrationType
    ''UPGRADE_WARNING: Array OrigChanCal may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    'Public OrigChanCal(200) As ChannelCalibrationType
	Public LimitResults() As String
	Public ABORTCAL As Boolean
	
	
	Public LoginId As UserType
	Public ModulePasswordOK As Boolean
	Public ModulePassword As String
End Module
// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// Instr_FCU.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for the full-band control unit. 
    /// </summary>
    public class Instr_FCU : Instrument, 
                                IBlackBoxIdentity,
                                IMSA300pin,
                                IMSA_ITLA,
                                IDsdbrLaserSetup
    {
        #region Private data
        /// <summary>
        /// Reference to chassis object, wrappimng JC's Ixolite VB code.
        /// </summary>
        Chassis_IxoliteWrapper ixoliteChassis;
        #endregion

        /// <summary>
        /// Construct FCU instrument driver.
        /// </summary>
        /// <param name="instrumentName">Name of instrument.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="slotId">Slot id. (Ignored)</param>
        /// <param name="subSlotId">Sub slot id. (Ignored)</param>
        /// <param name="chassis">Reference to Ixolite wrapper chassis.</param>
        public Instr_FCU(
            string instrumentName,
            string driverName,
            string slotId,
            string subSlotId,
            Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            /* Add hardware record. */
            ValidHardwareData.Add(
                "FCU",
                new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));

            /* Add chassis record. */
            ValidChassisDrivers.Add(
                "Chassis_IxoliteWrapper",
                new InstrumentDataRecord("Chassis_IxoliteWrapper", "0.0.0.0", "2.0.0.0"));

            /* Save chassis object reference. */
            ixoliteChassis = (Chassis_IxoliteWrapper)base.InstrumentChassis;
        }

        /// <summary>
        /// Gets the version of the ixolite driver from the chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Gest the hardware serial number.
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Set Default State (Does nothing.)
        /// </summary>
        public override void SetDefaultState()
        {
            /* Left empty for future expansion. Required by base class abstract. */
        }


        #region IBlackBoxIdentity Members

        public string GetUnitSerialNumber()
        {
//            return this.ixoliteChassis.GetUnitSerialNumber_MSA();
            byte[] ascSerno = this.ixoliteChassis.GetI2cReturnBytes(0xA8);

            return Encoding.ASCII.GetString(ascSerno).Trim('\0');
        }

        public void SetUnitSerialNumber(string serno)
        {
//            this.ixoliteChassis.SetSerialNumber_MSA(serno);
            this.ixoliteChassis.SetI2cWithSubcmdAndPaddedString(0xF8, 0x00, serno, 0x00, 16);
        }

        public DateTime GetUnitBuildDate()
        {
            string bDate = this.ixoliteChassis.GetManufactureDate_MSA();

            int year = int.Parse(bDate.Substring(0, 4));
            int mon = int.Parse(bDate.Substring(4, 2));
            int date = int.Parse(bDate.Substring(6, 2));
            return new DateTime(year, mon, date, 12, 0, 0);
        }

        public void SetUnitBuildDate(DateTime bdate)
        {
            string dateStr = bdate.ToString("yyyyMMdd");
            this.ixoliteChassis.SetManufactureDate_MSA(dateStr);
        }

        public string GetLaserSerialNumber()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public void SetLaserSerialNumber(string serno)
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public string GetBoardSerialNumber()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public void SetBoardSerialNumber(string serno)
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public string GetPartNumber()
        {
            return this.ixoliteChassis.GetUnitPartNumber_MSA();
        }

        public void SetPartNumber(string partno)
        {
            this.ixoliteChassis.SetPartNumber_MSA(partno);
        }

        public string GetSupplier()
        {
            return this.ixoliteChassis.GetSupplier_MSA();
        }

        /// <summary>
        /// Gets the laser technology identifier string
        /// </summary>
        /// <returns>Laser technology identifier</returns>
        public string GetLaserTechnology()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        /// <summary>
        /// Sets the laser technology identifier string
        /// </summary>
        /// <param name="laserTech">Laser technology identifier</param>
        public void SetLaserTechnology(string laserTech)
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        /// <summary>
        /// Get the Rx Serial Number - Not implemented for FCU
        /// </summary>
        /// <returns>The serial Number</returns>
        public string GetRxSerialNumber()
        {
            throw new Exception("The method or operation is not implemented.");
        }


        /// <summary>
        /// Set the Rx Serial Number. Not Implemented for FCU.
        /// </summary>
        /// <param name="serno">The serial number</param>
        public void SetRxSerialNumber(string serno)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IMSA300pin Members

        public void SetTxCommand(TxCommand txCmd)
        {
            this.ixoliteChassis.SetI2cWithBytes(0x40, txCmd.Data1, txCmd.Data2, txCmd.Data3);
        }

        public TxCommand ReadTxCommand()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x41, 3);
            return new TxCommand(resp[0], resp[1], resp[2]);
        }

        public void SaveTxRegister()
        {
            this.ixoliteChassis.SaveTxRegister();
        }

        public void RestoreTxRegister()
        {
            this.ixoliteChassis.RestoreTxRegister();
        }

        public bool GetLsEnable()
        {
            /* Read TxCommand. */
            TxCommand txCmd = this.ReadTxCommand();

            /* Return bit0 of byte 3. */
            return (txCmd.Data3 & 0x01) != 0;
        }

        public void SetLsEnable(bool lsEnable)
        {
            /* Read old TxCommand. */
            TxCommand txCmd = this.ReadTxCommand();
            byte data3 = txCmd.Data3;

            /* Zero-ify bit 0. */
            data3 &= 0x01;

            /* One-ify bit 0 if lsEnable==true. */
            if (lsEnable)
            {
                data3 |= 0x01;
            }

            /* If needed, set new TxCommand. */
            if (txCmd.Data3 != data3)
            {
                TxCommand newTxCmd = new TxCommand(txCmd.Data1, txCmd.Data2, data3);
                this.SetTxCommand(newTxCmd);
            }
        }

        public void SetRxCommand(RxCommand rxCmd)
        {
            this.ixoliteChassis.SetI2cWithBytes(0x44, rxCmd.Data1, rxCmd.Data2, rxCmd.Data3);
        }

        public RxCommand ReadRxCommand()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x41, 3);
            return new RxCommand(resp[0], resp[1], resp[2]);
        }

        public void SaveRxRegister()
        {
            this.ixoliteChassis.SaveRxRegister();
        }

        public void RestoreRxRegister()
        {
            this.ixoliteChassis.SendI2cCommandNoPayload(0x47);
        }


        public void SelectITUChannel(BandChannel bandChannel, bool protectMode)
        {
            //this.ixoliteChassis.SelectITUChannel(bandChannel.Band, bandChannel.Chan);
        }


        public void SelectITUChannel(BandChannel bandChannel)
        {
            this.ixoliteChassis.SelectITUChannel(bandChannel.Band, bandChannel.Chan);
        }

        public BandChannel GetITUChannel()
        {
            return this.ixoliteChassis.GetITUChannel();
        }

        public double ReadRxThreshold()
        {
            throw new TunableModuleException("Not implemented yet.", TunableProtocol.MSA);
        }

        public void SetRxThreshold(double thresh_pc)
        {
            this.ixoliteChassis.SetRxThreshold(thresh_pc);
        }

        public double GetLaserBiasCurrent_mA()
        {
            return this.ixoliteChassis.GetLaserBiasCurrent();
        }

        public double GetLaserOutputPower_uW()
        {
            return this.ixoliteChassis.LaserOutputPower();
        }

        public double GetSubMountTemp_degC()
        {
            return this.ixoliteChassis.SubMountTemp();
        }

        public double GetRxACPower()
        {
            return this.ixoliteChassis.RxACPower();
        }

        public double GetRxAvgPower_nW()
        {
            return this.ixoliteChassis.RxAvgPower();
        }

        public double GetLaserFrequencyOffset_MHz()
        {
            return this.ixoliteChassis.LaserWavelength();
        }

        public double GetModuleTemp_degC()
        {
            return this.ixoliteChassis.ModuleTemp();
        }

        public double GetAPDTemp_degC()
        {
            return this.ixoliteChassis.APDTemp();
        }

        public double GetModBias()
        {
            return this.ixoliteChassis.ModBias();
        }

        public double GetLaserAbsoluteTemperature_degC()
        {
            return this.ixoliteChassis.LaserAbsoluteTemperature();
        }

        public BandChannel GetLastChannel()
        {
            return this.ixoliteChassis.GetLastChannel();
        }

        public string GetModuleType()
        {
            return this.ixoliteChassis.GetModuleType();
        }

        public BandChannel GetFirstChannel()
        {
            return this.ixoliteChassis.GetFirstChannel();
        }

        public double ChannelSpacing_GHz()
        {
            return this.ixoliteChassis.ChannelSpacing();
        }

        public string GetManufactureDate()
        {
            return this.ixoliteChassis.GetManufactureDate_MSA();
        }

        public string GetUnitPartNumber()
        {
            return this.ixoliteChassis.GetUnitPartNumber_MSA();
        }

        public bool LinkStatus()
        {
            try { this.ixoliteChassis.LinkStatus(); }
            catch { return false; }
            return true;
        }

        public void EnterPinMode()
        {
            this.ixoliteChassis.EnterPinMode();
        }

        public int ReadMaxI2CRate_kbps()
        {
            return this.ixoliteChassis.ReadMaxI2CRate();
        }

        public void EnterProtectMode()
        {
            this.ixoliteChassis.EnterProtectMode();
        }

        public void EnterVendorProtectMode(string password)
        {
            this.ixoliteChassis.EnterVendorProtectMode(password);
        }

        public void ExitProtectMode()
        {
            this.ixoliteChassis.ExitProtectMode();
        }

        public void ResetCPN()
        {
            this.ixoliteChassis.ResetCPN();
        }

        public void EnterSoftMode()
        {
            this.ixoliteChassis.EnterSoftMode();
        }

        public void Loopback()
        {
            this.ixoliteChassis.Loopback();
        }

        public bool WaitForTempToSettle(int count, double wait_s, double target_degC, double devi_degC)
        {
            return this.ixoliteChassis.WaitForTempToSettle(count, wait_s, target_degC, devi_degC);
        }

        public string GetHardwareRevisionCode()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public string GetFirmwareRevisionCode()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public byte ReadMSACapability()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public byte ReadOperatingModeByte()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public byte ReadMSAEdition()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public double GetLsWAVEMON_GHz()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public double GetRxPOWMON_mW()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public double GetLsTEMPMON_DegC()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public double GetLsPOWMON_mW()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public double GetLsBIASMON_mA()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetPRBSERRDETState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetRXSState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetRxLOCKERRState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetRxSIGALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetRxPOWALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetRxALMINTState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public RxAlarmStatusRegister ReadRxAlarmStatusRegister()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxFIFOERRState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetModBIASALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetLsPOWALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxLOCKERRState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetLsTEMPALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetLsBIASALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxALMINTState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetLsWAVALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxDSCERRState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxLOFALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetTxOOAState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetModTEMPALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public bool GetEOLALMState()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        public TxAlarmStatusRegister ReadTxAlarmStatusRegister()
        {
            throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
        }

        #endregion

        #region IMSA_ITLA Members

        public int GetNOP()
        {
            return this.ixoliteChassis.GetNOP();
        }

        public string GetDeviceType()
        {
            return this.ixoliteChassis.GetDeviceType();
        }

        public string GetManufacturer()
        {
            return ((IBlackBoxIdentity)this).GetSupplier();
        }

        public string GetModelNumber()
        {
            return ((IBlackBoxIdentity)this).GetPartNumber();
        }

        public string GetSerialNumber()
        {
            return ((IBlackBoxIdentity)this).GetUnitSerialNumber();
        }

        public string GetManufacturingDate()
        {
            return this.ixoliteChassis.GetManufactureDate_MSA();
        }

        public string GetReleaseCode()
        {
            return this.ixoliteChassis.GetReleaseCode();
        }

        public string GetReleaseBackwards()
        {
            return this.ixoliteChassis.GetReleaseBackwards();
        }

        public void SaveConfig()
        {
             this.ixoliteChassis.SaveConfig();
        }

        public int GetIOCap()
        {
            return this.ixoliteChassis.GetIOCap();
        }

        public void SetIOCap(int value)
        {
             this.ixoliteChassis.SetIOCap(value);
        }

        public Bookham.TestLibrary.BlackBoxes.OifStatus GetStatusF()
        {
            return this.ixoliteChassis.GetStatusF();
        }

        public void ClearStatusF()
        {
             this.ixoliteChassis.ClearStatusF();
        }

        public Bookham.TestLibrary.BlackBoxes.OifStatus GetStatusW()
        {
            return this.ixoliteChassis.GetStatusW();
        }

        public void ClearStatusW()
        {
             this.ixoliteChassis.ClearStatusW();
        }

        public double GetFPowTh_dB()
        {
            return this.ixoliteChassis.GetFPowTh();
        }

        public void SetFPowTh_dB(double thresh)
        {
             this.ixoliteChassis.SetFPowTh(thresh);
        }

        public double GetWPowTh_dB()
        {
            return this.ixoliteChassis.GetWPowTh();
        }

        public void SetWPowTh_dB(double th)
        {
             this.ixoliteChassis.SetWPowTh(th);
        }

        public double GetFFreqTh_GHz()
        {
            return this.ixoliteChassis.GetFFreqTh();
        }

        public void SetFFreqTh_GHz(double th)
        {
             this.ixoliteChassis.SetFFreqTh(th);
        }

        public double GetWFreqTh_GHz()
        {
            return this.ixoliteChassis.GetWFreqTh();
        }

        public void SetWFreqTh_GHz(double th)
        {
             this.ixoliteChassis.SetWFreqTh(th);
        }

        public double GetFThermTh_degC()
        {
            return this.ixoliteChassis.GetFThermTh();
        }

        public void SetFThermTh_degC(double th)
        {
             this.ixoliteChassis.SetFThermTh(th);
        }

        public double GetWThermTh_degC()
        {
            return this.ixoliteChassis.GetWThermTh();
        }

        public void SetWThermTh_degC(double th)
        {
             this.ixoliteChassis.SetWThermTh(th);
        }

        public Bookham.TestLibrary.BlackBoxes.OifStatus GetSRQT()
        {
            return this.ixoliteChassis.GetSRQT();
        }

        public void SetSRQT(Bookham.TestLibrary.BlackBoxes.OifStatus mask)
        {
             this.ixoliteChassis.SetSRQT(mask);
        }

        public Bookham.TestLibrary.BlackBoxes.OifStatus GetFATALT()
        {
            return this.ixoliteChassis.GetFATALT();
        }

        public void SetFATALT(Bookham.TestLibrary.BlackBoxes.OifStatus mask)
        {
             this.ixoliteChassis.SetFATALT(mask);
        }

        public Bookham.TestLibrary.BlackBoxes.OifStatus GetALMT()
        {
            return this.ixoliteChassis.GetALMT();
        }

        public void SetALMT(Bookham.TestLibrary.BlackBoxes.OifStatus mask)
        {
             this.ixoliteChassis.SetALMT(mask);
        }

        public void SelectChannel(int chan)
        {
            this.ixoliteChassis.SelectChannel(checked((short)chan));
        }

        public int GetChannel()
        {
            return this.ixoliteChassis.GetChannel();
        }

        public void SetPowerSetpoint_dBm(double pwr)
        {
             this.ixoliteChassis.SetPowerSetpoint(pwr);
        }

        public double GetPowerSetpoint_dBm()
        {
            return this.ixoliteChassis.GetPowerSetpoint();
        }

        public void SoftwareEnableOutput(bool Sena)
        {
             this.ixoliteChassis.SoftwareEnableOutput(Sena);
        }

        public void ModuleReset()
        {
             this.ixoliteChassis.ModuleReset();
        }

        public void SoftReset()
        {
             this.ixoliteChassis.SoftReset();
        }

        public bool GetSoftwareEnableOutput()
        {
            return this.ixoliteChassis.GetSoftwareEnableOutput();
        }

        public bool GetADT()
        {
            return this.ixoliteChassis.GetADT();
        }

        public void SetADT(bool adt)
        {
             this.ixoliteChassis.SetADT(adt);
        }

        public bool GetSDF()
        {
            return this.ixoliteChassis.GetSDF();
        }

        public void SetSDF(bool Sdf)
        {
             this.ixoliteChassis.SetSDF(Sdf);
        }

        public void SetFrequencyGrid_GHz(double GHz)
        {
             this.ixoliteChassis.SetFrequencyGrid(checked((short)GHz));
        }

        public double GetFrequencyGrid_GHz()
        {
            return this.ixoliteChassis.GetFrequencyGrid();
        }

        void IMSA_ITLA.SetFirstChanFreq_GHz(double freq)
        {
            this.ixoliteChassis.SetFirstChanFreq_OIF(freq);
        }

        double IMSA_ITLA.GetFirstChanFreq_GHz()
        {
            return this.ixoliteChassis.GetFirstChanFreq_OIF();
        }

        public double GetLaserFrequency_GHz()
        {
            return this.ixoliteChassis.GetLaserFrequency();
        }

        public double GetLaserOutputPower_dBm()
        {
            return this.ixoliteChassis.GetLaserOutputPower();
        }

        public double GetCurrentTemperature_degC()
        {
            return this.ixoliteChassis.GetCurrentTemperature();
        }

        public double GetOpticalPowerMin_dBm()
        {
            return this.ixoliteChassis.GetOpticalPowerMin();
        }

        public double GetOpticalPowerMax_dBm()
        {
            return this.ixoliteChassis.GetOpticalPowerMax();
        }

        public double GetLaserFirstFreq_GHz()
        {
            return this.ixoliteChassis.GetLaserFirstFreq();
        }

        public void SetLaserFirstFreq_GHz(double freq_GHz)
        {
             this.ixoliteChassis.SetLaserFirstFreq(freq_GHz);
        }

        public double GetLaserLastFreq_GHz()
        {
            return this.ixoliteChassis.GetLaserLastFreq();
        }

        public void SetLaserLastFreq_GHz(double freq_GHz)
        {
             this.ixoliteChassis.SetLaserLastFreq(freq_GHz);
        }

        public void SetMinFrequencyGrid_GHz(double GHz)
        {
             this.ixoliteChassis.SetMinFrequencyGrid(GHz);
        }

        public double GetMinFrequencyGrid_GHz()
        {
            return this.ixoliteChassis.GetMinFrequencyGrid();
        }

        public Bookham.TestLibrary.BlackBoxes.DitherState GetDitherEnable()
        {
            return this.ixoliteChassis.GetDitherEnable();
        }

        public void SetDitherEnable(Bookham.TestLibrary.BlackBoxes.DitherState state)
        {
             this.ixoliteChassis.SetDitherEnable(state);
       }

        public double GetDitherR_KHz()
        {
            return this.ixoliteChassis.GetDitherR();
        }

        public void SetDitherR_KHz(double KHz)
        {
             this.ixoliteChassis.SetDitherR(checked((short)KHz));
        }

        public double GetDitherF_GHz()
        {
            return this.ixoliteChassis.GetDitherF();
        }

        public void SetDitherF_GHz(double GHz)
        {
             this.ixoliteChassis.SetDitherF(GHz);
        }

        public double GetDitherA_percent()
        {
            return this.ixoliteChassis.GetDitherA();
        }

        public void SetDitherA_percent(double percent)
        {
             this.ixoliteChassis.SetDitherA(percent);
        }

        public double GetTCaseL_degC()
        {
            return this.ixoliteChassis.GetTCaseL();
        }

        public void SetTCaseL_degC(double degC)
        {
             this.ixoliteChassis.SetTCaseL(degC);
        }

        public double GetTCaseH_degC()
        {
            return this.ixoliteChassis.GetTCaseH();
        }

        public void SetTCaseH_degC(double degC)
        {
             this.ixoliteChassis.SetTCaseH(degC);
        }

        public int GetFAgeTh()
        {
            return this.ixoliteChassis.GetFAgeTh();
        }

        public void SetFAgeTh(int percent)
        {
             this.ixoliteChassis.SetFAgeTh(checked((short)percent));
        }

        public int GetWAgeTh()
        {
            return this.ixoliteChassis.GetWAgeTh();
        }

        public void SetWAgeTh(int percent)
        {
             this.ixoliteChassis.SetWAgeTh(checked((short)percent));
        }

        #endregion

        #region IDsdbrLaserSetup Members
        /// <summary>
        /// Get/set Settling time for settings
        /// </summary>
        public int SettlingTime_ms
        {
            get
            {
                throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
            }

            set
            {
                throw new TunableModuleException("The method or operation is not implemented.", TunableProtocol.MSA);
            }
        }


        public void EnterLaserSetupMode()
        {
            this.EnterVendorProtectMode("THURSDAY");
        }

        public int GetSOADAC()
        {
            return this.ixoliteChassis.GetSOADAC();
        }

        public void SetSOADAC(int dac)
        {
            this.ixoliteChassis.SetSOADAC(dac);
        }

        public int GetGainDAC()
        {
            return this.ixoliteChassis.GetGainDAC();
        }

        public void SetGainDAC(int dac)
        {
            this.ixoliteChassis.SetGainDAC(dac);
        }

        public int GetFrontSectionPair()
        {
            return this.ixoliteChassis.GetControlRegFrontSection();
        }

        public void SetFrontSectionPair(int i)
        {
            this.ixoliteChassis.SetControlRegFrontSection(i);
        }

        public int GetFrontEvenDAC()
        {
            return this.ixoliteChassis.GetFrontEvenDAC();
        }

        public void SetFrontEvenDAC(int dac)
        {
            this.ixoliteChassis.SetFrontEvenDAC(dac);
        }

        public int GetFrontOddDAC()
        {
            return this.ixoliteChassis.GetFrontOddDAC();
        }

        public void SetFrontOddDAC(int dac)
        {
            this.ixoliteChassis.SetFrontOddDAC(dac);
        }

        public int GetRearDAC()
        {
            return this.ixoliteChassis.GetRearDAC();
        }

        public void SetRearDAC(int dac)
        {
            this.ixoliteChassis.SetRearDAC(dac);
        }

        public int GetPhaseDAC()
        {
            return this.ixoliteChassis.GetPhaseDAC();
        }

        public void SetPhaseDAC(int dac)
        {
            this.ixoliteChassis.SetPhaseDAC(dac);
        }

        public int GetModeCentrePhaseDAC()
        {
            return this.ixoliteChassis.GetModeCentrePhaseDAC_OifBkhm();
        }

        public void SetModeCentrePhaseDAC(int dac)
        {
            this.ixoliteChassis.SetModeCentrePhaseDAC_OifBkhm(dac);
        }

        public int GetBOLPhaseDAC()
        {
            return this.ixoliteChassis.GetBOLPhaseDAC_OifBkhm();
        }

        public void SetBOLPhaseDAC(int dac)
        {
            this.ixoliteChassis.SetBOLPhaseDAC_OifBkhm(dac);
        }

        public int GetDitherDAC()
        {
            return this.ixoliteChassis.GetDitherDAC();
        }

        public void SetDitherDAC(int dac)
        {
            this.ixoliteChassis.SetDitherDAC(checked((short)dac));
        }

        public void SaveChannel(int chan)
        {
            this.ixoliteChassis.SaveChannel(checked((short)chan));
        }

        public void DoAutoLockerSetup()
        {
            this.ixoliteChassis.SetControlRegAutoLockStart();
        }

        public bool GetLockerSlopePositive()
        {
            return this.ixoliteChassis.GetControlRegLockerSlopeOn();
        }

        public void SetLockerSlopePositive(bool on)
        {
            this.ixoliteChassis.SetControlRegLockerSlopeOn(on);
        }

        public int GetTxCoarsePot()
        {
            return this.ixoliteChassis.GetTxCoarsePot();
        }

        public void SetTxCoarsePot(int pot)
        {
            this.ixoliteChassis.SetTxCoarsePot(checked((short)pot));
        }

        public int GetRefCoarsePot()
        {
            return this.ixoliteChassis.GetRefCoarsePot();
        }

        public void SetRefCoarsePot(int rcp)
        {
            this.ixoliteChassis.SetRefCoarsePot(checked((short)rcp));
        }

        public int GetLockerErrorFactor()
        {
            return this.ixoliteChassis.GetLockerErrorFactor();
        }

        public void SetLockerErrorFactor(int lef)
        {
            this.ixoliteChassis.SetLockerErrorFactor(lef);
        }

        public bool GetLockerOn()
        {
            return this.ixoliteChassis.GetControlRegLockerOn();
        }

        public void SetLockerOn(bool on)
        {
            this.ixoliteChassis.SetControlRegLockerOn(on);
        }

        public int GetLockerTxMon_ADC()
        {
            return this.ixoliteChassis.GetLockerTxMon();
        }

        public int GetLockerRefMon_ADC()
        {
            return this.ixoliteChassis.GetLockerRefMon();
        }

        public int GetLaserPowerMon_ADC(int n)
        {
            return this.ixoliteChassis.GetLaserPowerMon(checked((short)n));
        }

        public int GetLsPhaseSense()
        {
            return this.ixoliteChassis.GetLsPhaseSense();
        }

        public int GetEOLPhaseADC_Min()
        {
            return this.ixoliteChassis.GetPhaseMin();
        }

        public void SetEOLPhaseADC_Min(int pmin)
        {
            this.ixoliteChassis.SetPhaseMin(pmin);
        }

        public int GetEOLPhaseADC_Max()
        {
            return this.ixoliteChassis.GetPhaseMax();
        }

        public void SetEOLPhaseADC_Max(int pmax)
        {
            this.ixoliteChassis.SetPhaseMax(pmax);
        }

        public int GetPowerMonFactor()
        {
            return this.ixoliteChassis.GetPowerMonFactor();
        }

        public void SetPowerMonFactor(int pmf)
        {
            this.ixoliteChassis.SetPowerMonFactor(pmf);
        }

        public int GetPowerMonPot()
        {
            return this.ixoliteChassis.GetPowerMonPot();
        }

        public void SetPowerMonPot(int pot)
        {
            this.ixoliteChassis.SetPowerMonPot(checked((short)pot));
        }

        public int GetPowerMonOffset()
        {
            return this.ixoliteChassis.GetPowerMonOffset();
        }

        public void SetPowerMonOffset(int pmo)
        {
            this.ixoliteChassis.SetPowerMonOffset(pmo);
        }

        public double GetLaserPowerEstimate_uW()
        {
            return this.ixoliteChassis.GetLaserPowerEstimate();
        }

        public int GetSBSDitherMultiplier()
        {
            return this.ixoliteChassis.GetSBSDitherMultiplier();
        }

        public void SetSBSDitherMultiplier(int mult)
        {
            this.ixoliteChassis.SetSBSDitherMultiplier(checked((byte)mult));
        }

        public bool GetSBSDitherOn()
        {
            return this.ixoliteChassis.GetControlRegDitherOn();
        }

        public void SetSBSDitherOn(bool on)
        {
            this.ixoliteChassis.SetControlRegDitherOn(on);
        }

        public bool GetPowerProfilingOn()
        {
            return this.ixoliteChassis.GetControlRegProfilingOn();
        }

        public void SetPowerProfilingOn(bool on)
        {
            this.ixoliteChassis.SetControlRegProfilingOn(on);
        }

        public bool GetPowerControlLoopOn()
        {
            return this.ixoliteChassis.GetPowerControlStateOn();
        }

        public void SetPowerControlLoopOn(bool enabled)
        {
            this.ixoliteChassis.SetPowerControlStateOn(enabled);
        }
        #endregion

        #region FCU only
        /* Former IMSABkhm - now FCU only. */
        public void SetCalMzMod_digipot(int digipot, bool nv)
        {
            this.ixoliteChassis.SetCalMzMod(checked((short)digipot), nv);
        }

        public int GetCalMzMod_digipot(bool nv)
        {
            return this.ixoliteChassis.GetCalMzMod(nv);
        }

        public void SetCalMzBiasN(int modBias, bool nv)
        {
            this.ixoliteChassis.SetCalMzBiasN(checked((short)modBias), nv);
        }

        public int GetCalMzBiasN(bool nv)
        {
            return this.ixoliteChassis.GetCalMzBiasN(nv);
        }

        public int GetADCReading(Bookham.TestLibrary.BlackBoxes.AdcId adc)
        {
            return this.ixoliteChassis.GetADCReading(adc);
        }

        public int GetSerdesRegister(int index)
        {
            return this.ixoliteChassis.GetSerdesRegister(checked((byte)index));
        }

        public void SetSerdesRam(int index, int value)
        {
            this.ixoliteChassis.SetSerdesRam(checked((byte)index), value);
        }

        public void SetSerdesRegister(int index, int value)
        {
            this.ixoliteChassis.SetSerdesRegister(checked((byte)index), value);
        }

        public void SetSerdesEEPROM(int index, int value)
        {
            this.ixoliteChassis.SetSerdesEEPROM(checked((byte)index), value);
        }
    
        public void SetRxPwrMonCalTable(int index)
        {
            this.ixoliteChassis.SetRxPwrMonCalTable(checked((byte)index));
        }

        public int GetRxPwrMonCalTable(int index)
        {
            return this.ixoliteChassis.GetRxPwrMonCalTable(checked((byte)index));
        }

        public int GetRxPwrMonCalTableSize()
        {
            return this.ixoliteChassis.GetRxPwrMonCalTableSize();
        }

        public int GetRxPwrPoint_dBm(int index)
        {
            return this.ixoliteChassis.GetRxPwrPoint(checked((byte)index));
        }

        public double GetRawOpticalPower()
        {
            return this.ixoliteChassis.GetRawOpticalPower();
        }

        public int GetModMonPower()
        {
            return this.ixoliteChassis.GetModMonPower();
        }

        public int GetMZModAmplitudeMon()
        {
            return this.ixoliteChassis.GetMZModAmplitudeMon();
        }

        public void SaveRxCal()
        {
            this.ixoliteChassis.SaveRxCal();
        }

        public void RecallRxCal()
        {
            this.ixoliteChassis.RecallRxCal();
        }

        public void SetCalTxPowerMon(int uW)
        {
            this.ixoliteChassis.SetCalTxPowerMon(checked((short)uW));
        }

        public void SetBOLLaserPower_uW(int uW)
        {
            this.ixoliteChassis.SetBOLLaserPower(checked((short)uW));
        }

        public void SetModulator_dac(int dac)
        {
            this.ixoliteChassis.SetModulator(checked((short)dac));
        }

        public int GetModulator_dac()
        {
            return this.ixoliteChassis.GetModulatorDac();
        }

        public void SaveModulator(int dac)
        {
            this.ixoliteChassis.SaveModulator(checked((short)dac));
        }

        public void SetModMonGain(BlackBoxes.ModMonGain high)
        {
            this.ixoliteChassis.SetModMonGain(high == ModMonGain.High);
        }

        public void SetMZLoop(BlackBoxes.MZLoop open)
        {
            this.ixoliteChassis.SetMZLoop(open == MZLoop.Open);
        }

        public void SetChirp(BlackBoxes.Chirp positive)
        {
            this.ixoliteChassis.SetChirp(positive == Chirp.Positive);
        }

        public void ModulatorDriverOn(bool enable)
        {
            this.ixoliteChassis.ModulatorDriver(enable);
        }
        public void SaveModulatorSettings()
        {
            this.ixoliteChassis.SaveModulatorSettings();
        }

        public void RecallModulatorSettings()
        {
            this.ixoliteChassis.RecallModulatorSettings();
        }

        public void SetFirstChanBand(ITUBand band)
        {
            this.ixoliteChassis.SetFirstChanBand(band);
        }

        public void SetLastChanBand(ITUBand band)
        {
            this.ixoliteChassis.SetLastChanBand(band);
        }

        public void SetPinModeBand(ITUBand band)
        {
            this.ixoliteChassis.SetPinModeBand(band);
        }

        public void SetFirstChanFreq_GHz(double GHz)
        {
            this.ixoliteChassis.SetFirstChanFreq_MSA(GHz);
        }

        public void SetLastChanFreq_GHz(double GHz)
        {
            this.ixoliteChassis.SetLastChanFreq(GHz);
        }

        public void SetPinModeFreq_GHz(double GHz)
        {
            this.ixoliteChassis.SetPinModeFreq(GHz);
        }

        public void SetChanSpacing_GHz(int GHz)
        {
            this.ixoliteChassis.SetChanSpacing(checked((short)GHz));
        }

        public void SaveBootData()
        {
            this.ixoliteChassis.SaveBootData();
        }

        public void OIFLaserEnable(bool enable)
        {
            this.ixoliteChassis.OIFLaser(enable);
        }

        public void SetHardwareRevision(string rev)
        {
            this.ixoliteChassis.SetHardwareRevision(rev);
        }

        public void SetLaserTemperature_ohms(double rTarget)
        {
            this.ixoliteChassis.SetLaserTemperature_ohms(rTarget);
        }

        public double GetLaserTemperature_ohms()
        {
            return this.ixoliteChassis.GetLaserTemperature_ohms();
        }

        /* Deleted functions from IOifBkhm, implemented as FCU only. */
        public double GetPCBTemp_degC()
        {
            return this.ixoliteChassis.GetPCBTemp();
        }

        public int GetWaveCode()
        {
            return checked((int)this.ixoliteChassis.GetWaveCode());
        }

        public void SetWaveCode(int wc)
        {
            this.ixoliteChassis.SetWaveCode(checked((short)wc));
        }

        public void SetChannelNumber(int chan)
        {
            this.ixoliteChassis.SaveChannel(checked((short)chan));
        }

        public void SelectInternalChannel(int chan)
        {
            this.ixoliteChassis.SelectChannel_OIFVen(checked((short)chan));
        }

        public int GetCurrentSrcNumber()
        {
            return this.ixoliteChassis.GetSrcNumber(SrcMode.Current);
        }

        public void SetCurrentSrcNumber(int pot)
        {
            this.ixoliteChassis.SetSrcNumber(SrcMode.Current, checked((short)pot));
        }

        public int GetCurrentSrcIndex()
        {
            return this.ixoliteChassis.GetSrcIndex(SrcMode.Current);
        }

        public void SetCurrentSrcIndex(int pot)
        {
            this.ixoliteChassis.SetSrcIndex(SrcMode.Current, checked((short)pot));
        }

        public double GetCurrentSrcValue()
        {
            return this.ixoliteChassis.GetSrcValue(SrcMode.Current);
        }

        public void SetCurrentSrcValue(double value)
        {
            this.ixoliteChassis.SetSrcValue(SrcMode.Current, checked((float)value));
        }

        public int GetBaseDac()
        {
            return this.ixoliteChassis.GetBaseDac();
        }

        public void SetBaseDac(int dac)
        {
            this.ixoliteChassis.SetBaseDac(dac);
        }

        public int GetBaseDac2()
        {
            return this.ixoliteChassis.GetBaseDac2();
        }

        public void SetBaseDac2(int dac)
        {
            this.ixoliteChassis.SetBaseDac2(dac);
        }

        public double GetPlateauPower()
        {
            return this.ixoliteChassis.GetPlateauPower();
        }

        public void SetPlateauPower(double pwr)
        {
            this.ixoliteChassis.SetPlateauPower(pwr);
        }

        public double GetSinkSlope()
        {
            return this.ixoliteChassis.GetSinkSlope();
        }

        public void SetSinkSlope(double slp)
        {
            this.ixoliteChassis.SetSinkSlope(slp);
        }

        public double GetWavelengthError()
        {
            return this.ixoliteChassis.GetWavelengthError();
        }

        public double GetSourceSlope()
        {
            return this.ixoliteChassis.GetSourceSlope();
        }

        public void SetSourceSlope(double slp)
        {
            this.ixoliteChassis.SetSourceSlope(slp);
        }

        public bool GetControlRegLaserTecOn()
        {
            return this.ixoliteChassis.GetControlRegLaserTecOn();
        }

        public void SetControlRegLaserTecOn(bool on)
        {
            this.ixoliteChassis.SetControlRegLaserTecOn(on);
        }

        /* Old IWbttFcu Common - Now FCU only. */

        public int GetVTECLevel()
        {
            return this.ixoliteChassis.GetVTECLevel();
        }

        public int GetLsBiasSense()
        {
            return this.ixoliteChassis.GetLsBiasSense();
        }

        public int GetLockerErrorMon()
        {
            return this.ixoliteChassis.GetLockerErrorMon();
        }

        public int GetLockerReference()
        {
            return this.ixoliteChassis.GetLockerReference();
        }

        public int GetTxFinePot()
        {
            return this.ixoliteChassis.GetTxFinePot();
        }

        public void SetTxFinePot(int pot)
        {
             this.ixoliteChassis.SetTxFinePot(checked((short)pot));
        }

        public int GetLockerRangePot()
        {
            return this.ixoliteChassis.GetLockerRangePot();
        }

        public void SetLockerRangePot(int pot)
        {
             this.ixoliteChassis.SetLockerRangePot(checked((short)pot));
        }

        public int GetAtmelReference()
        {
            return this.ixoliteChassis.GetAtmelReference();
        }

        /* Original FCU only. */

        public float GetIVSource(BlackBoxes.IVSourceIndexNumbers index)
        {
            return this.ixoliteChassis.GetIVSource(index);
        }

        public void SetIVSource(float value, BlackBoxes.IVSourceIndexNumbers index)
        {
            this.ixoliteChassis.SetIVSource(value, index);
        }

        public int GetTECADC()
        {
            return this.ixoliteChassis.GetTECADC();
        }

        public int GetVoltageSrcNumber()
        {
            return this.ixoliteChassis.GetSrcNumber(SrcMode.Voltage);
        }

        public void SetVoltageSrcNumber(int pot)
        {
            this.ixoliteChassis.SetSrcNumber(SrcMode.Voltage, checked((short)pot));
        }

        public int GetVoltageSrcIndex()
        {
            return this.ixoliteChassis.GetSrcIndex(SrcMode.Voltage);
        }

        public void SetVoltageSrcIndex(int pot)
        {
            this.ixoliteChassis.SetSrcIndex(SrcMode.Voltage, checked((short)pot));
        }

        public float GetVoltageSrcValue()
        {
            return this.ixoliteChassis.GetSrcValue(SrcMode.Voltage);
        }

        void SetVoltageSrcValue(float value)
        {
            this.ixoliteChassis.SetSrcValue(SrcMode.Voltage, value);
        }

        public int GetLeftMZBias()
        {
            return this.ixoliteChassis.GetLeftMZBias();
        }

        public void SetLeftMZBias(int pot)
        {
            this.ixoliteChassis.SetLeftMZBias(checked((short)pot));
        }

        public int GetRightMZBias()
        {
            return this.ixoliteChassis.GetRightMZBias();
        }

        public void SetRightMZBias(int pot)
        {
            this.ixoliteChassis.SetRightMZBias(checked((short)pot));
        }

        public int GetMZModulation()
        {
            return this.ixoliteChassis.GetMZModulation();
        }

        public void SetMZModulation(int pot)
        {
            this.ixoliteChassis.SetMZModulation(checked((short)pot));
        }

        public int GetLeftImbalance()
        {
            return this.ixoliteChassis.GetLeftImbalance();
        }

        public void SetLeftImbalance(int pot)
        {
            this.ixoliteChassis.SetLeftImbalance(checked((short)pot));
        }

        public int GetRightImbalance()
        {
            return this.ixoliteChassis.GetRightImbalance();
        }

        public void SetRightImbalance(int pot)
        {
            this.ixoliteChassis.SetRightImbalance(checked((short)pot));
        }

        public int GetLaserAbsoluteTemperature_ohms()
        {
            int therm_adc = this.ixoliteChassis.GetRawThermisterBridgeAlt_OifBkhm();
            if ((therm_adc == 0) || (therm_adc == 65535))
            {
                throw new InstrumentException("Invalid ADC reading : " + therm_adc);
            }
            return checked((int)(10000.0 / (65535.0/therm_adc - 1)));
        }
        #endregion


        #region IMSA300pin Members


 

        #endregion

        #region IBlackBoxIdentity Members



        #endregion
    }
}

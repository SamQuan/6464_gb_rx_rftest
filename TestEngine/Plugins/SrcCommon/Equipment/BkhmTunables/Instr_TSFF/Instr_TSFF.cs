// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// Instr_TSFF.cs
// 
// Authors: Bill Godfrey & Joseph Olajubu
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for the wide-band tunable transponder.
    /// </summary>
    public class Instr_TSFF : Instrument,
                                IBlackBoxIdentity,
                                IMSA300pin,
                                IDsdbrLaserSetup,
                                ITcmzLaserSetup,
                                IReceiverSetup
    {
        #region Private data
        /// <summary>
        /// Reference to chassis object, wrappimng JC's Ixolite VB code.
        /// </summary>
        Chassis_IxoliteWrapper ixoliteChassis;

        /// <summary>
        /// Used to set the reg/dac write delay 
        /// </summary>
        private int settlingTime_ms;

        #endregion

        /// <summary>
        /// Construct TSFF instrument driver.
        /// </summary>
        /// <param name="instrumentName">Name of instrument.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="slotId">Slot id. (Ignored)</param>
        /// <param name="subSlotId">Sub slot id. (Ignored)</param>
        /// <param name="chassis">Reference to Ixolite wrapper chassis.</param>
        public Instr_TSFF(
            string instrumentName,
            string driverName,
            string slotId,
            string subSlotId,
            Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            /* Add hardware record. */
            ValidHardwareData.Add(
                "TSFF",
                new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));

            /* Add chassis record. */
            ValidChassisDrivers.Add(
                "Chassis_IxoliteWrapper",
                new InstrumentDataRecord("Chassis_IxoliteWrapper", "0.0.0.0", "2.0.0.0"));

            /* Save chassis object reference. */
            ixoliteChassis = (Chassis_IxoliteWrapper)base.InstrumentChassis;
        }

        /// <summary>
        /// Gets the version of the ixolite driver from the chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Gest the hardware serial number.
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Set Default State (Does nothing.)
        /// </summary>
        public override void SetDefaultState()
        {
            // Default settling time = 200mS
            SettlingTime_ms = 150; 
        }
        /// <summary>
        /// TSFF settling time
        /// </summary>
        public int SettlingTime_ms
        {
            get { return settlingTime_ms; }
            set { settlingTime_ms = value; }
        }

        /// <summary>
        /// Sleep for the usual required delay after each set required by the
        /// TSFF.
        /// </summary>
        private void sleepForSetDelay()
        {
            if (settlingTime_ms > 0)
            {
                Thread.Sleep(settlingTime_ms);
            }
        }

        #region Read and write serders function
        
        /// <summary>
        /// Read Option Register
        /// </summary>
        /// <returns>Register value.</returns>
        public byte GetOptionRegister()
        {
            byte[] resp = this.ixoliteChassis.GetI2cWithSubcmdReturnNBytes(0xF1, 0x08, 1);
            return resp[0];
        }

        /// <summary>
        /// Write option register
        /// </summary>
        /// <param name="val"></param>
        public void SetOptionRegister(byte val)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x08, val);
            sleepForSetDelay();
        }
        /// <summary>
        /// Gets the Tx FIFO state. Not implemented at the moment, but will be in future FW releases
        /// </summary>
        /// <returns></returns>
        public byte GetTxFIFORegister()
        {
            byte[] resp = this.ixoliteChassis.GetI2cWithSubcmdReturnNBytes(0xF3, 0x1A, 12);
            return resp[0];
        }
        /// <summary>
        /// Set the Tx FIFO register
        /// </summary>
        /// <param name="val"></param>
        public void SetTxFIFORegister(byte val)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF2, 0x09, val);
            sleepForSetDelay();
        }

        /// <summary>
        /// Set the MZ Slope
        /// true = Positive, false = negative
        /// </summary>
        public void SetMzSlope(bool posSlope)
        {
            const byte bit2 = 0x04;  // Option Register
            const byte bit3 = 0x08;  // FIFO Register
        
            // Step 1. Read the current option & FIFO register value
            byte optionReg = GetOptionRegister();
            byte tempByte;

            // Not implemented in FW at the moment, so simply writing 0xCD, or 0xC5. 
            // Ideally, revert to changing bit3 when supported in FW.
//            byte fifoReg = GetTxFIFORegister();
            byte fifoReg = 0xC5;

            // Step 2. Set bit to its corresponding value, i.e 0=pos, 1=neg.
            if (posSlope)
            {
                unchecked
                {
                    tempByte = (byte)(~bit2);
                }
                optionReg &= tempByte;
//                fifoReg |= bit3;
                fifoReg = 0xCD;
            }
            else
            {
                optionReg |= Convert.ToByte(bit2);
//                fifoReg &= ~bit3;
                fifoReg = 0xC5;
            }

            // Step 3. Write the value back to the option register
            SetOptionRegister(optionReg);
            SetTxFIFORegister(fifoReg);
        }

        #endregion


        #region IBlackBoxIdentity Members

        /// <summary>
        /// Get the overall Unit Serial Number
        /// </summary>
        /// <returns>The serial Number</returns>
        public string GetUnitSerialNumber()
        {
            return this.ixoliteChassis.GetUnitSerialNumber_MSA().Trim();
        }

        /// <summary>
        /// Set the Unit Serial Number
        /// </summary>
        /// <param name="serno">The Serial number</param>
        public void SetUnitSerialNumber(string serno)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndPaddedString(0xF0, 0x31, serno, (byte)0, 16);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the unit build date
        /// </summary>
        /// <returns>The unit build date</returns>
        public DateTime GetUnitBuildDate()
        {
//            string bDate = this.ixoliteChassis.GetManufactureDate_MSA();

            byte[] dateAscStr = this.ixoliteChassis.GetI2cReturnNBytes(0xA9, 8);
            string bDate = Encoding.ASCII.GetString(dateAscStr);

            int year = int.Parse(bDate.Substring(0,4));
            int mon = int.Parse(bDate.Substring(4,2));
            int date = int.Parse(bDate.Substring(6,2));
            return new DateTime(year, mon, date, 12, 0, 0);
        }

        /// <summary>
        /// Set the Unit Build Date
        /// </summary>
        /// <param name="bdate">The Build date</param>
        public void SetUnitBuildDate(DateTime bdate)
        {
            string dateStr = bdate.ToString("yyyyMMdd");
            this.ixoliteChassis.SetI2cWithSubcmdAndString(0xF0, 0x3B, dateStr);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the TCMZ GoldBox Serial Number
        /// </summary>
        /// <returns>The GoldBox Serial Number</returns>
        public string GetLaserSerialNumber()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnString(0xF1, 0x23).Trim().Replace("\0", "");
        }

        /// <summary>
        /// Set the laser serial number
        /// </summary>
        /// <param name="serno">The Serial Number</param>
        public void SetLaserSerialNumber(string serno)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndString(0xF0, 0x23, serno);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the PCB serial number
        /// </summary>
        /// <returns>The PCB serial number</returns>
        public string GetBoardSerialNumber()
        {
            // Manual states 0x3B, but this errors. 0x3D ( same as SET_PCB_NUMBER )
            // appears to work.
//            return this.ixoliteChassis.GetI2cWithSubcmdReturnString(0xF1, 0x3B);
            return this.ixoliteChassis.GetI2cWithSubcmdReturnString(0xF1, 0x3D);
        }

        /// <summary>
        /// Set the PCB Serial Number
        /// </summary>
        /// <param name="serno">The PCB Serial Number</param>
        public void SetBoardSerialNumber(string serno)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndString(0xF0, 0x3D, serno);
            sleepForSetDelay();
        }
        /// <summary>
        /// Get the Part number
        /// </summary>
        /// <returns>The Part number</returns>
        public string GetPartNumber()
        {
            byte[] partNumArray = this.ixoliteChassis.GetI2cReturnNBytes(0xAA, 16);
            string partCode = Encoding.ASCII.GetString(partNumArray).Replace("\0", "").Trim();
            return partCode;
        }

        /// <summary>
        /// Set the Part Number
        /// </summary>
        /// <param name="partno"> The Part Number</param>
        public void SetPartNumber(string partno)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndString(0xF0, 0x25, partno);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the suppier string
        /// </summary>
        /// <returns>The supplier string</returns>
        public string GetSupplier()
        {
            return this.ixoliteChassis.GetSupplier_MSA();
        }

        /// <summary>
        /// Set the MZ Technology String. Not Supported by TSFF.
        /// </summary>
        /// <param name="mzTechnology"> MZ technology string </param>
        public virtual void SetLaserTechnology(string mzTechnology)
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Get the MZ Technology String. Not Supported by TSFF.
        /// </summary>
        /// <returns>The MZ Technology String</returns>
        public virtual string GetLaserTechnology()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        #endregion

        #region Other Identity Methods

        /// <summary>
        /// Get the hardware revision code
        /// </summary>
        /// <returns>The Hardware Revision Code</returns>
        public string GetHardwareRevisionCode()
        {
            byte[] revisionCode = this.ixoliteChassis.GetI2cReturnNBytes(0xA7, 16);
            string hwCode;

            hwCode = Encoding.ASCII.GetString(revisionCode, 0, 8).Replace("\0", "").Trim();

            return hwCode;
        }


        /// <summary>
        /// Get the hardware revision code as a number
        /// </summary>
        /// <returns>The Hardware Revision Code as a double</returns>
        public double GetHardwareRevisionCodeAsNumber()
        {
            byte[] revisionCode = this.ixoliteChassis.GetI2cReturnNBytes(0xA7, 16);
            string hwCode;

            hwCode = Encoding.ASCII.GetString(revisionCode, 0, 8).Replace("\0", "").Trim();
            hwCode = hwCode.Insert(2,".");


            return Convert.ToDouble(hwCode);
        }

        /// <summary>
        /// Get the Firmware Revision Code
        /// </summary>
        /// <returns>The Firmware Revision Code</returns>
        public string GetFirmwareRevisionCode()
        {
            byte[] revisionCode = this.ixoliteChassis.GetI2cReturnNBytes(0xA7, 16);
            string fwCode;

            fwCode = Encoding.ASCII.GetString(revisionCode, 8, 8).Replace("\0", "").Trim();
            return fwCode;
        }

        /// <summary>
        /// Get the Reciever Serial Number
        /// </summary>
        /// <returns>Reciever Serial Number</returns>
        public string GetRxSerialNumber()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnString(0xF1, 0x24);
        }

        /// <summary>
        /// Set the Reciever Serial Number
        /// </summary>
        /// <param name="serno">The serail number</param>
        public void SetRxSerialNumber(string serno)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndString(0xF0, 0x24, serno);
            sleepForSetDelay();
        }
        #endregion

        #region IMSA300pin Members
        /// <summary>
        /// Set the Tx Command register.
        /// </summary>
        /// <param name="txCmd">New register value.</param>
        public void SetTxCommand(TxCommand txCmd)
        {
            this.ixoliteChassis.SetI2cWithBytes(0x40, txCmd.Data1, txCmd.Data2, txCmd.Data3);
            sleepForSetDelay();
        }

        /// <summary>
        /// Read the current Tx Command register.
        /// </summary>
        /// <returns>Register value.</returns>
        public TxCommand ReadTxCommand()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x41,3);

            return new TxCommand(resp[0], resp[1], resp[2]);
        }

        /// <summary>
        /// Instruct the instrument to make an internal copy of the TxCommand register value.
        /// </summary>
        public void SaveTxRegister()
        {
            this.ixoliteChassis.SaveTxRegister();
        }

        /// <summary>
        /// Instruct the instrument to restore the internal copy of the TxCommand register.
        /// </summary>
        public void RestoreTxRegister()
        {
            this.ixoliteChassis.RestoreTxRegister();
            sleepForSetDelay();
        }


        /// <summary>
        /// Get the value of LsEnable bit of the  TxCommand register.
        /// </summary>
        /// <returns>True for one, false for zero.</returns>
        public bool GetLsEnable()
        {
            /* Read TxCommand. */
            TxCommand txCmd = this.ReadTxCommand();

            /* Return bit0 of byte 3. */
            return (txCmd.Data3 & 0x01) != 0;
        }

        /// <summary>
        /// Set only the LsEnable bit of the TxCommand register, leaving the other bits unmodified.
        /// </summary>
        /// <param name="lsEnable">True for one, false for zero.</param>
        public void SetLsEnable(bool lsEnable)
        {
            /* Read old TxCommand. */
            TxCommand txCmd = this.ReadTxCommand();
            byte data3 = txCmd.Data3;

            /* Zero-ify bit 0. */
//            data3 &= 0x01;
            data3 &= 0xFE;  // makes more sense

            /* One-ify bit 0 if lsEnable==true. */
            if (lsEnable)
            {
                data3 |= 0x01;
            }

            /* If needed, set new TxCommand. */
            if (txCmd.Data3 != data3)
            {
                TxCommand newTxCmd = new TxCommand(txCmd.Data1, txCmd.Data2, data3);
                this.SetTxCommand(newTxCmd);
            }
        }


        /// <summary>
        /// Set the Rx Command register.
        /// </summary>
        /// <param name="rxCmd">New register value.</param>
        public void SetRxCommand(RxCommand rxCmd)
        {
            this.ixoliteChassis.SetI2cWithBytes(0x44, rxCmd.Data1, rxCmd.Data2, rxCmd.Data3);
            sleepForSetDelay();
        }

        /// <summary>
        /// Read the current Rx Command register.
        /// </summary>
        /// <returns>Register value.</returns>
        public RxCommand ReadRxCommand()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x45, 3);
            return new RxCommand(resp[0], resp[1], resp[2]);
        }

        /// <summary>
        /// Instruct the instrument to make an internal copy of the RxCommand register value.
        /// </summary>
        public void SaveRxRegister()
        {
            this.ixoliteChassis.SaveRxRegister();
            sleepForSetDelay();
        }

        /// <summary>
        /// Instruct the instrument to restore the internal copy of the RxCommand register.
        /// </summary>
        public void RestoreRxRegister()
        {
            this.ixoliteChassis.SendI2cCommandNoPayload(0x47);
        }

        /// <summary>
        /// Select a new ITU channel.
        /// </summary>
        /// <param name="bandChannel">Band and channel.</param>
        public void SelectITUChannel(BandChannel bandChannel)
        {
            this.ixoliteChassis.SelectITUChannel(bandChannel.Band, bandChannel.Chan);
        }

        /// <summary>
        /// Select a new ITU channel.
        /// This overload will enter MSA Protect Mode, change the Channel, and then
        /// exit MSA Protect Mode if the protectModeChange parameter is TRUE.
        /// </summary>
        /// <param name="bandChannel">Band and channel.</param>
        /// <param name="protectModeChange">True if it is necessary to enter MSA Proteted Mode first</param>
        public void SelectITUChannel(BandChannel bandChannel, bool protectModeChange)
        {
            if (protectModeChange)
            {
                this.EnterProtectMode();
            }

            this.ixoliteChassis.SelectITUChannel(bandChannel.Band, bandChannel.Chan);

            if (protectModeChange)
            {
                this.ExitProtectMode();
            }
        }

        /// <summary>
        /// Get the current ITU channel setting.
        /// </summary>
        /// <returns>Band and channel.</returns>
        public BandChannel GetITUChannel()
        {
            return this.ixoliteChassis.GetITUChannel();
        }

        /// <summary>
        /// Get the current Rx Threshold (RxDTV).
        /// </summary>
        /// <returns>The Threshokd in Percent.</returns>
        public double ReadRxThreshold()
        {
            return this.ixoliteChassis.ReadRxThreshold();
        }


        /// <summary>
        /// Set the Rx Threshold (RxDTV).
        /// </summary>
        /// <param name="thresh_pc">The Threshold in Percent</param>
        public void SetRxThreshold(double thresh_pc)
        {
            this.ixoliteChassis.SetRxThreshold(thresh_pc);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get the laser bias current in mA.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetLaserBiasCurrent_mA()
        {
            return this.ixoliteChassis.GetLaserBiasCurrent();
        }

        /// <summary>
        /// Get the actual laser output power in microWatts.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetLaserOutputPower_uW()
        {
            return this.ixoliteChassis.LaserOutputPower();
        }

        /// <summary>
        /// Gets the current sub-mount temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetSubMountTemp_degC()
        {
            return this.ixoliteChassis.SubMountTemp();
        }

        /// <summary>
        /// Get Rx AC power.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetRxACPower()
        {
            return this.ixoliteChassis.RxACPower();
        }


        /// <summary>
        /// Get average Rx power in nanoWatts.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetRxAvgPower_nW()
        {
            return this.ixoliteChassis.RxAvgPower();
        }

        /// <summary>
        /// Get the laser frequency offset value.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetLaserFrequencyOffset_MHz()
        {
            return this.ixoliteChassis.LaserWavelength();
        }


        /// <summary>
        /// Get module temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetModuleTemp_degC()
        {
            return this.ixoliteChassis.ModuleTemp();
        }

        /// <summary>
        /// Get APD temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetAPDTemp_degC()
        {
            return this.ixoliteChassis.APDTemp();
        }

        /// <summary>
        /// Get module bias.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetModBias()
        {
            return this.ixoliteChassis.ModBias();
        }

        /// <summary>
        /// Get the laser's absolute temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        public double GetLaserAbsoluteTemperature_degC()
        {
            return this.ixoliteChassis.LaserAbsoluteTemperature();
        }

        /// <summary>
        /// Set the last ITU Channel number, i.e. 89, 100, etc
        /// </summary>
        /// <param name="channel"></param>
        public void SetLastITUChannel(int channel)
        {
            byte[] payload = BitConverter.GetBytes(channel * 16);

            // We now need to swap the 1st 2 bytes
            byte temp = payload[0];
            payload[0] = payload[1];
            payload[1] = temp;
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x34, payload);
        }


        /// <summary>
        /// Get the last channel of this instrument's capabilities.
        /// </summary>
        /// <returns>Register value.</returns>
        public BandChannel GetLastChannel()
        {
            return this.ixoliteChassis.GetLastChannel();
        }

        /// <summary>
        /// Gets the module type byte.
        /// </summary>
        /// <returns>Register value.</returns>
        public byte GetModuleTypeByte()
        {
            byte [] resp =  this.ixoliteChassis.GetI2cReturnNBytes(0xA1, 1);
            return resp[0];
        }

        /// <summary>
        /// Gets the module type as a string.
        /// </summary>
        /// <returns>Register value.</returns>
        public string GetModuleType()
        {
            return this.ixoliteChassis.GetModuleType();
        }

        /// <summary>
        /// Get the first channel of this instrument's capabilities.
        /// </summary>
        /// <returns>Register value.</returns>
        public BandChannel GetFirstChannel()
        {
            return this.ixoliteChassis.GetFirstChannel();
        }


        /// <summary>
        /// Get this instrumnet's channel spacing.
        /// </summary>
        /// <returns>Register value.</returns>
        public double ChannelSpacing_GHz()
        {
            return this.ixoliteChassis.ChannelSpacing();
        }

        /// <summary>
        /// Get the date of manufacture.
        /// </summary>
        /// <returns>Register value.</returns>
        public string GetManufactureDate()
        {
            return this.ixoliteChassis.GetManufactureDate_MSA();
        }

        /// <summary>
        /// Get the unit's part number.
        /// </summary>
        /// <returns>Register value.</returns>
        public string GetUnitPartNumber()
        {
            return this.ixoliteChassis.GetUnitPartNumber_MSA();
        }

        /// <summary>
        /// Query the link status.
        /// </summary>
        /// <returns>True for working. False for failed.</returns>
        public bool LinkStatus()
        {
            try { this.ixoliteChassis.LinkStatus(); }
            catch { return false; }
            return true;
        }

        /// <summary>
        /// Enter instrument "Pin mode".
        /// </summary>
        public void EnterPinMode()
        {
            this.ixoliteChassis.EnterPinMode();
            sleepForSetDelay();
        }

        /// <summary>
        /// Query the maximum I2C rate supported.
        /// </summary>
        /// <returns>Register value.</returns>
        public int ReadMaxI2CRate_kbps()
        {
            return this.ixoliteChassis.ReadMaxI2CRate();
        }

        /// <summary>
        /// Enter protect mode.
        /// </summary>
        public void EnterProtectMode()
        {
            this.ixoliteChassis.EnterProtectMode();
            sleepForSetDelay();
        }

        /// <summary>
        /// Enter vendor protection mode.
        /// </summary>
        /// <param name="password">Vendor password.</param>
        public void EnterVendorProtectMode(string password)
        {
            this.ixoliteChassis.EnterVendorProtectMode(password);
            sleepForSetDelay();
        }

        /// <summary>
        /// Enter Vendor Protect Mode using default password
        /// </summary>
        public void EnterVendorProtectMode()
        {
            /* Enable CAL_SET_PROTECT. */
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x07, Encoding.ASCII.GetBytes("BKHM"));
        }

        /// <summary>
        /// Leave protect mode.
        /// </summary>
        public void ExitProtectMode()
        {
            this.ixoliteChassis.ExitProtectMode();
            sleepForSetDelay();
        }

        /// <summary>
        /// Reset CPN.
        /// </summary>
        public void ResetCPN()
        {
            this.ixoliteChassis.ResetCPN();
            sleepForSetDelay();
        }

        /// <summary>
        /// Enter soft mode.
        /// </summary>
        public void EnterSoftMode()
        {
            this.ixoliteChassis.EnterSoftMode();
            sleepForSetDelay();
        }


        /// <summary>
        /// Perform a loopback over the I2C link.
        /// </summary>
        public void Loopback()
        {
            this.ixoliteChassis.Loopback();
        }


        /// <summary>
        /// Wait for temperature to settle
        /// </summary>
        /// <param name="count">count</param>
        /// <param name="wait_s">wait</param>
        /// <param name="target_degC">target temperature</param>
        /// <param name="devi_degC">deviation</param>
        /// <returns></returns>
        public bool WaitForTempToSettle(int count, double wait_s, double target_degC, double devi_degC)
        {
            return this.ixoliteChassis.WaitForTempToSettle(count, wait_s, target_degC, devi_degC);
        }

        /// <summary>
        /// Read the Tx Alarm Status Register 
        /// </summary>
        /// <returns>The Alarm status register, split into bytes</returns>
        public TxAlarmStatusRegister ReadTxAlarmStatusRegister()
        {
            //Do a double read, to clear any historical conditions.
            byte[] clearAlmReg = this.ixoliteChassis.GetI2cReturnNBytes(0x80, 3);
            byte [] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x80, 3);

            return new TxAlarmStatusRegister(resp[0], resp[1], resp[2]);
        }


        /// <summary>
        /// Get the state of the EOLALM (Laser end of life) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetEOLALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x01) != 0)
            {
                //EOLALM Bit is set - normal operation
                return false;
            }
            else
            {
                //EOLALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the ModTEMPALM (Modulator Temperature) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetModTEMPALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x02) != 0)
            {
                //ModTEMPALM Bit is set - normal operation
                return false;
            }
            else
            {
                //ModTEMPALM Bit is clear - alarm is active
                return true;
            }
        }


        /// <summary>
        /// Get the state of the TxOOA  (SFI-5 DESKEW) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxOOAState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x04) != 0)
            {
                //TxOOA Bit is set - normal operation
                return false;
            }
            else
            {
                //TxOOA Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the TxLOFALM (Loss of Frame) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxLOFALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x08) != 0)
            {
                //TxLOFALM Bit is set - normal operation
                return false;
            }
            else
            {
                //TxLOFALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the TxDSCERR  (Latching SFI-5 DESKEW Channel error, cleared on read) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxDSCERRState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x10) != 0)
            {
                //TxDSCERR Bit is set - normal operation
                return false;
            }
            else
            {
                //TxDSCERR Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsWAVALM (Laser Wavelength) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsWAVALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data2 & (byte)0x20) != 0)
            {
                //LsWAVALM Bit is set - normal operation
                return false;
            }
            else
            {
                //LsWAVALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the TxALM INT alarm (all TX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxALMINTState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x01) != 0)
            {
                //TxALM INT Bit is set - normal operation
                return false;
            }
            else
            {
                //TxALM INT Bit is clear - a Tx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsBIASALM (Laser bias current) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsBIASALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x02) != 0)
            {
                //LsBIASALM Bit is set - normal operation
                return false;
            }
            else
            {
                //LsBIASALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsTEMPALM (Laser temperature) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsTEMPALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x04) != 0)
            {
                //LsTEMPALM Bit is set - normal operation
                return false;
            }
            else
            {
                //LsTEMPALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the TxLOCKERR (Loss of TxPLL lock indicator) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxLOCKERRState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x08) != 0)
            {
                //TxLOCKERR Bit is set - normal operation
                return false;
            }
            else
            {
                //TxLOCKERR Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the LsPOWALM (Laser power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetLsPOWALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x20) != 0)
            {
                //LsPOWALM Bit is set - normal operation
                return false;
            }
            else
            {
                //LsPOWALM Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the ModBIASALM (Modulator bias) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetModBIASALMState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x40) != 0)
            {
                //ModBIASALM  Bit is set - normal operation
                return false;
            }
            else
            {
                //ModBIASALM  Bit is clear - alarm is active
                return true;
            }
        }


        /// <summary>
        /// Get the state of the TxFIFO ERR (Mux FIFO error indicator) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetTxFIFOERRState()
        {
            TxAlarmStatusRegister txAlarms = ReadTxAlarmStatusRegister();
            if ((txAlarms.Data3 & (byte)0x80) != 0)
            {
                //TxFIFO ERR Bit is set - normal operation
                return false;
            }
            else
            {
                //TxFIFO ERR Bit is clear - alarm is active
                return true;
            }
        }

        /// <summary>
        /// Read the Rx Alarm Status Register 
        /// </summary>
        /// <returns>The Alarm status register, split into bytes</returns>
        public RxAlarmStatusRegister ReadRxAlarmStatusRegister()
        {
            //Do a double read, to clear any historical conditions.
            byte[] clearAlmReg = this.ixoliteChassis.GetI2cReturnNBytes(0x81, 2);
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0x81, 2);

            return new RxAlarmStatusRegister(resp[0], resp[1]);
        }

        /// <summary>
        /// Get the state of the RxALM INT alarm (all RX alarms ORed)
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxALMINTState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x01) != 0)
            {
                //RxALM INT Bit is set - normal operation
                return false;
            }
            else
            {
                //RxALM INT Bit is clear - a Rx alarm is active
                return true;
            }
        }


        /// <summary>
        /// Get the state of the RxPOWALM (Loss DC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxPOWALMState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x02) != 0)
            {
                //RxPOWALM Bit is set - normal operation
                return false;
            }
            else
            {
                //RxPOWALM Bit is clear - a Rx alarm is active
                return true;
            }
        }


        /// <summary>
        /// Get the state of the RxSIGALM (Loss AC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxSIGALMState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x04) != 0)
            {
                //RxSIGALM Bit is set - normal operation
                return false;
            }
            else
            {
                //RxSIGALM Bit is clear - a Rx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the RxLOCKERR (Loss of lock of RxPOCLK) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRxLOCKERRState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x08) != 0)
            {
                //RxLOCKERR Bit is set - normal operation
                return false;
            }
            else
            {
                //RxLOCKERR Bit is clear - a Rx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the RXS(SFI-5 DEMUX status) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetRXSState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x10) != 0)
            {
                //RXS Bit is set - normal operation
                return false;
            }
            else
            {
                //RXS Bit is clear - a Rx alarm is active
                return true;
            }
        }

        /// <summary>
        /// Get the state of the PRBSERRDET (an error was detected by the PRBS error checker) alarm
        /// </summary>
        /// <returns>True if active</returns>
        public bool GetPRBSERRDETState()
        {
            RxAlarmStatusRegister rxAlarms = ReadRxAlarmStatusRegister();
            if ((rxAlarms.Data2 & (byte)0x20) != 0)
            {
                //PRBSERRDET Bit is set - normal operation
                return false;
            }
            else
            {
                //PRBSERRDET Bit is clear - a Rx alarm is active
                return true;
            }
        }


        /// <summary>
        /// Get LsBIASMON Value 
        /// </summary>
        /// <returns>Laser Bias Monitor Currrent in mA</returns>
        public double GetLsBIASMON_mA ()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cReturnNBytes(0x60, 3);
            int retVal = convert3BytesToSint32(bytes);
            return  retVal / 1000.0;
        }

        /// <summary>
        /// Get LsPOWMON  Value 
        /// </summary>
        /// <returns>Laser output power in mW</returns>
        public double GetLsPOWMON_mW()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cReturnNBytes(0x61, 3);
            int retVal = convert3BytesToSint32(bytes);
            return retVal / 1000.0;
        }

        /// <summary>
        /// Get LsTEMPMON Value 
        /// </summary>
        /// <returns>Laser Temperature in Degrees C</returns>
        public double GetLsTEMPMON_DegC()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cReturnNBytes(0x62, 3);
            int retVal = convert3BytesToSint32(bytes);
            return retVal / 1000.0;
        }

        /// <summary>
        /// Get RxPOWMON Value 
        /// </summary>
        /// <returns>Rx Power in mW</returns>
        public double GetRxPOWMON_mW()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cReturnNBytes(0x64, 3);
            int retVal = convert3BytesToSint32(bytes);
            return retVal / 1000000.0; //Hardware returns the result in nW
        }

        /// <summary>
        /// Get LsWAVEMON Value 
        /// </summary>
        /// <returns>Rx Wavelength in GHz</returns>
        public double GetLsWAVEMON_GHz()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cReturnNBytes(0x65, 3);
            int retVal = convert3BytesToSint32(bytes);
            return  retVal / 1000.0;
        }


        /// <summary>
        /// Read 300Pin MSA Edition Byte.
        /// </summary>
        /// <returns>The MSA Edition Byte</returns>
        public byte ReadMSAEdition()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0xC7, 3);

            return resp[0];
        }

        /// <summary>
        /// Read 300Pin MSA Operating Mode as Enum.
        /// </summary>
        /// <returns>The Operating Mode as MSA300PinOperatingModeEnum </returns>
        public MSA300PinOperatingModeEnum ReadOperatingMode()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0xC7, 3);
            
            MSA300PinOperatingModeEnum ret;
            if (resp[2] == 0x01)
            {
                ret = MSA300PinOperatingModeEnum.Pin;
            }
            else if (resp[2] == 0x02)
            {
                ret = MSA300PinOperatingModeEnum.Soft;
            }
            else
            {
                throw new InstrumentException ("Invalid Operating mode returned by TSFF: " + resp[2] + ". Only 0x01 and 0x02 are valid values");
            }

            return ret;
        }


        /// <summary>
        /// Read 300Pin MSA Operating Mode as a Byte
        /// </summary>
        /// <returns>The Operating Mode Byte</returns>
        public byte ReadOperatingModeByte()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0xC7, 3);

            return (resp[2]);
        }

        /// <summary>
        /// Read 300Pin MSA Capability Byte.
        /// </summary>
        /// <returns>The Capability Byte</returns>
        public byte ReadMSACapability()
        {
            byte[] resp = this.ixoliteChassis.GetI2cReturnNBytes(0xC7, 3);

            return resp[1];
        }

        #endregion

        #region IDsdbrLaserSetup Members

        /// <summary>
        /// Enter Laser Setup Mode
        /// </summary>
        public void EnterLaserSetupMode()
        {
            /* Enter vendor protect mode. */
            //this.EnterVendorProtectMode("THURSDAY");

            /* Enable MSA Protected Commands */
            this.ixoliteChassis.SetI2cWithBytes(0xC3);


            /* Enable CAL_SET_PROTECT. */
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x07, Encoding.ASCII.GetBytes("BKHM"));

            /* Enable calibration mode. */
            EnableCalibrationMode();
        }

        /// <summary>
        /// Enable Calibration Mode
        /// </summary>
        public void EnableCalibrationMode()
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x3A, 0xAA);
        }

        /// <summary>
        /// Disable Calibration mode
        /// </summary>
        public void DisableCalibrationMode()
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x3A, 0x55);
        }

        /// <summary>
        /// Get DAC Value, using indexed subcommand.
        /// </summary>
        /// <param name="index">DAC Index</param>
        /// <returns>DAC Value</returns>
        private int getDacByIndex(byte index)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x1F, index);
            sleepForSetDelay();
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x1F);
        }
                
        /// <summary>
        /// Set DAC Value, using indexed subcommand.
        /// </summary>
        /// <param name="index">DAC Index</param>
        /// <param name="dacValue">DAC Value</param>
        /// <returns>DAC Value</returns>
        private void setDacByIndex(byte index, int dacValue)
        {
            this.ixoliteChassis.SetI2cWithSubcmdIndexAndBigEndianUint(0xF0, 0x32, index, dacValue, 2);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get SOA Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetSOADAC()
        {           
            return getDacByIndex(5);
        }


        /// <summary>
        /// Set SOA Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetSOADAC(int dac)
        {
            setDacByIndex(5, dac);
        }

        /// <summary>
        /// Get SOA Dac Calibrated value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetSOADACCal()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x32);
        }


        /// <summary>
        /// Set SOA Dac Calibrated value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetSOADACCal(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x32, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Gain Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetGainDAC()
        {
            return getDacByIndex(4);
        }


        /// <summary>
        /// Set Gain Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetGainDAC(int dac)
        {
            setDacByIndex(4, dac);
        }

        /// <summary>
        /// Get FSPair value
        /// </summary>
        /// <returns>FS Pair Value</returns>
        public int GetFrontSectionPair()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x18);
        }

        /// <summary>
        /// Set Front Section Pair  value
        /// </summary>
        /// <param name="i">pair value</param>
        public void SetFrontSectionPair(int i)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x18, i, 1);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Front Even  Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetFrontEvenDAC()
        {
            return getDacByIndex(3);
        }

        /// <summary>
        /// Set Front Even Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetFrontEvenDAC(int dac)
        {
            setDacByIndex(3, dac);
        }

        /// <summary>
        /// Get Front Odd Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetFrontOddDAC()
        {
            return getDacByIndex(1);
        }

        /// <summary>
        /// Set Front ODD Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetFrontOddDAC(int dac)
        {
            setDacByIndex(1, dac);
        }

        /// <summary>
        /// Get Rear Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetRearDAC()
        {
            return getDacByIndex(6);
        }

        /// <summary>
        /// Set Rear Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetRearDAC(int dac)
        {
            setDacByIndex(6, dac);
        }


        /// <summary>
        /// Get Phase ITU Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetPhaseDAC()
        {
            return getDacByIndex(2);
        }

        /// <summary>
        /// Set Phase ITU Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetPhaseDAC(int dac)
        {
            setDacByIndex(2, dac);
        }


        /// <summary>
        /// Get Mode Center Phase Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetModeCentrePhaseDAC()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x11);
        }


        /// <summary>
        /// Set Mode Centre Phase Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetModeCentrePhaseDAC(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x11, dac, 2);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get BOL Phase Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetBOLPhaseDAC()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x10);
        }


        /// <summary>
        /// Set BOL Phase Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetBOLPhaseDAC(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x10, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Dither Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetDitherDAC()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x1A);
        }

        /// <summary>
        /// Set Dither Dac value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetDitherDAC(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x1A, dac, 1);
            sleepForSetDelay();
        }


        /// <summary>
        /// Save Channel settings
        /// </summary>
        /// <param name="chan">The Channel</param>
        public void SaveChannel(int chan)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x22, chan, 1);
            sleepForSetDelay();
        }

        /// <summary>
        /// Do Autolocker setup
        /// </summary>
        public void DoAutoLockerSetup()
        {
            this.ixoliteChassis.SendI2cCommandWithSubcmd(0xFA, 0x08);
            sleepForSetDelay();
        }


        private bool getTsffFlag(byte subcmd, byte mask)
        {
            byte[] flags = this.ixoliteChassis.GetI2cWithSubcmdReturnNBytes(0xF1, subcmd, 1);
            return (flags[0] | mask) != 0;
        }

        /// <summary>
        /// Get positive locker slope
        /// </summary>
        /// <returns>True if locker slope is positive.</returns>
        public bool GetLockerSlopePositive()
        {
            return getTsffFlag(0x20, 0x02);    /* Get bit 1 of subcmd 0x20 */
        }

        private void setTsffFlag(byte subcmd, byte mask, bool on)
        {
            /* Get byte value. */
            byte[] flags = this.ixoliteChassis.GetI2cWithSubcmdReturnNBytes(0xF1, subcmd, 1);
            byte flagZero = flags[0];

            /* Zero-ify the bits indicated in the mask. */
            flagZero &= (byte)(~mask);

            /* If on==true, one-ify the masked bits. */
            if (on)
            {
                flagZero |= mask;
            }

            /* Write value back. */
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, subcmd, flagZero);
            sleepForSetDelay();
        }

        /// <summary>
        /// Set Positive Locker Slope
        /// </summary>
        /// <param name="on">True if on</param>
        public void SetLockerSlopePositive(bool on)
        {
            setTsffFlag(0x20, 0x02, on);
        }

         /// <summary>
        /// Get Tx Coarse PotDac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetTxCoarsePot()
        {
            return getDacByIndex(27);
        }

        /// <summary>
        /// Set Tx Coarse Pot value
        /// </summary>
        /// <param name="pot">Pot Value</param>
        public void SetTxCoarsePot(int pot)
        {
            setDacByIndex(27, pot);
        }


        /// <summary>
        /// Get Ref Coarse Pot Dac value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetRefCoarsePot()
        {
            return getDacByIndex(28);
        }

        /// <summary>
        /// Set Ref Coarse Pot
        /// </summary>
        /// <param name="rcp">Pot Value</param>
        public void SetRefCoarsePot(int rcp)
        {
            setDacByIndex(28, rcp);
        }


        /// <summary>
        /// Get Locker error factor value
        /// </summary>
        /// <returns>Locker error factor</returns>
        public int GetLockerErrorFactor()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnReturnBigEndianSint(0xF1, 0x14, 2);
        }


        /// <summary>
        /// Sey Locker Error factor
        /// </summary>
        /// <param name="lef">Locker error factor</param>
        public void SetLockerErrorFactor(int lef)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianSint(0xF0, 0x14, lef, 2);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get Locker status
        /// </summary>
        /// <returns>True if on</returns>
        public bool GetLockerOn()
        {
            return getTsffFlag(0x20, 0x04);    /* Bit2 */
        }

        /// <summary>
        /// Enable/Disable Locker
        /// </summary>
        /// <param name="on">True to enable</param>
        public void SetLockerOn(bool on)
        {
            setTsffFlag(0x20, 0x04, on);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get ADC Value, using indexed subcommand.
        /// </summary>
        /// <param name="index">ADC Index</param>
        /// <returns>ADC Value</returns>
        private int getAdcByIndex(byte index)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x1E, index);
            sleepForSetDelay();
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x1E);            
        }

        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// </summary>
        /// <returns>The ADC Reading.</returns>
        public int GetRxPowerMon_ADC()
        {

            return (int)getAdcByIndex(0);
        }

        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>The ADC Reading.</returns>
        public int GetRxPowerMon_ADC(int average)
        {
            if (average != 0)
            {
                long sum = 0;
                for (int i = 0; i < average; ++i)
                {
                    sum += getAdcByIndex(0);
                }
                return (int)(sum / average);
            }
            else
            {
                return (int)getAdcByIndex(0);
            }
        }

        /// <summary>
        /// Get TransImpedance Anode ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetLockerTxMon_ADC()
        {
            return getAdcByIndex(11);
        }

        /// <summary>
        /// Get TransImpedance Anode ADC
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>ADC reading</returns>
        public int GetLockerTxMon_ADC(int average)
        {
            if (average != 0)
            {
                long sum = 0;
                for (int i = 0; i < average; ++i)
                {
                    sum += getAdcByIndex(11);
                }
                return (int)(sum / average);
            }
            else
            {
                return (int)getAdcByIndex(11);
            }
        }


        /// <summary>
        /// Get Reflectometer Monitor ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetLockerRefMon_ADC()
        {
            return getAdcByIndex(12);
        }

        /// <summary>
        /// Get Reflectometer Monitor ADC
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>ADC reading</returns>
        public int GetLockerRefMon_ADC(int average)
        {
            if (average != 0)
            {
                long sum = 0;
                for (int i = 0; i < average; ++i)
                {
                    sum += getAdcByIndex(12);
                }
                return (int)(sum / average);
            }
            else
            {
                return (int)getAdcByIndex(12);
            }
        }


        /// <summary>
        /// Get Laser Power Monitor ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetLaserPowerMon_ADC()
        {
            return (int)getAdcByIndex(14);
        }


        /// <summary>
        /// Get Laser Power Monitor ADC
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>ADC reading</returns>
        public int GetLaserPowerMon_ADC(int average)
        {
            if (average != 0)
            {
                long sum = 0;
                for (int i = 0; i < average; ++i)
                {
                    sum += getAdcByIndex(14);
                }
                return (int)(sum / average);
            }
            else
            {
                return (int)getAdcByIndex(14);
            }
        }

        /// <summary>
        /// Get LS Phase Sense Monitor ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetLsPhaseSense()
        {
            return getAdcByIndex(15);
        }

        /// <summary>
        /// Get LS Phase Sense Monitor ADC
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>ADC reading</returns>
        public int GetLsPhaseSense(int average)
        {
            if (average != 0)
            {
                long sum = 0;
                for (int i = 0; i < average; ++i)
                {
                    sum += getAdcByIndex(15);
                }
                return (int)(sum / average);
            }
            else
            {
                return (int)getAdcByIndex(15);
            }
        }


        /// <summary>
        /// Get EOL Phase ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetEOLPhaseADC_Min()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x1B);
        }

        /// <summary>
        /// Set EOL Phase ADC Min - The minimum phase current allowed for this channel 
        /// <param name="pmin"> The minimum phase current allowed for this channel </param>        
         /// </summary>
        public void SetEOLPhaseADC_Min(int pmin)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x1B, pmin, 2);
            sleepForSetDelay();
        }

     
         /// <summary>
        /// Get EOL Phase ADC Max
        /// </summary>
        /// <returns>The maximum phase current allowed for this channel </returns>
        public int GetEOLPhaseADC_Max()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x1C);
        }


        /// <summary>
        /// Set EOL Phase ADC Max
        /// </summary>
        /// <param name="pmax">The maximum phase current allowed for this channel </param>
        public void SetEOLPhaseADC_Max(int pmax)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x1C, pmax, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Optical power monitor slope in uW/LSbit of the monitor ADC
        /// </summary>
        /// <returns>power monitor slope</returns>
        public int GetPowerMonFactor()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x12);
        }

        /// <summary>
        /// Set Optical power monitor slope in uW/LSbit of the monitor ADC
        /// </summary>
        /// <param name="pmf">power monitor slope</param>
        public void SetPowerMonFactor(int pmf)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x12, pmf, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Optical power monitor offset in uW
        /// </summary>
        /// <returns>power mon offset</returns>
        public int GetPowerMonOffset()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x13);
        }


        /// <summary>
        /// Set Optical power monitor offset in uW
        /// </summary>
        /// <param name="pmo">power mon offset</param>
        public void SetPowerMonOffset(int pmo)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x13, pmo, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the current dither dac multiplier setting. 
        /// </summary>
        /// <returns>dither dac multiplier setting</returns>
        public int GetSBSDitherMultiplier()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x19);
        }

        /// <summary>
        /// Set the dither dac multiplier 
        /// </summary>
        /// <param name="mult">multiplier</param>
        public void SetSBSDitherMultiplier(int mult)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x19, mult, 1);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Power Mon Pot value
        /// </summary>
        /// <returns>Power mon pot value</returns>
        public int GetPowerMonPot()
        {
            return this.ixoliteChassis.GetPowerMonPot();
        }

        /// <summary>
        /// Set Power Mon Pot value
        /// </summary>
        /// <param name="pot">pot value</param>
        public void SetPowerMonPot(int pot)
        {
            this.ixoliteChassis.SetPowerMonPot(checked((short)pot));
        }

        /// <summary>
        /// Get SBS Dither state - Not implemented
        /// </summary>
        /// <returns>SBS Dither State</returns>
        public bool GetSBSDitherOn()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Enable/Disable SBS Dither - Not implemented.
        /// </summary>
        /// <param name="on">True  for enable</param>
        public void SetSBSDitherOn(bool on)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Get power profiling state - not Implemented
        /// </summary>
        /// <returns>True if enabled</returns>
        public bool GetPowerProfilingOn()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Enable/ddisable Power profiling
        /// </summary>
        /// <param name="on">True to enable</param>
        public void SetPowerProfilingOn(bool on)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        /// <summary>
        /// Get Automatic Power Conttrol Loop Status
        /// </summary>
        /// <returns>True if enabled</returns>
        public bool GetPowerControlLoopOn()
        {
            return getTsffFlag(0x08, 0x80);
        }

        /// <summary>
        /// Enable/disable Automatic Power control loop
        /// Power Control == Bit 7
        /// </summary>
        /// <param name="enabled">True to enable</param>
        public void SetPowerControlLoopOn(bool enabled)
        {
            setTsffFlag(0x08, 0x80, enabled);
        }

        /// <summary>
        /// Enable/Disable Crossing Point control loop
        /// Crossing Control == Bit 5
        /// </summary>
        /// <param name="enabled"></param>
        public void SetCrossingControlLoopOn(bool enabled)
        {
            setTsffFlag(0x08, 0x20, enabled);
        }

        /// <summary>
        /// Set the CAL_SET_SOA Register
        /// </summary>
        /// <param name="value"></param>
        public void SetCalSoaRegister(int value)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x21, value, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the CAL_SET_SOA Register value
        /// </summary>
        /// <returns>value</returns>
        public int GetCalSoaRegister()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x21);
        }


        #endregion

        #region TSFF Only

        /// <summary>
        /// Enter Receiver Setup Mode
        /// </summary>
        public void EnterRxSetupMode()
        {

            /* Enable MSA Protected Commands */
            this.ixoliteChassis.SetI2cWithBytes(0xC3);


            /* Enable CAL_SET_PROTECT - i.e. enable Vendor protected Commands */
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x07, Encoding.ASCII.GetBytes("BKHM"));
        }


        /// <summary>
        /// Get DAC_PWC value
        /// </summary>
        /// <returns>DAC_PWC value</returns>
        public int GetPWCDac()
        {
            return getDacByIndex(0x09);
        }

        /// <summary>
        /// Set DAC_PWC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetPWCDac(int dac)
        {
            setDacByIndex(0x09, dac);
        }
        
        /// <summary>
        /// Get Mz Bias Left DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftBiasDac()
        {
            return getDacByIndex(0x0A);
        }


        /// <summary>
        /// Set MZ Left Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzLeftBiasDac(int dac)
        {
            setDacByIndex(0x0A, dac);
        }

        /// <summary>
        /// Get Mz Bias Left DAC Nominal value (CAL_GET_LEFT_ARM_BIAS)
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftBiasDacNominal()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x03);
        }

        /// <summary>
        /// Set MZ Left Bias DAC Nominal Value (CAL_SET_LEFT_ARM_BIAS)
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzLeftBiasDacNominal(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x03, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Mz Bias Right DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzRightBiasDac()
        {
            return getDacByIndex(0x0B);
        }

        /// <summary>
        /// Set MZ Right Bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzRightBiasDac(int dac)
        {
            setDacByIndex(0x0B, dac);
        }

        /// <summary>
        /// Get Mz Mod DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzModSetDac()
        {
            return getDacByIndex(0x0C);
        }

        /// <summary>
        /// Set MZ Mod Set DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetMzModSetDac(int dac)
        {
            setDacByIndex(0x0C, dac);
        }

        /// <summary>
        /// Get Mz Mod DAC Nominal value (CAL_GET_MODSET)
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzModSetDacNominal()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x02);
        }

        /// <summary>
        /// Set Mz Mod DAC Nominal value (CAL_SET_MODSET)
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzModSetDacNominal(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x02, dac, 2);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get Mz Left Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftImbalDac()
        {
            return getDacByIndex(0x19);
        }

        /// <summary>
        /// Set Mz Left Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzLeftImbalDac(int dac)
        {
            setDacByIndex(0x19, dac);
        }


        /// <summary>
        /// Get Mz Left Imbalance DAC Nominal value (CAL_GET_LEFT_IMBALANCE)
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzLeftImbalDacNominal()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x04);
        }

        /// <summary>
        /// Set Mz Left Imbalance DAC Nominal value (CAL_SET_LEFT_IMBALANCE)
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzLeftImbalDacNominal(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x04, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Mz Right Imbalance DAC value
        /// </summary>
        /// <returns>Dac Value</returns>       
        public int GetMzRightImbalDac()
        {
            return getDacByIndex(0x18);
        }

        /// <summary>
        /// Set Mz Right Imbalance DAC value
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzRightImbalDac(int dac)
        {
            setDacByIndex(0x18, dac);
        }


        /// <summary>
        /// Get Mz Right Imbalance DAC Nominal value (CAL_GET_RIGHT_IMBALANCE)
        /// </summary>
        /// <returns>Dac Value</returns>
        public int GetMzRightImbalDacNominal()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x05);
        }

        /// <summary>
        /// Set Mz Right Imbalance DAC Nominal value (CAL_SET_RIGHT_IMBALANCE)
        /// </summary>
        /// <param name="dac">Dac Value</param>
        public void SetMzRightImbalDacNominal(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x05, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get MMI DC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetMmiDc_ADC()
        {
            return getAdcByIndex(17);
        }

        /// <summary>
        /// Get MMI AC ADC
        /// </summary>
        /// <returns>ADC reading</returns>
        public int GetMmiAc_ADC()
        {
            return getAdcByIndex(18);
        }

        /// <summary>
        /// Get M10 APD voltage bias DAC setting
        /// (CAL_GET_APD_BIAS)
        /// </summary>
        /// <returns>DAC Value</returns>
        public int GetM10ApdBiasVoltageDac()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x06);
        }

        /// <summary>
        /// Set M10 APD voltage bias DAC
        /// (CAL_SET_APD_BIAS)
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetM10ApdBiasVoltageDac(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x06, dac, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get Offset for Mz Crossing  control loop 
        /// (CAL_GET_MMIDIFF_OFFSET)
        /// </summary>
        /// <returns>The offset</returns>
        public int GetMmiDiffOffset()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnReturnBigEndianSint(0xF1, 0x0A, 2);
        }

        /// <summary>
        /// Set Offset for Mz Crossing  control loop. 
        /// (CAL_SET_MMIDIFF_OFFSET)
        /// </summary>
        /// <param name="offset">offset</param>
        public void SetMmiDiffOffset(int offset)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianSint(0xF0, 0x0A, offset, 2);
            sleepForSetDelay();
        }


        /// <summary>
        /// Get Rx Loss Of Signal (LOS) Alarm Threshold
        /// (CAL_GET_LOS_THRESHOLD)
        /// </summary>
        /// <returns>The Threshold setting</returns>
        public int GetRxLOSAlarmThreshold()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x0B);
        }

        /// <summary>
        /// Set the Rx Power Monitor Offset value
        /// </summary>
        /// <param name="count"></param>
        public void SetRxPowMonOffset(int count)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianSint(0xF0, 0x09, count, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get the RX power Monitor Offset Count
        /// </summary>
        /// <returns></returns>
        public int GetRxPowMonOffset()
        {
            return this.ixoliteChassis.GetI2cWithSubcmdReturnReturnBigEndianSint(0xF1, 0x09, 2);
        }

        /// <summary>
        /// Set Rx Loss Of Signal (LOS) Alarm Threshold
        /// (CAL_SET_LOS_THRESHOLD)
        /// </summary>
        /// <returns>The Threshold setting</returns>
        public void  SetRxLOSAlarmThreshold(int threshold)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint (0xF0, 0x0B, threshold, 2);
            sleepForSetDelay();
        }

        /// <summary>
        /// Get M3 APD voltage bias DAC setting
        /// (CAL_GET_APD_M3BIAS)
        /// </summary>
        /// <returns>DAC Value</returns>
        public int GetM3ApdBiasVoltageDac()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x0F);
        }

        /// <summary>
        /// Set M3 APD voltage bias DAC
        /// (CAL_SET_APD_M3BIAS)
        /// </summary>
        /// <param name="dac">DAC value</param>
        public void SetM3ApdBiasVoltageDac(int dac)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x0F, dac, 2);
            sleepForSetDelay();
        }
        /// <summary>
        /// Set Index to position in lookup table to store raw optical power monitor reading with adjustment for Apd voltage. 
        /// (CAL_SET_RX_POWER_MON)
        /// </summary>
        /// <param name="calPoint">The index</param>
        public void SetRxPowMonCalibrationPoint(byte calPoint)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x1D, calPoint);
        }

        /// <summary>
        /// Set Position in n point look up table to store Mmitap derived power reading used for interpolating Tx power
        /// (CAL_SET_TX_POWER_MON)
        /// </summary>
        /// <param name="calPoint">The index</param>
        public void SetTxPowMonCalibrationPoint(byte calPoint)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF0, 0x26, calPoint);
        }

        /// <summary>
        /// Get SOA Slope in nanoWatts/dacCount
        /// (CAL_GET_SOA_SLOPE)
        /// </summary>
        /// <returns>The SOA Slope</returns>
        public int GetSoaSlope()
        {
            return ixoliteChassis.GetI2cWithSubcmdReturnBigEndianUint(0xF1, 0x27);
        }

        /// <summary>
        /// Store SOA Slope in nanoWatts/dacCount
        /// (CAL_SET_SOA_SLOPE)
        /// </summary>
        /// <param name="slope">The slope</param>
        public void SetSoaSlope (int slope)
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBigEndianUint(0xF0, 0x27, slope, 2);
        }


        /// <summary>
        /// Get MZ Submount Temperature (MzTEMPMON) 
        /// </summary>
        /// <returns>The temperature in Deg C</returns>
        public double GetMzSubmountTemperature_DegC()
        {
            byte[] bytes = this.ixoliteChassis.GetI2cWithSubcmdReturnNBytes(0xF1, 0x39, 3);
            int retVal = convert3BytesToSint32(bytes);
            return retVal / 100;
        }

        /// <summary>
        /// Disable Modulation from Laser Driver (DO_MOD_ENABLE_OFF)
        /// </summary>
        public void DisableMzLaserModulation()
        {
            this.ixoliteChassis.SetI2cWithBytes(0xFA, 0x09);
        }

        /// <summary>
        /// Enable Modulation from Laser Driver (DO_MOD_ENABLE_ON)
        /// </summary>
        public void EnableMzLaserModulation()
        {
            this.ixoliteChassis.SetI2cWithBytes(0xFA, 0x0A);
        }

        /// <summary>
        /// Disable Laser output
        /// </summary>
        public void DisableLaserOutput()
        {
            this.SetLsEnable(true);
        }

        /// <summary>
        /// Enable Laser Output
        /// </summary>
        public void EnableLaserOutput()
        {
            this.SetLsEnable(false);
        }

        /// <summary>
        /// Disable Internal PRBS generator
        /// </summary>
        public void DisableInternalPrbs()
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF2, 0x0B, 0x67);
        }

        /// <summary>
        /// Enable Internal PRBS generator
        /// </summary>
        public void EnableInternalPrbs()
        {
            this.ixoliteChassis.SetI2cWithSubcmdAndBytes(0xF2, 0x0B, 0xE7);
        }


        #endregion

        #region Helper Methods

        /// <summary>
        /// Converts an Array of 3 Bytes to a Sint32
        /// </summary>
        /// <param name="bytes">Array of 3 bytes</param>
        /// <returns>The Signed 32-bit integer</returns>
        private Int32 convert3BytesToSint32(byte[] bytes)
        {
            byte[] temp = new byte [4];
                       
            temp[0] = bytes[2];
            temp[1] = bytes[1];
            temp[2] = bytes[0];

            if ((temp[2] & 0x80) == 0x80) //check state of upper bit of 24bit MSB
            {
                temp[3] = 0xFF; // if set, sign is negative, put 1's into 32bit MSB
            }
            else
            {
                temp[3] = 0; //if clear sign is positive, put 0's into 32bit MSB
            }
            return BitConverter.ToInt32(temp, 0);
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// MSA300PinOperatingModeEnum.cs
// 
// Author: Joseph Olajubu
// Design: Tunable Module Driver DD

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// The OPR (Operating Mode) part of the MSA 300 Pin Read Edition and Mode Command. 
    /// </summary>
    public enum MSA300PinOperatingModeEnum
    {
        /// <summary>Pin mode</summary>
        Pin = 0x01,
        /// <summary>Soft mode</summary>
        Soft = 0x02
    }
}
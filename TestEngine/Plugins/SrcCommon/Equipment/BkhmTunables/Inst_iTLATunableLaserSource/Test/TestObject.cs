using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.ChassisNS;

namespace iTLATLS_Harness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_IxoliteWrapper testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_iTLATunableLaserSource tls;        
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create equipment objects
            this.testChassis = new Chassis_IxoliteWrapper("Chassis_IxoliteWrapper", "Chassis_IxoliteWrapper", "COM,2,9600");
            this.tls = new Inst_iTLATunableLaserSource("Inst_iTLATunableLaserSource", "Chassis_IxoliteWrapper", "", "", this.testChassis);

            this.testChassis.EnableLogging = true;
            this.testChassis.IsOnline = true;
            this.tls.IsOnline = true;
            this.tls.EnableLogging = true;
            this.testChassis.Timeout_ms = 10000;
            this.testChassis.SerialPort.ReadTimeout = 10000;
            this.testChassis.SerialPort.WriteTimeout = 10000;
            this.tls.Timeout_ms = 10000;
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            this.testChassis.IsOnline = false;
            this.testChassis.EnableLogging = false;
            this.tls.IsOnline = false;
            this.tls.EnableLogging = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");

            this.tls.SetDefaultState();
            TestOutput("Set Defaut State OK");
            TestOutput("MaximumWavelengthINnm = " + this.tls.MaximumWavelengthINnm);
            TestOutput("MinimumWavelengthINnm = " + this.tls.MinimumWavelengthINnm);
            TestOutput("MaximumOuputPower = " + this.tls.MaximumOuputPower.ToString());
            TestOutput("MinimumOutputPower = " + this.tls.MinimumOutputPower.ToString());
            this.tls.WavelengthINnm = 1545;
            TestOutput(string.Format("Wavelength Set OK. Wavelength = {0}", this.tls.WavelengthINnm));
            this.tls.BeamEnable = true;
            TestOutput(string.Format("BeamEnable Set OK. BeamEnable = {0}", this.tls.BeamEnable.ToString()));
            this.tls.Power = 13;
            TestOutput(string.Format("Power Set OK. Power = {0}", this.tls.Power));
            this.tls.BeamEnable = false;
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

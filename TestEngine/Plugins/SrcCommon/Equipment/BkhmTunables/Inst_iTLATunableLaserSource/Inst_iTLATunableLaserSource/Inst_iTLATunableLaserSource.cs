// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_iTLATunableLaserSource.cs
//
// Author: koonlin.peng, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.Instruments
{
    
    /// <summary>
    /// TODO: 
    /// 1. Add reference to the Instrument type you are implementing.
    /// 2. Add reference to the Chassis type you are connecting via.
    /// 3. Change the base class to the type of instrument you are implementing.
    /// 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    /// 5. Fill in the gaps. 
    /// </summary>
    public class Inst_iTLATunableLaserSource : InstType_TunableLaserSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_iTLATunableLaserSource(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Bookham iTLA TLS 01",			// hardware name 
                "0.0.0.0",  			// minimum valid firmware version 
                "2.0.0.0");			// maximum valid firmware version 
            ValidHardwareData.Add("Inst_iTLATunableLaserSource", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_IxoliteWrapper",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Inst_iTLATunableLaserSource", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_IxoliteWrapper)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "1.0.0.0";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Bookham iTLA TLS 01";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.PowerUnits = EnPowerUnits.dBm;
            //add this as  error : SoftwareEnableOutput failed
            System.Threading.Thread.Sleep(1000); 
            // as some error appear , use try catch to find the cause
            try
            {
                               
                    this.BeamEnable = false;
                
            }
            catch (Exception)
            { 
            }
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Tunable Laser Source override methods
        
        /// <summary>
        /// Enables/disables the TLS channel beam output.
        /// </summary>
        public override bool BeamEnable
        {
            get
            {
                return this.instrumentChassis.GetSoftwareEnableOutput();
            }
            set
            {

               
                this.instrumentChassis.SoftwareEnableOutput(value);
                System.Threading.Thread.Sleep(1000);
                
            }
        }
        
        /// <summary>
        /// Returns the maximum settable output power for the TLS channel.
        /// </summary>
        public override double MaximumOuputPower
        {
            get
            {
                if (this.pwr_Unit == EnPowerUnits.dBm)
                {
                    return this.instrumentChassis.GetOpticalPowerMax();
                }
                else
                {
                    return Alg_PowConvert_dB.Convert_dBmtomW(this.instrumentChassis.GetOpticalPowerMax());
                }
            }
        }

        /// <summary>
        /// Returns the maximum settable wavelength for the TLS channel.
        /// </summary>
        public override double MaximumWavelengthINnm
        {
            get
            {
                return Math.Round(opticspeed_mps / this.instrumentChassis.GetLaserFirstFreq(), 3);
            }
        }
        
        /// <summary>
        /// Returns the minimum settable output power for the TLS channel.
        /// </summary>
        public override double MinimumOutputPower
        {
            get
            {
                if (this.pwr_Unit == EnPowerUnits.dBm)
                {
                    return this.instrumentChassis.GetOpticalPowerMin();
                }
                else
                {
                    return Alg_PowConvert_dB.Convert_dBmtomW(this.instrumentChassis.GetOpticalPowerMin());
                }
            }
        }
        
        /// <summary>
        /// Returns the minimum settable wavelength for the TLS channel
        /// </summary>
        public override double MinimumWavelengthINnm
        {
            get
            {
                return Math.Round(opticspeed_mps / this.instrumentChassis.GetLaserLastFreq(), 3);
            }
        }
        
        /// <summary>
        /// Reads/sets the signal output power level for the TLS channel.
        /// </summary>
        public override double Power
        {
            get
            {
                if (this.pwr_Unit == EnPowerUnits.dBm)
                {
                    return this.instrumentChassis.GetLaserOutputPower();
                }
                else
                {
                    return Alg_PowConvert_dB.Convert_dBmtomW(this.instrumentChassis.GetLaserOutputPower());
                }
            }
            set
            {
                if (value < this.MinimumOutputPower|| value > this.MaximumOuputPower)
                    throw new Exception(string.Format("Invalid Power = {0}", value));
                if (this.pwr_Unit == EnPowerUnits.dBm)
                {
                    this.instrumentChassis.SetPowerSetpoint(value);
                }
                else
                {
                    this.instrumentChassis.SetPowerSetpoint(Alg_PowConvert_dB.Convert_mWtodBm(value));
                }
                System.Threading.Thread.Sleep(1000);
            }
        }
        
        /// <summary>
        /// Reads/sets the power units to be used for the TLS channel.
        /// </summary>
        public override InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                return this.pwr_Unit;
            }
            set
            {
                this.pwr_Unit = value;
            }
        }
        
        /// <summary>
        /// Reads/sets the output signal wavelength for the TLS channel.
        /// </summary>
        public override double WavelengthINnm
        {
            get
            {
                return Math.Round(opticspeed_mps / this.instrumentChassis.GetLaserFrequency(), 3);
            }
            set
            {
                double dbLaserFrequency_GHz = (opticspeed_mps / value);
                double channel = (dbLaserFrequency_GHz - this.instrumentChassis.GetLaserFirstFreq()) / this.instrumentChassis.GetFrequencyGrid() + 1;
                this.instrumentChassis.SelectChannel(Convert.ToInt16(channel));
                System.Threading.Thread.Sleep(1000);
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        public Chassis_IxoliteWrapper instrumentChassis;

        /// <summary>
        /// 
        /// </summary>
        const double opticspeed_mps = 299792458;

        /// <summary>
        /// 
        /// </summary>
        InstType_TunableLaserSource.EnPowerUnits pwr_Unit = EnPowerUnits.dBm;

        #endregion
    }
}

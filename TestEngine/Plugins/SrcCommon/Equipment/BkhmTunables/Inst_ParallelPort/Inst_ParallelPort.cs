// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestSolution.Instruments
//
// Inst_ParallelPort.cs
//
// Author: William Godfrey, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver to control the PC parallel port in co-operation with the Ixolite
    /// library.
    /// </summary>
    public class Inst_ParallelPort : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_ParallelPort(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            /* Add hardware record. */
            ValidHardwareData.Add(
                "ParallelPort",
                new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));

            /* Add chassis record. */
            ValidChassisDrivers.Add(
                "Chassis_IxoliteWrapper",
                new InstrumentDataRecord("Chassis_IxoliteWrapper", "0.0.0.0", "2.0.0.0"));

            /* Save chassis object reference. */
            ixoliteChassis = (Chassis_IxoliteWrapper)base.InstrumentChassis;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get {   return "Unknown";   }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get {   return "Unknown";   }
        }

        /// <summary>
        /// Set instrument to default state by lowering all non-I2C bits.
        /// </summary>
        public override void SetDefaultState()
        {
            byte nonI2cDataMask = this.ixoliteChassis.AllowParPortI2CReserved ? (byte)0xff : (byte)0xfc;
            this.ixoliteChassis.SetParPortData(nonI2cDataMask, 0);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Parallel Port control
        /// <summary>
        /// Sets state of pin Dn (AKA DB25 pin n+2).
        /// </summary>
        /// <remarks>
        /// D0 maps to DB25 pin 2. D7 maps to DB25 pin 9.
        /// D0 and D1 are used for I2C.
        /// </remarks>
        /// <param name="n">Dn pin to modify.</param>
        /// <param name="state">New state.</param>
        public void SetOutputPin(int n, bool state)
        {
            /* Check range of n. */
            if ((n < 0) || (n > 7))
            {
                throw new InstrumentException("SetOutputPin called with out of range n. (" + n + ").");
            }

            /* Check for I2C reserved pins. */
            if (!ixoliteChassis.AllowParPortI2CReserved && ((n == 0) || (n == 1)))
            {
                throw new InstrumentException("SetOutputPin called for pin D" + n + ". (An I�C reserved pin.)");
            }

            byte mask = (byte)(1 << n);
            byte status; 
            if (state)
            {
                status = mask;
            }
            else
            {
                status = 0;
            }
            this.ixoliteChassis.SetParPortData(mask, status);
        }

        /// <summary>
        /// Gets the state of pin Sn. (S3=Pin15, S4=Pin13, S5=Pin12, S6=Pin10 S7=Pin11)
        /// </summary>
        /// <param name="n">Pin Sn to read.</param>
        /// <returns>Read state.</returns>
        public bool GetInputPin(int n)
        {
            /* Range check n. */
            if ((n < 3) || (n > 7))
            {
                throw new InstrumentException("GetInputPin called with out of range n. (" + n + ").");
            }

            /* Check for I2C reserved pins. */
            if (!ixoliteChassis.AllowParPortI2CReserved && ((n == 5) || (n == 7)))
            {
                throw new InstrumentException("GetInputPin called for pin S" + n + ". (An I�C reserved pin.)");
            }

            /* Read specified bit from parallel port status register. */
            byte mask = (byte)(1 << n);
            bool bitval = (this.ixoliteChassis.GetParPortStatus() & mask) != 0;

            /* If bit 7, return bitval. Else, invert bitval. */
            return (n == 7) ? bitval : !bitval;
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_IxoliteWrapper ixoliteChassis;
        #endregion
    }
}

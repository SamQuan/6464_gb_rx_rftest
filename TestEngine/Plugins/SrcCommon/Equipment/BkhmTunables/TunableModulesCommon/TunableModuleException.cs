// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// TunableModuleException.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Exception manage for tunable module
    /// </summary>
    public class TunableModuleException :ChassisException
    {
        /* Private varibales behind properties. */
        private TunableProtocol protocol;
        private bool errorPresent;
        private int errorCode;

        /// <summary>
        /// Construct exception with protocol, but no error code.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="protocol">Protocol where error detected.</param>
        public TunableModuleException(string message, TunableProtocol protocol)
            : base(message)
        {
            this.protocol = protocol;
            this.errorPresent = false;
            this.errorCode = 0;
        }

        /// <summary>
        /// Construct exception with protocol and error code.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="protocol">Protocol where error detected.</param>
        /// <param name="errorCode">Protocol specific error code.</param>
        public TunableModuleException(string message, TunableProtocol protocol, int errorCode)
            : base(message)
        {
            this.protocol = protocol;
            this.errorPresent = true;
            this.errorCode = errorCode;
        }

        /// <summary>
        /// Gets protocol where error was detected.
        /// </summary>
        public TunableProtocol Protocol
        {
            get { return this.protocol; }
        }

        /// <summary>
        /// Gets indication if exception contains an error code.
        /// </summary>
        public bool ErrorCodePresent
        {
            get { return this.errorPresent; }
        }

        /// <summary>
        /// Gets protocol specific error code.
        /// </summary>
        public int ErrorCode
        {
            get { return this.errorCode; }
        }

        /// <summary>
        /// Convert exception to a string.
        /// </summary>
        /// <returns>Exception contents as string.</returns>
        public override string ToString()
        {
            string returnStr = base.ToString() + " " + this.Message + " (" + this.Protocol;
            if (this.ErrorCodePresent)
            {
                returnStr += "/" + this.ErrorCode;
            }
            returnStr += ")";

            return returnStr;
        }
    }

    /// <summary>
    /// Enum indicating which protocol an error occurred on.
    /// </summary>
    public enum TunableProtocol
    {
        /// <summary>MSA Command protocol.</summary>
        MSA,
        /// <summary>OIF Register access protocol.</summary>
        OIF
    }
}

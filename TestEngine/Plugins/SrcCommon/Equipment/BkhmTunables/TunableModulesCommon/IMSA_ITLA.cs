using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Wrapper functions for the OIF standard registers.
    /// See "Integrable Tunable Laser Assembly MSA (ITLA-MSA)" by
    /// the Optical Internetworking Forum.
    /// </summary>
    public interface IMSA_ITLA
    {
        /// <summary>
        /// Get NOP register value.
        /// </summary>
        /// <returns>Register value.</returns>
        int GetNOP();

        /// <summary>
        /// Get device type.
        /// </summary>
        /// <returns>Device type string.</returns>
        string GetDeviceType();

        /// <summary>
        /// Get manufacturer name.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetManufacturer();

        /// <summary>
        /// Get model number.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetModelNumber();

        /// <summary>
        /// Get serial number of unit.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetSerialNumber();

        /// <summary>
        /// Get date of manufacture.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetManufacturingDate();

        /// <summary>
        /// Get release code.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetReleaseCode();

        /// <summary>
        /// Get "release backwards" value.
        /// </summary>
        /// <returns>Register value.</returns>
        string GetReleaseBackwards();

        /// <summary>
        /// Save current configuration to NV store.
        /// </summary>
        void SaveConfig();

        /// <summary>
        /// Get the IO capabilities register.
        /// </summary>
        /// <returns>Register value.</returns>
        int GetIOCap();

        /// <summary>
        /// Set the IO capabilities register.
        /// </summary>
        /// <param name="value">Register value.</param>
        void SetIOCap(int value);

        /// <summary>
        /// Get the OIF Fatal status flags.
        /// </summary>
        /// <returns>Register value.</returns>
        OifStatus GetStatusF();

        /// <summary>
        /// Clear/acknowledge the OIF Fatal status flags.
        /// </summary>
        void ClearStatusF();

        /// <summary>
        /// Get the OIF Warning status flags.
        /// </summary>
        /// <returns>Register value.</returns>
        OifStatus GetStatusW();

        /// <summary>
        /// Clear/acknowledge the OIF Warning status flags.
        /// </summary>
        void ClearStatusW();

        /// <summary>
        /// Get the fatal power threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetFPowTh_dB();

        /// <summary>
        /// Set the fatal power threshold register.
        /// </summary>
        /// <param name="thresh">Register value.</param>
        void SetFPowTh_dB(double thresh);

        /// <summary>
        /// Get the warning power threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetWPowTh_dB();

        /// <summary>
        /// Set the warning power threshold register.
        /// </summary>
        /// <param name="th">Register value.</param>
        void SetWPowTh_dB(double th);

        /// <summary>
        /// Get the fatal frequency threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetFFreqTh_GHz();

        /// <summary>
        /// Set the fatal frequency threshold register.
        /// </summary>
        /// <param name="th">Register value.</param>
        void SetFFreqTh_GHz(double th);

        /// <summary>
        /// Get the warning frequency threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetWFreqTh_GHz();

        /// <summary>
        /// Set the warning frequency threshold register.
        /// </summary>
        /// <param name="th">Register value.</param>
        void SetWFreqTh_GHz(double th);

        /// <summary>
        /// Get the fatal thermal threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetFThermTh_degC();

        /// <summary>
        /// Set the fatal thermal threshold register.
        /// </summary>
        /// <param name="th">Register value.</param>
        void SetFThermTh_degC(double th);

        /// <summary>
        /// Get the warning thermal threshold register.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetWThermTh_degC();

        /// <summary>
        /// Set the warning thermal threshold register.
        /// </summary>
        /// <param name="th">Register value.</param>
        void SetWThermTh_degC(double th);

        /// <summary>
        /// Get the status flags that will trigger an SRQ event.
        /// </summary>
        /// <returns>Register value.</returns>
        OifStatus GetSRQT();

        /// <summary>
        /// Set the status flags to trigger an SRQ event.
        /// </summary>
        /// <param name="mask">New register value.</param>
        void SetSRQT(OifStatus mask);

        /// <summary>
        /// Get the status flags that will trigger a FATAL event.
        /// </summary>
        /// <returns>Register value.</returns>
        OifStatus GetFATALT();

        /// <summary>
        /// Set the status flags to trigger a FATAL event.
        /// </summary>
        /// <param name="mask">New register value.</param>
        void SetFATALT(OifStatus mask);

        /// <summary>
        /// Get the status flags that will trigger an ALM event.
        /// </summary>
        /// <returns>Register value.</returns>
        OifStatus GetALMT();

        /// <summary>
        /// Set the status flags to trigger an ALM event.
        /// </summary>
        /// <param name="mask">New register value.</param>
        void SetALMT(OifStatus mask);

        /// <summary>
        /// Get current channel.
        /// </summary>
        /// <returns>Channel number.</returns>
        int GetChannel();

        /// <summary>
        /// Select a channel.
        /// </summary>
        /// <param name="chan">Channel number.</param>
        void SelectChannel(int chan);

        /// <summary>
        /// Get optical power set point.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetPowerSetpoint_dBm();

        /// <summary>
        /// Set optical power set point.
        /// </summary>
        /// <param name="pwr">New register value.</param>
        void SetPowerSetpoint_dBm(double pwr);

        /// <summary>
        /// Get software flag to enable optical output.
        /// </summary>
        /// <returns>True for on, flase for off.</returns>
        bool GetSoftwareEnableOutput();

        /// <summary>
        /// Set software flag to enable optical output.
        /// </summary>
        /// <param name="sena">True for on, flase for off.</param>
        void SoftwareEnableOutput(bool sena);

        /// <summary>
        /// Reset the module.
        /// </summary>
        void ModuleReset();

        /// <summary>
        /// Perform a soft reset.
        /// </summary>
        void SoftReset();

        /// <summary>
        /// Get ADT flag.
        /// </summary>
        /// <returns>Register value.</returns>
        bool GetADT();

        /// <summary>
        /// Set ADT flag.
        /// </summary>
        /// <param name="adt">New register value.</param>
        void SetADT(bool adt);

        /// <summary>
        /// Get SDF flag.
        /// </summary>
        /// <returns>Register value.</returns>
        bool GetSDF();

        /// <summary>
        /// Set SDF flag.
        /// </summary>
        /// <param name="sdf">New register value.</param>
        void SetSDF(bool sdf);

        /// <summary>
        /// Get frequency spacing grid.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetFrequencyGrid_GHz();

        /// <summary>
        /// Set frequency spacing grid.
        /// </summary>
        /// <param name="GHz">Register value.</param>
        void SetFrequencyGrid_GHz(double GHz);

        /// <summary>
        /// Get channel #1's frequency.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetFirstChanFreq_GHz();

        /// <summary>
        /// Set channel #1's frequency.
        /// </summary>
        /// <param name="GHz">New register value.</param>
        void SetFirstChanFreq_GHz(double GHz);

        /// <summary>
        /// Gets current laser freuqnecy.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserFrequency_GHz();

        /// <summary>
        /// Get current optical power.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserOutputPower_dBm();

        /// <summary>
        /// Get current temperature.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetCurrentTemperature_degC();

        /// <summary>
        /// Get minimum optical power.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetOpticalPowerMin_dBm();

        /// <summary>
        /// Get maximum optical power.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetOpticalPowerMax_dBm();

        /// <summary>
        /// Get laser's first frequency.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserFirstFreq_GHz();

        /// <summary>
        /// Get laser's last frequency.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetLaserLastFreq_GHz();

        /// <summary>
        /// Gets minimum frequency grid.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetMinFrequencyGrid_GHz();

        /// <summary>
        /// Gets dither enable state.
        /// </summary>
        /// <returns>Register value.</returns>
        DitherState GetDitherEnable();

        /// <summary>
        /// Sets dither enable state.
        /// </summary>
        /// <param name="state">New register value.</param>
        void SetDitherEnable(DitherState state);

        /// <summary>
        /// Get dither R setting.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetDitherR_KHz();

        /// <summary>
        /// Set dither R setting.
        /// </summary>
        /// <param name="KHz">New register value.</param>
        void SetDitherR_KHz(double KHz);

        /// <summary>
        /// Get dither F setting.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetDitherF_GHz();

        /// <summary>
        /// Set dither R setting.
        /// </summary>
        /// <param name="GHz">New register value.</param>
        void SetDitherF_GHz(double GHz);

        /// <summary>
        /// Get dither a setting.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetDitherA_percent();

        /// <summary>
        /// Set dither A setting.
        /// </summary>
        /// <param name="percent">New register value.</param>
        void SetDitherA_percent(double percent);

        /// <summary>
        /// Get T Case low value.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetTCaseL_degC();

        /// <summary>
        /// Set T Case low value.
        /// </summary>
        /// <param name="degC">New register value.</param>
        void SetTCaseL_degC(double degC);

        /// <summary>
        /// Get T Case high value.
        /// </summary>
        /// <returns>Register value.</returns>
        double GetTCaseH_degC();

        /// <summary>
        /// Set T Case high value.
        /// </summary>
        /// <param name="degC">New register value.</param>
        void SetTCaseH_degC(double degC);

        /// <summary>
        /// Get Fatal age threshold.
        /// </summary>
        /// <returns>Register value.</returns>
        int GetFAgeTh();
        
        /// <summary>
        /// Set Fatal age threshold.
        /// </summary>
        /// <param name="percent">New register value.</param>
        void SetFAgeTh(int percent);

        /// <summary>
        /// Get Warning age threshold.
        /// </summary>
        /// <returns>Register value.</returns>
        int GetWAgeTh();

        /// <summary>
        /// Set Warning age threshold.
        /// </summary>
        /// <param name="percent">New register value.</param>
        void SetWAgeTh(int percent);
    }
}
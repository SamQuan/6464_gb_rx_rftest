// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// TunableData.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Data class for SetTxCommand and ReadTxCommand.
    /// </summary>
    public class TxCommand
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly byte Data1;
        /// <summary>
        /// 
        /// </summary>
        public readonly byte Data2;
        /// <summary>
        /// 
        /// </summary>
        public readonly byte Data3;

        /// <summary>
        /// Construct TxCommand payload.
        /// </summary>
        /// <param name="data1">TxCommand data 1.</param>
        /// <param name="data2">TxCommand data 2.</param>
        /// <param name="data3">TxCommand data 3.</param>
        public TxCommand(byte data1, byte data2, byte data3)
        {
            this.Data1 = data1;
            this.Data2 = data2;
            this.Data3 = data3;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RxCommand : TxCommand
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <param name="d3"></param>
        public RxCommand(byte d1, byte d2, byte d3)
            : base(d1, d2, d3)
        {
        }
    }

    /// <summary>
    /// Enum for selecting a single ITU band.
    /// </summary>
    public enum ITUBand
    {
        /// <summary>ITU C Band.</summary>
        CBand,
        /// <summary>ITU L Band.</summary>
        LBand,
        /// <summary>ITU S Band.</summary>
        SBand
    }

    /// <summary>
    /// Class encapsulating an ITU Band and double channel.
    /// </summary>
    public class BandChannel
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly ITUBand Band;
        /// <summary>
        /// 
        /// </summary>
        public readonly double Chan;

        /// <summary>
        /// Construct container.
        /// </summary>
        /// <param name="band">Band id enum value.</param>
        /// <param name="chan">ITU channel.</param>
        public BandChannel(ITUBand band, double chan)
        {
            this.Band = band;
            this.Chan = chan;
        }
    }

    /// <summary>
    /// Adc values from MSA Vendor command 0xF7, subcmd 0x06.
    /// </summary>
    public enum AdcId
    {
        /// <summary>Rx Power Mon</summary>
        RxPowerMon = 1,
        /// <summary>APD Temp</summary>
        ApdTemp = 2,
        /// <summary>Rx DTV</summary>
        RxDtv = 3,
        /// <summary>+3V0 Ref</summary>
        Plus3V0Ref = 4,
        /// <summary>PCBA Temperature monitor</summary>
        PCBATemperatureMonitor = 5,
        /// <summary>DVR Mon</summary>
        DrvMon = 6,
        /// <summary>Rx Comp Mon</summary>
        RxCompMon = 7,
        /// <summary>Modulator AC Output</summary>
        ModulatorACOutput = 9,
        /// <summary>Modulator DC Output</summary>
        ModulatorDCOutput = 10,
        /// <summary>+1V8DTx monitor</summary>
        Plus1V8DTXMonitor = 11,
        /// <summary>-5.2DTx Monitor</summary>
        Minus5Point2DTXMonitor = 12,
        /// <summary>+3V3DRx monitor</summary>
        Plus3V3DRXMonitor = 13,
        /// <summary>+5V0ARx monitor</summary>
        Plus5V0ARXMonitor = 14,
        /// <summary>+1V8DRx Monitor</summary>
        Plus1V8DRXMonitor = 15,
        /// <summary>+3V3ARx Monitor</summary>
        Plus3V3ARXMonitor = 16,
        /// <summary>+3V3ATx Monitor</summary>
        Plus3V3ATXMonitor = 17,
        /// <summary>+5V0ATx Monitor</summary>
        Plus5V0ATXMonitor = 18
    }

    /// <summary>
    /// Enum for selecting and indicating dither enable state. 
    /// (See OIF standard, register 0x59)
    /// </summary>
    public enum DitherEnable
    {
        /// <summary>No dither.</summary>
        None,
        /// <summary>Dither is always zero.</summary>
        AlwaysZero,
        /// <summary>Digital dither.</summary>
        Digital
    }

    /// <summary>
    /// Enum for selecting and indicating waveform type.
    /// (See OIF standard, register 0x59)
    /// </summary>
    public enum Waveform
    {
        /// <summary>
        /// Sinusoidal waveform.
        /// </summary>
        Sinusoidal,

        /// <summary>
        /// Triangular waveform.
        /// </summary>
        Triangular
    }

    /// <summary>
    /// Class encapsulating DitherEnable and Waveworm enum values.
    /// (See OIF standard, register 0x59)
    /// </summary>
    public class DitherState
    {
        /// <summary>
        /// Gets enable state.
        /// </summary>
        public readonly DitherEnable Enable;

        /// <summary>
        /// Gets waveform state.
        /// </summary>
        public readonly Waveform Waveform;

        /// <summary>
        /// Construct class from enum values.
        /// </summary>
        /// <param name="enable">Enable state.</param>
        /// <param name="waveform">Waveform state.</param>
        public DitherState(DitherEnable enable, Waveform waveform)
        {
            this.Enable = enable;
            this.Waveform = waveform;
        }

        /// <summary>
        /// Construct class from bitmapped integer from OIF.
        /// </summary>
        /// <param name="fromInt">Integer value read from OIF.</param>
        public DitherState(int fromInt)
        {
            int de = (fromInt & 0x0003);    /* Extract bits 0 and 1 (DE) */
            int wf = (fromInt & 0x0030);    /* Extract bits 4 and 5 (WF) */

            switch (de)
            {
                case 0x0000: this.Enable = DitherEnable.None; break;
                case 0x0001: this.Enable = DitherEnable.AlwaysZero; break;
                case 0x0002: this.Enable = DitherEnable.Digital; break;
                default:
                    {
                        throw new TunableModuleException("Invalid DitherEnable value passed into DitherState constructor.", TunableProtocol.OIF);
                    }
            }

            switch (wf)
            {
                case 0x0000: this.Waveform = Waveform.Sinusoidal; break;
                case 0x0010: this.Waveform = Waveform.Triangular; break;
                default:
                    {
                        throw new TunableModuleException("Invalid Waveform value passed into DitherState constructor.", TunableProtocol.OIF);
                    }
            }
        }

        /// <summary>
        /// Generate bitmapped integer for OIF.
        /// </summary>
        /// <returns>Bitmapped integer.</returns>
        public int GetAsInt()
        {
            
            int de,wf;
            
            switch (Enable)
            {
                case DitherEnable.None:         de = 0x0000;    break;
                case DitherEnable.AlwaysZero:   de = 0x0001;    break;
                case DitherEnable.Digital:      de = 0x0002;    break;
                default:
                    {
                        throw new ChassisException("Unknown DitherEnable value " + Enable.ToString());
                    }
            }

            switch (Waveform)
            {
                case Waveform.Sinusoidal: wf = 0x0000; break;
                case Waveform.Triangular: wf = 0x0010; break;
                default:
                    {
                        throw new ChassisException("Unknown Waveform value " + Waveform.ToString());
                    }
            }

            return de | wf;
        }

    }

    /// <summary>
    /// Class supporting OIF status register and triggers.
    /// (See OIF Standard registers 0x20 and 0x21.)
    /// </summary>
    public class OifStatus
    {
        /// <summary>
        /// Value of OIF status register.
        /// </summary>
        private int status;

        /// <summary>
        /// Construct from OIF returned integer.
        /// </summary>
        /// <param name="status">Status integer from OIF.</param>
        public OifStatus(int status)
        {
            this.status = status;
        }

        /// <summary>
        /// Extract a single bit value from status.
        /// </summary>
        /// <param name="bitNum">Which bit to extract.</param>
        /// <returns>Value of indexed bit. True=1, False=0.</returns>
        private bool getBit(int bitNum)
        {
            return (this.status & (1 << bitNum)) != 0;
        }

        /// <summary>
        /// Set a single bit inside status.
        /// </summary>
        /// <param name="bitNum">Which bit to set.</param>
        /// <param name="val">New bit value.</param>
        private void setBit(int bitNum, bool val)
        {
            /* Calculate mask, a single 1 bit in the right position. */
            int mask = (1 << bitNum);

            /* Zero that bit, then OR with a 1 or a 0 in teh right place. */
            status = (status & ~mask) | (val ? mask : 0x0000);
        }

        /// <summary>Gets or sets value of the SRQ flag.</summary>
        public bool SRQ     { get { return getBit(15); }    set { setBit(15, value); }      }
        /// <summary>Gets or sets value of the ALM flag.</summary>
        public bool ALM { get { return getBit(14); } set { setBit(14, value); } }
        /// <summary>Gets or sets value of the FATAL flag.</summary>
        public bool FATAL { get { return getBit(13); } set { setBit(13, value); } }
        /// <summary>Gets or sets value of the DIS flag.</summary>
        public bool DIS { get { return getBit(12); } set { setBit(12, value); } }
        /// <summary>Gets or sets value of the VSF flag.</summary>
        public bool VSF { get { return getBit(11); } set { setBit(11, value); } }
        /// <summary>Gets or sets value of the FREQ flag.</summary>
        public bool FREQ { get { return getBit(10); } set { setBit(10, value); } }
        /// <summary>Gets or sets value of the THERM flag.</summary>
        public bool THERM { get { return getBit(9); } set { setBit(9, value); } }
        /// <summary>Gets or sets value of the PWR flag.</summary>
        public bool PWR { get { return getBit(8); } set { setBit(8, value); } }
        /// <summary>Gets or sets value of the XEL flag.</summary>
        public bool XEL { get { return getBit(7); } set { setBit(7, value); } }
        /// <summary>Gets or sets value of the CEL flag.</summary>
        public bool CEL { get { return getBit(6); } set { setBit(6, value); } }
        /// <summary>Gets or sets value of the MRL flag.</summary>
        public bool MRL { get { return getBit(5); } set { setBit(5, value); } }
        /// <summary>Gets or sets value of the CRL flag.</summary>
        public bool CRL { get { return getBit(4); } set { setBit(4, value); } }
        /// <summary>Gets or sets value of the VSFL flag.</summary>
        public bool VSFL { get { return getBit(3); } set { setBit(3, value); } }
        /// <summary>Gets or sets value of the FREQL flag.</summary>
        public bool FREQL { get { return getBit(2); } set { setBit(2, value); } }
        /// <summary>Gets or sets value of the THERML flag.</summary>
        public bool THERML { get { return getBit(1); } set { setBit(1, value); } }
        /// <summary>Gets or sets value of the PWRL flag.</summary>
        public bool PWRL { get { return getBit(0); } set { setBit(0, value); } }

        /// <summary>
        /// Returns flags as an OIF status register integer.
        /// </summary>
        /// <returns>OIF status register value.</returns>
        public int GetAsInt()
        {
            return status;
        }
    }

    /// <summary>
    /// Source numbers for the Get/SetIVSource functions. See OIF Vendor Registers 87-8A.
    /// </summary>
    public enum IVSourceIndexNumbers
    {
        /// <summary>
        /// 
        /// </summary>
        FrontOdd = 0,   
        /// <summary>
        /// 
        /// </summary>
        FrontEven = 1,
        /// <summary>
        /// 
        /// </summary>
        Rear = 2,
        /// <summary>
        /// 
        /// </summary>
        Phase = 3,
        /// <summary>
        /// 
        /// </summary>
        Gain = 4,
        /// <summary>
        /// 
        /// </summary>
        SOA = 16,
        /// <summary>
        /// 
        /// </summary>
        Left = 10,
        /// <summary>
        /// 
        /// </summary>
        Right = 11,
        /// <summary>
        /// 
        /// </summary>
        LeftImbalance = 14,
        /// <summary>
        /// 
        /// </summary>
        RightImbalance = 15
    }

    /// <summary>
    /// Enum indicating ModModGain level.
    /// </summary>
    public enum ModMonGain
    {
        /// <summary>High gain</summary>
        High,
        /// <summary>Low gain</summary>
        Low
    }

    /// <summary>
    /// Enum indicating MZ Loop state.
    /// </summary>
    public enum MZLoop
    {
        /// <summary>Open loop.</summary>
        Open,
        /// <summary>Closed loop.</summary>
        Closed
    }

    /// <summary>
    /// Enum indicating chirp level.
    /// </summary>
    public enum Chirp
    {
        /// <summary>+0.7 chirp.</summary>
        Positive,
        /// <summary>-0.7 chirp.</summary>
        Negative
    }

    /// <summary>
    /// Enum indicating source mode.
    /// </summary>
    public enum SrcMode
    {
        /// <summary>Current source.</summary>
        Current,
        /// <summary>Voltage source.</summary>
        Voltage
    }

}

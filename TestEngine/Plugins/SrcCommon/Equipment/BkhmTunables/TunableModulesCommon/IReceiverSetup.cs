using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Functions for Receivers on Tunable modules
    /// </summary>
    public interface IReceiverSetup
    {
        /// <summary>
        /// Enter Receiver Setup Mode
        /// </summary>
        void EnterRxSetupMode();

        /// <summary>
        /// Enter Protected Command Mode
        /// </summary>
        void EnterProtectMode();

        /// <summary>
        /// Exit protected Command mode
        /// </summary>
        void ExitProtectMode();

        /// <summary>
        /// Get/set Settling time for settings
        /// </summary>
        int SettlingTime_ms
        {
            get;
            set;
        }

        /// <summary>
        /// Get the current Rx Threshold (RxDTV).
        /// </summary>
        /// <returns>The Threshold in Percent.</returns>
        double ReadRxThreshold();

        
        /// <summary>
        /// Set the Rx Threshold (RxDTV).
        /// </summary>
        /// <param name="thresh_pc">The Threshold in Percent</param>
        void SetRxThreshold(double thresh_pc);

        /// <summary>
        /// Get M3 APD voltage bias DAC setting
        /// </summary>
        /// <returns>DAC Value</returns>
        int GetM3ApdBiasVoltageDac();

        /// <summary>
        /// Set M3 APD voltage bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetM3ApdBiasVoltageDac(int dac);

        /// <summary>
        /// Get M10 APD voltage bias DAC setting
        /// </summary>
        /// <returns>DAC Value</returns>
        int GetM10ApdBiasVoltageDac();

        /// <summary>
        /// Set M10 APD voltage bias DAC
        /// </summary>
        /// <param name="dac">DAC value</param>
        void SetM10ApdBiasVoltageDac(int dac);

        /// <summary>
        /// Set Index to position in lookup table to store raw optical power monitor reading with adjustment for Apd voltage. 
        /// </summary>
        /// <param name="calPoint">The index</param>
        void SetRxPowMonCalibrationPoint(byte calPoint);

        /// <summary>
        /// Get Rx Loss Of Signal (LOS) Alarm Threshold
        /// </summary>
        /// <returns>The Threshold setting</returns>
        int GetRxLOSAlarmThreshold();

        /// <summary>
        /// Set Rx Loss Of Signal (LOS) Alarm Threshold
        /// (CAL_SET_LOS_THRESHOLD)
        /// </summary>
        /// <returns>The Threshold setting</returns>
        void SetRxLOSAlarmThreshold(int threshold);


        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// </summary>
        /// <returns>The ADC Reading.</returns>
        int GetRxPowerMon_ADC();


        /// <summary>
        /// Get the Rx Power Monitor ADC Reading.
        /// This overload carries out an average reading function, 
        /// controlled by the average parameter.
        /// </summary>
        /// <param name="average">Number of ADC readings to average.</param>
        /// <returns>The ADC Reading.</returns>
        int GetRxPowerMon_ADC(int average);


        /// <summary>
        /// Get the state of the RxPOWALM (Loss DC power) alarm
        /// </summary>
        /// <returns>True if active</returns>
        bool GetRxPOWALMState();
        
        
    }
}

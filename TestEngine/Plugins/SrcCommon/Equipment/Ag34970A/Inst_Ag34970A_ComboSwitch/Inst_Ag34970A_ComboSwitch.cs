// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag34970A_ComboSwitch.cs
//
// Author: Brendan Kavanagh, 2006 
// Design: This instrument design was based on Inst_Ag34970A_Switch.cs by Paul Annetts.
//         This intrument acts like multiple Inst_Ag34970A_Switch instances in one unit.
//         See document Agilent_34970A_Driver_DD.doc (3).        

using System;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    ///  Agilent 34970A Data Acquisition Unit driver for combination-switching
    /// </summary>
    public class Inst_Ag34970A_ComboSwitch : InstType_SimpleSwitch
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Path to configuration file (NOT Slot ID)</param>
        /// <param name="subSlotId">Unused (NOT Sub Slot ID)</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag34970A_ComboSwitch(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information
            // Add Ag34970A internal Switch details
            InstrumentDataRecord ag34970A_Switch_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34970A",		// hardware name - chassis name only.
                "0",							// minimum valid firmware version 
                "9-1-2:1.0");							// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A_Combo", ag34970A_Switch_Data);// note "_Combo" suffix 

            // Configure valid chassis information
            // Add 34970A chassis details
            InstrumentDataRecord ag34970AChassisData = new InstrumentDataRecord(
                "Chassis_Ag34970A",								// chassis driver name  
                "0.0.0.0",										// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag34970A_Combo", ag34970AChassisData);// note "_Combo" suffix

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag34970A)base.InstrumentChassis;

            // Cannot easily check for a valid path {File.Exists() simply returns false 
            // rather than throwing}. However, we can check for these simple invalid conditions:
            if (slotId == null || slotId == "")
            {
                throw new InstrumentException("Null or empty 'slotId' string");
            }

            if (System.IO.File.Exists(slotId) == false) // "slotId" should be path to a file
            {
                throw new InstrumentException("File not found: " + slotId);
            }

            // The 34970A channel numbers are obtained from a configuration file.

            // Parsing of the configuration file can result in various exceptions so
            // convert them to an InstrumentException containing the original exception
            // as the 'inner' exception.
            try
            {
                // Create a cache of the mapped "position" integer (argument of property 
                // 'OutputSwitchPosition') to the combination switches (channel numbers) 
                // and their "on/off" values specified in the configuration file.
                this.mapper = new Inst_Ag34970A_ComboSwitchMapper(slotId);
            }
            catch (InstrumentException e)
            {
                throw new InstrumentException
                    ("Bad configuration file data: ", e);
            }
        }

        #region Instrument Overrides
        /// <summary>
        /// Firmware version of the Chassis (plugin not relevant). 
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the Chassis (plugin not relevant)
            get
            {
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware identity of chassis (plugin not relevant).
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware (plugin not relevant) 
            get
            {
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state (position '0' - for all switches "OFF").
        /// </summary>
        /// <remarks>
        /// - Position '0' should always be valid no matter what the config file settings.
        /// It results in the multiple switch settings all being set "OFF" for the GPIB ports.
        /// </remarks>
        public override void SetDefaultState()
        {
            this.SwitchState = 0; // Message the chassis to write to each channel
        }

        /// <summary>
        /// Set instrument on/offline. When setting online, comms to the instrument is tested.  
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
            }
        }
        #endregion

        #region Switch functions

        /// <summary>
        /// Output minimum position: always 0
        /// </summary>
        public override int MinimumSwitchState
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Output maximum position: 'N'
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                // support 1xN switches in the 34903A chassis 
                return this.mapper.MaximumPosition;
            }
        }

        /// <summary>
        /// Get / Set output switch position: 0 = Default, 1 -> N selects one combination
        /// for each value.
        /// </summary>
        public override int SwitchState
        {
            get
            {
                return this.ReadOutputSwitchPosition();
            }
            set
            {
                this.GenerateOutputSwitchPosition(value);
            }
        }
        #endregion

        /// <summary>
        /// Method ('internal' for development testing) to retrieve the user-selected position 
        /// integer value as per "Position" in the configuration XML file. If on-line the
        /// hardware is read to provide the combination switches. If off-line then the
        /// default setting should be returned.
        /// </summary>
        /// <returns>"Position" integer value</returns>
        internal int ReadOutputSwitchPosition()
        {
            // For each channel in config file, need to read the switch state, then
            // work out the "Position".

            int[] channelNumbers = this.mapper.SwitchNumbers;

            if (channelNumbers == null)
                throw new InstrumentException("Channels not mapped.");

            bool[] switchStates = new bool[channelNumbers.Length];
            int channelCount = 0;

            // A dictionary to hold the channel-state pairs:
            System.Collections.Generic.Dictionary<int, bool> switchSet = new Dictionary<int, bool>(8);

            foreach (int chassisChannel in channelNumbers)
            {
                // Is the switch closed?
                string command = String.Format("ROUT:CLOS? (@{0})", chassisChannel);

                string resp = "0";

                resp = GetCmd(command); // send message to instrument

                // What does the response mean?
                if (resp == "0")
                    switchStates[channelCount] = false; // OPEN
                else if (resp == "1")
                    switchStates[channelCount] = true; // CLOSED
                else
                    throw new InstrumentException("Bad OutputSwitchPosition response: " + resp);

                switchSet.Add(chassisChannel, switchStates[channelCount]); // add to dictionary
                channelCount++;
            }

            // OK, now need to convert the switch states back to a "Position":

            int position = this.mapper.GetPositionForSetOfSwitchStates(switchSet);

            // Generate an Instrument log entry for a "read" (actually the result of several reads) to the instrument:
            base.LogEvent("Read comboSwitch position of " + position.ToString());

            if (position == -1) // the error value returned?
            {
                throw new InstrumentException("No position integer found for switch states.");
            }

            return position;
        }

        /// <summary>
        /// Method ('internal' for development testing) to generate the simple switch 
        /// integer value for a given user-selected position as per "Position" in the 
        /// configuration XML file.
        /// </summary>
        /// <param name="userPosition">caller-selected position integer</param>
        internal void GenerateOutputSwitchPosition(int userPosition)
        {
            string command;
            // generate the appropriate command based on what we're being asked to do.

            if (userPosition > this.mapper.MaximumPosition)
            {
                throw new InstrumentException("Maximum selector 'N' exceeded: " + userPosition.ToString() + " v " + this.mapper.MaximumPosition.ToString());
            }

            int[] channels; // chassisChannel numbers
            bool[] switchStates; // on/off states of switches

            // Map 'userPosition' to set of channels and states for those channels:
            int selCount = this.mapper.RequestSelectors(userPosition, out channels, out switchStates);

            for (int channelCount = 0; channelCount < selCount; channelCount++)
            {
                int chassisChannel = channels[channelCount]; // get current chassis channel to be messaged.
                bool switchValue = switchStates[channelCount]; // get state for channel

                if (switchValue == true)
                {
                    command = String.Format("ROUT:CLOS (@{0})", chassisChannel); // close it.
                }
                else
                {
                    command = String.Format("ROUT:OPEN (@{0})", chassisChannel); // open it.
                }

                SetCmd(command); // in normal operation send message to instrument

            } // end of 'for' loop

            // Generate an Instrument log entry for a write to the instrument:
            base.LogEvent("Set comboSwitch position to " + userPosition.ToString() + " (Maximum possible = "
                + this.mapper.MaximumPosition.ToString() + ").");
        }

        #region Private data and helper functions
        /// <summary>
        /// "Set" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void SetCmd(string command)
        {
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// "Get" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string GetCmd(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// Instrument chassis object
        /// </summary>
        private Chassis_Ag34970A instrumentChassis;

        /// <summary>
        /// Cache for channels/ports mapping data extracted from XML file.
        /// </summary>
        private Inst_Ag34970A_ComboSwitchMapper mapper;

        #endregion
    }
}

 
// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag34970A_ComboSwitchMapper.cs
//
// Author: Brendan Kavanagh, 2006 
// Design: Agilent_34970A_Driver_DD.doc (3)

using System;
using System.Collections; // ICollection  
using System.Collections.Generic;
using System.Collections.Specialized; // StringDictionary
using System.Text;
using Bookham.TestEngine.Config; // ConfigDataAccessor
using Bookham.TestEngine.Framework.InternalData; // DatumList 
using Bookham.TestEngine.PluginInterfaces.Instrument; // InstrumentException 

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Mapper class to convert from a table in an XML configuration file to a 
    /// set of data usable by the Inst_Ag34970A_ComboSwitch instrument plug-in.
    /// </summary>
    internal class Inst_Ag34970A_ComboSwitchMapper
    {
        /// <summary>
        /// Constructor. Reads configuration file and sets up a cache of the mapping.
        /// </summary>
        /// <param name="totalFilePath">file path of XML configuration file</param>
        internal Inst_Ag34970A_ComboSwitchMapper(string totalFilePath)
        {
            // read config
            ConfigDataAccessor matrixConfigAccess = new ConfigDataAccessor(totalFilePath, "SwitchMatrix");

            // Find the number of positions (rows) in the file, 
            // Find the number of columns, then copy each row into a collection cache.

            // The collection cache needs to be accessable via the 'position' integer.
            // The cache has a integer key and variable-sized array fixed after construction.

            StringDictionary columnData = matrixConfigAccess.GetDataDetails(); // Get column details

            ICollection keySet = columnData.Keys;

            if (keySet.Count < 2) // sanity check
            {
                throw new InstrumentException("Key/column count < 2 in mapper xml file: " + totalFilePath);
            }

            this.switchNumbers = new int[keySet.Count - 1]; // exclude the 'position' key
            int switchNumbersCount = 0;

            bool positionKeyFound = false;

            foreach (string item in keySet)
            {
                // Look for primary key "Position":
                if (item.Equals(Inst_Ag34970A_ComboSwitchMapper.PositionKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    positionKeyFound = true;
                }
                else
                {   // Copy each key-name's number part into array - e.g. '304' if key is 'S304'

                    try // But have we a valid integer from the file?
                    {
                        string currentSwitchName = item.Substring(Inst_Ag34970A_ComboSwitchMapper.StartOfNumberPart);
                        switchNumbers[switchNumbersCount++] = Int32.Parse(currentSwitchName);
                    }
                    catch (Exception ex)
                    {
                        throw new InstrumentException("Bad integer for switch in config file: " + totalFilePath + " : ", ex);
                    }
                }
            }

            if (positionKeyFound != true)
            {
                throw new InstrumentException("SWITCH_COMBO" + ": 'P/position' key not found in xml file");
            }

            bool endOfRowsFound = false;
            int rowCount = 0;

            // Initialise the cached collection - an "array" of channel-state pairs indexed by "position":
            this.arraySwitchSets = new Dictionary<int, Dictionary<int, bool>>(9); // initial capacity chosen = 2**3 + 1 

            // We don't know in advance how many rows we might have tho' there shud be at
            // least one.

            bool atLeastOneRow = false;

            while (endOfRowsFound == false)
            {
                // The "primary" key is the Position key integer:
                StringDictionary primaryKey = new StringDictionary();
                primaryKey.Add(Inst_Ag34970A_ComboSwitchMapper.PositionKey, rowCount.ToString());

                // Check that there's a unique row matching the primary key and extract it:

                int numberOfMatchingRows = matrixConfigAccess.MatchingRowCount(primaryKey, true); // true => don't look at other keys

                if (numberOfMatchingRows == 1) // OK, found a row, extract the data
                {
                    DatumList list = matrixConfigAccess.GetData(primaryKey, false);

                    // Create a key-value pair
                    Dictionary<int, bool> switchSet = new Dictionary<int, bool>(8); // initial capacity 8
                    // Get the position integer
                    int positionNumber = (int)(list.ReadUint32(Inst_Ag34970A_ComboSwitchMapper.PositionKeyCapitalised));

                    foreach (Datum dtm in list)
                    {
                        if (dtm.Name.Equals(Inst_Ag34970A_ComboSwitchMapper.PositionKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue; // skip over the 'Position' column as only care about selectors here
                        }

                        if (!(dtm is DatumBool))
                        {
                            throw new InstrumentException("Non bool type from config file: DtmType =" + dtm.Type.ToString());
                        }

                        DatumBool switchInstance = (DatumBool)dtm;
                        string switchNumberString = switchInstance.Name.Substring(Inst_Ag34970A_ComboSwitchMapper.StartOfNumberPart);
                        int switchNumber = 0;

                        try
                        {
                            switchNumber = System.Int32.Parse(switchNumberString);
                        }
                        catch (Exception ex2)
                        {
                            throw new InstrumentException("Parse error on switchSelector int: ", ex2);
                        }

                        switchSet.Add(switchNumber, switchInstance.Value);

                    } // end of foreach

                    this.arraySwitchSets.Add(positionNumber, switchSet); // add to "array"

                    atLeastOneRow = true;
                }
                else // unique row not found
                {
                    if (numberOfMatchingRows != 0)
                    {
                        throw new InstrumentException("Position key not unique: " + rowCount.ToString());
                    }
                    else
                    {
                        endOfRowsFound = true;
                    }
                }

                rowCount++; // expect contiguous numbering in 'position' key.
            }

            if (atLeastOneRow == false)
            {
                throw new InstrumentException("No Position = '0' matching row found in config file: " + totalFilePath);
            }

            rowCount--; // get back to last match.

            this.MaximumPosition = rowCount - 1; // if 3 positions "0" - "2" then max posn is "2" for example. 

        } // end of constructor


        /// <summary>
        /// Method to request the selector channel numbers and corresponding boolean values 
        /// for a given "Position" integer.
        /// <para>
        /// - There is guaranteed only to be a minimum of a single row at position '0'. 
        /// Variable number of rows are expected.
        /// </para>
        /// </summary>
        /// <param name="position">the position (primary key 'Position' in the xml file)</param>
        /// <param name="channelInts">output array of selector channel numbers</param>
        /// <param name="onOffStates">output array of selector boolean switch states</param>
        /// <returns>the number of selectors</returns>
        internal int RequestSelectors(int position, out int[] channelInts, out bool[] onOffStates)
        {
            // Extract row of combination switches for the position:
            Dictionary<int, bool> switchSet = this.arraySwitchSets[position];
            int pairCount = switchSet.Count;
            int count = 0;

            int[] channelsArray = new int[pairCount];
            bool[] statesArray = new bool[pairCount];

            foreach (KeyValuePair<int, bool> pair in switchSet)
            {
                channelsArray[count] = pair.Key;
                statesArray[count] = pair.Value;
                count++;
            }

            channelInts = channelsArray;
            onOffStates = statesArray;

            return pairCount;
        }

        /// <summary>
        /// Method to get the "Position" integer which maps to a set of combination
        /// switches. They should map to a unique "Position" integer value except
        /// possibly where one position duplicates the settings of the default
        /// position (zero). In the case of this duplication the non-default 
        /// position is returned.
        /// </summary>
        /// <param name="switchSet">dictionary of channel-state pairs</param>
        /// <returns>position integer, -1 if no match found</returns>
        internal int GetPositionForSetOfSwitchStates(Dictionary<int, bool> switchSet)
        {
            int rowCount;
            bool matchFound = true;

            // Count down from maximum position integer so that if one duplicates
            // the set of values of the Zeroth position (the default), the duplicate
            // is returned rather than the Zeroth one.
            for (rowCount = (this.arraySwitchSets.Count - 1); rowCount >= 0; rowCount--)
            {
                matchFound = true;
                Dictionary<int, bool> currentSwitchSet = this.arraySwitchSets[rowCount];

                // compare currentSwitchSet with switchSet:

                foreach (KeyValuePair<int, bool> pair in currentSwitchSet)
                {
                    if (pair.Value != switchSet[pair.Key]) // if any states don't match
                        matchFound = false;             // set the flag to false.
                }

                if (matchFound == true)
                {
                    break; // exit 'for' loop without decrementing 'rowCount' last time
                }

            } // end of 'for' loop

            if (matchFound == false)
            {
                return -1; // for the error case.
            }

            return rowCount; // this corresponds to "Position" integer in the XML file.
        }

        /// <summary>
        /// 'Get' accessor for channel numbers (switch keys)(note it excludes "Position").
        /// The alpha prefix which was in the configuration file has also been removed.
        /// </summary>
        internal int[] SwitchNumbers
        {
            get
            {
                return switchNumbers;
            }
        }

        /// <summary>
        /// An "array" of channel-state key-value pairs. The "array" has an integer key
        /// of the 'position' identifier.
        /// </summary>
        private Dictionary<int, Dictionary<int, bool>> arraySwitchSets = null;

        /// <summary>
        /// Array of channel numbers (or "switch keys")
        /// </summary>
        private int[] switchNumbers = null;

        /// <summary>
        /// The names come out as lower case when using the method ConfigDataAccessor.GetDataDetails().
        /// </summary>
        public const string PositionKey = "position"; // The names come out as lower case.

        /// <summary>
        /// Position key with capitalised first letter.
        /// </summary>
        public const string PositionKeyCapitalised = "Position";

        /// <summary>
        /// The selector names in the config xml file have a single letter prefix. Need to strip
        /// this off. So S302, s303 become 302, 303. This constant is that offset of +1.
        /// </summary>
        private const int StartOfNumberPart = 1;

        /// <summary>
        /// The maximum position where the numbering starts from zero.
        /// </summary>
        public readonly int MaximumPosition;
    }
}
  
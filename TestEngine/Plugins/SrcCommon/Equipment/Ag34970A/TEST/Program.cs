using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Ag34970A_Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Ag34970_Test test = new Ag34970_Test();
            test.Setup();
            DateTime start = DateTime.Now;
            try
            {
                test.T01_Versions();
                test.T02_ChassisFunction();
                test.T03_DMM_Modes();
                test.T04_DMM_Ranges();
                test.T05_DMM_Read();
                test.T06_DMM_IntegrationTimes();
                test.T07_DMM_Defaults();
                test.T08_ChassisSwitch();
                test.T09_SwitchObjects();
            }
            finally
            {
                test.ShutDown();
            }
            TimeSpan t = DateTime.Now - start;
            Console.WriteLine("Took {0}", t);
            Console.WriteLine("Press RETURN to exit");
            Console.Read();
            Environment.Exit(0);
        }
    }
}

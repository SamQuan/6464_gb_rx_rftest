using System;
using System.Collections.Generic;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;

namespace Ag34970A_Tester
{
    /// <exclude />	
    [TestFixture]
    public class Ag34970_Test
    {
        #region Constants
        //const string visaResource = "//toaeng-305/GPIB1::9::INSTR";
        const string visaResource = "//pai-tx-labj1/GPIB0::9::INSTR";
        const string dmmSlot = "3";
        const string dmmChan1 = "1";
        const string dmmChan2 = "21";
        const string chassisName = "Chassis";
        const string dmm1Name = "DMM 1";
        const string dmm2Name = "DMM 2";
        const string sw1Name = "Switch 1";
        const string sw2Name = "Switch 2";

        const string switchSlot = "1";
        const string swCh1 = "1";
        const string swCh2 = "7";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Agilent 34970A Driver Test Initialising ***");

            // create equipment objects
            testChassis = new Chassis_Ag34970A(chassisName, "Chassis_Ag34970A", visaResource);
            TestOutput(chassisName, "Created OK");

            testDMM_1 = new Inst_Ag34970A_DMM(dmm1Name, "Inst_Ag34970A_DMM", dmmSlot, dmmChan1, testChassis);
            TestOutput(dmm1Name, "Created OK");
            testDMM_2 = new Inst_Ag34970A_DMM(dmm2Name, "Inst_Ag34970A_DMM", dmmSlot, dmmChan2, testChassis);
            TestOutput(dmm2Name, "Created OK");
            testSw1 = new Inst_Ag34970A_Switch(sw1Name, "Inst_Ag34970A_Switch", switchSlot, swCh1, testChassis);
            TestOutput(sw1Name, "Created OK");
            testSw2 = new Inst_Ag34970A_Switch(sw2Name, "Inst_Ag34970A_Switch", switchSlot, swCh2, testChassis);
            TestOutput(sw2Name, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testDMM_1.IsOnline = true;
            TestOutput(dmm1Name, "IsOnline set true OK");
            testDMM_2.IsOnline = true;
            TestOutput(dmm2Name, "IsOnline set true OK");
            testSw1.IsOnline = true;
            TestOutput(sw1Name, "IsOnline set true OK");
            testSw2.IsOnline = true;
            TestOutput(sw2Name, "IsOnline set true OK");

        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_Versions()
        {
            TestOutput("\n\n*** T01_Versions ***");
            TestOutput(chassisName, "Driver=" + testChassis.DriverVersion);
            TestOutput(chassisName, "Firmware=" + testChassis.FirmwareVersion);
            TestOutput(chassisName, "HW=" + testChassis.HardwareIdentity);

            TestOutput(dmm1Name, "Driver=" + testDMM_1.DriverVersion);
            TestOutput(dmm1Name, "Firmware=" + testDMM_1.FirmwareVersion);
            TestOutput(dmm1Name, "HW=" + testDMM_1.HardwareIdentity);

            TestOutput(dmm2Name, "Driver=" + testDMM_2.DriverVersion);
            TestOutput(dmm2Name, "Firmware=" + testDMM_2.FirmwareVersion);
            TestOutput(dmm2Name, "HW=" + testDMM_2.HardwareIdentity);

            TestOutput(sw1Name, "Driver=" + testSw1.DriverVersion);
            TestOutput(sw1Name, "Firmware=" + testSw1.FirmwareVersion);
            TestOutput(sw1Name, "HW=" + testSw1.HardwareIdentity);

            TestOutput(sw2Name, "Driver=" + testSw2.DriverVersion);
            TestOutput(sw2Name, "Firmware=" + testSw2.FirmwareVersion);
            TestOutput(sw2Name, "HW=" + testSw2.HardwareIdentity);
        }

        [Test]
        public void T02_ChassisFunction()
        {
            TestOutput("\n\n*** T02_ChassisFunction ***");
            TestOutput(chassisName, "Slot1=" + testChassis.GetSlotID(1));
            TestOutput(chassisName, "Slot2=" + testChassis.GetSlotID(2));
            TestOutput(chassisName, "Slot3=" + testChassis.GetSlotID(3));
        }

        [Test]
        public void T03_DMM_Modes()
        {
            TestOutput("\n\n*** T03_DMM_Modes ***");
            TestOutput("Checking AC Mode on DMMs");
            Assert.AreEqual(InstType_MultiMeter.MeterAcType.True_RMS, testDMM_1.AcType);
            Assert.AreEqual(InstType_MultiMeter.MeterAcType.True_RMS, testDMM_2.AcType);

            // Test DMM #1
            TestOutput(dmm1Name, "Testing mode changes - all but current modes");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Voltage_DC_V, testDMM_1.MultiMeterMode);

            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_AC_V_rms;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Voltage_AC_V_rms, testDMM_1.MultiMeterMode);

            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance2wire_ohms;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Resistance2wire_ohms, testDMM_1.MultiMeterMode);

            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance4wire_ohms;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Resistance4wire_ohms, testDMM_1.MultiMeterMode);

            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Voltage_DC_V, testDMM_1.MultiMeterMode);

            // Test DMM #2
            TestOutput(dmm2Name, "Testing mode changes - current modes only");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Current_DC_A, testDMM_2.MultiMeterMode);

            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_AC_A_rms;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Current_AC_A_rms, testDMM_2.MultiMeterMode);

            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            Assert.AreEqual(InstType_MultiMeter.MeterMode.Current_DC_A, testDMM_2.MultiMeterMode);
        }

        [Test]
        public void T04_DMM_Ranges()
        {
            TestOutput("\n\n*** T04_DMM_Ranges ***");
            TestOutput("Checking present ranges");
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            TestOutput(dmm1Name, testDMM_2.MeterRange.ToString());

            TestOutput("Set to autorange");
            testDMM_1.MeterRange = InstType_MultiMeter.AutoRange;
            Assert.AreEqual(InstType_MultiMeter.AutoRange, testDMM_1.MeterRange);
            testDMM_2.MeterRange = InstType_MultiMeter.AutoRange;
            Assert.AreEqual(InstType_MultiMeter.AutoRange, testDMM_2.MeterRange);

            TestOutput("Check Voltage DC range");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            testDMM_1.MeterRange = 0.045;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 0.25;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 2.4;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 22;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 244;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());

            TestOutput("Check Voltage AC range");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_AC_V_rms;
            testDMM_1.MeterRange = 0.045;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 0.32;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 4.5;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 26;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 259;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());

            TestOutput("Check 2-wire Resistance range");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance2wire_ohms;
            testDMM_1.MeterRange = 40;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 503;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 3600;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 34500;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 298000;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 2984000;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 34329800;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());

            TestOutput("Check 4-wire Resistance range");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance4wire_ohms;
            testDMM_1.MeterRange = 40;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 503;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 3600;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 34500;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 298000;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 2984000;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            testDMM_1.MeterRange = 34329800;
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());

            TestOutput("Check Current DC range");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            testDMM_2.MeterRange = 0.0023;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());
            testDMM_2.MeterRange = 0.0134;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());
            testDMM_2.MeterRange = 0.325;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());

            TestOutput("Check Current AC range");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_AC_A_rms;
            testDMM_2.MeterRange = 0.0023;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());
            testDMM_2.MeterRange = 0.0134;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());
            testDMM_2.MeterRange = 0.325;
            TestOutput(dmm2Name, testDMM_2.MeterRange.ToString());

            TestOutput("Reset mode and range");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            testDMM_1.MeterRange = InstType_MultiMeter.AutoRange;
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            testDMM_2.MeterRange = InstType_MultiMeter.AutoRange;
        }

        [Test]
        public void T05_DMM_Read()
        {
            TestOutput("\n\n*** T05_DMM_Read ***");
            TestOutput("Read Volts DC");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());

            TestOutput("Read Volts AC");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_AC_V_rms;
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());

            TestOutput("Read 2-wire res");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance2wire_ohms;
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());

            TestOutput("Read 4-wire res");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance4wire_ohms;
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());
            TestOutput(dmm1Name, testDMM_1.GetReading().ToString());

            TestOutput("Read Current DC");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            TestOutput(dmm2Name, testDMM_2.GetReading().ToString());
            TestOutput(dmm2Name, testDMM_2.GetReading().ToString());

            TestOutput("Read Current AC");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_AC_A_rms;
            TestOutput(dmm2Name, testDMM_2.GetReading().ToString());
            TestOutput(dmm2Name, testDMM_2.GetReading().ToString());
        }

        [Test]
        public void T06_DMM_IntegrationTimes()
        {
            TestOutput("\n\n*** T06_DMM_IntegrationTimes ***");
            TestOutput("Volts DC");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());
            testDMM_1.IntegrationTime_s = testDMM_1.IntegrationTime_s * 10;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());

            TestOutput("NB: Integration time invalid in Volts AC!");

            TestOutput("2-wire Res");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance2wire_ohms;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());
            testDMM_1.IntegrationTime_s = testDMM_1.IntegrationTime_s * 10;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());

            TestOutput("4-wire Res");
            testDMM_1.MultiMeterMode = InstType_MultiMeter.MeterMode.Resistance4wire_ohms;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());
            testDMM_1.IntegrationTime_s = testDMM_1.IntegrationTime_s * 10;
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());

            TestOutput("Current DC");
            testDMM_2.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            TestOutput(dmm2Name, testDMM_2.IntegrationTime_s.ToString());
            testDMM_2.IntegrationTime_s = testDMM_2.IntegrationTime_s * 10;
            TestOutput(dmm2Name, testDMM_2.IntegrationTime_s.ToString());

            TestOutput("NB: Integration time invalid in Current AC!");
        }

        [Test]
        public void T07_DMM_Defaults()
        {
            TestOutput("\n\n*** T07_DMM_Defaults ***");
            TestOutput(dmm1Name, "Resetting to default");
            testDMM_1.SetDefaultState();
            TestOutput(dmm1Name, testDMM_1.MultiMeterMode.ToString());
            TestOutput(dmm1Name, testDMM_1.MeterRange.ToString());
            TestOutput(dmm1Name, testDMM_1.IntegrationTime_s.ToString());

            TestOutput(dmm1Name, "Resetting to default");
            testDMM_2.SetDefaultState();
            TestOutput(dmm1Name, testDMM_2.MultiMeterMode.ToString());
            TestOutput(dmm1Name, testDMM_2.MeterRange.ToString());
            TestOutput(dmm1Name, testDMM_2.IntegrationTime_s.ToString());
        }

        [Test]
        public void T08_ChassisSwitch()
        {
            TestOutput("\n\n*** T08_ChassisSwitch ***");
            TestOutput("Initial position of switches");
            int switchSlot = Int32.Parse(Ag34970_Test.switchSlot);
            int swCh1 = Int32.Parse(Ag34970_Test.swCh1);
            int swCh2 = Int32.Parse(Ag34970_Test.swCh2);

            TestOutput(chassisName, testChassis.GetSwitchPosn(switchSlot, swCh1).ToString());
            TestOutput(chassisName, testChassis.GetSwitchPosn(switchSlot, swCh2).ToString());

            TestOutput("Exclusive close #1");
            testChassis.CloseSwitch(switchSlot, swCh1, true);
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh1));
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Open, testChassis.GetSwitchPosn(switchSlot, swCh2));

            TestOutput("Exclusive close #2");
            testChassis.CloseSwitch(switchSlot, swCh2, true);
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Open, testChassis.GetSwitchPosn(switchSlot, swCh1));
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh2));

            TestOutput("Close both switches");
            testChassis.CloseSwitch(switchSlot, swCh1, false);
            testChassis.CloseSwitch(switchSlot, swCh2, false);
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh1));
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh2));

            TestOutput("Open #1 only");
            testChassis.OpenSwitch(switchSlot, swCh1);
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Open, testChassis.GetSwitchPosn(switchSlot, swCh1));
            Assert.AreEqual(Chassis_Ag34970A.SwitchPosn.Closed, testChassis.GetSwitchPosn(switchSlot, swCh2));
        }

        [Test]
        public void T09_SwitchObjects()
        {
            TestOutput("\n\n*** T09_SwitchObjects ***");
            TestOutput("Testing switch setup");
            Assert.AreEqual(1, testSw1.MinimumSwitchState);
            Assert.AreEqual(2, testSw1.MaximumSwitchState);

            Assert.AreEqual(1, testSw2.MinimumSwitchState);
            Assert.AreEqual(2, testSw2.MaximumSwitchState);

            TestOutput("Current Positions");
            TestOutput(sw1Name, testSw1.SwitchState.ToString());
            TestOutput(sw2Name, testSw2.SwitchState.ToString());

            TestOutput("Toggle Switches");
            testSw1.SwitchState = 1;
            Assert.AreEqual(1, testSw1.SwitchState);
            testSw1.SwitchState = 2;
            Assert.AreEqual(2, testSw1.SwitchState);
            testSw1.SwitchState = 1;
            Assert.AreEqual(1, testSw1.SwitchState);

            testSw2.SwitchState = 1;
            Assert.AreEqual(1, testSw2.SwitchState);
            testSw2.SwitchState = 2;
            Assert.AreEqual(2, testSw2.SwitchState);
            testSw2.SwitchState = 1;
            Assert.AreEqual(1, testSw2.SwitchState);

            TestOutput("Default State");
            testSw1.SetDefaultState();
            testSw2.SetDefaultState();
            TestOutput(sw1Name, testSw1.SwitchState.ToString());
            TestOutput(sw2Name, testSw2.SwitchState.ToString());

        }

        #region Private helper fns and data
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }


        /// <summary>
        /// Chassis & Inst references
        /// </summary>
        private Chassis_Ag34970A testChassis;
        private Inst_Ag34970A_DMM testDMM_1;
        private Inst_Ag34970A_DMM testDMM_2;
        private Inst_Ag34970A_Switch testSw1;
        private Inst_Ag34970A_Switch testSw2;
        #endregion

    }

}
  
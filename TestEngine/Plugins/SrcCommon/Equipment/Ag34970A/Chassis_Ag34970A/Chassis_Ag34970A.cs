// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Ag34970A.cs
//
// Author: Paul Annetts, 2006
// Design: As specified in 34970A Driver DD 

using System;
using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.ChassisTypes;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Agilent 34970A Data Acquisition Unit Chassis driver
    /// </summary>
    public class Chassis_Ag34970A : ChassisType_Visa488_2
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="chassisName">Chassis name</param>
        /// <param name="driverName">Chassis driver name</param>
        /// <param name="resourceStringId">Resource data</param>
        public Chassis_Ag34970A(string chassisName, string driverName, string resourceStringId)
            : base(chassisName, driverName, resourceStringId)
        {
            // Configure valid hardware information
            // Add Ag34970A details
            ChassisDataRecord ag34970AData = new ChassisDataRecord(
                "HEWLETT-PACKARD 34970A",			// hardware name 
                "0",								// minimum valid firmware version 
                "9-1-2");					// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A", ag34970AData);

            // setup the slot contents arrays
            this.slotContents = new string[3];
            this.slotPluginFirmwareVersion = new string[3];
        }

        #region Chassis override functions
        /// <summary>
        /// Setup the chassis as soon as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                // if setting online
                if (value)
                {
                    // setup the standard event register mask 
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write("*CLS", null);

                    // find out what is in each slot                    
                    string slotStr;

                    slotStr = Query("SYST:CTYP? 100", null);
                    slotContents[0] = slotStr.Split(',')[1];
                    slotPluginFirmwareVersion[0] = slotStr.Split(',')[3];
                    slotStr = Query("SYST:CTYP? 200", null);
                    slotContents[1] = slotStr.Split(',')[1];
                    slotPluginFirmwareVersion[1] = slotStr.Split(',')[3];
                    slotStr = Query("SYST:CTYP? 300", null);
                    slotContents[2] = slotStr.Split(',')[1];
                    slotPluginFirmwareVersion[2] = slotStr.Split(',')[3];
                }
            }
        }


        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = this.Idn;

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware identity string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = this.Idn.Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Override of basic string to return the status of the questionable event register too.
        /// </summary>
        /// <returns>Error string</returns>
        public override string GetErrorString()
        {
            string resp = Query_Unchecked("STAT:QUES:EVEN?", null);
            string baseErrStr = base.GetErrorString();

            return String.Format("Questionable Data Register? {0}\n{1}", resp, baseErrStr);
        }
        #endregion

        #region DAU Chassis commands
        /// <summary>
        /// Gets the content of the slot specified
        /// </summary>
        /// <param name="slotNumber">Slot number</param>
        /// <returns>Name of model number of the plugin in place, or null string if empty slot</returns>
        public string GetSlotID(int slotNumber)
        {
            if ((slotNumber < 1) || (slotNumber > 3))
                throw new ChassisException("Invalid slot ID in 34970A: " + slotNumber.ToString());
            return slotContents[slotNumber - 1];
        }

        /// <summary>
        /// Gets the firmware version of the plugin in the slot specified
        /// </summary>
        /// <param name="slotNumber">Slot number</param>
        /// <returns>Firmware version string ("0" if no plugin present)</returns>
        public string GetSlotPluginFirmwareVersion(int slotNumber)
        {
            if ((slotNumber < 1) || (slotNumber > 3))
                throw new ChassisException("Invalid slot ID in 34970A: " + slotNumber.ToString());
            return slotPluginFirmwareVersion[slotNumber - 1];
        }

        #endregion

        #region DAU chassis switching
        /// <summary>
        /// Position of a switch channel in the DAU
        /// </summary>
        public enum SwitchPosn
        {
            /// <summary>
            /// Switch open
            /// </summary>
            Open = 0,
            /// <summary>
            /// Switch closed
            /// </summary>
            Closed = 1
        }

        /// <summary>
        /// Close the switch position stated
        /// </summary>
        /// <param name="slot">Slot in the DAU (1-3)</param>
        /// <param name="channel">Channel in the slot's plugin</param>
        /// <param name="exclusive">If true, then all other switches in this plugin module (e.g. 34903) are
        /// opened</param>
        public void CloseSwitch(int slot, int channel, bool exclusive)
        {
            string command;
            // calculate internal chassis number
            int chassisChannel = Get34970AChannelNumber(slot, channel);
            if (exclusive)
            {
                command = String.Format("ROUT:CLOS:EXCL (@{0})", chassisChannel);
            }
            else
            {
                command = String.Format("ROUT:CLOS (@{0})", chassisChannel);
            }

            Write(command, null);
        }

        /// <summary>
        /// Open the switch position stated
        /// </summary>
        /// <param name="slot">Slot in the DAU (1-3)</param>
        /// <param name="channel">Channel in the slot's plugin</param>
        public void OpenSwitch(int slot, int channel)
        {
            // calculate internal chassis number
            int chassisChannel = Get34970AChannelNumber(slot, channel);
            string command = String.Format("ROUT:OPEN (@{0})", chassisChannel);

            Write(command, null);
        }

        /// <summary>
        /// Get the position of switch stated
        /// </summary>
        /// <param name="slot">Slot in the DAU (1-3)</param>
        /// <param name="channel">Channel in the slot's plugin</param>        
        public SwitchPosn GetSwitchPosn(int slot, int channel)
        {
            int chassisChannel = (slot * 100) + channel;
            string command = String.Format("ROUT:OPEN? (@{0})", chassisChannel);
            string resp = Query(command, null);

            if (resp == "0") return SwitchPosn.Closed;
            if (resp == "1") return SwitchPosn.Open;
            else throw new ChassisException("Invalid return from '" + command + "' : " + resp);
        }

        /// <summary>
        /// Helper function
        /// </summary>
        /// <param name="slot">Slot number 1-3</param>
        /// <param name="channelInSlot">Channel number within the slot</param>
        /// <returns>Chassis channel number used for comms with this instrument</returns>
        public int Get34970AChannelNumber(int slot, int channelInSlot)
        {
            // range check the slot and channel number within the slot

            // (34970A has 3 slots)
            if ((slot < 1) || (slot > 3))
                throw new InstrumentException("Invalid 34970A slot Id: " + slot.ToString());

            // Each plugin module has up to 40 channels
            if ((channelInSlot < 1) || (channelInSlot > 40))
                throw new InstrumentException("Invalid 34970A channel number within a slot: " +
                    channelInSlot.ToString());

            // Chassis Channel number is slot number * 100 plus the channel number
            return ((slot * 100) + channelInSlot);
        }

        #endregion

        #region Plugin management
        /// <summary>
        /// Array containing the name of each of the slot contents in the 34970A.
        /// If slot is empty, element contains a blank string.
        /// This array is zero-based.
        /// </summary>
        string[] slotContents;

        /// <summary>
        /// Array containing the firmware versions of the slot modules in the 34970A.
        /// If slot is empty, element contains "0".
        /// This array is zero-based.
        /// </summary>
        string[] slotPluginFirmwareVersion;
        #endregion
    }
}
  
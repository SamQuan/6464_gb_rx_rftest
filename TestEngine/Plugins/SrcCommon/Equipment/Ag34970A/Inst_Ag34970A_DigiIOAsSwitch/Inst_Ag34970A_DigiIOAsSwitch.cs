// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag34970A_DigiIOAsSwitch.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Instrument driver for Ag34970A DAU's 34903A 20-Channel Actuator / General-Purpose Switch Card,
    /// modelled as 20 general purpose Digital IO lines. Using IInstType_DigiIOCollection to allow a single 
    /// entry in the Test Engine's equipment configuration file.
    /// </summary>
    public class Inst_Ag34970A_DigiIOAsSwitch : Instrument, IInstType_DigiIOCollection
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag34970A_DigiIOAsSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // Add Ag34970A internal Switch details
            InstrumentDataRecord ag34970A_DigiIOAsSwitch_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34970A",		// hardware name - chassis name only.
                "0",							// minimum valid firmware version 
                "13-2-2");							// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A_DigiIOAsSwitch", ag34970A_DigiIOAsSwitch_Data);// note "DigiIOAsSwitch" suffix 

            InstrumentDataRecord Ag34970A_DigiIOAsSwitch_Data2 = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34970A",		// hardware name - chassis name only.
                "0",							// minimum valid firmware version 
                "9-1-2");							// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A_DigiIOAsSwitch2", Ag34970A_DigiIOAsSwitch_Data2);// note "DigiIOAsSwitch" suffix 


            // Configure valid chassis driver information
            // Add 34970A chassis details
            InstrumentDataRecord ag34970AChassisData = new InstrumentDataRecord(
                "Chassis_Ag34970A",								// chassis driver name  
                "0.0.0.0",										// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag34970A_DigiIOAsSwitch", ag34970AChassisData);// note "_Combo" suffix

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag34970A)base.InstrumentChassis;

            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(20);
            int lineNumber = 0;
            for (int ii = 1; ii <= 20; ii++)
            {
                lineNumber = Convert.ToInt32(slotInit) * 100 + ii;
                digiOutLines.Add(new Inst_Ag34970A_DigiOutLine(instrumentNameInit + "_D" + ii, instrumentChassis, lineNumber));
            }
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {

                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {

                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                foreach (IInstType_DigitalIO dio in digiOutLines)
                {
                    dio.IsOnline = value;
                }
            }
        }
        #endregion

        #region IInstType_DigiIOCollection Members
        /// <summary>
        /// Get a Digital IO Line by line number
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
            return digiOutLines[lineNumber - 1];
        }
        /// <summary>
        /// Get enumerator for Digital IO Line
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get
            {
                return digiOutLines;
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag34970A instrumentChassis;

        /// <summary>
        /// List of Digital IO Lines
        /// </summary>
        internal List<IInstType_DigitalIO> digiOutLines;
        #endregion
    }
}
  

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag34970A_DigiOutLine.cs
//
// Author: Joseph Olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Ag34970A DAU Virtual Digital IO line instrument
    /// </summary>
    public class Inst_Ag34970A_DigiOutLine : UnmanagedInstrument, IInstType_DigitalIO
    {
        internal Inst_Ag34970A_DigiOutLine(string linename, Chassis_Ag34970A ag34970A, int lineNbr)
            : base(linename, ag34970A)
        {
            // remember the chassis
            this.instrumentChassis = ag34970A;
            // digital line number
            this.lineNbr = lineNbr;
        }

        #region Private data
        /// <summary>
        /// Underlying instrument
        /// </summary>
        private Chassis_Ag34970A instrumentChassis;

        /// <summary>
        /// Digital output line number 
        /// </summary>
        private int lineNbr;
        #endregion

        /// <summary>
        /// Line state
        /// </summary>
        public bool LineState
        {
            get
            {
                int channel = lineNbr;
                // Is the switch closed?
                string resp = "0";
                string command = "ROUT:CLOS? (@" + channel + ")";

                //Query the instrument
                resp = this.instrumentChassis.Query(command, this);

                // What does the response mean?
                bool lineState;
                if (resp == "0")
                {
                    lineState = false; // OPEN
                }
                else if (resp == "1")
                {
                    lineState = true; // CLOSED
                }
                else
                {
                    throw new InstrumentException("Bad OutputSwitchPosition response: " + resp);
                }
                return lineState;
            }
            set
            {
                int channel = lineNbr;
                string command;

                if (value)
                {
                    //Close the Switch (i.e. enable output)
                    command = "ROUT:CLOS (@" + channel + ")";
                }
                else
                {
                    //Open the switch (i.e. disable output)
                    command = "ROUT:OPEN (@" + channel + ")";
                }
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Default State
        /// </summary>
        public override void SetDefaultState()
        {
            LineState = false;
        }
    }
}

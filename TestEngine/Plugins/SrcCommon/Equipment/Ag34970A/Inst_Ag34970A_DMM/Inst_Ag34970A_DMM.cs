// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag34970A_DMM.cs
//
// Author: Paul Annetts, 2006
// Design: As specified in 34970A Driver DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    ///  Agilent 34970A Data Acquisition Unit driver for internal DMM
    /// </summary>
    public class Inst_Ag34970A_DMM : InstType_MultiMeter
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag34970A_DMM(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information
            // Add Ag34970A internal DMM details
            InstrumentDataRecord ag34970A_DMM_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34970A:34901A",				// hardware name 
                "0",								// minimum valid firmware version 
                "9-1-2:2.9");								// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A", ag34970A_DMM_Data);


            // Configure valid hardware information
            // Add Ag34970A internal DMM details
            InstrumentDataRecord ag34970A0_DMM_Data = new InstrumentDataRecord(
                "HEWLETT-PACKARD 34970A:0",				// hardware name 
                "0",								// minimum valid firmware version 
                "9-1-2:0");								// maximum valid firmware version 
            ValidHardwareData.Add("Ag34970A0", ag34970A0_DMM_Data);

            // Configure valid chassis information
            // Add 34970A chassis details
            InstrumentDataRecord ag34970AChassisData = new InstrumentDataRecord(
                "Chassis_Ag34970A",								// chassis driver name  
                "0.0.0.0",										// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag34970A", ag34970AChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag34970A)base.InstrumentChassis;
            // initialise the slot and channel number provided
            try
            {
                chassisSlot = Int32.Parse(slotId);
                chassisSubSlot = Int32.Parse(subSlotId);
            }
            catch (SystemException e)
            {
                throw new InstrumentException
                    ("Invalid slot or sub-slot Id format - must be integers!", e);
            }
            // get the chassis channel number
            chassisChannel = instrumentChassis.Get34970AChannelNumber(chassisSlot, chassisSubSlot);

            // generate the scan string that the 34970A will use when taking reading on this channel.
            // This is in a format which declares how many bytes follow, as documented in the AG34970A
            // programming guide (see "ROUT:SCAN?" command documentation).
            this.channelScanString = String.Format("#16(@{0})", chassisChannel);
        }

        #region Instrument class overrides
        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the Chassis and plugin
            get
            {
                string ver = String.Format("{0}:{1}",
                    this.instrumentChassis.FirmwareVersion,
                    this.instrumentChassis.GetSlotPluginFirmwareVersion(this.chassisSlot));
                return ver;
            }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware, and the plugin hardware
            get
            {
                string id = String.Format("{0}:{1}",
                    this.instrumentChassis.HardwareIdentity,
                    this.instrumentChassis.GetSlotID(this.chassisSlot));
                return id;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Changing the DMM function will reset defaults. Two mode changes are done
            // to ensure that there is at least one real change

            // default state depends on plugin type and channel number
            // Is this a current channel
            if ((this.chassisPluginType == "34901A") && (this.chassisSubSlot > 20))
            {
                // Change mode to AC Current mode
                this.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_AC_A_rms;
                // Change mode to DC Current mode (the default)
                this.MultiMeterMode = InstType_MultiMeter.MeterMode.Current_DC_A;
            }
            else
            {
                // Change mode to AC Voltage mode
                this.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_AC_V_rms;
                // Change mode to DC Voltage mode (the default)
                this.MultiMeterMode = InstType_MultiMeter.MeterMode.Voltage_DC_V;
            }
        }

        /// <summary>
        /// Set instrument on/offline. When setting online, initialises the instrument and checks
        /// that it is valid.
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // record the plugin type that this DMM channel is in
                    this.chassisPluginType = instrumentChassis.GetSlotID(chassisSlot);
                    // check that the 34970A's DMM is installed
                    string resp = GetCmd("INST:DMM:INST?");
                    if (resp != "1")
                    {
                        throw new InstrumentException("34970A DMM not installed");
                    }
                    // enable the internal DMM, if it isn't already
                    resp = GetCmd("INST:DMM?");
                    if (resp == "0")
                    {
                        SetCmd("INST:DMM ON");
                    }

                    // check can get DMM current function OK. If not a valid slot / sub-slot combination
                    // this function will throw an exception
                    try
                    {
                        InstType_MultiMeter.MeterMode mode = this.MultiMeterMode;
                    }
                    catch (Exception e)
                    {
                        throw new InstrumentException("Invalid slot and channel for DMM operation", e);
                    }
                }
            }
        }
        #endregion

        #region InstType_MultiMeter overrides
        /// <summary>
        /// Reads/sets the multimeter channel's measurement mode.
        /// </summary>
        public override InstType_MultiMeter.MeterMode MultiMeterMode
        {
            get
            {
                string resp = getDmmModeStr();

                // what is it?
                if (resp == "VOLT") return MeterMode.Voltage_DC_V;
                else if (resp == "VOLT:AC") return MeterMode.Voltage_AC_V_rms;
                else if (resp == "CURR") return MeterMode.Current_DC_A;
                else if (resp == "CURR:AC") return MeterMode.Current_AC_A_rms;
                else if (resp == "RES") return MeterMode.Resistance2wire_ohms;
                else if (resp == "FRES") return MeterMode.Resistance4wire_ohms;
                else throw new InstrumentException("Invalid MeterMode: " + resp);
            }
            set
            {
                string newMode = null;
                switch (value)
                {
                    case MeterMode.Voltage_DC_V: newMode = "VOLT"; break;
                    case MeterMode.Voltage_AC_V_rms: newMode = "VOLT:AC"; break;
                    case MeterMode.Current_DC_A: newMode = "CURR"; break;
                    case MeterMode.Current_AC_A_rms: newMode = "CURR:AC"; break;
                    case MeterMode.Resistance2wire_ohms: newMode = "RES"; break;
                    case MeterMode.Resistance4wire_ohms: newMode = "FRES"; break;
                    default:
                        throw new InstrumentException("Invalid meter mode value: " + value.ToString());
                }
                // new mode needs to be in double quotes
                string newModeFull = "\"" + newMode + "\"";
                // get existing mode - as change of mode will reset all defaults, so be slow!
                string command = String.Format("FUNC? (@{0})", chassisChannel);
                string resp = GetCmd(command);
                // same mode as before, do nothing
                if (resp == newModeFull) return;
                // change the mode
                command = String.Format("FUNC {0},(@{1})", newModeFull,
                    chassisChannel);
                SetCmd(command);
            }
        }
        /// <summary>
        /// Returns that this instrument is of True RMS type.
        /// </summary>
        public override InstType_MultiMeter.MeterAcType AcType
        {
            get { return MeterAcType.True_RMS; }
        }

        /// <summary>        
        /// Reads/sets the manual measurement range for the multimeter channel. 
        /// If "InstType_MultiMeter.AutoRange" is provided or returned, this is the value denoting auto-ranging.
        /// NOTE: If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// </summary>
        public override double MeterRange
        {
            get
            {
                // get DMM mode string
                string modeStr = getDmmModeStr();
                // is autorange?
                if (isAutoRange(modeStr)) return AutoRange;

                string command;
                string resp;
                // not autorange - what is it?
                command = String.Format("{0}:RANGE? (@{1})", modeStr, chassisChannel);
                resp = GetCmd(command);
                return getDblFromString(command, resp);
            }
            set
            {
                // get DMM mode string
                string modeStr = getDmmModeStr();
                string command;
                // get "AutoRange" state of the instrument
                bool instrInAutoRange = isAutoRange(modeStr);
                // are we being requested to go into autoRange?
                bool autoRangeRequested = IsAutoRange(value);

                // is autorange
                if (autoRangeRequested)
                {
                    // switch on autorange if it isn't already
                    if (!instrInAutoRange)
                    {
                        command = String.Format("{0}:RANGE:AUTO ON,(@{1})", modeStr, chassisChannel);
                        SetCmd(command);
                    }
                }
                else
                {
                    //  switch off autorange if it isn't already...
                    if (instrInAutoRange)
                    {
                        command = String.Format("{0}:RANGE:AUTO OFF,(@{1})",
                            modeStr, chassisChannel);
                        SetCmd(command);
                    }
                    // set to new range - always set this to avoid comparisons of doubles
                    // use scientific format (e.g. "1.23456E-001")
                    command = String.Format("{0}:RANGE {1},(@{2})", modeStr,
                        value.ToString("E"), chassisChannel);
                    SetCmd(command);
                }
            }
        }

        /// <summary>
        /// Reads the value measured on the multimeter channel. Units depend on 
        /// the current mode of the multimeter.
        /// </summary>
        public override double GetReading()
        {
            string command;
            string resp;

            // check the scan settings for the DAU - force to 1 channel only 
            // (this one!)
            resp = GetCmd("ROUT:SCAN?");
            // is the DAU already configured to the correct channel?
            if (resp != this.channelScanString)
            {
                // NO: set it up...
                command = String.Format("ROUT:SCAN (@{0})", chassisChannel);
                SetCmd(command);
            }

            command = "READ?";

            // read the channel back - use specific checking logic to take account of
            // possibilities of readings being overrange!
            resp = instrumentChassis.Query_Unchecked(command, this);

            double value;
            // check for overload condition
            if (resp.Substring(1, resp.Length - 1) == "9.90000000E+37")
            {
                // Yes, this was an overload
                value = Overload;

                // We need some custom error checking code here
                // Get the questionable data register - it should have some values due to the overload.
                // The actual value will depend on the mode.
                // Read will clear the register (which we need for future error checking to work)
                resp = instrumentChassis.Query_Unchecked("STAT:QUES:EVEN?", this);
                int questDataRegister = Int32.Parse(resp);
                // if no values, raise an exception on this chassis - something else was wrong!
                if (questDataRegister == 0)
                {
                    // RaiseChassisException takes a snapshot of the chassis' status values
                    // and throws ChassisException
                    instrumentChassis.RaiseChassisException(command, this);
                }
                // clear standard event register - do this by reading it
                byte standardEventByte = instrumentChassis.StandardEventRegister;
            }
            else
            {
                // convert string value to double
                value = getDblFromString(command, resp);

                // Replicate normal error checking code
                // get status byte
                StatusByteFlags statusByte = instrumentChassis.GetStatusByte();
                // If there is an error, this will seen in the Standard Event Register.
                // Check if the status byte is flagging problems here
                if (instrumentChassis.IsStandardEvent(statusByte))
                {
                    // RaiseChassisException takes a snapshot of the chassis' status values
                    // and throws ChassisException
                    instrumentChassis.RaiseChassisException(command, this);
                }
            }

            return value;
        }

        /// <summary>        
        /// Reads/sets the integration time (measurement averaging) in seconds for the multimeter channel. 
        /// </summary>
        public override double IntegrationTime_s
        {
            get
            {
                string modeStr = getDmmModeStr();
                if (modeStr == "VOLT:AC" || modeStr == "CURR:AC")
                {
                    throw new InstrumentException("Can't get integration time in AC mode!");
                }

                string command = String.Format("{0}:APER? (@{1})", modeStr, chassisChannel);
                string resp = GetCmd(command);
                return getDblFromString(command, resp);
            }
            set
            {
                string modeStr = getDmmModeStr();
                if (modeStr == "VOLT:AC" || modeStr == "CURR:AC")
                {
                    throw new InstrumentException("Can't set integration time in AC mode!");
                }
                string command = String.Format("{0}:APER {1},(@{2})", modeStr, value.ToString("E"),
                    chassisChannel);
                SetCmd(command);
            }
        }

        #endregion

        #region Private helper functions and data
        /// <summary>
        /// "Set" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void SetCmd(string command)
        {
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// "Get" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string GetCmd(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// Query the instrument to find it's current DMM mode.
        /// </summary>
        /// <returns>Mode string as returned from the 34970A, without the enclosing double quotes</returns>
        private string getDmmModeStr()
        {
            // get the mode
            string command = String.Format("FUNC? (@{0})", chassisChannel);
            string resp = GetCmd(command);

            // response comes in quotes, remove 1st and last chars
            if (resp.Length < 2)
            {
                throw new InstrumentException("Invalid response to '" + command + "' : " + resp);
            }
            resp = resp.Substring(1, resp.Length - 2);
            return resp;
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        /// <summary>
        /// Queries the DMM channel to see if it is in autorange mode
        /// </summary>
        /// <param name="modeStr">What DMM mode is this in (string)</param>
        /// <returns>true if autorange, false if not</returns>
        private bool isAutoRange(string modeStr)
        {
            string resp;
            string command;
            command = String.Format("{0}:RANGE:AUTO? (@{1})", modeStr, chassisChannel);
            resp = GetCmd(command);
            if (resp == "1") return true;
            else if (resp == "0") return false;
            else throw new InstrumentException("Bad response to '" + command + "' : " + resp);
        }

        /// <summary>
        /// Instrument chassis object
        /// </summary>
        private Chassis_Ag34970A instrumentChassis;

        /// <summary>
        /// Chassis slot for the DMM
        /// </summary>
        private int chassisSlot;

        /// <summary>
        /// Chassis channel for the DMM
        /// </summary>
        private int chassisSubSlot;

        /// <summary>
        /// Chassis Plugin Type (Model number)
        /// </summary>
        private string chassisPluginType;

        /// <summary>
        /// Which channel number is the DMM (Global for chassis)
        /// </summary>
        private int chassisChannel;

        /// <summary>
        /// Chassis scan string as used by "ROUT:SCAN?" command.
        /// </summary>
        private string channelScanString;
        #endregion
    }
}
  

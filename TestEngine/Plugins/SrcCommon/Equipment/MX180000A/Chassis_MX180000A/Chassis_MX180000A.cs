// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_MX180000A.cs
//
// Author: andy.li, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_MX180000A : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_MX180000A(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord mp1800AData = new ChassisDataRecord(
                "ANRITSU MP1800A",			// hardware name 
                "0",			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxx");		// maximum valid firmware version 
            ValidHardwareData.Add("MP1800A", mp1800AData);

            ChassisDataRecord mp1810AData = new ChassisDataRecord(
                 "ANRITSU MT1810A",			// hardware name 
                  "0",			// minimum valid firmware version 
                  "xxxxxxxxxxxxxxxx"); ;	// maximum valid firmware version 
            ValidHardwareData.Add("MP1810A", mp1810AData);

        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {

                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                return idn[2];
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {

                    //// Setup Standard Error register mask  on a 488.2 instrument
                    //base.Timeout_ms = 20000;
                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);
                }
            }
        }
        public string Query(string cmd,Instrument instrument)
        {
            cmd = cmd + "\n";
            return Query_Unchecked(cmd, instrument);
        }
        public void Write(string cmd, Instrument instrument)
        {
            cmd = cmd + "\n";
            Write_Unchecked(cmd, instrument);
        }
        #endregion
    }
}

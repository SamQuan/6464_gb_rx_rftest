using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;


namespace Test_MP1800A_PPG
{
    class Program
    {
        static void Main(string[] args)
        {

            initCode();

            RunCode();

            ShutDownCode();

            

        }


        #region Private helper fns
        private static void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private static void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private static void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion

        #region main function

        private static void initCode()
        {

            ppgChassis = new Chassis_MX180000A("PPGChassis", "Chassis_MX180000A", "MT1810A");

           
            ppgInstr = new Instr_MP1800A_PPG("PPGInstr", "Instr_MP1800A_PPG", "3", "", ppgChassis);

            ppgChassis.IsOnline = true;
            //ppgChassis.EnableLogging = true;
            //TestOutput(ppgChassis, "PPG Chassis Test OK!");

            ppgInstr.IsOnline = true;
            //ppgInstr.EnableLogging = true;
            //TestOutput(ppgInstr, "PPG Instruments test ok!");
        }


        private static void RunCode()
        {

            ppgInstr.SetDefaultState();
            Assert.AreEqual(true, ppgInstr.PattGenEnabled);
            //Assert.AreEqual(true, ppgInstr.ClockOutputsTrack);
            Assert.AreEqual(true, ppgInstr.DataOutputsTrack);
            Assert.AreEqual(1.0, ppgInstr.GetOutputVoltage(InstType_BertPattGenOutput.Data).Amplitude_V);

            InstType_BertRfCouplingMode mode = ppgInstr.GetOutputCoupling(InstType_BertPattGenOutput.Data);
            if (mode != InstType_BertRfCouplingMode.DcCoupledTerm_0V)
            {
                Assert.Fail("PPG output coupling not reset to default of DC 0V");
            }
            Assert.AreEqual(50, ppgInstr.GetOutputCrossingPt_Percent(InstType_BertPattGenOutput.Data));

            InstType_BertOutputVoltageDef vDef = ppgInstr.GetOutputVoltage(InstType_BertPattGenOutput.Data);
            Assert.AreEqual(1.0, vDef.Amplitude_V);
            Assert.AreEqual(3.3, vDef.Offset_V);

            InstType_BertDataPatternType patternType = ppgInstr.PatternType;
            if (patternType != InstType_BertDataPatternType.Prbs)
            {
                Assert.Fail("Invalid defualt pattern type");
            }

            Assert.AreEqual(23, ppgInstr.PrbsLength);

            //Enable the pattern Generator Output
            ppgInstr.PattGenEnabled = true;
            Assert.AreEqual(true, ppgInstr.PattGenEnabled);
            System.Threading.Thread.Sleep(2000);

            TestOutput("\n\n*** T01_CheckDefaultState: Confirmed PPG instrument set to defualt state ***");


            TestOutput("\n\n*** T03_ChangePatternToPrbs31 ***");
            ppgInstr.PrbsLength = 31;
            Assert.AreEqual(31, ppgInstr.PrbsLength);
        }

        private static void ShutDownCode()
        {
            TestOutput("Taking the chassis offline!");

            ppgChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        #endregion


        private static Chassis_MX180000A ppgChassis;
        private static Instr_MP1800A_PPG ppgInstr;

    }
}

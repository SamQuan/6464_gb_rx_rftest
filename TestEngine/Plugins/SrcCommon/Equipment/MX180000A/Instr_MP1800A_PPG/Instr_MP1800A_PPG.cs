// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_MP1800A_PPG.cs
//
// Author: wendy.wen, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestSolution.ChassisNS;


namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Instr_MP1800A_PPG : Instrument,IInstType_BertPatternGen
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_MP1800A_PPG(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord MU181040AData = new InstrumentDataRecord(
                "MU181040A",				// hardware name 
                "0.00.00",  			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxx");			// maximum valid firmware version 
            ValidHardwareData.Add("MU181040A", MU181040AData);

            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord MU181020AData = new InstrumentDataRecord(
                "MU181020A",				// hardware name 
                "0.00.00",  			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxxxx");			// maximum valid firmware version 
            ValidHardwareData.Add("MU181020A", MU181020AData);

            InstrumentDataRecord MP1800AData = new InstrumentDataRecord(
                "ANRITSU MP1800A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxxx");			// maximum valid firmware version 
            ValidHardwareData.Add("MP1800A", MP1800AData);

            InstrumentDataRecord MT1810AData = new InstrumentDataRecord(
                "ANRITSU MT1810A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxxx");			// maximum valid firmware version 
            ValidHardwareData.Add("MT1810A", MT1810AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord mx180000AChasis = new InstrumentDataRecord(
                "Chassis_MX180000A",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "2.00.00");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MT1800A", mx180000AChasis);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_MX180000A)chassisInit;

            slot = int.Parse(slotInit);

            cmdForSlot = cmd + Slot.ToString();

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return this.instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return this.instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            Reset();

            this.PattGenEnabled=false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion


        #region IInstType_BertPatternGen Members

        /// <summary>
        /// Get/set whether the clock outputs track
        /// </summary>
        public bool ClockOutputsTrack
        {
            get
            {
                bool ret=false;
                //string resp1 = instrumentChassis.Query("CON?", this);

                //int resp = convertRespToInt(resp1);
                ////byte[] respa = instrumentChassis.QueryIEEEBinary_Unchecked(":OUTP:CLOC:TRAC?", this);

                //if (resp == 0)
                //{
                //    ret = false;
                //}
                //else if (resp == 1)
                //{
                    instrumentChassis.Write("OOF 1", this);
                    string res1 = instrumentChassis.Query("TRK?", this);
                    int res = convertRespToInt(res1);

                    if (res == 0)
                    {
                        ret = false;
                        
                    }
                    else if (res == 1)
                    {
                        ret = true;
                    }
                    
                //}
                //else
                //{
                //    ret = false;
                //    throw new InstrumentException("Invalid response from instrument on Clock Output Track query: " + resp);
                //}

                return ret;
            }
            set
            {
                instrumentChassis.Write("CON 1", this);
                instrumentChassis.Write("OOF 1", this);

                if (value)
                {
                    
                    instrumentChassis.Write("TRK 1", this);
                }
                else
                {
                    instrumentChassis.Write(":TRK 0", this);
                }
            }
        }

        /// <summary>
        /// Get/set whether the data outputs track
        /// </summary>
        public bool DataOutputsTrack
        {
            get
            {
                bool ret=false;
                //string respString = instrumentChassis.Query("DON?", this);
                //int resp = convertRespToInt(respString);

                //if (resp == 1)
                //{
                    instrumentChassis.Write("OOF 0", this);
                    System.Threading.Thread.Sleep(1000);

                    string res = instrumentChassis.Query("TRK?", this);

                    int resInt=convertRespToInt(res);

                    if (resInt == 1)
                    {
                        ret = true;

                    }
                    else
                    {
                        ret = false;
                    }
    
                //}
                //else
                //{
                //    ret = false;
                //}
                

                

                return ret;
            }
            set
            {
                instrumentChassis.Write("DON 1", this);
                instrumentChassis.Write("OOF 0", this);

                if (value)
                {

                    instrumentChassis.Write("TRK 1", this);
                }
                else
                {
                    instrumentChassis.Write(":TRK 0", this);
                }
            }
        }


        /// <summary>
        /// Get Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Which mode</returns>
        public InstType_BertRfCouplingMode GetOutputCoupling(InstType_BertPattGenOutput output)
        {
            string commandDataOn = null;
            string commandDataSelect = null;
            string commandDCOn = null;
            string commandDCVlotage = null;
            InstType_BertRfCouplingMode mode = InstType_BertRfCouplingMode.DcCoupledTerm_0V;
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    break;
                case InstType_BertPattGenOutput.Data:
                    commandDataOn = "DON 1";
                    commandDataSelect = "OOF 0";
                    commandDCOn = "DAD?";
                    commandDCVlotage = "DOS?";
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    commandDataOn = "DON 1";
                    commandDataSelect = "OOF 0";
                    commandDCOn = "DAD?";
                    commandDCVlotage = "NOS?";
                    break;
                default:
                    break;
            }

            instrumentChassis.Write(commandDataOn, this);
            instrumentChassis.Write(commandDataSelect, this);
            int acOn = convertRespToInt(instrumentChassis.Query(commandDCOn, this));
            if (acOn == 1)
            {
                mode = InstType_BertRfCouplingMode.AcCoupled;
            }
            else
            {
                double offset = convertRespToDouble(instrumentChassis.Query(commandDCVlotage, this));

                if (offset == 0)
                {
                    mode = InstType_BertRfCouplingMode.DcCoupledTerm_0V;
                    
                }
                else if (offset == -2)
                {
                    mode = InstType_BertRfCouplingMode.DcCoupledTerm_minus2V;
                }

 
            }
            return mode;
            
        }

        /// <summary>
        /// Set Output RF coupling mode for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="mode">Which mode</param>
        public void SetOutputCoupling(InstType_BertPattGenOutput output, InstType_BertRfCouplingMode mode)
        {
            string commandDataOn = null;
            string commandDataSelect = null;
            string commandDCOn = null;
            string commandDCVlotage = null;

            commandDataOn = "DON 1";
            commandDataSelect = "OOF 0";

            commandDCOn = "DAD ";
            
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    break;
                case InstType_BertPattGenOutput.Data:
                    commandDCVlotage = "DOS ";
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    commandDCVlotage = "NOS ";
                    break;
                default:
                    break;
            }

            switch (mode)
            {
                case InstType_BertRfCouplingMode.AcCoupled:
                    instrumentChassis.Write(commandDCOn + "1", this);
                    break;
                case InstType_BertRfCouplingMode.DcCoupledTerm_0V:
                    instrumentChassis.Write(commandDCOn + "0", this);
                    instrumentChassis.Write(commandDCVlotage + "0", this);
                    break;
                case InstType_BertRfCouplingMode.DcCoupledTerm_minus2V:
                    instrumentChassis.Write(commandDCOn + "0", this);
                    instrumentChassis.Write(commandDCVlotage + "-2", this);
                    break;
                default:
                    break;
            }

        }


        /// <summary>
        /// Get Crossing Point percentage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Crossing Point percentage</returns>
        public double GetOutputCrossingPt_Percent(InstType_BertPattGenOutput output)
        {
            string command = null;
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
                case InstType_BertPattGenOutput.Data:
                    command = "DCR?";
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    command = "XDC?";
                    break;
                default:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
            }
            string resp = instrumentChassis.Query(command, this);

            return convertRespToInt(resp);
            
        }

        /// <summary>
        /// Set Crossing Point percentage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="percent">Crossing Point percentage</param>
        public void SetOutputCrossingPt_Percent(InstType_BertPattGenOutput output, double percent)
        {
            string command = null;
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
                case InstType_BertPattGenOutput.Data:
                    command = "DCR "+percent.ToString();
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    command = "XDC " + percent.ToString();
                    break;
                default:
                    throw new InstrumentException("Invalid patten generator type!");
                    break;
            }

            instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Get Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <returns>Output Voltage</returns>
        public InstType_BertOutputVoltageDef GetOutputVoltage(InstType_BertPattGenOutput output)
        {
            string command_Amp = null;
            string command_offset = null;
            string command_ACOn = null;
            InstType_BertOutputVoltageDef ret;
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    throw new InstrumentException("The method or operation is not implemented!");
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    throw new InstrumentException("The method or operation is not implemented!");
                    break;
                case InstType_BertPattGenOutput.Data:
                    command_Amp = "DAP?";
                    command_ACOn = "DAD?";
                    command_offset = "DOS?";
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    command_Amp = "NAP?";
                    command_ACOn = "DAD?";
                    command_offset = "NOS?";
                    break;
                default:
                    throw new InstrumentException("The method or operation is not implemented!");
                    break;
            }

            ret.Amplitude_V = convertRespToDouble(instrumentChassis.Query(command_Amp, this));

            int res = convertRespToInt(instrumentChassis.Query(command_ACOn, this));
            if (res == 0)
            {
                ret.Offset_V = convertRespToDouble(instrumentChassis.Query(command_offset, this));
            }
            else
            {
                ret.Offset_V = 0;
            }


            return ret;
            

        }


        /// <summary>
        /// Set Output voltage for the specified output
        /// </summary>
        /// <param name="output">Which output</param>
        /// <param name="vDef">Voltage setup structure</param>
        public void SetOutputVoltage(InstType_BertPattGenOutput output, InstType_BertOutputVoltageDef vDef)
        {
            string command = "";
            switch (output)
            {
                case InstType_BertPattGenOutput.Clock:
                    break;
                case InstType_BertPattGenOutput.ClockBar:
                    break;
                case InstType_BertPattGenOutput.Data:
                    command = "DAP ";
                    break;
                case InstType_BertPattGenOutput.DataBar:
                    command = "NAP ";
                    break;
                default:
                    break;
            }

            double voltageMin;
            double voltageMax;
            if ((output == InstType_BertPattGenOutput.Data) || (output == InstType_BertPattGenOutput.DataBar))
            {
                voltageMin = 0.25;
                voltageMax = 2.5;
            }
            else
            {
                voltageMin = 0.25;
                voltageMax = 2.5;
            }

            //On this instrument, the output voltage can be set between 0.5V and 2V, in 10mV steps 
            vDef.Amplitude_V = Math.Round(vDef.Amplitude_V, 2);

            if ((vDef.Amplitude_V < voltageMin) || (vDef.Amplitude_V > voltageMax))
            {
                throw new InstrumentException("Invalid output voltage specified: " + vDef.Amplitude_V + ". Must be between " + voltageMin + "V and 2V in 10mV increments");
            }

            if (vDef.Offset_V != 0)
            {
                throw new InstrumentException("DC Offset on Voltage specified for output not supported by this instrument. Offset value must be 0V");
            }

            //write to the instrument.
            instrumentChassis.Write(command + " " + vDef.Amplitude_V, this);
        }

        

        #endregion



        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        private void Reset()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("*RST", this);


        }


        /// <summary>
        /// Get/set Pattern Generator state (true:enabled, false:disabled)
        /// The outputs of data and databar are independently controlled. 
        /// For this driver will enable/disable them at the same time.
        /// </summary>
        public bool PattGenEnabled
        {
            get
            {
                int resp, resp1, resp2;

                resp = convertRespToInt(instrumentChassis.Query("DON?", this));
                resp1 = convertRespToInt(instrumentChassis.Query("OOF?", this));
                resp2 = convertRespToInt(instrumentChassis.Query("TRK?", this));

                if ((resp == 1) && (resp1 == 0) || (resp2 == 1))
                {
                    //It should never be the case that the data and databar in different states. 
                    //Unless someone has been messing about on the front panel.
                    //Assume that if either of them is disabled, that the instrument output is disabled.
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                string enable;
                if (value)
                {
                    enable = "1";
                }
                else
                {
                    enable = "0";
                }

                //Enable/disable both data and databar outputs
                instrumentChassis.Write("DON " + enable, this);

                if (!DataOutputsTrack)
                {
                    //Can only control databar if the outputs are not tracking.
                    instrumentChassis.Write("OOF 0", this);
                    System.Threading.Thread.Sleep(1000);
                    instrumentChassis.Write("TRK " + enable, this);
                }
            }
        }


        /// <summary>
        /// Get/set the Pattern type.
        /// In this version of the driver, only PRBS Patterns shall be supported.
        /// </summary>
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                string resp1 = instrumentChassis.Query("PTS?", this);

                int resp = convertRespToInt(resp1);

                if (resp == 3)
                {
                    //This should really be the only response seen, unless
                    //someones been playing with the front panel
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }

            }
            set
            {
                if (value == InstType_BertDataPatternType.Prbs)
                {
                    instrumentChassis.Write("PTS 3", this);
                    System.Threading.Thread.Sleep(1000);

                    instrumentChassis.Write("PTN " + prbsIndex, this);
                }
                else
                {
                    throw new InstrumentException("other patten type is not implement!");
                }
            }
        }


        /// <summary>
        /// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        /// </summary>
        public int PrbsLength
        {
            get
            {
                return this.prbsLength;
            }
            set
            {
                //Only valid values are 7,10,15,23 or 31
                if ((value == 7) || (value == 9) || (value == 10) || (value == 11) || (value == 15) || (value == 20) || (value == 23) || (value == 31))
                {
                    this.prbsLength = value;
                    convertPrbsLengthToIndex();

                    instrumentChassis.Write("PTN " + this.prbsIndex, this);
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS length attempted to be set: " + value);
                }
            }
        }

        /// <summary>
        /// Get/Set user defined pattern. Not supported by this version of the driver.
        /// </summary>
        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }


        /// <summary>
        /// Get/set the User Pattern length. Not supported by this version of the driver.
        /// </summary>
        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region additional method

        private void convertPrbsLengthToIndex()
        {
            switch (prbsLength)
            {
                case 7:
                    prbsIndex = 2;
                    break;
                case 9:
                    prbsIndex = 3;
                    break;
                case 10:
                    prbsIndex = 4;
                    break;
                case 11:
                    prbsIndex = 5;
                    break;
                case 15:
                    prbsIndex = 6;
                    break;
                case 20:
                    prbsIndex = 7;
                    break;
                case 23:
                    prbsIndex = 8;
                    break;
                case 31:
                    prbsIndex = 9;
                    break;
                default:
                    break;
            }
        }

        private int convertRespToInt(string resp)
        {
            int index = 0;
            
            index = resp.IndexOf(" ");

            string returnValue = resp.Substring(index + 1).Trim();

            return Convert.ToInt32(returnValue);
        }

        private double convertRespToDouble(string resp)
        {
            int index = 0;
            
            index = resp.IndexOf(" ");

            string returnValue = resp.Substring(index + 1).Trim();

            return Convert.ToDouble(returnValue);
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_MX180000A instrumentChassis;

        //PRBS sequence length. Default value is 23, 
        //which is what the instrument sets itself to after *RST
        //Valid values are 7,10,15,23 or 31
        private int prbsLength = 23;

        private int prbsIndex = 0;

        private int slot;

        string cmd = ":MODule:ID ";

        string cmdForSlot = null;
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_MX180000A.cs
//
// Author: andy.li, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_MX180000A : Instrument, IInstType_BertErrorAnalyser
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_MX180000A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord MU181020AData = new InstrumentDataRecord(
                "MU181040A",				// hardware name 
                "0.00.00",  			// minimum valid firmware version 
                "2.00.00");			// maximum valid firmware version 
            ValidHardwareData.Add("MU181020A", MU181020AData);
            InstrumentDataRecord MP1800AData = new InstrumentDataRecord(
                "ANRITSU MP1800A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "xxxxxxxxxxxxxxxxx");			// maximum valid firmware version 
            ValidHardwareData.Add("MP1800A", MP1800AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord mx180000AChasis = new InstrumentDataRecord(
                "Chassis_MX180000A",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "2.00.00");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MP1800A", mx180000AChasis);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_MX180000A)base.InstrumentChassis;

            // validate range for slot and subslot.
            int slot;
            //int subSlot;
            try
            {
                slot = int.Parse(this.Slot);
                //subSlot = int.Parse(this.SubSlot);
            }
            catch (SystemException e)
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot/subslot format",
                    this.Name);
                throw new InstrumentException(errStr, e);
            }

            // 8166 has 17 slots (maximum possible)
            if ((slot < 1) || (slot > 17))
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot {1}",
                    this.Name, this.Slot);
                throw new InstrumentException(errStr);
            }

            //// A maximum of two channels in each slot (81635, 81619)
            //if ((subSlot < 1) || (subSlot > 2))
            //{
            //    string errStr = String.Format("Instrument '{0}': Invalid sub-slot {1}",
            //        this.Name, this.SubSlot);
            //    throw new InstrumentException(errStr);
            //}

            //// Generate the common command stem that most commands use!
            //this.commandStem = String.Format(":SENS{0}:CHAN{1}:",
            //        this.Slot, this.SubSlot);
            //this.commandPowStem = this.commandStem + "POW:";

        }
        public void AutoSetup() 
        {
            //Select ED Module
            this.instrumentChassis.Write(":MODule:ID 4", this);
            //Turn AutoSync ON to look for Sync Error
            this.instrumentChassis.Write("SYN 1", this);
            this.instrumentChassis.Write(":SYSTem:CFUNction ASE", this);
            this.instrumentChassis.Write("ASM 0", this);
            ////try fine mode first. if not proper ,change it to coarse mode;
            //this.instrumentChassis.Write(":SENSe:MEASure:ASEarch:SMODe FINE", this);
            this.instrumentChassis.Write("ASE 4,1", this);
            this.instrumentChassis.Write("AST", this);
            System.Threading.Thread.Sleep(500);
            string resp = this.instrumentChassis.Query(":SENSe:MEASure:ASEarch:STATe?", this);
            
            while (resp != "0")
            {
                if (resp == "1")
                {
                    System.Threading.Thread.Sleep(500);
                    resp = this.instrumentChassis.Query(":SENSe:MEASure:ASEarch:STATe?", this);
                }
                else if (resp == "-1") break;
            }
            this.instrumentChassis.Write(":SYSTem:CFUNction OFF", this);
            //this.instrumentChassis.Write("SYN 0", this);
        }
        public void AnalyserStop() 
        {
            this.instrumentChassis.Write(":MODule:ID 4", this);
            this.instrumentChassis.Write("STO", this);
            System.Threading.Thread.Sleep(500);
        }

        public void AnalyserStart() 
        {
            this.instrumentChassis.Write(":MODule:ID 4", this);
            this.instrumentChassis.Write("STA", this);
        }
        // Summary:
        //     Get the analyser state (true: running, false: not running)
        public bool AnalyserEnabled
        {
            get
            {
                string resp = this.instrumentChassis.Query("MSR?", this);
                bool status = resp == "MSR 0" ? false : true;
                
                return status;
            }
        }
        //
        // Summary:
        //     How many errored bits
        public long ErroredBits { 
            get 
            {
                long errorCount;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"EC?";
                string errorString = this.instrumentChassis.Query(req, this).Remove(0, 2);
                if(errorString.Trim() == "1.0000E-99")
                    throw new InstrumentException("ED Status is SyncLoss or Measure is not started");
                //errorString = errorString.Trim();
                //try
                //{

                errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);
                //}
                //catch(Exception)
                //{
                //    if (errorString == "1.0000E-99")
                //    {
                //        errorCount = 0;
                //    }
                //    else
                //    {
                //        errorCount = 1;
                //    }
                //}

                return errorCount;
            }
        }
        //
        // Summary:
        //     The number of errors accumulated since the start of the gating period, where
        //     each error is a true data one received as a data zero
        public long ErroredBitsOnesRecievedAsZeros
        {
            get
            {
                long errorCount;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"END? 2,9";
                string errorString = this.instrumentChassis.Query(req, this).Trim('"');
                int n = 5;
                while (errorString == "ERR" && n > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    errorString = this.instrumentChassis.Query(req, this).Trim('"');
                    n--;
                }
                errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);

                return errorCount;
            }
        }
        // Summary:
        //     The number of errors accumulated since the start of the gating period, where
        //     each error is a true data zero received as a data one.
        public long ErroredBitsZerosRecievedAsOnes
        {
            get
            {
                long errorCount;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"END? 2,8";
                string errorString = this.instrumentChassis.Query(req, this).Trim('"');
                int n = 5;
                while (errorString == "ERR" && n > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    errorString = this.instrumentChassis.Query(req, this).Trim('"');
                    n--;
                }
                errorCount = Int64.Parse(errorString, System.Globalization.NumberStyles.Float);

                return errorCount;
            }
        }
        //
        // Summary:
        //     Ratio of errored bits to total bit count
        public double ErrorRatio
        {
            get
            {
                double errorRatio;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"ER?";
                string errorString = this.instrumentChassis.Query(req, this).Remove(0,2);
                if (errorString.Trim() == "0.0000E-00")
                {
                    throw new InstrumentException("ED Status is SyncLoss or Measure is not started");
                }
                else if(errorString!="-------")
                    errorRatio = Double.Parse(errorString, System.Globalization.NumberStyles.Float); 
                else errorRatio = 999;

                return errorRatio;
            }
        }
        //
        // Summary:
        //     The error ratio accumulated since the start of the gating period, where each
        //     error is a true data one received as a data zero
        public double ErrorRatioOfOnesRecievedAsZeros
        {
            get
            {
                double errorRatio;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"END? 2,7";
                string errorString = this.instrumentChassis.Query(req, this).Trim('"');
                int n = 5;
                while (errorString == "ERR" && n > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    errorString = this.instrumentChassis.Query(req, this).Trim('"');
                    n--;
                }
                errorRatio = Double.Parse(errorString, System.Globalization.NumberStyles.Float);

                return errorRatio;
            }
        }
        //
        // Summary:
        //     The error ratio accumulated since the start of the gating period, where each
        //     error is a true data zero received as a data one.
        public double ErrorRatioOfZerosRecievedAsOnes
        {
            get
            {
                double errorRatio;
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string req = @"END? 2,6";
                string errorString = this.instrumentChassis.Query(req, this).Trim('"');
                int n = 5;
                while (errorString == "ERR" && n > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    errorString = this.instrumentChassis.Query(req, this).Trim('"');
                    n--;
                }
                errorRatio = Double.Parse(errorString, System.Globalization.NumberStyles.Float);

                return errorRatio;
            }
        }
        //
        // Summary:
        //     Get/set the Eye Delay
        public double MeasPhaseDelay_ps 
        {
            get
            {
                return 0.0; 
            }
            set 
            { }
        }
        //
        // Summary:
        //     Get/set the Eye threshold voltage
        public double MeasThreshold_V
        {
            get
            {
                return 0.0;
            }
            set
            { }
        }
        //
        // Summary:
        //     Get/set the Pattern type
        public InstType_BertDataPatternType PatternType
        {
            get
            {
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string resp = this.instrumentChassis.Query("PTS?", this).Trim();
                if (resp == "PTS 3")
                {
                    //This should really be the only response seen, unless
                    //someones been playing with the front panel
                    return InstType_BertDataPatternType.Prbs;
                }
                else
                {
                    return InstType_BertDataPatternType.Other;
                }
            }
            set
            {
                if (value == InstType_BertDataPatternType.Prbs)
                {
                    this.instrumentChassis.Write(":MODule:ID 4", this);
                    this.instrumentChassis.Write("PTS 3", this); 
                }
                else
                {
                    throw new InstrumentException("Only PRBS Patter Types are supported by this driver");
                }
  
            }
        }
        //
        // Summary:
        //     Get/set the Data Polarity of the input
        public InstType_BertDataPolarity Polarity
        {
            get
            {
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string resp = this.instrumentChassis.Query("LGC?", this).Trim();

                if (resp == "LGC 0")
                {
                    return InstType_BertDataPolarity.Normal;
                }
                else if (resp == "LGC 1")
                {
                    return InstType_BertDataPolarity.Inverted;
                }
                else
                {
                    throw new InstrumentException("Invalid response to Polarity Query from instrument: " + resp);
                }
            }
            set
            {
                this.instrumentChassis.Write(":MODule:ID 4", this);
                if (value == InstType_BertDataPolarity.Normal)
                {
                    this.instrumentChassis.Write("LGC 0", this);
                }
                else
                {
                    this.instrumentChassis.Write("LGC 1", this);
                }
            }
        }
        //
        // Summary:
        //     Get/set the PRBS length 2^N-1 (i.e. this is the "N")
        public int PrbsLength
        {
            get
            {
                this.instrumentChassis.Write(":MODule:ID 4", this);
                string resp = this.instrumentChassis.Query("PTN?", this).Trim();
                if (resp == "2")
                {
                    return 7;
                }
                else if (resp == "3")
                {
                    return 9;
                }
                else if (resp == "4")
                {
                    return 10;
                }
                else if (resp == "5")
                {
                    return 11;
                }
                else if (resp == "6")
                {
                    return 15;
                }
                else if (resp == "8")
                {
                    return 23;
                }
                else if (resp == "9")
                {
                    return 31;
                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length recieved from instrument: " + resp);
                }
            }
            set
            {
                if ((value == 7) || (value == 9) || (value == 10) || (value == 11)
                    || (value == 15) || (value == 23) || (value == 31))
                {
                    this.instrumentChassis.Write(":MODule:ID 4", this);
                    this.instrumentChassis.Write(":SENSe:PATTern:PRBS:LENGth " + value, this);
                    

                }
                else
                {
                    throw new InstrumentException("Invalid PRBS Length Specified : " + value);
                }
            }
        }
        //
        // Summary:
        //     Has there been a sync loss since the analyser was last enabled?
        public bool SyncLossDetected
        {
            get
            {
                this.instrumentChassis.Write(":MODule:ID 4", this);
               
                string req = @"MTR? 3";
                string reqs = this.instrumentChassis.Query(req, this);
                if (reqs == "MTR 0") return true;

                req = @"MTR? 4";
               reqs = this.instrumentChassis.Query(req, this);
                if (reqs == "MTR 0") return true;




                return false;
            }
        }
        //
        // Summary:
        //     Get/set the User Pattern itself
        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        //
        // Summary:
        //     Get the User Pattern Length
        public int UserPatternLength
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // All the firmware is in the plugin slot, not the mainframe - check if it is indeed present
                // Read the SYSTem:MODule? string which has 6-8 comma seperated substrings                
                string command = String.Format(":SYSTem:MODule? {0}", this.Slot);
                string resp = instrumentChassis.Query(command, this);
                if (resp.Split(',')[0] == "NONE")
                {
                    throw new InstrumentException("Slot " + this.Slot + " is empty!");

                }
                string pluginFwVer = resp.Split(',')[2];

                // Return the 4th substring containing the firmware version
                return pluginFwVer;
                //return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // All the Identity is in the plugin slot, not the mainframe - check if it is indeed present
                // Read the SYSTem:MODule? string which has 6-8 comma seperated substrings                
                string command = String.Format(":SYSTem:MODule? {0}", this.Slot);
                string resp = instrumentChassis.Query(command, this);
                if (resp.Split(',')[0] == "NONE")
                {
                    throw new InstrumentException("Slot " + this.Slot + " is empty!");

                }
                string pluginFwVer = resp.Split(',')[0];

                // Return the 4th substring containing the firmware version
                return pluginFwVer;
                //return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.instrumentChassis.Write(":MODule:ID 4", this);
            this.instrumentChassis.Write("*RST", this);
            this.instrumentChassis.Write("*CLS", this);
            this.instrumentChassis.Write("MOD 2", this);
            this.instrumentChassis.Write("CUR 1", this);
            this.instrumentChassis.Write("CAL 0", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.checkSlotNotEmpty();
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        /// <summary>
        /// Check that the slot isn't empty. This may happen before errors setup, so 
        /// DON'T called checked Chassis methods.
        /// </summary>
        private void checkSlotNotEmpty()
        {
            bool slotEmpty;

            // All the firmware is in the plugin slot, not the head
            string command = String.Format(":SYSTem:MODule? {0}", this.Slot); ;
            string resp = instrumentChassis.Query(command, this); ;
            if (resp.Split(',')[0] == "NONE")
            {
                    throw new InstrumentException("Slot " + this.Slot + " is empty!"); ;

            };

        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_MX180000A instrumentChassis;
        #endregion
    }
}

using System;
using System.Collections.Generic;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace TESTODL
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        private Chassis_OZODL650 testChassis;

        private Instr_OZODL650 testInstr;
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_OZODL650("Chassis650", "Chassis_OZODL650", "COM8");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            testInstr = new Instr_OZODL650("Instr650", "Instr_OZODL650", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            testInstr.SetDefaultState();
            TestOutput("set defaultState ok!");

            testInstr.Delay_ps = 100;
            TestOutput("set delay time to 100ps");
            // You can use Assert to check return values from tests
            double delayTime = testInstr.Delay_ps;
            TestOutput("current delay time is " + delayTime.ToString());
          
            testInstr.DelayMin_ps = 5;
            TestOutput("set min delay time to 5ps");
            double delayMinTime_ps = testInstr.DelayMin_ps;
            TestOutput("current delay time is " + delayMinTime_ps);
            testInstr.Step = 10;
            TestOutput("set step to 10");
            double currentstep = testInstr.Step;
            TestOutput("current step is " + currentstep);

            
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            testInstr.SetDefaultState();

            testInstr.Delay_ps = 100;

            testInstr.DelayMin_ps = 5;

            testInstr.Step = 10;

        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

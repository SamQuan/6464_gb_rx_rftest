// Copyright @Oclaro
//
// Reference to the specific document
// Bookham.TestEngine.Equipment
//
// InstType_ODL.cs
//
// Author: Wendy Wen
// Design: As specified in Test Module/Program DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.InstrTypes
{
    /// <summary>
    /// Optical Delay line controller
    /// </summary>
    public abstract class InstType_ODL : Instrument
    {
        /// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public InstType_ODL(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
        }

        /// <summary>
        /// Set the instrument to home position
        /// </summary>
        public abstract void SetDefaultStatus();

        /// <summary>
        /// To re-starts and re-initializes the unit
        /// </summary>
        public abstract void Reset();

        /// <summary>
        /// To display or change the baud rate
        /// </summary>
        public abstract int BaudRate
        {
            get;
            set;
        }

        /// <summary>
        /// To query the current status on the unit
        /// </summary>
        public abstract OperatingStatuses Status
        {
            get;
        }

        /// <summary>
        /// To Set or Return the absolute minimum time delay(ps)
        /// </summary>
        public abstract double DelayMin_ps
        {
            get;
            set;
        }

        /// <summary>
        /// To set or get the motor an absolute number of delay 
        /// from the home position
        /// </summary>
        public abstract double Delay_ps
        {
            get;
            set;
        }
        
        /// <summary>
        /// To set the motor an absolute number of setps 
        /// from the home position
        /// </summary>
        public abstract int Step
        {
            get;
            set;
        }

        /// <summary>
        /// Operating status list
        /// </summary>
        public enum OperatingStatuses
        { 
            /// <summary>
            /// Idle status
            /// </summary>
            Idle,
            /// <summary>
            /// Busy status
            /// </summary>
            Busy,
            /// <summary>
            /// Home status
            /// </summary>
            Home,
            /// <summary>
            /// End status
            /// </summary>
            End
        }
    }
}

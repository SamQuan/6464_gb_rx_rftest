// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag86122x.cs
//
// Author: K Pillar
// Design: As specified in Ag86122x DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;
using System.Threading;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Agilent 86122x wavemeter driver
	/// </summary>
	public class Inst_Ag86122x : InstType_Wavemeter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag86122x(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information

            // Add all Ag86122x details
            InstrumentDataRecord Ag86122AData = new InstrumentDataRecord(
                "Agilent Technologies 86122A",    				// hardware name 
				"0",											// minimum valid firmware version 
				"1.0");									// maximum valid firmware version 
            Ag86122AData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86122AData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag86122A", Ag86122AData);

           
            InstrumentDataRecord Ag86120BData = new InstrumentDataRecord(
                "Agilent Technologies 86120B",    				// hardware name 
                "0",											// minimum valid firmware version 
                "1.0");									// maximum valid firmware version 
            Ag86120BData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86120BData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag861220B", Ag86120BData);


            InstrumentDataRecord Ag86120CData = new InstrumentDataRecord(
                "Agilent Technologies 86120C",    				// hardware name 
                "0",											// minimum valid firmware version 
                 "1.0");									  // maximum valid firmware version 
            Ag86120CData.Add("MinWavelength_nm", "1270");			// minimum wavelength
            Ag86120CData.Add("MaxWavelength_nm", "1650");			// maximum wavelength
            ValidHardwareData.Add("Ag861220C", Ag86120CData);
			
			// Configure valid chassis information
			// Add Chassis_Ag86122x chassis details
            InstrumentDataRecord Ag86122xChassisData = new InstrumentDataRecord(	
				"Chassis_Ag86122x",								// chassis driver name  
				"0",											// minimum valid chassis driver version  
				"01.00.00");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag86122x", Ag86122xChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
			instrumentChassis = (Chassis_Ag86122x) base.InstrumentChassis;

            safeModeOperation =true;
		}


		#region Public Instrument Methods and Properties

        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the Chassis 
            get
            {
                string ver = this.instrumentChassis.FirmwareVersion;
                return ver;
            }
        }

        /// <summary>
        /// Hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware, the plugin hardware and the channel number
            get
            {
                string id = this.instrumentChassis.HardwareIdentity;
                return id;
            }
        }


		/// <summary>
		/// Configures the instrument into a default state.
		/// </summary>
		public override void SetDefaultState()
		{
			// Send a reset, *RST, and a cls
			this.Write("*RST");
            this.Write("*CLS");
		}

		#endregion

		#region Public Wavemeter InstrumentType Methods and Properties

        /// <summary>
        /// Selects single or continuous measurement acquisition mode. 
        /// </summary>
        public enum MeasMode
        {
            Single,
            Continuous,
        }
        /// <summary>
        /// Selects single or continuous measurement acquisition mode. 
        /// When ON, the instrument continuously measures the input spectrum.
        /// </summary>
        public MeasMode SelectMode
        {
            get
            {
                //:INITiate:CONTinuous{?| {ON | OFF | 1 | 0}}
                string rtn = this.Query(":INIT:CONT?");
                return rtn.Trim().StartsWith("0") ? MeasMode.Single : MeasMode.Continuous;
            }
            set
            {
                string cmd = ":INIT:CONT " + (value == MeasMode.Single ? "0" : "1");
                this.Write(cmd);
            }
        }

		/// <summary>
		/// Return frequency in GHz
		/// </summary>
		public override double Frequency_GHz 
		{
			get 
			{
                //Putting the marker onto the max peak wavelength
                instrumentChassis.Write("DISP:MARK:MAX", this);

				// Read frequency in GHz
                string cmd = ":MEAS:SCAL:POW:FREQ? DEF";//":MEAS:SCAL:POW:FREQ? MAX"
                string rtn = this.Query(cmd);
                
                //Hp returns in Hz
                const Double HzToGigaHzScaleFactor = 1e-9;

                double result = getDblFromString(cmd, rtn);
				
                // convert to GHz
                return (result * HzToGigaHzScaleFactor);
			}
		}

		/// <summary>
		/// Return wavelength in nm
		/// </summary>
		public override double Wavelength_nm 
		{
			get 
			{
                //Putting the marker onto the max peak wavelength
                this.Write("DISP:MARK:MAX");
                
				// Read wavelength in nm
                string cmd = ":MEAS:SCAL:POW:WAV? DEF";
                string rtn = this.Query(cmd);

                double result = getDblFromString(cmd, rtn);

                //Hp returns in Meters
                const Double MetersToNanometers = 1e9;

                return (result * MetersToNanometers);
			}
		}
		
		/// <summary>
		/// Returns the signal input power level in dBm
		/// </summary>
		public override double Power_dBm
		{
			get
			{
				// Set power to dbm
				this.Write(":UNIT:POW DBM");

                string cmd = ":MEAS:POW?";
				// Read power in dBm
				string rtn = this.Query(cmd);

                double result = getDblFromString(cmd, rtn);
				
                return result;
			}
		}

		/// <summary>
		/// Returns the signal input power level in mW
		/// </summary>
		public override double Power_mW
		{
			get
			{
				// Set power to W
				this.Write(":UNIT:POW W");

                string cmd = ":MEAS:POW?";

				// Read power in W
                string rtn = this.Query(cmd);

                double result = getDblFromString(cmd, rtn);

                const double WattsToMilliWattsScaleFactor = 1000;

				// return result converted to mW
                return (result * WattsToMilliWattsScaleFactor);
			}
		}

		/// <summary>
		/// Reads/sets the measurement medium
		/// </summary>
		public override Medium MeasurementMedium 
		{
			get
			{
				// Query medium
				string rtn = this.Query(":SENS:CORR:MED?");

				// Convert to enum 
				Medium medm;
				switch (rtn)
				{
					case "VAC": medm = Medium.Vacuum; break;
					case "AIR": medm = Medium.Air; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + rtn + "'");
				}

				// Return result
				return medm;
			}

			set 
			{
				// Build string
				string medm;
				switch (value)
				{
					case Medium.Vacuum: medm = "VAC"; break;
					case Medium.Air: medm = "AIR"; break;
					default: throw new InstrumentException("Unrecognised measurement medium '" + value.ToString() + "'");
				}

				// Write to instrument
				this.Write(":SENS:CORR:MED " + medm);

			}
        }

         /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return (safeModeOperation);
            }
            set
            {
                safeModeOperation = value;
            }
        }


        #endregion


        #region private functions

        /// <summary>
        /// Write, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the command we wish to write to chassis</param>
        private void Write(string cmd)
        {
            if (this.safeModeOperation)
            {
                instrumentChassis.Write(cmd,this);
                return;
            }
            else
            {
                //async = true, errorcheck = false
                instrumentChassis.Write(cmd, this, true, false);
                return;
            }
        
        }

        /// <summary>
        /// Query, depending on safemodeoperation will call correct thing in chassis
        /// </summary>
        /// <param name="cmd">the query string</param>
        /// <returns></returns>
        private string Query(string cmd)
        {
         if (this.safeModeOperation)
            {
                return(instrumentChassis.Query(cmd, this));
            }
            else
            {
                // errorcheck = false
                return(instrumentChassis.Query(cmd,this,false));
            }
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }


        #endregion
        


        #region Private Data

        // Chassis reference
        private Chassis_Ag86122x instrumentChassis;
        private bool safeModeOperation;
		#endregion
	}
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag8153_OpticalPowerMeter.cs
//
// Author: kpillar, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using NationalInstruments.VisaNS;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
   /// <summary>
   /// Class implementing the instrument commands for an 8153 optical power meter
   /// </summary>
    public class Inst_Ag8153_OpticalPowerMeter : InstType_OpticalPowerMeter
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag8153_OpticalPowerMeter(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // Configure valid hardware information
            string maxPowMeterFwVersion = "1";

		// Add 81524A optical head details
            InstrumentDataRecord ag81524AData = new InstrumentDataRecord(
                "HP 81524A",					// hardware name 
                "0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81524AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81524A", ag81524AData);
            

            // Add 81525A optical head details
            InstrumentDataRecord ag81525AData = new InstrumentDataRecord(
                "HP 81525A",					// hardware name 
                "0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81525AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81525A", ag81525AData);

            
            // Add 81532A optical head details
            InstrumentDataRecord ag81532AData = new InstrumentDataRecord(
                "HP 81532A",					// hardware name 
                "0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81532AData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81532A", ag81532AData);

            // Add 81533A optical head details
            InstrumentDataRecord ag81533BData = new InstrumentDataRecord(
                "HP 81533B",					// hardware name 
                "0",											// minimum valid firmware version 
                maxPowMeterFwVersion);								// maximum valid firmware version 
            ag81533BData.Add("MaximumPwrRangeDbm", "10");		// maximum valid power range 
            ValidHardwareData.Add("Ag81533B", ag81533BData);



            // Configure valid chassis information
            // Add 8153 chassis details
            InstrumentDataRecord ag8153Data = new InstrumentDataRecord(
                "Chassis_Ag8153",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.1.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag8153", ag8153Data);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag8153)base.InstrumentChassis;

            // validate range for slot and subslot.
            int slot;
            int subSlot;
            try
            {
                slot = int.Parse(this.Slot);
                subSlot = int.Parse(this.SubSlot);
            }
            catch (SystemException e)
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot/subslot format",
                    this.Name);
                throw new InstrumentException(errStr, e);
            }

            // 8153 has 2 slots (maximum possible)
            if ((slot < 1) || (slot > 2))
            {
                string errStr = String.Format("Instrument '{0}': Invalid slot {1}",
                    this.Name, this.Slot);
                throw new InstrumentException(errStr);
            }

            // A maximum of one channel per slot 
            if ((subSlot < 1) || (subSlot > 1))
            {
                string errStr = String.Format("Instrument '{0}': Invalid sub-slot {1}",
                    this.Name, this.SubSlot);
                throw new InstrumentException(errStr);
            }

            // Generate the common command stem that most commands use!
            this.commandStem = String.Format(":SENS{0}:",
                    this.Slot);
            this.commandPowStem = this.commandStem + "POW:";

            // default to safe mode
            this.safeMode = true;
        }
        #endregion

        #region Instrument overrides


        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {             
                //check we have something in this slot               
                string resp = checkSlotNotEmpty(); 
                
                // We cannot query the fw version on a slot with 8153, 
                // the best we can do is confirm slot is not empty and return a default value
                // to make our code work still
                string pluginFwVer = "0";

                return pluginFwVer;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {               
                //check we have something in this slot               
                string resp = checkSlotNotEmpty();

                return resp;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            SetContinuousModeOff();
            Mode = MeterMode.Absolute_dBm;
            Range = InstType_OpticalPowerMeter.AutoRange;
            Wavelength_nm = 1550;
            AveragingTime_s = 0.1;
            CalOffset_dB = 0.0;
            ReferencePower = 0.0;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // read hardware data to remember maximum power
                    this.maxPowerRange_dBm = Convert.ToDouble(HardwareData["MaximumPwrRangeDbm"]);

                   
                    // turn off continuous measurement
                    string command = String.Format("INIT{0}:CONT OFF", this.Slot);
                    chassisWrite(command);

                    // get current mode
                    this.currMode = Mode;
                }
            }
        }
        #endregion

        #region Optical Power Meter functions
        /// <summary>
        /// Get / Set the mode of optical power meter. 
        /// If a set of a mode which is not supported by this instrument is attempted, 
        /// an exception should be thrown.
        /// </summary>
        public override InstType_OpticalPowerMeter.MeterMode Mode
        {
            get
            {
                string command;
                string resp;
                // are we in absolute or relative mode
                command = commandPowStem + "REF:STAT?";
                resp = chassisQuery(command);
                if (resp == "+1")
                {
                    this.currMode = MeterMode.Relative_dB;
                }
                else if (resp == "+0")
                {
                    // dBm or W
                    command = commandPowStem + "UNIT?";
                    resp = chassisQuery(command);
                    if (resp == powUnit_dBm) this.currMode = MeterMode.Absolute_dBm;
                    else if (resp == powUnit_W) this.currMode = MeterMode.Absolute_mW;
                    else
                    {
                        throw new InstrumentException("Invalid response to '" + command + "': " + resp);
                    }
                }
                else
                {
                    throw new InstrumentException("Invalid response to '" + command + "': " + resp);
                }
                return this.currMode;
            }

            set
            {
                // get current mode - refresh cached state
                this.currMode = this.Mode;
                if (value == this.currMode) return;

                switch (value)
                {
                    case MeterMode.Absolute_dBm:
                        if (currMode == MeterMode.Relative_dB)
                        {
                            chassisWrite(commandPowStem + "REF:STAT 0");
                        }
                        chassisWrite(commandPowStem + "UNIT " + powUnit_dBm);
                        break;
                    case MeterMode.Absolute_mW:
                        if (currMode == MeterMode.Relative_dB)
                        {
                            chassisWrite(commandPowStem + "REF:STAT 0");
                        }
                        chassisWrite(commandPowStem + "UNIT " + powUnit_W);
                        break;
                    case MeterMode.Relative_dB:
                        // force a reference mode of absolute power
                        chassisWrite(commandPowStem + "REF:STAT:RATI TOREF");
                        // turn on relative mode
                        chassisWrite(commandPowStem + "REF:STAT 1");
                        break;
                    default:
                        throw new InstrumentException("Unsupported Meter Mode: " + value.ToString());
                }
                // remember new mode
                this.currMode = value;
            }
        }

        /// <summary>
        /// Reads/sets the expected input signal wavelength in nanometres
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                // Read configured wavelength in metres, convert to nm and return
                string resp = chassisQuery(commandPowStem + "WAVE?");
                return Convert.ToDouble(resp) * 1e9;
            }
            set
            {
                // The meter can understand NM, so let it deal with the conversion...
                string command = String.Format("{0}WAVE {1}NM", commandPowStem, value);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Get / Set calibration offset in decibels.
        /// </summary>
        public override double CalOffset_dB
        {
            get
            {
                // Read configured offset
                string resp = chassisQuery(commandStem + "CORR?");
                return Convert.ToDouble(resp);
            }
            set
            {
                string command = String.Format("{0}CORR {1}DB", commandStem, value);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Get/Set reference power for relative measurements. Uses the 
        /// current MeterMode for units.
        /// MeterMode.Absolute_dBm -> dBm
        /// MeterMode.Absolute_mW  -> mW
        /// MeterMode.Relative_dB  -> dBm
        /// </summary>
        public override double ReferencePower
        {
            get
            {
                // read the power
                string resp = chassisQuery(commandPowStem + "REF? TOREF");
                double respValue = Convert.ToDouble(resp);
                if (this.currMode == MeterMode.Absolute_mW)
                {
                    // will have returned power in Watts, so transform
                    return respValue * 1e3;
                }
                // value is in dBm as expected
                else return respValue;
            }
            set
            {
                string unit;
                switch (this.currMode)
                {
                    case MeterMode.Absolute_dBm:
                    case MeterMode.Relative_dB:
                        unit = "DBM";
                        break;
                    case MeterMode.Absolute_mW:
                        unit = "MW";
                        break;
                    default:
                        throw new InstrumentException("Invalid power meter mode: " + this.currMode.ToString());
                }


                string command = String.Format("{0}REF TOREF,{1}{2}", commandPowStem, value, unit);
                chassisWrite(command);
            }
        }

        /// <summary>
        /// Sets/Returns the measurement range. Units depend on current mode.
        /// The range method guarantees to set a power at or *above* that specified in this function.
        /// I.e. the power level specified in the set is guaranteed to be measureable in this mode.        
        /// </summary>
        /// <remarks>N.B.: Manual ranging is independent of channel!</remarks>
        public override double Range
        {
            get
            {
                string resp;
                // check is autorange                
                if (inAutoRangeMode()) return AutoRange;

                // not auto-range - query actual range
                resp = chassisQuery(commandPowStem + "RANG?");
                double range_dBm = Convert.ToDouble(resp);
                // return dependent on current mode
                if (this.currMode == MeterMode.Absolute_mW)
                {
                    return Inst_Ag8153_OpticalPowerMeter.Convert_dBmtomW(range_dBm);
                }
                else return range_dBm;
            }
            set
            {
                string command;

                bool inAutoRange = inAutoRangeMode();
                // are we being asked to change into AutoRange mode?
                if (IsAutoRange(value))
                {
                    // only set to autorange if isn't already
                    if (!inAutoRange)
                    {
                        // auto-range only set on MASTER channel
                        command = String.Format(":SENS{0}:POW:RANG:AUTO ON",
                            this.Slot);
                        chassisWrite(command);
                    }
                }
                else
                {
                    // switch off autorange if necessary
                    if (inAutoRange)
                    {
                        // auto-range only set on MASTER channel
                        command = String.Format(":SENS{0}:POW:RANG:AUTO OFF",
                            this.Slot);
                        chassisWrite(command);
                    }
                    double setValue_dBm;
                    // set value must be sent in dBm, whatever the current mode
                    if (Mode == MeterMode.Absolute_mW)
                    {
                        setValue_dBm = Inst_Ag8153_OpticalPowerMeter.Convert_mWtodBm(value);
                    }
                    else setValue_dBm = value;

                    // 8153? Power Meters ROUND the value, don't take the next highest,
                    // so have to "unround" it by adding half the range. Luckily the ranges
                    // are all 10 dB apart, so add 5 dB to force the next range higher up...

                    // Check in driver that range isn't too high
                    if (setValue_dBm > this.maxPowerRange_dBm)
                    {
                        string errStr = String.Format("Power range set out-of-range (too high): {0} ({1} dBm)",
                            value, setValue_dBm);
                        throw new InstrumentException(errStr);
                    }

                    // Do the rounding, checking for high end condition
                    if (setValue_dBm > this.maxPowerRange_dBm - 5)
                    {
                        setValue_dBm = this.maxPowerRange_dBm;
                    }
                    else setValue_dBm = setValue_dBm + 5;

                    // send new range - manual range independently set PER channel
                    command = String.Format("{0}RANG {1}", commandPowStem, setValue_dBm);
                    chassisWrite(command);
                }
            }
        }

        /// <summary>
        /// Sets/returns the measurement averaging value in seconds for the power meter channel.
        /// </summary>
        /// <remarks>
        /// The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override double AveragingTime_s
        {
            get
            {
                // Read averaging time FROM CHANNEL 1 (MASTER)
                string command = String.Format(":SENS{0}:POW:ATIME?", this.Slot);
                string resp = chassisQuery(command);
                return Convert.ToDouble(resp);
            }
            set
            {
                // Sets the averaging time IN CHANNEL 1 (MASTER)
                string command = String.Format(":SENS{0}:POW:ATIME {1}", this.Slot
                    , value);
                chassisWrite(command);
            }
        }

        
        /// <summary>
        /// Initiate and return a power measurement. Returned units depend 
        /// on the current mode of the instrument (dBm, mW, dB). 
        /// </summary>
        public override double ReadPower()
        {
            // fetch the reading from this channel
            string command = String.Format("READ{0}:POW?", this.Slot);
            double pow = getPowerReadingAndCheckInRange(command);
            if (safeMode)
            {
                int tryCounter = 1;
                bool retryFlag;
                do
                {
                    retryFlag = instrumentChassis.HandleError(command, this, tryCounter);
                    tryCounter++;

                    if (retryFlag)
                    {
                        pow = getPowerReadingAndCheckInRange(command);
                    }
                } while (retryFlag);                
            }
            return pow;
        }

        /// <summary>
        /// Starts a calibration of power meter dark-current 
        /// value on the power meter channel. For this calibration to succeed the meter must 
        /// have zero light input. This function returns immediately once the command
        /// has been sent to the instrument (Async command).
        /// </summary>
        /// <remarks>The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override void ZeroDarkCurrent_Start()
        {
            string command;
           

            // start the zeroing
            command = String.Format(":SENS{0}:CORR:COLL:ZERO", this.Slot);
            this.instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Ends a calibration of optical dark-current. Waits for a previously started
        /// dark current cal to complete.
        /// </summary>
        /// <remarks>
        /// N.B.: The same value affects both channel 1 and 2, so sets may have 
        /// unforeseen consequences</remarks>
        public override void ZeroDarkCurrent_End()
        {
            // allow a timeout of 45 seconds for this very long operation
            int timeoutInit = instrumentChassis.Timeout_ms;
            instrumentChassis.Timeout_ms = 45000;
            // wait for pending operations
            instrumentChassis.WaitForOperationComplete();
            // reset timeout to normal
            instrumentChassis.Timeout_ms = timeoutInit;

            string command;
            string resp;

            // check the zero query
            command = String.Format(":SENS{0}:CORR:COLL:ZERO?", this.Slot);
            resp = chassisQuery(command);
            if (resp != "+0")
            {
                throw new InstrumentException("Zeroing operation failed, returned: " + resp);
            }
        }

        /// <summary>
        /// Get/Set Enable/Disable Min/Max hold function.
        /// </summary>
        public override bool MaxMinOn
        {
            get
            {
                throw new InstrumentException("This propery is not implmented.");
            }

            set
            {
                throw new InstrumentException("This propery is not implmented.");
            }
        }


        /// <summary>
        /// Return  the max and min power readings observed since the last activation 
        /// of Max/Min Hold Mode.
        /// </summary>
        public override InstType_OpticalPowerMeter.PowerMaxMin ReadPowerMaxMin()
        {
            throw new InstrumentException("This method is not implmented.");
        }


        #endregion

        #region 8153 Power Meter Specific functions
        /// <summary>
        /// SafeMode flag. If true, instrument will always wait for completion after every command and check
        /// the error registers. If false it will do neither.
        /// </summary>
        public bool SafeMode
        {
            get
            {
                return this.safeMode;
            }
            set
            {
                safeMode = value;
            }
        }



        #endregion

        #region Helper functions

        /// <summary>
        /// Command to get the power meter reading and check if it is in range
        /// </summary>
        /// <param name="command">GPIB command to get the power meter reading</param>
        /// <returns>The reading</returns>
        private double getPowerReadingAndCheckInRange(string command)
        {
            // manual check of errors as "negative" powers seen at zero low light levels mess
            // up the error registers. Also overload condition causes errors - in
            // fact these two conditions are pretty much indistinguishable.
            string resp = this.instrumentChassis.Query_Unchecked(command, this);
            if ((resp == "+3.40282300E+038") || (resp == "+9.91000000E+037") || (resp == "-1.04429397E+002"))
            {    // have a problem - underrange in dB mode or range overload.
                // clear the standard registers

                chassisWrite("*CLS");
                return OutOfRange;
            }
            double dbl = Convert.ToDouble(resp);
            if (this.currMode == MeterMode.Absolute_mW)
            {
                return dbl * 1e3;
            }
            else return dbl;
        }

        
        
        /// <summary>
        /// put this entire slot into non continuous mode
        /// </summary>
        private void SetContinuousModeOff()
        {
            string cmd = String.Format("INIT{0}:CONT OFF", this.Slot);
            chassisWrite(cmd);
        }

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            if (safeMode)
            {
                this.instrumentChassis.Write(command, this);
            }
            else
            {
                // if not in safe mode commands are async and no error checking
                this.instrumentChassis.Write(command, this, true, false);
            }
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            // Note: if not in safe mode, it won't check for errors
            return this.instrumentChassis.Query(command, this, safeMode);
        }

        /// <summary>
        /// Is the instrument in autoranging mode
        /// </summary>
        /// <returns>true if autoranging, false otherwise</returns>
        private bool inAutoRangeMode()
        {
            string command = String.Format(":SENS{0}:POW:RANG:AUTO?", this.Slot);
            string resp = chassisQuery(command);
            if (resp == trueStr) return true;
            else if (resp == falseStr) return false;
            else throw new InstrumentException("Bad return from command '" +
                command + "' : " + resp);
        }

        /// <summary>
        /// Check that the slot isn't empty, and return the product id string. This may happen before errors setup, so 
        /// DON'T called _Checked Chassis methods.
        /// </summary>
        private string checkSlotNotEmpty()
        {
           
            // All the firmware is in the plugin slot, not the head
            string command = String.Format("*OPT?");

            string resp = this.instrumentChassis.Query_Unchecked(command, this);

            string slotProductNoID = resp.Split(',')[(Convert.ToInt16(this.Slot))-1];
            if (slotProductNoID == "EMPTY")
            {
                throw new InstrumentException("Slot " + this.Slot + " is empty!");
            }
            
            return (slotProductNoID);
        }

      

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag8153 instrumentChassis;

        /// <summary>
        /// Current meter mode state cache
        /// </summary>
        private MeterMode currMode;

        private bool safeMode;

        /// <summary>
        /// Maximum power range supported by this Chassis
        /// </summary>
        private double maxPowerRange_dBm;

        // Power unit constants
        private const string powUnit_dBm = "+0";
        private const string powUnit_W = "+1";

        // Bool values
        private const string falseStr = "+0";
        private const string trueStr = "+1";

        // Common command stem ":SENS{0}:CHAN{1}:" for most commands
        private string commandStem;
        // Common command stem ":SENS{0}:CHAN{1}:POW:" for most power commands
        private string commandPowStem;

        #endregion
    }

}

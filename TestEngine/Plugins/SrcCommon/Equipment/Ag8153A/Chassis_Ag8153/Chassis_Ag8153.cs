// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ag8153.cs
//
// Author: kpillar, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using NationalInstruments.VisaNS;


namespace Bookham.TestLibrary.ChassisNS
{
    
    /// <summary>
    /// This is the chassis class for the agilent 8153 optical power meter chassis
    /// </summary>
    public class Chassis_Ag8153 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="visaResourceString">VISA resource string for communicating with the chassis</param>
        public Chassis_Ag8153(string chassisNameInit, string driverNameInit,
            string visaResourceString)
            : base(chassisNameInit, driverNameInit, visaResourceString)
        {
            // Setup expected valid hardware variants 
            // Add Ag8153 & B details
            ChassisDataRecord ag8153AData = new ChassisDataRecord(
                "HEWLETT-PACKARD 8153A",			// hardware name 
                "0",								// minimum valid firmware version 
                "2.1");					// maximum valid firmware version 
            ValidHardwareData.Add("Ag8153", ag8153AData);

           
            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // NOTHING TO DO
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    //reset to a default state
                    this.Write("*RST", null);   

                    // Set up Chassis standard event register mask
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    // clear the status registers
                    this.Write("*CLS", null);
                }
            }
        }

        /// <summary>
        /// Query the status byte register and return true if an error 
        /// condition is signalled.
        /// </summary>
        /// <returns>True if error, false if none</returns>
        bool isErrorConditionInStatusByte()
        {
            StatusByteFlags statusByte;

            statusByte = this.StatusByte488_2_Command;


            if (this.IsStandardEvent(statusByte)) return true;
            else return false;
        }

        
        /// <summary>
        /// Do we have the error code that causes us trouble
        /// </summary>
        /// <param name="command">our command that we sent</param>
        /// <param name="i">our instrument</param>
        /// <param name="tryCounter">the count of how many times we've been here before</param>
        /// <returns>True if we have a -410 error</returns>
        public bool HandleError(string command, Instrument i, int tryCounter)
        {
            if (isErrorConditionInStatusByte())
            {
                byte standardEvent = this.StandardEventRegister;

                // collate information about the error from the chassis and throw 
                // a ChassisException
                string systErr = this.Query_Unchecked("SYST:ERR?", null);
                //There is a timing issue with this unit and as a result we sometimes see
                // err -410
                if (systErr == "-410,\"\"")
                {
                    // clear status flags
                    this.Write_Unchecked("*CLS", i);
                    // wait for 50 ms
                    System.Threading.Thread.Sleep(50);
                    if (tryCounter >= 3)
                    {
                        throw new ChassisException("Command '" + command + "' failed: SYST:ERR returned -410 error 3 times in a row!");
                    }
                    return (true);
                }
                else
                {
                    string errStr = String.Format("Command '{0}' failed\n" +
                        "'*ESR?' returned '{1}'\n" + 
                        "'SYST:ERR' responded '{2}'", command, standardEvent, systErr);
                    throw new ChassisException(errStr);
                }
            }
            return (false);
        }

        /// <summary>
        /// Writes a command to the chassis and will wait for completion and check status
        /// byte and other registers for error conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <remarks>Uses the chassis' default asynchronous set and "no error check" setting</remarks>
        public new void Write(string command, Instrument i)
        {
            this.Write(command, i, false, true);
        }

        /// <summary>
        /// Writes a command to the chassis. Dependent on options given this will wait 
        /// for completion and check status
        /// byte and other registers for error conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <param name="asyncSet">Set asynchronously, therefore also don't check for errors</param>
        /// <param name="errorCheck">If async set to false, then this parameter determines
        /// whether errors are checked for</param>
        public new void Write(string command, Instrument i, bool asyncSet, bool errorCheck)
        {
            bool retryFlag;
            int tryCounter = 1;

            this.Write_Unchecked(command, i);
            if (asyncSet) return;
            this.WaitForOperationComplete();
            /* Optionally check for errors. */
            if (errorCheck)
            {
                do
                {
                    retryFlag = HandleError(command, i, tryCounter);
                    tryCounter++;
                    if (retryFlag)
                    {
                        this.Write_Unchecked(command, i);            
                        this.WaitForOperationComplete();                        
                    }                    
                }
                while (retryFlag);
            }
        }

        
        /// <summary>
        /// Sends a command to the chassis and waits for a response. Checks status
        /// byte and other registers for error conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <returns>string response</returns>
        public new string Query(string command, Instrument i)
        {
            return Query(command, i, true);
        }

        /// <summary>
        /// Sends a command to the chassis and waits for a response. Dependent on 
        /// arguments, will check the status byte and other registers for error 
        /// conditions.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <param name="i">Which instrument is this from?</param>
        /// <param name="errorCheck">Don't check for errors once value returns</param>
        /// <returns>string response</returns>
        public new string Query(string command, Instrument i, bool errorCheck)
        {
            bool retryFlag;
            int tryCounter = 1;

            string resp = this.Query_Unchecked(command, i); 
            /* Optionally check for errors. */
            if (errorCheck)
            {
                do
                {
                    retryFlag = HandleError(command, i, tryCounter);
                    tryCounter++;

                    if (retryFlag)
                    {
                        resp = this.Query_Unchecked(command, i);                         
                    }                    
                }
                while (retryFlag);                
            }
            return resp;
        }        
        
        #endregion
    }
}

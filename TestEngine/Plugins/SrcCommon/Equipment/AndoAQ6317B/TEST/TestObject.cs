using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace TEST
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region AndoAq6317B Private data for use during test.
        private Chassis_ANDOAQ6317B AQ6317BChassis;
        private Instrument_ANDOAQ6317B AQ6317BInstr;
        #endregion

        #region AndoAq6317B Constants for using during test.
        const string Aq6317BChasName = "Aq6317BChas";
        const string Aq6317BChasDriverName = "Chassis_ANDOAQ6317B";
        const string Aq6317BVisaRes = "GPIB0::1::INSTR";
        const string Aq6317BInstrName = "AQ6317B";
        const string Aq6317BInstrDriverName = "Instrument_ANDOAQ6317B";
        #endregion

        /// <summary>
        /// 
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");
            Setup_AQ6317B();
        }

        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void TestSets()
        {
            AQ6317B_TestSets();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestFixtureTearDown]
        public void ShutDown()
        {
            TestOutput("\n\n*** ShutDown ***");
            ShutDown_AQ6317B();

            // Test end
            TestOutput("Test Finished!");
        }

        #region Setup Methods
        private void Setup_AQ6317B()
        {
            AQ6317BChassis = new Chassis_ANDOAQ6317B(Aq6317BChasName, Aq6317BChasDriverName, Aq6317BVisaRes);
            TestOutput(AQ6317BChassis, "Created OK");

            AQ6317BInstr = new Instrument_ANDOAQ6317B(Aq6317BInstrName, Aq6317BInstrDriverName, "", "", AQ6317BChassis);
            TestOutput(AQ6317BInstr, "Created OK");

            AQ6317BChassis.IsOnline = true;
            AQ6317BChassis.EnableLogging = true;
            TestOutput(AQ6317BChassis, "IsOnline set true OK");

            AQ6317BInstr.IsOnline = true;
            AQ6317BInstr.EnableLogging = true;
            TestOutput(AQ6317BInstr, "IsOnline set true OK");
        }
        #endregion

        #region ShutDown methods
        private void ShutDown_AQ6317B()
        {
            AQ6317BInstr.IsOnline = false;
            TestOutput(AQ6317BInstr, "ShutDown OK");
            AQ6317BChassis.IsOnline = false;
            TestOutput(AQ6317BChassis, "ShutDown OK");
        }
        #endregion

        #region Testsets methods
        private void AQ6317B_TestSets()
        {
            //AQ6317B_FirstTest();
            //AQ6317B_SecondTest();
            //AQ6317B_ThirdTest();
            //test four for NF function only nemo 19 June 2008
            //AQ6317B_ForthTest();
            AQ6317B_FifthTest();
        }
        #endregion

        #region AQ6317B test methods
        private void AQ6317B_FirstTest()
        {
            TestOutput("\n\n*** AQ6317B_FirstTest ***");
            AQ6317BInstr.SetDefaultState();
            double wlbgn = AQ6317BInstr.WavelengthStart_nm;
            TestOutput("WavelengthStart_nm", wlbgn.ToString());
            Assert.AreEqual(1525, wlbgn);
            double wlend = AQ6317BInstr.WavelengthStop_nm;
            TestOutput("WavelengthStop_nm", wlend.ToString());
            Assert.AreEqual(1565, wlend);
            double wlctr = AQ6317BInstr.WavelengthCentre_nm;
            TestOutput("WavelengthCentre_nm", wlctr.ToString());
            Assert.AreEqual(1545, wlctr);
            double wlspn = AQ6317BInstr.WavelengthSpan_nm;
            TestOutput("WavelengthSpan_nm", wlspn.ToString());
            Assert.AreEqual(40, wlspn);
            double reflv = AQ6317BInstr.ReferenceLevel_dBm;
            TestOutput("ReferenceLevel_dBm", reflv.ToString());
            Assert.AreEqual(10, reflv);
            double apdiv = AQ6317BInstr.Amplitude_dBperDiv;
            TestOutput("Amplitude_dBperDiv", apdiv.ToString());
            Assert.AreEqual(10, apdiv);
            int trpts = AQ6317BInstr.TracePoints;
            TestOutput("TracePoints", trpts.ToString());
            Assert.AreEqual(2001, trpts);
            int vdavg = AQ6317BInstr.VideoAverage;
            TestOutput("VideoAverage", vdavg.ToString());
            Assert.AreEqual(10, vdavg);
            double rebdwd = AQ6317BInstr.ResolutionBandwidth;
            TestOutput("ResolutionBandwidth", rebdwd.ToString());
            Assert.AreEqual(0.2, rebdwd);
            Bookham.TestLibrary.InstrTypes.InstType_OSA.SweepModes spmd = AQ6317BInstr.SweepMode;
            TestOutput("SweepMode", spmd.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.SweepModes.Single, spmd);
            Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState sta = AQ6317BInstr.Status;
            TestOutput("Status", sta.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy, sta);
        }

        private void AQ6317B_SecondTest()
        {
            TestOutput("\n\n*** AQ6317B_SecondTest ***");
            AQ6317BInstr.SweepStart();
            Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState sta = AQ6317BInstr.Status;
            TestOutput("Status", sta.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy, sta);
            AQ6317BInstr.SweepStop();
            sta = AQ6317BInstr.Status;
            TestOutput("Status", sta.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Idle, sta);
        }

        private void AQ6317B_ThirdTest()
        {
            TestOutput("\n\n*** AQ6317B_ThirdTest ***");
            AQ6317BInstr.SweepStart();
            Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState sta = AQ6317BInstr.Status;
            TestOutput("Status", sta.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy, sta);

            AQ6317BInstr.MarkerOn = true;
            TestOutput("MarkerOn", AQ6317BInstr.MarkerOn.ToString());
            Assert.AreEqual(true, AQ6317BInstr.MarkerOn);
            double mkwl = AQ6317BInstr.MarkerWavelength_nm;
            TestOutput("MarkerWavelength_nm", mkwl.ToString());
            double mkapd = AQ6317BInstr.MarkerAmplitude_dBm;
            TestOutput("MarkerAmplitude_dBm", mkapd.ToString());

            AQ6317BInstr.MarkerNID(1);
            double mkNIDwl = AQ6317BInstr.MarkerNIDWavelength_nm(1);
            TestOutput("MarkerNIDWavelength_nm", mkNIDwl.ToString());
            Assert.AreEqual(mkwl, mkNIDwl);
            double mkNIDapd = AQ6317BInstr.MarkerNIDAmplitude_dBm(1);
            TestOutput("MarkerNIDAmplitude_dBm", mkNIDapd.ToString());
            Assert.AreEqual(mkapd, mkNIDapd);

            double tmpmkwl_l = mkwl - 0.02;
            AQ6317BInstr.MarkerWavelength_nm = tmpmkwl_l;
            mkwl = AQ6317BInstr.MarkerWavelength_nm;
            TestOutput("MarkerWavelength_nm", mkwl.ToString());
            Assert.AreEqual(tmpmkwl_l, mkwl);
            mkapd = AQ6317BInstr.MarkerAmplitude_dBm;
            TestOutput("MarkerAmplitude_dBm", mkapd.ToString());
            AQ6317BInstr.MarkerNID(2);
            mkNIDwl = AQ6317BInstr.MarkerNIDWavelength_nm(2);
            TestOutput("MarkerNIDWavelength_nm", mkNIDwl.ToString());
            Assert.AreEqual(mkwl, mkNIDwl);
            mkNIDapd = AQ6317BInstr.MarkerNIDAmplitude_dBm(2);
            TestOutput("MarkerNIDAmplitude_dBm", mkNIDapd.ToString());
            Assert.AreEqual(mkapd, mkNIDapd);

            double tmpmkwl_r = mkwl + 0.04;
            AQ6317BInstr.MarkerWavelength_nm = tmpmkwl_r;
            mkwl = AQ6317BInstr.MarkerWavelength_nm;
            TestOutput("MarkerWavelength_nm", mkwl.ToString());
            Assert.AreEqual(tmpmkwl_r, mkwl);
            mkapd = AQ6317BInstr.MarkerAmplitude_dBm;
            TestOutput("MarkerAmplitude_dBm", mkapd.ToString());
            AQ6317BInstr.MarkerNID(3);
            mkNIDwl = AQ6317BInstr.MarkerNIDWavelength_nm(3);
            TestOutput("MarkerNIDWavelength_nm", mkNIDwl.ToString());
            Assert.AreEqual(mkwl, mkNIDwl);
            mkNIDapd = AQ6317BInstr.MarkerNIDAmplitude_dBm(3);
            TestOutput("MarkerNIDAmplitude_dBm", mkNIDapd.ToString());
            Assert.AreEqual(mkapd, mkNIDapd);
            

            AQ6317BInstr.MarkerOn = false;
            TestOutput("MarkerOn", AQ6317BInstr.MarkerOn.ToString());
            Assert.AreEqual(false, AQ6317BInstr.MarkerOn);

            AQ6317BInstr.SweepStop();
            sta = AQ6317BInstr.Status;
            TestOutput("Status", sta.ToString());
            Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Idle, sta);
        }
        #endregion

        #region AQ6317B_ForthTest the test should be performed individual
        private void AQ6317B_ForthTest()
        {
            TestOutput("\n\n*** AQ6317B_forthTest NF test***");
            AQ6317BInstr.SetDefaultState();
            AQ6317BInstr.WavelengthStart_nm =1540;
            //TestOutput("WavelengthStart_nm", wlbgn.ToString());
            //Assert.AreEqual(1525, wlbgn);
            AQ6317BInstr.WavelengthStop_nm=1550;
            //TestOutput("WavelengthStop_nm", wlend.ToString());
            //Assert.AreEqual(1565, wlend);
            //double wlctr = AQ6317BInstr.WavelengthCentre_nm;
            //TestOutput("WavelengthCentre_nm", wlctr.ToString());
            //Assert.AreEqual(1545, wlctr);
            //double wlspn = AQ6317BInstr.WavelengthSpan_nm;
            //TestOutput("WavelengthSpan_nm", wlspn.ToString());
            //Assert.AreEqual(40, wlspn);
            //double reflv = AQ6317BInstr.ReferenceLevel_dBm;
            //TestOutput("ReferenceLevel_dBm", reflv.ToString());
            //Assert.AreEqual(10, reflv);
            //double apdiv = AQ6317BInstr.Amplitude_dBperDiv;
            //TestOutput("Amplitude_dBperDiv", apdiv.ToString());
            //Assert.AreEqual(10, apdiv);
            //int trpts = AQ6317BInstr.TracePoints;
            //TestOutput("TracePoints", trpts.ToString());
            //Assert.AreEqual(2001, trpts);
            //int vdavg = AQ6317BInstr.VideoAverage;
            //TestOutput("VideoAverage", vdavg.ToString());
            //Assert.AreEqual(10, vdavg);
            //double rebdwd = AQ6317BInstr.ResolutionBandwidth;
            //TestOutput("ResolutionBandwidth", rebdwd.ToString());
            //Assert.AreEqual(0.2, rebdwd);
            //Bookham.TestLibrary.InstrTypes.InstType_OSA.SweepModes spmd = AQ6317BInstr.SweepMode;
            //TestOutput("SweepMode", spmd.ToString());
            //Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.SweepModes.Single, spmd);
            //Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState sta = AQ6317BInstr.Status;
            //TestOutput("Status", sta.ToString());
            //Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy, sta);
            /*
             * method to test edfa NF 
             * 1. active trace A
             * 2. set trace A to write
             * 3. sweep input
             * 4. set trace A to fix
             * 5. active trace B
             * 6. set trace B to write
             * 7. sweep output
             * 8. set trace B to fix
             * 9. set analysis mode to EDNF
             * 10.return analysis result
             */
            //AQ6317BInstr.activeTrace(Instrument_ANDOAQ6317B.TraceName.TrancA);
            AQ6317BInstr.SetTraceToWrite(Instrument_ANDOAQ6317B.TraceName.TraceA);
            
            //!!!!!!!!!!!!!!!!!!!!!break here need to switch the optical path to set input to OSA!!!!!!!!!!!!!

            AQ6317BInstr.SweepStart();
            do
            {
                //Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Idle, sta);
                System.Threading.Thread.Sleep(500);
            } while (AQ6317BInstr.Status == Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy);
            AQ6317BInstr.SetTraceToFix(Instrument_ANDOAQ6317B.TraceName.TraceA);

            //!!!!!!!!!!!!!!!!!!!!!break here need to switch the optical path to set output to OSA!!!!!!!!!!!!!

            //AQ6317BInstr.activeTrace(Instrument_ANDOAQ6317B.TraceName.TrancB);
            AQ6317BInstr.SetTraceToWrite(Instrument_ANDOAQ6317B.TraceName.TraceB);
            AQ6317BInstr.SweepStart();
            do
            {
                //Assert.AreEqual(Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Idle, sta);
                System.Threading.Thread.Sleep(500);
            } while (AQ6317BInstr.Status == Bookham.TestLibrary.InstrTypes.InstType_OSA.ChannelState.Busy) ;
            AQ6317BInstr.SetTraceToFix(Instrument_ANDOAQ6317B.TraceName.TraceB);
            //set parameters
            AQ6317BInstr.setOffInpOfNfAna(7.0);
            AQ6317BInstr.setOffOutOfNfAna(6.7);
            AQ6317BInstr.setMinusMskOfNfAna(.5);
            AQ6317BInstr.setPlusMskOfNfAna(.5);
            AQ6317BInstr.setCvftTypeOfNfAna(Instrument_ANDOAQ6317B.CvftType.GAUSSIAN);
            AQ6317BInstr.setCvstThrOfNfAna(.1);
            AQ6317BInstr.setAnalysisMode("EDNF");
            TestOutput("analysis result",AQ6317BInstr.getAnalysisResult() );

        }

        private void AQ6317B_FifthTest()
        {
            TestOutput("\n\n*** AQ6317B_FifthTest NF test***");
            AQ6317BInstr.activeTrace(Instrument_ANDOAQ6317B.TraceName.TraceA);
            InstType_OSA.OptPowerPoint[] ATrace = AQ6317BInstr.GetDisplayTrace;
            AQ6317BInstr.activeTrace(Instrument_ANDOAQ6317B.TraceName.TraceB);
            InstType_OSA.OptPowerPoint[] BTrace = AQ6317BInstr.GetDisplayTrace;
            AQ6317BInstr.activeTrace(Instrument_ANDOAQ6317B.TraceName.TraceC);
            InstType_OSA.OptPowerPoint[] CTrace = AQ6317BInstr.GetDisplayTrace;
        }
        #endregion
        /*public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }*/


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(string para1, string para2)
        {
            string outputStr = String.Format("{0}: {1}", para1, para2);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

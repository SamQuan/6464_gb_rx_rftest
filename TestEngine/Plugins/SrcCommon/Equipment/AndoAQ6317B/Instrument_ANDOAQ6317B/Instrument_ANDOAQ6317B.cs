// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument_ANDOAQ6317B.cs
//
// Author: koonlin.peng, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// TODO: 
    /// 1. Add reference to the Instrument type you are implementing.
    /// 2. Add reference to the Chassis type you are connecting via.
    /// 3. Change the base class to the type of instrument you are implementing.
    /// 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    /// 5. Fill in the gaps.
    /// </summary>
    public class Instrument_ANDOAQ6317B : InstType_OSA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instrument_ANDOAQ6317B(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "AQ6317B",				// hardware name 
                "0",  			// minimum valid firmware version 
                "MR02.15 OR02.14");			// maximum valid firmware version 
            ValidHardwareData.Add("AQ6317B", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_ANDOAQ6317B",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("AQ6317B", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_ANDOAQ6317B)chassisInit;
        }
        #endregion

        #region Instrument overrides
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = instrumentChassis.Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                string strFirmwareVer = idn.Split(',')[3].Trim();

                //Log Event
                LogEvent("'FirmwareVersion' returned : " + strFirmwareVer);

                return strFirmwareVer;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                string strHID = idn[1];
                
                //Log event
                LogEvent("'HardwareIdentity' returned : " + strHID);
                return strHID;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send reset command to instrument
            //this.Reset(); mqn taken out as it takes too long and it may reset auto align and mess with the spectral rbw
            // set scan area to C-Band
            this.WavelengthSpan_nm = 40;
            this.WavelengthStart_nm = 1525;
            this.WavelengthStop_nm = 1565;
            this.WavelengthCentre_nm = 1545;
            this.ReferenceLevel_dBm = 10;
            this.Amplitude_dBperDiv = 10;
            this.TracePoints = 2001;
            this.VideoAverage = 10;
            this.ResolutionBandwidth = 0.2;
            this.SweepMode = SweepModes.Single;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void AutoAlign()
        {
            // Set instrument to power analysis
            this.instrumentChassis.Write_Unchecked("PWR", this);
            //this.instrumentChassis.Write_Unchecked("ACT0", this);
            this.instrumentChassis.Write_Unchecked("WRTA", this);
            //wait 1000ms for effecting
            System.Threading.Thread.Sleep(1000);
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool AutoRange
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        #endregion

        #region OSA InstrumentType property overrides
        /// <summary>
        /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthStart_nm
        {
            get
            {
                string command = "STAWL?";
                return System.Convert.ToDouble(instrumentChassis.Query_Unchecked(command, this));
            }
            set
            {
                string command = "STAWL" + Strings.Format(value, "0000.00");
                instrumentChassis.Write_Unchecked(command, this);

                //wait 1000ms for effecting.
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1800nm.
        /// </value>
        public override double WavelengthStop_nm
        {
            get
            {
                string command = "STPWL?";
                return System.Convert.ToDouble(instrumentChassis.Query_Unchecked(command, this));
            }
            set
            {
                string command = "STPWL" + Strings.Format(value, "0000.00");
                instrumentChassis.Write_Unchecked(command, this);

                //wait 1000ms for effecting.
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Reads/sets the centre wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthCentre_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("CTRWL?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 600 || value > 1750)
                {
                    throw new InstrumentException("Wavelength Centre must be between 600 and 1750 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked("CTRWL " + value, this);

                    // Wait 1000ms for effecting.
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength span for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 0.2 and 1200nm.
        /// </value>
        public override double WavelengthSpan_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("SPAN?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0.2 || value > 1200)
                {
                    throw new InstrumentException("Wavelength Span must be between 0.2 and 1200 nm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked("SPAN " + value, this);

                    // Wait 1000ms for effecting.
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Reads/sets the wavelength offset for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -1 and +1nm.
        /// </value>
        public override double WavelengthOffset_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("LCALT?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -1 || value > 1)
                {
                    throw new InstrumentException("Wavelength Offset must be between -1.0 and +1.0 nm. Set value = "
                                               + value);
                }
                else
                {
                    string command = "LCALT " + WavelengthCentre_nm.ToString() + ";" + value;
                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked(command, this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the reference level for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -90 and +20 dBm.
        /// </value>
        public override double ReferenceLevel_dBm
        {
            get
            {
                string command = "REFL?";
                return System.Convert.ToDouble(instrumentChassis.Query_Unchecked(command, this));
            }
            set
            {
                string command = "REFL" + Strings.Format(value, "000.0");
                instrumentChassis.Write_Unchecked(command, this);

                //wait 1000ms for effecting
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
        /// </summary>
        /// <value>
        /// Set value has to be between 0.1 and 10 db/Div.
        /// </value>
        public override double Amplitude_dBperDiv
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("LSCL?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 0.1 || value > 10)
                {
                    throw new InstrumentException("Amplitude scale must be be between 0.1 and 10 dB/Div. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked("LSCL " + Strings.Format(value, "00.0"), this);

                    //wait 1000ms for effecting
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Reads/sets the resolution bandwidth for the OSA channel.  
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 0.05, 0.07, 0.1, 0.2, 0.5, and 1 nm.
        /// </value>
        public override double ResolutionBandwidth
        {
            get
            {
                string command = "RESLN?";
                return System.Convert.ToDouble(instrumentChassis.Query_Unchecked(command, this));
            }
            set
            {
                string command = "RESLN" + Strings.Format(value, "0.00");
                instrumentChassis.Write_Unchecked(command, this);

                //wait 1000ms for effecting.
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Reads/sets the state of the internal Attenuator. 
        /// </summary>
        public override bool Attenuator
        {
            get
            {
                return this.bAttenuator;
            }
            set
            {
                bAttenuator = value;
            }
        }

        /// <summary>
        /// Reads/sets the number of trace points for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 51, 101, 201, 501, 1001, 2001, and 5001.
        /// </value>
        public override int TracePoints
        {
            get
            {
                string command = "SMPL?";
                return System.Convert.ToInt32(instrumentChassis.Query_Unchecked(command, this));
            }
            set
            {
                string command = "SMPL" + Strings.Format(value, "0000");
                instrumentChassis.Write_Unchecked(command, this);

                //Wait 1000ms for effecting
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Reads/sets the video averaging value for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int VideoAverage
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("AVG?", this);

                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < 1 || value > 1000)
                {
                    throw new InstrumentException("Video Average must be between 1 and 1000. Set value = "
                                               + value);
                }
                else
                {
                    // convert 0 to OFF
                    string outValue = Convert.ToString(value);

                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked("AVG " + outValue, this);

                    //Wait 1000ms for effecting
                    System.Threading.Thread.Sleep(500);
                }
            }

        }

        /// <summary>
        /// Reads/sets the video bandwidth for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.
        /// </value>
        public override double VideoBandwidth
        {
            get
            {
                string strRet = this.instrumentChassis.Query_Unchecked("SENS?", this);
                switch(strRet)
                {
                    case "3":
                        return 1000000;
                    case "2":
                        return 100000;
                    case "1":
                        return 10000;
                    case "6":
                        return 1000;
                    case "5":
                        return 100;
                    case "4":
                        return 10;
                    default:
                        throw new Exception(string.Format("Error return by query_unchecked:%s", strRet));
                }
            }
            set
            {
                string command = "";

                // Check whether value is in correct range
                if (value <= 10) command = "SNHD";
                else if (value <= 100) command = "SNAT";
                else if (value <= 1000) command = "SMID";
                else if (value <= 10000) command = "SHI1";
                else if (value <= 100000) command = "SHI2";
                else command = "SHI3";

                this.instrumentChassis.Write_Unchecked(command, this);
                System.Threading.Thread.Sleep(500);
            }
        }
        
        /// <summary>
        /// Reads/sets the sweep capture mode for the OSA channel.
        /// </summary>
        /// <value>
        /// Triggered mode is not available.
        /// Unspecified mode cannot be set.
        /// </value>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.SweepModes SweepMode
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("SWEEP?", this);

                // Convert response and return
                InstType_OSA.SweepModes mode = SweepModes.Unspecified;
                switch (Convert.ToInt32(response))
                {
                    case 1: mode = SweepModes.Single;       // Single sweep in progress
                        break;
                    case 2: mode = SweepModes.Continuous;   // Repeat sweep in progress
                        break;
                    //case 3:                                 // Auto sweep
                    //    break;
                }
                return mode;
            }
            set
            {
                // Select command
                string command = "";
                switch (value)
                {
                    case SweepModes.Single: command = "SGL";  // Start single sweep
                        break;
                    case SweepModes.Continuous: command = "RPT";  // Start repeat sweep
                        break;
                    case SweepModes.Unspecified: throw new InstrumentException("Cannot set to 'Unspecified' mode.");
                    case SweepModes.Triggered: throw new InstrumentException("'Triggered' mode not available.");
                }

                // Write the command to the instrument
                instrumentChassis.Write_Unchecked(command, this);

                //wait 1000ms for effecting
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Returns the executional status for the OSA channel. 
        /// </summary>
        /// <value>
        /// Busy is returned when the OSA is in the process of measureing a spectrum.
        /// Idle is returned when no spectrum is measured. Unspecified is returned when OSA is in power
        /// monitor mode. DataReady is not supported.
        /// </value>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.ChannelState Status
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("SWEEP?", this);

                // Convert response and return
                InstType_OSA.ChannelState status = ChannelState.Unspecified;
                switch (Convert.ToInt32(response))
                {
                    case 0: status = ChannelState.Idle; // Not sweeping
                        break;
                    case 1:                             // Single sweep in progress
                    case 2: status = ChannelState.Busy; // Repeat sweep in progress
                        //case 3:  // Auto sweep
                        break;
                }
                return status;
            }
        }
        #endregion

        #region OSA InstrumentType function overrides
        /// <summary>
        /// Starts a measurement sweep, and returns immediately.
        /// </summary>
        /// <remarks>
        /// Status() property should be used to determine sweep progress/completion. 
        /// </remarks>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStop"/>
        public override void SweepStart()
        {
            // Start the sweep
            instrumentChassis.Write_Unchecked("SGL", this);

            //Wait 500ms to effecting
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Stops an active measurement sweep. 
        /// </summary>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        public override void SweepStop()
        {
            // Stop the sweep
            instrumentChassis.Write_Unchecked("STP", this);

            //Wait 1000ms to effecting
            System.Threading.Thread.Sleep(500);
        }
        
        /// <summary>
        /// Shows/hides the marker for the OSA channel. 
        /// </summary>
        /// <remarks>
        /// Since the instrument does not have way to read whether a marker is active or not,
        /// this is handled using a private variable that is set to true every time the marker
        /// activated and false when all markes are erased.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        public override bool MarkerOn
        {
            get
            {
                // return soft buffer
                return markerOn;
            }
            set
            {
                if (value)
                {
                    // Detects the max value of level and set the moving marker.
                    MarkerToPeak();
                }
                else
                {
                    // Erase all markers
                    instrumentChassis.Write_Unchecked("MKCL", this);

                    // remember that marker is now off
                    markerOn = false;

                    // Wait 1000ms for effecting
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Reads/sets the marker wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between the current start and stop wavelengths.
        /// </value>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerWavelength_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query_Unchecked("MKR?", this);

                // extract wavelength from response (wavelength, amplitude)
                string wavelength = response.Split(',')[0];

                // Convert response to double and return
                return Convert.ToDouble(wavelength);
            }
            set
            {
                // Check whether value is in correct range
                if (value < this.WavelengthStart_nm || value > this.WavelengthStop_nm)
                {
                    throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                         " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write_Unchecked("WMKR " + Strings.Format(value, "0000.000"), this);

                    // Wait 1000ms for effecting
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Reads the present marker amplitude for the OSA channel.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the instrument does not return the level in dBm.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerAmplitude_dBm
        {
            get
            {
                // Query the instrument
                string response = instrumentChassis.Query_Unchecked("MKR?", this);

                // extract amplitude from response (wavelength, amplitude)
                string amplitude = response.Split(',')[1];

                // Convert response to double and return
                return Convert.ToDouble(amplitude);
            }
        }

        /// <summary>
        /// Moves the marker to the maximum signal point on the display.
        /// </summary>
        /// <remarks>
        /// Throws anexception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToPeak()
        {
            string command = "PKSR";
            instrumentChassis.Write_Unchecked(command, this);

            // remember that marker is now on
            markerOn = true;

            //wait 1000ms for effecting
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Moves the marker to the minimum signal point on the display. 
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToMinimum()
        {
            string command = "BTSR";
            instrumentChassis.Write_Unchecked(command, this);

            // remember that marker is now on
            markerOn = true;

            //wait 1000ms for effecting
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Moves the marker to the next peak to the left/right on the display.
        /// </summary>
        /// <param name="directionToMove">
        /// Enumeration representing the direction in which to search for the next peak.
        /// If unspecified is selected, the next peak is searched.
        /// </param>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
        {
            string command = "NSR";
            instrumentChassis.Write_Unchecked(command, this);

            // remember that marker is now on
            markerOn = true;

            //wait 1000ms for effecting
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Returns the display trace, as an array of numeric values. 
        /// </summary>
        public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
        {
            get
            {
                // Read wavelength and amplitude values from OSA
                string wlCommand = "";
                string ampCommand = "";
                int numPoints = this.TracePoints;
                InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
                switch (this.currentActTrace)
                {
                    case TraceName.TraceA:
                        {
                            wlCommand = string.Format("WDATA R1-R{0}", numPoints);
                            ampCommand = string.Format("LDATA R1-R{0}", numPoints);
                            break;
                        }
                    case TraceName.TraceB:
                        {
                            wlCommand = string.Format("WDATB R1-R{0}", numPoints);
                            ampCommand = string.Format("LDATB R1-R{0}", numPoints);
                            break;
                        }
                    case TraceName.TraceC:
                        {
                            wlCommand = string.Format("WDATC R1-R{0}", numPoints);
                            ampCommand = string.Format("LDATC R1-R{0}", numPoints);
                            break;
                        }
                    default:
                        throw new InstrumentException(string.Format("Error getTrace={0} value!", this.currentActTrace));
                }

                string wlResponse = this.instrumentChassis.Query_Unchecked(wlCommand, this);
                if (wlResponse.Length == 0)
                    return trace;
                string ampReponse = this.instrumentChassis.Query_Unchecked(ampCommand, this);
                if (ampReponse.Length == 0)
                    return trace;

                string[] wavelengthes = wlResponse.Split(',');
                string[] amplitudes = ampReponse.Split(',');
                if (wavelengthes.Length != amplitudes.Length || wavelengthes.Length > numPoints + 1)
                    return trace;

                for (int index = 0; index < wavelengthes.Length - 1; index++)
                {
                    // values are transferred as 16bit integers and 100 * float value
                    trace[index].wavelength_nm = Math.Round(Convert.ToDouble(wavelengthes[index + 1]), 2);
                    trace[index].power_dB = Math.Round(Convert.ToDouble(amplitudes[index + 1]), 2);
                }

                return trace;
            }
        }

        /// <summary>
        /// Reads/sets the point averaging for the OSA channel.
        /// </summary>
        public override int PointAverage
        {
            get
            {
                return this.pointAverage;
            }
            set
            {
                // Check whether value is in correct range
                if (value <= 0 || value == 1 || value > 1000)
                {
                    throw new InstrumentException("Point Average must be between 2 and 1000 or 0. Set value = "
                                               + value);
                }
                else
                {
                    this.pointAverage = value;
                }
            }
        }
        #endregion

        #region Instrument specific commands
        /// <summary>
        /// assign a marker No. for the current moving marker.
        /// </summary>
        public void MarkerNID(byte NID)
        {
            string command = "MKR " + Strings.Format(NID, "000");
            instrumentChassis.Write_Unchecked(command, this);

            // remember that marker is now on
            markerOn = true;

            // Wait 1000ms for effecting
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// get the wavelength of the marker No.ID.
        /// </summary>
        /// <param name="NID"></param>
        public double MarkerNIDWavelength_nm(byte NID)
        {
            string command = "MKR? " + Strings.Format(NID, "000");
            string response = instrumentChassis.Query_Unchecked(command, this);

            // extract amplitude from response (wavelength, amplitude)
            string wavelength = response.Split(',')[0];

            // Convert response to double and return
            return Convert.ToDouble(wavelength);
        }

        /// <summary>
        /// get the amplitude power value of the marker No.ID.
        /// </summary>
        /// <param name="NID"></param>
        public double MarkerNIDAmplitude_dBm(byte NID)
        {
            string command = "MKR? " + Strings.Format(NID, "000");
            string response = instrumentChassis.Query_Unchecked(command, this);

            // extract amplitude from response (wavelength, amplitude)
            string amplitude = response.Split(',')[1];

            // Convert response to double and return
            return Convert.ToDouble(amplitude);
        }

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        public void Reset()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("*RST", this);

            DateTime startTime = DateTime.Now;
            TimeSpan timeOut = new TimeSpan(0, 1, 0);
            System.Threading.Thread.Sleep(timeOut);

            // remember that marker is now off
            markerOn = false;
        }
        /// <summary>
        /// Set trace to write i.e. tranc A or trace B
        /// </summary>
        public void SetTraceToWrite(TraceName traceName)
        {
           
            switch (traceName)
            {
                case TraceName.TraceA:
                    {
                        instrumentChassis.Write_Unchecked("WRTA", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRA?", this) != "0")
                        {
                            throw new InstrumentException("TraceA write statu setup failed!");
                        }
                    }
                    break;
                case TraceName.TraceB:
                    {
                        instrumentChassis.Write_Unchecked("WRTB", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRB?", this) != "0")
                        {
                            throw new InstrumentException("TraceB write statu setup failed!");
                        }
                    }
                    break;
                case TraceName.TraceC:
                    {

                        instrumentChassis.Write_Unchecked("WRTC", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRC?", this) != "0")
                        {
                            throw new InstrumentException("TraceC write statu setup failed!");
                        }
                    }
                    break;
                default: throw new Exception(string.Format("Trace writhe failed!"));
            }

        }

        /// <summary>
        /// Set trace to fix i.e. tranc A or trace B
        /// </summary>
        public void SetTraceToFix(TraceName traceName)
        {
            switch (traceName)
            {
                case TraceName.TraceA:
                    {
                        instrumentChassis.Write_Unchecked("FIXA", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRA?", this) != "1")
                        {
                            throw new InstrumentException("TraceA FIX statu setup failed!");
                        }
                    }
                    break;
                case TraceName.TraceB:
                    {
                        instrumentChassis.Write_Unchecked("FIXB", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRB?", this) != "1")
                        {
                            throw new InstrumentException("TraceB FIX statu setup failed!");
                        }
                    }
                    break;
                case TraceName.TraceC:
                    {

                        instrumentChassis.Write_Unchecked("FIXC", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("TRC?", this) != "1")
                        {
                            throw new InstrumentException("TraceC FIX statu setup failed!");
                        }
                    }
                    break;
                default: throw new InstrumentException("set traces fix failed!");
            }
               
        }

        /// <summary>
        /// active trace to i.e. tranc A or trace B
        /// </summary>
        public void activeTrace(TraceName traceName)
        {
            switch (traceName)
            {
                case TraceName.TraceA:
                    {
                        instrumentChassis.Write_Unchecked("ACTVA", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("ACTV?", this) != "0")
                        {
                            throw new InstrumentException("set trace A active failed!");
                        }
                    }
                    break;
                case TraceName.TraceB:
                    {
                        instrumentChassis.Write_Unchecked("ACTVB", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("ACTV?", this) != "1")
                        {
                            throw new InstrumentException("set trace A active failed!");
                        }
                    }
                    break;
                case TraceName.TraceC:
                    {
                        instrumentChassis.Write_Unchecked("ACTVC", this);
                        System.Threading.Thread.Sleep(500);
                        if (instrumentChassis.Query_Unchecked("ACTV?", this) != "2")
                        {
                            throw new InstrumentException("set trace A active failed!");
                        }
                    }
                    break;
                default: throw new InstrumentException("set traces to active failed!");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private TraceName currentActTrace
        {
            get
            {
                string response = instrumentChassis.Query_Unchecked("ACTV?", this);
                switch (response)
                {
                    case "0":
                        return TraceName.TraceA;
                    case "1":
                        return TraceName.TraceB;
                    case "2":
                        return TraceName.TraceC;
                    default:
                        return TraceName.Invalid;
                }
            }
        }
            

        /// <summary>
        /// get analysis result
        /// </summary>
        public string getAnalysisResult()
        {

            string response = instrumentChassis.Query_Unchecked("ANA?", this);

            return response;
        }

         /// <summary>
        /// set analysis mode
        /// EDNF for edfa nosie figure
        /// WDWMAN for WDM analysis etc.
        /// </summary>
        public void setAnalysisMode(string mode)
        {

            instrumentChassis.Write_Unchecked(mode.ToUpper(), this);


        }
        #region papameters of NF function
        /// <summary>
        /// parameter set of edfa NF -- offset input
        /// </summary>
        public void setOffInpOfNfAna(double offIn)
        {
            instrumentChassis.Write_Unchecked("OFIN" + Strings.Format(offIn, "000.00"), this);
            //if (Strings.Format((instrumentChassis.Query_Unchecked("OFIN?", this)),"000,00") !=Strings.Format(offIn,"000,00"))
            //{
            //    throw new InstrumentException("set offset input failed!");
            //}
        }

        /// <summary>
        /// parameter set of edfa NF -- offset output
        /// </summary>
        public void setOffOutOfNfAna(double offOut)
        {
            instrumentChassis.Write_Unchecked("OFOUT" + Strings.Format(offOut, "000.00"), this);
            //if (Convert.ToSingle(instrumentChassis.Query_Unchecked("OFOUT?", this)) !=Math.Round( offOut, 2))
            //{
            //    throw new InstrumentException("set offset output failed!");
            //}
        }

        /// <summary>
        /// parameter set of edfa NF -- right side of signal light's peak wavelength
        /// </summary>
        public void setPlusMskOfNfAna(double value)
        {
            instrumentChassis.Write_Unchecked("PLMSK" + Strings.Format(value, "00.00"), this);
            if (Convert.ToSingle(instrumentChassis.Query_Unchecked("PLMSK?", this)) != value)
            {
                throw new InstrumentException("set plus(right) side failed!");
            }
        }

        /// <summary>
        /// parameter set of edfa NF -- left side of signal light's peak wavelength
        /// </summary>
        public void setMinusMskOfNfAna(double value)
        {
            instrumentChassis.Write_Unchecked("MIMSK" + Strings.Format(value, "00.00"), this);
            if (Convert.ToSingle(instrumentChassis.Query_Unchecked("MIMSK?", this)) != value)
            {
                throw new InstrumentException("set minus(left) side failed!");
            }
        }

        /// <summary>
        /// parameter set of edfa NF -- set CVFT TYPE
        /// </summary>
        public void setCvftTypeOfNfAna(CvftType value)
        {
            
            instrumentChassis.Write_Unchecked("EDFCVF" + Convert.ToString((int)value), this);
            if (instrumentChassis.Query_Unchecked("EDFCVF?", this) != Convert.ToString((int)value))
            {
                throw new InstrumentException("set CVFT TYPY failed!");
            }
        }

        /// <summary>
        /// parameter set of edfa NF -- set threshold when ASE curve is approximated
        /// </summary>
        public void setCvstThrOfNfAna(double value)
        {
            instrumentChassis.Write_Unchecked("EDFTH" + Strings.Format(value, "00.0"), this);
            if (Convert.ToSingle(instrumentChassis.Query_Unchecked("EDFTH?", this)) != Convert.ToSingle(value))
            {
                throw new InstrumentException("set CVFT threthold failed!");
            }
        }
        #endregion
        #endregion


        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_ANDOAQ6317B instrumentChassis;

        /// <summary>
        /// Soft marker on/off buffer
        /// </summary>
        private bool markerOn;

        /// <summary>
        /// simulator open or close the Attenuator.
        /// </summary>
        private bool bAttenuator;

        /// <summary>
        /// simulator the number of PointAverage
        /// </summary>
        private int pointAverage;

        /// <summary>
        /// 
        /// </summary>
        public enum TraceName
        {
            /// <summary>
            /// 
            /// </summary>
            TraceA = 0,

            /// <summary>
            /// 
            /// </summary>
            TraceB = 1,

            /// <summary>
            /// 
            /// </summary>
            TraceC = 2,

            /// <summary>
            /// 
            /// </summary>
            Invalid = 3,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum CvftType
        {
            /// <summary>
            /// 
            /// </summary>
            GAUSSIAN = 0,

            /// <summary>
            /// 
            /// </summary>
            LORENZIAN = 1,

            /// <summary>
            /// 
            /// </summary>
            POLY3RD = 2,

            /// <summary>
            /// 
            /// </summary>
            POLY4TH = 3,

            /// <summary>
            /// 
            /// </summary>
            POLY5TH = 4,
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_ANDOAQ6317B.cs
//
// Author: koonlin.peng, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// 1. Change the base class to the type of chassis you are implementing 
    ///   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    ///    based types).
    /// 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    /// 3. Fill in the gaps.
    /// </summary>
    public class Chassis_ANDOAQ6317B : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_ANDOAQ6317B(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisAQ6317B = new ChassisDataRecord(
                "AQ6317B",
                "0",
                "MR02.15 OR02.14");
            ValidHardwareData.Add("AQ6317B", chassisAQ6317B);

            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware verison of this chassis
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[1];
            }
        }

        /// <summary>
        /// Sends the correct string to the chassis to retrieve the error code/name.
        /// </summary>
        /// <returns>Error code/name</returns>
        public override string GetErrorString()
        {
            // Read the Error string
            string error = Query_Unchecked("ERR?", null);

            // Return the Error string
            return error;
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
            }
        }
        /// <summary>
        /// set event register mask and clear status.
        /// </summary>
        public void SetEventMaskAndClrStatus()
        {
            //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
            this.StandardEventRegisterMask = 4 + 8 + 16 + 32;

            //// clear the status registers
            this.Write("*CLS", null);
        }

        #endregion
    }
}

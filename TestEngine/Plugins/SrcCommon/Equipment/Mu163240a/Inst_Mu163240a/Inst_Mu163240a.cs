// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Mu163240a.cs
//
// Author: mark.fullalove, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    public class Inst_Mu163240a : Instrument, IInstType_BertErrorAnalyser
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Mu163240a(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "MU163240",				// hardware name 
                "0",  			// minimum valid firmware version 
                "0");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Mu163240a",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Mu163240a)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Unsupported ?
                return "0";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Unsupported ?
                return "MU163240";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// Instrument's chassis
        /// </summary>
        private Chassis_Mu163240a instrumentChassis;
        #endregion

        #region IInstType_BertErrorAnalyser Members

        public bool AnalyserEnabled
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void AnalyserStart()
        {
            instrumentChassis.Write_Unchecked(":SENS" + this.Slot + ":MEAS:START", this);
        }

        public void AnalyserStop()
        {
            instrumentChassis.Write_Unchecked(":SENS" + this.Slot + ":MEAS:STOP", this);
        }

        public void AutoSetup()
        {
            // Phase and threshold mode
            instrumentChassis.Write_Unchecked(":SENS" + this.Slot + ":MEAS:ASE:MODE PHTR", this);
            // Auto search start
            instrumentChassis.Write_Unchecked(":SENS" + this.Slot + ":MEAS:ASE:STAR", this);
        }

        public double ErrorRatio
        {
            get
            {
                if (SyncLossDetected)
                    return 1;
                string rtn = instrumentChassis.Query_Unchecked(":CALC" + this.Slot + ":DATA:EAL? " + "\"ER TOT\"", this);
                return Convert.ToInt64(rtn.Substring(1, rtn.Length - 1));
            }
        }

        public double ErrorRatioOfOnesRecievedAsZeros
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double ErrorRatioOfZerosRecievedAsOnes
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public long ErroredBits
        {
            get
            {
                if (SyncLossDetected)
                    return long.MaxValue;

                string rtn = instrumentChassis.Query_Unchecked(":CALC" + this.Slot + ":DATA:EAL? " + "\"ER TOT\"", this);
                return Int64.Parse(rtn.Substring(1, rtn.Length - 1));
            }
        }

        public long ErroredBitsOnesRecievedAsZeros
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public long ErroredBitsZerosRecievedAsOnes
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double MeasPhaseDelay_ps
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double MeasThreshold_V
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_BertDataPatternType PatternType
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_BertDataPolarity Polarity
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public int PrbsLength
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool SyncLossDetected
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":CALC" + this.Slot + ":DATA:MON? PSL", this);
                return rtn.Equals("Occur");
            }
        }

        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion
    }
}

﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_SR830.cs
//
// Author: sam.quan, 2011
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    /// <summary>
    /// 
    /// </summary>
    public class Chassis_SR865 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_SR865(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord SR830Data = new ChassisDataRecord(
            "Stanford_Research_Systems SR830", // hardware name Stanford_Research_Systems SR830
                "0",										// minimum valid firmware version 
                "ver1.07");										// maximum valid firmware version 
            ValidHardwareData.Add("SR830", SR830Data);

            //SR865A
            ChassisDataRecord SR865Data = new ChassisDataRecord(
            "Stanford_Research_Systems SR865A", // hardware name Stanford_Research_Systems SR830
            "0",										// minimum valid firmware version 
            "V1.47");										// maximum valid firmware version 
            ValidHardwareData.Add("SR865", SR865Data);
        }
        #endregion

        #region Chassis overrides
        
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                string fv = idn[3]; //.Substring(0, 7);
                return fv;
              
            }
        }

        /// <summary>
        /// Hardware identity string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                //string hi = idn[0] + " " + idn[1];
                return idn[0] + " " + idn[1];
                
            }
        }
       
        /// <summary>
        /// 
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // needed for error detection routines
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                    //this.StandardEventRegisterMask = 60;
                }
            }
        }

        #endregion
    }
}

﻿// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SR830.cs
//
// Author: sam.quan , 2011
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    /// <summary>
    /// /
    /// </summary>
    public class Inst_SR865 : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SR865(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord SR830Data = new InstrumentDataRecord(
            "Stanford_Research_Systems SR830", // hardware name Stanford_Research_Systems SR830
                "0",										// minimum valid firmware version 
                "ver1.07");										// maximum valid firmware version 
            ValidHardwareData.Add("SR830", SR830Data);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord SR865chassisData = new InstrumentDataRecord(
                "Chassis_SR865",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "6");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SR865", SR865chassisData);

            InstrumentDataRecord SR865Data = new InstrumentDataRecord(
            "Stanford_Research_Systems SR865A", // hardware name Stanford_Research_Systems SR830
            "0",										// minimum valid firmware version 
            "V1.47");										// maximum valid firmware version 
            ValidHardwareData.Add("SR865", SR865Data);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_SR865)base.InstrumentChassis;
        }
        #endregion

        #region SR865 function

        /// <summary>
        /// get/set the synchronous filter status, true--on, false--off
        /// </summary>
        public bool SynchronousFilter
        {
            get 
            {
                string strTemp = instrumentChassis.Query_Unchecked("SYNC?", this);
                if (strTemp == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set 
            {
                string command = "SYNC 0";
                if (value)
                {
                    command = "SYNC 1";
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// get/set lower pass filter sloper
        /// </summary>
        public FilterSlopeEnum LowerPassFilterSloper
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("OFSL?", this);
                return (FilterSlopeEnum)Enum.Parse(typeof(FilterSlopeEnum), strTemp);
            }
            set 
            {
                int slopNum = (int)value;
                instrumentChassis.Write_Unchecked("OFSL " + slopNum.ToString(), this);
            }
        }

        /// <summary>
        /// get/set time constant
        /// </summary>
        public TimeConstantEnum TimeConstant
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("OFLT?", this);
                return (TimeConstantEnum)Enum.Parse(typeof(TimeConstantEnum), strTemp);
            }
            set
            {
                int timeConstant = (int)value;
                instrumentChassis.Write_Unchecked("OFLT " + timeConstant.ToString(), this);
            }
        }

        /// <summary>
        /// get/set sensitivity
        /// </summary>
        public SensitivityEnum Sensitivity
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("SENS?", this);
                return (SensitivityEnum)Enum.Parse(typeof(SensitivityEnum), strTemp);
            }
            set
            {
                int sensitivity = (int)value;
                instrumentChassis.Write_Unchecked("SENS " + sensitivity.ToString(), this);
            }
        }

        /// <summary>
        /// set/get reserve mode
        /// </summary>
        public ReserveModeEnum ReserveMode
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("RMOD?", this);
                return (ReserveModeEnum)Enum.Parse(typeof(ReserveModeEnum), strTemp);
            }
            set
            {
                int reserveMode = (int)value;
                instrumentChassis.Write_Unchecked("RMOD " + reserveMode.ToString(), this);
            }
        }

        /// <summary>
        /// set/get input signal type
        /// </summary>
        public InputSignalConfigEnum InputSignalConfiguration
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("ISRC?", this);
                return (InputSignalConfigEnum)Enum.Parse(typeof(InputSignalConfigEnum), strTemp);
            }
            set
            {
                int inputConfiguration = (int)value;
                instrumentChassis.Write_Unchecked("ISRC " + inputConfiguration.ToString(), this);
            }
        }

        /// <summary>
        /// set/get shield Ground status
        /// </summary>
        public ShieldGroundEnum ShieldGround
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("IGND?", this);
                return (ShieldGroundEnum)Enum.Parse(typeof(ShieldGroundEnum), strTemp);
            }
            set
            {
                int shieldGroundType = (int)value;
                instrumentChassis.Write_Unchecked("IGND " + shieldGroundType.ToString(), this);
            }
        }

        /// <summary>
        /// get/set input couple 
        /// </summary>
        public InputCoupleTypeEnum InputCouple
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("ICPL?", this);
                return (InputCoupleTypeEnum)Enum.Parse(typeof(InputCoupleTypeEnum), strTemp);
            }
            set
            {
                int inputCoupleType = (int)value;
                instrumentChassis.Write_Unchecked("ICPL " + inputCoupleType.ToString(), this);
            }
        }

        /// <summary>
        /// get/set notch filter type
        /// </summary>
        public NotchFilterEnum NotchFilter
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("ILIN?", this);
                return (NotchFilterEnum)Enum.Parse(typeof(NotchFilterEnum), strTemp);
            }
            set
            {
                int inputCoupleType = (int)value;
                instrumentChassis.Write_Unchecked("ILIN " + inputCoupleType.ToString(), this);
            }
        }

        /// <summary>
        /// set/get reference phase shift to xxx,range is -360.00 to 729.99
        /// </summary>
        public double ReferencePhase
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("PHAS?", this);
                return double.Parse(strTemp);
            }
            set
            {
                if (value >= -360.00 && value <= 729.99)
                {
                    instrumentChassis.Write_Unchecked("PHAS " + value.ToString(), this);
                }
                else
                {
                    throw new Exception("value out of range -360.00--729.99");
                }
                
            }
        }

        /// <summary>
        /// get/set reference source type
        /// </summary>
        public ReferenceSourceEnum ReferenceSource
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("FMOD?", this);
                return (ReferenceSourceEnum)Enum.Parse(typeof(ReferenceSourceEnum), strTemp);
            }
            set
            {
                int RefSourceType = (int)value;
                string command = "FMOD " + RefSourceType.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// get/set the reference frequency when use internal reference source,freqency range is 0.001--102000 Hz
        /// </summary>
        public double ReferenceFreq_Hz
        {
            get
            {
                double returnData;
                string strTemp = instrumentChassis.Query_Unchecked("FREQ?", this);
                try
                {
                    returnData = double.Parse(strTemp);
                }
                catch (Exception)
                {

                    throw new Exception("can't not receive correct data for instrument");
                }
                return returnData;
            }
            set
            {
                if (value > 0.0001 && value < 102000)
                {
                    string command = "FREQ " + value.ToString();
                    instrumentChassis.Write_Unchecked(command, this);
                }
                else
                {
                    throw new Exception("value out of range 0.0001--102000");
                    
                }
            }
        }

        /// <summary>
        /// Reference trigger
        /// </summary>
        public ReferTrigEnum ReferenceTrigger
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("RSLP?", this);
                return (ReferTrigEnum)Enum.Parse(typeof(ReferTrigEnum), strTemp);
            }
            set
            {
                int RefSourceType = (int)value;
                instrumentChassis.Write_Unchecked("RSLP " + RefSourceType.ToString(), this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputType"></param>
        /// <returns></returns>
        public double ReadOutput(OutputTypeEnum outputType)
        {
            int outputTypeValue = (int)outputType;
            string resp = instrumentChassis.Query_Unchecked("OUTP? " + outputTypeValue.ToString(), this);

            return double.Parse(resp);
        }

        /// <summary>
        /// set display and ratio
        /// </summary>
        /// <param name="chan">channel number</param>
        /// <param name="display">display type</param>
        /// <param name="ratio">ratio type</param>
        public void SetDisplayAndRatio(ChanNumEnum chan, DisplyChanOneEnum display, RatioChanOneEnum ratio)
        {
            int intTemp = (int)chan;
            string command = intTemp.ToString() + ",";
            intTemp = (int)display;
            command = command + intTemp.ToString() + ",";
            intTemp = (int)ratio;
            command = command + intTemp.ToString();

            instrumentChassis.Write_Unchecked("DDEF " + command, this);
        }

        /// <summary>
        /// set display and ratio
        /// </summary>
        /// <param name="chan">channel number</param>
        /// <param name="display">display type</param>
        /// <param name="ratio">ratio type</param>
        public void SetDisplayAndRatio(ChanNumEnum chan, DisplayChanTowEnum display, RatioChanTwoEnum ratio)
        {
            int intTemp = (int)chan;
            string command = intTemp.ToString() + ",";
            intTemp = (int)display;
            command = command + intTemp.ToString() + ",";
            intTemp = (int)ratio;
            command = command + intTemp.ToString();

            instrumentChassis.Write_Unchecked("DDEF " + command, this);
        }

        /// <summary>
        /// get/set front pannel output source
        /// </summary>
        public FrontPanelOutputSourceEnum FrontPanelOutputSource
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("FPOP?", this);
                return (FrontPanelOutputSourceEnum)Enum.Parse(typeof(FrontPanelOutputSourceEnum), strTemp);
            }
            set
            {
                int outputSource = (int)value;
                instrumentChassis.Write_Unchecked("FPOP " + outputSource.ToString(), this);
            }
        }

        /// <summary>
        /// set offset and expands
        /// </summary>
        /// <param name="signalType">signal type ,X,Y,R</param>
        /// <param name="expand">expands,noExpand,x10,x100</param>
        /// <param name="offsetPercent">range from -105.00 to 105.00</param>
        public void SetOffsetAndExpands(signalTypeEnum signalType, ExpandEnum expand, double offsetPercent)
        {
            if (offsetPercent < -105.00 || offsetPercent > 105.00)
            {
                throw new Exception("offset out of range -105.00---105.00");
            }
            string command = "OEXP ";
            int intTemp = (int)signalType;
            command = command + intTemp + "," + offsetPercent.ToString() + ",";
            intTemp = (int)expand;
            command = command + intTemp;
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// auto offset to zero
        /// </summary>
        public void AutoOffset(signalTypeEnum signalType)
        {
            int signal = (int)signalType;
            instrumentChassis.Write_Unchecked("AOFF " + signal.ToString(), this);
        }

        #region setup command
        /// <summary>
        /// set/get the communication interface,rs232 or GPIB
        /// </summary>
        public InterfaceCommunicateEnum CommunicateInterface
        {
            get
            {
                string strTemp = instrumentChassis.Query_Unchecked("OUTX?", this);
                return (InterfaceCommunicateEnum)Enum.Parse(typeof(InterfaceCommunicateEnum), strTemp);
            }
            set
            {
                int CommInterface = (int)value;
                instrumentChassis.Write_Unchecked("OUTX " + CommInterface.ToString(), this);
            }
        
        }

        /// <summary>
        /// save the lock-in setup in setting buffer(1--9)
        /// </summary>
        /// <param name="bufferNumber"></param>
        public void SaveSettingInBuff(int bufferNumber)
        {
            if (bufferNumber >= 1 && bufferNumber <= 9)
            {
                instrumentChassis.Write_Unchecked("SSET " + bufferNumber.ToString(), this);
            }
            else
            {
                throw new Exception("buffer number out of range 1--9");
            }
        }

        /// <summary>
        /// recall the lock-in setting
        /// </summary>
        /// <param name="bufferNumber"></param>
        public void RecallSettingInBuff(int bufferNumber)
        {
            if (bufferNumber >= 1 && bufferNumber <= 9)
            {
                instrumentChassis.Write_Unchecked("RSET " + bufferNumber.ToString(), this);
            }
            else
            {
                throw new Exception("buffer number out of range 1--9");
            }
        }

        #endregion

        #region auto function

        /// <summary>
        /// set auto gain function
        /// </summary>
        public void AutoGain()
        {
            instrumentChassis.Write_Unchecked("AGAN", this);
        }

        /// <summary>
        /// set auto reserve function
        /// </summary>
        public void AutoReserve()
        {
            instrumentChassis.Write_Unchecked("ARSV", this);
        }

        /// <summary>
        /// set auto phase function
        /// </summary>
        public void AutoPhase()
        {
            instrumentChassis.Write_Unchecked("APHS", this);
        }

        #endregion

        #endregion

        #region Instrument function

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {

            // Set factory default
            instrumentChassis.Write_Unchecked("*RST", this);
            ReferenceSource = Inst_SR865.ReferenceSourceEnum.External;
            ReserveMode = Inst_SR865.ReserveModeEnum.Normal;
            //AutoOffset(Inst_SR830.signalTypeEnum.X);
            InputCouple = Inst_SR865.InputCoupleTypeEnum.DC;
            InputSignalConfiguration = Inst_SR865.InputSignalConfigEnum.A;
            LowerPassFilterSloper = Inst_SR865.FilterSlopeEnum.eighteen_dBperOct;
            NotchFilter = Inst_SR865.NotchFilterEnum.LineFilter;
            Sensitivity = Inst_SR865.SensitivityEnum._50mVperNA;
            SetDisplayAndRatio(Inst_SR865.ChanNumEnum.Ch1, Inst_SR865.DisplayChanTowEnum.deg, Inst_SR865.RatioChanTwoEnum.None);
            SetOffsetAndExpands(Inst_SR865.signalTypeEnum.X, Inst_SR865.ExpandEnum.noExpand, 10);
            ShieldGround = Inst_SR865.ShieldGroundEnum.Ground;
            SynchronousFilter = true;
            TimeConstant = Inst_SR865.TimeConstantEnum._30us;

        }

        #endregion

        #region Private data
        private Chassis_SR865 instrumentChassis;

        /// <summary>
        /// filter slope
        /// </summary>
        public enum FilterSlopeEnum
        { 
            /// <summary>
            /// 6dB/oct
            /// </summary>

            six_dBperOct,

            /// <summary>
            /// 12dB/oct
            /// </summary>

            twelve_dBperOct,

            /// <summary>
            /// 18dB/oct
            /// </summary>

            eighteen_dBperOct,

            /// <summary>
            /// 24dB/oct
            /// </summary>

            twentyFour_dBperOct
        }

        /// <summary>
        /// Enum of time counstant
        /// </summary>
        public enum TimeConstantEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            _10us,
            /// <summary>
            /// 
            /// </summary>
            _30us,
            /// <summary>
            /// 
            /// </summary>
            _100us,
            /// <summary>
            /// 
            /// </summary>
            _300us,
            /// <summary>
            /// 
            /// </summary>
            _1ms,
            /// <summary>
            /// 
            /// </summary>
            _3ms,
            /// <summary>
            /// 
            /// </summary>
            _10ms,
            /// <summary>
            /// 
            /// </summary>
            _30ms,
            /// <summary>
            /// 
            /// </summary>
            _100ms,
            /// <summary>
            /// 
            /// </summary>
            _300ms,
            /// <summary>
            /// 
            /// </summary>
            _1s,
            /// <summary>
            /// 
            /// </summary>
            _3s,
            /// <summary>
            /// 
            /// </summary>
            _10s,
            /// <summary>
            /// 
            /// </summary>
            _30s,
            /// <summary>
            /// 
            /// </summary>
            _100s,
            /// <summary>
            /// 
            /// </summary>
            _300s,
            /// <summary>
            /// 
            /// </summary>
            _1ks,
            /// <summary>
            /// 
            /// </summary>
            _3ks,
            /// <summary>
            /// 
            /// </summary>
            _10ks,
            /// <summary>
            /// 
            /// </summary>
            _30ks,

        }

        /// <summary>
        /// Enum of sensitivity
        /// </summary>
        public enum SensitivityEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            _2nVperfA,
            /// <summary>
            /// 
            /// </summary>
            _5nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _10nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _20nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _50nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _100nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _200nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _500nVperFA,
            /// <summary>
            /// 
            /// </summary>
            _1uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _2uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _5uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _10uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _20uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _50uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _100uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _200uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _500uVperPA,
            /// <summary>
            /// 
            /// </summary>
            _1mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _2mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _5mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _10mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _20mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _50mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _100mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _200mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _500mVperNA,
            /// <summary>
            /// 
            /// </summary>
            _1VperuA

        }

        /// <summary>
        /// Enum reserve mode
        /// </summary>
        public enum ReserveModeEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            Reserve,
            /// <summary>
            /// 
            /// </summary>
            Normal,
            /// <summary>
            /// 
            /// </summary>
            LowNoise
        }

        /// <summary>
        /// input signal config Enum
        /// </summary>
        public enum InputSignalConfigEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            A,
            /// <summary>
            /// 
            /// </summary>
            A_B,
            /// <summary>
            /// 
            /// </summary>
            I_10Mohm,
            /// <summary>
            /// 
            /// </summary>
            I_100Mohm
        }

        /// <summary>
        /// shield ground enum
        /// </summary>
        public enum ShieldGroundEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            Float,
            /// <summary>
            /// 
            /// </summary>
            Ground
        }

        /// <summary>
        /// input couple type
        /// </summary>
        public enum InputCoupleTypeEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            AC,
            /// <summary>
            /// 
            /// </summary>
            DC
        }

        /// <summary>
        /// Notch filter enum
        /// </summary>
        public enum NotchFilterEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            noFilter,
            /// <summary>
            /// 
            /// </summary>
            LineFilter,
            /// <summary>
            /// 
            /// </summary>
            TwoLineFilter
        }

        /// <summary>
        /// reference source enum
        /// </summary>
        public enum ReferenceSourceEnum
        {
            /// <summary>
            /// 
            /// </summary>
            External,
            /// <summary>
            /// 
            /// </summary>
            Internal
            
        }

        /// <summary>
        /// channel number
        /// </summary>
        public enum ChanNumEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            Ch1=1,
            /// <summary>
            /// 
            /// </summary>
            Ch2
        }

        /// <summary>
        /// chan one display content
        /// </summary>
        public enum DisplyChanOneEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            X=1,
            /// <summary>
            /// 
            /// </summary>
            R,
            /// <summary>
            /// 
            /// </summary>
            X_Noise,
            /// <summary>
            /// 
            /// </summary>
            AuxIn1,
            /// <summary>
            /// 
            /// </summary>
            AuxIn2
        }

        /// <summary>
        /// chan two display content
        /// </summary>
        public enum DisplayChanTowEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            Y,
            /// <summary>
            /// 
            /// </summary>
            deg,
            /// <summary>
            /// 
            /// </summary>
            Y_Noise,
            /// <summary>
            /// 
            /// </summary>
            AuxIn3,
            /// <summary>
            /// 
            /// </summary>
            AuxIn4
        }

        /// <summary>
        /// 
        /// </summary>
        public enum RatioChanOneEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            none,
            /// <summary>
            /// 
            /// </summary>
            AuxIn1,
            /// <summary>
            /// 
            /// </summary>
            AuxIn2
        }
        /// <summary>
        /// 
        /// </summary>
        public enum RatioChanTwoEnum
        { 
            /// <summary>
            /// 
            /// </summary>
            None,
            /// <summary>
            /// 
            /// </summary>
            AuxIn3,
            /// <summary>
            /// 
            /// </summary>
            AuxIn4
        }
        /// <summary>
        /// output source content
        /// </summary>
        public enum FrontPanelOutputSourceEnum
        { 
            /// <summary>
            /// output display signal
            /// </summary>
            ChanDisplay,
            /// <summary>
            /// output  X or Y signal
            /// </summary>
            XorY
        }
        /// <summary>
        /// signal type
        /// </summary>
        public enum signalTypeEnum
        { 
            /// <summary>
            /// X
            /// </summary>
            X=1,
            /// <summary>
            /// Y
            /// </summary>
            Y,
            /// <summary>
            /// R
            /// </summary>
            R
        }
        /// <summary>
        /// expand enum
        /// </summary>
        public enum ExpandEnum
        { 
            /// <summary>
            /// no expand output
            /// </summary>
            noExpand,
            /// <summary>
            /// x10 output
            /// </summary>
            x10,
            /// <summary>
            /// x100 output
            /// </summary>
            x100
        }
        /// <summary>
        /// communication interface enum
        /// </summary>
        public enum InterfaceCommunicateEnum
        { 
            /// <summary>
            /// rs232 comunication mode
            /// </summary>
            RS232,
            /// <summary>
            /// GPIB comunication mode
            /// </summary>
            GPIB
        }
        /// <summary>
        /// refer trig enum
        /// </summary>
        public enum ReferTrigEnum
        { 
            /// <summary>
            /// zero crossing trigger mode
            /// </summary>
            ZeroCrossing,
            /// <summary>
            /// rising edge trigger mode
            /// </summary>
            RisingEdge,
            /// <summary>
            /// falling edge trigger mode
            /// </summary>
            FallingEdge
        }
        /// <summary>
        /// output type enum
        /// </summary>
        public enum OutputTypeEnum
        { 
            /// <summary>
            /// x output
            /// </summary>
            X=1,
            /// <summary>
            /// y output
            /// </summary>
            Y,
            /// <summary>
            /// r output
            /// </summary>
            R,
            /// <summary>
            /// phase output
            /// </summary>
            Phase
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham Library
// Bookham.TestEngine.Equipment
//
// TecsDataValues.cs
//
// Author: K Pillar
// Design: As specified in Driver_Nt10A DD 

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Internal class to act as a container for the tecs data values
    /// </summary>
    internal sealed class TecsDataValues
    {
        internal double PRTTemperature_DegC;
        internal double ThermistorResistance_Kohms;
        internal double PeltierCurrent_A;
        internal double PeltierVoltage_V;
        internal bool OutputOn;
        internal bool OutputCurrentLimitOn;
        internal bool AveragingOn;
        internal InstType_TecController.ControlMode ControlMode;
    }
}

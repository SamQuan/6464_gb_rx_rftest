using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;

namespace EquipmentTest_Inst_Nt10a
{
	/// <exclude />	
	[TestFixture]
	public class Inst_Nt10a_Test 
	{		
		/// <exclude />
		/// <summary>
		/// Constructor 
		/// </summary>
        public Inst_Nt10a_Test()
		{}

		/// <exclude />
		[TestFixtureSetUp]
		public void Setup()
		{
			// initialise logging domain
			Bookham.TestEngine.Framework.Logging.Initializer.Init();
		}

		/// <exclude />
		[TestFixtureTearDown]
		public void ShutDown()
		{
			testInst.OutputEnabled = false;
			testChassis.IsOnline = false;

			// Test end
			Debug.WriteLine("Test Finished!");
		}

		[Test]
		public void T01_CreateInst(string visaResource)
		{
            testChassis = new Chassis_Nt10a("TestChassis", "Chassis_Nt10a", visaResource);
			Debug.WriteLine("Chassis created OK");
            testInst = new Inst_Nt10a("TestInst", "Inst_Nt10a", "1", "", testChassis);
			Debug.WriteLine("Instrument created OK");
		}

		/// <exclude />
		[Test]
		[ExpectedException(typeof(System.Exception))]
		public void T02_TryCommsNotOnline()
		{
			 //Cannot try this as logging shuts down the app after the exception
			try
			{
				Debug.WriteLine(testInst.HardwareIdentity);
			}
			catch(ChassisException e)
			{
				Debug.WriteLine("Expected exception :" + e.Message);
			}
		}

		[Test]
		public void T03_SetOnline()
		{
			testChassis.IsOnline = true;
			Debug.WriteLine("Chassis IsOnline set true OK");
			testInst.IsOnline = true;
			Debug.WriteLine("Instrument IsOnline set true OK");
		}

		[Test]
		public void T04_DriverVersion()
		{
			Debug.WriteLine(testInst.DriverVersion);
		}

		[Test]
		public void T05_FirmwareVersion()
		{
			Debug.WriteLine(testInst.FirmwareVersion);
		}

		[Test]
		public void T06_HardwareID()
		{
			Debug.WriteLine(testInst.HardwareIdentity);
		}

		[Test]
		public void T07_SetDefaultState()
		{
			testInst.SetDefaultState();
		}

		[Test]
		public void T08_EnableOutput()
		{
            
			Debug.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
			testInst.OutputEnabled = true;
			Debug.WriteLine("Output state set to 'true' OK");
            Debug.WriteLine("Output state : " + testInst.OutputEnabled.ToString());
		}

        [Test]
        public void T09_ControlConstants()
        {
            double proportionalGain = 0;
            double integralGain = 0;


            Debug.WriteLine("proportionalGain originally = " + proportionalGain.ToString());
            Debug.WriteLine("integralGain originally = " + integralGain.ToString());

            proportionalGain = 32;
            integralGain = 32;

            Debug.WriteLine("Setting both values equal to 32");

            testInst.ProportionalGain = proportionalGain;
            testInst.IntegralGain = integralGain;

           
            Debug.WriteLine("Reading back changed control constants.");
            Debug.WriteLine("Proportional Gain is = " + testInst.ProportionalGain.ToString());
            Debug.WriteLine("Integral Gain is = " + testInst.IntegralGain.ToString());
        }

        [Test]
        public void T10_OperatingMode()
        {
            Debug.WriteLine("Setting Operating mode to temperature");
            testInst.OperatingMode = InstType_TecController.ControlMode.Temperature;
            testInst.OutputEnabled = true;
            Debug.WriteLine("Operating mode now = " + testInst.OperatingMode);
            testInst.OutputEnabled = false;
            testInst.OutputEnabled = true;
            Debug.WriteLine("Setting Operating mode to resistance");
            testInst.OperatingMode = InstType_TecController.ControlMode.Resistance;
            testInst.OutputEnabled = true;
            Debug.WriteLine("Operating mode now = " + testInst.OperatingMode);
        }

        public void T11_SensorType()
        {
            Debug.WriteLine("Setting Operating mode to resistance");
            testInst.OperatingMode = InstType_TecController.ControlMode.Resistance;
            testInst.OutputEnabled = true;
            Debug.WriteLine("Sensor type now = " + testInst.Sensor_Type);
            Debug.WriteLine("Now setting Operating mode to temperature");
            testInst.OperatingMode = InstType_TecController.ControlMode.Temperature;
            Debug.WriteLine("Sensor type now = " + testInst.Sensor_Type);
        
        }

        public void T12_TemperatureAndResistance()
        {
            Debug.WriteLine("Setting Operating mode to resistance & set point value to 10k");
            testInst.OperatingMode = InstType_TecController.ControlMode.Resistance;
            testInst.OutputEnabled = true;
            testInst.SensorResistanceSetPoint_ohm = 10000;
            Debug.WriteLine("Set Resistance value = " + testInst.SensorResistanceSetPoint_ohm);
            Debug.WriteLine("Actual Resistance value = " + testInst.SensorResistanceActual_ohm);
            Debug.WriteLine("Setting Operating mode to temperature & set point value to 25c");
            testInst.OperatingMode = InstType_TecController.ControlMode.Temperature;
            testInst.OutputEnabled = true;
            testInst.SensorTemperatureSetPoint_C = 25;
            Debug.WriteLine("Set temperature value = " + testInst.SensorTemperatureSetPoint_C);
            Debug.WriteLine("Actual temperature value = " + testInst.SensorTemperatureActual_C);     
        }

        public void T13_TecCurrent()
        {
            Debug.WriteLine("Reading Tec current, it = " + testInst.TecCurrentActual_amp);
        }

        public void T14_TecVoltage()
        {
            Debug.WriteLine("Reading Tec voltage, it = " + testInst.TecVoltageActual_volt);
        }

        public void T15_TecCurrentCompliance()
        {
            Debug.WriteLine("Tec current compliance = " + testInst.TecCurrentCompliance_amp);
            Debug.WriteLine("Setting Tec current compliance to 7A");
            testInst.TecCurrentCompliance_amp=7;
            Debug.WriteLine("Tec current compliance now = " + testInst.TecCurrentCompliance_amp);
        }

        [Test]

        public void T16_TimingComparision(bool Safemode)
        {

            int i;



            System.DateTime StartTime;

            System.DateTime EndTime;

            System.TimeSpan Duration;



            StartTime = System.DateTime.Now;


            Debug.WriteLine("Start Time: " + StartTime);

            int maxloops = 50;


            //testInst.SafeModeOperation = Safemode;

            testInst.SetDefaultState();

            for (i = 0; i < maxloops; i++)
            {
                testInst.OperatingMode = InstType_TecController.ControlMode.Resistance;
                testInst.OutputEnabled = true;
                testInst.SensorResistanceSetPoint_ohm = 10000;
                testInst.OutputEnabled = false;
            }

            EndTime = System.DateTime.Now;

            Debug.WriteLine("End Time: " + EndTime);

            Duration = EndTime - StartTime;

            Debug.WriteLine("SafeMode = " + Safemode + "\n " +

                "Did " + maxloops + " sets of operations in " + Duration.TotalSeconds + " seconds");



        }


		/// <summary>
		/// Chassis & Inst references
		/// </summary>
		private	Chassis_Nt10a testChassis;
		private Inst_Nt10a testInst;

	}

}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer
{
    /// <summary>
    /// point list
    /// </summary>
    public class Trace:List<Point>
    {       
        /// <summary>
        /// add point
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Add(double x, double y)
        {
            Point point=new Point(x,y);
            Add(point);
        }

        /// <summary>
        /// get X array
        /// </summary>
        /// <returns>X array</returns>
        public double[] GetXArray()
        {
            double[] xArray = new double[Count];
            int i=0;
            foreach (Point point in this)
            {
                xArray[i] = point.X;
                i++;
            }

            return xArray;
        }

        /// <summary>
        /// get Y array
        /// </summary>
        /// <returns>Y array</returns>
        public double[] GetYArray()
        {
            double[] yArray = new double[Count];
            int i = 0;
            foreach (Point point in this)
            {
                yArray[i] = point.Y;
                i++;
            }

            return yArray;
        }

    }

    /// <summary>
    /// contains x and y values
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }

        private double _x;
        private double _y;

        /// <summary>
        /// set/get X value
        /// </summary>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }
        /// <summary>
        /// set/get Y value
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }
    }
}

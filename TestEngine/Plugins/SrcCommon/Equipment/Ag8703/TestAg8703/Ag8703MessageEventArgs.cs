using System;
using System.Collections.Generic;
using System.Text;

namespace TestAg8703
{
    class Ag8703MessageEventArgs : EventArgs
    {
        private string _message = "";
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public Ag8703MessageEventArgs():base()
        {

        }

        public Ag8703MessageEventArgs(string message)
        {
            _message = message;
        }

        public override string ToString()
        {

            return Message;
          
        }
    }
}

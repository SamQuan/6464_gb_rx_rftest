using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;

namespace TestAg8703
{
    public partial class Main : Form
    {
        private Ag8703 _ag8703 = new Ag8703();

        public Main()
        {
            InitializeComponent();
            _ag8703.MessageArrived += new EventHandler<Ag8703MessageEventArgs>(_ag8703_MessageArrived);
        }

        void _ag8703_MessageArrived(object sender, Ag8703MessageEventArgs e)
        {

            listBoxOutput.Items.Add(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ": " + e.Message);
        }


        private void buttonClearOutput_Click(object sender, EventArgs e)
        {
            listBoxOutput.Items.Clear();
        }

        private void buttonInitialize_Click(object sender, EventArgs e)
        {
            _ag8703.Setup((int)numericUpDownGPIB.Value);
        }

        private void buttonGetStartFrequency_Click(object sender, EventArgs e)
        {
            _ag8703.GetStartFrequency();

        }

        private void buttonGetStopFrequency_Click(object sender, EventArgs e)
        {
            _ag8703.GetStopFrequency();
        }

        private void buttonGetTraceData_Click(object sender, EventArgs e)
        {
            _ag8703.GetTraceData();
        }

        private void listBoxOutput_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxOutput.Text = listBoxOutput.Items[listBoxOutput.SelectedIndex].ToString();
            }
            catch
            {
            }
        }

        private void buttonPreset_Click(object sender, EventArgs e)
        {
            _ag8703.Preset();
        }

        private void buttonSetActiveChannel_Click(object sender, EventArgs e)
        {
            int channel = (int)numericUpDownChannel.Value;
            switch (channel)
            {
                case 1:
                    _ag8703.SetActiveChannel(Channel.Channel1);
                    break;
                case 2:
                    _ag8703.SetActiveChannel(Channel.Channel2);
                    break;
                default:
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _ag8703.GetActiveChannel();
        }

        private void buttonGetDisplayFormat_Click(object sender, EventArgs e)
        {
            _ag8703.GetDisplayFormat();
        }

        private void buttonGetAveragingFactor_Click(object sender, EventArgs e)
        {
            _ag8703.GetActiveAveragingFactorChannel();
        }

        private void buttonTriggerSweep_Click(object sender, EventArgs e)
        {
            SweepTriggerMode sweepMode = SweepTriggerMode.Continuous;
            try
            {
                sweepMode = (SweepTriggerMode)Enum.Parse(typeof(SweepTriggerMode), comboBoxSweepMode.Text);
            }
            catch
            {
                MessageBox.Show("Please select a sweep trigger mode!");
                return;
            }

            _ag8703.TriggerSweep(sweepMode);
        }

        private void buttonSetSourcePower_Click(object sender, EventArgs e)
        {
            double power = (double)numericUpDownPower.Value;
            _ag8703.SetSourcePower(power);
        }

        private void buttonGetSourcePower_Click(object sender, EventArgs e)
        {
            _ag8703.GetSourcePower();
        }

        private void buttonSetSweepTime_Click(object sender, EventArgs e)
        {
            double time = (double)numericUpDownSweepTime.Value;
            if (time == 0)
            {
                time = InstType_LightwaveComponentAnalyzer.AUTO_SWEEP_TIME;
            }
            _ag8703.SetSweepTime(time);
        }

        private void buttonGetSweepTime_Click(object sender, EventArgs e)
        {
            _ag8703.GetSweepTime();
        }

        private void buttonCalibrateS22_Click(object sender, EventArgs e)
        {
            _ag8703.CalibrateS22();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.Instruments.LightwaveComponentAnalyzer;
using Bookham.TestSolution.ChassisNS;

namespace TestAg8703
{
    class Ag8703
    {
        public event EventHandler<Ag8703MessageEventArgs> MessageArrived = null;

        private Chassis_Ag8703 _chassisAg8703 = null;
        private Inst_Ag8703_LightwaveComponentAnalyzer _instAg8703 = null;
        private Chassis_Ag8703 ChassisAg8703
        {
            get { return _chassisAg8703; }
            set { _chassisAg8703 = value; }
        }
        private Inst_Ag8703_LightwaveComponentAnalyzer InstAg8703
        {
            get { return _instAg8703; }
            set { _instAg8703 = value; }
        }

        public Ag8703()
        {
        }

        public void OnMessageArrived(string message)
        {
            EventHandler<Ag8703MessageEventArgs> messageArrived = MessageArrived;
            if (messageArrived != null)
            {
                Ag8703MessageEventArgs args = new Ag8703MessageEventArgs(message);
                messageArrived(this, args);
            }
        }


        public void Setup(int gpibAddress)
        {
            string resource = "GPIB0::" + gpibAddress.ToString() + "::INSTR";
            ChassisAg8703 = new Chassis_Ag8703("HEWLETT PACKARD 8703A", "Chassis_Ag8703", resource);
            OnMessageArrived("chassis ok");
            InstAg8703 = new Inst_Ag8703_LightwaveComponentAnalyzer("HEWLETT PACKARD 8703A", "Inst_Ag8703_LightwaveComponentAnalyzer", "", "", ChassisAg8703);
            OnMessageArrived("instrument ok");
            ChassisAg8703.IsOnline = true;
            OnMessageArrived("chassis online ok");
            InstAg8703.IsOnline = true;
            OnMessageArrived("instrument online ok");


        }

        public void GetStartFrequency()
        {
            OnMessageArrived("Start Frequency:" + InstAg8703.StartFrequency_GHz.ToString() + "GHz");
        }
        public void GetStopFrequency()
        {
            OnMessageArrived("Stop Frequency:" + InstAg8703.StopFrequency_GHz.ToString() + "GHz");
        }
        public void GetTraceData()
        {
            Trace trace = _instAg8703.GetTraceData();
           // Trace trace2 = _instAg8703.GetTraceData(true);
            StringBuilder builder = new StringBuilder();
            builder.Append("Trace data:");
            StreamWriter sw = new StreamWriter(@"C:\SamQuan\rowData.csv");
            foreach (Point point in trace)
            {
                builder.Append("(" + point.X + "," + point.Y + ");");
                //builder.Append(point.X + "," + point.Y + "," + point.Z);
                sw.WriteLine(point.X + "," + point.Y);
            }
            sw.Close();
            OnMessageArrived(builder.ToString());
        }

        public void Preset()
        {
            _instAg8703.Preset();
            OnMessageArrived("Preset ok");
        }

        public void SetActiveChannel(Channel channel)
        {
            _instAg8703.ActiveMeausurementChannel = channel;
            OnMessageArrived("set active channel ok");
        }

        public void GetActiveChannel()
        {
            Channel channel = _instAg8703.ActiveMeausurementChannel;
            OnMessageArrived("Active Channel:" + channel.ToString());
        }

        public void GetDisplayFormat()
        {
            DisplayFormat displayFormat = _instAg8703.DisplayFormat;
            OnMessageArrived("Display format:" + displayFormat.ToString());
        }

        public void GetActiveAveragingFactorChannel()
        {
            OnMessageArrived("Active Channel Averaging Factor:" + _instAg8703.ActiveChannelAveragingFactor.ToString());
        }

        public void TriggerSweep(SweepTriggerMode sweepMode)
        {
            _instAg8703.TriggerSweep(sweepMode);

            OnMessageArrived("Trigger Sweep ok.Sweep mode:" + sweepMode.ToString());
        }

        public void SetSourcePower(double power)
        {
            _instAg8703.SourceOutputPower_dBm = power;
            OnMessageArrived("Set power(" + power.ToString() + ") ok.");
        }

        public void GetSourcePower()
        {
            OnMessageArrived("Source Power:" + _instAg8703.SourceOutputPower_dBm.ToString());
        }

        public void SetSweepTime(double time)
        {
            _instAg8703.SweepTime = time;
            OnMessageArrived("Set Sweep time ok:" + time.ToString());
        }

        public void GetSweepTime()
        {
            OnMessageArrived("Sweep time ok:" + _instAg8703.SweepTime.ToString());
        }

        public void GetSmoothingAperture()
        {
            OnMessageArrived("Smoothing Aperture:" + _instAg8703.SmoothingAperturePercent.ToString() + "%");
        }

        public void CalibrateS22()
        {
            _instAg8703.ActiveMeausurementChannel = Channel.Channel1;
            _instAg8703.SparameterMeasurementMode = SparameterMeasurementMode.S22;
            _instAg8703.DisplayFormat = DisplayFormat.LinearMagnitude;
            _instAg8703.CalibrateS22OnPort1();

            OnMessageArrived("Calibrate S22 ok.");

        }
    }
}
   
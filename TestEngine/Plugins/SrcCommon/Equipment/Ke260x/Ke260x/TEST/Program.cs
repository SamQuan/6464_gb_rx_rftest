using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using System.Collections;
using Bookham.TestSolution.TestScriptLanguage;

namespace TEST
{
    class Program
    {
        static Node ThisNode;
        static void Main(string[] args)
        {

            Chassis_Ke260x ch2602 = new Chassis_Ke260x("Chassis_Ke260x", "Chassis_Ke260x", "visa://szn-dt-jackxue/GPIB0::26::INSTR");
            //Node_ID = 2;Selected Channel is CHANNEL B;
            Inst_Ke260x ke2602 = new Inst_Ke260x("Inst_Ke260x", "Inst_Ke260x", "1", "A", ch2602);
            ch2602.IsOnline = true;
            ke2602.IsOnline = true;

            ke2602.SetDefaultState();
            ThisNode = ke2602.ThisNode;
            Node.smuX smub =ThisNode.smub;
            Node.smuX smua = ThisNode.smua;

            Node.Status.StatusByteBit b = ThisNode.status.condition;
            Node.ErrorQueue.MsgStruct Msg = ThisNode.errorqueue.next();
            Node.Status.StdEvtByteBit std = ThisNode.status.standard.Event;
            ThisNode.waitcomplete(3000);
            ThisNode.status.reset();
            ThisNode.errorqueue.clear();

            #region trigger sweep
            Node.smuX.IvStruct[] ivReadings = ThisNode.LinearSweepAndTrigger(Node.Channel.SMUA, Node.smuX.Func.OUTPUT_DCVOLTS,
                -3, 3, 0.1f, 0.1f, Node.TrigDirection.None,1,0.1f, 2,100,4);

            double[] ireadings, vreadings;
            ThisNode.LinearSweepAndTrigger(Node.Channel.SMUB, Node.smuX.Func.OUTPUT_DCVOLTS,
                Node.Format.DataFmt.REAL32,
               0.5f, 5f, 0.05f,0.1f, Node.TrigDirection.Output_Trigger,3, 0.01f,1,10, 4,
               out ireadings, out vreadings);
            #endregion

            //test smux source fucntion
            SmuxSourceFucntion(smua);
            //test smux measure function
            SmuxMeasureFunction(smua);
            SmuxSourceFucntion(smub);
            SmuxMeasureFunction(smub);
            //smux other function
            SmuxOtherfunction(smua);
            SmuxOtherfunction(smub);
            #region 2602 TSP code for Setup
            //ThisNode.linefreq = 50;
            int n = ThisNode.tsplink.node;

            smub.reset(); //Reset SMUB
            smua.reset(); //Reset SMUA

            smub.source.func = Node.smuX.Func.OUTPUT_DCAMPS; //Set to source voltage
            Node.smuX.Func func = smub.source.func;
            ThisNode.reset();

            smub.measure.autozero = Node.smuX.AutoZero.AUTOZERO_ONCE;
            Node.smuX.AutoZero autozero = smub.measure.autozero;

            smub.measure.nplc = 1; //Set integration rate
            double nplc = smub.measure.nplc;

            smub.source.rangei = 0.01f; //Set source range
            smub.source.levelv = 0; //Set source value
            smub.source.output = Node.smuX.Output.OUTPUT_ON; //Enable SMUB

            smua.source.func = Node.smuX.Func.OUTPUT_DCVOLTS; //Set to source voltage
            smua.measure.autozero = Node.smuX.AutoZero.AUTOZERO_AUTO; //Set Autozero to Auto
            smua.measure.nplc = 1; //Set integration rate

            smua.source.rangev = 40; //Set source range
            smua.source.levelv = 0; //Set source value
            smua.source.output = Node.smuX.Output.OUTPUT_ON; //Enable SMUA

            #endregion

            #region How do I store measurements in non-voliatile memory?

            Node.smuX.nvbufferX rb1 = smub.nvbuffer1;
            //Step 1: Clear and configure the buffer
            rb1.clear();
            rb1.collecttimestamps = 1;
            rb1.collectsourcevalues = 1;
            //Step 2: Perform measurements
            smub.measure.count = 3;
            smub.source.output = Node.smuX.Output.OUTPUT_ON;
            smub.measure.v(rb1);
            smub.source.output = Node.smuX.Output.OUTPUT_OFF;
            //Step 3: Read the buffer
            Node.smuX.BufferData bd = rb1.PrintBuffer(1,rb1.n,true,true);
            
            #endregion

            #region Defined buffer example

            smua.reset();
            ThisNode.display.screen = Node.Display.DispScreen.SMUA;
            ThisNode.display.smua.measure.func = Node.Display.MeasFunc.MEASURE_DCAMPS;
            smua.measure.autorangei = Node.smuX.autorangeY.AUTORANGE_ON;
            ThisNode.format.data = Node.Format.DataFmt.ASCII;
            smua.nvbuffer1.clear();
            ThisNode.status.reset();
            ThisNode.errorqueue.clear();
            smua.nvbuffer1.appendmode = 1;
            smua.nvbuffer1.collectsourcevalues = 1;
            smua.measure.count = 1;
            smua.source.func = Node.smuX.Func.OUTPUT_DCVOLTS;
            smua.source.levelv = 0.0;
            ThisNode.format.data = Node.Format.DataFmt.REAL32;
            smua.nvbuffer1.clear();
            smua.source.output = Node.smuX.Output.OUTPUT_ON;
            for (float v = 0; v <= 0.5; v+=0.01f)
            {
                smua.source.levelv = v;
                ThisNode.delay(0.05f);
                smua.measure.i(smua.nvbuffer1);
                ThisNode.delay(0.15f);
            }
            ThisNode.waitcomplete(15000);
            smua.source.output = Node.smuX.Output.OUTPUT_OFF;
            
            Node.smuX.BufferData bd2 = smua.nvbuffer1.PrintBuffer(1, 100, false, false);
            smua.nvbuffer1.clear();
            smua.nvbuffer1.appendmode = 0;
            #endregion

        }
        private static void SmuxSourceFucntion(Node.smuX smux)
        {
            smux.reset();

            Node.smuX.Source source = smux.source;

            source.autorangei = Node.smuX.autorangeY.AUTORANGE_OFF;
            Node.smuX.autorangeY autorangeY = source.autorangei;

            source.autorangev = Node.smuX.autorangeY.AUTORANGE_OFF;
            autorangeY = source.autorangev;

            source.delay = 0.05;
            double reading = source.delay;

            source.func = Node.smuX.Func.OUTPUT_DCAMPS;
            Node.smuX.Func func = source.func;

            source.leveli = 100e-3;
            reading = source.leveli;
            source.limitv = 100e-3;
            reading = source.limitv;

            source.lowrangei = 10e-3;
            reading = source.lowrangei;

            Node.Status.StatusByteBit b = ThisNode.status.condition;
            Node.ErrorQueue.MsgStruct Msg = ThisNode.errorqueue.next();

            source.func = Node.smuX.Func.OUTPUT_DCVOLTS;
            source.levelv = 0.02;
            reading = source.levelv;
            source.limiti = 10e-3;
            reading = source.limiti;

            bool compliance = source.compliance;
            source.lowrangev = 0.01;
            reading = source.lowrangev;

            reading = source.offlimiti;
            source.offlimiti = reading;

            source.offmode = Node.smuX.OffMode.OUTPUT_NORMAL;
            Node.smuX.OffMode offmode = source.offmode;

            source.levelv = 0.02;
            ThisNode.waitcomplete(3000);
            source.output = Node.smuX.Output.OUTPUT_ON;

            b = ThisNode.status.condition;
            Msg = ThisNode.errorqueue.next();

            Node.smuX.Output outp = source.output;
            source.output = Node.smuX.Output.OUTPUT_OFF;

            reading = source.rangei;
            source.rangei = reading;

            reading = source.rangev;
            source.rangev = reading;

            //end


        }
        private static void SmuxOtherfunction(Node.smuX smux)
        {
            Node.smuX.Sense sense = smux.sense;
            smux.sense = sense;

            smux.source.output= Node.smuX.Output.OUTPUT_ON;
            smux.source.func = Node.smuX.Func.OUTPUT_DCAMPS;
            double reading = smux.measureIandstep(0.2);
            smux.source.func = Node.smuX.Func.OUTPUT_DCVOLTS;
            reading = smux.measureVandstep(1.5);
            Node.smuX.IvStruct ivreadings = smux.measureivandstep(0.5);
            reading = smux.measurePandstep(0);
            smux.source.output = Node.smuX.Output.OUTPUT_OFF;
            int count = smux.nvbuffer1.n;
            smux.nvbuffer1.clear();
            count = smux.nvbuffer2.n;
            smux.nvbuffer2.clear();
        }
        
        private static void SmuxMeasureFunction(Node.smuX smux)
        {
            //smux.reset();

            Node.smuX.Measure measure = smux.measure;

            Node.smuX.autorangeY autorangeY;
            measure.autorangei = Node.smuX.autorangeY.AUTORANGE_OFF;
            autorangeY = measure.autorangei;

            measure.autorangev = Node.smuX.autorangeY.AUTORANGE_OFF;
            autorangeY = measure.autorangev;

            Node.smuX.AutoZero autozero;
            measure.autozero = Node.smuX.AutoZero.AUTOZERO_AUTO;
            autozero = measure.autozero;

            int count = 0;
            measure.count = 2;
            count = measure.count;
            measure.count = 1;

            Node.smuX.Measure.Filter filter = measure.filter;

            count = filter.count;
            filter.count = count;

            filter.enable = Node.smuX.Filter.FILTER_OFF;
            Node.smuX.Filter flt = filter.enable;

            filter.type = Node.smuX.FilterType.FILTER_MOVING_AVG;
            Node.smuX.FilterType ft = filter.type;

            double reading = measure.i();
            measure.i(smux.nvbuffer1);
            Node.smuX.BufferData bd = smux.nvbuffer1.PrintBuffer(1, 2, false, false);
            double reading2 = bd.MeasuredReadings[0];

            reading = measure.interval;
            measure.interval = 0.02;
            ThisNode.format.data = Node.Format.DataFmt.REAL32;

            Node.smuX.IvStruct ivreadings = measure.iv();

            reading = measure.lowrangei;
            measure.lowrangei = reading;

            reading = measure.lowrangev;
            measure.lowrangev = reading;

            reading = measure.nplc;
            measure.nplc = reading;

            ThisNode.errorqueue.clear();
            ThisNode.status.reset();
            measure.count = 1000;
            measure.interval = 0.01;
            smux.nvbuffer1.clear();
            smux.nvbuffer1.collectsourcevalues = 1;
            smux.nvbuffer1.collecttimestamps = 1;
            ThisNode.format.data = Node.Format.DataFmt.REAL32;
            measure.count = 1000;
            ThisNode.status.reset();
            ThisNode.errorqueue.clear();

            measure.overlappedi(smux.nvbuffer1);
            //System.Threading.Thread.Sleep(10 * 2*1000);
            bd = smux.nvbuffer1.PrintBuffer(1, 3000, true, true);
            Node.Status.StatusByteBit b = ThisNode.status.condition;
            Node.ErrorQueue.MsgStruct Msg = ThisNode.errorqueue.next();

            reading2 = bd.MeasuredReadings[0];

            ThisNode.format.data = Node.Format.DataFmt.ASCII;

            measure.overlappedi(smux.nvbuffer1);
            System.Threading.Thread.Sleep(10 * 2 * 1000);
            bd = smux.nvbuffer1.PrintBuffer(1, 100, true, true);
            bd = smux.nvbuffer1.PrintBuffer(101, 200, true, true);
            reading2 = bd.MeasuredReadings[0];

            ThisNode.errorqueue.clear();
            ThisNode.status.reset();
            measure.count = 1;
            measure.interval = 0.02;
            smux.nvbuffer1.clear();
            smux.nvbuffer1.collectsourcevalues = 0;
            smux.nvbuffer1.collecttimestamps = 0;
            measure.overlappedv(smux.nvbuffer1);
            bd = smux.nvbuffer1.PrintBuffer(1, 1, false, false);
            reading2 = bd.MeasuredReadings[0];

            measure.overlappediv(smux.nvbuffer1, smux.nvbuffer2);
            bd = smux.nvbuffer1.PrintBuffer(1, 1, false, false);
            reading2 = bd.MeasuredReadings[0];
            bd = smux.nvbuffer2.PrintBuffer(1, 1, false, false);
            reading2 = bd.MeasuredReadings[0];

            measure.overlappedp(smux.nvbuffer1);
            bd = smux.nvbuffer1.PrintBuffer(1, 1, false, false);
            reading2 = bd.MeasuredReadings[0];

            reading = measure.p();

            reading = measure.rangei;
            measure.rangei = reading;

            reading = measure.rangev;
            measure.rangev = reading;

            reading = measure.v();

            Node.smuX.Measure.Rel rel = measure.rel;
            rel.enablei = Node.smuX.RelEnableY.REL_ON;
            Node.smuX.RelEnableY re = rel.enablei;

            rel.enablev = Node.smuX.RelEnableY.REL_ON;
            re = rel.enablev;

            reading = rel.leveli;
            rel.leveli = reading;

            reading = rel.levelv;
            rel.levelv = reading;

            reading = rel.levelp;
            rel.levelp = reading;
            //end
        }
    }
}

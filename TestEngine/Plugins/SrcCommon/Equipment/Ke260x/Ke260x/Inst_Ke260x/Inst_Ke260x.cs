// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ke260x.cs
//
// Author: tommy.yu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.TestScriptLanguage;
using System.Collections;
using System.Diagnostics;


namespace Bookham.TestSolution.Instruments
{
    
    /// <summary>
    /// Instrument driver for Keithley 260x SMU
    /// Provides basic source functions only for 'ElectricalSource' instrument type
    /// </summary>
    public class Inst_Ke260x : InstType_TriggeredElectricalSource, ICommnd
    {
        #region ICommnd Members

        public void Write_Unchecked(string command)
        {
            this.instrumentChassis.Write_Unchecked(command, this);
        }

        public string Query_Unchecked(string command)
        {
            return this.instrumentChassis.Query_Unchecked(command, this);
        }

        public byte[] QueryByteArray_Unchecked(string command)
        {
            return this.instrumentChassis.QueryByteArray_Unchecked(command, this);
        }
        public string Read_Unchecked()
        {
            return this.instrumentChassis.Read_Unchecked(this);
        }
        #endregion

        #region Constructor

        struct ModelDataRecord
        {
            internal int Model;
            internal string MinimumVersion, MaximumVersion;
            internal double MaxCurrentLimitAmp, MaxVoltageLimit, MaxVoltageLimitLV;
            internal bool IsLVUnit;
        }
        public enum CHANNEL
        {
            A = 1, B
        }

        private  Node thisNode;
        private Node.smuX thisSmux;
        private Nodes node;

        public readonly int NodeNumber;
        public readonly CHANNEL Channel;
        public Node.smuX ThisSmux
        {
            get
            {
                return thisSmux;
            }
        }
        public Node ThisNode
        {
            get
            {
                return thisNode;
            }
        }
        public Nodes Node
        {
            get
            {
                return node;
            }
        }

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ke260x(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {

            if (string.IsNullOrEmpty(slotInit))
                throw new InstrumentException("Please set Slot of TestEquipmentConfig.xml file,and the slot value within the range of 1 to 64");
            else if (!int.TryParse(slotInit, out NodeNumber) || !(NodeNumber >= 1 && NodeNumber <= 64))
                throw new InstrumentException("the value of Slot of TestEquipmentConfig.xml file is error,and the slot value within the range of 1 to 64");
            if (string.IsNullOrEmpty(subSlotInit))
                throw new InstrumentException("Please set Sub Slot of TestEquipmentConfig.xml file,and A or 1 is Channel A; B or 2 is Channel B");
            else if (subSlotInit != "A" && subSlotInit != "1" && subSlotInit != "B" && subSlotInit != "2")
                throw new InstrumentException("the value of Sub Slot of TestEquipmentConfig.xml file is error,and A or 1 is Channel A; B or 2 is Channel B");
            Channel = (CHANNEL)Enum.Parse(typeof(CHANNEL), subSlotInit);

            // Setup expected valid hardware variants 

            const string MinimumVersion = "1.0.2";
            const string MaximumVersion = "1.3.3";
            ModelDataRecord InstData = new ModelDataRecord();
            List<ModelDataRecord> Ke260xData = new List<ModelDataRecord>();
            // Add Ke2601/Ke2602 details
            InstData.MinimumVersion = MinimumVersion;
            InstData.MaximumVersion = MaximumVersion;
            InstData.Model = 2601;
            InstData.MaxCurrentLimitAmp = 3.03f;
            InstData.MaxVoltageLimit = 40.4f;
            InstData.MaxVoltageLimitLV = 21;
            InstData.IsLVUnit = true;
            Ke260xData.Add(InstData);
            InstData.Model = 2602;
            Ke260xData.Add(InstData);
            // Add ke2611/2612 details
            InstData.Model = 2611;
            InstData.MaxCurrentLimitAmp = 10;
            InstData.MaxVoltageLimit = 200;
            InstData.MaxVoltageLimitLV = 200;
            InstData.IsLVUnit = false;
            Ke260xData.Add(InstData);
            InstData.Model = 2612;
            Ke260xData.Add(InstData);
            // Add ke2635/2636 details
            InstData.Model = 2635;
            InstData.MaxCurrentLimitAmp = 10e-6f;
            InstData.MaxVoltageLimit = 200;
            InstData.MaxVoltageLimitLV = 200;
            InstData.IsLVUnit = false;
            Ke260xData.Add(InstData);
            InstData.Model = 2636;
            Ke260xData.Add(InstData);

            foreach (ModelDataRecord ModelData in Ke260xData)
            {
                InstrumentDataRecord ke260xData = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL " + ModelData.Model.ToString(),			// hardware name 
                ModelData.MinimumVersion,											// minimum valid firmware version 
                ModelData.MaximumVersion);										// maximum valid firmware version 
                ke260xData.Add("MaxCurrentLimitAmp", ModelData.MaxCurrentLimitAmp.ToString());		// maximum current limit
                ke260xData.Add("MaxVoltageLimit", ModelData.MaxVoltageLimit.ToString());			// maximum voltage limit
                ke260xData.Add("MaxVoltageLimitLV", ModelData.MaxVoltageLimitLV.ToString());		// maximum voltage limit if LV 
                ke260xData.Add("IsLVUnit", ModelData.IsLVUnit.ToString());					// Indicates this is an LV (low voltage) unit
                // Set this parameter false for standard units
                ke260xData.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
                ke260xData.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
                ke260xData.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
                ke260xData.Add("4wireSense", "false");				// Use 4 wire sensing false (default) or true 
                ValidHardwareData.Add("Ke" + ModelData.Model.ToString(), ke260xData);
            }

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ke260x",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ke260x", chassisInfo);

            // initialise this instrument's chassis
            // don't forget to change the private variable type too.
            //this.instrumentChassis = (Chassis_Ke260x)chassisInit;
            this.instrumentChassis = (Chassis_Ke260x)base.InstrumentChassis;
        }
        /// <summary>
        /// Get tsp link state. if online, then return true,else return false.
        /// </summary>
        public bool TspLinkState
        {
            get
            {
                this.Write_Unchecked("tsplink.reset()");
                System.Threading.Thread.Sleep(500);
                string resp = this.Query_Unchecked("print(tsplink.state)");
                return resp.ToLower().Contains("online");
            }
        }

        public void Reset()
        {
            thisSmux.reset();
        }

        #endregion


        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                //// Read the full IDN? string which has 4 comma seperated substrings
                //string[] idn = instrumentChassis.Query("*IDN?", this).Split(',');

                //// The 4th substring containins the firmware version
                //// The first 3 characters are the version
                //string fv = idn[3].Substring(1, 5);

                //// Log event 
                //LogEvent("'FirmwareVersion' returned : " + fv);

                //// Return 
                //return fv;
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                //// Read the full IDN? string which has 4 comma seperated substrings
                //string[] idn = instrumentChassis.Query("*IDN?", this).Split(',');

                //// Build the string from 2 substrings, the manufacturer name & the model number
                //string hid = idn[0].Trim() + " " + idn[1].Trim();

                //// Log event 
                //LogEvent("'HardwareIdentity' returned : " + hid);

                //return hid;
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Clear any pending operations
            instrumentChassis.Clear();
            // Set factory default
            instrumentChassis.Write("*RST", this);
            // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
            //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
            thisNode.reset();
            thisNode.status.reset();
            thisNode.errorqueue.clear();
            thisNode.status.standard.enable = (Node.Status.StdEvtByteBit)(4 + 8 + 16 + 32);
            thisSmux.nvbuffer1.clear();
            
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // custom setup for this instrument after it goes online    
                    instrumentChassis.Clear();
                    instrumentChassis.Write_Unchecked("*CLS", null);

                    if (TspLinkState)
                    {
                        node = new Nodes(this);
                        
                        thisNode = node[NodeNumber];
                        switch (Channel)
                        {
                            case CHANNEL.A:
                                thisSmux = node[NodeNumber].smua;
                                break;
                            case CHANNEL.B:
                                thisSmux = node[NodeNumber].smub;
                                break;
                        }
                    }
                    else
                    {
                        node = new Nodes(this);
                        thisNode = new Node(string.Empty, this);
                        switch (Channel)
                        {
                            case CHANNEL.A:
                                thisSmux = thisNode.smua;
                                break;
                            case CHANNEL.B:
                                thisSmux = thisNode.smub;
                                break;
                        }
                    }
                }
            }
        }
        #endregion

        #region Private data

        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ke260x instrumentChassis;
        #endregion

        #region InstType_TriggeredElectricalSource
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void AbortSweep()
        {
            throw new Exception("The method or operation is not implemented.");

        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void AutoZeroRefresh()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void CleanUpSweep()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ClearSweepTraceData()
        {
            
            
            throw new Exception("The method or operation is not implemented.");
            ThisSmux.nvbuffer1.clear();
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ClearSweepTriggering()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ConfigureCurrentSweepReadings()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ConfigureTriggering(int numPts, int inputTriggerLine, int outputTriggerLine, bool expectInputTrigger)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void ConfigureVoltageSweepReadings()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void DisableTriggering()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void GetSweepDataSet(out double[] SourceData, out double[] SenseData)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceIMeasureV_CurrentSweep(double Imin_A, double Imax_A, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceIMeasureV_MeasurementAccuracy(double voltageCompliance_V, double voltageRange_V, bool sourceMeasAutoDelay, double sourceMeasureDelay_s, int numberOfAverages, double integrationRate, bool AutoZeroState)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceIMeasureV_TriggeredFixedCurrent(double I, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceIMeasureV_UntriggeredSpotMeas()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceVMeasureI_MeasurementAccuracy(double currentCompliance_A, double currentRange_A, bool sourceMeasAutoDelay, double sourceMeasureDelay_s, int numberOfAverages, double integrationRate, bool AutoZeroState)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceVMeasureI_TriggeredFixedVoltage(double V, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceVMeasureI_UntriggeredSpotMeas()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void InitSourceVMeasureI_VoltageSweep(double Vmin_V, double Vmax_V, int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override float[] ParseBinaryData(byte[] buffer)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override bool SenseCurrent(double IsenseCompliance, double IsenseRange)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SenseVoltage(double VsenseComplicance, double VsenseRange)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SetCurrent(double value_amps)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SetMeasurementAccuracy(bool SourceMeasAutoDelay, double SourceMeasSpecificDelay_s, int numAverages, double integrationRate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SetMeasurementAccuracy(bool SourceMeasAutoDelay, double SourceMeasSpecificDelay_s, int numAverages, double integrationRate, bool AutoZeroState)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SetSourceDelayMeasure(bool autoDelay, double specificDelay_s)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SourceFixedCurrent()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SourceFixedVoltage()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SourceSweptCurrent(double Imin_A, double Imax_A, int numPts)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void SourceSweptVoltage(double Vmin_V, double Vmax_V, int numPts)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void StartSweep()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override void Trigger()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override bool WaitForSweepToFinish()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double CurrentActual_amp
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double CurrentSetPoint_amp
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override bool OutputEnabled
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double VoltageActual_Volt
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// This method is obsolete;
        /// </summary>
        [Obsolete("This method is obsolete; use other method instead")]
        public override double VoltageSetPoint_Volt
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

    }



}

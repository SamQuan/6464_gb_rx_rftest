// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ke260x.cs
//
// Author: tommy.yu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    /// <summary>
    /// Ke260x Chassis Driver
    /// </summary>
    public class Chassis_Ke260x : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ke260x(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            //ChassisDataRecord chassisData = new ChassisDataRecord(
            //    "Hardware_Unknown",			// hardware name 
            //    "Firmware_Unknown",			// minimum valid firmware version 
            //    "Firmware_Unknown");		// maximum valid firmware version 
            //ValidHardwareData.Add("ChassisData", chassisData);

            // Add details of Ke260x chassis,Model 2601/2602/2611/2612/2635/2636
            const string MinimumVersion = "1.0.2";
            const string MaximumVersion = "1.3.3";
            int[] Model ={ 2601, 2602, 2611, 2612, 2635, 2636 };
            foreach (int model in Model)
            {
                ChassisDataRecord chassisData = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL " + model.ToString(),	// hardware name 
                MinimumVersion,			                                    // minimum valid firmware version 
                MaximumVersion);		                                        // maximum valid firmware version 
                ValidHardwareData.Add("Ke" + model.ToString(), chassisData);
            }

        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                this.Clear();
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                return idn[3].Substring(1, 5);
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                this.Clear();
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0].Trim() + " " + idn[1].Trim();
            }
        }
        
            

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }
                
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);    
                    // needed for error detection routines
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                    
                }
            }
        }
        #endregion

        
    }
}

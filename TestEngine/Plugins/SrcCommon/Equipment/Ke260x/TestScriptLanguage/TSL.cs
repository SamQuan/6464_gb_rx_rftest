using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Threading;


namespace Bookham.TestSolution.TestScriptLanguage
{
    public interface ICommnd
    {
        void Write_Unchecked(string command);
        string Query_Unchecked(string command);
        byte[] QueryByteArray_Unchecked(string command);
        string Read_Unchecked();

    }
    public abstract class Action
    {
        protected string prefixName;
        protected readonly ICommnd Chassis;
        private readonly string strNode;
        protected static string ConcatName(string Name, string SubName)
        {
            if (string.IsNullOrEmpty(Name))
                return SubName;
            else
                return string.Concat(Name, ".", SubName);
        }
        protected Action(string prefixName, ICommnd Chassis)
        {
            this.prefixName = prefixName;
            this.Chassis = Chassis;
            strNode = GetNode(prefixName);
        }
        private string GetNode(string Command)
        {
            string strNode = string.Empty;
            string[] tmp = Command.Split('.');
            if (tmp[0].StartsWith("localnode") || tmp[0].StartsWith("node[")) strNode = tmp[0];
            return strNode;
        }

        protected string print(string Command)
        {
            string cmd = ConcatName(GetNode(Command), string.Concat("print(", Command, ")"));
            string resp = Query(cmd);
            return resp;
        }
        protected string printbuffer(string Command)
        {
            string cmd = ConcatName(GetNode(Command), string.Concat("printbuffer(", Command, ")"));
            return Query(cmd);
        }
        protected byte[] printbufferIEEE(string Command)
        {
            string cmd = ConcatName(GetNode(Command), string.Concat("printbuffer(", Command, ")"));
            return QueryIEEEBinary(cmd);

        }
        protected void Write_Unchecked(string Command)
        {
            Debug.WriteLine(Command);
            Chassis.Write_Unchecked(Command);
        }
        protected void Write(string Command)
        {
            Write(Command, 0);
        }
        protected void Write(string Command, int timeout_ms)
        {
            Write_Unchecked(Command);

            WaitComplete(timeout_ms);
            CheckExecError();
        }

        protected string Query(string Command)
        {
            Debug.WriteLine(Command);
            string resp = Chassis.Query_Unchecked(Command);
            CheckExecError();
            return resp;
        }

        protected byte[] QueryIEEEBinary(string Command)
        {
            Debug.WriteLine(Command);
            byte[] logBuffer = Chassis.QueryByteArray_Unchecked(Command);
            CheckExecError();

            if (logBuffer[0] != Convert.ToByte('#'))
                throw new Exception("Invalid IEEE754 format.");
            int noOfDigits = logBuffer[1] - Convert.ToByte('0');
            byte[] chDataSize = new byte[noOfDigits];
            for (int i = 0; i < noOfDigits; i++)
                chDataSize[i] = logBuffer[i + 2];
            int NumberOfDataByte;
            byte[] buffer;
            if (chDataSize.Length != 0)
            {
                NumberOfDataByte = (int)Convert.ToSingle(UnicodeEncoding.ASCII.GetString(chDataSize));
                buffer = new byte[NumberOfDataByte];
                for (int j = 0; j < NumberOfDataByte; j++)
                    buffer[j] = logBuffer[noOfDigits + 2 + j];
            }
            else
            {
                int len = logBuffer.Length - 3;
                buffer = new byte[len];
                for (int j = 0; j < len; j++)
                    buffer[j] = logBuffer[noOfDigits + 2 + j];
            }
            return buffer;

        }

        protected void WaitComplete(int timeout_ms)
        {
            string Command;
            if (timeout_ms < 0) timeout_ms = 0;
            Command = ConcatName(strNode, "opc()");
            Write_Unchecked(Command);

            Node.Status.StdEvtByteBit StdEvtByte = 0;
            Command = ConcatName(strNode, "print(status.standard.event)");
            Stopwatch watch = new Stopwatch();
            watch.Start();
            do
            {
                try
                {
                    
                    string StdEvt = Chassis.Query_Unchecked(Command);
                    StdEvtByte = (Node.Status.StdEvtByteBit)(byte)Convert.ToSingle(StdEvt);

                    bool flag = (StdEvtByte & Node.Status.StdEvtByteBit.OPERATION_COMPLETE)
                    == Node.Status.StdEvtByteBit.OPERATION_COMPLETE;
                    if (flag) break;
                }
                catch (Exception ex)
                {
                    //throw;
                }
                if (timeout_ms == 0) break;
                if (watch.ElapsedMilliseconds > timeout_ms + 100)
                {
                    watch.Stop();
                    throw new Exception("operation time out.");
                }
                Thread.Sleep(200);
            } while (true);
            if (watch.IsRunning) watch.Stop();
            watch.Reset();

            bool flagEXE = (StdEvtByte & Node.Status.StdEvtByteBit.EXECUTION_ERROR)
                        == Node.Status.StdEvtByteBit.EXECUTION_ERROR;
            if (flagEXE)
            {
                Command = ConcatName(strNode, "print(errorqueue.next())");
                string errMsg = Chassis.Query_Unchecked(Command);
                throw new Exception(errMsg);
            }
        }

        protected void CheckExecError()
        {
            string Command = ConcatName(strNode, "print(errorqueue.next())");
            string errMsg = Chassis.Query_Unchecked(Command);
            if (!errMsg.Contains("Queue Is Empty"))
                throw new Exception(errMsg);
        }
    }
    public class Nodes : Action
    {
        private Node[] node;
        private ICommnd subChassis;
        public Nodes(ICommnd Chassis)
            : base(string.Empty, Chassis)
        {
            this.subChassis = Chassis;
            node = new Node[64];
        }

        
        public Node this[int Number]
        {
            get
            {
                if (Number > 64 || Number < 1)
                    throw new ArgumentException("the node number should be 1 to 64");
                if (node[Number-1]==null)
                {
                    string prefixName = "node[" + Number.ToString() + "]";
                    node[Number - 1] = new Node(prefixName, subChassis);
                }
                return node[Number-1];
            }
        }
        /// <summary>
        /// Resets all nodes in a TSP-Link system.
        /// </summary>
        public void ResetAllNodes()
        {
            Write_Unchecked("reset()");
        }
        /// <summary>
        /// An abort will terminate an executing script and return all nodes to local operation 
        /// (REM annunciators turn off), dissolving the Master/Slave relationships between nodes.
        /// </summary>
        public void Abort()
        {
            Write_Unchecked("abort");
        }

    }
    public class Node : Action
    {
        public readonly smuX smua, smub;
        public readonly DigitalIO digio;
        public readonly Display display;
        public readonly ErrorQueue errorqueue;
        public readonly Format format;
        public readonly Trigger trigger;
        public readonly Gpib gpib;
        public readonly TSPLink tsplink;
        public readonly Timer timer;
        public readonly Status status;


        /// <summary>
        /// Get/Set Power line frequency.
        /// Set to 50 or 60.
        /// </summary>
        /// <remarks>
        /// To achieve optimum noise rejection when performing measurements at integer
        /// NPLC apertures, the line frequency setting must match the frequency (50Hz or 60Hz)
        /// of the AC power line.
        /// When used in an expanded system (TSP-Link), localnode.linefreq is sent to the
        /// Remote Master node only. Use node[N].linefreq (where N is the node number)
        /// to send the command to any node in the system. See Section 9 for details on TSP-Link.
        /// When this attribute is set, the localnode.autolinefreq attribute will be
        /// automatically set to false.
        /// </remarks>
        public double linefreq
        {
            get
            {
                string cmd = ConcatName(prefixName, "linefreq");
                cmd = print(cmd);
                return Convert.ToDouble(cmd);
            }
            set
            {
                if (!(value == 50 || value == 60))
                    throw new ArgumentException("line frequency should be 50 or 60");
                string cmd = ConcatName(prefixName, "linefreq = " + value.ToString());
                Write_Unchecked(cmd);
            }
        }
        /// <summary>
        /// Get/Set Automatic power line frequency detection control.
        /// </summary>
        /// <remarks>
        /// When this attribute is set to true, the power line frequency will be detected
        /// automatically the next time the SourceMeter powers up.
        /// After the power line frequency is automatically detected at power-up, the
        /// localnode.linefreq attribute will be set automatically to 50 or 60.
        /// If the localnode.linefreq attribute is explicitly set,
        /// localnode.autolinefreq will be automatically set to false.
        /// When used in an expanded system (TSP-Link), localnode.autolinefreq is
        /// sent to the Remote Master node only.
        /// Use node[N].autolinefreq (where N is the node number) to
        /// send the command to any node in the system. See Section 9 for details on
        /// TSP-Link.
        /// This command is only available in firmware version 1.2.0 and later.
        /// </remarks>
        public bool autolinefreq
        {
            get
            {
                string cmd = ConcatName(prefixName, "autolinefreq");
                cmd = print(cmd);
                return Convert.ToBoolean((int)Convert.ToSingle(cmd));
            }
            set
            {
                string cmd = ConcatName(prefixName, "autolinefreq = " + value.ToString());
                Write_Unchecked(cmd);
            }
        }
        /// <summary>
        /// Get/Set Prompting mode.
        /// Set to 0 to disable or 1 to enable.
        /// </summary>
        public int prompts
        {
            get
            {
                string cmd = ConcatName(prefixName, "prompts");
                cmd = print(cmd);
                return (int)Convert.ToSingle(cmd);
            }
            set
            {
                if (!(value == 0 || value == 1))
                    throw new ArgumentException("the value should be 0 or 1");
                string cmd = ConcatName(prefixName, "prompts =" + value.ToString());
                Write_Unchecked(cmd);
            }
        }
        /// <summary>
        /// Automatic display of errors. Set to 0 or 1.
        /// If this attribute is set to 1, for any errors that are generated, the unit will automatically
        /// display the errors stored in the error queue, and then clear the queue. Errors will be
        /// processed at the end of executing a command message (just prior to issuing a
        /// prompt if prompts are enabled).
        /// If this attribute is set to 0, errors will be left in the error queue and must be explicitly
        /// read or cleared.
        /// </summary>
        public int showerrors
        {
            get
            {
                string cmd = ConcatName(prefixName, "showerrors");
                cmd = print(cmd);
                return (int)Convert.ToSingle(cmd);
            }
            set
            {
                if (!(value == 0 || value == 1))
                    throw new ArgumentException("the value should be 0 or 1");
                string cmd = ConcatName(prefixName, "showerrors =" + value.ToString());
                Write_Unchecked(cmd);
            }
        }
        /// <summary>
        /// Delays system operation.
        /// </summary>
        /// <param name="seconds">Set delay in seconds (100000 seconds maximum).</param>
        public void delay(float seconds)
        {
            if (seconds < 50e-6) seconds = 50e-6f;
            else if (seconds > 100000) seconds = 100000;
            string cmd = ConcatName(prefixName, "delay(" + seconds + ")");
            Write_Unchecked(cmd);
            Thread.Sleep((int)(seconds * 1000));
        }
        /// <summary>
        /// Stops execution of a script.
        /// Terminates script execution when called from a script that is being executed.
        /// This command will not wait for overlapped commands to complete before terminating script
        /// execution. If overlapped commands are required to finish, use the waitcomplete function prior to calling exit.
        /// </summary>
        public void exit()
        {
            string cmd = ConcatName(prefixName, "exit()");
            Write_Unchecked(cmd);
        }
        /// <summary>
        /// Resets the logical instruments to the default settings.
        /// </summary>
        public void reset()
        {
            string cmd = ConcatName(prefixName, "reset()");
            Write_Unchecked(cmd);
        }
        /// <summary>
        /// Sets the Operation Complete status bit when all overlapped commands are completed.
        /// </summary>
        /// <remarks>
        /// This function will cause the Operation Complete bit in the Standard Event Status
        /// Register to be set when all previously started local overlapped commands are
        /// complete. Note that each node will independently set their Operation Complete bits
        /// in their own status models.
        /// Any nodes not actively performing overlapped commands will set their bits
        /// immediately. All remaining nodes will set their own bits as they complete their own
        /// overlapped commands.
        /// </remarks>
        public void opc()
        {
            string cmd = ConcatName(prefixName, "opc()");
            Write_Unchecked(cmd);
        }

        /// <summary>
        /// Only send waitcomplete() to instrument,computer no wait for instrument operation.
        /// </summary>
        public void waitcomplete()
        {
            string cmd = ConcatName(prefixName, "waitcomplete()");
            Write_Unchecked(cmd);
        }
        /// <summary>
        /// Computer Waits for all overlapped commands to complete.
        /// if time out,then throw new exception.
        /// </summary>
        public void waitcomplete(int timeout_ms)
        {
            string cmd = ConcatName(prefixName, "waitcomplete()");
            Write(cmd, timeout_ms);

        }

        public Node(string prefixName, ICommnd Chassis)
            : base(prefixName, Chassis)
        {
            digio = new DigitalIO(ConcatName(prefixName, "digio"), Chassis);
            display = new Display(ConcatName(prefixName, "display"), Chassis);
            errorqueue = new ErrorQueue(ConcatName(prefixName, "errorqueue"), Chassis);
            format = new Format(ConcatName(prefixName, "format"), Chassis);
            smua = new smuX(ConcatName(prefixName, "smua"), Chassis);
            smub = new smuX(ConcatName(prefixName, "smub"), Chassis);
            trigger = new Trigger(ConcatName(prefixName, "trigger"), Chassis);
            gpib = new Gpib(ConcatName(prefixName, "gpib"), Chassis);
            tsplink = new TSPLink(ConcatName(prefixName, "tsplink"), Chassis);
            timer = new Timer(ConcatName(prefixName, "timer"), Chassis);
            status = new Status(ConcatName(prefixName, "status"), Chassis);
        }

        public class Display : Action
        {
            #region Enum Type
            public enum Digits
            {
                /// <summary>
                /// Set display to 4.5 digits
                /// </summary>
                DIGITS_4_5,
                /// <summary>
                /// Set display to 5.5 digits.
                /// </summary>
                DIGITS_5_5,
                /// <summary>
                /// Set display to 6.5 digits.
                /// </summary>
                DIGITS_6_5
            }

            public enum MeasFunc
            {
                MEASURE_DCAMPS, MEASURE_DCVOLTS, MEASURE_OHMS, WATTS
            }
            public enum DispScreen
            {
                SMUA, SMUB, SMUA_SMUB, USER
            }

            public struct CursorPosition
            {
                /// <summary>
                /// row = 1 or 2
                /// </summary>
                public int row;
                /// <summary>
                /// column = 1 to 20 (Row 1) 
                /// column = 1 to 32 (Row 2)
                /// </summary>
                public int column;
                /// <summary>
                /// style = 0 (invisible) 
                /// style = 1 (blink)
                /// </summary>
                public int style;

            }
            #endregion
            public readonly smuX smua, smub;

            internal Display(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
                smua = new smuX(prefixName + ".smua", Chassis);
                smub = new smuX(prefixName + ".smub", Chassis);
            }
            /// <summary>
            /// Clearing the display
            /// </summary>
            public void clear()
            {
                string cmd = string.Format("{0}.clear()", prefixName);
                Write_Unchecked(cmd);
            }
            public class smuX : Action
            {
                public readonly Measure measure;

                internal smuX(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                    measure = new Measure(prefixName + ".measure", Chassis);

                }
                /// <summary>
                /// Get/Set display resolution
                /// </summary>
                public Digits digits
                {
                    get
                    {
                        string cmd = string.Format("{0}.digits", prefixName);
                        cmd = print(cmd);
                        return (Display.Digits)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.digits = display.{1}", prefixName, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }

                public class Measure : Action
                {
                    internal Measure(string Name, ICommnd Chassis)
                        : base(Name, Chassis)
                    {

                    }
                    /// <summary>
                    /// Get/Set the displayed measure function.
                    /// </summary>
                    public MeasFunc func
                    {
                        get
                        {
                            string cmd = string.Format("{0}.func", prefixName);
                            cmd = print(cmd);
                            return (MeasFunc)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.func = display.{1}", prefixName, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                }

            }
            /// <summary>
            /// Get/Set the display screen.
            /// </summary>
            public DispScreen screen
            {
                get
                {
                    string cmd = string.Format("{0}.screen", prefixName);
                    cmd = print(cmd);
                    return (DispScreen)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string cmd = string.Format("{0}.screen = display.{1}", prefixName, value.ToString().ToUpper());
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// Set Cursor position
            /// </summary>
            /// <param name="row">row = 1 or 2</param>
            /// <param name="column">
            /// column = 1 to 20 (Row 1),
            /// column = 1 to 32 (Row 2)
            /// </param>
            public void setcursor(int row, int column)
            {
                if (row < 1 || row > 2)
                    throw new ArgumentException("the row should be 1 or 2");
                if (row == 1 && (column < 1 && column > 20))
                    throw new ArgumentException("column = 1 to 20 (Row 1)");
                else if (row == 2 && (column < 1 && column > 32))
                    throw new ArgumentException("column = 1 to 32 (Row 2)");
                string cmd = string.Format("{0}.setcursor({1},{2})", prefixName, row, column);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Set Cursor position
            /// </summary>
            /// <param name="row">row = 1 or 2</param>
            /// <param name="column">
            /// column = 1 to 20 (Row 1),
            /// column = 1 to 32 (Row 2)
            /// </param>
            /// style = 0 (invisible)
            /// style = 1 (blink)
            /// </param>
            public void setcursor(int row, int column, int style)
            {
                if (row < 1 || row > 2)
                    throw new ArgumentException("the row should be 1 or 2");
                if (row == 1 && (column < 1 && column > 20))
                    throw new ArgumentException("column = 1 to 20 (Row 1)");
                else if (row == 2 && (column < 1 && column > 32))
                    throw new ArgumentException("column = 1 to 32 (Row 2)");
                if (style < 0 || style > 1)
                    throw new ArgumentException("the style should be 0(invisible)or 1(blink)");
                string cmd = string.Format("{0}.setcursor({1},{2},{3})", prefixName, row, column, style);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Return the present cursor position.
            /// </summary>
            public CursorPosition getcursor()
            {
                string cmd = string.Format("{0}.getcursor()", prefixName);
                cmd = print(cmd);
                string[] tmp = cmd.Split(',');
                int nsize = tmp.Length;
                CursorPosition cp = new CursorPosition();
                cp.row = int.MinValue;
                cp.column = int.MinValue;
                cp.style = int.MinValue;
                if (nsize == 1)
                    cp.row = (int)Convert.ToSingle(tmp[0]);
                else if (nsize == 2)
                {
                    cp.row = (int)Convert.ToSingle(tmp[0]);
                    cp.column = (int)Convert.ToSingle(tmp[nsize - 1]);
                }
                else if (nsize == 3)
                {
                    cp.row = (int)Convert.ToSingle(tmp[0]);
                    cp.column = (int)Convert.ToSingle(tmp[1]);
                    cp.style = (int)Convert.ToSingle(tmp[nsize - 1]);
                }
                return cp;
            }
            /// <summary>
            /// The method is used to define and display a message. 
            /// The message will start at the present cursor position.
            /// </summary>
            /// <param name="text">the text string to be displayed.</param>
            public void settext(string text)
            {
                string cmd = string.Format("{0}.settext({1})", prefixName, text);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Returns the displayed message.
            /// </summary>
            /// <returns></returns>
            public string gettext()
            {
                string cmd = string.Format("{0}.gettext()", prefixName);
                cmd = print(cmd);
                return cmd;
            }

        }
        public class ErrorQueue : Action
        {
            internal ErrorQueue(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
            }
            /// <summary>
            /// Clears all entries out of the error/event queue.
            /// </summary>
            public void clear()
            {
                string cmd = string.Format("{0}.clear()", prefixName);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Get the number of entries in the error/event queue.
            /// </summary>
            /// <returns></returns>
            public int count
            {
                get
                {
                    string cmd = string.Format("{0}.count", prefixName);
                    cmd = print(cmd);
                    return (int)Convert.ToSingle(cmd);
                }
            }
            public struct MsgStruct
            {
                /// <summary>
                /// Returns the error code number for the ent
                /// </summary>
                public int errorcode;
                /// <summary>
                /// Returns the severity level (0, 10, 20, 30 or 40).
                /// </summary>
                public string severity;
                /// <summary>
                /// Returns the node number where the error originated.
                /// </summary>
                public int node;
                /// <summary>
                /// Returns the message that describes the entry.
                /// </summary>
                public string message;

                public string RawMessage;
            }
            /// <summary>
            /// Reads an entry from the error/event queue.
            /// If there are no entries in the queue, code 0, "Queue Is Empty" is returned.
            /// </summary>
            /// <returns></returns>
            /// <remarks>
            /// Returned severity levels include the following:
            /// 0 Informational 每 Indicates no error: "Queue is Empty".
            /// 10 Informational 每 Indicates an event or a minor error. Examples: ※Reading
            /// Available§ and ※Reading Overflow§.
            /// 20 Recoverable 每 Indicates possible invalid user input. Operation will continue
            /// but action should be taken to correct the error. Examples: "Exponent Too
            /// Large" and "Numeric Data Not Allowed".
            /// 30 Serious 每 Indicates a serious error and may require technical assistance.
            /// Example: "Saved calibration constants corrupted".
            /// 40 Fatal 每 Indicates that the Series 2600 is non-operational and will require
            /// service. Contact information for service is provided in Section 1. Examples:
            /// "Bad SMU AFPGA image size", "SMU is unresponsive" and "Communication
            /// Timeout with DFPGA".
            /// </remarks>
            public MsgStruct next()
            {
                string cmd = string.Format("{0}.next()", prefixName);
                cmd = print(cmd);
                //errorcode, message, severity, node = errorqueue.next()
                MsgStruct ms = new MsgStruct();
                ms.RawMessage = cmd;
                string[] tmp = cmd.Split('\t');
                try
                {
                    ms.errorcode = (int)Convert.ToSingle(tmp[0]);
                    ms.message = tmp[1];
                    ms.severity = tmp[2];//(int)Convert.ToSingle(tmp[2]);
                    ms.node = (int)Convert.ToSingle(tmp[3]);
                }
                catch (Exception ex)
                {
                    //throw;
                }
                return ms;
            }
        }
        /// <summary>
        /// The format class are used to configure the output formats used by the print and 
        /// printbuffer functions. These attributes are used to set the data format (ASCII or binary), ASCII 
        /// precision (number of digits) and binary byte order (normal or swapped).
        /// </summary>
        public class Format : Action
        {
            public enum ByteOrderFmt
            {
                /// <summary>
                /// Most significant byte first.
                /// </summary>
                NORMAL = 0,
                /// <summary>
                /// Least significant byte first.
                /// </summary>
                SWAPPED = 1
            }
            public enum DataFmt
            {
                /// <summary>
                /// ASCII format.
                /// </summary>
                ASCII = 1,
                /// <summary>
                /// Single precision IEEE-754 binary format.
                /// </summary>
                REAL32,
                /// <summary>
                /// Double precision IEEE-754 binary format.
                /// </summary>
                REAL64
            }
            internal Format(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
            }
            /// <summary>
            /// Get/Set the precision (number of digits) for all numbers printed with the ASCII format.
            /// Set from 1 to 16.
            /// </summary>
            /// <remarks>
            /// This attribute selects the precision (number of digits) for data printed with the print,
            /// printnumber and printbuffer functions. The precision attribute is only used with
            /// the ASCII format. The precision must be a number between 1 and 16.
            /// Note that the precision is the number of significant digits printed. There will always
            /// be one digit to the left of the decimal point. Be sure to include this digit when setting the precision.
            /// The default (reset) precision is 6.
            /// </remarks>
            public int asciiprecision
            {
                get
                {
                    string cmd = string.Format("{0}.asciiprecision", prefixName);
                    cmd = print(cmd);
                    return (int)Convert.ToSingle(cmd);
                }
                set
                {
                    //if (value < 0)
                    //    throw new ArgumentException("the precision value should be from 1 to 16");
                    if (value < 1)
                        value = 1;
                    else if (value > 16)
                        value = 16;
                    string cmd = string.Format("{0}.asciiprecision = {1}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// The binary byte order for data printed using the print,printnumber and printbuffer functions.
            /// </summary>
            /// <remarks>
            /// This attribute selects the byte order that data is written when printing data values
            /// with the printnumber and the printbuffer functions. The byte order attribute is
            /// only used with the SREAL, REAL, REAL32, and REAL64 data formats.
            /// NORMAL, BIGENDIAN, and NETWORK select the same byte order. SWAPPED
            /// and LITTLEENDIAN select the same byte order. They are alternative identifiers.
            /// Selecting which to use is a matter of preference.
            /// Select the SWAPPED or LITTLEENDIAN byte order when sending data to an IBM PC compatible computer.
            /// </remarks>
            public ByteOrderFmt byteorder
            {
                get
                {
                    string cmd = string.Format("{0}.byteorder", prefixName);
                    cmd = print(cmd);
                    return (ByteOrderFmt)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string cmd = string.Format("{0}.byteorder = format.{1}", prefixName, value.ToString().ToUpper());
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// The data format for data printed using the printnumber and printbuffer functions.
            /// </summary>
            /// <remarks>
            /// This attribute selects the data format used to print data values with the
            /// printnumber and printbuffer functions.
            /// The precision of the ASCII format can be controlled with the format.asciiprecision
            /// attribute. The byte order of SREAL, REAL, REAL32, and REAL64 can be selected
            /// with the format.byteorder attribute.
            /// REAL32 and SREAL select the same single precision format. REAL and REAL64
            /// select the same double precision format. They are alternative identifiers. Selecting
            /// which to use is a matter of preference.
            /// The IEEE-754 binary formats use 4 bytes each for single precision values and 8
            /// bytes each for double precision values.
            /// When data is written with any of the binary formats, the response message will start
            /// with ※#0§ and end with a new line. When data is written with the ASCII format,
            /// elements will be separated with a comma and space.
            /// </remarks>
            public DataFmt data
            {
                get
                {
                    string cmd = string.Format("{0}.data", prefixName);
                    cmd = print(cmd);
                    return (DataFmt)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string cmd = string.Format("{0}.data = format.{1}", prefixName, value.ToString().ToUpper());
                    Write_Unchecked(cmd);
                }
            }
        }
        /// <summary>
        /// NOTE The digital I/O lines can be used for both input and output. You must
        /// write a 1 to all digital I/O lines that are to be used as inputs.
        /// </summary>
        public class DigitalIO : Action
        {
            public readonly Triggers trigger;

            internal DigitalIO(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {

                trigger = new Triggers(prefixName, Chassis);
            }

            #region Commands for basic I/O

            /// <summary>
            /// Read one digital I/O input line.
            /// </summary>
            /// <param name="bit">Line 1 to 14</param>
            /// <returns>1 or 0</returns>
            public int readbit(int bit)
            {
                if (bit < 1 || bit > 14)
                    throw new ArgumentException("the bit should be within in the range of 1 to 14");
                string cmd = string.Format("{0}.readbit({1})", prefixName, bit);
                cmd = print(cmd);
                return (int)Convert.ToSingle(cmd);
            }
            /// <summary>
            /// Read digital I/O port.
            /// </summary>
            /// <returns>0 to 16383</returns>
            public int readport()
            {
                string cmd = string.Format("{0}.readport()", prefixName);
                cmd = print(cmd);
                return (int)Convert.ToSingle(cmd);
            }
            /// <summary>
            /// Write data to one digital I/O output line.
            /// </summary>
            /// <param name="bit">Line #1 to 14</param>
            /// <param name="data">1 = high; 0 = low,</param>
            public void writebit(int bit, int data)
            {
                if (bit < 1 || bit > 14)
                    throw new ArgumentException("the bit should be within in the range of 1 to 14");
                if (data < 0 || data > 1)
                    throw new ArgumentException("the data should be within in the range of 0 to 1");

                string cmd = string.Format("{0}.writebit({1},{2})", prefixName, bit, data);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Write data to digital I/O port.
            /// </summary>
            /// <param name="data">data is from 0 to 16383</param>
            public void writeport(int data)
            {
                if (data < 0 || data > 16383)
                    throw new ArgumentException("the data should be within in the range of 0 to 16,383");
                string cmd = string.Format("{0}.writeport({1})", prefixName, data);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Get/Set Write protect mask to digital I/O port.(0~16383)
            /// To remove write protection, entering the same value.
            /// </summary>
            public int writeprotect
            {
                get
                {
                    string cmd = string.Format("{0}.writeprotect", prefixName);
                    cmd = print(cmd);
                    return (int)Convert.ToSingle(cmd);
                }
                set
                {
                    if (value < 0 || value > 16383)
                        throw new ArgumentException("write protect digital I/O line(s) within the range of 0 to 16,383");
                    string cmd = string.Format("{0}.writeprotect={1}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }

            #endregion

            #region Commands for digital I/O triggering

            public class Triggers : Action
            {
                Trigger[] trigs;
                ICommnd subChassis;
                internal Triggers(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                    this.subChassis = Chassis;
                    trigs = new Trigger[14];
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="line">from 1 to 14</param>
                /// <returns></returns>
                public Trigger this[int line]
                {
                    get
                    {
                        if (line < 1 || line > 14)
                            throw new ArgumentException("the line should be within in the range of 1 to 14");
                        if (trigs[line - 1] == null)
                        {
                            string strName = ConcatName(prefixName, "trigger[" + line.ToString() + "]");
                            trigs[line - 1] = new Trigger(strName, subChassis);
                        }
                        return trigs[line - 1];
                    }
                }
            }

            public class Trigger : Action
            {

                internal Trigger(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                }

                /// <summary>
                /// Generate a trigger on digital I/O line.
                /// </summary>
                public void assert()
                {
                    string cmd = string.Format("{0}.assert()", prefixName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Clear the event detector for a trigger.
                /// </summary>
                public void clear()
                {
                    string cmd = string.Format("{0}.clear()", prefixName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Release latched trigger.
                /// </summary>
                public void release()
                {
                    string cmd = string.Format("{0}.release()", prefixName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Get/Set trigger pulse width.(in seconds)
                /// </summary>
                public double pulsewidth
                {
                    get
                    {
                        string cmd = string.Format("{0}.pulsewidth", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.pulsewidth = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }

                /// <summary>
                /// Wait for a trigger event. (In seconds)
                /// </summary>
                /// <param name="timeout">Maximum amount of time in seconds to wait for the trigger.</param>
                /// <returns>Returns true if a trigger was detected. Returns false if no 
                /// triggers were detected during the timeout period.
                /// </returns>
                public bool wait(int timeout_s)
                {
                    if (timeout_s < 0) timeout_s = 0;
                    string cmd = string.Format("{0}.wait({1})", prefixName, timeout_s);
                    Write_Unchecked(cmd);
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    do
                    {
                        try
                        {
                            cmd = false.ToString();
                            cmd = Chassis.Read_Unchecked();
                            break;
                        }
                        catch (Exception ex)
                        {
                            //throw;
                        }
                        if (watch.ElapsedMilliseconds > timeout_s * 1000 + 500)
                        {
                            watch.Stop();
                            throw new Exception("operation time out.");
                        }
                        Thread.Sleep(100);
                    } while (true);
                    if (watch.IsRunning) watch.Stop();
                    watch.Reset();

                    return Convert.ToBoolean(cmd);
                }
                public enum digio
                {
                    /// <summary>
                    /// falling edge triggers
                    /// </summary>
                    TRIG_FALLING,
                    /// <summary>
                    /// rising edge triggers
                    /// </summary>
                    TRIG_RISING,
                    /// <summary>
                    /// falling or rising triggers
                    /// </summary>
                    TRIG_EITHER,
                    /// <summary>
                    /// detect and latch falling edge triggers
                    /// </summary>
                    TRIG_SYNCHRONOUS
                }
                /// <summary>
                /// Control I/O trigger event detector mode
                /// </summary>
                public digio mode
                {
                    get
                    {
                        string cmd = string.Format("{0}.mode", prefixName);
                        cmd = print(cmd);
                        return (digio)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.mode = digio.{1}", prefixName, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
            }

            #endregion
        }
        public class smuX : Action
        {
            public readonly nvbufferX nvbuffer1, nvbuffer2;
            public readonly Source source;
            public readonly Measure measure;

            internal smuX(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
                source = new Source(prefixName + ".source", Chassis);
                measure = new Measure(prefixName + ".measure", Chassis);
                nvbuffer1 = new nvbufferX(prefixName + ".nvbuffer1", Chassis);
                nvbuffer2 = new nvbufferX(prefixName + ".nvbuffer2", Chassis);

            }

            #region Enum type

            public enum autorangeY
            {
                AUTORANGE_OFF, AUTORANGE_ON
            }
            public enum AutoZero
            {
                /// <summary>
                /// Disable auto zero.
                /// </summary>
                AUTOZERO_OFF,
                /// <summary>
                /// Force one ref and zero.
                /// </summary>
                AUTOZERO_ONCE,
                /// <summary>
                /// Force ref and zero with each measurement.
                /// </summary>
                AUTOZERO_AUTO
            }
            public enum Filter
            {
                FILTER_OFF, FILTER_ON
            }
            public enum FilterType
            {
                /// <summary>
                /// Select moving average filter type.
                /// </summary>
                FILTER_MOVING_AVG,
                /// <summary>
                /// Select repeat filter type.
                /// </summary>
                FILTER_REPEAT_AVG,
                /// <summary>
                /// Select median filter type.
                /// </summary>
                FILTER_MEDIAN

            }
            public enum RelEnableY
            {
                REL_OFF, REL_ON
            }
            public enum Func
            {
                /// <summary>
                /// Select current source function.
                /// </summary>
                OUTPUT_DCAMPS,
                /// <summary>
                /// Select voltage source function.
                /// </summary>
                OUTPUT_DCVOLTS,
            }
            public enum OffMode
            {
                /// <summary>
                /// Selects normal output-off state.
                /// </summary>
                OUTPUT_NORMAL,
                /// <summary>
                /// Selects zero output-off state.
                /// </summary>
                OUTPUT_ZERO,
                /// <summary>
                /// Selects high-impedance outputoffstate.
                /// </summary>
                OUTPUT_HIGH_Z
            }
            public enum Output
            {
                /// <summary>
                /// Turn off source output.
                /// </summary>
                OUTPUT_OFF,
                /// <summary>
                /// Turn on source output.
                /// </summary>
                OUTPUT_ON,

            }

            public enum OutputEnableAction
            {
                OE_NONE, OE_OUTPUT_OFF
            }
            public enum Sense
            {
                /// <summary>
                /// Selects local (2-wire) sense.
                /// </summary>
                SENSE_LOCAL,
                /// <summary>
                /// Selects remote (4-wire) sense.
                /// </summary>
                SENSE_REMOTE

            }
            #endregion
            public struct IvStruct
            {
                public double i, v;
            }
            /// <summary>
            /// Turns off the output and resets the SMU to the default settings.
            /// </summary>
            public void reset()
            {
                string cmd = string.Format("{0}.reset()", prefixName);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Sense mode selection
            /// </summary>
            public Sense sense
            {
                get
                {
                    string cmd = string.Format("{0}.sense", prefixName);
                    cmd = print(cmd);
                    return (Sense)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string[] tmp = prefixName.Trim().Split('.');
                    string smux = tmp[tmp.Length - 1];
                    string cmd = string.Format("{0}.sense = {1}.{2}", prefixName, smux, value.ToString().ToUpper());
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// Performs two measurements and then steps the source.
            /// </summary>
            /// <param name="sourcevalue">Source value to be set after the measurement is made.</param>
            /// <returns>Returns the two measured readings before stepping the source.</returns>
            /// <remarks>
            /// The specified source value should be appropriate for the selected source function.
            /// For example, if the source voltage function is selected, then sourcevalue is
            /// expected to be a new voltage level.
            /// Both source and measure auto range must be disabled before using this function.
            /// This function is provided for very fast execution of source-measure loops. The
            /// measurement will be made prior to stepping the source. Prior to using this function,
            /// and before any loop this function may be used in, the source value should be set to
            /// its initial level.
            /// </remarks>
            public IvStruct measureivandstep(double sourcevalue)
            {
                string cmd = string.Format("{0}.measureivandstep({1})", prefixName, sourcevalue);
                cmd = print(cmd);
                string[] reading = cmd.Split('\t');
                IvStruct riv = new IvStruct();
                riv.i = Convert.ToDouble(reading[0]);
                riv.v = Convert.ToDouble(reading[1]);
                return riv;
            }
            /// <summary>
            /// Performs one measurements and then steps the source.
            /// </summary>
            /// <param name="sourcevalue">Source value to be set after the measurement is made.</param>
            /// <returns>Returns the measured reading before stepping the source.</returns>
            /// <remarks>
            /// The specified source value should be appropriate for the selected source function.
            /// For example, if the source voltage function is selected, then sourcevalue is
            /// expected to be a new voltage level.
            /// Both source and measure auto range must be disabled before using this function.
            /// This function is provided for very fast execution of source-measure loops. The
            /// measurement will be made prior to stepping the source. Prior to using this function,
            /// and before any loop this function may be used in, the source value should be set to
            /// its initial level.
            /// </remarks>
            /// <example>
            /// This Model 2601/2602 measure and step function measures current starting at a
            /// source value of 0V. After each current measurement, the source is stepped 100mV
            /// for the next current measurement. The final source level is 1V where current is
            /// again measured.
            /// local ivalues = {}
            /// smua.source.rangev = 1
            /// smua.source.levelv = 0
            /// smua.measure.rangei = 0.01
            /// smua.source.output = smua.OUTPUT_ON
            /// for index = 1, 10 do
            ///     ivalues[index] = smua.measureiandstep(index / 10)
            /// end
            /// ivalues[11] = smua.measure.i()
            /// </example>
            public double measureIandstep(double sourcevalue)
            {
                string cmd = string.Format("{0}.measureiandstep({1})", prefixName, sourcevalue);
                cmd = print(cmd);
                //if (cmd == "nil") return double.NaN;
                return Convert.ToDouble(cmd);
            }
            /// <summary>
            /// Performs one measurements and then steps the source.
            /// </summary>
            /// <param name="sourcevalue">Source value to be set after the measurement is made.</param>
            /// <returns>Returns the measured reading before stepping the source.</returns>
            /// <remarks>
            /// The specified source value should be appropriate for the selected source function.
            /// For example, if the source voltage function is selected, then sourcevalue is
            /// expected to be a new voltage level.
            /// Both source and measure auto range must be disabled before using this function.
            /// This function is provided for very fast execution of source-measure loops. The
            /// measurement will be made prior to stepping the source. Prior to using this function,
            /// and before any loop this function may be used in, the source value should be set to
            /// its initial level.
            /// </remarks>
            /// <example>
            /// This Model 2601/2602 measure and step function measures current starting at a
            /// source value of 0V. After each current measurement, the source is stepped 100mV
            /// for the next current measurement. The final source level is 1V where current is
            /// again measured.
            /// local ivalues = {}
            /// smua.source.rangev = 1
            /// smua.source.levelv = 0
            /// smua.measure.rangei = 0.01
            /// smua.source.output = smua.OUTPUT_ON
            /// for index = 1, 10 do
            ///     ivalues[index] = smua.measureiandstep(index / 10)
            /// end
            /// ivalues[11] = smua.measure.i()
            /// </example>
            public double measureVandstep(double sourcevalue)
            {
                string cmd = string.Format("{0}.measurevandstep({1})", prefixName, sourcevalue);
                cmd = print(cmd);
                //if (cmd == "nil") return double.NaN;
                return Convert.ToDouble(cmd);
            }
            /// <summary>
            /// Performs one measurements and then steps the source.
            /// </summary>
            /// <param name="sourcevalue">Source value to be set after the measurement is made.</param>
            /// <returns>Returns the measured reading before stepping the source.</returns>
            /// <remarks>
            /// The specified source value should be appropriate for the selected source function.
            /// For example, if the source voltage function is selected, then sourcevalue is
            /// expected to be a new voltage level.
            /// Both source and measure auto range must be disabled before using this function.
            /// This function is provided for very fast execution of source-measure loops. The
            /// measurement will be made prior to stepping the source. Prior to using this function,
            /// and before any loop this function may be used in, the source value should be set to
            /// its initial level.
            /// </remarks>
            /// <example>
            /// This Model 2601/2602 measure and step function measures current starting at a
            /// source value of 0V. After each current measurement, the source is stepped 100mV
            /// for the next current measurement. The final source level is 1V where current is
            /// again measured.
            /// local ivalues = {}
            /// smua.source.rangev = 1
            /// smua.source.levelv = 0
            /// smua.measure.rangei = 0.01
            /// smua.source.output = smua.OUTPUT_ON
            /// for index = 1, 10 do
            ///     ivalues[index] = smua.measureiandstep(index / 10)
            /// end
            /// ivalues[11] = smua.measure.i()
            /// </example>
            public double measureRandstep(double sourcevalue)
            {
                string cmd = string.Format("{0}.measurerandstep({1})", prefixName, sourcevalue);
                cmd = print(cmd);
                return Convert.ToDouble(cmd);
            }
            /// <summary>
            /// Performs one measurements and then steps the source.
            /// </summary>
            /// <param name="sourcevalue">Source value to be set after the measurement is made.</param>
            /// <returns>Returns the measured reading before stepping the source.</returns>
            /// <remarks>
            /// The specified source value should be appropriate for the selected source function.
            /// For example, if the source voltage function is selected, then sourcevalue is
            /// expected to be a new voltage level.
            /// Both source and measure auto range must be disabled before using this function.
            /// This function is provided for very fast execution of source-measure loops. The
            /// measurement will be made prior to stepping the source. Prior to using this function,
            /// and before any loop this function may be used in, the source value should be set to
            /// its initial level.
            /// </remarks>
            /// <example>
            /// This Model 2601/2602 measure and step function measures current starting at a
            /// source value of 0V. After each current measurement, the source is stepped 100mV
            /// for the next current measurement. The final source level is 1V where current is
            /// again measured.
            /// local ivalues = {}
            /// smua.source.rangev = 1
            /// smua.source.levelv = 0
            /// smua.measure.rangei = 0.01
            /// smua.source.output = smua.OUTPUT_ON
            /// for index = 1, 10 do
            ///     ivalues[index] = smua.measureiandstep(index / 10)
            /// end
            /// ivalues[11] = smua.measure.i()
            /// </example>
            public double measurePandstep(double sourcevalue)
            {
                string cmd = string.Format("{0}.measurepandstep({1})", prefixName, sourcevalue);
                cmd = print(cmd);
                return Convert.ToDouble(cmd);
            }

            public struct BufferData
            {
                private double[] Measured, Tstamps, SrcVal;
                /// <summary>
                /// Get readings
                /// </summary>
                public double[] MeasuredReadings
                {
                    get
                    {
                        return Measured;
                    }
                    internal set
                    {
                        Measured = value;
                    }
                }
                /// <summary>
                /// Get TimeStamps
                /// </summary>
                public double[] TimeStamps
                {
                    get
                    {
                        return Tstamps;
                    }
                    internal set
                    {
                        Tstamps = value;
                    }
                }
                /// <summary>
                /// Get SourceValues
                /// </summary>
                public double[] SourceValues
                {
                    get
                    {
                        return SrcVal;
                    }
                    internal set
                    {
                        SrcVal = value;
                    }
                }
            }
            /// <summary>
            /// NOTE:You must clear the buffer using the smuX.nvbufferX.clear() 
            /// command before changing buffer control attributes.
            /// </summary>
            public class nvbufferX : Action
            {
                ICommnd subChassis;
                internal nvbufferX(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                    this.subChassis = Chassis;
                }

                internal string BufferFullName
                {
                    get
                    {
                        return prefixName;
                    }
                }

                /// <summary>
                /// Clear Buffer
                /// NOTE:You must clear the buffer using the smuX.nvbufferX.clear() 
                /// command before changing buffer control attributes.
                /// </summary>
                public void clear()
                {
                    string cmd = string.Format("{0}.clear()", prefixName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Print data from buffer.
                /// </summary>
                /// <param name="startIndex">Starting index of values to print,the first element index is 1,not 0. </param>
                /// <param name="endIndex">Ending index of values to print</param>
                /// <param name="printTimeStamp">Print the timestamps? true : print; false : no print</param>
                /// <param name="printSrcVal">Print the source values? true : print; false : no print</param>
                /// <returns>The output is returned in the following order:
                /// reading1, [timestamp1], [sourcevalue1], reading2, [timestamp2], [sourcevalue2],......
                /// </returns>
                /// <remarks>
                /// When data is written with any of the binary formats, the response message will start
                /// with ※#0§ and end with a new line. When data is written with the ASCII format,
                /// elements will be separated with a comma and space.
                /// </remarks>
                public BufferData PrintBuffer(int startIndex, int endIndex,
                    bool printTimeStamp, bool printSrcVal)
                {
                    BufferData bd = new BufferData();

                    WaitComplete(180 * 1000);
                    int n = this.n;
                    if (n < 1) return bd;
                    int timestamps = collecttimestamps;
                    int sourcevalues = collectsourcevalues;
                    if (startIndex < 1) startIndex = 1;
                    if (n < endIndex) endIndex = n;
                    if (startIndex > endIndex)
                        throw new ArgumentException("startIndex can not more than endIndex!");
                    //Get data format
                    string[] tmp = prefixName.Split('.');
                    tmp[tmp.Length - 2] = "format";
                    Format format = new Format(string.Join(".", tmp, 0, tmp.Length - 1), subChassis);
                    Format.DataFmt datafmt = format.data;
                    int MaxNumber = 300;
                    switch (datafmt)
                    {
                        case Format.DataFmt.ASCII:
                            MaxNumber = 300;
                            if (printTimeStamp && 1 == timestamps) MaxNumber -= 50;
                            if (printSrcVal && 1 == sourcevalues) MaxNumber -= 50;
                            if (MaxNumber < 200) MaxNumber = 200;
                            break;
                        case Format.DataFmt.REAL32:
                            MaxNumber = 1000;
                            if (printTimeStamp && 1 == timestamps) MaxNumber -= 1000;
                            if (printSrcVal && 1 == sourcevalues) MaxNumber -= 1000;
                            if (MaxNumber < 1000) MaxNumber = 1000;
                            break;
                        case Format.DataFmt.REAL64:
                            MaxNumber = 500;
                            if (printTimeStamp && 1 == timestamps) MaxNumber -= 500;
                            if (printSrcVal && 1 == sourcevalues) MaxNumber -= 500;
                            if (MaxNumber < 500) MaxNumber = 500;
                            break;
                    }

                    if (endIndex - startIndex + 1 <= MaxNumber)
                        bd = PrintBuffer(startIndex, endIndex, printTimeStamp, printSrcVal);
                    else
                    {
                        List<double> MeasuredReadings = new List<double>();
                        List<double> TimeStamps = new List<double>();
                        List<double> SourceValues = new List<double>();
                        for (int i = startIndex; i <= endIndex; i += MaxNumber)
                        {
                            bd = PrintBuffer(i, Math.Min(i + MaxNumber - 1, endIndex), printTimeStamp, printSrcVal);
                            MeasuredReadings.AddRange(bd.MeasuredReadings);
                            TimeStamps.AddRange(bd.TimeStamps);
                            SourceValues.AddRange(bd.SourceValues);
                        }
                        bd.MeasuredReadings = MeasuredReadings.ToArray();
                        bd.SourceValues = SourceValues.ToArray();
                        bd.TimeStamps = TimeStamps.ToArray();
                    }
                    return bd;
                }

                /// <summary>
                /// Print data from buffer.
                /// </summary>
                /// <param name="startIndex">Starting index of values to print,the first element index is 1,not 0. </param>
                /// <param name="endIndex">Ending index of values to print</param>
                /// <param name="printTimeStamp">Print the timestamps? true : print; false : no print</param>
                /// <param name="printSrcVal">Print the source values? true : print; false : no print</param>
                /// <returns>The output is returned in the following order:
                /// reading1, [timestamp1], [sourcevalue1], reading2, [timestamp2], [sourcevalue2],......
                /// </returns>
                /// <remarks>
                /// When data is written with any of the binary formats, the response message will start
                /// with ※#0§ and end with a new line. When data is written with the ASCII format,
                /// elements will be separated with a comma and space.
                /// </remarks>
                private BufferData printbuffer(int startIndex, int endIndex,
                    bool printTimeStamp, bool printSrcVal)
                {
                    WaitComplete(60 * 1000);
                    int n = this.n;
                    BufferData BD = new BufferData();
                    if (n < 1) return BD;
                    int timestamps = collecttimestamps;
                    int sourcevalues = collectsourcevalues;
                    if (startIndex < 1) startIndex = 1;
                    if (n < endIndex) endIndex = n;
                    if (startIndex > endIndex)
                        throw new ArgumentException("startIndex can not more than endIndex!");
                    string cmd = string.Format("{0},{1},{2}", startIndex, endIndex, prefixName);
                    if (printTimeStamp && 1 == timestamps)
                        cmd += string.Format(",{0}.timestamps", prefixName);
                    if (printSrcVal && 1 == sourcevalues)
                        cmd += string.Format(",{0}.sourcevalues", prefixName);

                    List<double> meas = new List<double>(), stamp = new List<double>(), sour = new List<double>();
                    //Get data format
                    string[] tmp = prefixName.Split('.');
                    tmp[tmp.Length - 2] = "format";
                    Format format = new Format(string.Join(".", tmp, 0, tmp.Length - 1), subChassis);
                    Format.DataFmt datafmt = format.data;
                    switch (datafmt)
                    {
                        case Format.DataFmt.ASCII:
                            cmd = printbuffer(cmd);
                            tmp = cmd.Split(',');
                            IEnumerator ie = tmp.GetEnumerator();
                            while (ie.MoveNext())
                            {
                                meas.Add(Convert.ToDouble(ie.Current));
                                if (printTimeStamp && 1 == timestamps)
                                {
                                    ie.MoveNext();
                                    stamp.Add(Convert.ToDouble(ie.Current));
                                }
                                if (printSrcVal && 1 == sourcevalues)
                                {
                                    ie.MoveNext();
                                    sour.Add(Convert.ToDouble(ie.Current));
                                }
                            }
                            break;
                        case Format.DataFmt.REAL32:
                            byte[] byt = printbufferIEEE(cmd);
                            int nSize = sizeof(float);
                            for (int i = 0; i < byt.Length; i += nSize)
                            {
                                meas.Add(BitConverter.ToSingle(byt, i));
                                if (printTimeStamp && 1 == timestamps)
                                {
                                    i += nSize;
                                    stamp.Add(BitConverter.ToSingle(byt, i));
                                }
                                if (printSrcVal && 1 == sourcevalues)
                                {
                                    i += nSize;
                                    sour.Add(BitConverter.ToSingle(byt, i));
                                }
                            }
                            break;
                        case Format.DataFmt.REAL64:
                            byt = printbufferIEEE(cmd);
                            nSize = sizeof(double);
                            for (int i = 0; i < byt.Length; i += nSize)
                            {
                                meas.Add(BitConverter.ToDouble(byt, i));
                                if (printTimeStamp && 1 == timestamps)
                                {
                                    i += nSize;
                                    stamp.Add(BitConverter.ToDouble(byt, i));
                                }
                                if (printSrcVal && 1 == sourcevalues)
                                {
                                    i += nSize;
                                    sour.Add(BitConverter.ToDouble(byt, i));
                                }
                            }
                            break;
                    }
                    BD.MeasuredReadings = meas.ToArray();
                    BD.TimeStamps = stamp.ToArray();
                    BD.SourceValues = sour.ToArray();
                    return BD;
                }


                #region Buffer storage control attributes

                /// <summary>
                /// Get/Set the append modes are either off (=0) or on (=1). When the append mode is off, a new
                /// measurement to this buffer will overwrite the previous contents. When the
                /// append mode is on, the first new measurement will be stored at what was
                /// formerly rb[n+1]. This attribute is initialized to off when the buffer is created.
                /// </summary>
                public int appendmode
                {
                    get
                    {
                        string cmd = string.Format("{0}.appendmode", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        if (value < 0 || value > 1)
                            throw new ArgumentException("the value should be 0 or 1");
                        string cmd = string.Format("{0}.appendmode = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set collecttimestamps.
                /// When this attribute is on(=1,off=0), timestamps will be stored with readings in the
                /// buffer. This requires 4 extra bytes of storage per reading. This value, off or
                /// on, can only be changed when the buffer is empty. When the buffer is created,
                /// this attribute is initialized to off.
                /// </summary>
                public int collecttimestamps
                {
                    get
                    {
                        string cmd = string.Format("{0}.collecttimestamps", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        if (value < 0 || value > 1)
                            throw new ArgumentException("the value should be 0 or 1");
                        string cmd = string.Format("{0}.collecttimestamps = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set collectsourcevalues.
                /// When this attribute is on(=1,off=0), source values will be stored with readings in the
                /// buffer. This requires 4 extra bytes of storage per reading. This value, off or
                /// on, can only be changed when the buffer is empty. When the buffer is created,
                /// this attribute is initialized to off.
                /// </summary>
                public int collectsourcevalues
                {
                    get
                    {
                        string cmd = string.Format("{0}.collectsourcevalues", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        if (value < 0 || value > 1)
                            throw new ArgumentException("the value should be 0 or 1");
                        string cmd = string.Format("{0}.collectsourcevalues = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set timestamp resolution (in seconds).
                /// The timestamp resolution, in seconds. When the buffer is created, its initial
                /// resolution is 0.000001 seconds. At this resolution, the reading buffer can
                /// store unique timestamps for up to 71 minutes. This value can be increased
                /// for very long tests. 
                /// Note: The minimum resolution setting is 1米s (0.000001 seconds).
                /// </summary>
                public double timestampresolution
                {
                    get
                    {
                        string cmd = string.Format("{0}.timestampresolution", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        if (value < 0.000001 || value > 71 * 60)
                            throw new ArgumentException("the value should be 0.000001 or 71*60 seconds.");
                        string cmd = string.Format("{0}.timestampresolution = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }

                #endregion

                #region Buffer read-only attributes

                /// <summary>
                /// Get the number of readings in the reading buffer.
                /// </summary>
                public int n
                {
                    get
                    {
                        string cmd = string.Format("{0}.n", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                }
                /// <summary>
                /// Get The timestamp of when the reading at rb[1] was stored, in seconds from power-up.
                /// </summary>
                public double basetimestamp
                {
                    get
                    {
                        string cmd = string.Format("{0}.basetimestamp", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                }
                /// <summary>
                /// Get the total number of readings that can be stored in the reading buffer.
                /// </summary>
                public int capacity
                {
                    get
                    {
                        string cmd = string.Format("{0}.capacity", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                }

                #endregion

                #region Buffer reading attributes


                #endregion

            }
            public class Measure : Action
            {
                readonly string smuX;
                public readonly Filter filter;
                public readonly Rel rel;

                internal Measure(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                    string[] tmp = prefixName.Split('.');
                    smuX = tmp[tmp.Length - 2];

                    filter = new Filter(prefixName + ".filter", Chassis);
                    rel = new Rel(prefixName + ".rel", Chassis);

                }
                /// <summary>
                /// Get/Set current measure auto range.
                /// </summary>
                public smuX.autorangeY autorangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.autorangei", prefixName);
                        cmd = print(cmd);
                        return (smuX.autorangeY)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.autorangei = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage measure auto range.
                /// </summary>
                public smuX.autorangeY autorangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.autorangev", prefixName);
                        cmd = print(cmd);
                        return (smuX.autorangeY)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.autorangev = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Auto zero command
                /// </summary>
                public smuX.AutoZero autozero
                {
                    get
                    {
                        string cmd = string.Format("{0}.autozero", prefixName);
                        cmd = print(cmd);
                        return (smuX.AutoZero)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.autozero = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set the number of measurements performed when a measurement is requested.
                /// This attribute controls the number of measurements taken any time a measurement
                /// is requested. When using a reading buffer with a measure command, the count also
                /// controls the number of readings to be stored.
                /// The reset function sets the measure count to 1.
                /// </summary>
                public int count
                {
                    get
                    {
                        string cmd = string.Format("{0}.count", prefixName);
                        cmd = print(cmd);
                        return (int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        if (value < 1) value = 1;
                        string cmd = string.Format("{0}.count = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Interval between multiple measurements.Set interval (in seconds) from 0 to 1.
                /// This attribute sets the time interval between groups of measurements when
                /// smua.measure.count is set to a value greater than 1. The SMU will do its best
                /// to start the measurement of each group when scheduled.
                /// The reset function sets the measure interval to 0.
                /// </summary>
                public double interval
                {
                    get
                    {
                        string cmd = string.Format("{0}.interval", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        if (value < 0) value = 0;
                        else if (value > 1) value = 1;
                        string cmd = string.Format("{0}.interval = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }

                /// <summary>
                /// Get/Set lowest I measure range for auto range.
                /// </summary>
                public double lowrangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.lowrangei", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.lowrangei = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set lowest V measure range for auto range.
                /// </summary>
                public double lowrangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.lowrangev", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.lowrangev = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                public class Filter : Action
                {
                    readonly string smuX;
                    internal Filter(string Name, ICommnd Chassis)
                        : base(Name, Chassis)
                    {
                        string[] tmp = prefixName.Split('.');
                        smuX = tmp[tmp.Length - 3];
                    }
                    /// <summary>
                    /// Get/Set filter count (1 to 100).
                    /// </summary>
                    public int count
                    {
                        get
                        {
                            string cmd = string.Format("{0}.count", prefixName);
                            cmd = print(cmd);
                            return (int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            if (value < 1)
                                value = 1;
                            else if (value > 100)
                                value = 100;
                            string cmd = string.Format("{0}.count = {1}", prefixName, value);
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Get/Set Enable or Disable filter.
                    /// </summary>
                    public smuX.Filter enable
                    {
                        get
                        {
                            string cmd = string.Format("{0}.enable", prefixName);
                            cmd = print(cmd);
                            return (smuX.Filter)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.enable = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Get/Set filter type.
                    /// </summary>
                    public smuX.FilterType type
                    {
                        get
                        {
                            string cmd = string.Format("{0}.type", prefixName);
                            cmd = print(cmd);
                            return (smuX.FilterType)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.type = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                }
                /// <summary>
                /// The rel (relative) feature can be used to null offsets or 
                /// subtract a baseline reading from present and future readings.
                /// With REL enabled, subsequent readings will be the difference between the
                /// actual input value and the rel value as follows:
                /// Displayed Reading = Actual Input - Rel Value
                /// </summary>
                public class Rel : Action
                {
                    readonly string smuX;
                    internal Rel(string Name, ICommnd Chassis)
                        : base(Name, Chassis)
                    {
                        string[] tmp = prefixName.Split('.');
                        smuX = tmp[tmp.Length - 3];
                    }

                    /// <summary>
                    /// Enabling and disabling rel i
                    /// </summary>
                    public smuX.RelEnableY enablei
                    {
                        get
                        {
                            string cmd = string.Format("{0}.enablei", prefixName);
                            cmd = print(cmd);
                            return (smuX.RelEnableY)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.enablei = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Enabling and disabling rel v
                    /// </summary>
                    public smuX.RelEnableY enablev
                    {
                        get
                        {
                            string cmd = string.Format("{0}.enablev", prefixName);
                            cmd = print(cmd);
                            return (smuX.RelEnableY)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.enablev = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Enabling and disabling rel i
                    /// </summary>
                    public smuX.RelEnableY enablep
                    {
                        get
                        {
                            string cmd = string.Format("{0}.enablep", prefixName);
                            cmd = print(cmd);
                            return (smuX.RelEnableY)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.enablep = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Enabling and disabling rel v
                    /// </summary>
                    public smuX.RelEnableY enabler
                    {
                        get
                        {
                            string cmd = string.Format("{0}.enabler", prefixName);
                            cmd = print(cmd);
                            return (smuX.RelEnableY)(int)Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.enabler = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                            Write_Unchecked(cmd);
                        }
                    }

                    /// <summary>
                    /// Get/Set current rel value.
                    /// </summary>
                    public double leveli
                    {
                        get
                        {
                            string cmd = string.Format("{0}.leveli", prefixName);
                            cmd = print(cmd);
                            return Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.leveli = {1}", prefixName, value);
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Get/Set voltage rel value.
                    /// </summary>
                    public double levelv
                    {
                        get
                        {
                            string cmd = string.Format("{0}.levelv", prefixName);
                            cmd = print(cmd);
                            return Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.levelv = {1}", prefixName, value);
                            Write_Unchecked(cmd);
                        }
                    }

                    /// <summary>
                    /// Get/Set power rel value.
                    /// </summary>
                    public double levelp
                    {
                        get
                        {
                            string cmd = string.Format("{0}.levelp", prefixName);
                            cmd = print(cmd);
                            return Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.levelp = {1}", prefixName, value);
                            Write_Unchecked(cmd);
                        }
                    }
                    /// <summary>
                    /// Get/Set resistance rel value.
                    /// </summary>
                    public double levelr
                    {
                        get
                        {
                            string cmd = string.Format("{0}.levelr", prefixName);
                            cmd = print(cmd);
                            return Convert.ToSingle(cmd);
                        }
                        set
                        {
                            string cmd = string.Format("{0}.levelr = {1}", prefixName, value);
                            Write_Unchecked(cmd);
                        }
                    }
                }
                /// <summary>
                /// Get/Set speed,Set speed (nplc = 0.001 to 25)
                /// </summary>
                public double nplc
                {
                    get
                    {
                        string cmd = string.Format("{0}.nplc", prefixName);
                        cmd = print(cmd);
                        return Convert.ToSingle(cmd);
                    }
                    set
                    {
                        if (value > 25)
                            value = 25;
                        else if (value < 0.001)
                            value = 0.001;
                        string cmd = string.Format("{0}.nplc = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set current measure range.
                /// </summary>
                public double rangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.rangei", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        //Model 2601/2602
                        double[] availableCurrRanges ={ 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 0.1, 1, 3 };
                        int length = availableCurrRanges.Length;
                        if (value <= availableCurrRanges[0])
                            value = availableCurrRanges[0];
                        else if (value >= availableCurrRanges[length - 1])
                            value = availableCurrRanges[length - 1];
                        else
                            for (int i = 0; i < length - 1; i++)
                                if (value > availableCurrRanges[i] && value <= availableCurrRanges[i + 1])
                                {
                                    value = availableCurrRanges[i + 1];
                                    break;
                                }
                        string cmd = string.Format("{0}.rangei = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage measure range.
                /// </summary>
                public double rangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.rangev", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        //Model 2601/2602
                        double[] availableVoltsRanges ={ 0.1, 1, 6, 40 };
                        int length = availableVoltsRanges.Length;
                        if (value <= availableVoltsRanges[0])
                            value = availableVoltsRanges[0];
                        else if (value >= availableVoltsRanges[length - 1])
                            value = availableVoltsRanges[length - 1];
                        else
                            for (int i = 0; i < length - 1; i++)
                                if (value > availableVoltsRanges[i] && value <= availableVoltsRanges[i + 1])
                                {
                                    value = availableVoltsRanges[i + 1];
                                    break;
                                }
                        string cmd = string.Format("{0}.rangev = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Request a current reading.
                /// </summary>
                /// <returns></returns>
                public double i()
                {
                    string cmd = string.Format("{0}.i()", prefixName);
                    cmd = print(cmd);
                    return Convert.ToDouble(cmd);
                }
                /// <summary>
                /// Request a voltage reading.
                /// </summary>
                /// <returns></returns>
                public double v()
                {
                    string cmd = string.Format("{0}.v()", prefixName);
                    cmd = print(cmd);
                    return Convert.ToDouble(cmd);
                }
                /// <summary>
                /// Request a current and voltage reading.
                /// </summary>
                /// <returns></returns>
                public IvStruct iv()
                {
                    string cmd = string.Format("{0}.iv()", prefixName);
                    cmd = print(cmd);
                    string[] reading = cmd.Split('\t');
                    IvStruct riv = new IvStruct();
                    riv.i = Convert.ToDouble(reading[0]);
                    riv.v = Convert.ToDouble(reading[1]);
                    return riv;
                }
                /// <summary>
                /// Request a resistance reading.
                /// </summary>
                /// <returns></returns>
                public double r()
                {
                    string cmd = string.Format("{0}.r()", prefixName);
                    cmd = print(cmd);
                    return Convert.ToDouble(cmd);
                }
                /// <summary>
                /// Request a power reading.
                /// </summary>
                /// <returns></returns>
                public double p()
                {
                    string cmd = string.Format("{0}.p()", prefixName);
                    cmd = print(cmd);
                    return Convert.ToDouble(cmd);
                }

                /// <summary>
                /// Store current readings in buffer.
                /// </summary>
                /// <param name="rbuffer">rbuffer, ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2.</param>
                public void overlappedi(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.overlappedi({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Store voltage readings in buffer.
                /// </summary>
                /// <param name="rbuffer">rbuffer, ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2</param>
                public void overlappedv(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.overlappedv({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Store power readings in buffer.
                /// </summary>
                /// <param name="rbuffer">rbuffer, ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2.</param>
                public void overlappedp(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.overlappedp({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Store resistance readings in buffer.
                /// </summary>
                /// <param name="rbuffer">rbuffer, ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2</param>
                public void overlappedr(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.overlappedr({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
                /// <summary>
                /// Store current and voltage readings in respective
                /// buffers (current and then voltage are stored in separate buffers).
                /// </summary>
                /// <param name="ibuffer">ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2.</param>
                /// <param name="vbuffer">ibuffer, and vbuffer = smuX.nvbuffer1 or smuX.nvbuffer2.</param>
                public void overlappediv(nvbufferX ibuffer, nvbufferX vbuffer)
                {
                    if (ibuffer.BufferFullName == vbuffer.BufferFullName)
                        throw new ArgumentException("current and voltage should be stored in separate buffers.");
                    string cmd = string.Format("{0}.overlappediv({1},{2})", prefixName, ibuffer.BufferFullName, vbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }


                public void i(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.i({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
                public void v(nvbufferX rbuffer)
                {
                    string cmd = string.Format("{0}.v({1})", prefixName, rbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }

                public void iv(nvbufferX ibuffer, nvbufferX vbuffer)
                {
                    if (ibuffer.BufferFullName == vbuffer.BufferFullName)
                        throw new ArgumentException("current and voltage should be stored in separate buffers.");
                    string cmd = string.Format("{0}.iv({1},{2})", prefixName, ibuffer.BufferFullName, vbuffer.BufferFullName);
                    Write_Unchecked(cmd);
                }
            }
            public class Source : Action
            {
                readonly string smuX;

                internal Source(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                    string[] tmp = prefixName.Split('.');
                    smuX = tmp[tmp.Length - 2];

                }

                /// <summary>
                /// Get/Set lowest I source range for auto range.
                /// </summary>
                public double lowrangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.lowrangei", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.lowrangei = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set lowest V source range for auto range.
                /// </summary>
                public double lowrangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.lowrangev", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.lowrangev = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }

                /// <summary>
                /// Get/Set current source auto range.
                /// </summary>
                public smuX.autorangeY autorangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.autorangei", prefixName);
                        cmd = print(cmd);
                        return (smuX.autorangeY)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.autorangei = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage source auto range.
                /// </summary>
                public smuX.autorangeY autorangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.autorangev", prefixName);
                        cmd = print(cmd);
                        return (smuX.autorangeY)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.autorangev = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set current source range.
                /// </summary>
                public double rangei
                {
                    get
                    {
                        string cmd = string.Format("{0}.rangei", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        //Model 2601/2602
                        double[] availableCurrRanges ={ 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 0.1, 1, 3 };
                        int length = availableCurrRanges.Length;
                        if (value <= availableCurrRanges[0])
                            value = availableCurrRanges[0];
                        else if (value >= availableCurrRanges[length - 1])
                            value = availableCurrRanges[length - 1];
                        else
                            for (int i = 0; i < length - 1; i++)
                                if (value > availableCurrRanges[i] && value <= availableCurrRanges[i + 1])
                                {
                                    value = availableCurrRanges[i + 1];
                                    break;
                                }
                        string cmd = string.Format("{0}.rangei = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage source range.
                /// </summary>
                public double rangev
                {
                    get
                    {
                        string cmd = string.Format("{0}.rangev", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        //Model 2601/2602
                        double[] availableVoltsRanges ={ 0.1, 1, 6, 40 };
                        int length = availableVoltsRanges.Length;
                        if (value <= availableVoltsRanges[0])
                            value = availableVoltsRanges[0];
                        else if (value >= availableVoltsRanges[length - 1])
                            value = availableVoltsRanges[length - 1];
                        else
                            for (int i = 0; i < length - 1; i++)
                                if (value > availableVoltsRanges[i] && value <= availableVoltsRanges[i + 1])
                                {
                                    value = availableVoltsRanges[i + 1];
                                    break;
                                }
                        string cmd = string.Format("{0}.rangev = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set current source value.
                /// </summary>
                public double leveli
                {
                    get
                    {
                        string cmd = string.Format("{0}.leveli", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.leveli = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage source value.
                /// </summary>
                public double levelv
                {
                    get
                    {
                        string cmd = string.Format("{0}.levelv", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.levelv = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set Source delay. The default is 0, no delays.
                ///  0 or smuX.DELAY_OFF    No delay.
                /// -1 or smuX.DELAY_AUTO   Auto delay.
                ///  User_value             Set user delay value.
                /// </summary>
                /// <remarks>
                /// smuX.source.delay = Y -- Writes source delay.
                /// This attribute allows for additional source settling time after an output step.
                /// Setting this attribute to DELAY_AUTO will cause a range dependent delay to be
                /// inserted when ever the source is changed.
                /// Y can be set to a specific user defined value that will set the delay that is used
                /// regardless of range.
                /// </remarks>
                public double delay
                {
                    get
                    {
                        string cmd = string.Format("{0}.delay", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.delay = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set current compliance limit.
                /// </summary>
                public double limiti
                {
                    get
                    {
                        string cmd = string.Format("{0}.limiti", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.limiti = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Get/Set voltage compliance limit.
                /// </summary>
                public double limitv
                {
                    get
                    {
                        string cmd = string.Format("{0}.limitv", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.limitv = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Test if in compliance (true = in compliance;false = not in compliance).
                /// readonly
                /// </summary>
                public bool compliance
                {
                    get
                    {
                        string cmd = string.Format("{0}.compliance", prefixName);
                        cmd = print(cmd);
                        return Convert.ToBoolean(cmd);
                    }
                }
                /// <summary>
                /// Select source function.
                /// </summary>
                public smuX.Func func
                {
                    get
                    {
                        string cmd = string.Format("{0}.func", prefixName);
                        cmd = print(cmd);
                        return (smuX.Func)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.func = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Turns on/off the SMU output.
                /// </summary>
                public smuX.Output output
                {
                    get
                    {
                        string cmd = string.Format("{0}.output", prefixName);
                        cmd = print(cmd);
                        return (smuX.Output)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.output = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write(cmd, 4000);
                        //Write_Unchecked(cmd);
                    }
                }

                /// <summary>
                /// Sets current limit in normal output-off state for firmware revision 1.2.0, or later.
                /// </summary>
                public double offlimiti
                {
                    get
                    {
                        string cmd = string.Format("{0}.offlimiti", prefixName);
                        cmd = print(cmd);
                        return Convert.ToDouble(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.offlimiti = {1}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// Selecting the Output-off state
                /// </summary>
                public smuX.OffMode offmode
                {
                    get
                    {
                        string cmd = string.Format("{0}.offmode", prefixName);
                        cmd = print(cmd);
                        return (smuX.OffMode)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.offmode = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
                /// <summary>
                /// When set to smuX.OE_NONE, the Model 2601/2602 SourceMeter will take no action when the
                /// Output Enable line goes low. When set to smuX.OE_OUTPUT_OFF, the SourceMeter will turn its
                /// output off as if the smuX.source.output = smuX.OUTPUT_OFF command had been received.
                /// The SourceMeter will not automatically turn its output on when the Output Enable line returns to
                /// the high state.
                /// </summary>
                public smuX.OutputEnableAction outputenableaction
                {
                    get
                    {
                        string cmd = string.Format("{0}.outputenableaction", prefixName);
                        cmd = print(cmd);
                        return (smuX.OutputEnableAction)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.outputenableaction = {1}.{2}", prefixName, smuX, value.ToString().ToUpper());
                        Write_Unchecked(cmd);
                    }
                }
            }

        }
        public class Timer : Action
        {
            public readonly Measure measure;
            internal Timer(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
                measure = new Measure(prefixName + ".measure", Chassis);
            }
            /// <summary>
            /// Resets the timer to 0 seconds.
            /// </summary>
            public void reset()
            {
                string cmd = string.Format("{0}.reset()", prefixName);
                Write_Unchecked(cmd);
            }
            public class Measure : Action
            {
                internal Measure(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                }
                /// <summary>
                /// Measures the elapsed time since the timer was last reset.
                /// </summary>
                /// <returns>the elapsed time in seconds.</returns>
                public double t()
                {
                    string cmd = string.Format("{0}.t()", prefixName);
                    cmd = print(cmd);
                    return Convert.ToDouble(cmd);
                }

            }
        }
        public class Trigger : Action
        {
            internal Trigger(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
            }

            /// <summary>
            /// Clear pending triggers.
            /// </summary>
            public void clear()
            {
                string cmd = string.Format("{0}.clear()", prefixName);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Wait for a trigger event.
            /// </summary>
            /// <param name="timeout">Maximum amount of time in seconds to wait for the trigger.</param>
            /// <returns>Returns true if a trigger was detected. Returns false if no 
            /// triggers were detected during the timeout period.
            /// </returns>
            public bool wait(int timeout_s)
            {
                if (timeout_s < 0) timeout_s = 0;
                string cmd = string.Format("{0}.wait({1})", prefixName, timeout_s);
                Write_Unchecked(cmd);
                Stopwatch watch = new Stopwatch();
                watch.Start();
                do
                {
                    try
                    {
                        cmd = Chassis.Read_Unchecked();
                        break;
                    }
                    catch (Exception ex)
                    {
                        //throw;
                    }
                    if (watch.ElapsedMilliseconds > timeout_s * 1000 + 500)
                    {
                        watch.Stop();
                        throw new Exception("operation time out.");
                    }
                    Thread.Sleep(100);
                } while (true);
                if (watch.IsRunning) watch.Stop();
                watch.Reset();

                return Convert.ToBoolean(cmd);
            }
        }
        public class Gpib : Action
        {
            internal Gpib(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
            }

            /// <summary>
            /// Get/Set the GPIB address.
            /// Set from 0 to 30.
            /// </summary>
            /// <remarks>
            /// A new GPIB address takes effect when the command is processed. If there are
            /// response messages in the output queue when this command is processed they
            /// must be read at the new address.
            /// The user should allow ample time for the command to be processed before
            /// attempting to communicate with the instrument again. After sending this command,
            /// make sure to use the new address to communicate with the instrument.
            /// The GPIB address is stored in non-volatile memory. The reset function has no
            /// effect on the address.
            /// </remarks>
            public int address
            {
                get
                {
                    string cmd = string.Format("{0}.address", prefixName);
                    cmd = print(cmd);
                    return (int)Convert.ToSingle(cmd);
                }
                set
                {
                    if (value < 0 || value > 30)
                        throw new ArgumentException("the gpib address should be 0 to 30");
                    string cmd = string.Format("{0}.address = {1}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }
        }
        public class TSPLink : Action
        {
            internal TSPLink(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
            }
            /// <summary>
            /// Initializes the TSP-Link system.
            /// </summary>
            public void reset()
            {
                string cmd = string.Format("{0}.reset()", prefixName);
                Write_Unchecked(cmd);
            }
            /// <summary>
            /// Get tsplink state.
            /// Returns online if the most recent TSP-Link reset was successful. 
            /// Returns offline if the reset failed.
            /// </summary>
            public string state
            {
                get
                {
                    string cmd = string.Format("{0}.state", prefixName);
                    cmd = print(cmd);
                    return cmd;
                }
            }
            /// <summary>
            /// Get/Set the node number for an instrument. N = 1 to 64
            /// </summary>
            public int node
            {
                get
                {
                    string cmd = string.Format("{0}.node", prefixName);
                    cmd = print(cmd);
                    return (int)Convert.ToSingle(cmd);
                }
                set
                {
                    if (value < 1 || value > 64)
                        throw new ArgumentException("the node number should be 1 to 64");
                    string cmd = string.Format("{0}.node = {1}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }

        }
        public class Status : Action
        {
            public readonly Standard standard;

            internal Status(string Name, ICommnd Chassis)
                : base(Name, Chassis)
            {
                standard = new Standard(prefixName + ".standard", Chassis);
            }

            /// <summary>
            /// Reset bits of status registers to 0.
            /// </summary>
            public void reset()
            {
                string cmd = string.Format("{0}.reset()", prefixName);
                Write_Unchecked(cmd);
            }

            /// <summary>
            /// The bits of the status byte register.
            /// </summary>
            [Flags]
            public enum StatusByteBit : byte
            {
                /// <summary>
                /// Bit B0, Measurement Summary Bit (MSB)
                /// </summary>
                MSB = 1,
                /// <summary>
                /// Bit B1, System Summary Bit (SSB)
                /// </summary>
                SSB = 2,
                /// <summary>
                /// Bit B2, Error Available (EAV) 
                /// </summary>
                EAV = 4,
                /// <summary>
                /// Bit B3, Questionable Summary Bit (QSB)
                /// </summary>
                QSB = 8,
                /// <summary>
                /// Bit B4, Message Available (MAV)
                /// 每 Set summary bit indicates that a response message is present in the Output Queue.
                /// </summary>
                MAV = 16,
                /// <summary>
                /// Bit B5, Event Summary Bit (ESB) 
                /// 每 Set summary bit indicates that an enabled standard event has occurred.
                /// </summary>
                ESB = 32,
                /// <summary>
                /// Bit B6, Request Service (RQS)/Master Summary Status (MSS)
                /// </summary>
                RQS_MSS = 64,
                /// <summary>
                /// Bit B7, Operation Summary (OSB)
                /// </summary>
                OSB = 128
            }

            /// <summary>
            /// Get Status Byte Register.(*STB?)
            /// The bits of the status byte register are described as follows:
            /// Bit B0, Measurement Summary Bit (MSB) 每 Set summary bit indicates that an
            /// enabled measurement event has occurred.
            /// Bit B1, System Summary Bit (SSB) 每 Set summary bit indicates that an enabled
            /// system event has occurred.
            /// Bit B2, Error Available (EAV) 每 Set summary bit indicates that an error or status
            /// message is present in the Error Queue.
            /// Bit B3, Questionable Summary Bit (QSB) 每 Set summary bit indicates that an
            /// enabled questionable event has occurred.
            /// Bit B4, Message Available (MAV) 每 Set summary bit indicates that a response
            /// message is present in the Output Queue.
            /// Bit B5, Event Summary Bit (ESB) 每 Set summary bit indicates that an enabled
            /// standard event has occurred.
            /// Bit B6, Request Service (RQS)/Master Summary Status (MSS) 每 Set bit
            /// indicates that an enabled summary bit of the Status Byte Register is set.
            /// Depending on how it is used, Bit B6 of the Status Byte Register is either the
            /// Request for Service (RQS) bit or the Master Summary Status (MSS) bit:
            /// When using the GPIB serial poll sequence of the SourceMeter to obtain the status
            /// byte (serial poll byte), B6 is the RQS bit.
            /// When using status.condition or the *STB? common command to read the
            /// status byte, B6 is the MSS bit.
            /// Bit B7, Operation Summary (OSB) 每 Set summary bit indicates that an enabled
            ///operation event has occurred.
            /// </summary>
            public StatusByteBit condition
            {
                get
                {
                    string cmd = string.Format("{0}.condition", prefixName);
                    cmd = print(cmd);
                    return (StatusByteBit)(int)Convert.ToSingle(cmd);
                }
            }

            #region To set service request enable register bits

            /// <summary>
            /// System SRQ enable register bit attributes
            /// </summary>
            [Flags]
            public enum RequestByteBit : byte
            {
                /// <summary>
                /// Enable MSB. Bit: B0
                /// </summary>
                MEASUREMENT_SUMMARY_BIT = 1,
                MSB = 1,
                /// <summary>
                /// Enable SSB. Bit: B1
                /// </summary>
                SYSTEM_SUMMARY_BIT = 2,
                SSB = 2,
                /// <summary>
                /// Enable EAV bit. Bit: B2
                /// </summary>
                ERROR_AVAILABLE = 4,
                EAV = 4,
                /// <summary>
                /// Enable QSB. Bit: B3
                /// </summary>
                QUESTIONABLE_SUMMARY_BIT = 8,
                QSB = 8,
                /// <summary>
                /// Enable MAV bit. Bit: B4
                /// </summary>
                MESSAGE_AVAILABLE = 16,
                MAV = 16,
                /// <summary>
                /// Enable ESB. Bit: B5
                /// </summary>
                EVENT_SUMMARY_BIT = 32,
                ESB = 32,
                /// <summary>
                /// Enable X . Bit: B6
                /// </summary>
                NotUsed = 64,
                /// <summary>
                /// Enable SSB. Bit: B7
                /// </summary>
                OPERATION_SUMMARY_BIT = 128,
                OSB = 128,
            }

            /// <summary>
            /// Get/Set the Service Request Enable Register.(0 to 255)
            /// </summary>
            public RequestByteBit request_enable
            {
                get
                {
                    string cmd = string.Format("{0}.request_enable", prefixName);
                    cmd = print(cmd);
                    return (RequestByteBit)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string cmd = string.Format("{0}.request_enable = {1:d}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// Get the service request event register
            /// </summary>
            public RequestByteBit request_event
            {
                get
                {
                    string cmd = string.Format("{0}.request_event", prefixName);
                    cmd = print(cmd);
                    return (RequestByteBit)(int)Convert.ToSingle(cmd);
                    //return Convert.ToByte(cmd);
                }
            }
            #endregion

            #region To set system node enable register bits

            [Flags]
            public enum NodeByteBit : byte
            {
                /// <summary>
                /// Enable MSB. Bit: B0
                /// </summary>
                MEASUREMENT_SUMMARY_BIT = 1, MSB = 1,
                /// <summary>
                /// Bit: B1
                /// </summary>
                NotUsed = 2,
                /// <summary>
                /// Enable EAV bit. Bit: B2
                /// </summary>
                ERROR_AVAILABLE = 4, EAV = 4,
                /// <summary>
                /// Enable QSB. Bit: B3
                /// </summary>
                QUESTIONABLE_SUMMARY_BIT = 8, QSB = 8,
                /// <summary>
                /// Enable MAV bit. Bit: B4
                /// </summary>
                MESSAGE_AVAILABLE = 16, MAV = 16,
                /// <summary>
                /// Enable ESB. Bit: B5
                /// </summary>
                EVENT_SUMMARY_BIT = 32, ESB = 32,
                /// <summary>
                /// Enable MSS bit. Bit: B6
                /// </summary>
                MASTER_SUMMARY_STATUS = 64, MSS = 64,
                /// <summary>
                /// Enable OSB. Bit: B7
                /// </summary>
                OPERATION_SUMMARY_BIT = 128, OSB = 128,

            }
            /// <summary>
            /// Get/Set the Node Enable Register.(0 to 255)
            /// </summary>
            public NodeByteBit node_enable
            {
                get
                {
                    string cmd = string.Format("{0}.node_enable", prefixName);
                    cmd = print(cmd);
                    return (NodeByteBit)(int)Convert.ToSingle(cmd);
                }
                set
                {
                    string cmd = string.Format("{0}.node_enable = {1:d}", prefixName, value);
                    Write_Unchecked(cmd);
                }
            }
            /// <summary>
            /// Get the status node event register
            /// </summary>
            public NodeByteBit node_event
            {
                get
                {
                    string cmd = string.Format("{0}.node_event", prefixName);
                    cmd = print(cmd);
                    return (NodeByteBit)(int)Convert.ToSingle(cmd);
                    //return Convert.ToByte(cmd);
                }
            }
            #endregion

            /// <summary>
            /// Standard event status Enable registers and bits
            /// </summary>
            [Flags]
            public enum StdEvtByteBit : byte
            {
                /// <summary>
                /// Bit: B0
                /// </summary>
                OPERATION_COMPLETE = 1, OPC = 1,
                /// <summary>
                /// Bit: B1
                /// </summary>
                NotUsed = 2,
                /// <summary>
                /// Bit: B2
                /// </summary>
                QUERY_ERROR = 4, QYE = 4,
                /// <summary>
                /// Bit: B3
                /// </summary>
                DEVICE_DEPENDENT_ERROR = 8, DDE = 8,
                /// <summary>
                /// Bit: B4
                /// </summary>
                EXECUTION_ERROR = 16, EXE = 16,
                /// <summary>
                /// Bit: B5
                /// </summary>
                COMMAND_ERROR = 32, CME = 32,
                /// <summary>
                /// Bit: B6
                /// </summary>
                USER_REQUEST = 64, URQ = 64,
                /// <summary>
                /// Bit: B7
                /// </summary>
                POWER_ON = 128, PON = 128,
            }

            /// <summary>
            /// Standard event commands
            /// </summary>
            public class Standard : Action
            {
                internal Standard(string Name, ICommnd Chassis)
                    : base(Name, Chassis)
                {
                }

                /// <summary>
                /// Get/Set the Standard Event Status Enable Register.(0 to 255)
                /// </summary>
                public StdEvtByteBit enable
                {
                    get
                    {
                        string cmd = string.Format("{0}.enable", prefixName);
                        cmd = print(cmd);
                        return (StdEvtByteBit)(int)Convert.ToSingle(cmd);
                    }
                    set
                    {
                        string cmd = string.Format("{0}.enable = {1:d}", prefixName, value);
                        Write_Unchecked(cmd);
                    }
                }

                public byte condition
                {
                    get
                    {
                        string cmd = string.Format("{0}.condition", prefixName);
                        cmd = print(cmd);
                        return Convert.ToByte(cmd);
                    }
                }
                /// <summary>
                /// Get Standard Event Status Register. (*ESR?)
                /// </summary>
                public StdEvtByteBit Event
                {
                    get
                    {
                        string cmd = string.Format("{0}.event", prefixName);
                        cmd = print(cmd);
                        return (StdEvtByteBit)(int)Convert.ToSingle(cmd);
                        //return Convert.ToByte(cmd);
                    }
                }


            }

        }

        public enum Channel
        {
            SMUA, SMUB
        }
        public enum TrigDirection
        {
            None,
            Output_Trigger,
            Input_Trigger,
            Output_Meas_Input_Trigger,
            Input_Meas_Output_Trigger
        }

        public void LinearSweepAndTrigger(Channel channel, smuX.Func func, Format.DataFmt OutputDataFormat,
            float start, float stop, float step, float SrcDelay_S,
            TrigDirection TrigDir, int TrigOutLine, float TrigDelay_S,
            int TrigInLine, int InputTrigCount, int WaitTrigTimeout_s,
            out double[] ireadings, out double[] vreadings)
        {
            smuX smux = null;
            switch (channel)
            {
                case Channel.SMUA:
                    smux = smua;
                    break;
                case Channel.SMUB:
                    smux = smub;
                    break;
            }
            smuX.nvbufferX ibuffer = smux.nvbuffer1, vbuffer = smux.nvbuffer2;
            try
            {
                //clear stb and error msg
                status.reset();
                errorqueue.clear();
                // set data output format
                format.data = OutputDataFormat;
                //-- Disable all autoranging to measure both I and V
                smux.source.autorangei = smuX.autorangeY.AUTORANGE_OFF;
                smux.measure.autorangev = smuX.autorangeY.AUTORANGE_OFF;
                smux.source.autorangev = smuX.autorangeY.AUTORANGE_OFF;
                smux.measure.autorangei = smuX.autorangeY.AUTORANGE_OFF;
                //-- Set the appropriate source functions based on sourcing 'amps' or 'volts'.
                switch (func)
                {
                    case smuX.Func.OUTPUT_DCAMPS:
                        smux.source.func = smuX.Func.OUTPUT_DCAMPS;
                        smux.source.rangei = Math.Max(Math.Abs(start), Math.Abs(stop));
                        //-- Set the output to the initial value
                        smux.source.leveli = start;
                        break;
                    case smuX.Func.OUTPUT_DCVOLTS:
                        smux.source.func = smuX.Func.OUTPUT_DCVOLTS;
                        smux.source.rangev = Math.Max(Math.Abs(start), Math.Abs(stop));
                        //-- Set the output to the initial value
                        smux.source.levelv = start;
                        break;
                }
                delay(0.1f);
                //-- Turn output ON depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_ON;
                //-- Execute sweep. Measure both I and V. Index the new level by the step value in order to
                //-- provide the 'channel.measure.iv' command with the next value.
                ibuffer.clear();
                ibuffer.appendmode = 1;
                ibuffer.collectsourcevalues = 0;
                ibuffer.collecttimestamps = 0;
                vbuffer.clear();
                vbuffer.appendmode = 1;
                vbuffer.collectsourcevalues = 0;
                vbuffer.collecttimestamps = 0;
                waitcomplete(4000);

                switch (TrigDir)
                {
                    case TrigDirection.None:
                        for (float newlevel = start; newlevel <= stop; newlevel += step)
                        {
                            switch (func)
                            {
                                case smuX.Func.OUTPUT_DCAMPS:
                                    smux.source.leveli = newlevel;
                                    break;
                                case smuX.Func.OUTPUT_DCVOLTS:
                                    smux.source.levelv = newlevel;
                                    break;
                            }
                            //--wait before making measurement
                            delay(SrcDelay_S);
                            smux.measure.iv(ibuffer, vbuffer);
                            waitcomplete(1000);
                        }
                        break;
                    case TrigDirection.Output_Trigger:
                        for (float newlevel = start; newlevel <= stop; newlevel += step)
                        {
                            switch (func)
                            {
                                case smuX.Func.OUTPUT_DCAMPS:
                                    smux.source.leveli = newlevel;
                                    break;
                                case smuX.Func.OUTPUT_DCVOLTS:
                                    smux.source.levelv = newlevel;
                                    break;
                            }
                            //--wait before making measurement
                            delay(SrcDelay_S);
                            digio.trigger[TrigOutLine].assert(); //--Assert trigger on trigoutline.
                            delay(TrigDelay_S);
                            smux.measure.iv(ibuffer, vbuffer);
                            waitcomplete(1000);
                        }
                        break;
                    case TrigDirection.Input_Trigger:
                        for (int i = 0; i < InputTrigCount; i++)
                        {
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                            smux.measure.iv(ibuffer, vbuffer);
                        }
                        break;
                    case TrigDirection.Output_Meas_Input_Trigger:
                        for (float newlevel = start; newlevel <= stop; newlevel += step)
                        {
                            switch (func)
                            {
                                case smuX.Func.OUTPUT_DCAMPS:
                                    smux.source.leveli = newlevel;
                                    break;
                                case smuX.Func.OUTPUT_DCVOLTS:
                                    smux.source.levelv = newlevel;
                                    break;
                            }
                            //--Wait before making measurement
                            delay(SrcDelay_S);
                            //--Assert trigger on trigoutline.
                            digio.trigger[TrigOutLine].assert();
                            delay(TrigDelay_S);
                            //Measure
                            smux.measure.iv(ibuffer, vbuffer);
                            waitcomplete(1000);
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                        }
                        break;
                    case TrigDirection.Input_Meas_Output_Trigger:
                        for (int i = 0; i < InputTrigCount; i++)
                        {
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                            //Measure
                            smux.measure.iv(ibuffer, vbuffer);
                            waitcomplete(1000);
                            //--Assert trigger on trigoutline.
                            digio.trigger[TrigOutLine].assert();
                            delay(TrigDelay_S);
                        }
                        break;

                }
                waitcomplete(5000);
                //-- Turn output OFF depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_OFF;
                int points = ibuffer.n;
                Node.smuX.BufferData bd = ibuffer.PrintBuffer(1, points, false, false);
                ireadings = bd.MeasuredReadings;
                points = vbuffer.n;
                bd = vbuffer.PrintBuffer(1, points, false, false);
                vreadings = bd.MeasuredReadings;
                ibuffer.clear();
                vbuffer.clear();
                ibuffer.appendmode = 0;
                vbuffer.appendmode = 0;
            }
            catch (Exception ex)
            {
                //-- Turn output OFF depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_OFF;
                ibuffer.clear();
                vbuffer.clear();
                ibuffer.appendmode = 0;
                vbuffer.appendmode = 0;
                throw;
            }
        }

        public smuX.IvStruct[] LinearSweepAndTrigger(Channel channel, smuX.Func func,
            float start, float stop, float step, float SrcDelay_S,
            TrigDirection TrigDir, int TrigOutLine, float TrigDelay_S,
            int TrigInLine, int InputTrigCount, int WaitTrigTimeout_s)
        {
            smuX smux = null;
            switch (channel)
            {
                case Channel.SMUA:
                    smux = smua;
                    break;
                case Channel.SMUB:
                    smux = smub;
                    break;
            }
            try
            {
                //clear stb and error msg
                status.reset();
                errorqueue.clear();
                //-- Disable all autoranging to measure both I and V
                smux.source.autorangei = smuX.autorangeY.AUTORANGE_OFF;
                smux.measure.autorangev = smuX.autorangeY.AUTORANGE_OFF;
                smux.source.autorangev = smuX.autorangeY.AUTORANGE_OFF;
                smux.measure.autorangei = smuX.autorangeY.AUTORANGE_OFF;
                //-- Set the appropriate source functions based on sourcing 'amps' or 'volts'.
                switch (func)
                {
                    case smuX.Func.OUTPUT_DCAMPS:
                        smux.source.func = smuX.Func.OUTPUT_DCAMPS;
                        smux.source.rangei = Math.Max(Math.Abs(start), Math.Abs(stop));
                        //-- Set the output to the initial value
                        smux.source.leveli = start;
                        break;
                    case smuX.Func.OUTPUT_DCVOLTS:
                        smux.source.func = smuX.Func.OUTPUT_DCVOLTS;
                        smux.source.rangev = Math.Max(Math.Abs(start), Math.Abs(stop));
                        //-- Set the output to the initial value
                        smux.source.levelv = start;
                        break;
                }
                delay(0.1f);
                //-- Turn output ON depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_ON;
                waitcomplete(4000);
                //-- Execute sweep. Measure both I and V. Index the new level by the step value in order to
                //-- provide the 'channel.measureivandstep' command with the next value.
                //-- Define and set specific variables
                int sweep_index = 0, points = 0;
                float newlevel = start;
                smuX.IvStruct[] ivreadings = null;

                switch (TrigDir)
                {
                    case TrigDirection.None:
                        points = (int)Math.Ceiling(((stop - start) / Math.Abs(step)) + 1);
                        ivreadings = new smuX.IvStruct[points];
                        while (sweep_index < points)
                        {
                            //--wait before making measurement
                            delay(SrcDelay_S);
                            newlevel += step;
                            ivreadings[sweep_index++] = smux.measureivandstep(newlevel);
                            waitcomplete(1000);
                        }
                        break;
                    case TrigDirection.Output_Trigger:
                        points = (int)Math.Ceiling(((stop - start) / Math.Abs(step)) + 1);
                        ivreadings = new smuX.IvStruct[points];
                        while (sweep_index < points)
                        {
                            //--wait before making measurement
                            delay(SrcDelay_S);
                            digio.trigger[TrigOutLine].assert(); //--Assert trigger on trigoutline.
                            delay(TrigDelay_S);
                            newlevel += step;
                            ivreadings[sweep_index++] = smux.measureivandstep(newlevel);
                            waitcomplete(1000);
                        }
                        break;
                    case TrigDirection.Input_Trigger:
                        points = InputTrigCount;
                        ivreadings = new smuX.IvStruct[points];
                        while (sweep_index < points)
                        {
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                            ivreadings[sweep_index++] = smux.measure.iv();
                        }
                        break;
                    case TrigDirection.Output_Meas_Input_Trigger:
                        points = (int)Math.Ceiling(((stop - start) / Math.Abs(step)) + 1);
                        ivreadings = new smuX.IvStruct[points];
                        while (sweep_index < points)
                        {
                            switch (func)
                            {
                                case smuX.Func.OUTPUT_DCAMPS:
                                    smux.source.leveli = newlevel;
                                    break;
                                case smuX.Func.OUTPUT_DCVOLTS:
                                    smux.source.levelv = newlevel;
                                    break;
                            }
                            //--Wait before making measurement
                            delay(SrcDelay_S);
                            //--Assert trigger on trigoutline.
                            digio.trigger[TrigOutLine].assert();
                            delay(TrigDelay_S);
                            //Measure
                            ivreadings[sweep_index++] = smux.measure.iv();
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                            newlevel += step;
                        }
                        break;
                    case TrigDirection.Input_Meas_Output_Trigger:
                        points = InputTrigCount;
                        ivreadings = new smuX.IvStruct[points];
                        while (sweep_index < points)
                        {
                            //Wait for a trigger event
                            bool flag = digio.trigger[TrigInLine].wait(WaitTrigTimeout_s);
                            if (!flag) throw new Exception("no triggers were detected during the timeout period.");
                            //Measure
                            ivreadings[sweep_index++] = smux.measure.iv();
                            waitcomplete(1000);
                            //--Assert trigger on trigoutline.
                            digio.trigger[TrigOutLine].assert();
                            delay(TrigDelay_S);
                        }
                        break;
                }
                waitcomplete(5000);
                //-- Turn output OFF depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_OFF;
                return ivreadings;
            }
            catch (Exception ex)
            {
                //-- Turn output OFF depending on channel selected
                smux.source.output = smuX.Output.OUTPUT_OFF;
                throw;
            }
        }
    }
}

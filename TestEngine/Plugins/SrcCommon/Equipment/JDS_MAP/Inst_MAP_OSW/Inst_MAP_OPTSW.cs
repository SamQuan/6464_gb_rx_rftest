using System;
using System.Collections.Generic;
using System.Text;

using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// MAP Mainframe Optical Switch Module Driver
    /// </summary>
    public class Inst_MAP_OPTSW : InstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_MAP_OPTSW(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "M A P",				// hardware name 
                "0",  			// minimum valid firmware version 
                "6");			// maximum valid firmware version 
            ValidHardwareData.Add("JDS_MAP_OPTSW", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_MAP",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_MAP", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_MAP)chassisInit;
        }
        #endregion

        #region InstType_SimpleSwitch Overrides

        /// <summary>
        /// Returns the Maximum Settable Output Switch Position
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                string[] slotConfig = (instrumentChassis.Query_Unchecked(":SYST:CONF? " + SystemCommandString(), this).Split(','));
                string[] switchConfig = (slotConfig[Convert.ToInt32(base.SubSlot) - 1]).Split(' ');   // Array is zero based.
                int maxPosition = 0;
                if (switchConfig.Length == 6)
                    maxPosition = Convert.ToInt32(switchConfig[4]);
                else
                    maxPosition = Convert.ToInt32(switchConfig[3]);
                return maxPosition;
            }
        }

        /// <summary>
        /// Returns the Minimum Settable Output Switch Poisition
        /// </summary>
        public override int MinimumSwitchState
        {
            get { return 1; }
        }

        /// <summary>
        /// Sets / Gets the Output Switch Position
        /// </summary>
        public override int SwitchState
        {
            get
            {
                int switchPosition = Convert.ToInt32(this.instrumentChassis.Query_Unchecked(":ROUTE:CLOSE? " + DeviceCommandString() + "," + MinimumSwitchState.ToString(), this));
                return switchPosition;
            }
            set
            {
                if ((value <= this.MaximumSwitchState) && (value >= this.MinimumSwitchState))
                {
                    this.instrumentChassis.Write_Unchecked(":ROUTE:CLOSE " + this.DeviceCommandString() + "," + MinimumSwitchState.ToString() + "," + value.ToString(), this);
                }
                else
                {
                    throw new InstrumentException("Switch position requested is outside the operational range of this instruments");
                }
            }
        }

        #endregion

        #region Instrument Overrides

        /// <summary>
        /// Returns the firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            {
                string idn = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] info = idn.Split(',');
                // opComplete();

                return info[3].TrimStart('R'); 
            }
        }

        /// <summary>
        /// Returns the Hardware Identity
        /// </summary>
        public override string HardwareIdentity
        {
            get 
            {
                string idn = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] info = idn.Split(',');
                // opComplete();

                return info[1].Trim();
            }
        }

        /// <summary>
        /// Sets the Switch into its Default State
        /// </summary>
        public override void SetDefaultState()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                System.Threading.Thread.Sleep(10);
                base.IsOnline = value;
            }
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_MAP instrumentChassis;

        #endregion

        #region Private Member Functions

        private string SystemCommandString()
        {
            const string chassis = "1";
            return (chassis + "," + base.Slot);
        }

        private string DeviceCommandString()
        {
            const string chassis = "1";
            return (chassis + "," + base.Slot + "," + base.SubSlot);
        }

        private void opComplete()
        {
            int comp = 0, count = 0;
            while (comp != 1)
            {
                comp = Convert.ToInt16(instrumentChassis.Query_Unchecked(":SYST:BUSY?", this));
                count++;
            }
        }

        #endregion
    }
}

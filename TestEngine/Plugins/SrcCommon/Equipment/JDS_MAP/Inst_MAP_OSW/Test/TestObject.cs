using System;
using System.Collections.Generic;

using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
//using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;

namespace SC_sw_Test
{
    /// <exclude />	
    //[TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        private Chassis_MAP testChassis;
        private Inst_MAP_OPTSW testSW;
        private Inst_MAP_OPTSW testSW2;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::7::INSTR"; //e.g. "//pai-tx-labj1/GPIB0::9::INSTR";
        const string chassisName = "Chassis";
        const string instName = "Instrument";
        const string instName2 = "Instrument2";
        #endregion

        /// <exclude />
        //[TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            testChassis = new Chassis_MAP(chassisName, "Chassis_MAP", visaResource);
            TestOutput(chassisName, "Created OK");

            testSW = new Inst_MAP_OPTSW(instName, "Inst_MAP","2","1", testChassis);
            TestOutput(instName, "Created OK");
            testSW2 = new Inst_MAP_OPTSW(instName2, "Inst_MAP", "2", "2", testChassis);
            TestOutput(instName2, "Created OK");

            // put them online
            // TODO...
            //TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            TestOutput(chassisName, "IsOnline set true OK");
            testSW.IsOnline = true;
            TestOutput(instName, "IsOnline set true OK");
            testSW2.IsOnline = true;
            TestOutput(instName, "IsOnline set true OK");
        }

        /// <exclude />
        //[TestFixtureTearDown]
        public void ShutDown()
        {
            testSW.IsOnline = false;
            testSW2.IsOnline = false;
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        //[Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            for (int i = testSW.MinimumSwitchState;  i<=testSW.MaximumSwitchState; i++)
            {
                TestOutput("Setting Switch 1 to Position " + i.ToString());
                testSW.SwitchState = i;
                TestOutput("Switch 1 Position Now " + testSW.SwitchState.ToString());
            }

            for (int i = testSW2.MinimumSwitchState; i <= testSW2.MaximumSwitchState; i++)
            {
                TestOutput("Setting Switch 2 Position " + i.ToString());
                testSW2.SwitchState = i;
                TestOutput("Switch 2 Position Now " + testSW2.SwitchState.ToString());
            }
            //Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            //Assert.IsNaN(Double.NaN);
        
        
        }

        //[Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            //Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

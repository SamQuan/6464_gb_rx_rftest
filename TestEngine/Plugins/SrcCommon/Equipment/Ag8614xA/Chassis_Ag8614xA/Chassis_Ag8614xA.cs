// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Ag8614xA.cs
//
// Author: Keith Pillar
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Agilent 86140x OSA Chassis Driver
    /// </summary>
    public class Chassis_Ag8614xA : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">VISA resource string for communicating with the chassis</param>
        public Chassis_Ag8614xA(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            string maxFwVer = "B.06.02 A.0";
            // Setup expected valid hardware variants 
            ChassisDataRecord ag86146B = new ChassisDataRecord(
               "AGILENT 86146B",			// hardware name 
               "0",			// minimum valid firmware version 
               maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("Ag86146B", ag86146B);

            // Setup expected valid hardware variants 
            ChassisDataRecord ag86140A = new ChassisDataRecord(
               "HEWLETT-PACKARD 86140A",			// hardware name 
               "0",			// minimum valid firmware version 
               maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("Ag86140A", ag86140A);

            // Setup expected valid hardware variants 
            ChassisDataRecord ag86142A = new ChassisDataRecord(
               "HEWLETT-PACKARD 86142A",			// hardware name 
               "0",			// minimum valid firmware version 
               maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("Ag86142A", ag86142A);

            //for Ag68142B
            ChassisDataRecord ag86142b = new ChassisDataRecord(
               "AGILENT 86142B",			// hardware name 
               "0",			// minimum valid firmware version 
               maxFwVer);		// maximum valid firmware version 
            this.ValidHardwareData.Add("Ag86142B", ag86142b);
        }
        #endregion

        #region Chassis virtual abstracts
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    this.Write("*CLS", null);
                }
            }
        }
        /// <summary>
        /// returns the instrument error information
        /// </summary>
        /// <returns></returns>
        public override string GetErrorString()
        {
            string resp = Query_Unchecked("STAT:QUES:EVEN?", null);
            string baseErrStr = base.GetErrorString();

            baseErrStr = String.Format("Questionable Data Register? {0}\n{1}", resp, baseErrStr);            

            string errStr = this.Query_Unchecked("SYST:ERR?", null);
            errStr = errStr + " : " + baseErrStr;
            return errStr;

        }


        /// <summary>
        /// Reads an array of comma separated doubles from the instrument
        /// </summary>
        /// <param name="command">The command to send to the instrument</param>
        /// <param name="maxNumofValues">The maximum number of values expected from the instrument.</param>
        /// <returns>An array of values</returns>
        public double[] QueryDoubleArray(string command, int maxNumofValues)
        {
            //Increase the default buffer size  (default is 16KBytes)if necessary. The Instrument returns ASCII format 
            //doubles using 17 bytes (including the , separator). Use 20 * as a safety margin, and add an extra 100 bytes for safety
            int requiredBufferSize = 20 * maxNumofValues + 100;

            if (requiredBufferSize > (1024 * 16))
            {
                this.VisaSession.DefaultBufferSize = requiredBufferSize;
            }

            //string resp = this.Query(command, null);
            string resp = this.Query_Unchecked(command, null);
            //Put the default buffer size back to the default 16 KBytes
            this.VisaSession.DefaultBufferSize = 16 * 1024;
            string[] asciiValues = resp.Split(',');
            double[] doubleValues = new double[asciiValues.Length];

            for (int x = 0; x < asciiValues.Length; x++)
            {
                doubleValues[x] = Convert.ToDouble(asciiValues[x]);
            }

            return doubleValues;
        }

        #endregion
        
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag8614xA.cs
//
// Author: Keith Pillar
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 86140xA OSA Instrument Driver
    /// </summary>
    public class Inst_Ag8614xA : InstType_OSA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag8614xA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            string maxFwVer = "B.06.02 A.0";
            // Setup expected valid hardware variants 
            InstrumentDataRecord ag86146bData = new InstrumentDataRecord(
                "AGILENT 86146B",			// hardware name 
                "0",			// minimum valid firmware version 
                maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("AG86146B", ag86146bData);

            // Setup expected valid hardware variants 
            InstrumentDataRecord ag86140aData = new InstrumentDataRecord(
                "HEWLETT-PACKARD 86140A",			// hardware name 
                "0",			// minimum valid firmware version 
                maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("AG86140A", ag86140aData);

            // Setup expected valid hardware variants 
            InstrumentDataRecord ag86142aData = new InstrumentDataRecord(
                "HEWLETT-PACKARD 86142A",			// hardware name 
                "0",			// minimum valid firmware version 
                maxFwVer);		// maximum valid firmware version 
            ValidHardwareData.Add("ag86142A", ag86142aData);

            //added by ken.wu, Aug.7.2007, support for [Ag68142G]
            InstrumentDataRecord ag86142bData = new InstrumentDataRecord(
                "AGILENT 86142B",
                "0",
                maxFwVer);
            this.ValidHardwareData.Add("ag86142", ag86142bData);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag8614xA",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag8614xA)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string

                string idn = instrumentChassis.Query("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                string fwv = idn.Split(',')[3].Trim();

                // Log event 
                LogEvent("'FirmwareVersion' returned : " + fwv);

                return fwv;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields

                string[] idn = instrumentChassis.Query("*IDN?", null).Split(',');


                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        /// <remarks>
        /// Resets the instrument, sets up sweep range 1525-1565nm and starts single sweep.
        /// </remarks>
        public override void SetDefaultState()
        {
            // Send reset command to instrument
            //this.Reset(); mqn taken out as it takes too long and it may reset auto align and mess with the spectral rbw
            // set scan area to C-Band
            this.WavelengthStart_nm = 1525;
            this.WavelengthStop_nm = 1565;
            this.Amplitude_dBperDiv = 10;
            this.TracePoints = 2001;
            this.VideoAverage = 0;
            this.VideoBandwidth = 100;
            this.ResolutionBandwidth = 1;
            this.SweepMode = SweepModes.Single;
            this.MarkerOn = true;
            // start single sweep
            this.SweepMode = SweepModes.Single;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        /// <remarks>If set to true, sets the instrument into the default state.</remarks>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region OSA InstrumentType property overrides

        /// <summary>
        /// Reads/sets the display amplitude for the OSA channel in dB/Div. 
        /// </summary>
        /// <value>
        /// Set value has to be between 0.01 and 20 db/Div.
        /// </value>
        public override double Amplitude_dBperDiv
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("DISP:WIND:TRAC:Y:SCAL:PDIV?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                instrumentChassis.Write("DISP:WIND:TRAC:Y:SCAL:PDIV " + value + " DB", this);
            }
        }

        /// <summary>
        /// This property is only partially available on this instrument (set only).
        /// </summary>
        public override bool AutoRange
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:POW:DC:RANGE:AUTO?", this);
                
                // Convert response to double and return
                switch (response)
                {
                    case "0":
                        return (false);
                    case "1":
                        return (true);
                    default:
                        throw new InstrumentException("Autorange query gave unexpected response= " + response);                                        
                }
            }
            set
            {
                string myValue="";
                switch (value)
                {
                    case true:
                        myValue = "1";
                        break;
                    case false:
                        myValue = "0";
                        break;
                }
                instrumentChassis.Write("SENS:POW:DC:RANGE:AUTO " + myValue, this);
            }
        }

        /// <summary>
        /// Returns the display trace, as an array of numeric values. 
        /// </summary>
        public override InstType_OSA.OptPowerPoint[] GetDisplayTrace
        {
            get
            {
                // read amplitude values from OSA 

                int numPoints = this.TracePoints;

                double[] response = instrumentChassis.QueryDoubleArray("TRAC:DATA:Y? TRA", numPoints);

                InstType_OSA.OptPowerPoint[] trace = new OptPowerPoint[numPoints];
                double waveLength = this.WavelengthStart_nm;
                double waveLengthStep = this.WavelengthSpan_nm / (numPoints - 1);

                for (int index = 0; index < numPoints; index++)
                {                    
                    trace[index].power_dB = response[index];
                    trace[index].wavelength_nm = Math.Round(waveLength, 2);
                    waveLength += waveLengthStep;
                }

                return trace;
            }
        }

        /// <summary>
        /// Reads the present marker amplitude for the OSA channel.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the instrument does not return the level in dBm.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerAmplitude_dBm
        {
            get
            {
                // Query the instrument
                
                string response = instrumentChassis.Query("CALC:MARK:Y?;", this);

                markerOn = true;  // remember that marker is now on

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
        }

        /// <summary>
        /// Shows/hides the marker for the OSA channel. 
        /// </summary>
        /// <remarks>
        /// Since the instrument does not have way to read whether a marker is active or not,
        /// this is handled using a private variable that is set to true every time the marker
        /// activated and false when all markes are erased.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        public override bool MarkerOn
        {
            get
            {
                // return soft buffer
                return markerOn;
            }
            set
            {
                if (value)
                {
                    // turns marker 1 (by default)ON
                    instrumentChassis.Write("CALC:MARK ON", this);
                    markerOn = true;  // remember that marker is now on
                }
                else
                {
                    // turns all markers OFF
                    instrumentChassis.Write("CALC:MARK:AOFF", this);
                    markerOn = false;  // remember that marker is now off
                }
            }
        }

        /// <summary>
        /// Reads/sets the marker wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between the current start and stop wavelengths.
        /// </value>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override double MarkerWavelength_nm
        {
            get
            {
                // Query the instrument   

                string response = instrumentChassis.Query("CALC:MARK:X?", this);

                markerOn = true;  // remember that marker is now on
                
                // Convert response to double and return
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Check whether value is in correct range
                if (value < this.WavelengthStart_nm || value > this.WavelengthStop_nm)
                {
                    throw new InstrumentException("Marker Wavelength must be between " + this.WavelengthStart_nm +
                                         " and " + this.WavelengthStop_nm + " nm. Set value = " + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("CALC:MARK:X:WAV " + (value/1e9), this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the point averaging for the OSA channel. 
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int PointAverage
        {
            get
            {
                // Query the instrument 
                string response = instrumentChassis.Query("CALC:AVER:STAT?", this);

                // check if OFF was returned
                 if ((response == "0")||(response == "OFF"))
                 {
                     return (0);
                 }
                 else
                 {
                     response = instrumentChassis.Query("CALC:AVER:COUN?", this);
                 }
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                // Check whether value is in correct range
               
                if (value == 0)
                {
                    instrumentChassis.Write("CALC:AVER:STAT OFF", this);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("CALC:AVER:COUN " + value, this);
                    instrumentChassis.Write("CALC:AVER:STAT ON", this);
                }
               
            }
        }

        /// <summary>
        /// Reads/sets the reference level for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between -90 and +30 dBm.
        /// </value>
        public override double ReferenceLevel_dBm
        {
            get
            {
                // Query the instrument

                string response = instrumentChassis.Query("DISP:WIND:TRAC:Y:SCAL:RLEV?", this);

                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Check whether value is in correct range
                if (value < -90 || value > 30)
                {
                    throw new InstrumentException("Reference level must be between -90 and +30 dBm. Set value = "
                                               + value);
                }
                else
                {
                    // Write the value to the instrument
                    instrumentChassis.Write("DISP:WIND:TRAC:Y:SCAL:RLEV " + value + " DBM;" , this);
                }
            }
        }

        /// <summary>
        /// Reads/sets the resolution bandwidth for the OSA channel in NM.  
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 0.06, 0.07, 1, 2, and 5 nm.
        /// </value>
        public override double ResolutionBandwidth
        {
            get
            {

                // Query the instrument   
                string response = instrumentChassis.Query("SENS:BWID:RES?", this);
                // Convert response to double and return
                return (Convert.ToDouble(response)*1e9);

            }
            set
            {                
                // Write the value to the instrument
                instrumentChassis.Write("SENS:BWID:RES " + value + "NM", this);
            }
        }

        /// <summary>
        /// Returns the executional status for the OSA channel. 
        /// </summary>
        /// <value>
        /// Busy is returned when the OSA is in the process of measuring a spectrum.
        /// Otherwise Status, ie Dataready is returned.  If in continuous sweep mode we will 
        /// get busy status until the first sweep has completed, at which point Data is ready
        /// to be collected so status will change
        /// </value>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.ChannelState Status
        {
            get
            {
                if (mStatus != ChannelState.Busy)
                {
                    return(mStatus);
                }
                TimeSpan duration = DateTime.Now - mStatusStart;
                if (duration.TotalMilliseconds > mSatusPollDelayms) // impliments a none blocking delay
                {
                    try
                    {
                        // check if standard event register has bit set
                       
                        byte esrReg = instrumentChassis.StandardEventRegister;
                        Int32 esrValue = Convert.ToInt32(esrReg);
                        if ( esrValue == 0 )
                        {
                            mStatus = ChannelState.DataReady; 
                        }
                        
                    }
                    catch (ChassisException ce)
                    {
                        // IGNORE 101 no peak found
                        if (!ce.Message.Contains("Failed VISA Query")) // ignore reset lock up
                        {
                            if (!ce.Message.Contains("101 :")) throw new ChassisException("Status generated unexpected OSA error", ce);
                        }
                    }

                    // prepare next poll pause
                    mStatusStart = DateTime.Now;
                }
                // check for OSA Lock up
                duration = DateTime.Now - mDTStartWaitTime;
                if (duration.TotalSeconds > mStatusTimeOutSeconds)
                {
                    throw new InstrumentException("The OSA appears to have locked up, suggest power cycle");
                }
                return mStatus;
            }
        }

        /// <summary>
        /// Reads/sets the sweep capture mode for the OSA channel.
        /// </summary>
        /// <value>
        /// Triggered mode is not available.
        /// Unspecified mode cannot be set.
        /// </value>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepStart"/>
        /// <seealso cref="SweepStop"/>
        public override InstType_OSA.SweepModes SweepMode
        {
            get
            {
                return mode;
            }
            set
            {
                // Select command
                string command = "";
                switch (value)
                {
                    case SweepModes.Single:
                        {
                            command = "INIT:CONT OFF"; 
                            instrumentChassis.Write(command, this);
                            break;
                        }
                    case SweepModes.Continuous:
                        {
                            command = "INIT:CONT ON";  // Start continuous sweep
                            break;
                        }
                    case SweepModes.Unspecified:
                        {
                            throw new InstrumentException("Cannot set to 'Unspecified' mode.");
                            
                        }
                    case SweepModes.Triggered:
                        {
                            throw new InstrumentException("'Triggered' mode not available.");
                        }
                }

                // Write the command to the instrument
                instrumentChassis.Write(command, this);
                this.mode = value;
            }
        }

        /// <summary>
        /// Reads/sets the number of trace points for the OSA channel.
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 51, 101, 201, 501, 1001, 2001, and 5001.
        /// </value>
        public override int TracePoints
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:SWE:POIN?", this);
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {                
                // Write the value to the instrument
                instrumentChassis.Write("SENS:SWE:POIN " + value, this);   
            }
        }

        /// <summary>
        /// Reads/sets the video averaging value for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value has to be between 2 and 1000 or 0.
        /// </value>
        public override int VideoAverage
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("CALC:AVER:COUN?", this);
                // check if OFF was returned
                if (response == "OFF") response = "0";
                // Convert response to double and return
                return Convert.ToInt32(response);
            }
            set
            {
                {
                    // convert 0 to OFF
                    string outValue = Convert.ToString(value);
                    if (value == 0)
                    {
                        outValue = "OFF";
                        // Write the value to the instrument
                        instrumentChassis.Write("CALC:AVER:STAT OFF", this);
                    }
                    else
                    {
                        // Write the value to the instrument
                        instrumentChassis.Write("CALC:AVER:STAT ON", this);
                        // Write the value to the instrument
                        instrumentChassis.Write("CALC:AVER:COUN " + outValue, this);
                    }
                }
            }
        }

        /// <summary>
        /// Reads/sets the video bandwidth for the OSA channel. (HZ) 
        /// </summary>
        /// <value>
        /// If an exact match is not possible the closest value ABOVE that supplied will be used.
        /// Possible set values are: 10, 100, 1k, 10k, 100k, and 1MHz.  Pass HZ value in
        /// </value>
        public override double VideoBandwidth
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:BAND:VID?", this);
                // extract value
                double vbw = 0;
                if (response.IndexOf("GHZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e9;
                if (response.IndexOf("MHZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e6;
                else if (response.IndexOf("KHZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 3)) * 1e3;
                else if (response.IndexOf("HZ") >= 0)
                    vbw = Convert.ToDouble(response.Substring(0, response.Length - 2));

                // Convert response to double and return
                return vbw;
            }
            set
            {
               instrumentChassis.Write("SENS:BAND:VID " + value + "HZ", this);    
            }
        }

        /// <summary>
        /// Reads/sets the centre wavelength for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm.
        /// </value>
        public override double WavelengthCentre_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:WAV:CENT?", this);
                // Convert response to double and return
                return (Convert.ToDouble(response)*1e9);
            }
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write("SENS:WAV:CENT " + value + "NM", this);        
            }
        }

        /// <summary>
        /// Reads/sets the wavelength offset for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm
        /// </value>
        public override double WavelengthOffset_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:WAV:OFFS?", this);
                // Convert response to double and return
                return (Convert.ToDouble(response)*1e9);
            }
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write("SENS:WAV:OFFS " + value + " NM", this);
            }
        }


        /// <summary>
        /// Reads / sets the sensitivity level in dBm
        /// </summary>
        public double Sensitivity_dBm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query(":SENS:POW:DC:RANGE:LOW?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write(":SENS:POW:DC:RANGE:LOW " + value + "DBM", this);
            }
        }


        /// <summary>
        /// Reads/sets the wavelength span for the OSA channel.
        /// </summary>
        /// <value>
        /// The set value in nm
        /// </value>
        public override double WavelengthSpan_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:WAV:SPAN?", this);
                // Convert response to double and return
                return (Convert.ToDouble(response)*1e9);
            }
           
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write("SENS:WAV:SPAN " + value + "NM", this);        
            }
            
        }

        /// <summary>
        /// Reads/sets the start wavelength for the OSA channel (left-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value has to be between 600 and 1750nm.
        /// </value>
        public override double WavelengthStart_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:WAV:STAR?", this);
                // Convert response to double and return in NM (not Metres)
                return (Convert.ToDouble(response)*1e9);
            }
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write("SENS:WAV:STAR " + value + "NM", this);
            }
        }

        /// <summary>
        /// Reads/sets the stop wavelength for the OSA channel (right-hand edge of the display).
        /// </summary>
        /// <value>
        /// The set value in nm.
        /// </value>
        public override double WavelengthStop_nm
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:WAV:STOP?", this);
                // Convert response to double and return in NM (not Metres)
                return (Convert.ToDouble(response) * 1e9);
            }
            set
            {
                // Write the value to the instrument
                instrumentChassis.Write("SENS:WAV:STOP " + value + "NM", this);
            }
        }

        #endregion

        #region Ag8614B properties
        public bool DisplayFeature
        {
            get
            {
                string ver = this.instrumentChassis.FirmwareVersion;

                string[] vers = ver.Split(new char[] { '.', ' ' });

                // minimum require of firm version: B.04.00
                if (vers[0].Trim()[0] < 'B')
                {
                    return false;
                }

                if (int.Parse(vers[1]) < 4)
                {
                    return false;
                }

                return true;
            }
        }

        public bool Display
        {
            get
            {
                string rtn = this.instrumentChassis.Query(":DISP?", this);
                return rtn == "1";
            }
            set
            {
                string cmd = string.Format(":DISP {0}", value ? "ON" : "OFF");
                this.instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        #endregion

        #region OSA InstrumentType function overrides

        /// <summary>
        /// Moves the marker to the minimum signal point on the display. 
        /// </summary>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToMinimum()
        {
            // Write the value to the instrument
            instrumentChassis.Write("CALC:MARK:MIN", this);
            markerOn = true; 
        }

        /// <summary>
        /// Moves the marker to the next peak to the left/right on the display.
        /// </summary>
        /// <param name="directionToMove">
        /// Enumeration representing the direction in which to search for the next peak.
        /// If unspecified is selected, the next peak is searched.
        /// </param>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToNextPeak(InstType_OSA.Direction directionToMove)
        {
            string direction = "";

            switch (directionToMove)
            {
                case Direction.Left: direction = "LEFT";
                    break;
                case Direction.Right: direction = "RIGH";
                    break;
                case Direction.Unspecified: direction = "NEXT";
                    break;
            }

            instrumentChassis.Write("CALC:MARK:MAX:" + direction, this);
            markerOn = true;  // remember that marker is now on

        }

        /// <summary>
        /// Moves the marker to the maximum signal point on the display.
        /// </summary>
        /// <remarks>
        /// Throws an exception if the peak search is unsuccessful.
        /// </remarks>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerToMinimum"/>
        /// <seealso cref="MarkerToNextPeak"/>
        /// <seealso cref="MarkerWavelength_nm"/>
        /// <seealso cref="MarkerAmplitude_dBm"/>
        /// <seealso cref="MarkerOn"/>
        public override void MarkerToPeak()
        {
            // Write the value to the instrument
            instrumentChassis.Write("CALC:MARK:MAX", this);
            markerOn = true;  // remember that marker is now on
        }

        /// <summary>
        /// Starts a measurement sweep, and returns immediately.
        /// </summary>
        /// <remarks>
        /// Not using the *opc, as we would then need to set up timeouts
        /// based on num of points in sweeps etc, the onus here is on the user
        /// to start sweep and wait however long they need to before trying to read back results
        /// Status() property should be used to determine sweep progress/completion. 
        /// </remarks>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStop"/>
        public override void SweepStart()
        {
            //will initiate a new measurement sweep
            // prepare the test status
            this.PrepareForWait(2, 0x02, 200, 120);
            instrumentChassis.Write_Unchecked("INIT:IMM;", this);
        }

        /// <summary>
        /// Stops an active measurement sweep. 
        /// </summary>
        /// <seealso cref="Status"/>
        /// <seealso cref="SweepMode"/>
        /// <seealso cref="SweepStart"/>
        public override void SweepStop()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }


        /// <summary>
        /// Gets / sets the internal attenuator of the osa on or off
        /// </summary>
        public override bool Attenuator
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }
        #endregion

        #region Instrument specific commands

        /// <summary>
        /// Performs a reset of the instrument and waits for it to finish. 
        /// </summary>
        public void Reset()
        {
            // Start the reset
            this.PrepareForWait(2, 0x10, 1000, 100);
            instrumentChassis.Write_Unchecked("*RST", this);
            while (this.Status == ChannelState.Busy) ;

            markerOn = false; // remember that marker is now off
        }

        /// <summary>
        /// Clear the last state of the ESR register, to be done before any sweep or marker find
        /// </summary>
        /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
        private void ClearESR(byte esrRegister)
        {
            if (esrRegister < 1 || esrRegister > 3)
                throw new InstrumentException("Number of ESR Register has to be between 1 and 3. ESR# = " + esrRegister);
            else
            {

                string response = "0";
                try
                {
                    // ESR is 1 register. Not sure what is going on here. Temporary patch. 
                    //response = instrumentChassis.Query_Unchecked("*ESR" + esrRegister + "?", this);
                    response = instrumentChassis.Query_Unchecked("*ESR?", this);
                }
                catch (Bookham.TestEngine.PluginInterfaces.Chassis.ChassisException)
                {
                    response = "0";
                }
            }
            mStatus = ChannelState.Unspecified; // do not know the state yet

        }
        /// <summary>
        /// check for ESR bits to be set
        /// </summary>
        /// <param name="esrRegister">Number of the ESR register. (1-3)</param>
        /// <param name="esrBitMask">Mask of the bits to check.</param>
        private bool ESRBitMaskSet(byte esrRegister, byte esrBitMask)
        {
            if (esrRegister < 1 || esrRegister > 3)
                throw new InstrumentException("Number of ESR Register has to be between 1 and 3. ESR# = " + esrRegister);
            else
            {
                // check if bit is set
                string response;
                
                response = instrumentChassis.Query_Unchecked("*ESR?", this);
                return ((Convert.ToInt32(response) & (esrBitMask)) == (esrBitMask));
            }
        }
        /// <summary>
        /// check for ESR bits in the mask are set in ESR
        /// </summary>
        /// <param name="esrBitMask">Number of the bit to be checked.</param>
        private bool ESRBitMaskSet(byte esrBitMask)
        {
            // check if bit is set
            string response;
            response = instrumentChassis.Query_Unchecked("*ESR?", this);
            this.mCurrentESR = Convert.ToByte(response);
            return ((this.mCurrentESR & esrBitMask) == esrBitMask);
        }
        /// <summary>
        /// clear any exisiting errors
        /// should be called before any command that has a CheckOSAErrors call after it, apart from Status
        /// </summary>
        private void ClearOSAErrors()
        {
            string response = instrumentChassis.Query_Unchecked("*ESR?", this); // clear errors
        }
        /// <summary>
        /// Send a command and check that it has been set
        /// </summary>
        /// <param name="strCommand">The command header.</param>
        /// <param name="dblValue">Command value.</param>
        /// <param name="dblTimeOutSeconds">Time out for the retry loop.</param>
        /// <param name="intDecPlaces">TNumber of decimal points for the dblValue to return compare.</param>
        private void ChassisWriteWithCheck(string strCommand, double dblValue, double dblTimeOutSeconds, int intDecPlaces)
        {
            string response;
            DateTime StartTime;
            TimeSpan duration;
            StartTime = DateTime.Now;
            Int32 loops = 0; // used to introduce a pause on retry
            dblValue = Math.Round(dblValue, intDecPlaces);  // just make sure the caller does as expected
            do
            {
                response = instrumentChassis.Query(strCommand + "?", this);  // what is there
                if (response.Contains(",") == true) response = response.Split(',')[0]; // peel off first element if marker
                if (Math.Round(Convert.ToDouble(response), intDecPlaces) == dblValue) break;          // does it compare

                instrumentChassis.Write(strCommand + " " + dblValue, this);     // no so send it
                duration = DateTime.Now - StartTime;

                if (loops++ > 0) System.Threading.Thread.Sleep(50);
            } while (duration.TotalSeconds < dblTimeOutSeconds);
        }
        
        private void PrepareForWait(byte bytReg, byte bytStatusBitMask, int PollIntervalms, int TimeOutSeconds)
        {
            // store the register and bit mask to be used in status
            mCurrentStatusRegister = bytReg;
            mCurrentStatusBitMask = bytStatusBitMask;
            mSatusPollDelayms = PollIntervalms;
            mStatusStart = DateTime.Now;

            // clear any existing status bits
            this.ClearESR(bytReg);
            // clear general error register
            this.ClearOSAErrors();
            // mark current status as busy
            mStatus = ChannelState.Busy;
            // prepare start 
            mDTStartWaitTime = DateTime.Now;
            mStatusTimeOutSeconds = TimeOutSeconds;
        }


        /// <summary>
        /// Auto Align Calibration of the OSA, with the largest signal found in full span
        /// </summary>
        public override void AutoAlign()
        {
            // Start the reset
            instrumentChassis.Write_Unchecked("CAL:ALIG", this);
            System.Threading.Thread.Sleep(500);
            
        }

        /// <summary>
        /// Returns the actual resulution bandwidth as displayed on the screen
        /// </summary>
        public double ResolutionBandwidth_Actual
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query("SENS:BAND:BWID:RES?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);

            }
        }

        /// <summary>
        /// Sets or returns the peak excursion setting for peak detection
        /// </summary>
        public double PeakExcursion_dB
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query(":CALC:MARK1:PEXC:PEAK?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }

            set
            {
                instrumentChassis.Write(":CALC:MARK1:PEXC:PEAK " + value.ToString("0.00"), this);
            }
        }

        /// <summary>
        /// Sets or returns the pit excursion setting for pit detection
        /// </summary>
        public double PitExcursion_dB
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query(":CALC:MARK1:PEXC:PIT?", this);
                // Convert response to double and return
                return Convert.ToDouble(response);
            }

            set
            {
                instrumentChassis.Write(":CALC:MARK1:PEXC:PIT " + value.ToString("0.00"), this);
            }
        }

        /// <summary>
        /// Sets or returns the screen title text
        /// </summary>
        public string ScreenTitle
        {
            get
            {
                // Query the instrument   
                string response = instrumentChassis.Query(":DISP:TEXT:DATA?", this);
                // Return
                return response;
            }

            set
            {
                string title = "'" + value.Trim() + "'";
                instrumentChassis.Write(":DISP:TEXT:DATA " + title, this);
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag8614xA instrumentChassis;
        /// <summary>
        /// Soft marker on/off buffer
        /// </summary>
        private Boolean markerOn;
        private InstType_OSA.SweepModes mode;
        /// <summary>
        /// The status of sweeps or marker sets
        /// </summary>
        private ChannelState mStatus;
        /// <summary>
        /// The current working register for testing status
        /// </summary>
        private byte mCurrentStatusRegister;
        /// <summary>
        /// The current working register test bit mask for testing status, normally specifys only one bit
        /// </summary>
        private byte mCurrentStatusBitMask;
        /// <summary>
        ///Current ESR register value
        /// </summary>
        private byte mCurrentESR;
        /// <summary>
        ///Time out for any status check after a wcommand which involves waiting such as sweep
        /// </summary>
        private Int32 mSatusPollDelayms;
        /// <summary>
        ///Start Time marker for sweep or marker search used for OSA lock up detection
        /// </summary>
        private DateTime mDTStartWaitTime;
        /// <summary>
        ///This is used to delay the next status query period, need to stop polling too often at the same time allowing this thread to do other things
        /// </summary>
        private DateTime mStatusStart;
        /// <summary>
        ///Time out for status poll, if the status is polled for a longer period than this then error thrown
        /// </summary>
        private int mStatusTimeOutSeconds;
        #endregion
    }
}

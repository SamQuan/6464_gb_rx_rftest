// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_TunicsPlus_TLS.cs
//
// Author: chris.lewis, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Instrument Driver for the NetTest(Photonetics) Tunable Laser Source
    /// </summary>
    public class Inst_TunicsPlus_TLS : InstType_TunableLaserSource
    {
        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_TunicsPlus instrumentChassis;
        private double setPower;
        private InstType_TunableLaserSource.EnPowerUnits setUnit;
        private bool coherenceControl;
        private bool autoPowerMode;
        private bool beamEnabled;
        #endregion

        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_TunicsPlus_TLS(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord NTtunicsPlusData = new InstrumentDataRecord(
                "NetTest Tunics_Plus",				// hardware name 
                "4.06",  			// minimum valid firmware version 
                "4.06");			// maximum valid firmware version 
            ValidHardwareData.Add("NTTunics_Plus", NTtunicsPlusData);
            InstrumentDataRecord PTtunicsPlusData = new InstrumentDataRecord(
                "PHOTONETICS TUNICS",			// hardware name 
                "4.03",			// minimum valid firmware version 
                "4.03");		// maximum valid firmware version 
            ValidHardwareData.Add("PTTunics_Plus", PTtunicsPlusData);

            InstrumentDataRecord UnknowntunicsPlusData = new InstrumentDataRecord(
                "XXX XXXX",			// hardware name 
                 "",			// minimum valid firmware version 
                 "3.07");		// maximum valid firmware version 
            ValidHardwareData.Add("UnknownTunics_Plus", UnknowntunicsPlusData);

            InstrumentDataRecord UnknowntunicsPlusData1 = new InstrumentDataRecord(
                "XXXX XXXX",			// hardware name 
                 "",			// minimum valid firmware version 
                 "3.07");		// maximum valid firmware version 
            ValidHardwareData.Add("UnknownTunics_Plus1", UnknowntunicsPlusData1);

            InstrumentDataRecord AnritsuTunicsPlusData = new InstrumentDataRecord(
                "Anritsu Tunics", // hardware name 
                "0",			// minimum valid firmware version 
                "6.06");		// maximum valid firmware version 
            ValidHardwareData.Add("ArTTunics_Plus", AnritsuTunicsPlusData);
            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_TunicsPlus",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_TunicsPlus)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = myQuery_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                string fwv = idn.Split(',')[3].Trim();
                if (fwv.Contains("V"))
                    fwv = fwv.Remove(0, 1);
                // Log event 
                LogEvent("'FirmwareVersion' returned : " + fwv);

                return fwv;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string
                string[] idn = myQuery_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                string hi = idn[0].Trim() + " " + idn[1].Trim();

                // Log event 
                LogEvent("'Hardware Identity' returned : " + hi);

                return hi;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.Timeout_ms = 2000;
            this.WavelengthINnm = 1540.0;
            this.PowerUnits = EnPowerUnits.dBm;
            this.Power = 0;
            this.CoheranceControl = true;
            this.AutomaticPowerControl = true;
            this.BeamEnable = false;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();                    
                }
            }
        }
        #endregion

        #region Inherited Property overrides from InstType_TunableLaserSource

        /// <summary>
        /// Enable the output of the TLS true = enabled.
        /// </summary>
        public override bool BeamEnable
        {
            get
            {
                string enabled = myQuery_Unchecked("P?", this);
               
                return !enabled.Contains("DISABLED");
              
            }
            set
            {
                if(value)
                    this.instrumentChassis.Write_Unchecked("ENABLE", this);
                else
                    this.instrumentChassis.Write_Unchecked("DISABLE", this);

                this.WaitForCommandComplete();
                this.beamEnabled = value;
            }
        }

        /// <summary>
        /// Get the maximum allowed output power of the TLS 
        /// </summary>
        public override double MaximumOuputPower
        {
            get
            {
                double maxPower;
                switch (this.PowerUnits)
                {
                    case EnPowerUnits.dBm:
                        maxPower = 8;
                        break;
                    case EnPowerUnits.mW:
                        maxPower = 6.31;
                        break;
                    default:
                        throw new Exception("Invalid Power Unit");
                }
                return maxPower;
            }
        }

        /// <summary>
        /// Gets the Maximum wavelength of the TLS
        /// </summary>
        public override double MaximumWavelengthINnm
        {
            //Max wavelength from spec sheet
            get { return 1625.00; }
        }

        /// <summary>
        /// gets the minimum output power that can be set ny the TLS
        /// </summary>
        public override double MinimumOutputPower
        {
            get 
            {
                double minPower;
                switch (this.PowerUnits)
                {
                    case EnPowerUnits.dBm:
                        minPower = -6.9;
                        break;
                    case EnPowerUnits.mW:
                        minPower = 0.2;
                        break;
                    default:
                        throw new Exception("Invalid Power Unit");
                }
                return minPower;
            }
        }

        /// <summary>
        /// minimun wavelength the the TLS is capable of setting
        /// </summary>
        public override double MinimumWavelengthINnm
        {
            //Minimum wavelength from spec sheet
            get { return 1470.00; }
        }


        /// <summary>
        /// Set the output power of the TLS. This depends on the power unit that the TLS is
        /// currently set to. If TLS is set to mW, the power passed in will be assumed to be in mW
        /// </summary>
        public override double Power
        {
            get
            {
                string response = myQuery_Unchecked("P?", this);
                if (response.Trim() == "DISABLED")
                {
                    //If the TLS is disabled it returns disabled instead of a power. 
                    //In this case, return the cached set power from the set command
                    return setPower;
                }
                else
                {
                    response = response.Substring(response.IndexOf('=') + 1);
                    return Convert.ToDouble(response);
                }
            }
            set
            {
                //Check to see if the power is in range.
                if ((value < this.MinimumOutputPower) || (value > this.MaximumOuputPower))
                {
                    throw new Exception("Power is out of Range of the TLS");
                }
                else
                {
                    //Set the new power to the TLS.
                    SetNewPower(value);
                    //Make sure that the limit hasnt come on. If so back the power off
                    PowerLimitVerify();
                }
            }
        }

        /// <summary>
        /// Set the output power unit of the TLS (mW or dBm)
        /// </summary>
        public override InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                //Cannot get the Power unit from the unit. 
                //Return cached value
                return setUnit;
            }
            set
            {
                switch(value)
                {
                    case EnPowerUnits.dBm:
                        this.instrumentChassis.Write_Unchecked("DBM", this);
                        break;
                    case EnPowerUnits.mW:
                        this.instrumentChassis.Write_Unchecked("MW", this);
                        break;
                    default:
                        throw new Exception("Invalid power unit set to TLS");
                }
                WaitForCommandComplete();
                //Save for later
                setUnit = value;
            }
        }

        /// <summary>
        /// Set the Wavelength of the TLS in nm
        /// </summary>
        public override double WavelengthINnm
        {
            get
            {
                //Get value from TLS
                string response = myQuery_Unchecked("L?", this);
                response = response.Substring(response.IndexOf('=') + 1);
                return Convert.ToDouble(response);
            }
            set
            {
                // don't set if already there
                if (value != this.WavelengthINnm)
                {
                    //Check to see if the property value is in range
                    if ((value < this.MinimumWavelengthINnm) || (value > this.MaximumWavelengthINnm))
                        throw new Exception("Wavelength is out of range for this TLS");
                    else
                        this.instrumentChassis.Write_Unchecked(string.Format("L={0}", value.ToString()), this);

                    //Int32 tt = this.Timeout_ms; // save to restore
                    //this.Timeout_ms = 10000; // this needs longer than other commands
                    this.WaitForCommandComplete();  // needed when unchecked
                    //this.Timeout_ms = tt;
                    //Check to see if the new wavelength setting hasn't caused to power to limit
                    PowerLimitVerify();
                }
            }
        }
        #endregion

        #region Other properties supported by Tunics Plus TLS

        /// <summary>
        /// Set the Conherance control of the TLS
        /// </summary>
        public bool CoheranceControl
        {
            get
            {
                //Cant return CoheranceControl control directly from the
                //instrument, so return the cached value
                return coherenceControl;
            }
            set
            {
                if (value)
                    this.instrumentChassis.Write_Unchecked("CTRLON", this);
                else
                    this.instrumentChassis.Write_Unchecked("CTRLOFF", this);
                WaitForCommandComplete();

                coherenceControl = value;
            }
        }

        /// <summary>
        /// Set the Automatic Power Control of the TLS. 
        /// When ON, the TLS is os constant power mode, when off it is in constant current mode
        /// </summary>
        public bool AutomaticPowerControl
        {
            get
            {
                //Cant return power mode from the unit, so return the cached value
                return autoPowerMode;
            }
            set
            {
                if (value)
                    this.instrumentChassis.Write_Unchecked("APCON", this);
                else
                    this.instrumentChassis.Write_Unchecked("APCOFF", this);
                WaitForCommandComplete();

                autoPowerMode = value;
            }
        }

        /// <summary>
        /// Returns true if the TLS is in Laser current limit
        /// </summary>
        public bool Limit
        {
            get
            {
                string response = myQuery_Unchecked("LIMIT?", this);
                if (response == "YES")
                    return true;
                else
                    return false;
            }
        }
        #endregion

        #region Private functions
        private string myQuery_Unchecked(string cmd, Bookham.TestEngine.PluginInterfaces.Instrument.Instrument ins)
        {
            string ret = "";
            DateTime timeStart = DateTime.Now;
            TimeSpan timeOut = new TimeSpan(0, 0, 0, 0, Timeout_ms);
           
            this.instrumentChassis.Write_Unchecked(cmd, ins);
            do
            {
                // needs a delay
                System.Threading.Thread.Sleep(300); // mainly a problem with photonetics
                // get back the data
                ret = this.instrumentChassis.Read_Unchecked(ins);
            } while ((ret == "") && (DateTime.Now.Subtract(timeStart) < timeOut));
            return ret;
        }
        /// <summary>
        /// Called when the power or wavelength is changed
        /// Checks to see if the new setting has caused the TLS to go into limit
        /// then reduces the power of the TLS.
        /// </summary>
        private void PowerLimitVerify()
        {
            double currentPower;
            while(this.Limit)
            {
                currentPower = this.Power;
                SetNewPower(currentPower - 0.2);
                System.Threading.Thread.Sleep(200);
            }
        }

        /// <summary>
        /// Set the power to the TLS.
        /// </summary>
        /// <param name="newPower"></param>
        private void SetNewPower(double newPower)
        {
            this.instrumentChassis.Write_Unchecked(string.Format("P={0}", newPower.ToString()), this);
            WaitForCommandComplete();
            //Cache value if needed
            setPower = newPower;
        }

        /// <summary>
        /// Sits in a loop waiting for the command complete (*OPC?) status register to 
        /// show that a command has finished.
        /// </summary>
        private void WaitForCommandComplete()
        {
            DateTime timeStart = DateTime.Now;
            TimeSpan timeOut = new TimeSpan(0,0,0,0,Timeout_ms );
            while (!(myQuery_Unchecked("*OPC?", this)).Contains("1") && (DateTime.Now.Subtract(timeStart) < timeOut))
            {
                System.Threading.Thread.Sleep(100);
            }

        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_TunicsPlus.cs
//
// Author: chris.lewis, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// Chassis Driver for Phototetics Tunics Plus Tunable Laser Source(TLS)
    /// </summary>
    public class Chassis_TunicsPlus : ChassisType_Visa488_2 
    {
        #region Constructor
        /// <summary>
        /// Instrument Chassis Constructor for Tunics Plus TLS. "Photonetics TunicsPlus" is the only variant.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_TunicsPlus(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            ChassisDataRecord NTtunicsPlusData = new ChassisDataRecord(
                "NetTest Tunics_Plus", // hardware name 
                "4.06",			// minimum valid firmware version 
                "4.06");		// maximum valid firmware version 
            ValidHardwareData.Add("NTTunics_Plus", NTtunicsPlusData);

           ChassisDataRecord PNtunicsPlusData = new ChassisDataRecord(
                "PHOTONETICS TUNICS",			// hardware name 
                "4.03",			// minimum valid firmware version 
                "4.03");		// maximum valid firmware version 
            ValidHardwareData.Add("PNTunics_Plus", PNtunicsPlusData);

            ChassisDataRecord UnknowntunicsPlusData = new ChassisDataRecord(
                "XXX XXXX",			// hardware name 
                "",			// minimum valid firmware version 
                "3.07");		// maximum valid firmware version 
            ValidHardwareData.Add("UnknownTunics_Plus", UnknowntunicsPlusData);
            ChassisDataRecord UnknowntunicsPlusData1 = new ChassisDataRecord(
                 "XXXX XXXX",			// hardware name 
                 "",			// minimum valid firmware version 
                 "3.07");		// maximum valid firmware version 
            ValidHardwareData.Add("UnknownTunics_Plus1", UnknowntunicsPlusData1);
            ChassisDataRecord AnritsuTunicsPlusData = new ChassisDataRecord(
                "Anritsu Tunics", // hardware name 
                "0",			// minimum valid firmware version 
                "6.06");		// maximum valid firmware version 
            ValidHardwareData.Add("ArTTunics_Plus", AnritsuTunicsPlusData);

        }
        #endregion
        #region Chassis overrides
     
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = myQuery_Unchecked("*IDN?", null);

                //Return the firmware version in the 4th comma seperated field
                string ret = idn.Split(',')[3].Trim();
                if(ret.Contains("V"))
                    ret = ret.Remove(0,1); // remove V
                return ret;
                //return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                base.VisaSession.TerminationCharacterEnabled = true;
                base.VisaSession.TerminationCharacter = 10;
                // Read the chassis ID string
                string[] idn = myQuery_Unchecked("*IDN?", null).Split(',');
                string ret = idn[0].Trim() + " " + idn[1].Trim();
                
                return ret;
                //return "Hardware_Unknown";
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;
                if (value)
                {
                    //// clear the status registers
                    this.Write_Unchecked("*CLS", null);
                }
            }
        }
        #endregion
        #region PRIVATE
        private string myQuery_Unchecked(string cmd, Bookham.TestEngine.PluginInterfaces.Instrument.Instrument ins)
        {
            string ret = "";
            this.Write_Unchecked(cmd, ins);
            do
            {
                // needs a delay
                System.Threading.Thread.Sleep(300); // mainly a problem with photonetics
                // get back the data
                ret = this.Read_Unchecked(ins);
            }while (ret == "");
            return ret;
        }
        #endregion
    }
}

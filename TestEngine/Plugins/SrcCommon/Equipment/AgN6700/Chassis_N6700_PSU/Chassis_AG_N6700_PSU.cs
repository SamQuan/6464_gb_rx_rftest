// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// Chassis driver for Agilent N6700 modular 1u psu
    /// </summary>
    public class Chassis_AG_N6700_PSU : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Agilent N6700 psu driver.
        /// </summary>
        public Chassis_AG_N6700_PSU(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord N6700A_Data = new ChassisDataRecord(
                "N6700A",			// hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("AG_N6700A_PSU", N6700A_Data);

            ChassisDataRecord N6700B_Data = new ChassisDataRecord(
                "N6700B",			// hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("AG_N6700B_PSU", N6700B_Data);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string ret = Query_Unchecked("*IDN?", null);
                string[] info = ret.Split(',');
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string ret = Query_Unchecked("*IDN?", null);
                string[] info = ret.Split(',');
                return info[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        #endregion
    }
}

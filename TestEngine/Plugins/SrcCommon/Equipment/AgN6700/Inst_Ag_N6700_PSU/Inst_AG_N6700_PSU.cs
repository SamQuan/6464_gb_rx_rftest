// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instrument1.cs
//
// Author: michael.day, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Instrument driver for N6700 modular PSU module
    /// </summary>
    public class Inst_AG_N6700_PSU : InstType_ElectricalSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_AG_N6700_PSU(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "N6700A",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("AG_N6700A_PSU", instrVariant1);

            InstrumentDataRecord instrVariant2 = new InstrumentDataRecord(
                "N6700B",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("AG_N6700B_PSU", instrVariant2);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_AG_N6700_PSU",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_AG_N6700_PSU", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_AG_N6700_PSU)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string ret = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] info = ret.Split(',');
                // leaving this as 'firmware unknown as the return is alpha numeric'
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string ret = instrumentChassis.Query_Unchecked("*IDN?", this);
                string[] info = ret.Split(',');
                return info[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //Flush out the error queue
            ErrorBufferClear();
            this.OutputEnabled = false;
            this.VoltageSetPoint_Volt = 0;
            this.CurrentComplianceSetPoint_Amp = 0;
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    this.SetDefaultState();
                }
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_AG_N6700_PSU instrumentChassis;
        private bool opState=false;
        #endregion

        /// <summary>
        /// Reads actual curent
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query("MEAS:SCAL:CURR:DC? (@" + base.Slot +")", this));
            }

        }
        /// <summary>
        /// Read / Set the complience level.
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query("SOUR:CURR:LEV:IMM:AMPL? (@" + base.Slot + ")", this));
            }
            set
            {
                instrumentChassis.Write("SOUR:CURR:LEV:IMM:AMPL " + value.ToString() + ", (@" + base.Slot + ")", this);
                ErrorCheck();
            }
        }

        /// <summary>
        /// Can't use this function, as PSU is Voltage drive only
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                throw new Exception("This PSU is to be used in Voltage Drive only.");
            }
            set
            {
                throw new Exception("This PSU is to be used in Voltage Drive only.");
            }
        }

        /// <summary>
        /// Bool - enable disable TRUE = enabled, FALSE = disabled.
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                return opState;
            }
            set
            {

                string state = "";
                switch (value)
                {
                    case true:
                        state="ON";
                        opState = true;
                        break;
                        
                    case false:
                        state="OFF";
                        opState = false;
                        break;
                        
                }

                instrumentChassis.Write("OUTP:STAT " + state + ", (@" + base.Slot + ")", null);
                ErrorCheck();
            }
        }

        /// <summary>
        /// Reads back the actual voltage
        /// </summary>
        public override double VoltageActual_Volt
        {
            get 
            {
                return Convert.ToDouble(instrumentChassis.Query("MEAS:SCAL:VOLT:DC? (@" + base.Slot + ")", this)); 
            }
        }
        
        /// <summary>
        /// Sets / Gets the voltage compliance level
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query("SOUR:VOLT:PROT:LEV? (@" + base.Slot + ")", this));
            }
            set
            {
                instrumentChassis.Write("SOUR:VOLT:PROT:LEV " + value.ToString() + ", (@" + base.Slot + ")", this);
                ErrorCheck();
            }
        }
        /// <summary>
        /// Set/Get the PSU voltage setpoint
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query("SOUR:VOLT:LEV:IMM:AMPL? (@" + base.Slot + ")", this));
            }
            set
            {
                instrumentChassis.Write("SOUR:VOLT:LEV:IMM:AMPL " + value.ToString() + ", (@" + base.Slot + ")", null);
                ErrorCheck();
            }
        }

        #region Private member Functions

        /// <summary>
        /// read back systen error log to find out what errors occured during send
        /// </summary>
        private void ErrorCheck()
        {
            //Query the error
            string response = this.instrumentChassis.Query(":SYST:ERR?", this);
            //Take the right of the comma (the error string)
            int commaPos = response.IndexOf(',');
            response = response.Substring(commaPos + 1, (response.Length - (commaPos + 1)));
            if (response.IndexOf("No error") < 0)
            {
                //throw error with description
                throw new InstrumentException(response);
            }
        }

        /// <summary>
        /// Clear out the old errors
        /// </summary>
        private void ErrorBufferClear()
        {
            string response = "";
            do
            {
                //Read back the next error in the queue
                response = this.instrumentChassis.Query(":SYST:ERR?", this);
            } while (response.IndexOf("No error") < 0);     //Loop until "No error"
        }

        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_MellesGriotMotionStage.cs
//
// Author: Alexander.Wu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// 
    /// </summary>
    public class Inst_MellesGriotMotionStage : InstType_MotionStage
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_MellesGriotMotionStage(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord MGMotionStageData = new InstrumentDataRecord(
                "NANOSTEP",				// hardware name 
                "0",  			                    // minimum valid firmware version 
                "4.00");			                // maximum valid firmware version 
            ValidHardwareData.Add("Motion Stage", MGMotionStageData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_MellesGriotMotionStage",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_MellesGriotMotionStage", chassisInfo);

            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_MellesGriotMotionStage)base.InstrumentChassis;

            stageNo = int.Parse(slotInit);
        }
        #endregion
         
        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
              //  string idnline = instrumentChassis.Query_Unchecked("*IDN?", this);
//                string[] idn = idnline.Split(',');
//                return idn[3].Trim();
                return "0";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                //string idnline = instrumentChassis.Query_Unchecked("*IDN?", this);
                //string[] idn = idnline.Split(' ');
                //return idn[0].Trim() + "," + idn[1].Trim();
                return "NANOSTEP";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            //
            instrumentChassis.Write_Unchecked("ACK=0", this);
            
            //
            //System.Threading.Thread.Sleep(200);           
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion


        #region Public IntrumentType methods

        private void Wait()
        {
            int i = 0;

            while (IsInMotion)
            { 
                // Add a timeoutCount
                if (i > timeCount)
                    throw new Exception("Driver_MellesGriot waitForCompletion Operation timed out after " + Convert.ToString(readDelay * timeCount / 1000) + "seconds");

                System.Threading.Thread.Sleep(readDelay);

                i++;     
            }
        }

        /// <summary>
        /// Move the stage to the home position
        /// </summary>
        /// <returns></returns>
        public override bool HomeStage()
        {
            instrumentChassis.Write_Unchecked("MCAL" + stageNo.ToString(), this);

            if (waitUntilStopped)
            {
                Wait();
            }

            return true;
        }

        /// <summary>
        /// Move the stage with a relative position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public override bool MoveRelative(double position)
        {
            instrumentChassis.Write_Unchecked("MR" + stageNo.ToString() + "=" + position.ToString(), this);

            if (waitUntilStopped)
            {
                Wait();
            }

            return true;
        }

        #endregion

        #region Pulic InstrumentType Properties

        /// <summary>
        /// Query the position of the stage 
        /// Set the position of the stage
        /// </summary>
        public override double Position
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked("?P"+stageNo.ToString(), this);

                return double.Parse(rtn);
            }
            set
            {
                instrumentChassis.Write_Unchecked("MA" + stageNo.ToString() + "=" + value.ToString(), this);

                if (waitUntilStopped)
                {
                    Wait();
                }
            }
        }

        
        /// <summary>
        /// Query whether the stage is in motion
        /// </summary>
        public override bool IsInMotion
        {
            get 
            {
                double oldPosn = Position;

                System.Threading.Thread.Sleep(500);

                double newPosn = Position;

                return ( (newPosn == oldPosn) ? false : true );
            }
        }

        /// <summary>
        /// Set the acceleration of the specific stageNo
        /// </summary>
        public override double Acceleration
        {
            set 
            {
                instrumentChassis.Write_Unchecked("A" + stageNo.ToString() + "=" + value.ToString(), this);
            }
        }

        /// <summary>
        /// Set the velocity of the stage
        /// </summary>
        public override double Velocity
        {
            set
            {
                instrumentChassis.Write_Unchecked("V" + stageNo.ToString() + "=" + value.ToString(), this);
            }
        }


        /// <summary>
        /// Query adn set the stageNo
        /// </summary>
        public override int StageNo
        {
            get
            {
                return stageNo;
            }
            set
            {
                stageNo = value;
            }
        }

        /// <summary>
        /// Query/Set the flag to wait until stopped
        /// </summary>
        public override bool WaitUntilStopped
        {
            get
            {
                return waitUntilStopped;
            }
            set
            {
                waitUntilStopped = value;
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;

        private int stageNo;

        private bool waitUntilStopped;

        private int timeCount = 60;
        private int readDelay = 500;

        #endregion




 

    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// InstType_MellesGriot.cs
//
// Author: Alexander Wu
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.InstrTypes
{
	/// <summary>
	/// Digital IO instrument type 
	/// </summary>
	public abstract class InstType_MotionStage : Instrument
	{
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
		public InstType_MotionStage(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
		}

		/// <summary>
		/// Sets the acceleration of the stage
		/// </summary>
		public abstract double Acceleration
		{
			set;
		}

        /// <summary>
        /// Sets the velocity of the stage
        /// </summary>
        public abstract double Velocity
        {
            set;
        }

        /// <summary>
        /// Query whether the Motion Stage is in motion
        /// </summary>
        public abstract bool IsInMotion
        {
            get;
        }

        /// <summary>
        /// Sets/Gets the position of the Motion Stage
        /// </summary>
        public abstract double Position
        {
            get;
            set;
        }

        /// <summary>
        /// Move the stage to the home position
        /// </summary>
        /// <returns></returns>
        public abstract bool HomeStage();

        /// <summary>
        /// Move the Stage with a relative position
        /// </summary>
        /// <param name="position">The value of the relative position</param>
        /// <returns></returns>
        public abstract bool MoveRelative(double position);

        /// <summary>
        /// Gets/Sets whether wait for the stage stopped
        /// </summary>
        public abstract bool WaitUntilStopped
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/Sets the Stage No
        /// </summary>
        public abstract int StageNo
        {
            get;
            set;
        }
	} 

}

// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestSolution.Chassis
//
// Chassis_OzDd100mc.cs
//
// Author: 
// As per OzDD100mc/600mc driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using System.Threading;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for OZ DD100mc/600mc Optical Attenuator.
    /// </summary>
    public class Chassis_OzDd100mc : ChassisType_Serial
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="comResourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_OzDd100mc(string chassisNameInit, string driverNameInit,
            string comResourceString)
            : base(chassisNameInit, driverNameInit, comResourceString)
        {
            //Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "DD100MC",			// hardware name 
                "0000000",			// minimum valid firmware version 
                "EVA7_V8.00");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);

            string[] resByComma = comResourceString.Split(',');

            if (resByComma[0].Contains("COM"))
            {
                if ((resByComma.Length < 1) && (resByComma.Length > 2))
                    throw new ChassisException(
                        "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");

                // Setup RS232 variables
                this.BaudRate = Convert.ToInt32((resByComma.Length == 2) ? resByComma[1] : "9600");  // 9600;

                this.DataBits = 8;
                this.StopBits = System.IO.Ports.StopBits.One;
                this.Handshaking = System.IO.Ports.Handshake.None;
                this.Parity = System.IO.Ports.Parity.None;
                this.InputBufferSize_bytes = 1024;
                this.OutputBufferSize_bytes = 1024;
                this.Timeout_ms = 50000;
                this.NewLine = "\r\n";

            }
            else
                throw new ChassisException(
                    "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");
            
        }
        #endregion

        /// <summary>
        /// get firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get 
            {
                //return "Firmware_Unknown";

                string command = "CD";
                this.WriteLine(command, null);
                string respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null) + ",";
                }
                string firmware = respond.Split(',')[3].Trim();
                return firmware;

            }
        }

        /// <summary>
        /// get hardware identity
        /// </summary>
        public override string HardwareIdentity
        {
            get 
            {
                //return "Hardware_Unknown";
                string command = "E0";
                string respond = "";
                this.WriteLine(command, null);
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null);
                }
                
                command = "CD";
                this.WriteLine(command, null);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.ReadLine(null) + ",";
                }
                string hardware = respond.Split(',')[2].Trim();
                return hardware;
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    this.Configure(BaudRate, DataBits, StopBits,
                        Parity, Handshaking, InputBufferSize_bytes, OutputBufferSize_bytes);
                    
                }

                // setup base class
                base.IsOnline = value;
                Thread.Sleep(1000);

                if (value) // if setting online                
                {
                    //off echo,the response to command is transmitted but the input command will not
                    string command = "E0";
                    this.WriteLine(command, null);
                    string respond = "";
                    while (!respond.Contains("Done"))
                    {
                        respond += this.ReadLine(null);
                    }
                }
            }
        }

    }
}

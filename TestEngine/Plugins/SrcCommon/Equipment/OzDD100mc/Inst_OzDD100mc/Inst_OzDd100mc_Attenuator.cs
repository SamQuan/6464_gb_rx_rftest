// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_OzDd100mc_Attenuator.cs
//
// Author: 
// Design: As specified in OzDD100mc driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;
using System.IO;
//using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// OzDD100mc optical attenuator driver
    /// </summary>
    public class Inst_OzDd100mc_Attenuator : InstType_OpticalAttenuator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_OzDd100mc_Attenuator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all OzDd100mc details
            InstrumentDataRecord OzDd100mcInstData = new InstrumentDataRecord(
                "DD100MC",      // hardware name 
                "0000000",  			                // minimum valid firmware version 
                "EVA7_V8.02");			            // maximum valid firmware version 

            ValidHardwareData.Add("OzDd100mc", OzDd100mcInstData);

            // Configure valid chassis information
            // Add Chassis_OzDd100mc chassis details
            InstrumentDataRecord OzDd100mcChassisData = new InstrumentDataRecord(
                "Chassis_OzDd100mc",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "1.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("OzDd100mc", OzDd100mcChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_OzDd100mc) base.InstrumentChassis;

            

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
               return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state(max attenu)
        /// </summary>
        public override void SetDefaultState()
        {
        //    HomePosition();

            EchoOnOff = false;
            
            // set the attenuation to max attenu 
            string respond;
            // clear inputbuff
            do
            {
                respond = this.instrumentChassis.ReadLine(this);
            } while (respond!="");

            //get current attenuator configuration
            string command = "CD";
            this.instrumentChassis.WriteLine(command, this);
            respond = "";
            while (!respond.Contains("Done"))
            {
                respond += this.instrumentChassis.ReadLine(this) + ",";
            }

            //get the max attenu that support
            string[] tempStrArray = respond.Split(',');
            string maximumAttenu = "";
            foreach (string str in tempStrArray)
            {
                if (str.Contains("MAX ATTEN:"))
                {
                    maximumAttenu = str;
                }
            }
            if (!maximumAttenu.Contains("MAX ATTEN:"))
            {
                throw new InstrumentException("Can not Queryed the max attenuation this module support!");
            }
            maximumAttenu = maximumAttenu.Replace("MAX ATTEN:", "");
           
            maxAttenuation_dB = double.Parse(maximumAttenu);
            
            //set to maximum attenuation
            Attenuation_dB = double.Parse(maximumAttenu);
            

            this.OutputEnabled = true;

            string inserLossStr = "";
            //query the inser loss and initialise "inserloss" variable
            foreach (string str in tempStrArray)
            {
                if (str.Contains("IL:"))
                {
                    inserLossStr = str;
                    inserLoss_dB = double.Parse(inserLossStr.Replace("IL:", ""));
                }
            }

            if (!inserLossStr.Contains("IL:"))
            {
                throw new InstrumentException("Can not Queryed the inserloss of this module");
            }
 
        }

        public void SetQuickDefaultState()
        {


            EchoOnOff = false;

            // set the attenuation to max attenu 
            string respond;
            // clear inputbuff
            System.Threading.Thread.Sleep(500);
            do
            {
                respond = this.instrumentChassis.ReadLine(this);
            } while (respond != "");

            //get current attenuator configuration
            string command = "CD";
            this.instrumentChassis.WriteLine(command, this);
            respond = "";
            while (!respond.Contains("Done"))
            {
                respond += this.instrumentChassis.ReadLine(this) + ",";
            }

            //get the max attenu that support
            string[] tempStrArray = respond.Split(',');
            string maximumAttenu = "";
            foreach (string str in tempStrArray)
            {
                if (str.Contains("MAX ATTEN:"))
                {
                    maximumAttenu = str;
                }
            }
            if (!maximumAttenu.Contains("MAX ATTEN:"))
            {
                throw new InstrumentException("Can not Queryed the max attenuation this module support!");
            }
            maximumAttenu = maximumAttenu.Replace("MAX ATTEN:", "");

            maxAttenuation_dB = double.Parse(maximumAttenu);

            //set to maximum attenuation
            Attenuation_dB = double.Parse(maximumAttenu);


            this.OutputEnabled = true;

            string inserLossStr = "";
            //query the inser loss and initialise "inserloss" variable
            foreach (string str in tempStrArray)
            {
                if (str.Contains("IL:"))
                {
                    inserLossStr = str;
                    inserLoss_dB = double.Parse(inserLossStr.Replace("IL:", ""));
                }
            }

            if (!inserLossStr.Contains("IL:"))
            {
                throw new InstrumentException("Can not Queryed the inserloss of this module");
            }
            this.instrumentChassis.WriteLine("H", this);

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region InstType_OpticalAttenuator

        /// <summary>
        /// Set/Return attenuation level (in dB),range from IL to 60dB
        /// need check module whether support this command
        /// </summary>
        public override double Attenuation_dB
        {
            get
            {
                string respond;
                // clear inputbuff
                do
                {
                    respond = this.instrumentChassis.ReadLine(this);
                } while (respond != "");

                //get current attenuation
                string command = "D";
                this.instrumentChassis.WriteLine(command, this);

                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.instrumentChassis.ReadLine(this) + ",";
                }

                string[] tempStrArray = respond.Split(',');
                string atteun = "";
                foreach (string str in tempStrArray)
                {
                    if (str.Contains("ATTEN:"))
                    {
                        atteun = str;
                    }
                }
                if (!atteun.Contains("ATTEN:"))
                {
                    throw new InstrumentException("Can not Queryed the current attenuation!");
                }

                atteun = atteun.Replace("ATTEN:", "");

                //return the current attenu value
                if (currentOutputStatus)
                {
                    currentAttenu_dB = double.Parse(atteun);
                    return currentAttenu_dB;
                }
                else
                {
                    return double.Parse(atteun);
                }
                
                
            }
            set
            {
                string respond;
                // clear inputbuff
                do
                {
                    respond = this.instrumentChassis.ReadLine(this);

                } while (respond != "");

                //Set attenuation
                //the minimum attenuation is inserloss dB,so value<= inserloss dB will equal set to inserloss dB attenu
                // value>=maximum attenuation ,value equal maxAttenuation_dB 
                if (value<=inserLoss_dB)
                {
                    value = inserLoss_dB;
                }

                if (value > maxAttenuation_dB)
                {
                    value = maxAttenuation_dB;
                }
                string command = "A" + value.ToString();
                this.instrumentChassis.WriteLine(command, this);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.instrumentChassis.ReadLine(this) + ",";
                }
                System.Threading.Thread.Sleep(1000);

                currentAttenu_dB = value;
            }
        }

        /// <summary>
        /// Set wavelength (in nm)
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                // Set the wavelength (nm)
                string subCmd = "";
                switch((int)value)
                {
                    case 1330:
                        subCmd = "0";
                        break;
                    case 1550:
                        subCmd = "1";
                        break;
                    default :
                        throw new InstrumentException("Invalid wavelength");
                }

                string respond;
                // clear inputbuff
                do
                {
                    respond = this.instrumentChassis.ReadLine(this);
                } while (respond != "");

                //set wavelength
                string command = "W" + subCmd;
                this.instrumentChassis.WriteLine(command, this);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.instrumentChassis.ReadLine(this) + ",";
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Set/get output enable
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                return currentOutputStatus;
            }
            set
            {
                if (value)
                {
                    Attenuation_dB = currentAttenu_dB;
                    currentOutputStatus = true;
                }
                else
                {
                    string respond;
                    // clear inputbuff
                    do
                    {
                        respond = this.instrumentChassis.ReadLine(this);
                    } while (respond != "");

                    string command = "CD";
                    this.instrumentChassis.WriteLine(command, null);
                    
                    //get the maximum attenuation
                    respond = "";
                    while (!respond.Contains("Done"))
                    {
                        respond += this.instrumentChassis.ReadLine(null) + ",";
                    }

                    string[] tempStrArray = respond.Split(',');
                    string maxAttenu = "";
                    foreach (string str in tempStrArray)
                    {
                        if (str.Contains("MAX ATTEN:"))
                        {
                            maxAttenu = str;
                        }
                    }
                    if (!maxAttenu.Contains("MAX ATTEN:"))
                    {
                        throw new InstrumentException("Can not Queryed the current attenuation!");
                    }

                    maxAttenu = maxAttenu.Replace("MAX ATTEN:", "");

                    //set the attenuation to maxAttenu,as the output disable
                    command = "A" + maxAttenu;
                    this.instrumentChassis.WriteLine(command, this);
                    respond = "";
                    while (!respond.Contains("Done"))
                    {
                        respond += this.instrumentChassis.ReadLine(this) + ",";
                    }
                    System.Threading.Thread.Sleep(1000);
                    currentOutputStatus = false;
                }
            }
        }

        /// <summary>
        /// set/get calibration factor,if get,should set first
        /// </summary>
        public override double CalibrationFactor_dB
        {
            get
            {
                return inserLoss_dB;
            }
            set
            {
                InsertLoss = value;
                inserLoss_dB = value;
            }
        }

        #endregion

        #region OzDD100mc Optical Attenuator Specific functions

        /// <summary>
        /// put the mechanical elements of the attenuator into a fixed, known position
        /// at this position,the attenuation level is generally at a minimum value
        /// </summary>
        public void HomePosition()
        {
            string command = "H";
            this.instrumentChassis.WriteLine(command, this);

            //wait for home finished
            System.Threading.Thread.Sleep(4000);
            string respond = "";
            while (!respond.Contains("Done"))
            {
                respond += this.instrumentChassis.ReadLine(this) + ",";
            }
        }
        /// <summary>
        /// re-initialize the unit.
        /// </summary>
        public void Reset()
        {
            string command = "RST";
            this.instrumentChassis.WriteLine(command, this);
            string respond = "";
            while (!respond.Contains("Done"))
            {
                respond += this.instrumentChassis.ReadLine(this) + ",";
            }
        }

        /// <summary>
        /// set inserLoss
        /// </summary>
        public double InsertLoss
        {
            set
            {
                string respond;
                // clear input buff
                do
                {
                    respond = this.instrumentChassis.ReadLine(this);
                } while (respond != "");

                //set insert loss
                string command = "L" + value.ToString();
                this.instrumentChassis.WriteLine(command, this);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.instrumentChassis.ReadLine(this) + ",";
                }

                inserLoss_dB = value;
            }
        }

        /// <summary>
        /// set echo on/off status,true is ON,false is OFF
        /// </summary>
        private bool EchoOnOff
        {
            set
            {
                string respond;
                // clear input buff
                do
                {
                    respond = this.instrumentChassis.ReadLine(this);
                } while (respond != "");

                //set set echo status
                string command;
                if (value)
                {
                    command = "E1";
                }
                else
                {
                    command = "E0";
                }
                this.instrumentChassis.WriteLine(command, this);
                respond = "";
                while (!respond.Contains("Done"))
                {
                    respond += this.instrumentChassis.ReadLine(this) + ",";
                }
            }
        }

        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_OzDd100mc instrumentChassis;

        private double inserLoss_dB;
        private double maxAttenuation_dB;
        private double currentAttenu_dB = double.PositiveInfinity;
        private bool currentOutputStatus = true;
        #endregion
    }
}

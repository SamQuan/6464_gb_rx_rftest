using System;

namespace EquipmentTest_Inst_Ag662x
{
	public class TestWrapper
	{
		public static int Main(string[] args)
		{
			// Init tester 
			Inst_Ag662x_Test test = new  Inst_Ag662x_Test();

			// Run tests
			test.Setup();

			// Create chassis
            test.T01_CreateInst("GPIB0::17::INSTR");

			test.T02_TryCommsNotOnline();

			// set online
			test.T03_SetOnline();

            //test.T04_DriverVersion();

            //test.T05_FirmwareVersion();
			
            //test.T06_HardwareID();

            //test.T07_SetDefaultState();

            //test.T08_EnableOutput();

            //test.T09_Current_amp();

            //test.T10_Voltage_volt();

            //test.T11_Voltage_compliance();

            //test.T12_CheckSomeErrors();

            //test.T13_TimingComparision(true);
            //test.T13_TimingComparision(false);

            test.T14_MultiChannelReads();

			// Shutdown
			test.ShutDown();

			// End
			return 0;
		}
	}
}

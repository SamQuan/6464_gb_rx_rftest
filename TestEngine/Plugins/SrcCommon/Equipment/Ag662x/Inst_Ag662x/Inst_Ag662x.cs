// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Instruments
//
// Inst_Ag662x.cs
//
// Author: K Pillar
// Design: As specified in Ag662x driver design DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Instrument driver for Agilent 662x SMU
	/// Provides basic source functions only for 'ElectricalSource' instrument type
	/// </summary>
	public class Inst_Ag662x : InstType_ElectricalSource
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="instrumentName">Instrument name</param>
		/// <param name="driverName">Instrument driver name</param>
		/// <param name="slotId">Slot ID for the instrument</param>
		/// <param name="subSlotId">Sub Slot ID for the instrument</param>
		/// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ag662x(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
			: base (instrumentName, driverName, slotId, subSlotId, chassis)
		{
			// Configure valid hardware information
            //
            //Note: In this case slot refers to the same thing as channel
            //
            // Add Inst_Ag662x details
            InstrumentDataRecord ag6621aData = new InstrumentDataRecord(
                "HP6621A",			// hardware name 
				"0",				// minimum valid firmware version 
				"0");				// maximum valid firmware version 

            ag6621aData.Add("MaxOutputs", "2");    
           
            ValidHardwareData.Add("Ag6621a", ag6621aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6622aData = new InstrumentDataRecord(
                "HP6622A",			// hardware name 
                "0",				// minimum valid firmware version 
                "0");			    // maximum valid firmware version 
            ag6622aData.Add("MaxOutputs", "2");    
           
            ValidHardwareData.Add("Ag6622a", ag6622aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6623aData = new InstrumentDataRecord(
                "HP6623A",			// hardware name 
                "0",				// minimum valid firmware version 
                "0");			    // maximum valid firmware version 
            ag6623aData.Add("MaxOutputs", "3");    
            
            ValidHardwareData.Add("Ag6623a", ag6623aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6624aData = new InstrumentDataRecord(
                "HP6624A",			// hardware name 
                "0",				// minimum valid firmware version 
                "3833");			    // maximum valid firmware version 
            ag6624aData.Add("MaxOutputs", "4");    
            
            ValidHardwareData.Add("Ag6624a", ag6624aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6625aData = new InstrumentDataRecord(
                "HP6625A",			// hardware name 
                "0",				// minimum valid firmware version 
                "0");			    // maximum valid firmware version 
            ag6625aData.Add("MaxOutputs", "2");    
           
            ValidHardwareData.Add("Ag6625a", ag6625aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6626aData = new InstrumentDataRecord(
                "HP6626A",			// hardware name 
                "0",				// minimum valid firmware version 
                "3833");			// maximum valid firmware version 
            ag6626aData.Add("MaxOutputs", "4");    
          
            ValidHardwareData.Add("Ag6626a", ag6626aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6627aData = new InstrumentDataRecord(
                "HP6627A",			// hardware name 
                "0",				// minimum valid firmware version 
                "0");			    // maximum valid firmware version 
            ag6627aData.Add("MaxOutputs", "4");    
            
            ValidHardwareData.Add("Ag6627a", ag6627aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6628aData = new InstrumentDataRecord(
                "HP6628A",			// hardware name 
                "0",				// minimum valid firmware version 
                "0");			    // maximum valid firmware version 
            ag6628aData.Add("MaxOutputs", "2");    
           
            ValidHardwareData.Add("Ag6628a", ag6628aData);

            // Add Inst_Ag662x details
            InstrumentDataRecord ag6629aData = new InstrumentDataRecord(
                "HP6629A",			// hardware name 
                "0",				// minimum valid firmware version 
                "3833");			    // maximum valid firmware version 
            ag6629aData.Add("MaxOutputs", "4");    
            
            ValidHardwareData.Add("Ag6629a", ag6629aData);

			// Configure valid chassis information
			// Add ag662x chassis details
			InstrumentDataRecord ag662xChassisData = new InstrumentDataRecord(
                "Chassis_Ag662x",	// chassis driver name  
				"0",				// minimum valid chassis driver version  
				"1.0.0.0");			// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag662x", ag662xChassisData);

			// Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag662x)base.InstrumentChassis;

            //ensure safe mode operation by default
            safeModeOperation = true;
		}


		#region Public Instrument Methods and Properties

        /// <summary>
        /// We need to override the is online function so that we can check the indicated
        /// slot is in fact correct
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value == true)
                {
                    Int16 myMaxOutputs = Convert.ToInt16(HardwareData["MaxOutputs"]);
                    Int16 slotNo = Convert.ToInt16(this.Slot);
                    if ((slotNo < 1) || (slotNo > myMaxOutputs))
                    {
                        throw new InstrumentException("This instrument supports only " + myMaxOutputs.ToString() + " slots, but config table says this is using slot " + this.Slot);
                    }
                }
            }
        }


		/// <summary>
		/// Unique hardware identification string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the full IDN? string which has 4 comma seperated substrings
                string hid = instrumentChassis.HardwareIdentity;
				return hid;
			}
		}

		/// <summary>
		/// Hardware firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				
				string fv = instrumentChassis.FirmwareVersion;

				// Return firmware version
				return fv;
			}
		}

		/// <summary>
		/// Configures the instrument into a default state.
		/// 
		/// </summary>
		public override void SetDefaultState()
		{
			// Set factory default
			instrumentChassis.Write_Unchecked("CLR", this);
            this.CheckForAnyErrors();
		}

		#endregion

		#region Public ElectricalSource InstrumentType Methods and Properties

		/// <summary>
		/// Sets/returns the output state
		/// </summary>
		public override bool OutputEnabled
		{
			get 
			{
				// Query the output state
				string rtn = instrumentChassis.EmptyString_QueryUnchecked("OUT? " + this.Slot, this);
                this.CheckForAnyErrors();

				// Return bool value
                if (rtn == trueStr)
                {
                    return (true);
                }
                else if (rtn == falseStr)
                {
                    return (false);
                }
                else
                {
                    throw new InstrumentException("Output enabled query has returned unrecognised chars: " + rtn);
                }
			}
			set 
			{
				// Convert bool value to string
				string boolVal = value ? trueStr : falseStr;
                
				// Set output
				instrumentChassis.Write_Unchecked("OUT " +this.Slot +"," + boolVal, this);
                this.CheckForAnyErrors();
			}
		}

		/// <summary>
		/// Sets/returns the current (Iset) setpoint level
		/// </summary>
		public override double CurrentSetPoint_amp
		{
            get
            {
                // Read current in amps
                string cmd = "ISET? " + this.Slot;
                string meas = instrumentChassis.Query_Unchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
			
			set
			{
				// Set the source level in amps
				instrumentChassis.Write_Unchecked("ISET " + this.Slot +", " + value.ToString(), this);
                this.CheckForAnyErrors();
			}
		}

        /// <summary>
        /// Gets the actual measured output current level
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                // Read current in amps
                string cmd = "IOUT? " + this.Slot;
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the voltage level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                // Read voltage in Volts
                string cmd = "VSET? " + this.Slot;
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }

            set
            {
                // Set voltage  source level in volts
                instrumentChassis.Write_Unchecked("VSET " + this.Slot + ", " + value.ToString().Trim(), this);
                this.CheckForAnyErrors();
            }
        }


        /// <summary>
        /// Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Read voltage in Volts
                string cmd = "VOUT? " + this.Slot;
                string meas = instrumentChassis.Query_Unchecked(cmd, this);
                this.CheckForAnyErrors();

                // convert to double
                double rtn = getDblFromString(cmd, meas);

                // Return
                return rtn;
            }
        }

        /// <summary>
        /// Sets/returns the compliance voltage set point
        /// </summary>
		public override double VoltageComplianceSetPoint_Volt
        {
            set
            {
                instrumentChassis.Write_Unchecked("OVSET" + this.Slot + ", " + value.ToString().Trim(), this);
                this.CheckForAnyErrors();
            }
            get
            {
                // Read voltage compliance in volts
                string cmd = "OVSET? " + this.Slot;
                string meas = instrumentChassis.EmptyString_QueryUnchecked(cmd, this);

                this.CheckForAnyErrors();
               
                // convert to double
                double rtn = getDblFromString(cmd, meas);
                
                // Return
                return rtn;

            }
        }


        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                return (safeModeOperation);
            }
            set
            {
                safeModeOperation = value;
            }
        }

		#endregion

        #region Private Methods


        /// <summary>
        /// This function does two checks, first it will call the chassis level checks, ie are there any errors
        /// Second, it checks for slot specific errors, ie status registers 
        /// </summary>
        private void CheckForAnyErrors()
        {
            if (SafeModeOperation)
            {
                instrumentChassis.CheckErrorStatus();
                this.CheckForSlotSpecificErrors();
            }
        }


        /// <summary>
        /// This is checking the registers related to slot specific operations, and throwing
        /// exceptions based on the known error flags.
        /// </summary>
        private void CheckForSlotSpecificErrors()
        {
            byte checkstatusbits = overVoltageProtection | overTemperatureProtection | overCurrentProtection;
            string statusRegister = instrumentChassis.EmptyString_QueryUnchecked("STS? " + this.Slot, this);
            
            byte accumulatedStatusRegister=0;
            string errString = "";
            try
            {
                accumulatedStatusRegister = Convert.ToByte(statusRegister);
            }
            catch (SystemException e)
            {
                errString = "Bad status register: " + statusRegister;
            }
            

            //do we have any flags set whatsoever
            if ((accumulatedStatusRegister & checkstatusbits) != 0)
            {

                //If overVoltageProtection set then lets try and reset the over voltage,
                if ((accumulatedStatusRegister & overVoltageProtection) != 0)
                {
                    //must remove error condition before resetting flag
                    instrumentChassis.Write_Unchecked("VSET " + this.Slot + ", 0.0 ", this);
                    instrumentChassis.Write_Unchecked("OVRST" + this.Slot, this);
                    errString += "OverVoltageProtection flagged" + "\n";
                }

                //If overCurrentProtection set then lets try and reset over current,
                if ((accumulatedStatusRegister & overCurrentProtection) != 0)
                {
                    //must remove error condition before resetting flag
                    instrumentChassis.Write_Unchecked("ISET " + this.Slot + ", 0.0 ", this);
                    instrumentChassis.Write_Unchecked("OCRST" + this.Slot, this);
                    errString += "OverCurrentProtection flagged" + "\n";
                }

                //If overTemperatureProtection set, we can't do anything other than report it.
                if ((accumulatedStatusRegister & overTemperatureProtection) != 0)
                {
                    errString += "OverTemperatureProtection flagged" + "\n";
                }
                
                throw new InstrumentException("Accumulated Status Register indicates error " + accumulatedStatusRegister.ToString() + " for slot " +
                    this.Slot + "\n" + errString);
            }


            //this one doesn't always indicate an error, but potentially it has just changed our current or voltage setting.
            // I will raise as an exception, and user should handle accordingly in their code.
            // We need to prove this one with a 6624 type instrument.
            
            //checkstatusbits = coupledParameter;
            //if ((accumulatedStatusRegister & checkstatusbits) != 0)
            //{
            //    throw new RangeChangeException("Accumulated Status Register indicates coupled parameter bit is set to " + accumulatedStatusRegister + " for slot " + this.Slot);
            //}
        
        }



        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        
        #endregion


        #region Private Data



        // Chassis reference
		private Chassis_Ag662x instrumentChassis;

		// Bool values
		private const string falseStr = "0";
		private const string trueStr = "1";

        //status register bytes
        private const byte overVoltageProtection = 8;
        private const byte overTemperatureProtection = 16;
        private const byte overCurrentProtection = 64;
        private const byte coupledParameter = 128;

        private bool safeModeOperation;
		#endregion

        #region Unsupported methods

        /// <summary>
        /// Sets/returns the compliance current setpoint
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }

        #endregion
    }
}

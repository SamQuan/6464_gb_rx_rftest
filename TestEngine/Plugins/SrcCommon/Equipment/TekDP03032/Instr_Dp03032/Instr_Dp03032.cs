// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_Dp03032.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Instr_Dp03032 : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_Dp03032(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_Dp03032 = new InstrumentDataRecord(
                "TEKTRONIX DPO3032",				// hardware name 
                "0",  			// minimum valid firmware version 
                "CF:91.1CT FV:v1.04");			// maximum valid firmware version 
            ValidHardwareData.Add("Dp03032", instr_Dp03032);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Dp03032",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Dp03032", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Dp03032)chassisInit;

            
           
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idn = res.Split(',');
                return idn[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idn = res.Split(',');
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            this.Clear();
            this.Reset();
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Oscillograph special function

        /// <summary>
        /// Get or Set channel No;
        /// </summary>
        public int ChanNo
        {
            
            get
            {
                string command;
                string res;
               
                command = "MEASU:IMM:SOUrce1?";
                res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Trim().ToUpper() == "CH1")
                {
                    chanNo = 1;
                }
                else if (res.Trim().ToUpper() == "CH2")
                {
                    chanNo = 2;
                }
                
                    
              
                return chanNo;
            }
            set
            {
                chanNo = value;
                string command = "MEASU:IMM:SOUrce1 CH" + chanNo.ToString();
                
                
                this.instrumentChassis.Write_Unchecked(command, this);

            }
        }

        /// <summary>
        /// Get or Set the BandWidth
        /// </summary>
        public BandWidth BW
        {
            get
            {
                string command = "CH" + chanNo.ToString() + ":BAND?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                BandWidth bwRes = BandWidth.FULl;
                if (double.Parse(res) == 20E+6)
                {
                    bwRes = BandWidth.TWEnty;
                }
                else if (double.Parse(res) == 300E+6)
                {
                    bwRes = BandWidth.FULl;
                }

                

                return bwRes;
            }
            set
            {
                bw = value;
                string command = "CH" + chanNo.ToString() + ":BAND " + bw.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
                
            }
        }


        /// <summary>
        /// Get or Set Impedance
        /// </summary>
        public Impedance Resistance
        {
            get
            {
                
                string command = "CH" + chanNo.ToString() + ":TER?";
                string res = instrumentChassis.Query_Unchecked(command, this);
                Impedance imp = Impedance.FIFty;
                if (double.Parse(res.Trim()) == 1E+6)
                {
                    resistance = Impedance.MEG;
                }
                else if (double.Parse(res.Trim()) == 50)
                {
                    resistance = Impedance.FIFty;
                }
                else if (double.Parse(res.Trim()) == 75)
                {
                    resistance = Impedance.SEVENTYFive;
                }
                
                return resistance;
            }
            set
            {
               
                resistance = value;
                string command = "CH" + chanNo.ToString() + ":TER " + resistance.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);


            }
        }


        /// <summary>
        /// Set or Get the Amplitude
        /// </summary>
        public double Amplitude_V
        {
            get
            {
                string command = "POW:RIPP:RES:AMPL?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                amplitude = double.Parse(res);
                return amplitude;

            }
            set
            {
                amplitude = value;
                string command = "POW:RIPP:RES:AMPL " + amplitude.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        /// <summary>
        /// Get or Set the Frequency.
        /// </summary>
        public double Frequency_Khz
        {
            get
            {
                this.MeasureType = MeasurementType.MEAN;
                this.MeasureType = MeasurementType.FREQuency;
                string unit = this.MeasurementUnit;
                double value = this.MeasureValue;
                frequency = value;
                if (unit.Trim().ToLower() == "hz")
                {
                    frequency = value / 1000;
                }
                else
                {
                    throw new InstrumentException("this unit is not support!");
                }

                return frequency;

            }
           
        }

        /// <summary>
        /// Get or Set the RMS.
        /// </summary>
        public double VRMS_mV
        {
            get
            {
                this.MeasureType = MeasurementType.MEAN;
                this.MeasureType = MeasurementType.RMS;
                string unit = this.MeasurementUnit;
                unit = unit.Substring(0, 1);
                double value = this.MeasureValue;
                rms = value;
                if (unit.Trim().ToUpper() == "V")
                {
                    rms = value * 1000;
                }
                else
                {
                    throw new InstrumentException("this unit is not support!");
                }

                return rms;

            }

        }


        /// <summary>
        /// Get the Measurement value
        /// </summary>
        public double MeasureValue
        {
            get
            {
                string command = "MEASUrement:IMMed:VALue?";

                string res ="";
                string error;
                try
                {
                    res = this.instrumentChassis.Query_Unchecked(command, this);
                }
                catch (Exception ex)
                {
                    error = ex.ToString();
                }
                measureValue = double.Parse(res.Trim());

                return measureValue;
            }
        }

        /// <summary>
        /// Get the Measurement unit
        /// </summary>
        public string MeasurementUnit
        {
            get
            {
                string command = "MEASUrement:IMMed:UNIts?";
               
                string res ="";
                string error;
                try
                {
                    res = this.instrumentChassis.Query_Unchecked(command, this);
                }
                catch (Exception ex)
                {
                    error = ex.ToString();
                }
                measureUnit = res.Trim();
                //int index1 = res.IndexOf('\\');
                //int index2 = res.LastIndexOf("\\");
                measureUnit = res.Trim().Substring(1, 2);

                return measureUnit;
            }
        }

        /// <summary>
        /// Set or Get the Measurement Type
        /// for example, frequency, RMS
        /// and maximum, mean,minimum
        /// </summary>
        public MeasurementType MeasureType
        {
            get
            {
                string command = "MEASUrement:IMMed:TYPe?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Trim() == MeasurementType.FREQuency.ToString())
                {
                    measureType = MeasurementType.FREQuency;
                }
                else if (res.Trim() == MeasurementType.RMS.ToString())
                {
                    measureType = MeasurementType.RMS;
                }
                else if (res.Trim() == MeasurementType.HIGH.ToString())
                {
                    measureType = MeasurementType.HIGH;
                }
                else if (res.Trim() == MeasurementType.LOW.ToString())
                {
                    measureType = MeasurementType.LOW;
                }
                else if (res.Trim() == MeasurementType.MAXI.ToString())
                {
                    measureType = MeasurementType.MAXI;
                }
                else if (res.Trim() == MeasurementType.MINI.ToString())
                {
                    measureType = MeasurementType.MINI;
                }
                else if (res.Trim() == MeasurementType.MEAN.ToString())
                {
                    measureType = MeasurementType.MEAN;
                }
                else
                {
                    throw new InstrumentException("this function has not implement!");
                }

                return measureType;
            }
            set
            {
                measureType = value;
                string command = "MEASUrement:IMMed:TYPe " + measureType.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// the list of the measurement type
        /// </summary>
        public enum MeasurementType
        {
            FREQuency,
            RMS,
            HIGH,
            LOW,
            MAXI,
            MINI,
            MEAN

        }

        /// <summary>
        /// Band width
        /// </summary>
        public enum BandWidth
        {
            TWEnty,
            //TWOfifty,
            FULl,
            //NR3

        }

        /// <summary>
        /// Impedance
        /// </summary>
        public enum Impedance
        {
            FIFty,
            SEVENTYFive,
            MEG,
            //NR3
        }

        //public struct SelectChannelNoStatus
        //{
        //    public int ChannelNo;
        //    public bool ChannelNoStatus;
        //}

        #endregion

        #region command special

        private string cmdChannnel;
        private string cmdImpedance;
        private string cmdBW;
        private string cmdMesFreq;
        private string cmdMesSqrt;
        //private string cmdImpedance;
        private string cmdFreq;

        #endregion

        #region helper method

        /// <summary>
        /// Clear the instrument
        /// </summary>
        public void Clear()
        {
            string command = "*CLS";
            this.instrumentChassis.Write_Unchecked(command, this);

        }

        /// <summary>
        /// Reset the instrument
        /// </summary>
        public void Reset()
        {
            string command = "*RST";
            this.instrumentChassis.Write_Unchecked(command, this);
        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Dp03032 instrumentChassis;
        private MeasurementType measureType;
        private double rms;
        private string measureUnit;
        private double measureValue;
        private int chanNo;
        private BandWidth bw;
        private Impedance resistance;
        private double amplitude;
        private double frequency;
        private double measMean;
        #endregion
    }
}

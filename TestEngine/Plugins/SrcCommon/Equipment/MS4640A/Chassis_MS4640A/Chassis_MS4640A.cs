// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_MS4640A.cs
//
// Author: wendy.wen, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.ChassisNS
{
    /// <summary>
    /// // TODO: 
    /// 1. Change the base class to the type of chassis you are implementing 
    ///   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    ///    based types).
    /// 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    /// 3. Fill in the gaps.
    /// </summary>
    public class Chassis_MS4640A : ChassisType_Visa
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_MS4640A(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassMS4640A = new ChassisDataRecord(
               // "ANRITSU MS4640A",			// hardware name 
              //  "ANRITSU 37397C",			// hardware name 
                "ANRITSU MS46322A",			// hardware name 
                "0",			// minimum valid firmware version 
                "zzzzzzzzzz");		// maximum valid firmware version 
            ValidHardwareData.Add("37397C", chassMS4640A);
            
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassMS4644A = new ChassisDataRecord(
                "ANRITSU MS4644A",			// hardware name 
                "0",			// minimum valid firmware version 
                "zzzzzzzzzz");		// maximum valid firmware version 
            ValidHardwareData.Add("MS4644A", chassMS4644A);

            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassMS4644B = new ChassisDataRecord(
                "ANRITSU MS4644B",			// hardware name 
                "0",			// minimum valid firmware version 
                "zzzzzzzzzz");		// maximum valid firmware version 
            ValidHardwareData.Add("MS4644B", chassMS4644B);
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassMS4647B = new ChassisDataRecord(
                "ANRITSU MS4647B",			// hardware name 
                "0",			// minimum valid firmware version 
                "zzzzzzzzzz");		// maximum valid firmware version 
            ValidHardwareData.Add("MS4647B", chassMS4647B);
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chass_sim = new ChassisDataRecord(
                "ANRITSU DEFAULT",			// hardware name 
                "0",			// minimum valid firmware version 
                "zzzzzzzzzz");		// maximum valid firmware version 
            ValidHardwareData.Add("Simulated", chass_sim);


        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = this.GetIdn();

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = this.GetIdn().Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis before it goes online
                    // E.g. setup RS232 variables
                }

                // setup base class

                base.IsOnline = value;
                



                if (value) // if setting online                
                {
                    // TODO: custom setup for this chassis after it goes online, 
                    // e.g. Standard Error register on a 488.2 instrument

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);    
                    base.Timeout_ms = 5000;                
                }
            }
        }
        #endregion


        #region other members

        /// <summary>
        /// get IDN
        /// </summary>
        /// <returns>Identity</returns>
        public string GetIdn()
        {
            //System.Threading.Thread.Sleep(3);
            string res = Query_Unchecked("*IDN?\n", null);


            return res;
            
        }

        /// <summary>
        /// write command
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="instrument"></param>
        public void Write(string cmd, Instrument instrument)
        {
            Write_Unchecked(cmd, instrument);
            System.Threading.Thread.Sleep(20);
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// InstType_LightwaveComponentAnalyzer.cs
//
// Author: cypress.chen, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer
{
    
 


    /// <summary>
    /// Lightwave Componet Analyzer instrument type, eg. Ag8703B etc
    /// </summary>
    public abstract class InstType_LightwaveComponentAnalyzer : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public InstType_LightwaveComponentAnalyzer(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit):base(instrumentNameInit,driverNameInit,
                    slotInit,subSlotInit,chassisInit)
        {
        }
        #endregion

        #region constant
        /// <summary>
        /// Means sweep time is auto setting
        /// </summary>
        public const double AUTO_SWEEP_TIME = double.MaxValue;
        #endregion


        #region Instrument Type abstract methods
        /// <summary>
        /// set/get which channel is active for measurement
        /// </summary>
        public abstract int ActiveMeausurementChannel
        {
            set;
            get;
        }

        /// <summary>
        /// Set/get what data are displayed
        /// </summary>
        public abstract void SetDataDisplayMode(DataDisplayMode dataDisplayMode);

        /// <summary>
        /// Stores the data trace in channel memory 
        /// </summary>
        public abstract void StoreTraceDataInChannelMemory();

        /// <summary>
        /// Set/get active marker 
        /// </summary>
        public abstract Marker ActiveMarker
        {
            get;
            set;
        }

        /// <summary>
        /// Set/get the reference position 
        /// </summary>
        public abstract double ReferencePosition
        {
            get;
            set;
        }

        /// <summary>
        /// Set/get the output power level 
        /// </summary>
        public abstract double SourceOutputPower_dBm { get;set;}

        /// <summary>
        /// Set/get the IF bandwidth 
        /// </summary>
        public abstract double ForwardCurrentBandwidth { get;set;}

        /// <summary>
        /// Set/get the sweep time 
        /// </summary>
        public abstract double SweepTime { get;set;}

   
        /// <summary>
        /// Set/get the reference line value 
        /// </summary>
        public abstract double ReferenceLineValue { get;set;}

        /// <summary>
        /// Set the reference value to that of the active marker�s amplitude 
        /// </summary>
        public abstract void SetReferenceValueToActiveMarker();

        /// <summary>
        /// Preset the analyzer to the factory preset state 
        /// </summary>
        public abstract void Preset();

        /// <summary>
        /// Sets the trace scale factor 
        /// </summary>
        public abstract double Scale { get;set;}

        /// <summary>
        /// Set/get the number of points in the sweep, or in a sweep segment 
        /// </summary>
        public abstract uint NumberOfSweepPoint { get;set;}

        /// <summary>
        /// Set/get smoothing state
        /// </summary>
        public abstract bool SmoothingEnable { get;set;}

        /// <summary>
        /// Set/get the smoothing aperture as a percent of the trace. 
        /// </summary>
        public abstract double SmoothingAperturePercent { get;set;}

        /// <summary>
        /// Set/get the averaging on and off on active channel 
        /// </summary>
        public abstract bool ActiveChannelAveragingEnable { get;set;}

        /// <summary>
        /// Set/get the averaging factor on the active channel 
        /// </summary>
        public abstract double ActiveChannelAveragingFactor { get;set;}

        /// <summary>
        /// Recalls from save/recall registers
        /// </summary>
        /// <param name="registerNumber">register number</param>
        public abstract void RecallRegister(int registerNumber);
        
        /// <summary>
        /// Set a specified marker active and its stimulus value 
        /// </summary>
        /// <param name="marker">marker</param>
        /// <param name="value">stimulus value</param>
        public abstract void SetSpecifiedMarkerStimulus_GHz(Marker marker,double value);

        /// <summary>
        /// double ReadSpecifiedMarkerStinulus_GHz(Marker)
        /// </summary>
        /// <param name="marker"></param>
        /// <returns></returns>
        public abstract double ReadSpecifiedMarkerStimulus_GHz(Marker marker);

        /// <summary>
        /// Places the fixed marker at the active marker position and makes it the delta reference
        /// </summary>
        public abstract void ZeroMarker();

        /// <summary>
        /// search for maximum on current channel's trace 
        /// </summary>
        public abstract void SetMarkerMaximum();

        /// <summary>
        /// search for maximum on current channel's trace 
        /// </summary>
        public abstract void SetMarkerMinimum();

        /// <summary>
        /// Trigger instrument sweep in a specified mode 
        /// </summary>
        /// <param name="sweepTriggerMode">Sweep trigger mode</param>
        public abstract void TriggerSweep(SweepTriggerMode sweepTriggerMode);

        /// <summary>
        /// Set/get start frequency 
        /// </summary>
        public abstract double StartFrequency_GHz { get;set;}

        /// <summary>
        /// Set/get stop frequence 
        /// </summary>
        public abstract double StopFrequency_GHz { get;set;}

        /// <summary>
        /// Set/get s-parameter 
        /// </summary>
        public abstract SparameterMeasurementMode SparameterMeasurementMode { get;set;}

        /// <summary>
        /// Set/get display format 
        /// </summary>
        public abstract DisplayFormat DisplayFormat { get;set;}

        /// <summary>
        /// Sets electrical length so group delay is zero at the active marker's stimulus.
        /// </summary>
        public abstract void SetMarkerDelay();

        /// <summary>
        /// Set/get the electrical delay offset in second 
        /// </summary>
        public abstract double ElectricalDelayOffset_s { get;set;}

        /// <summary>
        /// Set/get the phase offset. 0-360 degrees
        /// </summary>
        public abstract double PhaseOffset { get;set;}

            /// <summary>
        /// Measurement restart 
        /// </summary>
        public abstract void MeasurementRestart();

        /// <summary>
        /// Waits for all buffered instructions to complete before returning to the caller 
        /// </summary>
        public abstract void WaitForCleanSweep();

        
        public abstract void WaitForCleanSweepFast();
        
        /// <summary>
        /// Auto scale the active channel 
        /// </summary>
        public abstract void AutoScale();

        /// <summary>
        /// Sets the centre stimulus value to that of the active marker's stimulus value. 
        /// </summary>
        public abstract void SetMarkerToCentre();

        /// <summary>
        /// Sets the start stimulus to that of the active marker�s. 
        /// </summary>
        public abstract void SetMarkerToStart();

        /// <summary>
        /// gets the trace data in a specified format 
        /// </summary>
        /// <param name="outputFormat"></param>
        /// <returns></returns>
        public abstract string GetSpecifiedFormatTraceData(OutputFormat outputFormat);

        /// <summary>
        /// gets the trace data from the instrument
        /// </summary>
        /// <returns></returns>
        public abstract Trace GetTraceData();

        /// <summary>
        /// Gets the trace data from the instrument
        /// </summary>
        /// <param name="pointType">2D or 3D</param>
        /// <returns>Points list</returns>
        public abstract Trace GetTraceData(PointType pointType);

        public abstract Trace GetTraceDataFAST(PointType pointType);

        #endregion


        // Dave Smith - Update 1 start

        #region VirtualMethods

        public virtual void SetCWSweepMode(double cwFrequency, int numPts)
        {
            throw new Exception("CW mode not supported");
        }

        public virtual void ReadActiveMarker(ref double mag, ref double phase)
        {
            throw new Exception("ReadActiveMarker not implemented");
        }
        #endregion
        // Dave Smith - Update 1 end

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer
{
    /// <summary>
    /// point list
    /// </summary>
    public class Trace : List<Point>
    {
        /// <summary>
        /// add point
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Add(double x, double y)
        {
            Point point = new Point(x, y);
            Add(point);
        }


        /// <summary>
        /// add point
        /// </summary>
        /// <param name="x">x</param>
        /// <param name="y">y</param>
        /// <param name="z">z</param>
        public void Add(double x, double y, double z)
        {
            Point point = new Point(x, y, z);
            Add(point);
        }

        /// <summary>
        /// get X array
        /// </summary>
        /// <returns>X array</returns>
        public double[] GetXArray()
        {
            double[] xArray = new double[Count];
            int i = 0;
            foreach (Point point in this)
            {
                xArray[i] = point.X;
                i++;
            }

            return xArray;
        }

        /// <summary>
        /// get Y array
        /// </summary>
        /// <returns>Y array</returns>
        public double[] GetYArray()
        {
            double[] yArray = new double[Count];
            int i = 0;
            foreach (Point point in this)
            {
                yArray[i] = point.Y;
                i++;
            }

            return yArray;
        }


        /// <summary>
        /// get Z array
        /// </summary>
        /// <returns>Z array</returns>
        public double[] GetZArray()
        {
            double[] zArray = new double[Count];
            int i = 0;
            foreach (Point point in this)
            {
                zArray[i] = point.Z;
                i++;
            }

            return zArray;
        }
    }

    /// <summary>
    /// contains x and y values
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(double x, double y)
        {
            _x = x;
            _y = y;
            _z = 0;
        }

        /// <summary>
        /// Constructor added at Point include real and imag
        /// </summary>
        /// <param name="x">X Axis</param>
        /// <param name="y">Real</param>
        /// <param name="z">Imag</param>
        public Point(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        private double _x;
        private double _y;
        private double _z;

        /// <summary>
        /// set/get X value
        /// </summary>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }
        /// <summary>
        /// set/get Y value
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// set/get Z value
        /// </summary>
        public double Z
        {
            get { return _z; }
            set { _z = value; }
        }
    }
}

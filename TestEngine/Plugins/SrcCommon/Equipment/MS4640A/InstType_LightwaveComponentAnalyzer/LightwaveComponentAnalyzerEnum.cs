using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer
{
    #region enum type

    /// <summary>
    /// Channel name
    /// </summary>
    public enum Channel
    {
        /// <summary>
        /// channel 1,value is 0
        /// </summary>
        Channel1=0,
        /// <summary>
        /// channel 2,value is 1
        /// </summary>
        Channel2=1

    }

    /// <summary>
    /// Display what data
    /// </summary>
    public enum DataDisplayMode
    {
        /// <summary>
        /// 
        /// </summary>
        DataOnly,
        /// <summary>
        /// 
        /// </summary>
        DataAndMemory,
        /// <summary>
        /// 
        /// </summary>
        DataDividedByMemory,
        /// <summary>
        /// 
        /// </summary>
        DataMinusMemory,
        /// <summary>
        /// 
        /// </summary>
        MemoryOnly,
        /// <summary>
        /// 
        /// </summary>
        DataPlusMemory,
        /// <summary>
        /// 
        /// </summary>
        DataTimesMemory,
        /// <summary>
        /// 
        /// </summary>
        Memory1DividedByMemory2,
        /// <summary>
        /// 
        /// </summary>
        Memory1MinusByMemory2,
        /// <summary>
        /// 
        /// </summary>
        Memory1PlusMemory2,
        /// <summary>
        /// 
        /// </summary>
        Memory1timesMemory2,
        /// <summary>
        /// 
        /// </summary>
        Memory2DividedByMemory1,
        /// <summary>
        /// 
        /// </summary>
        Memory2MinuseMemory1,
        /// <summary>
        /// 
        /// </summary>
        MathResults,
        /// <summary>
        /// 
        /// </summary>
        MemoryMinusData,
        /// <summary>
        /// 
        /// </summary>
        MemoryDividedByData
    }

    /// <summary>
    /// Marker name
    /// </summary>
    public enum Marker
    {
        /// <summary>
        /// No Mark for MS4640A
        /// </summary>
        NoMarker = 0,
        /// <summary>
        /// 
        /// </summary>
        Marker1=1,
        /// <summary>
        /// 
        /// </summary>
        Marker2=2,
        /// <summary>
        /// 
        /// </summary>
        Marker3=3,
        /// <summary>
        /// 
        /// </summary>
        Marker4=4,
        /// <summary>
        /// 
        /// </summary>
        Marker5=5,

        /// <summary>
        /// Marker 6 for MS4640A
        /// </summary>
        Marker6=6
    }

    /// <summary>
    /// Sweep trigger mode
    /// </summary>
    public enum SweepTriggerMode
    {
        /// <summary>
        /// single sweep
        /// </summary>
        Single,
        /// <summary>
        /// Activates the indicated number of groups
        /// of sweeps. A group is whatever is needed to
        /// update the current parameter once. This
        /// function restarts averaging if it is enabled.
        /// </summary>
        Group,
        /// <summary>
        /// continuous sweep trigger mode
        /// </summary>
        Continuous,
        /// <summary>
        /// hold mode
        /// </summary>
        Hold
    }

    /// <summary>
    /// s-parameter measurement mode
    /// </summary>
    public enum SparameterMeasurementMode
    {
        /// <summary>
        /// 
        /// </summary>
        S11,
        /// <summary>
        /// 
        /// </summary>
        S21,
        /// <summary>
        /// 
        /// </summary>
        S12,
        /// <summary>
        /// 
        /// </summary>
        S22
    }

    /// <summary>
    /// display format
    /// </summary>
    public enum DisplayFormat
    {
        /// <summary>
        /// the data formatted as group delay
        /// </summary>
        Delay,
        /// <summary>
        /// the imaginary display forma
        /// </summary>
        Imaginary,
        /// <summary>
        /// the linear magnitude display format
        /// </summary>
        LinearMagnitude,
        /// <summary>
        /// the log magnitude display format
        /// </summary>
        LogMagnitude,
        /// <summary>
        /// the phase display format
        /// </summary>
        Phase,
        /// <summary>
        /// the polar display format
        /// </summary>
        Polar,
        /// <summary>
        /// the real display format
        /// </summary>
        Real,
        /// <summary>
        /// the Smith Chart display forma
        /// </summary>
        SmithChart,
        /// <summary>
        /// the SWR display format
        /// </summary>
        SWR
    }

    ///// <summary>
    ///// register name
    ///// </summary>
    //public enum Register
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    Register1,
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    Register2,
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    Register3,
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    Register4,
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    Register5
    //}

    /// <summary>
    /// output data format which a PC gets
    /// </summary>
    public enum OutputFormat
    {
        /// <summary>
        /// The analyzer's internal binary format, 6 bytes-per-data point.
        /// The array is preceded by a four-byte header. The first two 
        /// bytes represent the string ?A? the standard block header. 
        /// The second two bytes are an integer representing the number of 
        /// bytes in the block to follow. 
        /// </summary>
        Binary,
        /// <summary>
        /// IEEE 32-bit floating-point format, 4 bytes-per-number,
        /// 8 bytes-per-data point. The data is preceded by the same header 
        /// as in Binary format. Each number consists of a 1-bit sign, an 
        /// 8-bit biased exponent, and a 23-bit mantissa.
        /// </summary>
        IEEE32bitFloatingPoint,
        /// <summary>
        /// IEEE 64-bit floating-point format, 8 bytes-per-number, 16 
        /// bytes-per-data point. The data is preceded by the same header 
        /// as in FORM1. Each number consists of a 1-bit sign, an 11-bit 
        /// biased exponent, and a 52-bit mantissa. This format may be used 
        /// with double-precision floating-point numbers.
        /// </summary>
        IEEE64bitFloatingPoint,
        /// <summary>
        /// ASCII floating-point format. The data is transmitted as ASCII 
        /// numbers.
        /// </summary>
        AsciiFloatingPoint,
        /// <summary>
        /// PC-DOS 32-bit floating-point format with 4 bytes-per-number,8 
        /// bytes-per-data point.
        /// </summary>
        Pcdos32bitFloatingPoint
    }

    /// <summary>
    /// Point Type
    /// </summary>
    public enum PointType
    {
        /// <summary>
        /// 
        /// </summary>
         Two_Dimensional,
        /// <summary>
        /// 
        /// </summary>
         Three_Dimensional
    }

    /// <summary>
    /// 
    /// </summary>
    public enum FlexibleCalType
    {
        /// <summary>
        /// 
        /// </summary>
        S11,
        /// <summary>
        /// 
        /// </summary>
        S12,
        /// <summary>
        /// 
        /// </summary>
        S22,
        /// <summary>
        /// 
        /// </summary>
        S21,
    }



    #endregion
}

using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using System.IO;

namespace TEST.MS4640A
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_MS4640A testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Instr_MS4640A_VNA testInstr;
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_MS4640A("MyChassis", "Chassis_MS4640A", "TCPIP0::vs-1130237::inst0::INSTR");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Instr_MS4640A_VNA("MS4640A", "Instr_MS4640A_VNA", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            //// You can use Assert to check return values from tests
            //Assert.AreEqual(1, 1); // this will succeed
            //// Don't forget to use special functions for Double metavalues...
            //Assert.IsNaN(Double.NaN);
           
            //////testInstr.SetDefaultState();

            //////testInstr.StartFrequency_GHz = 0.13;
            //////Assert.AreEqual(0.13, testInstr.StartFrequency_GHz);

            //////testInstr.StopFrequency_GHz = 20;
            //////Assert.AreEqual(20, testInstr.StopFrequency_GHz);

            //////testInstr.ActiveChannelAveragingEnable = true;
            //////Assert.AreEqual(true, testInstr.ActiveChannelAveragingEnable);

            //////testInstr.ActiveChannelAveragingFactor = 1;
            //////Assert.AreEqual(1, testInstr.ActiveChannelAveragingFactor);


            //////testInstr.SmoothingEnable = true;
            //////Assert.AreEqual(true, testInstr.SmoothingEnable);

            //////testInstr.SmoothingAperturePercent = 3;
            //////Assert.AreEqual(3, testInstr.SmoothingAperturePercent);

            //////testInstr.NumberOfSweepPoint = 801;
            //////Assert.AreEqual(801, testInstr.NumberOfSweepPoint);

            //////testInstr.ForwardCurrentBandwidth = 1000;
            //////Assert.AreEqual(1000, testInstr.ForwardCurrentBandwidth);

            testInstr.Preset();
            testInstr.RecalSetup("'c:\\testset\\cal\\full port.chx'");
            testInstr.RecalEmbedingSetting("'c:\\testset\\cal\\EmbedDeembed.edl'");
            testInstr.ChannelCount = 1;
            testInstr.ActiveMeausurementChannel = 1;
            testInstr.SparameterMeasurementMode = SparameterMeasurementMode.S22;
            //testInstr.DisplayFormat = DisplayFormat.LinearMagnitude;

            testChassis.Timeout_ms = 2000;
            testInstr.Timeout_ms = 2000;
            testInstr.SweepTime = 1000;

            testInstr.WaitForCleanSweep();
            testInstr.MeasurementRestart();
            int number = (int)testInstr.NumberOfSweepPoint;

            Trace trace = testInstr.GetTraceData(PointType.Three_Dimensional);

            double[] freq = trace.GetXArray();
            double[] real = trace.GetYArray();
            double[] mig = trace.GetZArray();
            List<double> mag = new List<double>();
            List<double> phaseList = new List<double>();
            for (int i = 0; i < real.Length; i++)
            {
                double temp = real[i] * real[i] + mig[i] * mig[i];
                temp = Math.Sqrt(temp);
                temp = 10 * Math.Log10(temp);
                mag.Add(temp);
                phaseList.Add(Math.Atan2(mig[i], real[i]) / Math.PI * 180);
            }
            double[] magArray = mag.ToArray();
            double[] phaseArray = phaseList.ToArray();
            Assert.AreEqual(801, trace.Count);
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");

            testInstr.SetDefaultState();

            testInstr.SetDataDisplayMode(DataDisplayMode.DataOnly);

            //1.set cal status to OFF
            testInstr.CalibrationStatus = false;
            //2.set start stop freq and sweep point
            testInstr.StartFrequency_GHz = 0.13;
            testInstr.StopFrequency_GHz = 40;
            testInstr.NumberOfSweepPoint = 801;
            //
            testInstr.SourceOutputPower_dBm = 0;

            //set display
            //testInstr.DisplayFormat= DisplayFormat .LinearMagnitude;
            testInstr.RecalSetup(@"'c:\\testset\\cal\\XChipSetup.chx'");


            testInstr.CalibrationPortType = Instr_MS4640A_VNA.CalibrationPortTypes.TwoPort;
            testInstr.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT1;
            testInstr.CalibrationMethod= Instr_MS4640A_VNA.CalibrationMethods.SOLR;

            //S11 OPEN SHORT LOAD
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT1:OPEN", this.testInstr);
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT1:SHORT", this.testInstr);
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT1:LOAD", this.testInstr);
            //S22 OPEN SHORT LOAD
            testInstr.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT2;
            testInstr.CalibrationMethod = Instr_MS4640A_VNA.CalibrationMethods.SOLR;
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT2:OPEN", this.testInstr);
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT2:SHORT", this.testInstr);
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT2:LOAD", this.testInstr);
            //S21 THRU
            testInstr.CalibrationPort = Instr_MS4640A_VNA.CalibrationPorts.PORT12;
            this.testChassis.Write(":SENSe1:CORRection:COLLect:PORT12:THRU", this.testInstr);

            this.testChassis.Write(":SENSe1:CORRection:COLLect:SAVE", this.testInstr);





            testInstr.RecalSetup(@"'c:\\testset\\cal\\XChipSetup.chx'");



            this.testChassis.Write(":CALCulate1:FSIMulator:NETWork:CLEar", this.testInstr);
            //string str2 = this.testChassis.Query_Unchecked(":CALCulate1:FSIMulator:NETWork:S2P?", this.testInstr);
            //string str4 = this.testChassis.Query_Unchecked(":CALCulate1:FSIMulator:NETWork?", this.testInstr);
            //this.testChassis.Write(":CALCulate1:FSIMulator:NETWork1:DELete", this.testInstr);

            //this.testChassis.Write(":CALCulate1:FSIMulator:NETWork:ADD", this.testInstr);
            //this.testChassis.Write(":CALCulate1:FSIMulator:NETWork:TYPe S2P", this.testInstr);
            //string str3=this.testChassis.Query_Unchecked(":CALCulate1:FSIMulator:NETWork:TYPe?", this.testInstr);
            //this.testChassis.Write(":CALCulate1:FSIMulator:NETWork:S2P 'C:\\testset\\ddfff\\MZdeembeded201111121123.s2p'", this.testInstr);
            //this.testInstr.EmbedOrDeEmbedEnable = true;
            //this.testChassis.Write(":SENSe1:CORRection:COLLect:FLEXible:DEFine CUSTOM,S21", this.testInstr);
            //this.testChassis.Write(":SENSe1:CORRection:COLLect:FLEXible 1", this.testInstr);
            this.testInstr.SparameterMeasurementMode = SparameterMeasurementMode.S11;



            testInstr.StoreTraceDataInChannelMemory();

            testInstr.ActiveMarker = Marker.Marker3;

            Assert.AreEqual(Marker.Marker3, testInstr.ActiveMarker);

            testInstr.ReferencePosition = 5;

            Assert.AreEqual(5, testInstr.ReferencePosition);

            testInstr.SourceOutputPower_dBm = 0;

            Assert.AreEqual(0, testInstr.SourceOutputPower_dBm);

           
            testInstr.ForwardCurrentBandwidth = 3;
            Assert.AreEqual(3, testInstr.ForwardCurrentBandwidth);

            testInstr.SweepTime = 200;

            Assert.AreEqual(200, testInstr.SweepTime);

            testInstr.ReferenceLineValue = 5;

            Assert.AreEqual(5, testInstr.ReferenceLineValue);

            testInstr.SetReferenceValueToActiveMarker();



            testInstr.Preset();



            testInstr.Scale = 20;

            Assert.AreEqual(20, testInstr.Scale);


            testInstr.SmoothingEnable = true;

            Assert.AreEqual(true, testInstr.SmoothingEnable);

            testInstr.SmoothingAperturePercent = 5;

            Assert.AreEqual(5, testInstr.SmoothingAperturePercent);

            testInstr.ActiveChannelAveragingEnable = true;

            Assert.AreEqual(true, testInstr.ActiveChannelAveragingEnable);

            testInstr.ActiveChannelAveragingFactor = 3;

            Assert.AreEqual(3, testInstr.ActiveChannelAveragingFactor);

            testInstr.RecallRegister(3);


            // Value Set is not same as read out
            testInstr.SetSpecifiedMarkerStimulus_GHz(Marker.Marker3, 2);



            double markValue = testInstr.ReadSpecifiedMarkerStimulus_GHz(Marker.Marker3);



            testInstr.ZeroMarker();



            testInstr.SetMarkerMaximum();



            testInstr.SetMarkerMinimum();



            testInstr.TriggerSweep(SweepTriggerMode.Continuous);





            testInstr.SparameterMeasurementMode = SparameterMeasurementMode.S21;

            Assert.AreEqual(SparameterMeasurementMode.S21, testInstr.SparameterMeasurementMode);
            testInstr.TraceFormat = Instr_MS4640A_VNA.TraceFormats.LINPH;

            Assert.AreEqual(Instr_MS4640A_VNA.TraceFormats.LINPH, testInstr.TraceFormat);


            testInstr.SetMarkerDelay();


            testInstr.ElectricalDelayOffset_s = 3;
            Assert.AreEqual(3, testInstr.ElectricalDelayOffset_s);

            // not verified for 
            testInstr.PhaseOffset = 50;

            Assert.AreEqual(50, testInstr.PhaseOffset);

            testInstr.MeasurementRestart();



            testInstr.WaitForCleanSweep();



            testInstr.AutoScale();



            testInstr.SetMarkerToCentre();



            testInstr.SetMarkerToStart();



        }





        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_MS4640A_VNA.cs
//
// Author: wendy.wen, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes.LightwaveComponentAnalyzer;
using Bookham.TestSolution.ChassisNS;
using System.Xml;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// MS4640A driver
    /// </summary>
    public class Instr_MS4640A_VNA : InstType_LightwaveComponentAnalyzer
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_MS4640A_VNA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_MS4640A = new InstrumentDataRecord(
                //  "ANRITSU MS4640A",				// hardware name 
                //  "ANRITSU 37397C",				// hardware name 
                "ANRITSU MS46322A",			// hardware name 
                "0",  			// minimum valid firmware version 
                "zzzzzzzzzz");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrMS4640A", instr_MS4640A);
            //      ValidHardwareData.Add("InstrMS37397", instr_MS4640A);

            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_MS4644A = new InstrumentDataRecord(
                "ANRITSU MS4644A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "zzzzzzzzzz");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrMS4644A", instr_MS4644A);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_MS4640A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_MS4640A", chassisInfo);



            InstrumentDataRecord instr_MS4644B = new InstrumentDataRecord(
            "ANRITSU MS4644B",				// hardware name 
            "0",  			// minimum valid firmware version 
            "zzzzzzzzzz");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrMS4644B", instr_MS4644B);

            InstrumentDataRecord instr_MS4647B = new InstrumentDataRecord(
            "ANRITSU MS4647B",				// hardware name 
            "0",  			// minimum valid firmware version 
            "zzzzzzzzzz");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrMS4647B", instr_MS4647B);

            InstrumentDataRecord instr_sim = new InstrumentDataRecord(
            "ANRITSU DEFAULT",				// hardware name 
             "0",  			// minimum valid firmware version 
            "zzzzzzzzzz");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrSim", instr_sim);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_MS4640A)chassisInit;

            //ChannelCount = 1;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = this.instrumentChassis.GetIdn();

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the instrument ID string and split the comma seperated fields
                string[] idn = this.instrumentChassis.GetIdn().Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            instrumentChassis.Write("*RST", this);
            System.Threading.Thread.Sleep(10);
            instrumentChassis.Write("*CLS", this);
            System.Threading.Thread.Sleep(10);

            Timeout_ms = 4000;

            ChannelCount = 1;
            System.Threading.Thread.Sleep(20);
            ActiveMeausurementChannel = 1;

            port = CalibrationPorts.PORT1;

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_MS4640A instrumentChassis;

        /// <summary>
        /// current channel indicated
        /// </summary>
        private int currentChan;

        /// <summary>
        /// Max Channel Count
        /// </summary>
        private int channelCount;

        ///// <summary>
        ///// Current SParameter test
        ///// </summary>
        //private SparameterMeasurementMode sParameter;

        /// <summary>
        /// Reference Plane Port Setting
        /// </summary>
        private CalibrationPorts port;

        /// <summary>
        /// Auto scale type
        /// </summary>
        private AutoScaleTypes autoScaleType;
        #endregion

        #region InstType_LightwaveComponentAnalyzer

		/// <summary>
        /// 
        /// </summary>
        public FlexibleCalType FlexibleCal
        {
            get{
           
                string command = ":SENSe"+currentChan.ToString()+":CORRection:COLLect:FLEXible:DEFine?";
                string str = this.instrumentChassis.Query_Unchecked(command,this);
                FlexibleCalType flexibleCalType = (FlexibleCalType)Enum.Parse(typeof(FlexibleCalType), str);
                
                return flexibleCalType;
            }
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:FLEXible:DEFine CUSTOM,{1}",
                    currentChan, Enum.GetName(typeof(FlexibleCalType), value));

                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// set/get flexible calculate status
        /// </summary>
        public bool FlexibleCalStatus
        {
            get
            {

                string command = ":SENSe" + currentChan.ToString() + ":CORRection:COLLect:FLEXible:STATe?";
                string str = this.instrumentChassis.Query_Unchecked(command,this);
                if (str == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:FLEXible:STATe", currentChan);
                if (value)
                {
                    command += " ON";
                }
                else
                {
                    command += " OFF";
                }

                this.instrumentChassis.Write(command, this);
            }
        }

		
        /// <summary>
        /// set/get which channel is active for measurement
        /// </summary>
        public override int ActiveMeausurementChannel
        {
            get
            {
                string command = ":DISPlay:WINDow:ACTivate?";
                currentChan = ReadIntegerValueOut(command);
                return currentChan;
            }
            set
            {
                currentChan = value;
                if (channelCount >= currentChan)
                {
                    string command = string.Format(":DISPlay:WINDow{0}:ACTivate", currentChan);
                    this.instrumentChassis.Write(command, this);
                }
                else
                {
                    throw new InstrumentException(string.Format("Invalid active channel {0} setting,current max channel is {1}", currentChan, channelCount));
                }

            }
        }

        /// <summary>
        /// Set/get what data are displayed
        /// Just Implement part of display mode, due to the enum list is not same as Ag8703
        /// </summary>
        public override void SetDataDisplayMode(DataDisplayMode dataDisplayMode)
        {
            string command = string.Format(":CALCulate{0}:MATH: ", currentChan);
            string subCmd = "";
            switch (dataDisplayMode)
            {
                case DataDisplayMode.DataOnly:
                    subCmd = "Disp data";
                    break;
                case DataDisplayMode.DataAndMemory:
                    subCmd = "Disp dtm";
                    break;
                case DataDisplayMode.DataDividedByMemory:
                    subCmd = "Func Div";
                    break;
                case DataDisplayMode.DataMinusMemory:
                    subCmd = "Func Sub";
                    break;
                case DataDisplayMode.MemoryOnly:
                    subCmd = "Disp mem";
                    break;
                case DataDisplayMode.DataPlusMemory:
                    subCmd = "Func Add";
                    break;
                case DataDisplayMode.DataTimesMemory:
                    break;
                case DataDisplayMode.Memory1DividedByMemory2:
                    break;
                case DataDisplayMode.Memory1MinusByMemory2:
                    break;
                case DataDisplayMode.Memory1PlusMemory2:
                    break;
                case DataDisplayMode.Memory1timesMemory2:
                    break;
                case DataDisplayMode.Memory2DividedByMemory1:
                    break;
                case DataDisplayMode.Memory2MinuseMemory1:
                    break;
                case DataDisplayMode.MathResults:
                    subCmd = "Disp dmm";
                    break;
                case DataDisplayMode.MemoryMinusData:
                    break;
                case DataDisplayMode.MemoryDividedByData:
                    break;
                default:
                    break;
            }
            if (string.IsNullOrEmpty(subCmd))
            {
                throw new InstrumentException("Invalid Display mode!");
            }
            else
            {
                string commandSet = command + subCmd;
                this.instrumentChassis.Write(commandSet, this);
            }
        }

        /// <summary>
        /// Stores the data trace in channel memory 
        /// </summary>
        public override void StoreTraceDataInChannelMemory()
        {
            string command = string.Format(":Calc{0}:Selected:Math:Mem", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Set/get active marker 
        /// </summary>
        public override Marker ActiveMarker
        {
            get
            {
                string command = "MRX?";
                string resp = this.instrumentChassis.Query_Unchecked(command, this);
                return (Marker)(int.Parse(resp.Trim()));
            }
            set
            {
                // marker off command ":CALC1:MARK:OFF"
                if (value != Marker.NoMarker)
                {
                    string command = string.Format("MR{0}", (int)value);
                    this.instrumentChassis.Write(command, this);
                }
                else
                {
                    throw new InstrumentException("Invalid Active Marker!");
                }

            }
        }

        /// <summary>
        /// Set/get the reference position 
        /// </summary>
        public override double ReferencePosition
        {
            get
            {
                //REFP
                string command = "REF?";
                return ReadDoubleValueOut(command);
            }
            set
            {
                if (value - Convert.ToInt32(value) != 0)
                {
                    throw new InstrumentException("Instrument only could accept the integer value!");
                }
                else
                {
                    string command = string.Format("REF {0}", value);
                    this.instrumentChassis.Write(command, this);
                }
            }
        }

        /// <summary>
        /// Set/get the output power level 
        /// </summary>
        public override double SourceOutputPower_dBm
        {
            get
            {
                //:Source{1~4}:EXT:POW?
                string command = string.Format(":Source{0}:EXT:POW?", currentChan);
                return ReadDoubleValueOut(command);

            }
            set
            {
                //:Source{1~4}:EXT:POW
                if (value >= -15 && value <= 30)
                {
                    string command = string.Format(":Source{0}:EXT:POW {1}", currentChan, value);
                    this.instrumentChassis.Write(command, this);
                }
                else
                {
                    throw new InstrumentException("Out of external Power Range!");
                }
            }
        }

        /// <summary>
        /// Set/get the IF bandwidth
        /// Note Unit, current Hz
        /// only could accept the 1, 3, 10, 30, 100, 300 Hz; 1, 3, 10, 30, 100, 300 kHz; and 1 MHz
        /// </summary>
        public override double ForwardCurrentBandwidth
        {
            get
            {
                string command = string.Format(":SENSe{0}:BANDwidth?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return Convert.ToDouble(res.Trim());

            }
            set
            {
                // only could accept the 1, 3, 10, 30, 100, 300 Hz; 1, 3, 10, 30, 100, 300 kHz; and 1 MHz
                int count = 1;
                int multiply = Convert.ToInt32(value);
                while (multiply % 10 == 0)
                {
                    multiply = Convert.ToInt32(multiply) / 10;
                    count++;
                }

                if ((multiply != 1 && multiply != 3) || count > 40)
                {
                    throw new InstrumentException("Invalid IF Bandwidth!");
                }

                string command = string.Format(":SENSe{0}:BANDwidth {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get the sweep time 
        /// </summary>
        public override double SweepTime
        {
            get
            {
                //:SENSe{1-16}:SWEep:TIMe?
                string command = string.Format(":SENSe{0}:SWEep:TIMe?", currentChan);
                return ReadDoubleValueOut(command);
            }
            set
            {
                //:SENSe{1-16}:SWEep:TIMe
                string command = string.Format(":SENSe{0}:SWEep:TIMe {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get the reference line value 
        /// </summary>
        public override double ReferenceLineValue
        {
            get
            {
                //REF?//REF2?
                string command = "OFF?";
                return ReadDoubleValueOut(command);
            }
            set
            {
                this.instrumentChassis.Write("OFF " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Set the reference value to that of the active marker�s amplitude 
        /// </summary>
        public override void SetReferenceValueToActiveMarker()
        {
            //:CALCulate{1-16}[:SELected]:MARKer:MOVe:REFMarker
            string command = string.Format(":CALCulate{0}:MARKer:MOVe:REFMarker", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Preset the analyzer to the factory preset state 
        /// </summary>
        public override void Preset()
        {
            string command = ":SYSTem:PRESet";
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Sets the trace scale factor 
        /// </summary>
        public override double Scale
        {
            get
            {
                //SCL?
                string command = "SCL?";
                return ReadDoubleValueOut(command);
            }
            set
            {
                //SCL
                // range is 0,10,20,30,-3
                if (value == 0 || value == 10 || value == 20 || value == 30 || value == -3)
                {
                    string command = string.Format("SCL {0}", value);
                    this.instrumentChassis.Write(command, this);
                }
                else
                {
                    throw new InstrumentException("The Scale value is out of range!");
                }
            }
        }

        /// <summary>
        /// Set/get the number of points in the sweep, or in a sweep segment 
        /// </summary>
        public override uint NumberOfSweepPoint
        {
            get
            {
                string command = string.Format(":SENSe{0}:SWEep:POINts?", currentChan);
                int res = ReadIntegerValueOut(command);
                return (uint)res;
            }
            set
            {
                string command = ":SENSE:SWEEP:POINTS " + value.ToString();
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get smoothing state
        /// </summary>
        public override bool SmoothingEnable
        {
            get
            {
                string command = string.Format(":CALCulate{0}:SMOothing?", currentChan);
                return ReadBooleanValueOut(command);
            }
            set
            {
                string command = string.Format(":CALCulate{0}:SMOothing {1}", currentChan, Convert.ToInt32(value));
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get the smoothing aperture as a percent of the trace. 
        /// </summary>
        public override double SmoothingAperturePercent
        {
            get
            {
                string command = string.Format(":CALCulate{0}:SMOothing:APERture?", currentChan);
                return ReadDoubleValueOut(command);
            }
            set
            {
                string command = string.Format(":CALCulate{0}:SMOothing:APERture {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get the averaging on and off on active channel 
        /// </summary>
        public override bool ActiveChannelAveragingEnable
        {
            get
            {
                string command = string.Format(":SENSe{0}:AVERage?", currentChan);
                return ReadBooleanValueOut(command);
            }
            set
            {
                string cmd = string.Format(":SENSe{0}:AVERage {1}", currentChan, Convert.ToInt32(value));
                this.instrumentChassis.Write(cmd, this);
            }
        }

        /// <summary>
        /// Set/get the averaging factor on the active channel 
        /// </summary>
        public override double ActiveChannelAveragingFactor
        {
            get
            {
                string cmd = "AVG?";
                return ReadDoubleValueOut(cmd);
            }
            set
            {

                string cmd = "AVG " + value.ToString();
                this.instrumentChassis.Write(cmd, this);

            }
        }

        /// <summary>
        /// Recalls from save/recall registers
        /// </summary>
        /// <param name="registerNumber">register number</param>
        public override void RecallRegister(int registerNumber)
        {
            //:SENSe{1-16}:CORRection:COLLect:SAVE
            if (registerNumber < 1 || registerNumber > 8)
                throw new InstrumentException("Out of recall register range(1-8):" + registerNumber.ToString());
            string command = "RECA" + registerNumber.ToString();
            this.instrumentChassis.Write(command, this);

        }

        /// <summary>
        /// Set a specified marker active and its stimulus value 
        /// </summary>
        /// <param name="marker">marker</param>
        /// <param name="value">stimulus value</param>
        [Obsolete("the value set it not same as read out", true)]
        public override void SetSpecifiedMarkerStimulus_GHz(Marker marker, double value)
        {
            double valueSet = value * 1E+9;
            string command = string.Format("MK{0} {1}", (int)marker, valueSet.ToString("0E+0"));

            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// double ReadSpecifiedMarkerStinulus_GHz(Marker)
        /// </summary>
        /// <param name="marker"></param>
        /// <returns></returns>
        [Obsolete("the value set it not same as read out", true)]
        public override double ReadSpecifiedMarkerStimulus_GHz(Marker marker)
        {
            if (marker != Marker.NoMarker)
            {
                string command = "MK" + (int)marker + "?";
                double valueRead = ReadDoubleValueOut(command);
                valueRead = valueRead * 1E-9;
                return valueRead;
            }
            else
            {
                throw new InstrumentException("Invalid Marker selected!");
            }

        }

        /// <summary>
        /// Places the fixed marker at the active marker position and makes it the delta reference
        /// </summary>
        [Obsolete("Not found related command,just according to InstType Commentes! just for reference", false)]
        public override void ZeroMarker()
        {
            // Turn delta reference mode on
            string command = "DRF";
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(5);

            // Get the active marker
            Marker activeMark = ActiveMarker;
            if (activeMark != Marker.NoMarker)
            {
                // set the active marker as delta reference marker
                command = "DR" + (int)activeMark;
                this.instrumentChassis.Write(command, this);
            }

        }

        /// <summary>
        /// set the marker to maximum
        /// </summary>
        public override void SetMarkerMaximum()
        {
            string command = string.Format(":CALCulate{0}:MARKer:SEArch {1}", currentChan, MarkerTypes.MAX.ToString());
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// set the marker to mimimum
        /// </summary>
        public override void SetMarkerMinimum()
        {
            string command = string.Format(":CALCulate{0}:MARKer:SEArch {1}", currentChan, MarkerTypes.MIN.ToString());
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Trigger instrument sweep in a specified mode 
        /// </summary>
        /// <param name="sweepTriggerMode">Sweep trigger mode</param>
        public override void TriggerSweep(SweepTriggerMode sweepTriggerMode)
        {
            //string parameter = "";
            if (sweepTriggerMode != SweepTriggerMode.Group)
            {
                string command = string.Format(":SENSe{0}:HOLD:FUNCtion {1}",
                    currentChan, sweepTriggerMode.ToString().Substring(0, 4).ToUpper());
                this.instrumentChassis.Write(command, this);
            }
            else
            {
                // According to Ag8703 Driver to implement
                double averagineFactor = ActiveChannelAveragingFactor;
                System.Threading.Thread.Sleep(5);
                int timeOut = instrumentChassis.Timeout_ms;
                System.Threading.Thread.Sleep(5);
                instrumentChassis.Timeout_ms = (int)(averagineFactor * SweepTime * 1000);
                System.Threading.Thread.Sleep(5);

                WaitForCleanSweep();
                instrumentChassis.Timeout_ms = timeOut;
            }

        }

        /// <summary>
        /// Set/get start frequency 
        /// </summary>
        public override double StartFrequency_GHz
        {
            get
            {
                string command = string.Format(":SENSe{0}:FREQuency:STARt?", currentChan);
                double startFreq = ReadDoubleValueOut(command);
                startFreq = startFreq * 1E-9;
                return startFreq;
            }
            set
            {
                double valueSet = value * 1E9;
                if (valueSet < 1E+7)
                {
                    throw new InstrumentException("The Start frequency is out of range, and the min start frequency is 10MHz");
                }
                string command = string.Format(":SENSe{0}:FREQuency:STARt {1}", currentChan, valueSet);
                this.instrumentChassis.Write(command, this);

            }
        }

        /// <summary>
        /// Set/get stop frequence 
        /// </summary>
        public override double StopFrequency_GHz
        {
            get
            {
                string command = string.Format(":SENSe{0}:FREQuency:STop?", currentChan);
                double res = ReadDoubleValueOut(command);
                res = res * 1E-9;
                return res;
            }
            set
            {
                double valueSet = value * 1E9;
                if (valueSet < 1E+7)
                {
                    throw new InstrumentException("The Start frequency is out of range, and the min stop frequency is 10MHz");
                }
                string command = string.Format(":SENSe{0}:FREQuency:STop {1}", currentChan, valueSet);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get s-parameter 
        /// </summary>
        public override SparameterMeasurementMode SparameterMeasurementMode
        {
            get
            {
                string command = string.Format(":CALCulate{0}:PARameter{0}:DEFine?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return (SparameterMeasurementMode)Enum.Parse(typeof(SparameterMeasurementMode), res.Substring(0, 3));
            }
            set
            {
                string command = string.Format(":CALCulate{0}:PARameter{0}:DEFine {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get display format 
        /// </summary>
        [Obsolete("Advice use the TraceFormat method, due to the enum list is not same as Ag8703", true)]
        public override DisplayFormat DisplayFormat
        {
            get
            {
                //:CALCulate{1-16}[:SELected]:FORMat?
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                //:CALCulate{1-16}[:SELected]:FORMat?
                throw new InstrumentException("The method or operation is not implemented.");

            }
        }

        /// <summary>
        /// Sets electrical length so group delay is zero at the active marker's stimulus.
        /// </summary>
        public override void SetMarkerDelay()
        {
            string command = string.Format(":CALCulate{0}:GDELay:APERture {1}", currentChan, 0);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Set/get the electrical delay offset in second 
        /// </summary>
        public override double ElectricalDelayOffset_s
        {
            get
            {
                string command = string.Format(":SENSe{0}:SWEep:DELay?", currentChan);
                return ReadDoubleValueOut(command);
            }
            set
            {
                string command = string.Format(":SENSe{0}:SWEep:DELay {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set/get the phase offset. -360-360 degrees, default set to Port 1
        /// </summary>
        public override double PhaseOffset
        {
            get
            {
                string command = string.Format(":CALCulate{0}:REFerence:EXTension:PORT1:PHAse?", currentChan);
                return ReadDoubleValueOut(command);
            }
            set
            {
                string command = string.Format(":CALCulate{0}:REFerence:EXTension:PORT1:PHAse {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Measurement restart
        /// this function will clear measurement and restart the measurement
        /// </summary>
        public override void MeasurementRestart()
        {
            WaitForCleanSweep();
        }

        /// <summary>
        /// Waits for all buffered instructions to complete before returning to the caller 
        /// this function will clear measurement and restart the measurement
        /// </summary>
        public override void WaitForCleanSweep()
        {
            string command = string.Format(":SENSe{0}:AVERage:CLEar", currentChan);
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(2000);
        }

        public override void WaitForCleanSweepFast()
        {
            string command = string.Format(":SENSe{0}:AVERage:CLEar", currentChan);
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(2000);
        }

        /// <summary>
        /// Auto scale the active channel 
        /// </summary>
        public override void AutoScale()
        {
            string command = string.Format(":DISPlay:WINDow{0}:Y:AUTO", currentChan);

            this.instrumentChassis.Write(command, this);
        }


        /// <summary>
        /// Sets the centre stimulus value to that of the active marker's stimulus value. 
        /// </summary>
        public override void SetMarkerToCentre()
        {

            string command = string.Format(":CALCulate{0}:MARKer:MOVe:CENTer", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Sets the start stimulus to that of the active marker�s. 
        /// </summary>
        public override void SetMarkerToStart()
        {
            string command = string.Format(":CALCulate{0}:MARKer:MOVe:STARt", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// gets the trace data in a specified format 
        /// </summary>
        /// <param name="outputFormat"></param>
        /// <returns></returns>
        [Obsolete("Due to the enum not same as base class, so advice use the another overload function!", true)]
        public override string GetSpecifiedFormatTraceData(OutputFormat outputFormat)
        {
            //:FORMat:DATA?
            //XML format
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// gets the trace data from the instrument
        /// </summary>
        /// <returns></returns>
        public override Trace GetTraceData()
        {
            return GetTraceData(PointType.Three_Dimensional);
        }

        /// <summary>
        /// Gets the trace data from the instrument
        /// </summary>
        /// <param name="pointType">2D or 3D</param>
        /// <returns>Points list</returns>
        public override Trace GetTraceData(PointType pointType)
        {


            // This is a huge Kludge to get the sweep to complete prior to transfer
            // This sets the analyser to single sweep mode, triggers a sweep, transfers the data and then set VNA vack to continuous mode
            // See Dave Smith           Date Added 24-Jan-2018

            this.instrumentChassis.Write(":SENS:HOLD:FUNC HOLD", this);
            System.Threading.Thread.Sleep(100);
            this.instrumentChassis.Write(":TRIG:SING",this);
            System.Threading.Thread.Sleep(3000);
            this.instrumentChassis.Write(":STAT:OPER:COND?", this);
            string RetValue =  this.instrumentChassis.Read_Unchecked(this);
            // we have now finished sweep and VNA is in HOLD mode so get the data

            string command = ":FORMat:DATA " + DataFormats.ASC.ToString();
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(100);

            command = string.Format(":CALCulate1:TDATA:SDATa?", currentChan);
            StringBuilder res = new StringBuilder();
            //  get detail of failure 
            try
            {
                res = ReadLargeStrDataOut(command);
            }
            catch (Exception e)
            {
                throw new Exception(" read VNA output error " + e.Message);
            }

              Trace trace = new Trace();
              try
              {
                  trace = AnalysisTraceData(res);
              }
              catch (Exception e)
              {
                  throw new Exception(" AnalysisTraceData  error " + e.Message);
              }
              this.instrumentChassis.Write(":SENS:HOLD:FUNC CONT",this);        // Set to continuous sweep mode
            return trace;

        }

        public override Trace GetTraceDataFAST(PointType pointType)
        {
            string command = ":FORMat:DATA " + DataFormats.ASC.ToString();
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(100);
            command = string.Format(":CALCulate1:TDATA:SDATa?", currentChan);
            StringBuilder res = new StringBuilder();
            //  get detail of failure 
            try
            {
                res = ReadLargeStrDataOutFAST(command);
            }
            catch (Exception e)
            {
                throw new Exception(" read VNA output error " + e.Message);
            }

              Trace trace = new Trace();
              try
              {
                  trace = AnalysisTraceData(res);
              }
              catch (Exception e)
              {
                  throw new Exception(" AnalysisTraceData  error " + e.Message);
              }

            return trace;

        }

        
        #endregion


        #region MS4640A Function

        /// <summary>
        /// Sets or gets the number of displayed channels.
        /// The Channel Count just be 1,2,3,4,6,8,9,10,12 and 16 for 25,000 point mode.
        /// The Channel Count just be 1 at 100,000 points mode
        /// </summary>
        public int ChannelCount
        {
            get
            {
                string command = ":DISPlay:COUNt?";
                channelCount = ReadIntegerValueOut(command);
                return channelCount;
            }
            set
            {

                if (value == 1 || value == 2 || value == 3 || value == 4 || value == 6
                    || value == 8 || value == 9 || value == 10 || value == 12 || value == 16)
                {
                    string command = ":DISPlay:COUNt " + value.ToString();
                    this.instrumentChassis.Write(command, this);
                    channelCount = value;
                }
                else
                {
                    throw new InstrumentException("Invalid Channel Count setting!");
                }

            }
        }

        /// <summary>
        /// Get or Set the Trace Format
        /// </summary>
        public TraceFormats TraceFormat
        {
            get
            {
                string command = string.Format(":CALCulate{0}:FORMat?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                TraceFormats format = (TraceFormats)Enum.Parse(typeof(TraceFormats), res.Trim());
                return format;
            }
            set
            {
                string command = string.Format(":CALCulate{0}:FORMat {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Get or Set Sweep Type
        /// </summary>
        public SweepTypes SweepType
        {
            get
            {
                string command = string.Format(":SENSe{0}:SWEep:TYPe?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                SweepTypes rtnValue = (SweepTypes)Enum.Parse(typeof(SweepTypes), res.Trim());
                return rtnValue;
            }
            set
            {
                string command = string.Format(":SENSe{0}:SWEep:TYPe {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Output the network embedding/deembedding function on/off state on the indicated channel.
        /// </summary>
        public bool EmbedOrDeEmbedEnable
        {
            get
            {
                //:CALCulate{1-16}:FSIMulator:NETWork[:STATe]?
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork?",currentChan);
                int value = ReadIntegerValueOut(command);
                if (value == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            set
            {
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork {1}", currentChan,Convert.ToInt16(value));
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Calibration Port
        /// </summary>
        public CalibrationPorts CalibrationPort
        {
            get
            {
                string command = string.Format(":SENS{0}:CORR:COLL:PORT?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                CalibrationPorts port = CalibrationPorts.PORT12;
                if (res == "PORT1,PORT2")
                {
                    port = CalibrationPorts.PORT12;
                }
                else
                {
                    port = (CalibrationPorts)Enum.Parse(typeof(CalibrationPorts), res.Trim());

                }
                //:SENS1:CORR:COLL:PORT PORT2 | PORT1 | PORT12
                return port;
            }
            set
            {
                string command = string.Format(":SENS{0}:CORR:COLL:PORT {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Get or Set the Auto Scale Type
        /// </summary>
        public AutoScaleTypes AutoScaleType
        {
            set
            {
                autoScaleType = value;
            }
            get
            {
                return autoScaleType;
            }
        }

        /// <summary>
        /// Auto Scale for specific auto scale type
        /// </summary>
        /// <param name="autoScaleType">auto scale type</param>
        public void AutoScale(AutoScaleTypes autoScaleType)
        {
            string command = "";
            switch (autoScaleType)
            {
                case AutoScaleTypes.UnKnown:
                    throw new InstrumentException("Invalid Auto Scale Type Selected!");
                    //break;
                case AutoScaleTypes.ActiveTrace:
                    throw new InstrumentException("Please Use Another AutoScale(int activeTrace) instead!");
                    //break;
                case AutoScaleTypes.ActiveChannel:
                    command = string.Format(":DISPlay:WINDow{0}:Y:AUTO", currentChan);
                    break;
                case AutoScaleTypes.ActiveAllChannels:
                    command = ":DISPlay:Y:AUTO";
                    break;
                default:
                    throw new InstrumentException("Invalid Auto Scale Type!");
                    //break;
            }
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Auto Scale for specific trace
        /// </summary>
        /// <param name="activeTrace">Active Trace</param>
        public void AutoScale(int activeTrace)
        {
            string command = string.Format(":DISPlay:WINDow{0}:TRACe{1}:Y:AUTO", currentChan, activeTrace);

            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Analysis Trace data
        /// </summary>
        /// <param name="dataStr">data from instrument just for XML format</param>
        /// <returns>points list</returns>
        public Trace AnalysisTraceData(StringBuilder dataStr)
        {
            Trace trace = new Trace();
            string recordinfomation = "";  




            try
            {
                string dataVNA = dataStr.ToString().Replace("\r\n" , "");
                dataVNA.Replace(" ", "");
                string[] ggg = new string[1];
                ggg[0] = "Imag-->";

                string[]  resultVNA = dataVNA.Split(ggg,StringSplitOptions.RemoveEmptyEntries);

                string middle = resultVNA[1];

                ggg[0] = "<P";


                string[] rowData = middle.Split(ggg,StringSplitOptions.RemoveEmptyEntries);

                foreach (string var in rowData)
                {

                    if (var.Contains("oint") && var.IndexOf('>') > 0 && var.LastIndexOf("</Point") > 0)
                    {
                        recordinfomation = var;
                        int startIndex = var.IndexOf('>');
                        int endIndex = var.LastIndexOf("</Point");
                        string context = var.Substring(startIndex + 1, endIndex - startIndex - 1);
                        string[] data = context.Split(';');
                        trace.Add(Convert.ToDouble(data[0].Trim()),
                            Convert.ToDouble(data[1].Trim()), Convert.ToDouble(data[2].Trim()));
                    }
                }

                
            }

            catch (Exception e)
            {

                System.IO.FileStream fs = null;
                System.IO.StreamWriter sw = null;
                try
                {
                    // Create File stream
                    DateTime nowUtc = DateTime.Now.ToUniversalTime();
                    string timestamp = nowUtc.ToString("yyyyMMddHHmmssfff");

                    fs = new System.IO.FileStream("C:\\fordebugVNA" + timestamp +".txt", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite);

                    sw = new System.IO.StreamWriter(fs);
                    sw.Write(recordinfomation);
                    sw.WriteLine();
                    sw.Write(dataStr.ToString());
                }
                catch (Exception)
                {

                }
                finally
                {
                    // Close File   
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }

                }
                throw new Exception("vna OUTPUT ERROR ,PLEASE RETRY");
            }
          

            return trace;
        }

        /// <summary>
        /// Get specific format data instead of output format data
        /// </summary>
        /// <param name="outputFormat">data format</param>
        /// <returns>trace data</returns>
        public string GetSpecifiedFormatTraceData(DataFormats outputFormat)
        {
            string command = ":FORMat:DATA " + outputFormat.ToString();
            this.instrumentChassis.Write(command, this);
            System.Threading.Thread.Sleep(20);
            command = string.Format(":CALCulate1:TDATA:SDATa?", currentChan);

            return ReadLargeStrDataOut(command).ToString();
        }

        /// <summary>
        /// Read the large string data out
        /// </summary>
        /// <param name="command">command</param>
        /// <returns>response</returns>
        private StringBuilder ReadLargeStrDataOut(string command)
        {
            StringBuilder res = new StringBuilder();
            this.instrumentChassis.Write(command, this);
            //yubo  add delay because it fail to get any data 
            System.Threading.Thread.Sleep(500);

            
            bool finished = false;
            while (!finished)
            {
                try
                {
                    System.Threading.Thread.Sleep(50);
                    res.Append(this.instrumentChassis.Read_Unchecked(this));
                    //comment it when "\r\n" may  cause error
                    //if (res[res.Length - 1] == '>')
                    //    res.Append("\r\n");

                }
                catch (Exception)
                {
                    finished = true;
                }

            }
            return res;
        }


        private StringBuilder ReadLargeStrDataOutFAST(string command)
        {
            StringBuilder res = new StringBuilder();
            this.instrumentChassis.Write(command, this);
            //yubo  add delay because it fail to get any data 
            //System.Threading.Thread.Sleep(500);


            bool finished = false;
            while (!finished)
            {  
                    //System.Threading.Thread.Sleep(50);
                    res.Append(this.instrumentChassis.Read_Unchecked(this));
                    //comment it when "\r\n" may  cause error
                    //if (res[res.Length - 1] == '>')
                    //    res.Append("\r\n");

                    if (res.ToString().Contains("</DataFile>")) finished=true;

            }
            return res;
        }

        private StringBuilder ReadLargeStrDataOut_FAST(string command)
        {
            StringBuilder res = new StringBuilder();
            this.instrumentChassis.Write(command, this);
            //yubo  add delay because it fail to get any data 
            System.Threading.Thread.Sleep(500);


            bool finished = false;
            while (!finished)
            {
                try
                {
                    System.Threading.Thread.Sleep(50);
                    res.Append(this.instrumentChassis.Read_Unchecked(this));
                    //comment it when "\r\n" may  cause error
                    //if (res[res.Length - 1] == '>')
                    //    res.Append("\r\n");

                }
                catch (Exception)
                {
                    finished = true;
                }

            }
            return res;
        }

        /// <summary>
        /// Get or Set Calibration Methods
        /// </summary>
        public CalibrationMethods CalibrationMethod
        {
            get
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:METHod?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return (CalibrationMethods)Enum.Parse(typeof(CalibrationMethods), res.Trim());
            }
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:METHod {1}", currentChan, value);
                this.instrumentChassis.Write(command,this);
            }
        }

        /// <summary>
        /// All Calibration Line types
        /// </summary>
        public CalibrationLineTypes CalibrationLineType
        {
            get
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:LINE?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                return (CalibrationLineTypes)Enum.Parse(typeof(CalibrationLineTypes), res.Trim());
            }
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:LINE {1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Execute Calibration
        /// </summary>
        /// <param name="types">Calibration Type</param>
        /// <param name="ports">Port for calibration</param>
        public void ExecuteCalibration(CalibrationStandardType types, CalibrationPorts ports)
        {
            if (types == CalibrationStandardType.OPEN
                || types == CalibrationStandardType.SHORT
                || types == CalibrationStandardType.LOAD)
            {
                string command =
                                string.Format(":SENSe{0}:CORRection:COLLect:PORT{1}:{2}",
                                currentChan, (int)ports, types.ToString());
                this.instrumentChassis.Write(command,this);
            }
            else if (types == CalibrationStandardType.ISOL || types == CalibrationStandardType.THRU)
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:PORT{1}:{2}",
                                currentChan, (int)ports, types.ToString());
                this.instrumentChassis.Write(command, this);
            }
            else
            {
                throw new InstrumentException("Invalid calibration type!");
            }
        }

        
        /// <summary>
        /// Begins the sequence for an S11 1-port calibration (ES models), 
        /// or a reflection 1-port calibration (ET models)
        /// </summary>
        public void CalibrateS11OnPort1()
        {
            string command = "CALIS111";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Begins the sequence for an S22 1-port
        /// calibration.
        /// </summary>
        public void CalibrateS22OnPort1()
        {
            string command = "CALIS221";
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// Set or get calibration status
        /// </summary>
        public bool CalibrationStatus
        {
            get
            {
                //:SENSe{1-16}:CORRection:STATe?
                string command = string.Format(":SENSe{0}:CORRection:STATe?", currentChan); ;
                this.instrumentChassis.Write(command, this);
                string res = this.instrumentChassis.Read_Unchecked(this);
                if (res.Trim() == "1")
                {
                    return true;
                }
                return false;
            }
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:STATe {1}", currentChan,Convert.ToInt16(value)); ;
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Set the Power Coupled Power
        /// </summary>
        /// <param name="portNumber">port Number</param>
        /// <param name="valueSet">power value set</param>
        public void SetPowerForPowerCoupled(int portNumber, double valueSet)
        {
            string command = string.Format(":SOURce{0}:EFFective:POWer:PORT{1} {2}", currentChan, portNumber, valueSet);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Get the power from power coupled
        /// </summary>
        /// <param name="portNumber">port number</param>
        /// <returns>power value read out</returns>
        public double GetPowerFromPowerCoupled(int portNumber)
        {
            string command = string.Format(":SOURce{0}:EFFective:POWer:PORT{1}?", currentChan, portNumber);
            return ReadDoubleValueOut(command);
        }

        /// <summary>
        /// Calibration Port Type Selected
        /// </summary>
        public CalibrationPortTypes CalibrationPortType
        {
            set
            {
                string command = string.Format(":SENSe{0}:CORRection:COLLect:PORT12:FULL{1}", currentChan, (int)value);
                this.instrumentChassis.Write(command, this);
            }
        }

         /// <summary>
        /// Done calibration
        /// </summary>

        public void DoneCalibration()
        {
            string command = string.Format(":SENSe{0}:CORRection:COLLect:SAVE", currentChan);

            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Save calibration data for a single channel
        /// </summary>
        /// <param name="fileName">file name, the postfix should be end with .chx</param>
        public void SaveCalDataForSpecificChannel(string fileName)
        {
            //:MMEM:LOAD <string> |
            //:MMEM:STORe <string>
            string command = ":MMEM:STORe " + "'" + fileName + "'";
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Load calibration data from a single channel
        /// </summary>
        /// <param name="fileName">calibration file name,the postfix should be end with .chx</param>
        [Obsolete("Waitting for verify",false)]
        public void LoadCalDataFromSpecificChannel(string fileName)
        {
            string command = ":MMEM:LOAD " + fileName ;
            string res = this.instrumentChassis.Query_Unchecked(command, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void RecalSetup(string fileName)
        {
            string command = ":MMEM:LOAD " + fileName;
            this.instrumentChassis.Write_Unchecked(command, this);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void RecalEmbedingSetting(string fileName)
        {
            string command = ":MMEM:LOAD " + fileName;
            this.instrumentChassis.Write_Unchecked(command, this);
 //           command = "CALC1:FSIM:NETW:MOD EMB";
 //           this.instrumentChassis.Write_Unchecked(command, this);
 //           System.Threading.Thread.Sleep(500);
 //           command = "CALC1:FSIM:NETW:MOD DEEM";
 //           this.instrumentChassis.Write_Unchecked(command, this);

        }



        /// <summary>
        /// Clear All Networks on indicated channel
        /// </summary>
        public void DeleteNetworks()
        {
            //:CALCulate{1-16}:FSIMulator:NETWork:CLEar
            //:CALCulate{1-16}:FSIMulator:NETWork{1-50}:DELete
            string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:CLEar", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// the indicated network port number on the indicated channel.
        /// if set the value, the value is should be 1 or 2
        /// </summary>
        public int NetWorkPort
        {
            get 
            {
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork1:PORT?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Trim().ToUpper() == "PORT1")
                {
                    return 1;
                }
                else if (res.Trim().ToUpper() == "PORT2")
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
            set {
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork1:PORT PORT{1}", currentChan, value);
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// the current network embed/deembed mode on the indicated channel.
        /// </summary>
        public EmbedType NetWorksMode
        {
            get {
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:MODe?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command,this);
                EmbedType type = (EmbedType)Enum.Parse(typeof(EmbedType), res.Trim());
                return type;
            }
            set {
                string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:MODe {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Add the current network just defined on the indicated channel.
        /// </summary>
        public void AddNetWorks()
        {
            string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:ADD", currentChan);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// Set the current network type on the current channel. 
        /// </summary>
        /// <param name="type">network type</param>
        public void SetNetwortType(NetwortType type)
        {
            string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:TYPe {1}", currentChan, Enum.GetName(typeof(NetwortType), type));
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// load s21 file
        /// </summary>
        /// <param name="s2pFile">s2p file</param>
        public void LoadS2pFile(string s2pFile)
        {
            string command = string.Format(":CALCulate{0}:FSIMulator:NETWork:S2P '{1}'", currentChan, s2pFile);
            this.instrumentChassis.Write(command, this);
        }

        /// <summary>
        /// 
        /// </summary>
        public double RefPlaneExtensionInDistance
        {
            get
            {
                string command = string.Format(":CALCulate{0}:REFerence:EXTension:PORT1:DISTance?", currentChan);
                string respond = this.instrumentChassis.Query_Unchecked(command, this);


                double returnData;
                bool parseSuccess = double.TryParse(respond, out returnData);
                if (parseSuccess)
                {
                    return returnData;
                }
                else
                {
                    throw new Exception("Parse data error!");
                }
            }
            set
            {
                string command = string.Format(":CALCulate{0}:REFerence:EXTension:PORT1:DISTance {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        /// <summary>
        /// Properties of sweep mode
        /// </summary>
        public  SweepModeEnum SweepMode
        {
            get
            {
                string command = string.Format(":SENSe{0}:HOLD:FUNCtion?", currentChan);
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                SweepModeEnum type = (SweepModeEnum)Enum.Parse(typeof(SweepModeEnum), res.Trim());
                return type;
            }
            set
            {
                string command = string.Format(":SENSe{0}:HOLD:FUNCtion {1}", currentChan, value.ToString());
                this.instrumentChassis.Write(command, this);
            }
        }

        #region MS4640A Enum List

        /// <summary>
        /// Calibration Port Type
        /// </summary>
        public enum CalibrationPortTypes
        {
            /// <summary>
            /// 
            /// </summary>
            OnePort = 1,
            /// <summary>
            /// 
            /// </summary>
            TwoPort = 2
        }

        /// <summary>
        /// Calibration Standard Type list
        /// </summary>
        public enum CalibrationStandardType
        {
            /// <summary>
            /// 
            /// </summary>
            OPEN,
            /// <summary>
            /// 
            /// </summary>
            SHORT,
            /// <summary>
            /// 
            /// </summary>
            LOAD,
            /// <summary>
            /// 
            /// </summary>
            ISOL,
            /// <summary>
            /// 
            /// </summary>
            THRU
        }

        /// <summary>
        /// Embed List
        /// </summary>
        public enum EmbedType
        {
            /// <summary>
            /// 
            /// </summary>
            EMB,
            /// <summary>
            /// 
            /// </summary>
            DEEM
        }

        /// <summary>
        /// Trace format on display menu
        /// </summary>
        public enum TraceFormats
        {
            /// <summary>
            /// Group Delay
            /// </summary>
            GDEL,       
            /// <summary>
            /// Imaginary 
            /// </summary>
            IMAG, 
            /// <summary>
            /// Linear Mag and Phase
            /// </summary>
            LINPH,      
            /// <summary>
            /// Log Mag and Phase
            /// </summary>
            LOGPH, 
            /// <summary>
            /// Linear Mag
            /// </summary>
            MLIN, 
            /// <summary>
            /// Log Mag
            /// </summary>
            MLOG,  
            /// <summary>
            /// Phase
            /// </summary>
            PHAS, 
            /// <summary>
            /// Linear Polar Lin/Phase
            /// </summary>
            PLIN, 
            /// <summary>
            /// Linear Polar Real/Imag
            /// </summary>
            PLINCOMP,  
            /// <summary>
            /// Log Polar Lin/Phase
            /// </summary>
            PLOG, 
            /// <summary>
            /// Log Polar Real/Imag
            /// </summary>
            PLOGCOMP, 
            /// <summary>
            /// Power In
            /// </summary>
            PWRI,    
            /// <summary>
            ///  Power Out
            /// </summary>
            PWRO,  
            /// <summary>
            /// Real
            /// </summary>
            REAL,     
            /// <summary>
            /// Real and Imaginary
            /// </summary>
            REIM,      
            /// <summary>
            /// Smith(G+jB) Real/Imag
            /// </summary>
            SADCOMP,    
            /// <summary>
            /// Smith(G+jB) Lin/Phase
            /// </summary>
            SADLIN,    
            /// <summary>
            /// Smith(G+jB) Log/Phase
            /// </summary>
            SADLOG,     
            /// <summary>
            /// Smith(G+jB) Admitance
            /// </summary>
            SADM,      
            /// <summary>
            /// Smith(R+jX) Real/Imag
            /// </summary>
            SCOMP,     
            /// <summary>
            /// Smith(R+jX) Lin/Phase
            /// </summary>
            SLIN,      
            /// <summary>
            /// Smith(R+jX) Log/Phase
            /// </summary>
            SLOG,    
            /// <summary>
            /// Smith(R+jX) Impedance
            /// </summary>
            SMIT,      
            /// <summary>
            /// SWR
            /// </summary>
            SWR,       
            /// <summary>
            /// Impedance Real
            /// </summary>
            ZREAL,    
            /// <summary>
            /// Impedance Real  Imaginary
            /// </summary>
            ZCOMP,    
            /// <summary>
            /// Impedance Imaginary
            /// </summary>
            ZIMAG,     
            /// <summary>
            /// Impedance Magnitude
            /// </summary>
            ZMAGN    
        }

        /// <summary>
        /// Sweep Types on sweep menu
        /// </summary>
        public enum SweepTypes
        {
            /// <summary>
            /// Frequency Sweep (Linear)
            /// </summary>
            LIN,
            /// <summary>
            /// Frequency Sweep (Log)
            /// </summary>
            LOG,
            /// <summary>
            /// Segmented Sweep (Freq-based)
            /// </summary>
            FSEGM,
            /// <summary>
            /// Segmented Sweep (Index-based)
            /// </summary>
            ISEGM,
            /// <summary>
            /// Power Sweep (CW Freq)
            /// </summary>
            POW,
            /// <summary>
            /// Power Sweep (Swept Freq)
            /// </summary>
            MFGC
        }

        /// <summary>
        /// Ports
        /// </summary>
        public enum CalibrationPorts
        {
            /// <summary>
            /// 
            /// </summary>
            PORT1 = 1,
            /// <summary>
            /// 
            /// </summary>
            PORT2 = 2,
            /// <summary>
            /// 
            /// </summary>
            PORT12 = 12,
        }

        /// <summary>
        /// Auto Scale Type List
        /// </summary>
        public enum AutoScaleTypes
        {
            /// <summary>
            /// 
            /// </summary>
            UnKnown,
            /// <summary>
            /// 
            /// </summary>
            ActiveTrace,
            /// <summary>
            /// 
            /// </summary>
            ActiveChannel,
            /// <summary>
            /// 
            /// </summary>
            ActiveAllChannels
        }

        /// <summary>
        /// Data formats instead of output format
        /// </summary>
        public enum DataFormats
        {
            /// <summary>
            /// 
            /// </summary>
            ASC,
            /// <summary>
            /// 
            /// </summary>
            REAL,
            /// <summary>
            /// 
            /// </summary>
            REAL32
        }

        /// <summary>
        /// Marker type list
        /// </summary>
        public enum MarkerTypes
        {
            /// <summary>
            /// 
            /// </summary>
            MAX,
            /// <summary>
            /// 
            /// </summary>
            MIN,
            /// <summary>
            /// 
            /// </summary>
            PEAK,
            /// <summary>
            /// 
            /// </summary>
            TARGet
        }

        /// <summary>
        /// All Calibration Methods
        /// </summary>
        public enum CalibrationMethods
        {
            /// <summary>
            /// 
            /// </summary>
            AUTO,
            /// <summary>
            /// 
            /// </summary>
            ACLI,      // Auto Calibration
            /// <summary>
            /// 
            /// </summary>
            LRL,       // Manual Calibration
            /// <summary>
            /// 
            /// </summary>
            LRM,
            /// <summary>
            /// 
            /// </summary>
            SOLR,
            /// <summary>
            /// 
            /// </summary>
            SOLT,
            /// <summary>
            /// 
            /// </summary>
            SSLT,
            /// <summary>
            /// 
            /// </summary>
            SSST
        }

        /// <summary>
        /// All Calibration Line Types
        /// </summary>
        public enum CalibrationLineTypes
        {
            /// <summary>
            /// 
            /// </summary>
            COAX,
            /// <summary>
            /// 
            /// </summary>
            MICRO,
            /// <summary>
            /// 
            /// </summary>
            NONDIS,
            /// <summary>
            /// 
            /// </summary>
            WAVE
        }
		
        /// <summary>
        /// networt type
        /// </summary>
        public enum NetwortType
        { 
            /// <summary>
            /// 
            /// </summary>
            LSCP,
            /// <summary>
            /// 
            /// </summary>
            CSLP,
            /// <summary>
            /// 
            /// </summary>
            CPLS,
            /// <summary>
            /// 
            /// </summary>
            LPCS,
            /// <summary>
            /// 
            /// </summary>
            RS,
            /// <summary>
            /// 
            /// </summary>
            RP,
            /// <summary>
            /// 
            /// </summary>
            TLine,
            /// <summary>
            /// 
            /// </summary>
            S2Pfile
        }

        /// <summary>
        /// Enum of sweep mode
        /// </summary>
        public enum SweepModeEnum

        {
            /// <summary>
            /// set sweep into continuous mode
            /// </summary>
            CONT,
            /// <summary>
            /// set sweep into hold mode,stop sweep
            /// </summary>
            HOLD,
            /// <summary>
            /// set sweep into single mode
            /// </summary>
            SING
        }

        #endregion

        #endregion
        // Dave Smith - Update 1 start

        public override void SetCWSweepMode(double cwFrequency, int numPts)
        {
            string Command = string.Format(":SENS{0}:SWE:CW ON", currentChan);
            this.instrumentChassis.Write(Command, this);
            System.Threading.Thread.Sleep(100);
            if (numPts > 0)
            {

                Command = string.Format(":SENS{0}:SWE:CW:POIN {1}", currentChan, numPts);
                this.instrumentChassis.Write(Command, this);
                System.Threading.Thread.Sleep(100);
            }
            Command = string.Format(":SENS{0}:FREQ:CW {1}", currentChan, cwFrequency);
            this.instrumentChassis.Write(Command, this);
            System.Threading.Thread.Sleep(100);

        }

        public override void ReadActiveMarker(ref double mag, ref double phase)
        {
            char[] SplitChars = {','};
            string Command = string.Format(":CALC{0}:PAR1:MARK:ACT?", currentChan);
            string RetValue = this.instrumentChassis.Query_Unchecked(Command, this);

            int MarkerNum = Convert.ToInt32(RetValue);

            Command = string.Format(":CALC{0}:PAR1:MARK{1}:Y?", currentChan, MarkerNum);
            RetValue = this.instrumentChassis.Query_Unchecked(Command, this);
            string[] SplitVals = RetValue.Split(SplitChars);
            mag = Convert.ToDouble(SplitVals[0]);
        //    phase = Convert.ToDouble(SplitVals[1]);
            phase = 0;
        }

        // Dave Smith - Update 1 end

        #region Helper Methods

        /// <summary>
        /// Convert register value to int type
        /// </summary>
        /// <param name="command">command</param>
        /// <returns>int result</returns>
        private int ReadIntegerValueOut(string command)
        {
            WaitForCleanSweep();
            string res = this.instrumentChassis.Query_Unchecked(command, this);
            int rtnValue = 0;
            try
            {
                rtnValue = int.Parse(res.Trim());
            }
            catch (Exception)
            {
                throw new InstrumentException("Channel Count Access Exception!");
            }
            return rtnValue;
        }

        /// <summary>
        /// Convert register value to double type
        /// </summary>
        /// <param name="command">command</param>
        /// <returns>double result</returns>
        private double ReadDoubleValueOut(string command)
        {
            string res = this.instrumentChassis.Query_Unchecked(command, this);
            double rtnValue = 0;
            try
            {
                rtnValue = double.Parse(res.Trim());
            }
            catch (Exception ex)
            {
                throw new InstrumentException("Invalid data format for data convert!\n" + ex.Message);
            }
            return rtnValue;
        }

        /// <summary>
        /// Convert register value to boolean type
        /// </summary>
        /// <param name="command">command</param>
        /// <returns>boolean result</returns>
        private bool ReadBooleanValueOut(string command)
        {
            string res = this.instrumentChassis.Query_Unchecked(command, this);
            if (res.Trim() == "1")
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}

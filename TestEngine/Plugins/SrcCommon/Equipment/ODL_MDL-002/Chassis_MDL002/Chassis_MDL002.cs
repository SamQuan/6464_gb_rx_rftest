﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    public class Chassis_MDL002 : ChassisType_Serial
    {
        
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_MDL002(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisMDL002 = new ChassisDataRecord(
                "MDL-200",			// hardware name 
                "0",			// minimum valid firmware version 
                "4.12");		// maximum valid firmware version 
            ValidHardwareData.Add("chassisMDL002", chassisMDL002);

            string[] resByComma = resourceString.Split(',');

            if (resByComma[0].Contains("COM"))
            {
                if ((resByComma.Length < 1) && (resByComma.Length > 2))
                    throw new ChassisException(
                        "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");

                // Setup RS232 variables
                this.BaudRate = Convert.ToInt32((resByComma.Length == 2) ? resByComma[1] : "9600");  // 9600;

                
                this.DataBits = 8;
                this.StopBits = System.IO.Ports.StopBits.One;
                this.Handshaking = System.IO.Ports.Handshake.None;
                this.Parity = System.IO.Ports.Parity.None;
                this.InputBufferSize_bytes = 1024;
                this.OutputBufferSize_bytes = 1024;
                this.Timeout_ms = 120000;
                this.NewLine = "\r\n";

            }
            else
                throw new ChassisException(
                    "Bad resource string. Use \"COMn\" or \"COMn,baud\". eg \"COM1,9600\"");
        }
        #endregion
        public override string FirmwareVersion
        {
            get { return "0"; }
        }

        public override string HardwareIdentity
        {
            get { return "MDL-200"; }
        }

        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value)
                    this.Configure(BaudRate, DataBits, StopBits,
                                                Parity, Handshaking, InputBufferSize_bytes, OutputBufferSize_bytes);
                base.IsOnline = value;

            }
        }
    }
}

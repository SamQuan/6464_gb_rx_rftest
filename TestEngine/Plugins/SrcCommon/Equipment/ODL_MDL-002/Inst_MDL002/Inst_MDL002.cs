﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    public class Inst_MDL002 : InstType_ODL
    {
        #region Instance data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_MDL002 instChassis;
        #endregion
        double delay_value = 0;
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_MDL002(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instMDL002 = new InstrumentDataRecord(
                "MDL-002",				// hardware name 
                "0",  			// minimum valid firmware version 
                "4.12");			// maximum valid firmware version 
            ValidHardwareData.Add("instMDL002", instMDL002);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_MDL002",								// chassis driver name  
                "0",									// minimum valid chassis driver version  
                "4");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_MDL002", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instChassis = (Chassis_MDL002) chassisInit;
        }
        #endregion
        private string WaitForResponse()
        {
            string respond = "";
            while (respond.Length < 2)
            {
                respond += this.instChassis.Read(this);
            }

            //respond = respond.Substring(0, respond.Length - 4);

            return respond.ToUpper();
        }

        public override double DelayMin_ps
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override double Delay_ps
        {
            get
            {
               // throw new NotImplementedException();
                return delay_value ;
            }
            set
            {
                this.instChassis.Write(string.Format("_ABS_{0:##0.000}$",value),this);
                delay_value = value;
                string ReturnValue = WaitForResponse();

                if (ReturnValue == "NO") throw new Exception("Failed to move to the required position");
            }
        }

        public override void Reset()
        {
            this.instChassis.Write("_STP_$",this);           // Stop Motor
            string RetValue = WaitForResponse();
            //if (RetValue != "OK") throw new Exception("Invalid response string after Stop command");

            this.instChassis.Write("_ORG_$",this);
            //System.Threading.Thread.Sleep(15000);
            RetValue = WaitForResponse();
            if (RetValue != "OK") throw new Exception("Invalid response string after Reset command");
            this.instChassis.Write("_PSU_$", this);
            RetValue = WaitForResponse();
            if (RetValue != "OK") throw new Exception("Invalid response string after SetUnits command");
        }

        public override InstType_ODL.OperatingStatuses Status
        {
            get { throw new NotImplementedException(); }
        }

        public override int Step
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string FirmwareVersion
        {
            get { return "0"; }
        }

        public override string HardwareIdentity
        {
            get { return "MDL-002"; }
        }

        public override void SetDefaultState()
        {
            // Do nothing as we can't read anything back. 
            //throw new NotImplementedException();
            this.instChassis.Write("_STP_$", this);             // Stop Motor
            string RetValue = WaitForResponse();
            //if (RetValue != "OK") throw new Exception("Invalid response string after Stop command");

            this.instChassis.Write("_ORG_$", this);             // Go to origin 0.000mm
            //System.Threading.Thread.Sleep(15000);
            RetValue = WaitForResponse();
            if (RetValue != "OK") throw new Exception("Invalid response string after Reset to origin command");
            this.instChassis.Write("_PSU_$", this);             // Set units to ps
            RetValue = WaitForResponse();
            if (RetValue != "OK") throw new Exception("Invalid response string after SetUnits command");
            delay_value = 0;
        }

        public override int BaudRate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void SetDefaultStatus()
        {
            throw new NotImplementedException();
        }
    }
}

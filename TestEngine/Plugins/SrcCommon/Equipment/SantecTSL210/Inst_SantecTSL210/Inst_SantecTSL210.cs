// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SantecTSL210.cs
//
// Author: cypress.chen, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// 
    /// </summary>
    public class Inst_SantecTSL210:Bookham.TestLibrary.InstrTypes.InstType_TunableLaserSource
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SantecTSL210(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SantecTSL210",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MyChassis", chassisInfo);
       
            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_SantecTSL210) chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Hardware_Unknown";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_SantecTSL210 instrumentChassis;
        #endregion

        #region Tunable Laser Source implement
        /// <summary>
        /// Sets/returns laser power output status
        /// </summary>
        public override bool BeamEnable
        {
            get
            {
                string command="SU";
                string result = instrumentChassis.Query_Unchecked(command, this);
                if (result.Substring(0, 1) == "-")
                    return true;
                else
                    return false;
            }
            set
            {
                string command = null;
                int tryCount = 30;
                int waitTime_ms = 500;
                int i=0;
                if (value == true)
                {
                    command = "LO";
                }
                else
                {
                    command = "LF";
                }
                instrumentChassis.Write_Unchecked(command, this);

                //wait for completion
                while (BeamEnable == false)
                {
                    i++;
                    System.Threading.Thread.Sleep(waitTime_ms);
                    if (i > tryCount)
                    {
                        throw new InstrumentException("can't turn off the Santec TSL power!");
                    }
                }

                


            }
        }

        /// <summary>
        /// no implementation
        /// </summary>
        public override double MaximumOuputPower
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// no implementation
        /// </summary>
        public override double MaximumWavelengthINnm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// no implementation
        /// </summary>
        public override double MinimumOutputPower
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// no implementation
        /// </summary>
        public override double MinimumWavelengthINnm
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// Reads/sets laser output power in dBm
        /// </summary>
        public override double Power
        {
            get
            {
                string command="OP";
                string result = instrumentChassis.Query_Unchecked(command, this);
                try
                {
                    double power = double.Parse(result);
                    return power;
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Wrong Returned Power value:" + result,ex);
                }

            }
            set
            {
                string command = "OP" + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Bookham.TestLibrary.InstrTypes.InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Reads/sets present laser output wavelength in nm
        /// </summary>
        public override double WavelengthINnm
        {
            get
            {
                string command = "WA";
                string result = instrumentChassis.Query_Unchecked(command, this);
                try
                {
                    double wl = double.Parse(result);
                    return wl;
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Wrong Returned Wavelength:" + result,ex);
                }
            }
            set
            {
                string command = string.Format("WA{0}", value);
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        #endregion

        #region other funtions
        /// <summary>
        /// Reads/sets laser output power in mW
        /// </summary>
        public double Power_mW
        {
            get
            {
                string command = "LP";
                string result = instrumentChassis.Query_Unchecked(command, this);
                try
                {
                    double power = double.Parse(result);
                    return power;
                }
                catch (Exception ex)
                {

                    throw new InstrumentException("Wrong Returned Power value:" + result,ex);
                }

            }
            set
            {
                string command = "LP" + value.ToString();
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        #endregion
    }
}

// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Mp1764c.cs
//
// Author: mark.fullalove, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestSolution.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    public class Inst_Mp1764c : Instrument, IInstType_BertErrorAnalyser
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Mp1764c(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "ANRITSU,MP1762C",		// hardware name 
                "0",			        // minimum valid firmware version 
                "1.0");		            // maximum valid firmware version 
            ValidHardwareData.Add("MP1764C", instrVariant1);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Mp1764c",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("MP1764C", chassisInfo);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Mp1764c)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", this).Split(',');

                // e.g. ANRITSU,MP1762C,0,1.0
                return idn[3];
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // e.g. ANRITSU,MP1762C,0,1.0
                // "MP1762C" can be returned by model MP1764
                return idn[0] + "," + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Mp1764c instrumentChassis;
        #endregion

        #region IInstType_BertErrorAnalyser Members

        public bool AnalyserEnabled
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public void AnalyserStart()
        {
            // Measurement start
            instrumentChassis.Write_Unchecked("STA", this);
        }

        public void AnalyserStop()
        {
            // Measurement stop
            instrumentChassis.Write_Unchecked("STO", this);
        }

        public void AutoSetup()
        {
            // Auto search ON
            instrumentChassis.Write_Unchecked("SRH 1", this);
        }

        public double ErrorRatio
        {
            get 
            {
                // return 1 (= 100% errors) if in sync loss
                if (SyncLossDetected)
                    return 1;

                string rtn = instrumentChassis.Query_Unchecked("ER?", this);
                // Ignore the first character
                return Double.Parse(rtn.Substring(2, rtn.Length - 2));
            }
        }

        public double ErrorRatioOfOnesRecievedAsZeros
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double ErrorRatioOfZerosRecievedAsOnes
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public long ErroredBits
        {
            get               
            {   
                // Error count
                //
                // Response is "EC n" where "n" is the error count or "1.00000E-99" if sync loss. 
                // The synch loss response is not an integer, so return a big number of errors in this case.
                if (SyncLossDetected)
                    return long.MaxValue;

                string rtn = instrumentChassis.Query_Unchecked("EC?", this);
                return Int64.Parse(rtn.Substring(4, rtn.Length - 4));
            }
        }

        public long ErroredBitsOnesRecievedAsZeros
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public long ErroredBitsZerosRecievedAsOnes
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public double MeasPhaseDelay_ps
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public double MeasThreshold_V
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_BertDataPatternType PatternType
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public InstType_BertDataPolarity Polarity
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public int PrbsLength
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool SyncLossDetected
        {
            get
            {
                // "SLI 1" or "SLI 0"
                string rtn = instrumentChassis.Query_Unchecked("SLI?", this);
                return rtn.Substring(4, 1).Equals("1");
            }
        }

        public bool[] UserPattern
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public int UserPatternLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion
    }
}

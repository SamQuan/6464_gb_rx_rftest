// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/Request.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;
using System.IO;
using System.Globalization;
using Bookham.TestEngine.PluginInterfaces.MES;



namespace Bookham.TestLibrary.MES
{
    /// <summary>
    /// A request to perform a FactoryWorks operation on
    /// a single batch or component(s) therein.
    /// </summary>
    internal sealed class Request : System.IDisposable
    {

        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="lot">The batch/lot name e.g. CL12345.1</param>
        /// <param name="components">The component ID(s) e.g. CL12345.001</param>
        /// <param name="part">The component part code e.g. PTC100-152727</param>
        /// <param name="stage">Current FactoryWorks stage of the batch</param>
        /// <param name="code">Fail/rework/onhold code</param>
        /// <param name="operation">The FactoryWorks operation to perform e.g. LOTSTATE</param>
        /// <param name="data">Data associated with the FactoryWorks operation</param>
        /// <param name="level">Either LOT or COMPONENT - determines which drop folder is used</param>
        /// <param name="deleteFileOnDisposal">If true delete the request file on disposal of this object instance</param>
        internal Request(string lot, string components, string part, string stage, string code, string operation, string data, RequestLevel level, bool deleteFileOnDisposal)
        {
            this.componentDropDir = Request.DropDir;
            this.lotDropDir = Request.DropDir + "LOT";

            this.level = level;
            this.state = RequestState.FILE_NOT_SAVED;

            this.tokenList = new string[15];
            this.SetStandardFileElementList(lot, components, part, stage, code, operation, data);

            this.deleteFileOnDisposal = deleteFileOnDisposal;
        }
        internal Request(string lot, string components, string part, string stage, string code, string operation, string data, RequestLevel level, bool deleteFileOnDisposal, string userId)
        {
            this.componentDropDir = Request.DropDir;
            this.lotDropDir = Request.DropDir + "LOT";

            this.level = level;
            this.state = RequestState.FILE_NOT_SAVED;

            this.tokenList = new string[15];
            this.SetStandardFileElementList(lot, components, part, stage, code, operation, data, userId);

            this.deleteFileOnDisposal = deleteFileOnDisposal;
        }
        /// <summary>
        /// Full path of temporary local folder
        /// </summary>
        internal static string LocalDir
        {
            get
            {
                return Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "FactoryWorks");
            }
        }

        /// <summary>
        /// Full pathname of the temporary local request file
        /// </summary>
        internal string LocalFilePath
        {
            get
            {
                return Path.Combine(Request.LocalDir, this.filename);
            }
        }

        /// <summary>
        /// Full pathname of the request file in the FactoryWorks drop directory
        /// </summary>
        internal string DropFilePath
        {
            get
            {
                if (this.level == RequestLevel.COMPONENT)
                    return Path.Combine(this.componentDropDir, this.filename);
                else // LOT
                    return Path.Combine(this.lotDropDir, this.filename);
            }
        }

        /// <summary>
        /// Full pathname of the request file in its current location.
        /// This could be either the temporary local folder or the
        /// FactoryWorks drop folder.
        /// </summary>
        internal string FilePath
        {
            get
            {
                if (this.state == RequestState.FILE_NOT_SAVED)
                {
                    return "";
                }
                else if (this.state == RequestState.LOCAL)
                {
                    return this.LocalFilePath;
                }
                else // DROPPED
                {
                    return this.DropFilePath;
                }
            }
        }

        /// <summary>
        /// Create the local folder to hold request files before they
        /// are copied to the FactoryWorks drop folder.
        /// </summary>
        internal static void CreateTempLocalFolderIfNotPresent()
        {
            try
            {
                if (Directory.Exists(Request.LocalDir) == false)
                    Directory.CreateDirectory(Request.LocalDir);
            }
            catch (Exception e)
            {
                throw new MESFatalErrorException("Could not create temporary local folder '" + Request.LocalDir + "'", e);
            }
        }

        /// <summary>
        /// Create the request file on the local PC drive
        /// </summary>
        internal void CreateLocalFile()
        {
            TextWriter output = null;
            try
            {
                // Write the file
                DateTime now;
                if (FactoryWorks.UsingUtc)
                {
                    now = DateTime.UtcNow;
                }
                else
                {
                    now = DateTime.Now;
                }
                this.filename = now.ToString("yyyyMMddHHmmssff", CultureInfo.InvariantCulture) + "." + Request.Node;

                output = File.CreateText(this.LocalFilePath);

                // Concatenate the strings in tokenList into a CSV list
                output.Write(string.Join(",", this.tokenList));
                // update the state to say that the file is local
                this.state = RequestState.LOCAL;
            }
            catch (Exception e)
            {
                throw new MESFatalErrorException("Could not create local file '" + this.LocalFilePath + "'", e);
            }
            finally
            {
                if (output != null)
                    output.Close();
            }
        }

        /// <summary>
        /// Get a string representation of the command sent to FactoryWorks
        /// </summary>
        internal string GetRequestCommand()
        {
            // Concatenate the strings in tokenList into a CSV list and return it.
            return (string.Join(",", this.tokenList));
        }

        /// <summary>
        /// Move the request file to the FactoryWorks drop folder.
        /// </summary>
        internal void MoveToDropFolder()
        {
            try
            {
                // Cannot create file within 1 ms of previous due to naming convention
                // Possible to create local file, move to drop, then create another local file
                // with same name - hence folowing move to drop fails because of existing file
                // Possible, but unlikely to happen
                File.Move(this.LocalFilePath, this.DropFilePath);
                // update the state to say that the file is now dropped
                this.state = RequestState.DROPPED;
            }
            catch (Exception e)
            {
                throw new MESFatalErrorException("Could not move local file '" + this.LocalFilePath + "' to drop dir'" + this.DropFilePath + "'", e);
            }
        }

        /// <summary>
        /// Delete the request file from the relevant FactoryWorks drop folder
        /// </summary>
        internal void Remove()
        {
            try
            {
                File.Delete(this.FilePath);
            }
            catch (Exception e)
            {
                throw new MESFatalErrorException("Could not delete file '" + this.FilePath + "'", e);
            }
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Do this when the object instance is destroyed
        /// </summary>
        public void Dispose()
        {
            if (deleteFileOnDisposal && (this.state != RequestState.FILE_NOT_SAVED)
                && (File.Exists(this.FilePath)))
                this.Remove();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Prepare the file contents.
        /// Populate with information common to all requests.
        /// </summary>
        /// <param name="lot">The batch/lot name e.g. CL12345.1</param>
        /// <param name="components">The component ID e.g. CL12345.001 or list of components (format varies with operation)</param>
        /// <param name="part">The component part code e.g. PTC100-152727</param>
        /// <param name="stage">Current FactoryWorks stage of the batch</param>
        /// <param name="code">Fail/rework/onhold code</param>
        /// <param name="operation">The FactoryWorks operation to perform e.g. LOTSTATE</param>
        /// <param name="data">Data associated with the FactoryWorks operation</param>
        private void SetStandardFileElementList(string lot, string components, string part, string stage, string code, string operation, string data)
        {
            CheckStandardFileElement("lot", lot);
            CheckStandardFileElement("components", components);
            CheckStandardFileElement("part", part);
            CheckStandardFileElement("stage", stage);
            CheckStandardFileElement("code", code);
            CheckStandardFileElement("operation", operation);
            CheckStandardFileElement("data", data);

            this.tokenList[1] = "[" + lot + "]" + components;
            this.tokenList[3] = part;
            this.tokenList[5] = stage;
            this.tokenList[7] = Request.Node.ToString(CultureInfo.InvariantCulture);
            this.tokenList[9] = code;
            this.tokenList[11] = operation;
            this.tokenList[13] = "fwmes";
            this.tokenList[14] = data; // Only used for ATTRIB function
        }

        //xiaojiang
        //add user id
        private void SetStandardFileElementList(string lot, string components, string part, string stage, string code, string operation, string data, string userId)
        {
            CheckStandardFileElement("lot", lot);
            CheckStandardFileElement("components", components);
            CheckStandardFileElement("part", part);
            CheckStandardFileElement("stage", stage);
            CheckStandardFileElement("code", code);
            CheckStandardFileElement("operation", operation);
            CheckStandardFileElement("data", data);

            this.tokenList[1] = "[" + lot + "]" + components;
            this.tokenList[3] = part;
            this.tokenList[5] = stage;
            this.tokenList[7] = Request.Node.ToString(CultureInfo.InvariantCulture);
            this.tokenList[9] = code;
            this.tokenList[11] = operation;
            this.tokenList[13] = userId;
            this.tokenList[14] = data; // Only used for ATTRIB function
        }

        /// <summary>
        /// Check file element does not contain commas
        /// </summary>
        /// <param name="name">Name of file element</param>
        /// <param name="val">Value of file element</param>
        private static void CheckStandardFileElement(string name, string val)
        {
            if (val.IndexOf(',') != -1)
                throw new MESFatalErrorException("Standard file element " + name + " cannot contain commas");
        }
        #endregion

        #region Public Static Data
        /// <summary>The node number of the client making the request</summary>
        public static int Node;
        /// <summary>Full path of the folder into which the request file will be dropped</summary>
        public static string DropDir;
        #endregion

        #region Private Data
        /// <summary>Drop folder for component level requests</summary>
        private string componentDropDir;
        /// <summary>Drop folder for lot level requests</summary>
        private string lotDropDir;
        /// <summary>Drop file name</summary>
        private string filename;
        /// <summary>List of string values corresponding to the drop file contents</summary>
        private string[] tokenList;
        /// <summary>COMPONENT or LOT level</summary>
        private RequestLevel level;
        /// <summary>Is the drop file local, or has it been dropped</summary>
        private RequestState state;
        /// <summary>If true, delete the drop file on disposal of this instance</summary>
        private bool deleteFileOnDisposal;
        #endregion
    }
}

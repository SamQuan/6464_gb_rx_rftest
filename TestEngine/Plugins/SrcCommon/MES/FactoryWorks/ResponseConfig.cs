// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/ResponseConfig.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;



namespace Bookham.TestLibrary.MES
{
	/// <summary>
	/// Struct for response config
	/// </summary>
	internal struct ResponseConfig
	{
		/// <summary>If true, wait for a response to confirm operation complete and status</summary>
		internal bool WaitForResponse;
		/// <summary>Max time to wait for a response in seconds</summary>
		internal int  Timeout_s;
	}
}

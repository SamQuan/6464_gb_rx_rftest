// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/DefaultMESPlugin/DefaultMESPlugin.cs
// 
// Author: Joseph Olajubu
// Design: 

using System;
using System.Xml;
using System.Data;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.Program;

namespace Bookham.TestLibrary.MES
{
	/// <summary>
	/// Default specific MES plug-in. Provides stubs to all IMES 
    /// defined methods, for Test Software Solutions which do not 
    /// need to interface to an MES, or where the MES functionality is to 
    /// be added at a later date. Where the methods return values,
    /// dummy default values are used. 
	/// </summary>
	public sealed class DefaultMESPlugin : IMES
	{

		#region Constructors and related methods
		/// <summary>
		/// Constructor
		/// </summary>
		public DefaultMESPlugin()
		{
            // Initialise Standard attributes as returned by LOTQUERYFULL
            this.standardAttributes = new StringCollection();
            this.standardAttributes.Add("CompId");
            this.standardAttributes.Add("FacetCoatId");
            this.standardAttributes.Add("WaferId");
            this.standardAttributes.Add("ChipId");
            this.standardAttributes.Add("Position");
            this.standardAttributes.Add("PartId");
            this.standardAttributes.Add("OutputTray");
            this.standardAttributes.Add("2DBarcode");
            this.standardAttributes.Add("MesaWidth");
            this.standardAttributes.Add("SpecificId");
            this.standardAttributes.Add("FibreType");
		}

		#endregion

		#region IMES implementation

        /// <summary>
        /// Initialise method to set-up plugin object with initial values. MES ILogging.
        /// </summary>
        /// <param name="logger">interface handle to system Logging object</param>
        /// <param name="usingUtcDateTime">If true,use UTC Date Time,otherwise,use local date time.</param>
        public void Initialise(ILogging logger,bool usingUtcDateTime)
        {
            if (logger == null)
            {
                throw new Bookham.TestEngine.PluginInterfaces.MES.MESFatalErrorException("'logger' is null");
            }

            this.logger = logger;
        }

		/// <summary>
		/// Node number (uniquely identifies testset/client)
		/// </summary>
		public int Node
		{
			set
			{
				this.node = value;
			}
			get
			{
				return this.node;
			}
		}

		/// <summary>
		/// Get information about a batch/lot. This implementation returns  
        /// attributes with a set of default values.
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="getAttributes">If true, get the default set of attributes</param>
		/// <returns>The batch</returns>
		public MESbatch LoadBatch( string batchId, bool getAttributes )
		{
            if (getAttributes)
            {
                return (LOTQUERYFULL(batchId));
            }
            else
            {
                return (LOTQUERY(batchId));
            }
		}

		/// <summary>
		/// Get specific information about a batch/lot
		/// User specifies which attributes to fetch
        /// This incarnation returns the attribute name, prepended with the 
        /// string "Default".
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="getTheseBatchAttributes">Get these batch attributes specifically</param>
		/// <param name="getTheseComponentAttributes">Get these component attributes specifically</param>
		/// <returns>The batch</returns>
		public MESbatch LoadBatch( string batchId, string[] getTheseBatchAttributes, string[] getTheseComponentAttributes )
		{
			// Get the batch and all 'standard' component attributes
			MESbatch batch = LOTQUERYFULL(batchId);

			// Now look to see if any other batch attibutes are required
			if( getTheseBatchAttributes != null )
			{
				foreach( string attribName in getTheseBatchAttributes )
				{
					if( standardAttributes.Contains(attribName) == false )
					{
						batch.Attributes.Add( new DatumString(attribName, "Default"+attribName) );
					}
				}
			}

			// Now look to see if any other component attibutes are required
			if( getTheseComponentAttributes != null )
			{
				foreach( string attribName in getTheseComponentAttributes )
				{
					if( standardAttributes.Contains(attribName) == false )
					{
						foreach( DUTObject dut in batch )
						{
							string componentId = dut.SerialNumber;
                            dut.Attributes.Add (new DatumString(attribName, "Default" + attribName));
						}
					}
				}
			}

			return batch;
		}

		/// <summary>
		/// Track batch into next step.
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Batch to track in</param>
		public void TrackIn( string batchId )
		{
		}

		/// <summary>
		/// Set a batch level attribute.
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="attributeName">Attribute name</param>
		/// <param name="attributeValue">Attribute value</param>
		public void SetBatchAttribute( string batchId, string attributeName, string attributeValue )
		{
		}

		/// <summary>
		/// Set a component level attribute
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="componentId">Component</param>
		/// <param name="attributeName">Attribute name</param>
		/// <param name="attributeValue">Attribute value</param>
		public void SetComponentAttribute( string batchId, string componentId, string attributeName, string attributeValue )
		{
		}

		/// <summary>
		/// Mark the selected components as defective
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="componentIDlist">Components to mark as defective</param>
		/// <param name="failCodeList">Corresponding failure codes (one per component)</param>
		public 	void MarkAsDefective( string batchId, string[] componentIDlist, string[] failCodeList )
		{
		}

		/// <summary>
		/// Split a component into a new batch and track out
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="partCode">Partcode to assign to component</param>
        /// <returns>New batch containing component: "DefaultSplitBatchId" </returns>
		public string TrackOutComponentPass( string batchId, string stage, string componentId, string partCode )
		{
			return ("DefaultSplitBatchId");
		}

		/// <summary>
		/// Split a component into a new batch and send for rework
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="reworkCode">Rework code</param>
        /// <returns>New batch containing component: "DefaultReworkBatchId"</returns>
		public string TrackOutComponentRework( string batchId, string stage, string componentId, string reworkCode )
		{
			return  ("DefaultReworkBatchId");
		}

		/// <summary>
		/// Split a component into a new batch and put on hold
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="onHoldCode">On-hold code</param>
        /// <returns>New batch containing component: "DefaultOnHoldBatchId"</returns>
		public string TrackOutComponentOnHold( string batchId, string stage, string componentId, string onHoldCode )
		{
            return ("DefaultOnHoldBatchId");
		}

		/// <summary>
		/// Split a component into a new batch and scrap
        /// This incarnation is a stub that does nothing.
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to scrap</param>
		/// <param name="failCode">Failure code</param>
		public void TrackOutComponentFail( string batchId, string stage, string componentId, string failCode )
		{
		}

		/// <summary>
		/// Track out whole batch and optionally assign new part code
        /// This incarnation is a stub that does nothing
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="partCode">New part code (optional)</param>
		public void TrackOutBatchPass( string batchId, string stage, string partCode )
		{
		}

		/// <summary>
		/// Track out whole batch and put on hold
        /// This incarnation is a stub that does nothing
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="stage">Stage</param>
        /// <param name="onHoldCode">On-hold code</param>
		public void TrackOutBatchOnHold( string batchId, string stage, string onHoldCode )
		{
		}
		
        # endregion

		#region Private Methods

		/// <summary>
		/// Perform a LOTQUERY
		/// For the specified batch, return default  product, stage and components therein 
		/// </summary>
		/// <param name="batchId">The batch to load</param>
		/// <returns>The batch as an MESbatch object</returns>
		private MESbatch LOTQUERY( string batchId )
		{	
                MESbatch batch = new MESbatch();
                batch.Stage = "DefaultTestStage";
                batch.PartCode = "DefaultPartCode";
			
				DUTObject dut = new DUTObject();
                dut.SerialNumber = "00000";
				batch.Add( dut );

                this.logger.DatabaseTransactionWrite(DefaultMESPlugin.LogLabelRead, "DefaultMES LoadBatch" + " - Specified Attributes " + "BatchId= " + batchId + " Made-up data ...");

    			return batch;
		}

		/// <summary>
		/// Perform a LOTQUERYFULL
		/// As LOTQUERY but load all 'standard' component attributes
		/// </summary>
		/// <param name="batchId">The batch to load</param>
		/// <returns>The batch as an MESbatch object</returns>
		private MESbatch LOTQUERYFULL( string batchId )
		{
            MESbatch batch = new MESbatch();
            batch.Stage = "DefaultTestStage";
            batch.PartCode = "DefaultPartCode";

            // Add a default set of attributes to the DUT Object. Set them all to default values.
            DUTObject dut = new DUTObject();
            dut.BatchID = batchId;
            dut.SerialNumber = "00000";
            dut.TrayPosition = "0";
            dut.PartCode = batch.PartCode;
            dut.TestStage = batch.Stage;
            dut.BatchID = batchId;
            dut.EquipmentID = "000";
            dut.Attributes.AddString("FacetCoatId", "DefaultFacetCoatId");
            dut.Attributes.AddString("WaferId", "DefaultWaferId");
            dut.Attributes.AddString("ChipId", "DefaultChipId");
            dut.Attributes.AddString("OutputTray", "DefaultOutputTray");
            dut.Attributes.AddString("2DBarcode", "Default2DBarcode");
            dut.Attributes.AddString("MesaWidth", "DefaultMesaWidth");
            dut.Attributes.AddString("SpecificId", "DefaultSpecificId");
            dut.Attributes.AddString("FibreType", "DefaultFibreType");

            this.logger.DatabaseTransactionWrite(DefaultMESPlugin.LogLabelRead, "DefaultMES LoadBatch" + " " + "BatchId= " + batchId + " Made-up data ...");

            return batch;
		}
		
		#endregion
		

		#region Private Data
        /// <summary>Node</summary>
        private int node;

		/// <summary>Standard attributes as returned by LOTQUERYFULL</summary>
		private StringCollection standardAttributes;

        /// <summary>
        /// Reference to component providing logging functionality.
        /// </summary>
        private ILogging logger;

        /// <summary>
        /// Log entry application label for MES data reads (differs slightly from real MES plugin).
        /// </summary>
        private const string LogLabelRead = "MESDataRdDefault";

        /// <summary>
        /// Log entry application label for MES data writes (differs slightly from real MES plugin).
        /// </summary>
        private const string LogLabelWrite = "MESDataWrDefault";

		#endregion
	}
}

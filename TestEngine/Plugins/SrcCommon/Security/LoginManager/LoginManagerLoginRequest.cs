// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/LoginManagerLoginRequest.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Inherited message payload (login manager -> security server) for login credentials.
	/// </summary>
	public class LoginManagerLoginRequest: LoginRequestMsg
	{
		/// <summary>
		/// Constructs the login request message.
		/// </summary>
		/// <param name="userId">User id, passed into base class.</param>
		/// <param name="passWord">Password text.</param>
		public LoginManagerLoginRequest(string userId, string passWord)
			:base(userId)
		{
			this.PassWord = passWord;
		}

		/// <summary>
		/// Gets the login password.
		/// </summary>
		public readonly string PassWord;
	}
}

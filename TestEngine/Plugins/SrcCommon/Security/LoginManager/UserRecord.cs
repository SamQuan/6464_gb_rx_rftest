// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/UserRecord.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using Bookham.TestEngine.PluginInterfaces.Security;	/* For PrivilegeLevel */

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Class containing information about a single user.
	/// </summary>
	public class UserRecord
	{
		/// <summary>
		/// Construct a single user record.
		/// </summary>
		/// <param name="user">User's name.</param>
		/// <param name="passHash">User's hashed password.</param>
		/// <param name="priv">User's privilege.</param>
		internal UserRecord(string user, string passHash, PrivilegeLevel priv)
		{
			this.User = user;
			this.PassHash = passHash;
			this.Priv = priv;
		}

		/// <summary>
		/// Gets the user's name.
		/// </summary>
		public readonly string User;

		/// <summary>
		/// Gets the user's password hash string.
		/// </summary>
		public readonly string PassHash;

		/// <summary>
		/// Gets the user's privilege setting.
		/// </summary>
		public readonly PrivilegeLevel Priv;
	}
}

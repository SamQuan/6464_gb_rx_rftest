// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/UserInfoXml.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.PluginInterfaces.Security;	/* For PrivilegeLevel */

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Class holding cached user information and tools to load the file.
	/// </summary>
	public class UserInfoXml
	{
		/// <summary>
		/// Load user info from file. Any exceptions thrown in the process will be allowed to
		/// fall.
		/// </summary>
		/// <exception cref="UserInfoXmlException">
		/// Thrown on expected errors, including;
		/// <list type="bullet">
		/// <item>File not found,</item>
		/// <item>Access denied,</item>
		/// <item>IO failure,</item>
		/// <item>XML parser failure,</item>
		/// <item>Bad Privilege value,</item>
		/// </list>
		/// </exception>
		public UserInfoXml()
		{
			this.users = new SortedList();

			/* Data for current user record loaded in from XML file. */
			string loadedUser = "";
			string loadedPassHash = "";
			PrivilegeLevel loadedPriv = PrivilegeLevel.Operator;

			/* Open user data XML file. This constructor call only conducts a scant validation of the
			 * file name. Any expected error will not occur until the first call to Read. */
			XmlTextReader userData = new XmlTextReader(userDataFileName);

			try
			{
				/* Scan file, one element a time. */
				while (userData.Read())
				{
					/* Found the start of an element. */
					if (userData.NodeType == XmlNodeType.Element)
					{
						/* If element is "User" clear out older data. */
						if (userData.LocalName == "User")
						{
							loadedUser = "";
							loadedPassHash = "";
							loadedPriv = PrivilegeLevel.Operator;
						}

							/* Else if found members of User element, then store data. */
						else if (userData.LocalName == "UserId")
						{
							loadedUser = userData.ReadString();
						}
						else if (userData.LocalName == "Password")
						{
							loadedPassHash = userData.ReadString();
						}
						else if (userData.LocalName == "Privilege")
						{
							string loadedPrivStr = userData.ReadString();

							try
							{
								/* Convert to enum, allowing parse exceptions to fall */
								loadedPriv = 
									(PrivilegeLevel)Enum.Parse(typeof(PrivilegeLevel), loadedPrivStr, true);
							}
							catch (ArgumentException ae)
							{
								/* Privilege field contained neither "Operator" or "Technician". */
								throw new UserInfoXmlException(
									String.Format(
										UserInfoXml.malformedPrivilegeFormat,
										System.IO.Path.GetFileName(userDataFileName),
										userData.LineNumber,
										userData.LinePosition,
										loadedPrivStr),
									ae);
							}
						}
					} /* End if found element. */

						/* Found the </User> tag. */
					else if ((userData.NodeType == XmlNodeType.EndElement) && 
						(userData.LocalName == "User"))
					{
						this.users.Add(
							loadedUser, 
							new UserRecord(loadedUser, loadedPassHash, loadedPriv));
					}
				} /* End while */
			} /* End try */
			catch (UnauthorizedAccessException uae)
			{
				/* User did not have read permission for file. */
				throw new UserInfoXmlException(
					String.Format(
						UserInfoXml.accessDeniedFormat, System.IO.Path.GetFileName(userDataFileName)),
					uae);
			}
			catch (System.IO.FileNotFoundException fnfe)
			{
				/* File not found error. */
				throw new UserInfoXmlException(
					String.Format(UserInfoXml.fileNotFoundFormat, fnfe.FileName),
					fnfe);
			}
			catch (System.IO.IOException ioe)
			{
				/* General IO failure. Not file not found. */
				throw new UserInfoXmlException(
					String.Format(
						UserInfoXml.generalIoFailureFormat,
						System.IO.Path.GetFileName(userDataFileName),
						ioe.Message),
					ioe);
			}
			catch (System.Xml.XmlException xe)
			{
				/* XML parse error. */
				throw new UserInfoXmlException(
					String.Format(
						UserInfoXml.malformedXmlFormat,
						System.IO.Path.GetFileName(userDataFileName),
						xe.Message),
					xe);
			}
			finally
			{
				/* Close file. */
				userData.Close();
			}
		}

		/// <summary>
		/// Gets the loaded user record by user name key.
		/// </summary>
		public UserRecord this[string user]
		{
			get
			{
				/* Get object from collection */
				object userObj = this.users[user];
				/* Return null if not found. */
				if (userObj == null) return null;
				/* Return user record, or null if object is wrong type. */
				return  userObj as UserRecord;
			}
		}
				
		#region Private data
		/// <summary>
		/// Collection of UserRecord instances, key'd by user name.
		/// </summary>
		readonly SortedList users;

		/// <summary>
		/// Location of username list.
		/// </summary>
		const string userDataFileName = "Configuration/UserInfo.xml";
		#endregion

		#region Error messages
		/// <summary>
		/// Template for a Malformed privilege field error message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>{0} will be replaced with short file name.</item>
		/// <item>{1} will be replaced with line number where error was detected.</item>
		/// <item>{2} will be replaced with the poisition on the line where the error was detected.</item>
		/// <item>{3} will be replaved with the malformed data value.</item>
		/// </list></remarks>
		const string malformedPrivilegeFormat =
			"\"{0}\" line {1} position {2}: Privilege field contained \"{3}\", " +
			"instead of \"Operator\" or \"Technician\".";

		/// <summary>
		/// Template for a file access denied error.
		/// </summary>
		/// <remarks>{0} will be replaced with file name which failed to open for lack of privilege.</remarks>
		const string accessDeniedFormat = "Access to \"{0}\" denied.";
 
		/// <summary>
		/// Template for a file not found error message.
		/// </summary>
		/// <remarks>{0} will be replaced with full path of missing file.</remarks>
		const string fileNotFoundFormat = "File \"{0}\" not found. Contact your local technician.";
		
		/// <summary>
		/// Template for a general IO failure message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>{0} will be replaced with failed filename.</item>
		/// <item>{1} Will be replaced with specific error message.</item>
		/// </list></remarks>
		const string generalIoFailureFormat = "General IO failure reading \"{0}\". {1}";

		/// <summary>
		/// Template for a malformed XML error message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>(0} will be replaced with the short filename of the malformed XML file.</item>
		/// <item> {1} will be replaced withe the error generated by the XML parser.</item>
		/// </list></remarks>
		const string malformedXmlFormat = "Malformed XML in \"{0}\". {1}";																	 

		#endregion
		
	}
}

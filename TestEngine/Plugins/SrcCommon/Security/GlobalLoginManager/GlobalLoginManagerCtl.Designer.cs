namespace Bookham.TestLibrary.Security
{
    partial class GlobalLoginManagerCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            //GlobalLoginManagerCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "GlobalLoginManagerCtl";
            this.Enter += new System.EventHandler(this.GlobalLoginManagerCtl_Enter);
            this.Load += new System.EventHandler(this.GlobalLoginManagerCtl_Load);
            this.ActOnIntoView += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.IntoViewDlg(this.GlobalLoginManagerCtl_ActOnIntoView);
            this.LoginResponse += new Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.LoginResponseDelegate(this.GlobalLoginManagerCtl_LoginResponse);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Timer timer1;
    }
}

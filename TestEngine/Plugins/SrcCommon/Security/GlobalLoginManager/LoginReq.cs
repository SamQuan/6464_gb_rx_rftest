// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
// LoginReq.cs
//
// Author: Ian Gurney, 2006
// Design: [Reference design documentation]

using System;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Inherited message payload (login manager -> security server) for login credentials.
	/// </summary>
    public class LoginReq : LoginRequestMsg
	{
        /// <summary>
        /// Internal message.
        /// </summary>
        /// <param name="userName">User name which can be ignored in this instance.</param>
        /// <param name="returnMsg">Message to be returned to Test Engine.</param>
		public LoginReq(string userName, LoginResponseMsg returnMsg) : base(userName)
        {
            msg = returnMsg;
        }

        /// <summary>
        /// Message to be returned to Test Engine.
        /// </summary>
        public readonly LoginResponseMsg msg;
    }
}

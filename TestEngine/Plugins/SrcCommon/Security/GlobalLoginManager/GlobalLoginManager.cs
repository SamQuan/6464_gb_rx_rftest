// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestPrograms
//
//GlobalLoginManager.cs
//
// Author: Ian Gurney, 2006
// Design: [Reference design documentation]

using System;
using System.Xml;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
    /// <summary>
    /// Bookham login plugin.
    /// </summary>
    public class GlobalLoginManager : ISecurityPlugin
    {
        /// <summary>
        /// Bookham login plugin.
        /// </summary>
        public GlobalLoginManager()
        {
            // Nothing here //
        }

        /// <summary>
        /// Return type of GUI control to use with this plugin.
        /// </summary>
        public Type Control
        {
            get
            {
                return typeof(GlobalLoginManagerCtl);
            }
        }

        /// <summary>
        /// Process a login request.
        /// </summary>
        /// <param name="msg">Authenticated credentials from the login GUI control.</param>
        /// <param name="logWriter">Reference to log writer object.</param>
        /// <returns>Success/failure result.</returns>
        public LoginResponseMsg LoginRequest(LoginRequestMsg msg, ILogWriter logWriter)
        {
            LoginReq oRequest = msg as LoginReq;
            return oRequest.msg;
        }

        /// <summary>
        /// Process a logout notification.
        /// </summary>
        /// <remarks>This class cannot fail a logout request.</remarks>
        /// <param name="user">Logging out user.</param>
        /// <param name="priv">Former user's privilege.</param>
        /// <param name="logWriter">Reference to log writer object.</param>
        /// <returns>Success result.</returns>
        public LogoutStatus LogoutNotification(string user, PrivilegeLevel priv, ILogWriter logWriter)
        {
            return LogoutStatus.Success;
        }
    }
}
